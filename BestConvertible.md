Best Linux Convertible
======================

This is a list of convertibles with best Linux-compatibility in the terms of
maximal number of Linux-compatible devices on board and maximal overall
popularity of a model.

Everyone can contribute to this report by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Contents
--------

1. [ By Model ](#by-model)
2. [ By Vendor ](#by-vendor)

By Model
--------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Model                                            | Year | Samples | Devices | Non-working |
|----------------------|--------------------------------------------------|------|---------|---------|-------------|
| Hewlett-Packard      | ENVY x360 m6 Convertible                         | 2015 | 13      | 47      | 0           |
| Dell                 | Inspiron 7306 2n1                                | 2020 | 11      | 49      | 0           |
| ASUSTek Computer     | TP410UAR                                         | 2017 | 11      | 36      | 0           |
| AMI                  | Intel                                            | 2019 | 8       | 48      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-bq1xx                  | 2018 | 8       | 48      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop TP412FA_TP412FA              | 2019 | 8       | 40      | 0           |
| Lenovo               | Yoga 710-14IKB 80V4                              | 2017 | 8       | 35      | 0           |
| Dell                 | Inspiron 7415 2-in-1                             | 2021 | 7       | 51      | 0           |
| Medion               | E2292                                            | 2019 | 7       | 49      | 0           |
| ASUSTek Computer     | ZenBook UX462DA                                  | 2019 | 7       | 45      | 0           |
| Lenovo               | Yoga 510-14ISK 80S7                              | 2018 | 7       | 35      | 0           |
| Acer                 | Spin SP315-51                                    | 2017 | 7       | 35      | 0           |
| Hewlett-Packard      | Stream x360 Convertible 11-aa0XX                 | 2016 | 7       | 30      | 0           |
| Hewlett-Packard      | Spectre x360 Convertible 13-ac0XX                | 2016 | 6       | 40      | 0           |
| Dell                 | Inspiron 7373                                    | 2018 | 6       | 34      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-bp0xx                  | 2017 | 6       | 34      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15-bq1xx                   | 2018 | 5       | 48      | 0           |
| Samsung Electronics  | 950QDB                                           | 2021 | 5       | 43      | 0           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-dw1xxx              | 2020 | 5       | 43      | 0           |
| Dell                 | Inspiron 7391 2n1                                | 2020 | 5       | 43      | 0           |
| ASUSTek Computer     | UX370UAR                                         | 2017 | 5       | 40      | 0           |
| Acer                 | Spin SP111-34N                                   | 2019 | 5       | 38      | 0           |
| Hewlett-Packard      | Spectre x360 Convertible 13-w0XX                 | 2016 | 5       | 33      | 0           |
| Hewlett-Packard      | Spectre x360 Convertible 13                      | 2016 | 5       | 28      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-cp0xxx                 | 2019 | 4       | 57      | 0           |
| Positivo             | C464C                                            | 2020 | 4       | 46      | 0           |
| Acer                 | Spin SP313-51N                                   | 2020 | 4       | 46      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-eu0xxx                 | 2021 | 4       | 42      | 0           |
| Hewlett-Packard      | Pavilion x360 Convertible 14m-cd0xxx             | 2018 | 4       | 39      | 0           |
| Lenovo               | Yoga 510-14AST 80S9                              | 2017 | 4       | 37      | 0           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-ba1xx               | 2017 | 4       | 35      | 0           |
| ASUSTek Computer     | UX461UA                                          | 2017 | 4       | 33      | 0           |
| TrekStor             | YOURBOOK C11B                                    | 2018 | 3       | 47      | 0           |
| Dell                 | Latitude 3190 2-in-1                             | 2020 | 3       | 46      | 0           |
| Dell                 | Inspiron 7573                                    | 2018 | 3       | 44      | 0           |
| Dell                 | Latitude 3390 2-in-1                             | 2019 | 3       | 41      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15-ed0xxx                  | 2020 | 3       | 40      | 0           |
| ASUSTek Computer     | UX370UAF                                         | 2017 | 3       | 40      | 0           |
| Dell                 | Inspiron 5481                                    | 2019 | 3       | 39      | 0           |
| Dell                 | Inspiron 7390 2n1                                | 2019 | 3       | 38      | 0           |
| ASUSTek Computer     | ZenBook UX461FA_UX461FA                          | 2019 | 3       | 38      | 0           |
| Acer                 | Spin SP111-32N                                   | 2017 | 3       | 38      | 0           |
| ASUSTek Computer     | TP401NA                                          | 2019 | 3       | 37      | 0           |
| Lenovo               | Yoga 330-11IGM 81A6                              | 2018 | 3       | 37      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 13-y0XX                    | 2016 | 3       | 37      | 0           |
| ASUSTek Computer     | Q325UA                                           | 2019 | 3       | 34      | 0           |
| Samsung Electronics  | 740U3L                                           | 2016 | 3       | 31      | 0           |
| Hewlett-Packard      | Convertible x360 11-ab0XX                        | 2017 | 3       | 27      | 0           |
| Lenovo               | ThinkPad X13 Yoga Gen 2                          | 2021 | 2       | 51      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15-ee1xxx                  | 2021 | 2       | 51      | 0           |
| Dell                 | Inspiron 7300 2n1                                | 2020 | 2       | 51      | 0           |
| Medion               | E3222 MD62450                                    | 2018 | 2       | 47      | 0           |
| Medion               | E2294 MD62700                                    | 2018 | 2       | 47      | 0           |
| Irbis                | NB 111                                           | 2018 | 2       | 43      | 0           |
| Irbis                | NB131                                            | 2017 | 2       | 43      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 13m-ag0xxx                 | 2020 | 2       | 42      | 0           |
| TrekStor             | Primebook C11B                                   | 2018 | 2       | 42      | 0           |
| Dell                 | Inspiron 7500 2n1 Silver                         | 2020 | 2       | 41      | 0           |
| ASUSTek Computer     | ZenBook Q326FA_Q326FA                            | 2019 | 2       | 40      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15-bp0xx                   | 2018 | 2       | 40      | 0           |
| ASUSTek Computer     | VivoBook Flip 14_ASUS Flip TP412UA               | 2018 | 2       | 40      | 0           |
| Hewlett-Packard      | Pavilion x360 Convertible 14m-dw0xxx             | 2021 | 2       | 39      | 0           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-dw0xxx              | 2020 | 2       | 39      | 0           |
| ASUSTek Computer     | Q535UD                                           | 2018 | 2       | 39      | 0           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-bq0xx                  | 2017 | 2       | 39      | 0           |
| ASUSTek Computer     | UX461UN                                          | 2018 | 2       | 38      | 0           |
| Acer                 | Spin SP314-54N                                   | 2021 | 2       | 36      | 0           |
| Acer                 | Spin SP111-33                                    | 2020 | 2       | 36      | 0           |
| Lenovo               | Flex 6-11IGM 81A7                                | 2019 | 2       | 36      | 0           |
| Lenovo               | IdeaPadFlex 4-1470 80SA                          | 2017 | 2       | 36      | 0           |
| ASUSTek Computer     | TP401NAS                                         | 2017 | 2       | 36      | 0           |
| Hewlett-Packard      | Pavilion x360 m3 Convertible                     | 2016 | 2       | 36      | 0           |
| Dell                 | Inspiron 5400 2n1                                | 2020 | 2       | 35      | 0           |
| Lenovo               | Yoga 710-11ISK 80TX                              | 2017 | 2       | 34      | 0           |
| Hewlett-Packard      | Pavilion x360 Convertible 14m-ba0xx              | 2017 | 2       | 34      | 0           |
| Hewlett-Packard      | ENVY x360 NoteBook PC                            | 2017 | 2       | 34      | 0           |
| ASUSTek Computer     | Q325UAR                                          | 2017 | 2       | 34      | 0           |
| Acer                 | Spin SP513-51                                    | 2016 | 2       | 34      | 0           |
| ASUSTek Computer     | UX360CAK                                         | 2019 | 2       | 33      | 0           |
| Hewlett-Packard      | Spectre Folio Convertible 13-ak0xxx              | 2018 | 2       | 32      | 0           |
| Acer                 | TravelMate Spin B118-R                           | 2017 | 2       | 32      | 0           |
| Hewlett-Packard      | Pavilion x360 m1 Convertible                     | 2016 | 2       | 32      | 0           |
| Lenovo               | N23 80UR                                         | 2016 | 2       | 30      | 0           |
| Medion               | E2228T MD61250                                   | 2017 | 2       | 27      | 0           |
| Medion               | E2228T MD60250                                   | 2017 | 2       | 26      | 0           |
| Casper               | CASPER N400 GRI 11.6 INC 2in1 4GB 32GB           | 2017 | 2       | 24      | 0           |
| TrekStor             | SurfTab twin 11.6                                | 2016 | 2       | 23      | 0           |
| Hewlett-Packard      | Pavilion x360 Convertible                        | 2015 | 30      | 41      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible 13-ae0xx                | 2017 | 27      | 50      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible 13-ap0xxx               | 2019 | 24      | 49      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible                         | 2015 | 23      | 48      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible 15-ee0xxx                  | 2020 | 22      | 54      | 1           |
| Lenovo               | Yoga 7 14ITL5 82BH                               | 2020 | 19      | 61      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-cd0xxx              | 2018 | 19      | 41      | 1           |
| Hewlett-Packard      | EliteBook x360 1030 G2                           | 2017 | 18      | 54      | 1           |
| Lenovo               | Yoga C940-14IIL 81Q9                             | 2019 | 18      | 44      | 1           |
| Dell                 | XPS 13 9365                                      | 2017 | 17      | 54      | 1           |
| Dell                 | Inspiron 5406 2n1                                | 2020 | 17      | 47      | 1           |
| Lenovo               | Yoga C930-13IKB 81C4                             | 2018 | 16      | 49      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-ba0xx               | 2017 | 16      | 39      | 1           |
| Lenovo               | ThinkPad X1 Yoga 2nd                             | 2017 | 14      | 47      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-dh1xxx              | 2019 | 14      | 44      | 1           |
| Lenovo               | Yoga 9 14ITL5 82BG                               | 2020 | 13      | 55      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible 13-ag0xxx                  | 2018 | 13      | 51      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible 15-cp0xxx                  | 2018 | 13      | 49      | 1           |
| Dell                 | XPS 15 9575                                      | 2019 | 12      | 55      | 1           |
| Lenovo               | ThinkPad X380 Yoga                               | 2018 | 12      | 52      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible 15-cn0xxx                  | 2018 | 12      | 47      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 15-cr0xxx              | 2018 | 12      | 37      | 1           |
| Teclast              | F5                                               | 2018 | 11      | 49      | 1           |
| Lenovo               | IdeaPad C340-14IWL 81RL                          | 2019 | 11      | 43      | 1           |
| Hewlett-Packard      | Elite Dragonfly                                  | 2019 | 10      | 52      | 1           |
| Lenovo               | Yoga C740-15IML 81TD                             | 2019 | 10      | 42      | 1           |
| Dell                 | Inspiron 7386                                    | 2018 | 10      | 39      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible 15-ed1xxx                  | 2020 | 9       | 52      | 1           |
| Dell                 | Inspiron 7405 2n1                                | 2020 | 9       | 47      | 1           |
| Lenovo               | Yoga 520-14IKB 80X8                              | 2017 | 9       | 45      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible 13-aw2xxx               | 2020 | 8       | 53      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible                            | 2017 | 8       | 48      | 1           |
| Lenovo               | Yoga 520-14IKB 81C8                              | 2017 | 8       | 40      | 1           |
| Lenovo               | Yoga 920-13IKB 80Y7                              | 2017 | 8       | 39      | 1           |
| Dell                 | Inspiron 5491 2n1                                | 2019 | 8       | 37      | 1           |
| Dell                 | Latitude 7410                                    | 2020 | 7       | 59      | 1           |
| Dell                 | Inspiron 7591 2n1                                | 2019 | 7       | 51      | 1           |
| Lenovo               | ThinkPad L390 Yoga                               | 2018 | 7       | 50      | 1           |
| Google               | Eve                                              | 2019 | 7       | 47      | 1           |
| Lenovo               | ThinkBook 14s Yoga ITL 20WE                      | 2020 | 7       | 45      | 1           |
| Lenovo               | Yoga 730-13IKB 81CT                              | 2018 | 7       | 45      | 1           |
| Lenovo               | Yoga 720-13IKB 81C3                              | 2017 | 7       | 44      | 1           |
| Acer                 | Spin SP513-54N                                   | 2020 | 7       | 42      | 1           |
| Lenovo               | ThinkPad Yoga 370                                | 2019 | 7       | 38      | 1           |
| Hewlett-Packard      | Stream x360 Convertible PC 11                    | 2014 | 7       | 32      | 1           |
| Dell                 | Latitude 7400 2-in-1                             | 2019 | 6       | 50      | 1           |
| Lenovo               | IdeaPad 2in1 14 81CW                             | 2017 | 6       | 47      | 1           |
| Dell                 | Inspiron 5482                                    | 2019 | 6       | 45      | 1           |
| Dell                 | Inspiron 7586                                    | 2019 | 6       | 43      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-cd1xxx              | 2018 | 6       | 43      | 1           |
| ASUSTek Computer     | UX360UAK                                         | 2018 | 6       | 41      | 1           |
| Lenovo               | Yoga 720-13IKB 80X6                              | 2017 | 6       | 41      | 1           |
| Lenovo               | IdeaPad Flex 5 15IIL05 81X3                      | 2020 | 6       | 38      | 1           |
| Lenovo               | Yoga 710-15IKB 80V5                              | 2017 | 6       | 35      | 1           |
| Prestigio            | PNT10131DEDB                                     | 2017 | 6       | 24      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible 15-bl1XX                | 2017 | 5       | 57      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible 14-ea0xxx               | 2020 | 5       | 56      | 1           |
| Dell                 | Inspiron 7791 2n1                                | 2019 | 5       | 52      | 1           |
| Lenovo               | IdeaPad Flex 5 14ITL05 82HS                      | 2020 | 5       | 41      | 1           |
| Lenovo               | IdeaPad FLEX-14IWL 81SQ                          | 2019 | 5       | 40      | 1           |
| Samsung Electronics  | 950QCG                                           | 2020 | 5       | 39      | 1           |
| Lenovo               | Yoga 730-13IWL 81JR                              | 2018 | 5       | 38      | 1           |
| Lenovo               | Yoga 720-12IKB 81B5                              | 2017 | 5       | 37      | 1           |
| Hewlett-Packard      | EliteBook x360 1040 G5                           | 2020 | 4       | 54      | 1           |
| Hewlett-Packard      | EliteBook x360 830 G6                            | 2019 | 4       | 53      | 1           |
| Lenovo               | Yoga 730-15IWL 81JS                              | 2018 | 4       | 51      | 1           |
| Lenovo               | Yoga C940-15IRH 81TE                             | 2019 | 4       | 46      | 1           |
| TrekStor             | Primebook C13                                    | 2017 | 4       | 46      | 1           |
| Lenovo               | IdeaPadFlex 5 14ALC05 82HU                       | 2021 | 4       | 44      | 1           |
| Lenovo               | Yoga S740-15IRH 81NX                             | 2019 | 4       | 43      | 1           |
| Dell                 | Inspiron 7590 2n1                                | 2019 | 4       | 42      | 1           |
| Lenovo               | IdeaPad FLEX 4-1470 80SA                         | 2017 | 4       | 38      | 1           |
| Lenovo               | ThinkPad X1 Carbon 4th                           | 2019 | 4       | 36      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible 15-bl0XX                | 2017 | 4       | 36      | 1           |
| ASUSTek Computer     | Q504UAK                                          | 2016 | 4       | 32      | 1           |
| Cube                 | SurfTab twin 11.6                                | 2016 | 4       | 25      | 1           |
| Dell                 | Inspiron 5485 2n1                                | 2019 | 3       | 54      | 1           |
| Lenovo               | Yoga Book C930 ZA3S                              | 2020 | 3       | 46      | 1           |
| Lenovo               | IdeaPadFlex 5 15IIL05 81X3                       | 2020 | 3       | 46      | 1           |
| Samsung Electronics  | 940X5N                                           | 2018 | 3       | 46      | 1           |
| Lenovo               | Yoga 9 15IMH5 82DE                               | 2020 | 3       | 45      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible 13m-bd0xxx                 | 2020 | 3       | 43      | 1           |
| Lenovo               | IdeaPad FLEX 6-14ARR 81HA                        | 2018 | 3       | 42      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 15-dq1xxx              | 2019 | 3       | 41      | 1           |
| Samsung Electronics  | 930QCG                                           | 2020 | 3       | 40      | 1           |
| Hewlett-Packard      | EliteBook x360 830 G7 Notebook PC                | 2020 | 3       | 40      | 1           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-cn0xxx                 | 2018 | 3       | 40      | 1           |
| Lenovo               | IdeaPad Flex 5 14IIL05 81WS                      | 2020 | 3       | 39      | 1           |
| Hewlett-Packard      | ZBook Studio x360 G5                             | 2020 | 3       | 39      | 1           |
| ASUSTek Computer     | ZenBook Q536FD_Q536FD                            | 2018 | 3       | 39      | 1           |
| ASUSTek Computer     | TP510UA                                          | 2017 | 3       | 37      | 1           |
| Dell                 | Latitude 5289                                    | 2018 | 3       | 36      | 1           |
| Acer                 | Spin SP513-52N                                   | 2018 | 3       | 33      | 1           |
| Lenovo               | YB1-X91F                                         | 2017 | 3       | 24      | 1           |
| Dell                 | Latitude 7420                                    | 2021 | 2       | 55      | 1           |
| Lenovo               | ThinkPad                                         | 2019 | 2       | 50      | 1           |
| Lenovo               | IdeaPadFlex 5-1570 81CA                          | 2018 | 2       | 50      | 1           |
| Medion               | E3216 MD60900                                    | 2017 | 2       | 49      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible PC                     | 2019 | 2       | 46      | 1           |
| ASUSTek Computer     | ZenBook UX461FN_UX461FN                          | 2019 | 2       | 46      | 1           |
| Hewlett-Packard      | EliteBook x360 830 G8 Notebook PC                | 2020 | 2       | 44      | 1           |
| Hewlett-Packard      | Spectre x360 Convertible 15t-eb100               | 2021 | 2       | 42      | 1           |
| Lenovo               | IdeaPad FLEX-15IML 81XH                          | 2019 | 2       | 41      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 14m-dw1xxx             | 2020 | 2       | 38      | 1           |
| Lenovo               | ThinkPad L390                                    | 2019 | 2       | 38      | 1           |
| Hewlett-Packard      | Pavilion x360 Convertible 15-dq0xxx              | 2019 | 2       | 37      | 1           |
| Acer                 | Spin SP515-51GN                                  | 2019 | 2       | 37      | 1           |
| Lenovo               | Yoga 510-14ISK 80UK                              | 2018 | 2       | 37      | 1           |
| ASUSTek Computer     | Q525UAR                                          | 2018 | 2       | 37      | 1           |
| ASUSTek Computer     | UX561UAR                                         | 2019 | 2       | 36      | 1           |
| Lenovo               | IdeaPad FLEX-15IWL 81SR                          | 2019 | 2       | 35      | 1           |
| ASUSTek Computer     | ZenBook UX562FAC_UX562FA                         | 2019 | 2       | 35      | 1           |
| Acer                 | Spin SP513-53N                                   | 2018 | 2       | 35      | 1           |
| Lenovo               | Yoga 510-15IKB 80VC                              | 2017 | 2       | 35      | 1           |
| Lenovo               | ThinkPad Yoga 460                                | 2019 | 2       | 34      | 1           |
| Dell                 | Latitude 7390 2-in-1                             | 2019 | 2       | 34      | 1           |
| ASUSTek Computer     | Q505UAR                                          | 2017 | 2       | 34      | 1           |
| ASUSTek Computer     | Q324UAK                                          | 2016 | 2       | 33      | 1           |
| ASUSTek Computer     | Q304UAK                                          | 2016 | 2       | 30      | 1           |
| Positivo             | Q432A                                            | 2019 | 2       | 27      | 1           |
| Prestigio            | PNT10130C                                        | 2016 | 2       | 27      | 1           |
| Lenovo               | ThinkPad L13 Yoga Gen 2                          | 2020 | 62      | 61      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 13-ay0xxx                  | 2020 | 42      | 55      | 2           |
| Dell                 | XPS 13 7390 2-in-1                               | 2019 | 25      | 59      | 2           |
| Lenovo               | ThinkPad X1 Yoga 4th                             | 2019 | 24      | 48      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 13-ar0xxx                  | 2019 | 21      | 54      | 2           |
| Lenovo               | Yoga 530-14IKB 81EK                              | 2018 | 20      | 39      | 2           |
| Lenovo               | Yoga 530-14ARR 81H9                              | 2018 | 18      | 44      | 2           |
| Hewlett-Packard      | Pavilion x360 Convertible 14-dh0xxx              | 2019 | 18      | 43      | 2           |
| Lenovo               | ThinkPad X1 Yoga 1st                             | 2017 | 18      | 38      | 2           |
| Hewlett-Packard      | ProBook x360 435 G7                              | 2020 | 16      | 49      | 2           |
| Lenovo               | Yoga 720-15IKB 80X7                              | 2017 | 15      | 52      | 2           |
| Lenovo               | Yoga 910-13IKB 80VF                              | 2016 | 15      | 49      | 2           |
| Dell                 | Inspiron 7506 2n1                                | 2020 | 14      | 55      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 15-ds0xxx                  | 2019 | 14      | 50      | 2           |
| Hewlett-Packard      | Spectre x360 Convertible 13-aw0xxx               | 2019 | 14      | 49      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 15-dr1xxx                  | 2019 | 12      | 55      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-ee0xxx                 | 2020 | 12      | 44      | 2           |
| Lenovo               | IdeaPad Flex 5 14IIL05 81X1                      | 2020 | 12      | 39      | 2           |
| Lenovo               | Yoga C640-13IML 81UE                             | 2019 | 12      | 35      | 2           |
| Hewlett-Packard      | EliteBook x360 1030 G3                           | 2019 | 11      | 50      | 2           |
| Lenovo               | IdeaPad FLEX 5-1570 81CA                         | 2017 | 11      | 40      | 2           |
| Lenovo               | Yoga 730-15IKB 81CU                              | 2019 | 11      | 39      | 2           |
| Lenovo               | ThinkPad Yoga 260                                | 2019 | 10      | 36      | 2           |
| Lenovo               | Yoga 510-14IKB 80VB                              | 2017 | 9       | 37      | 2           |
| Hewlett-Packard      | Spectre x360 Convertible 15-df0xxx               | 2018 | 8       | 48      | 2           |
| Lenovo               | ThinkPad L380 Yoga                               | 2018 | 8       | 42      | 2           |
| Hewlett-Packard      | EliteBook x360 1040 G6                           | 2019 | 7       | 48      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 15-cn1xxx                  | 2018 | 7       | 47      | 2           |
| Dell                 | Inspiron 7706 2n1                                | 2020 | 7       | 45      | 2           |
| Lenovo               | Yoga 520-14IKB 80YM                              | 2017 | 7       | 37      | 2           |
| Lenovo               | ThinkPad X390 Yoga                               | 2019 | 6       | 53      | 2           |
| Hewlett-Packard      | Spectre x360 Convertible 15-eb0xxx               | 2020 | 6       | 51      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 15-dr0xxx                  | 2019 | 6       | 48      | 2           |
| Lenovo               | ThinkPad X1 Yoga Gen 5                           | 2020 | 6       | 44      | 2           |
| Dell                 | Latitude 9420                                    | 2021 | 5       | 61      | 2           |
| Lenovo               | Yoga 6 13ALC6 82ND                               | 2021 | 5       | 47      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop TP420IA_TM420IA              | 2020 | 5       | 47      | 2           |
| Dell                 | Latitude 5300 2-in-1                             | 2019 | 5       | 46      | 2           |
| Dell                 | Inspiron 7786                                    | 2019 | 5       | 46      | 2           |
| Hewlett-Packard      | ENVY x360 Convertible 15-ds1xxx                  | 2020 | 5       | 44      | 2           |
| Lenovo               | Yoga 6 13ARE05 82FN                              | 2020 | 5       | 43      | 2           |
| Lenovo               | Yoga 310-11IAP 80U2                              | 2017 | 5       | 39      | 2           |
| Hewlett-Packard      | Spectre x360 Convertible 15t-df100               | 2019 | 4       | 52      | 2           |
| Hewlett-Packard      | ProBook x360 440 G1                              | 2019 | 4       | 45      | 2           |
| Lenovo               | Yoga 7 15ITL5 82BJ                               | 2020 | 3       | 48      | 2           |
| Hewlett-Packard      | EliteBook x360 1030 G4                           | 2019 | 3       | 46      | 2           |
| Hewlett-Packard      | Spectre x360 Convertible 15-eb1xxx               | 2021 | 3       | 45      | 2           |
| Hewlett-Packard      | EliteBook x360 1040 G7 Notebook PC               | 2020 | 3       | 41      | 2           |
| Lenovo               | Yoga C930-13IKB Glass 81EQ                       | 2018 | 3       | 41      | 2           |
| Lenovo               | ThinkPad P40 Yoga                                | 2019 | 3       | 40      | 2           |
| Acer                 | Aspire R5-571TG                                  | 2016 | 3       | 36      | 2           |
| Dell                 | Latitude 9520                                    | 2021 | 2       | 54      | 2           |
| Lenovo               | IdeaPad S740-15IRH 81NY                          | 2019 | 2       | 45      | 2           |
| Lenovo               | Yoga C640-13IML LTE 81XL                         | 2020 | 2       | 37      | 2           |
| Lenovo               | IdeaPadFlex 5 14IIL05 81X1                       | 2020 | 2       | 36      | 2           |
| Lenovo               | IdeaPad FLEX 6-14IKB 81EM                        | 2018 | 2       | 34      | 2           |
| Lenovo               | IdeaPad Flex 5 14ARE05 81X2                      | 2020 | 51      | 52      | 3           |
| Lenovo               | IdeaPadFlex 5 14ARE05 81X2                       | 2020 | 17      | 45      | 3           |
| Lenovo               | ThinkPad X1 Yoga 3rd                             | 2019 | 15      | 46      | 3           |
| Hewlett-Packard      | ENVY x360 Convertible 15-eu0xxx                  | 2021 | 11      | 50      | 3           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-ds0xxx                 | 2019 | 10      | 48      | 3           |
| Lenovo               | IdeaPad C340-14IWL 81N4                          | 2019 | 9       | 46      | 3           |
| Hewlett-Packard      | Spectre x360 Convertible 15-df1xxx               | 2019 | 7       | 47      | 3           |
| Lenovo               | ThinkPad X13 Yoga Gen 1                          | 2020 | 5       | 51      | 3           |
| Lenovo               | IdeaPad Flex 5 14ALC05 82HU                      | 2021 | 4       | 43      | 3           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-ed0xxx                 | 2020 | 2       | 35      | 3           |
| Lenovo               | Yoga C740-14IML 81TC                             | 2019 | 22      | 57      | 4           |
| Dell                 | XPS 13 9310 2-in-1                               | 2020 | 18      | 57      | 4           |
| Hewlett-Packard      | Spectre x360 Convertible 15-ch0xx                | 2018 | 16      | 49      | 4           |
| Lenovo               | ThinkPad L13 Yoga                                | 2019 | 13      | 45      | 4           |
| Lenovo               | IdeaPad C340-14IML 81TK                          | 2019 | 7       | 42      | 4           |
| Hewlett-Packard      | ENVY x360 Convertible 15m-dr1xxx                 | 2019 | 7       | 42      | 4           |
| Dell                 | Latitude 9510                                    | 2020 | 4       | 45      | 6           |

By Vendor
---------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Samples | Devices | Non-working |
|----------------------|---------|---------|-------------|
| Lenovo               | 779     | 39      | 1           |
| Hewlett-Packard      | 746     | 39      | 1           |
| Dell                 | 262     | 42      | 1           |
| ASUSTek Computer     | 115     | 36      | 1           |
| Acer                 | 55      | 36      | 1           |
| Samsung Electronics  | 30      | 38      | 1           |
| Medion               | 26      | 42      | 1           |
| TrekStor             | 14      | 42      | 1           |
| Teclast              | 12      | 44      | 1           |
| AMI                  | 8       | 45      | 0           |
| Prestigio            | 8       | 23      | 1           |
| Positivo             | 7       | 36      | 1           |
| Google               | 7       | 39      | 1           |
| Irbis                | 6       | 43      | 0           |
| Cube                 | 4       | 24      | 1           |
| LG Electronics       | 3       | 40      | 0           |
| Casper               | 3       | 25      | 1           |
| Chuwi                | 2       | 47      | 1           |
| Fujitsu              | 2       | 35      | 2           |

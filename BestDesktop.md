Best Linux Desktop
==================

This is a list of desktops with best Linux-compatibility in the terms of
maximal number of Linux-compatible devices on board and maximal overall
popularity of a model.

Everyone can contribute to this report by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Contents
--------

1. [ By Model ](#by-model)
2. [ By Vendor ](#by-vendor)

By Model
--------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Model                                            | Year | Samples | Devices | Non-working |
|----------------------|--------------------------------------------------|------|---------|---------|-------------|
| ASRock               | H55M-LE                                          | 2010 | 35      | 51      | 0           |
| ASRock               | FM2A75M Pro4+                                    | 2014 | 32      | 40      | 0           |
| ASUSTek Computer     | M4A78LT-M-LE                                     | 2009 | 29      | 36      | 0           |
| ASUSTek Computer     | P8H61-M LE/USB3                                  | 2011 | 27      | 39      | 0           |
| Gigabyte Technology  | H97-HD3                                          | 2014 | 26      | 42      | 0           |
| ASRock               | X370 Taichi                                      | 2017 | 23      | 58      | 0           |
| Medion               | MS-7728                                          | 2011 | 23      | 40      | 0           |
| Lenovo               | ThinkCentre E73 10DR0033SP                       | 2014 | 22      | 31      | 0           |
| Gigabyte Technology  | Z170X-Gaming 7                                   | 2015 | 21      | 52      | 0           |
| ASUSTek Computer     | M4A79XTD EVO                                     | 2009 | 21      | 46      | 0           |
| Gigabyte Technology  | G41MT-S2P                                        | 2010 | 21      | 34      | 0           |
| Gigabyte Technology  | X79-UD3                                          | 2011 | 20      | 92      | 0           |
| ASUSTek Computer     | A88XM-PLUS                                       | 2014 | 20      | 44      | 0           |
| ASUSTek Computer     | M4A77TD                                          | 2009 | 20      | 40      | 0           |
| Hewlett-Packard      | Pro 3500 Series                                  | 2012 | 20      | 31      | 0           |
| Hewlett-Packard      | Compaq dc7600 Small Form Factor                  | 2005 | 20      | 31      | 0           |
| ASUSTek Computer     | P5B-Deluxe                                       | 2007 | 19      | 43      | 0           |
| ASUSTek Computer     | M2A-VM                                           | 2007 | 19      | 42      | 0           |
| Gigabyte Technology  | H61M-S2V-B3                                      | 2011 | 19      | 36      | 0           |
| Gigabyte Technology  | F2A88XM-DS2                                      | 2013 | 18      | 44      | 0           |
| ASRock               | Z87 Killer                                       | 2013 | 18      | 41      | 0           |
| Gigabyte Technology  | B250M-D3H                                        | 2016 | 18      | 38      | 0           |
| Gigabyte Technology  | GA-MA78LMT-S2                                    | 2010 | 18      | 37      | 0           |
| Fujitsu              | ESPRIMO P910                                     | 2012 | 18      | 34      | 0           |
| ASUSTek Computer     | P5G41T-M LX2/BR                                  | 2009 | 18      | 31      | 0           |
| Acer                 | Aspire XC-885                                    | 2018 | 17      | 40      | 0           |
| ASUSTek Computer     | P8H67-M LX                                       | 2011 | 17      | 40      | 0           |
| ASUSTek Computer     | P8H67-V                                          | 2011 | 17      | 39      | 0           |
| Hewlett-Packard      | Compaq 8000 Elite CMT PC                         | 2009 | 17      | 39      | 0           |
| Gigabyte Technology  | P41T-D3P                                         | 2010 | 17      | 34      | 0           |
| Gigabyte Technology  | C1037UN-EU                                       | 2013 | 17      | 31      | 0           |
| Gigabyte Technology  | GA-MA785GMT-UD2H                                 | 2009 | 16      | 44      | 0           |
| Gigabyte Technology  | EP45-UD3LR                                       | 2009 | 16      | 43      | 0           |
| ASUSTek Computer     | M2A-MX                                           | 2007 | 16      | 39      | 0           |
| ASRock               | H81M-VG4 R3.0                                    | 2016 | 16      | 37      | 0           |
| ASUSTek Computer     | P8H77-M LE                                       | 2012 | 16      | 35      | 0           |
| Gigabyte Technology  | H61MS                                            | 2013 | 16      | 29      | 0           |
| ASUSTek Computer     | P7P55D PRO                                       | 2009 | 15      | 72      | 0           |
| ASUSTek Computer     | ROG STRIX X570-I GAMING                          | 2019 | 15      | 53      | 0           |
| Medion               | MS-7800                                          | 2012 | 15      | 49      | 0           |
| Gigabyte Technology  | 970-GAMING                                       | 2015 | 15      | 48      | 0           |
| ASUSTek Computer     | P5P43TD PRO                                      | 2009 | 15      | 44      | 0           |
| Gigabyte Technology  | EP43-S3L                                         | 2008 | 15      | 44      | 0           |
| ASUSTek Computer     | CM6330_CM6630_CM6730_CM6830_M11AA-8              | 2013 | 15      | 43      | 0           |
| ASRock               | Z87 Extreme4                                     | 2013 | 15      | 43      | 0           |
| Acer                 | Aspire TC-605                                    | 2013 | 15      | 38      | 0           |
| Foxconn              | G31MX Series                                     | 2007 | 15      | 34      | 0           |
| Gigabyte Technology  | H55M-D2H                                         | 2010 | 14      | 56      | 0           |
| Gigabyte Technology  | EP45-DS3                                         | 2008 | 14      | 46      | 0           |
| Medion               | MS-7646                                          | 2010 | 14      | 45      | 0           |
| ASUSTek Computer     | A88XM-E                                          | 2014 | 14      | 44      | 0           |
| MSI                  | MS-7924                                          | 2014 | 14      | 40      | 0           |
| ASRock               | H81M-HDS R2.0                                    | 2014 | 14      | 39      | 0           |
| Gateway              | DX4860                                           | 2011 | 14      | 38      | 0           |
| Pegatron             | IPPPV-D3G                                        | 2010 | 14      | 30      | 0           |
| Dell                 | Inspiron 5675                                    | 2017 | 13      | 53      | 0           |
| Gigabyte Technology  | F2A88XN-WIFI                                     | 2014 | 13      | 44      | 0           |
| ASUSTek Computer     | M3A                                              | 2008 | 13      | 41      | 0           |
| Intel                | SHARKBAY                                         | 2013 | 13      | 40      | 0           |
| ASRock               | Z370 Pro4                                        | 2017 | 13      | 39      | 0           |
| Lenovo               | IdeaCentre Q180 10087&3110                       | 2011 | 13      | 37      | 0           |
| Hewlett-Packard      | xw4600 Workstation                               | 2008 | 13      | 37      | 0           |
| Gigabyte Technology  | H310M H                                          | 2018 | 13      | 35      | 0           |
| Hewlett-Packard      | Compaq 4000 Pro SFF PC                           | 2011 | 13      | 35      | 0           |
| Hewlett-Packard      | EliteDesk 800 G2 DM 35W                          | 2015 | 13      | 31      | 0           |
| Fujitsu              | CELSIUS_W550                                     | 2015 | 13      | 31      | 0           |
| Gigabyte Technology  | H61M-DS2 DVI                                     | 2012 | 13      | 30      | 0           |
| ASRock               | X570 Extreme4                                    | 2019 | 12      | 60      | 0           |
| Gigabyte Technology  | H55M-S2V                                         | 2010 | 12      | 55      | 0           |
| ASRock               | B550 Phantom Gaming 4                            | 2020 | 12      | 49      | 0           |
| ASUSTek Computer     | Maximus IX HERO                                  | 2016 | 12      | 48      | 0           |
| ASRock               | B550M-ITX/ac                                     | 2020 | 12      | 47      | 0           |
| Gigabyte Technology  | Z370 AORUS Gaming 5                              | 2018 | 12      | 47      | 0           |
| ASRock               | B450M-HDV                                        | 2018 | 12      | 47      | 0           |
| ASUSTek Computer     | TUF B350M-PLUS GAMING                            | 2017 | 12      | 46      | 0           |
| Acer                 | Nitro N50-600                                    | 2018 | 12      | 42      | 0           |
| ASRock               | FM2A58M-VG3+ R2.0                                | 2014 | 12      | 39      | 0           |
| ECS                  | A780GM-A                                         | 2008 | 12      | 38      | 0           |
| Gigabyte Technology  | EG41MF-US2H                                      | 2009 | 12      | 36      | 0           |
| Dell                 | Vostro 220s Series                               | 2008 | 12      | 36      | 0           |
| Dell                 | OptiPlex 9020M                                   | 2015 | 12      | 34      | 0           |
| Intel                | DH77EB AAG39073-304                              | 2012 | 12      | 33      | 0           |
| Hewlett-Packard      | ProDesk 400 G1 SFF                               | 2014 | 12      | 32      | 0           |
| ECS                  | H81H3-M4                                         | 2014 | 12      | 31      | 0           |
| Biostar              | H81MHV3                                          | 2014 | 12      | 31      | 0           |
| Dell                 | Vostro 260                                       | 2011 | 12      | 31      | 0           |
| Acer                 | Aspire M5811                                     | 2009 | 11      | 60      | 0           |
| ASUSTek Computer     | TUF GAMING X570-PLUS_BR                          | 2019 | 11      | 55      | 0           |
| ASRock               | AB350M-HDV                                       | 2017 | 11      | 50      | 0           |
| ASUSTek Computer     | ROG STRIX B450-F GAMING II                       | 2020 | 11      | 47      | 0           |
| Gigabyte Technology  | B460MDS3H                                        | 2020 | 11      | 46      | 0           |
| ASRock               | Z390 Pro4                                        | 2018 | 11      | 46      | 0           |
| Gigabyte Technology  | F2A78M-DS2                                       | 2013 | 11      | 46      | 0           |
| Gigabyte Technology  | B450 AORUS ELITE V2                              | 2020 | 11      | 45      | 0           |
| ASUSTek Computer     | M5A88-M                                          | 2011 | 11      | 45      | 0           |
| Gigabyte Technology  | GA-73PVM-S2H                                     | 2007 | 11      | 45      | 0           |
| Gigabyte Technology  | Z97-D3H                                          | 2014 | 11      | 44      | 0           |
| Gigabyte Technology  | GA-880GM-USB3                                    | 2010 | 11      | 43      | 0           |
| Gigabyte Technology  | F2A75M-D3H                                       | 2012 | 11      | 41      | 0           |
| MSI                  | MS-7B33                                          | 2018 | 11      | 39      | 0           |
| ASRock               | H97 Pro4                                         | 2014 | 11      | 39      | 0           |
| Gigabyte Technology  | GA-A55M-DS2                                      | 2011 | 11      | 39      | 0           |
| Fujitsu Siemens      | ESPRIMO P5730                                    | 2009 | 11      | 38      | 0           |
| Gigabyte Technology  | B150M-D3H-CF                                     | 2015 | 11      | 37      | 0           |
| ASRock               | AM1B-ITX                                         | 2014 | 11      | 37      | 0           |
| ASRock               | G41M-GS3                                         | 2010 | 11      | 37      | 0           |
| ASRock               | 880GM-LE FX                                      | 2006 | 11      | 37      | 0           |
| ASRock               | Z97 Anniversary                                  | 2014 | 11      | 34      | 0           |
| ASRock               | H81M-DGS R2.0                                    | 2013 | 11      | 34      | 0           |
| ASUSTek Computer     | P5G-MX                                           | 2007 | 11      | 33      | 0           |
| MSI                  | MS-7B53                                          | 2018 | 11      | 31      | 0           |
| ASUSTek Computer     | VM40B                                            | 2013 | 11      | 31      | 0           |
| ASRock               | H61M-S                                           | 2011 | 11      | 31      | 0           |
| Medion               | MS-7616                                          | 2009 | 10      | 53      | 0           |
| Intel                | DP55WB AAE64798-206                              | 2009 | 10      | 53      | 0           |
| ASUSTek Computer     | ROG STRIX B450-E GAMING                          | 2019 | 10      | 52      | 0           |
| Gigabyte Technology  | 970A-DS3                                         | 2013 | 10      | 52      | 0           |
| Gigabyte Technology  | GA-MA790FXT-UD5P                                 | 2009 | 10      | 52      | 0           |
| Gigabyte Technology  | G1.Sniper A88X-CF                                | 2013 | 10      | 50      | 0           |
| ASRock               | 960GM/U3S3 FX                                    | 2013 | 10      | 47      | 0           |
| Gigabyte Technology  | Z170-HD3P                                        | 2015 | 10      | 44      | 0           |
| ASUSTek Computer     | Z170M-PLUS                                       | 2015 | 10      | 44      | 0           |
| ASUSTek Computer     | M4A88T-M/USB3                                    | 2010 | 10      | 42      | 0           |
| AMD                  | Inagua CRB                                       | 2011 | 10      | 41      | 0           |
| Gigabyte Technology  | H81M-S2PH                                        | 2013 | 10      | 40      | 0           |
| Hewlett-Packard      | Z220 CMT Workstation                             | 2012 | 10      | 37      | 0           |
| Acer                 | Aspire M3970                                     | 2011 | 10      | 36      | 0           |
| ASRock               | H110M-ITX                                        | 2016 | 10      | 35      | 0           |
| ASRock               | G41C-GS R2.0                                     | 2014 | 10      | 35      | 0           |
| Acer                 | Aspire XC-605                                    | 2013 | 10      | 35      | 0           |
| Fujitsu              | ESPRIMO P400                                     | 2011 | 10      | 35      | 0           |
| ECS                  | A740GM-M                                         | 2008 | 10      | 35      | 0           |
| ASUSTek Computer     | P5B-MX                                           | 2007 | 10      | 35      | 0           |
| ASUSTek Computer     | M2N-X                                            | 2007 | 10      | 35      | 0           |
| MSI                  | MS-7392                                          | 2007 | 10      | 34      | 0           |
| ASUSTek Computer     | H170 PRO GAMING                                  | 2015 | 10      | 33      | 0           |
| Lenovo               | H520 10094                                       | 2012 | 10      | 33      | 0           |
| Gigabyte Technology  | H61M-S2P                                         | 2012 | 10      | 33      | 0           |
| ASRock               | B75 Pro3                                         | 2012 | 10      | 33      | 0           |
| Dell                 | Vostro 270s                                      | 2012 | 10      | 32      | 0           |
| Gigabyte Technology  | H67M-D2-B3                                       | 2011 | 10      | 31      | 0           |
| ASUSTek Computer     | P5KPL/1600                                       | 2008 | 10      | 31      | 0           |
| Gigabyte Technology  | H61M-DS2H                                        | 2012 | 10      | 30      | 0           |
| ASUSTek Computer     | H61M-E                                           | 2012 | 10      | 29      | 0           |
| ASRock               | H61M-HVGS                                        | 2011 | 10      | 29      | 0           |
| Gigabyte Technology  | X79-UP4                                          | 2012 | 9       | 90      | 0           |
| ASUSTek Computer     | P7H55D-M PRO                                     | 2009 | 9       | 63      | 0           |
| Gigabyte Technology  | Z97X-UD5H                                        | 2014 | 9       | 53      | 0           |
| ASRock               | H67DE3                                           | 2011 | 9       | 51      | 0           |
| ASUSTek Computer     | A78M-A                                           | 2013 | 9       | 49      | 0           |
| Gigabyte Technology  | 970A-DS3P FX                                     | 2017 | 9       | 46      | 0           |
| Gigabyte Technology  | Z390 AORUS ELITE                                 | 2019 | 9       | 45      | 0           |
| LattePanda           | Alpha                                            | 2018 | 9       | 44      | 0           |
| Gigabyte Technology  | Z390 I AORUS PRO WIFI                            | 2018 | 9       | 44      | 0           |
| Gigabyte Technology  | GA-870A-USB3                                     | 2011 | 9       | 44      | 0           |
| Gigabyte Technology  | GA-MA78G-DS3H                                    | 2008 | 9       | 43      | 0           |
| ASRock               | FM2A88M-HD+                                      | 2013 | 9       | 42      | 0           |
| Gigabyte Technology  | H55M-S2                                          | 2010 | 9       | 42      | 0           |
| Gigabyte Technology  | EP35-DS3L                                        | 2008 | 9       | 42      | 0           |
| Dell                 | Vostro 3900                                      | 2014 | 9       | 41      | 0           |
| Gigabyte Technology  | Z87X-D3H                                         | 2013 | 9       | 41      | 0           |
| MSI                  | MS-7376                                          | 2007 | 9       | 40      | 0           |
| MSI                  | MS-7715                                          | 2011 | 9       | 39      | 0           |
| Gigabyte Technology  | EP45-UD3P                                        | 2008 | 9       | 39      | 0           |
| Hewlett-Packard      | Compaq Elite 8300 MT                             | 2012 | 9       | 37      | 0           |
| ASUSTek Computer     | F2A85-M                                          | 2012 | 9       | 37      | 0           |
| ASRock               | H87 Pro4                                         | 2013 | 9       | 36      | 0           |
| Fujitsu              | ESPRIMO P700                                     | 2011 | 9       | 36      | 0           |
| Medion               | H81H3-EM2                                        | 2014 | 9       | 34      | 0           |
| ASRock               | ALiveNF6G-VSTA                                   | 2007 | 9       | 34      | 0           |
| Dell                 | OptiPlex 5040                                    | 2016 | 9       | 33      | 0           |
| Gigabyte Technology  | Z77MX-D3H                                        | 2012 | 9       | 33      | 0           |
| Biostar              | G41D3+                                           | 2010 | 9       | 33      | 0           |
| ASUSTek Computer     | P5G41-M LX                                       | 2009 | 9       | 33      | 0           |
| Gigabyte Technology  | H310M S2P 2.0                                    | 2019 | 9       | 31      | 0           |
| ASUSTek Computer     | H110M-CS/BR                                      | 2017 | 9       | 31      | 0           |
| ASRock               | H61M-GS                                          | 2011 | 9       | 31      | 0           |
| Gigabyte Technology  | H110M-H DDR3                                     | 2016 | 9       | 30      | 0           |
| ASRock               | D1800B-ITX                                       | 2014 | 9       | 30      | 0           |
| ECS                  | H61H2-M2                                         | 2011 | 9       | 30      | 0           |
| ASRock               | D1800M                                           | 2014 | 9       | 29      | 0           |
| Intel                | DN2820FYK H24582-201                             | 2013 | 9       | 29      | 0           |
| Gigabyte Technology  | GB-BACE-3160                                     | 2016 | 9       | 28      | 0           |
| Hewlett-Packard      | EliteDesk 800 G2 DM 65W                          | 2015 | 9       | 28      | 0           |
| Intel                | D54250WYK H13922-304                             | 2014 | 9       | 28      | 0           |
| Dell                 | Vostro 230                                       | 2009 | 9       | 28      | 0           |
| ECS                  | 945GCT-M2                                        | 2007 | 9       | 27      | 0           |
| Hewlett-Packard      | Z4 G4 Workstation                                | 2018 | 8       | 89      | 0           |
| ASUSTek Computer     | P6T WS PRO                                       | 2010 | 8       | 76      | 0           |
| Dell                 | Studio XPS 435MT                                 | 2008 | 8       | 70      | 0           |
| ASUSTek Computer     | P7P55D EVO                                       | 2010 | 8       | 61      | 0           |
| ASRock               | Z170 Extreme7+                                   | 2015 | 8       | 58      | 0           |
| Gigabyte Technology  | P55-US3L                                         | 2009 | 8       | 58      | 0           |
| ASRock               | FM2A88X+ Killer                                  | 2013 | 8       | 52      | 0           |
| ASRock               | 970DE3/U3S3                                      | 2012 | 8       | 47      | 0           |
| Gigabyte Technology  | AB350M-D3H                                       | 2017 | 8       | 46      | 0           |
| Medion               | MS-7748                                          | 2011 | 8       | 46      | 0           |
| Intel                | DG33BU AAD79951-407                              | 2007 | 8       | 46      | 0           |
| ASUSTek Computer     | P8Z68 DELUXE                                     | 2011 | 8       | 45      | 0           |
| ASUSTek Computer     | F1A75-V PRO                                      | 2012 | 8       | 44      | 0           |
| Fujitsu              | ESPRIMO P5731                                    | 2010 | 8       | 44      | 0           |
| MSI                  | Cubi N 8GL                                       | 2018 | 8       | 43      | 0           |
| ASUSTek Computer     | P8Z68-V LE                                       | 2011 | 8       | 43      | 0           |
| ASUSTek Computer     | F1A75-M LE                                       | 2011 | 8       | 43      | 0           |
| ASRock               | P67 Extreme6                                     | 2011 | 8       | 43      | 0           |
| ASUSTek Computer     | P5B-PLUS Series                                  | 2007 | 8       | 43      | 0           |
| Gigabyte Technology  | Z370N WIFI                                       | 2017 | 8       | 42      | 0           |
| ASUSTek Computer     | STRIX B250F GAMING                               | 2016 | 8       | 42      | 0           |
| ASRock               | A88M-G                                           | 2016 | 8       | 42      | 0           |
| ASRock               | B365M Pro4                                       | 2019 | 8       | 41      | 0           |
| ASUSTek Computer     | F2A55-M LK2                                      | 2012 | 8       | 41      | 0           |
| Gigabyte Technology  | EP35-DS4                                         | 2008 | 8       | 41      | 0           |
| ASUSTek Computer     | PRIME B250-PLUS                                  | 2016 | 8       | 40      | 0           |
| Medion               | MS-7713                                          | 2010 | 8       | 40      | 0           |
| Acer                 | Aspire X3400                                     | 2010 | 8       | 40      | 0           |
| Gigabyte Technology  | H370AORUSGAMING3WIFI                             | 2018 | 8       | 39      | 0           |
| Dell                 | OptiPlex 580                                     | 2010 | 8       | 39      | 0           |
| ASUSTek Computer     | P5QL                                             | 2008 | 8       | 39      | 0           |
| Acer                 | Aspire M5100                                     | 2007 | 8       | 39      | 0           |
| Gigabyte Technology  | Q87M-D2H                                         | 2013 | 8       | 38      | 0           |
| ASUSTek Computer     | CM6870                                           | 2012 | 8       | 38      | 0           |
| MSI                  | MS-7367                                          | 2007 | 8       | 38      | 0           |
| Gigabyte Technology  | G33M-DS2R                                        | 2007 | 8       | 38      | 0           |
| Gigabyte Technology  | 965P-DS3                                         | 2006 | 8       | 38      | 0           |
| ASUSTek Computer     | M4A785D-M PRO                                    | 2009 | 8       | 37      | 0           |
| ASUSTek Computer     | P5B-VM                                           | 2006 | 8       | 37      | 0           |
| ASUSTek Computer     | Z77-A                                            | 2013 | 8       | 36      | 0           |
| ASUSTek Computer     | V-P8H67E                                         | 2011 | 8       | 36      | 0           |
| Intel                | DG31PR AAD97573-306                              | 2009 | 8       | 36      | 0           |
| Dell                 | OptiPlex 5050                                    | 2016 | 8       | 35      | 0           |
| Medion               | MS-7848                                          | 2013 | 8       | 35      | 0           |
| Intel                | DG41RQ AAE54511-203                              | 2009 | 8       | 35      | 0           |
| ASUSTek Computer     | M2N-X Plus                                       | 2007 | 8       | 35      | 0           |
| ASRock               | H310CM-DVS                                       | 2018 | 8       | 34      | 0           |
| MSI                  | MS-7865                                          | 2014 | 8       | 34      | 0           |
| Gigabyte Technology  | H61N-USB3                                        | 2012 | 8       | 34      | 0           |
| Biostar              | N61PC-M2S                                        | 2009 | 8       | 34      | 0           |
| Gigabyte Technology  | H370M-DS3H                                       | 2018 | 8       | 33      | 0           |
| ASUSTek Computer     | PRIME B250M-K                                    | 2017 | 8       | 33      | 0           |
| ASUSTek Computer     | PRIME Z270M-PLUS                                 | 2016 | 8       | 33      | 0           |
| Lenovo               | IdeaCentre Q190 10115                            | 2012 | 8       | 33      | 0           |
| Dell                 | Vostro 460                                       | 2011 | 8       | 33      | 0           |
| Hewlett-Packard      | ProDesk 600 G3 SFF                               | 2017 | 8       | 32      | 0           |
| ASUSTek Computer     | P8H61-I                                          | 2011 | 8       | 32      | 0           |
| Gigabyte Technology  | GB-BACE-3150                                     | 2015 | 8       | 28      | 0           |
| Intel                | D54250WYK H13922-303                             | 2013 | 8       | 27      | 0           |
| MSI                  | MS-7A94                                          | 2017 | 7       | 79      | 0           |
| ASRock               | X370 Killer SLI                                  | 2017 | 7       | 56      | 0           |
| System76             | Thelio Major                                     | 2019 | 7       | 55      | 0           |
| ASRock               | X470 Master SLI/ac                               | 2018 | 7       | 53      | 0           |
| Gigabyte Technology  | AX370-Gaming K3                                  | 2017 | 7       | 51      | 0           |
| ASRock               | FM2A75 Pro4                                      | 2012 | 7       | 51      | 0           |
| ASUSTek Computer     | TUF GAMING B450-PLUS II                          | 2020 | 7       | 48      | 0           |
| Lenovo               | H535 10117                                       | 2013 | 7       | 48      | 0           |
| MSI                  | MS-7512                                          | 2008 | 7       | 48      | 0           |
| Fujitsu Siemens      | CELSIUS W360                                     | 2008 | 7       | 48      | 0           |
| MSI                  | MS-7891                                          | 2015 | 7       | 47      | 0           |
| Gigabyte Technology  | GA-MA780G-UD3H                                   | 2008 | 7       | 47      | 0           |
| Gigabyte Technology  | GA-A75-D3H                                       | 2011 | 7       | 45      | 0           |
| ASUSTek Computer     | PRIME A520M-K                                    | 2020 | 7       | 44      | 0           |
| Gigabyte Technology  | H270M-DS3H                                       | 2016 | 7       | 42      | 0           |
| ASUSTek Computer     | K30BF_M32BF                                      | 2014 | 7       | 42      | 0           |
| ASRock               | M3A UCC                                          | 2010 | 7       | 42      | 0           |
| ASUSTek Computer     | P5B-V                                            | 2007 | 7       | 42      | 0           |
| ASUSTek Computer     | ROG STRIX Z390-I GAMING                          | 2018 | 7       | 40      | 0           |
| Hewlett-Packard      | Z240 Tower Workstation                           | 2016 | 7       | 39      | 0           |
| Acer                 | Predator G3-710                                  | 2015 | 7       | 39      | 0           |
| ASUSTek Computer     | ROG STRIX B360-I GAMING                          | 2018 | 7       | 38      | 0           |
| MSI                  | MS-7B61                                          | 2017 | 7       | 38      | 0           |
| Gigabyte Technology  | H97N-WIFI                                        | 2014 | 7       | 38      | 0           |
| ASRock               | FM2A78M-HD+                                      | 2013 | 7       | 38      | 0           |
| Gigabyte Technology  | P61-USB3-B3                                      | 2011 | 7       | 38      | 0           |
| ASRock               | 880GMH/USB3                                      | 2010 | 7       | 38      | 0           |
| Foxconn              | RS690M2MA                                        | 2007 | 7       | 38      | 0           |
| Gigabyte Technology  | 965GM-S2                                         | 2006 | 7       | 38      | 0           |
| Gigabyte Technology  | H77N-WIFI                                        | 2012 | 7       | 37      | 0           |
| MSI                  | MS-7732                                          | 2011 | 7       | 37      | 0           |
| Gigabyte Technology  | P61A-D3                                          | 2011 | 7       | 37      | 0           |
| ASUSTek Computer     | M5A88-M EVO                                      | 2011 | 7       | 37      | 0           |
| MSI                  | MS-7125                                          | 2005 | 7       | 37      | 0           |
| Dell                 | Precision Tower 3420                             | 2016 | 7       | 36      | 0           |
| ASRock               | Z68 Pro3 Gen3                                    | 2011 | 7       | 36      | 0           |
| ASRock               | H170M Pro4                                       | 2015 | 7       | 35      | 0           |
| ASRock               | H87M Pro4                                        | 2013 | 7       | 35      | 0           |
| Gigabyte Technology  | HA65M-D2H-B3                                     | 2011 | 7       | 35      | 0           |
| Nvidia               | NF-MCP61                                         | 2007 | 7       | 35      | 0           |
| Gigabyte Technology  | EG41MFT-US2H                                     | 2010 | 7       | 34      | 0           |
| ASUSTek Computer     | P5LD2-X                                          | 2007 | 7       | 34      | 0           |
| ASRock               | H310CM-HDV                                       | 2019 | 7       | 33      | 0           |
| Foxconn              | H61MXT1/F2/-S/-V                                 | 2011 | 7       | 33      | 0           |
| Acer                 | Aspire XC-780                                    | 2016 | 7       | 32      | 0           |
| Positivo             | POS-ECIG41BS                                     | 2010 | 7       | 32      | 0           |
| ASUSTek Computer     | P5KPL-AM-CKD-VISUM-SI                            | 2009 | 7       | 32      | 0           |
| ASUSTek Computer     | P5KPL-C                                          | 2007 | 7       | 32      | 0           |
| PC Engines           | APU2                                             | 2018 | 7       | 31      | 0           |
| Hewlett-Packard      | EliteDesk 800 G2 TWR                             | 2016 | 7       | 31      | 0           |
| ASUSTek Computer     | H110M-PLUS                                       | 2016 | 7       | 31      | 0           |
| Gigabyte Technology  | Z68M-D2H                                         | 2012 | 7       | 31      | 0           |
| ASRock               | J5005-ITX                                        | 2018 | 7       | 30      | 0           |
| ASRock               | H110M-STX                                        | 2016 | 7       | 30      | 0           |
| Dell                 | Inspiron One 2020                                | 2012 | 7       | 30      | 0           |
| MSI                  | MS-7528                                          | 2008 | 7       | 30      | 0           |
| Intel                | DG31PR AAD97573-206                              | 2008 | 7       | 30      | 0           |
| Hewlett-Packard      | Compaq dc5700 Small Form Factor                  | 2006 | 7       | 30      | 0           |
| Pegatron             | SAISHIAT2                                        | 2012 | 7       | 29      | 0           |
| Intel                | DH67BL AAG10189-208                              | 2011 | 7       | 29      | 0           |
| Fujitsu              | ESPRIMO P900                                     | 2011 | 7       | 29      | 0           |
| ASRock               | H310M-HDV                                        | 2018 | 7       | 28      | 0           |
| ASUSTek Computer     | P8H61-I LX R2.0                                  | 2012 | 7       | 28      | 0           |
| ASRock               | J3455B-ITX                                       | 2016 | 7       | 25      | 0           |
| ASUSTek Computer     | Z8NA-D6                                          | 2010 | 6       | 89      | 0           |
| Intel                | X64                                              | 2020 | 6       | 85      | 0           |
| ASUSTek Computer     | Rampage III GENE                                 | 2010 | 6       | 69      | 0           |
| ASRock               | X470 Gaming K4                                   | 2018 | 6       | 63      | 0           |
| Gigabyte Technology  | TRX40 AORUS XTREME                               | 2020 | 6       | 62      | 0           |
| MSI                  | MS-7B77                                          | 2018 | 6       | 61      | 0           |
| MSI                  | MS-7C36                                          | 2020 | 6       | 59      | 0           |
| Dell                 | Inspiron 5676                                    | 2018 | 6       | 59      | 0           |
| Gigabyte Technology  | H55-UD3H                                         | 2010 | 6       | 58      | 0           |
| Gigabyte Technology  | P55-UD3                                          | 2009 | 6       | 58      | 0           |
| ASRock               | X570 Steel Legend WiFi ax                        | 2019 | 6       | 57      | 0           |
| Gigabyte Technology  | X399 AORUS XTREME                                | 2018 | 6       | 57      | 0           |
| Packard Bell         | ixtreme M5740                                    | 2009 | 6       | 57      | 0           |
| ASRock               | X399M Taichi                                     | 2018 | 6       | 55      | 0           |
| Intel                | DP55WB AAE64798-205                              | 2009 | 6       | 55      | 0           |
| Gigabyte Technology  | Z370 HD3P                                        | 2017 | 6       | 53      | 0           |
| ASUSTek Computer     | ROG CROSSHAIR VI HERO                            | 2017 | 6       | 53      | 0           |
| ASRock               | FM2A85X Extreme6                                 | 2012 | 6       | 53      | 0           |
| ASRock               | H55M/USB3                                        | 2010 | 6       | 53      | 0           |
| Gigabyte Technology  | GA-890FXA-UD5                                    | 2010 | 6       | 51      | 0           |
| ASUSTek Computer     | M4A78-E                                          | 2011 | 6       | 48      | 0           |
| ASUSTek Computer     | PRIME Z270-AR                                    | 2018 | 6       | 47      | 0           |
| Gigabyte Technology  | Z87M-D3H                                         | 2013 | 6       | 47      | 0           |
| Intel                | DP55WB AAE64798-207                              | 2010 | 6       | 47      | 0           |
| MSI                  | MS-7A67                                          | 2017 | 6       | 46      | 0           |
| MSI                  | MS-7699                                          | 2012 | 6       | 46      | 0           |
| Gigabyte Technology  | Z68X-UD3-B3                                      | 2011 | 6       | 46      | 0           |
| ASRock               | Z68 Extreme3 Gen3                                | 2012 | 6       | 45      | 0           |
| Alienware            | Aurora R7                                        | 2018 | 6       | 44      | 0           |
| ASUSTek Computer     | M5A97 EVO                                        | 2011 | 6       | 44      | 0           |
| MSI                  | MS-7A12                                          | 2015 | 6       | 43      | 0           |
| ASUSTek Computer     | P8Z77-V LE                                       | 2012 | 6       | 43      | 0           |
| Fujitsu Siemens      | MS-7379VP                                        | 2008 | 6       | 43      | 0           |
| Acer                 | Aspire TC-710                                    | 2015 | 6       | 42      | 0           |
| Acer                 | Aspire TC-705                                    | 2014 | 6       | 42      | 0           |
| ASUSTek Computer     | P8H67-M EVO                                      | 2011 | 6       | 42      | 0           |
| ASUSTek Computer     | F1A55-M LE                                       | 2011 | 6       | 42      | 0           |
| ASRock               | P5B-DE                                           | 2009 | 6       | 42      | 0           |
| Gigabyte Technology  | P35-S3                                           | 2007 | 6       | 42      | 0           |
| ASRock               | 880GMH/U3S3                                      | 2011 | 6       | 41      | 0           |
| Hewlett-Packard      | Pro 3010 Microtower PC                           | 2009 | 6       | 41      | 0           |
| Gigabyte Technology  | GA-MA790X-UD4                                    | 2009 | 6       | 41      | 0           |
| MSI                  | MS-7366                                          | 2007 | 6       | 41      | 0           |
| ASUSTek Computer     | M2NPV-MX                                         | 2006 | 6       | 41      | 0           |
| Gigabyte Technology  | B365M D3H                                        | 2019 | 6       | 40      | 0           |
| Gigabyte Technology  | H370HD3                                          | 2018 | 6       | 40      | 0           |
| ASUSTek Computer     | ROG STRIX Z370-I GAMING                          | 2018 | 6       | 40      | 0           |
| Gigabyte Technology  | Z270-Gaming K3                                   | 2017 | 6       | 40      | 0           |
| ASRock               | Z270 Killer SLI/ac                               | 2016 | 6       | 40      | 0           |
| MSI                  | MS-7919                                          | 2014 | 6       | 40      | 0           |
| Fujitsu              | ESPRIMO P7936                                    | 2010 | 6       | 40      | 0           |
| MSI                  | MS-7576                                          | 2009 | 6       | 40      | 0           |
| Dell                 | Vostro 3470                                      | 2018 | 6       | 39      | 0           |
| ASRock               | Z370M-ITX/ac                                     | 2017 | 6       | 39      | 0           |
| Gigabyte Technology  | Z270X-Gaming 5                                   | 2016 | 6       | 39      | 0           |
| Gigabyte Technology  | H110M-DS2                                        | 2016 | 6       | 39      | 0           |
| Gigabyte Technology  | B75M-D3P                                         | 2013 | 6       | 39      | 0           |
| Gigabyte Technology  | F2A75M-HD2                                       | 2012 | 6       | 39      | 0           |
| Gigabyte Technology  | GA-A55M-S2V                                      | 2011 | 6       | 39      | 0           |
| Gigabyte Technology  | GA-MA74GMT-S2                                    | 2010 | 6       | 39      | 0           |
| Gigabyte Technology  | EP45-UD3                                         | 2009 | 6       | 39      | 0           |
| Positivo             | POS-PIQ77CL                                      | 2013 | 6       | 38      | 0           |
| Gigabyte Technology  | GA-770T-D3L                                      | 2010 | 6       | 38      | 0           |
| Hewlett-Packard      | Pro 3010 Small Form Factor PC                    | 2009 | 6       | 38      | 0           |
| ASUSTek Computer     | M2N68 Plus                                       | 2009 | 6       | 38      | 0           |
| Fujitsu Siemens      | ESPRIMO C5730                                    | 2008 | 6       | 38      | 0           |
| Dell                 | Vostro 410                                       | 2008 | 6       | 38      | 0           |
| Fujitsu Siemens      | M2R-FVM                                          | 2006 | 6       | 38      | 0           |
| ASRock               | ConRoeXFire-eSATA2                               | 2006 | 6       | 38      | 0           |
| MSI                  | MS-7025                                          | 2005 | 6       | 38      | 0           |
| ASUSTek Computer     | P8H61 PRO                                        | 2011 | 6       | 37      | 0           |
| ASUSTek Computer     | B202                                             | 2008 | 6       | 37      | 0           |
| Hewlett-Packard      | Compaq dc7700p Convertible Minitower             | 2006 | 6       | 37      | 0           |
| ASRock               | H81M-HG4                                         | 2015 | 6       | 36      | 0           |
| Lenovo               | 70A4000HUX ThinkServer TS140                     | 2013 | 6       | 36      | 0           |
| Hewlett-Packard      | Compaq Elite 8300 Touch All-in-One PC            | 2012 | 6       | 36      | 0           |
| ASUSTek Computer     | F2A85-V                                          | 2012 | 6       | 36      | 0           |
| ASRock               | B75M R2.0                                        | 2012 | 6       | 36      | 0           |
| Gigabyte Technology  | Z68A-D3H-B3                                      | 2011 | 6       | 36      | 0           |
| Fujitsu              | ESPRIMO E7935                                    | 2010 | 6       | 36      | 0           |
| Gigabyte Technology  | GA-MA78LM-S2                                     | 2009 | 6       | 36      | 0           |
| Gigabyte Technology  | GA-MA770-US3                                     | 2009 | 6       | 36      | 0           |
| Intel                | DG31PR AAD97573-302                              | 2008 | 6       | 36      | 0           |
| Hewlett-Packard      | Compaq dc7800p Convertible Minitower             | 2007 | 6       | 36      | 0           |
| ASUSTek Computer     | P5LD2-VM SE                                      | 2006 | 6       | 36      | 0           |
| Minix                | Z83-4                                            | 2016 | 6       | 35      | 0           |
| Alienware            | ASM100                                           | 2014 | 6       | 35      | 0           |
| Dell                 | Vostro 270                                       | 2012 | 6       | 35      | 0           |
| ASUSTek Computer     | P8B WS                                           | 2012 | 6       | 35      | 0           |
| Biostar              | A880GZ                                           | 2011 | 6       | 35      | 0           |
| Packard Bell         | imedia S1300                                     | 2010 | 6       | 35      | 0           |
| Dell                 | Inspiron 410                                     | 2010 | 6       | 35      | 0           |
| ASRock               | 775i945GZ                                        | 2006 | 6       | 35      | 0           |
| Hewlett-Packard      | xw4300 Workstation                               | 2005 | 6       | 35      | 0           |
| ASUSTek Computer     | TUF GAMING B460M-PLUS                            | 2020 | 6       | 34      | 0           |
| ASUSTek Computer     | P8H61-M LX PLUS                                  | 2011 | 6       | 34      | 0           |
| Hewlett-Packard      | p6610f                                           | 2010 | 6       | 34      | 0           |
| Gigabyte Technology  | M52LT-D3                                         | 2010 | 6       | 34      | 0           |
| WinFast              | MCP61M2MA                                        | 2007 | 6       | 34      | 0           |
| ASUSTek Computer     | A8N5X                                            | 2005 | 6       | 34      | 0           |
| MSI                  | MS-7C89                                          | 2020 | 6       | 33      | 0           |
| Dell                 | Vostro 3681                                      | 2020 | 6       | 33      | 0           |
| Gigabyte Technology  | H110M-S2PV                                       | 2016 | 6       | 33      | 0           |
| ASUSTek Computer     | P8H77-I                                          | 2013 | 6       | 33      | 0           |
| Gigabyte Technology  | G41MT-D3V                                        | 2011 | 6       | 33      | 0           |
| ASUSTek Computer     | P5G41-M                                          | 2009 | 6       | 33      | 0           |
| Gigabyte Technology  | GA-K8NMF-9                                       | 2005 | 6       | 33      | 0           |
| ASUSTek Computer     | P5GD1 PRO                                        | 2005 | 6       | 33      | 0           |
| Gigabyte Technology  | H310M S2V 2.0                                    | 2018 | 6       | 32      | 0           |
| Gigabyte Technology  | Z77N-WIFI                                        | 2012 | 6       | 32      | 0           |
| Gigabyte Technology  | B75N                                             | 2012 | 6       | 32      | 0           |
| Intel                | DP67BA AAG10219-303                              | 2011 | 6       | 32      | 0           |
| ASRock               | N68C-GS UCC                                      | 2010 | 6       | 32      | 0           |
| ASRock               | B365M Phantom Gaming 4                           | 2019 | 6       | 31      | 0           |
| Biostar              | TB250-BTC                                        | 2017 | 6       | 31      | 0           |
| PCWare               | IPMH110G                                         | 2016 | 6       | 31      | 0           |
| Fujitsu              | ESPRIMO E900                                     | 2011 | 6       | 31      | 0           |
| Acer                 | Aspire M3920                                     | 2010 | 6       | 31      | 0           |
| Hewlett-Packard      | EliteDesk 800 G4 SFF                             | 2018 | 6       | 30      | 0           |
| Gigabyte Technology  | J3455N-D3H                                       | 2017 | 6       | 30      | 0           |
| Acer                 | Veriton X2631G                                   | 2014 | 6       | 30      | 0           |
| Hewlett-Packard      | ProDesk 400 G1 MT                                | 2013 | 6       | 30      | 0           |
| ASUSTek Computer     | H61M-C                                           | 2013 | 6       | 30      | 0           |
| Acer                 | Aspire R3610                                     | 2009 | 6       | 30      | 0           |
| Gigabyte Technology  | 8I945GMF                                         | 2005 | 6       | 30      | 0           |
| MSI                  | MS-7C13                                          | 2019 | 6       | 29      | 0           |
| Gigabyte Technology  | H61M-DS2 x.x                                     | 2012 | 6       | 29      | 0           |
| ASUSTek Computer     | H110M-C/BR                                       | 2017 | 6       | 28      | 0           |
| ECS                  | H61H2-M6                                         | 2012 | 6       | 27      | 0           |
| Huanan               | X79 VAA1                                         | 2018 | 5       | 99      | 0           |
| MSI                  | MS-7A90                                          | 2019 | 5       | 87      | 0           |
| Gigabyte Technology  | X58A-UD7                                         | 2010 | 5       | 79      | 0           |
| MSI                  | MS-7A31                                          | 2017 | 5       | 76      | 0           |
| ASUSTek Computer     | Rampage II GENE                                  | 2009 | 5       | 71      | 0           |
| ASRock               | P55 Pro                                          | 2009 | 5       | 57      | 0           |
| MSI                  | MS-7A78                                          | 2016 | 5       | 54      | 0           |
| Gigabyte Technology  | X399 AORUS PRO                                   | 2018 | 5       | 53      | 0           |
| Gigabyte Technology  | Z97X-UD5H-BK                                     | 2014 | 5       | 52      | 0           |
| Hewlett-Packard      | Elite 7100 Microtower PC                         | 2010 | 5       | 52      | 0           |
| ASRock               | B550 Taichi                                      | 2020 | 5       | 51      | 0           |
| ASRock               | X570 Phantom Gaming 4S                           | 2019 | 5       | 50      | 0           |
| MSI                  | MS-7B00                                          | 2017 | 5       | 50      | 0           |
| Intel                | DG965WH AAD41692-304                             | 2006 | 5       | 50      | 0           |
| ASUSTek Computer     | ROG STRIX B550-A GAMING                          | 2020 | 5       | 49      | 0           |
| Biostar              | X370GT5                                          | 2018 | 5       | 49      | 0           |
| MSI                  | MS-7B36                                          | 2017 | 5       | 48      | 0           |
| Biostar              | B350GT3                                          | 2017 | 5       | 48      | 0           |
| ASRock               | B550M-HDV                                        | 2020 | 5       | 47      | 0           |
| Gigabyte Technology  | Z390 D                                           | 2019 | 5       | 47      | 0           |
| Gigabyte Technology  | AX370M-DS3H                                      | 2018 | 5       | 47      | 0           |
| ASRock               | Z370 Extreme4                                    | 2017 | 5       | 47      | 0           |
| Gigabyte Technology  | G1.Sniper 3                                      | 2012 | 5       | 47      | 0           |
| MSI                  | MS-7696                                          | 2011 | 5       | 47      | 0           |
| ASUSTek Computer     | F1A75                                            | 2011 | 5       | 47      | 0           |
| ASRock               | 870 Extreme3                                     | 2010 | 5       | 47      | 0           |
| Gigabyte Technology  | Z370M DS3H                                       | 2018 | 5       | 45      | 0           |
| ASUSTek Computer     | F1A55-M LX                                       | 2012 | 5       | 45      | 0           |
| Gigabyte Technology  | GA-890XA-UD3                                     | 2010 | 5       | 45      | 0           |
| ASUSTek Computer     | M2V                                              | 2006 | 5       | 45      | 0           |
| ASUSTek Computer     | A8N-VM CSM                                       | 2005 | 5       | 45      | 0           |
| Gigabyte Technology  | EP35C-DS3R                                       | 2008 | 5       | 44      | 0           |
| MSI                  | MS-7B58                                          | 2017 | 5       | 43      | 0           |
| Biostar              | A780L3L                                          | 2010 | 5       | 43      | 0           |
| Acer                 | Aspire X1700                                     | 2008 | 5       | 43      | 0           |
| Gigabyte Technology  | Z68XP-UD3P                                       | 2011 | 5       | 42      | 0           |
| ASRock               | H310M-STX                                        | 2019 | 5       | 41      | 0           |
| Hewlett-Packard      | ProLiant ML310e Gen8 v2                          | 2013 | 5       | 41      | 0           |
| Gigabyte Technology  | GA-A75M-S2V                                      | 2011 | 5       | 41      | 0           |
| ASUSTek Computer     | Maximus Extreme                                  | 2009 | 5       | 41      | 0           |
| Gigabyte Technology  | EP45C-DS3R                                       | 2008 | 5       | 41      | 0           |
| Gigabyte Technology  | EP45C-DS3                                        | 2008 | 5       | 41      | 0           |
| ASUSTek Computer     | P5K3 Deluxe                                      | 2007 | 5       | 41      | 0           |
| Gigabyte Technology  | B460MD3H                                         | 2020 | 5       | 40      | 0           |
| ASUSTek Computer     | TUF Z370-PRO GAMING                              | 2017 | 5       | 40      | 0           |
| ASRock               | FM2A88X-ITX+                                     | 2014 | 5       | 40      | 0           |
| Hewlett-Packard      | G5320fr                                          | 2010 | 5       | 40      | 0           |
| Gigabyte Technology  | EP45T-UD3LR                                      | 2010 | 5       | 40      | 0           |
| Packard Bell         | imedia S3720                                     | 2009 | 5       | 40      | 0           |
| Fujitsu Siemens      | ESPRIMO P7935                                    | 2008 | 5       | 40      | 0           |
| ASUSTek Computer     | M3N78-EH                                         | 2008 | 5       | 40      | 0           |
| MSI                  | MS-7C67                                          | 2019 | 5       | 39      | 0           |
| ASUSTek Computer     | Z170-E                                           | 2018 | 5       | 39      | 0           |
| ASUSTek Computer     | STRIX Z270I GAMING                               | 2017 | 5       | 39      | 0           |
| ASRock               | H77 Pro4-M                                       | 2012 | 5       | 39      | 0           |
| ASUSTek Computer     | P8Z68-M PRO                                      | 2011 | 5       | 39      | 0           |
| ASRock               | H67M                                             | 2011 | 5       | 39      | 0           |
| Gigabyte Technology  | GA-770T-USB3                                     | 2010 | 5       | 39      | 0           |
| Foxconn              | A76ML-K 30                                       | 2010 | 5       | 39      | 0           |
| ASUSTek Computer     | M4N68T-M-V2                                      | 2010 | 5       | 39      | 0           |
| Acer                 | Aspire X3300                                     | 2009 | 5       | 39      | 0           |
| Gigabyte Technology  | GA-MA790GP-DS4H                                  | 2008 | 5       | 39      | 0           |
| Intel                | DP35DP AAD81073-207                              | 2007 | 5       | 39      | 0           |
| Gigabyte Technology  | P35-DS3P                                         | 2007 | 5       | 39      | 0           |
| MSI                  | MS-7238                                          | 2006 | 5       | 39      | 0           |
| Gigabyte Technology  | B460MDS3HV2                                      | 2020 | 5       | 38      | 0           |
| ASUSTek Computer     | ROG STRIX Z490-G GAMING                          | 2020 | 5       | 38      | 0           |
| ASUSTek Computer     | PRIME H370-PLUS                                  | 2019 | 5       | 38      | 0           |
| Acer                 | Aspire TC-105                                    | 2013 | 5       | 38      | 0           |
| Gigabyte Technology  | P43T-ES3G                                        | 2009 | 5       | 38      | 0           |
| ASUSTek Computer     | M4A785T-M                                        | 2009 | 5       | 38      | 0           |
| ASUSTek Computer     | P5B-E Plus                                       | 2006 | 5       | 38      | 0           |
| Acer                 | Veriton EX2620G                                  | 2018 | 5       | 37      | 0           |
| Gigabyte Technology  | H170M-DS3H                                       | 2017 | 5       | 37      | 0           |
| Fujitsu              | CELSIUS W530                                     | 2015 | 5       | 37      | 0           |
| ASRock               | FM2A85X-ITX                                      | 2013 | 5       | 37      | 0           |
| Acer                 | Aspire M5800/M3800                               | 2009 | 5       | 37      | 0           |
| ASUSTek Computer     | M3A76-CM                                         | 2009 | 5       | 37      | 0           |
| ASUSTek Computer     | P5B-Premium                                      | 2008 | 5       | 37      | 0           |
| Intel                | D945GNT AAC96315-405                             | 2006 | 5       | 37      | 0           |
| ASUSTek Computer     | PRIME Z490M-PLUS                                 | 2020 | 5       | 36      | 0           |
| ASUSTek Computer     | K31CD-K                                          | 2017 | 5       | 36      | 0           |
| ASRock               | FM2A55M-HD+ R2.0                                 | 2016 | 5       | 36      | 0           |
| Gigabyte Technology  | Z97M-D3H                                         | 2014 | 5       | 36      | 0           |
| ASRock               | FM2A55M-VG3+                                     | 2013 | 5       | 36      | 0           |
| Gateway              | DX4870                                           | 2012 | 5       | 36      | 0           |
| Biostar              | A960D+                                           | 2012 | 5       | 36      | 0           |
| Acer                 | Aspire X1430                                     | 2011 | 5       | 36      | 0           |
| ASUSTek Computer     | P8H61-M PRO                                      | 2011 | 5       | 36      | 0           |
| Hewlett-Packard      | Pro 3130 Microtower PC                           | 2010 | 5       | 36      | 0           |
| Gateway              | DT55                                             | 2010 | 5       | 36      | 0           |
| ASRock               | N68-GE3 UCC                                      | 2010 | 5       | 36      | 0           |
| MSI                  | MS-7514                                          | 2009 | 5       | 36      | 0           |
| ASUSTek Computer     | M2N-VM DVI                                       | 2007 | 5       | 36      | 0           |
| ASUSTek Computer     | B150M PRO GAMING                                 | 2016 | 5       | 35      | 0           |
| Intel                | DB75EN AAG39650-302                              | 2012 | 5       | 35      | 0           |
| Gigabyte Technology  | H61MA-D2V                                        | 2012 | 5       | 35      | 0           |
| Acer                 | Veriton S2610G                                   | 2011 | 5       | 35      | 0           |
| Gigabyte Technology  | G33M-S2L                                         | 2007 | 5       | 35      | 0           |
| CyberPowerPC         | C Series                                         | 2018 | 5       | 34      | 0           |
| Lenovo               | H530 10130                                       | 2013 | 5       | 34      | 0           |
| Intel                | DH61WW AAG23116-302                              | 2012 | 5       | 34      | 0           |
| Gigabyte Technology  | H67MA-UD2H-B3                                    | 2011 | 5       | 34      | 0           |
| Gigabyte Technology  | H61M-USB3-B3                                     | 2011 | 5       | 34      | 0           |
| Gigabyte Technology  | G41M-ES2H                                        | 2009 | 5       | 34      | 0           |
| ASUSTek Computer     | M2A74-AM                                         | 2009 | 5       | 34      | 0           |
| Dell                 | Vostro 400                                       | 2007 | 5       | 34      | 0           |
| Hewlett-Packard      | ProDesk 600 G4 SFF                               | 2018 | 5       | 33      | 0           |
| ASRock               | J4105M                                           | 2018 | 5       | 33      | 0           |
| ASRock               | H81M-DG4                                         | 2015 | 5       | 33      | 0           |
| Gigabyte Technology  | M4HM87P-00                                       | 2014 | 5       | 33      | 0           |
| Fujitsu              | ESPRIMO Q520                                     | 2014 | 5       | 33      | 0           |
| Intel                | DH87RL AAG74240-403                              | 2013 | 5       | 33      | 0           |
| Hewlett-Packard      | Elite 7500 Series MT                             | 2012 | 5       | 33      | 0           |
| MSI                  | MS-7740                                          | 2011 | 5       | 33      | 0           |
| Acer                 | Aspire X3910                                     | 2010 | 5       | 33      | 0           |
| ASRock               | N68C-S                                           | 2010 | 5       | 33      | 0           |
| MSI                  | CML-U PRO Cubi 5                                 | 2019 | 5       | 32      | 0           |
| ASRock               | B365 Pro4                                        | 2019 | 5       | 32      | 0           |
| ASUSTek Computer     | PRIME H310-PLUS                                  | 2018 | 5       | 32      | 0           |
| ASRock               | B150M Pro4S/D3                                   | 2015 | 5       | 32      | 0           |
| Gigabyte Technology  | H61M-USB3V                                       | 2013 | 5       | 32      | 0           |
| ASRock               | Z87E-ITX                                         | 2013 | 5       | 32      | 0           |
| Acer                 | Veriton M4620G                                   | 2012 | 5       | 32      | 0           |
| ASRock               | B75M-GL R2.0                                     | 2012 | 5       | 32      | 0           |
| Positivo             | POS-MIH61CF                                      | 2011 | 5       | 32      | 0           |
| Lenovo               | XXXX 2222222                                     | 2008 | 5       | 32      | 0           |
| Nvidia               | NF-MCP68                                         | 2007 | 5       | 32      | 0           |
| ECS                  | NFORCE6M-A                                       | 2007 | 5       | 32      | 0           |
| Fujitsu Siemens      | SCENIC P / SCENICO P                             | 2003 | 5       | 32      | 0           |
| Hewlett-Packard      | ProDesk 600 G2 DM                                | 2016 | 5       | 31      | 0           |
| Shuttle              | DS61                                             | 2013 | 5       | 31      | 0           |
| ASUSTek Computer     | P5P41D                                           | 2009 | 5       | 31      | 0           |
| ECS                  | nForce                                           | 2005 | 5       | 31      | 0           |
| Gigabyte Technology  | H310N 2.0                                        | 2019 | 5       | 30      | 0           |
| ASUSTek Computer     | PRIME B365M-K                                    | 2019 | 5       | 30      | 0           |
| ASRock               | H81M-HG4 R4.0                                    | 2019 | 5       | 30      | 0           |
| ASRock               | H110M-ITX/ac                                     | 2016 | 5       | 30      | 0           |
| ASRock               | H81M-VG4                                         | 2013 | 5       | 30      | 0           |
| Fujitsu              | ESPRIMO C710                                     | 2012 | 5       | 30      | 0           |
| Biostar              | B75MU3B                                          | 2012 | 5       | 30      | 0           |
| Dell                 | Vostro 260s                                      | 2011 | 5       | 30      | 0           |
| Dell                 | Inspiron 620s                                    | 2011 | 5       | 30      | 0           |
| Hewlett-Packard      | 100eu                                            | 2010 | 5       | 30      | 0           |
| ASUSTek Computer     | M2N-CM DVI                                       | 2008 | 5       | 30      | 0           |
| Hewlett-Packard      | Compaq dc7700 Ultra-slim Desktop                 | 2007 | 5       | 30      | 0           |
| Gigabyte Technology  | 8I945PLGE-RH                                     | 2006 | 5       | 30      | 0           |
| DEPO Computers       | DPH110M                                          | 2017 | 5       | 29      | 0           |
| ASRock               | H310CM-HG4                                       | 2019 | 5       | 28      | 0           |
| Hewlett-Packard      | EliteDesk 800 G3 TWR                             | 2018 | 5       | 28      | 0           |
| Gigabyte Technology  | H110N                                            | 2016 | 5       | 28      | 0           |
| ASRock               | H110M-DVS R2.0                                   | 2016 | 5       | 28      | 0           |
| Shuttle              | XS35V4                                           | 2014 | 5       | 28      | 0           |
| ECS                  | H61H2-CM                                         | 2011 | 5       | 28      | 0           |
| Intel                | DG41WV                                           | 2010 | 5       | 28      | 0           |
| Information Compu... | PrimePC Solo30                                   | 2009 | 5       | 28      | 0           |
| PCWare               | IPX1800E1                                        | 2016 | 5       | 27      | 0           |
| PCWare               | IPX1800G2                                        | 2014 | 5       | 26      | 0           |
| Fujitsu              | ESPRIMO E710                                     | 2012 | 5       | 26      | 0           |
| Intel                | DH61WW AAG23116-203                              | 2011 | 5       | 26      | 0           |
| Acer                 | Veriton L4610G                                   | 2011 | 5       | 26      | 0           |
| Biostar              | H61MLV2                                          | 2012 | 5       | 25      | 0           |
| Megaware             | MW-H61H2-M2                                      | 2011 | 5       | 25      | 0           |
| Intel                | HM65DESK                                         | 2019 | 5       | 23      | 0           |
| MSI                  | MS-7B05                                          | 2017 | 4       | 92      | 0           |
| ASUSTek Computer     | Rampage IV FORMULA                               | 2014 | 4       | 84      | 0           |
| Fujitsu              | CELSIUS R570-2                                   | 2010 | 4       | 84      | 0           |
| Huanan               | X79 V6.11                                        | 2018 | 4       | 83      | 0           |
| Dell                 | Precision T5610                                  | 2014 | 4       | 81      | 0           |
| Intel                | X79 V2.72A                                       | 2018 | 4       | 73      | 0           |
| Fujitsu              | CELSIUS R670-2                                   | 2010 | 4       | 71      | 0           |
| Alienware            | Aurora                                           | 2009 | 4       | 71      | 0           |
| Gigabyte Technology  | P55A-UD3R                                        | 2009 | 4       | 67      | 0           |
| Gigabyte Technology  | X58-USB3                                         | 2010 | 4       | 66      | 0           |
| ASUSTek Computer     | P6T6 WS REVOLUTION                               | 2009 | 4       | 66      | 0           |
| ASRock               | B550 Extreme4                                    | 2020 | 4       | 57      | 0           |
| ASUSTek Computer     | ROG Maximus XI FORMULA                           | 2018 | 4       | 57      | 0           |
| ASRock               | P55 Pro/USB3                                     | 2010 | 4       | 57      | 0           |
| Gateway              | DX4831                                           | 2009 | 4       | 57      | 0           |
| Hewlett-Packard      | EliteDesk 705 G4 DM 35W                          | 2018 | 4       | 55      | 0           |
| Gigabyte Technology  | F2A85XM-HD3                                      | 2012 | 4       | 55      | 0           |
| Gigabyte Technology  | GA-790FXTA-UD5                                   | 2009 | 4       | 55      | 0           |
| Fujitsu              | ESPRIMO P1510                                    | 2010 | 4       | 54      | 0           |
| Hewlett-Packard      | ProLiant ML350 G5                                | 2007 | 4       | 54      | 0           |
| MSI                  | MS-7589                                          | 2009 | 4       | 52      | 0           |
| MSI                  | MS-7583                                          | 2009 | 4       | 52      | 0           |
| Gigabyte Technology  | B550 GAMING X V2                                 | 2020 | 4       | 51      | 0           |
| Intel                | DQ57TM AAE70931-404                              | 2010 | 4       | 51      | 0           |
| ECS                  | IC55H-A                                          | 2010 | 4       | 51      | 0           |
| MSI                  | MS-7B90                                          | 2018 | 4       | 50      | 0           |
| Gigabyte Technology  | Z170N-WIFI                                       | 2018 | 4       | 50      | 0           |
| ASUSTek Computer     | PRIME A320M-C R2.0                               | 2017 | 4       | 50      | 0           |
| ASRock               | X370 Gaming-ITX/ac                               | 2017 | 4       | 50      | 0           |
| Gigabyte Technology  | F2A88X-D3HP                                      | 2015 | 4       | 50      | 0           |
| Lenovo               | ThinkStation C20 4263BA7                         | 2011 | 4       | 50      | 0           |
| Intel                | DP55WG AAE57269-404                              | 2009 | 4       | 50      | 0           |
| EVGA                 | 132-CK-NF79                                      | 2008 | 4       | 50      | 0           |
| XFX                  | Nforce 680i LT                                   | 2007 | 4       | 50      | 0           |
| Gigabyte Technology  | B550 AORUS PRO V2                                | 2020 | 4       | 49      | 0           |
| ASUSTek Computer     | M2NBP-VM CSM                                     | 2007 | 4       | 49      | 0           |
| Biostar              | B450MH                                           | 2018 | 4       | 48      | 0           |
| ASUSTek Computer     | F1A75-M PRO                                      | 2011 | 4       | 48      | 0           |
| Hewlett-Packard      | xw8400 Workstation                               | 2007 | 4       | 48      | 0           |
| Gigabyte Technology  | GA-MA790FX-DS5                                   | 2007 | 4       | 48      | 0           |
| ASRock               | B550 PG Velocita                                 | 2020 | 4       | 47      | 0           |
| Gigabyte Technology  | Z170X-UD5 TH                                     | 2018 | 4       | 47      | 0           |
| ASUSTek Computer     | ROG Maximus X CODE                               | 2017 | 4       | 47      | 0           |
| Medion               | MS-7708                                          | 2010 | 4       | 47      | 0           |
| Biostar              | B450GT3                                          | 2019 | 4       | 46      | 0           |
| ASRock               | H270M-ITX/ac                                     | 2016 | 4       | 46      | 0           |
| Gigabyte Technology  | Z87X-UD7 TH                                      | 2013 | 4       | 46      | 0           |
| ASUSTek Computer     | TUF GAMING B450M-PLUS II                         | 2020 | 4       | 45      | 0           |
| Gigabyte Technology  | C246-WU4                                         | 2019 | 4       | 45      | 0           |
| Biostar              | X370GTN                                          | 2018 | 4       | 45      | 0           |
| ASUSTek Computer     | A88X-PLUS/USB                                    | 2016 | 4       | 45      | 0           |
| Intel                | DH55TC AAE70932-303                              | 2010 | 4       | 45      | 0           |
| ASUSTek Computer     | M4N98TD EVO                                      | 2010 | 4       | 45      | 0           |
| Gigabyte Technology  | GA-770TA-UD3                                     | 2009 | 4       | 45      | 0           |
| Intel                | DG33FB AAD81072-303                              | 2007 | 4       | 45      | 0           |
| Biostar              | X370GT7                                          | 2019 | 4       | 44      | 0           |
| Gigabyte Technology  | Z370 AORUS Ultra Gaming                          | 2018 | 4       | 44      | 0           |
| Gigabyte Technology  | A320M-HD2                                        | 2017 | 4       | 44      | 0           |
| ASRock               | FM2A88M Pro3+                                    | 2016 | 4       | 44      | 0           |
| Dell                 | Studio XPS 7100                                  | 2010 | 4       | 44      | 0           |
| ASUSTek Computer     | M4A79 Deluxe                                     | 2010 | 4       | 44      | 0           |
| ASUSTek Computer     | P5E3 PRO                                         | 2009 | 4       | 44      | 0           |
| ASUSTek Computer     | M3A-H/HDMI                                       | 2008 | 4       | 44      | 0           |
| ASRock               | H470M-ITX/ac                                     | 2020 | 4       | 43      | 0           |
| ASRock               | Z370 Gaming K6                                   | 2018 | 4       | 43      | 0           |
| ASRock               | FM2A75M-HD+                                      | 2013 | 4       | 43      | 0           |
| ASRock               | E350M1/USB3                                      | 2011 | 4       | 43      | 0           |
| ECS                  | A790GXM-AD3                                      | 2009 | 4       | 43      | 0           |
| ASUSTek Computer     | Maximus Formula                                  | 2007 | 4       | 43      | 0           |
| Hewlett-Packard      | OMEN by HP Obelisk Desktop 875-0xxx              | 2019 | 4       | 42      | 0           |
| Medion               | MS-7857                                          | 2013 | 4       | 42      | 0           |
| Biostar              | TA970                                            | 2012 | 4       | 42      | 0           |
| Gateway              | SX2370                                           | 2011 | 4       | 42      | 0           |
| Foxconn              | G41MXE/G41MXE-K                                  | 2010 | 4       | 42      | 0           |
| ASUSTek Computer     | M3N72-D                                          | 2010 | 4       | 42      | 0           |
| ASUSTek Computer     | P5Q-VM                                           | 2008 | 4       | 42      | 0           |
| ASUSTek Computer     | P5E3 Deluxe                                      | 2008 | 4       | 42      | 0           |
| Gigabyte Technology  | H170-HD3                                         | 2017 | 4       | 41      | 0           |
| Gigabyte Technology  | A320M-DS2                                        | 2017 | 4       | 41      | 0           |
| ASRock               | FM2A88M-HD+ R3.0                                 | 2015 | 4       | 41      | 0           |
| MSI                  | MS-7759                                          | 2012 | 4       | 41      | 0           |
| ASUSTek Computer     | F2A55-M                                          | 2012 | 4       | 41      | 0           |
| Gigabyte Technology  | Z68X-UD3P-B3                                     | 2011 | 4       | 41      | 0           |
| ASUSTek Computer     | F1A75-M                                          | 2011 | 4       | 41      | 0           |
| ASUSTek Computer     | CM6630_CM6730_CM6830                             | 2011 | 4       | 41      | 0           |
| ASUSTek Computer     | M3A32-MVP DELUXE                                 | 2010 | 4       | 41      | 0           |
| Fujitsu Siemens      | ESPRIMO P5720                                    | 2008 | 4       | 41      | 0           |
| ASUSTek Computer     | P5E Deluxe                                       | 2008 | 4       | 41      | 0           |
| ASRock               | 4Core1600Twins-P35                               | 2008 | 4       | 41      | 0           |
| Intel                | DP35DP AAD81073-206                              | 2007 | 4       | 41      | 0           |
| Gigabyte Technology  | GA-MA770-S3                                      | 2007 | 4       | 41      | 0           |
| ASUSTek Computer     | M2V-MX SE                                        | 2007 | 4       | 41      | 0           |
| ASUSTek Computer     | M2A-MVP                                          | 2006 | 4       | 41      | 0           |
| ASRock               | A320M Pro4-F                                     | 2019 | 4       | 40      | 0           |
| Gigabyte Technology  | H170N-WIFI                                       | 2018 | 4       | 40      | 0           |
| ASRock               | H97 Killer                                       | 2018 | 4       | 40      | 0           |
| ASUSTek Computer     | Maximus V EXTREME                                | 2012 | 4       | 40      | 0           |
| Hewlett-Packard      | 320-1030                                         | 2011 | 4       | 40      | 0           |
| Fujitsu              | ESPRIMO E7936                                    | 2010 | 4       | 40      | 0           |
| Gigabyte Technology  | GA-MA78GM-UD2H                                   | 2009 | 4       | 40      | 0           |
| Dell                 | Studio Hybrid 140g                               | 2008 | 4       | 40      | 0           |
| Intel                | DG33FB AAD81072-306                              | 2007 | 4       | 40      | 0           |
| ASRock               | AB350M Pro4 R2.0                                 | 2018 | 4       | 39      | 0           |
| Gigabyte Technology  | H270N-WIFI                                       | 2017 | 4       | 39      | 0           |
| Acer                 | Aspire XC-230                                    | 2017 | 4       | 39      | 0           |
| Gigabyte Technology  | Z170-HD3                                         | 2016 | 4       | 39      | 0           |
| ASUSTek Computer     | H170M-E D3                                       | 2015 | 4       | 39      | 0           |
| Acer                 | Aspire XC-705                                    | 2014 | 4       | 39      | 0           |
| MSI                  | MS-7792                                          | 2013 | 4       | 39      | 0           |
| ZOTAC                | AMD HUDSON-M1                                    | 2011 | 4       | 39      | 0           |
| ECS                  | A785GM-AD3                                       | 2010 | 4       | 39      | 0           |
| Acer                 | Aspire X5950                                     | 2010 | 4       | 39      | 0           |
| ASUSTek Computer     | M4A78L-M                                         | 2010 | 4       | 39      | 0           |
| ASUSTek Computer     | M3A78-VM                                         | 2010 | 4       | 39      | 0           |
| Biostar              | GF8100 M2+ TE                                    | 2009 | 4       | 39      | 0           |
| ASUSTek Computer     | P5N73-AM                                         | 2008 | 4       | 39      | 0           |
| MSI                  | MS-7255                                          | 2007 | 4       | 39      | 0           |
| Fujitsu Siemens      | A8NE-FM                                          | 2005 | 4       | 39      | 0           |
| Gigabyte Technology  | B360N WIFI                                       | 2018 | 4       | 38      | 0           |
| Hewlett-Packard      | 402 G1 SFF Business PC                           | 2014 | 4       | 38      | 0           |
| ASRock               | Z97M Anniversary                                 | 2014 | 4       | 38      | 0           |
| ASRock               | FM2A55M-HD+                                      | 2013 | 4       | 38      | 0           |
| ASRock               | FM2A75 Pro4-M                                    | 2012 | 4       | 38      | 0           |
| Foxconn              | nT-A3000 series                                  | 2011 | 4       | 38      | 0           |
| Foxconn              | H67M-S/H67M-V/H67                                | 2011 | 4       | 38      | 0           |
| MSI                  | MS-7345                                          | 2007 | 4       | 38      | 0           |
| Gigabyte Technology  | GA-MA69GM-S2H                                    | 2007 | 4       | 38      | 0           |
| ASUSTek Computer     | TUF B360-PRO GAMING                              | 2019 | 4       | 37      | 0           |
| ASUSTek Computer     | ROG STRIX B360-G GAMING                          | 2018 | 4       | 37      | 0           |
| Acer                 | Aspire XC-330                                    | 2017 | 4       | 37      | 0           |
| Gigabyte Technology  | Z170-HD3P-CF                                     | 2016 | 4       | 37      | 0           |
| Hewlett-Packard      | EliteDesk 705 G1 MT                              | 2014 | 4       | 37      | 0           |
| Gigabyte Technology  | GA-E350N                                         | 2012 | 4       | 37      | 0           |
| ECS                  | A75F2-M2                                         | 2012 | 4       | 37      | 0           |
| ASUSTek Computer     | F1A55-M LX PLUS                                  | 2011 | 4       | 37      | 0           |
| Intel                | DP43TF AAE34878-404                              | 2009 | 4       | 37      | 0           |
| Hewlett-Packard      | xw9400 Workstation                               | 2009 | 4       | 37      | 0           |
| ELSA                 | P45IA-R2 1048                                    | 2009 | 4       | 37      | 0           |
| Dell                 | Inspiron 546                                     | 2009 | 4       | 37      | 0           |
| ASUSTek Computer     | P5KPL-AM/PS                                      | 2009 | 4       | 37      | 0           |
| Hewlett-Packard      | Compaq dx7500 Microtower                         | 2008 | 4       | 37      | 0           |
| Gigabyte Technology  | GA-E7AUM-DS2H                                    | 2008 | 4       | 37      | 0           |
| Biostar              | NF520-A2 TE                                      | 2008 | 4       | 37      | 0           |
| ASUSTek Computer     | P5Q SE/R                                         | 2008 | 4       | 37      | 0           |
| Hewlett-Packard      | workstation xw8200                               | 2005 | 4       | 37      | 0           |
| ASRock               | K8NF4G-SATA2                                     | 2005 | 4       | 37      | 0           |
| SYWZ                 | S210H Series                                     | 2020 | 4       | 36      | 0           |
| Gigabyte Technology  | Z68A-D3-B3                                       | 2012 | 4       | 36      | 0           |
| Biostar              | A960G+                                           | 2012 | 4       | 36      | 0           |
| Biostar              | A780L3C                                          | 2012 | 4       | 36      | 0           |
| MSI                  | MS-7698                                          | 2011 | 4       | 36      | 0           |
| Nvidia               | MCP7A                                            | 2009 | 4       | 36      | 0           |
| Dell                 | Inspiron 580s                                    | 2009 | 4       | 36      | 0           |
| ASUSTek Computer     | P5Q-VM DO                                        | 2009 | 4       | 36      | 0           |
| ASRock               | AMCP7AION-HT                                     | 2009 | 4       | 36      | 0           |
| MSI                  | MS-7357                                          | 2007 | 4       | 36      | 0           |
| Acer                 | Nitro N50-610                                    | 2020 | 4       | 35      | 0           |
| Hewlett-Packard      | Z1 Entry Tower G5                                | 2019 | 4       | 35      | 0           |
| Dell                 | OptiPlex 5060                                    | 2018 | 4       | 35      | 0           |
| Fujitsu              | ESPRIMO P520                                     | 2015 | 4       | 35      | 0           |
| ASUSTek Computer     | A_F_K31DA_K31DAG_K20DA                           | 2015 | 4       | 35      | 0           |
| ASRock               | H170M-ITX/ac                                     | 2015 | 4       | 35      | 0           |
| Gigabyte Technology  | J1900N-D3V                                       | 2014 | 4       | 35      | 0           |
| Gigabyte Technology  | H97-Gaming 3                                     | 2014 | 4       | 35      | 0           |
| Hewlett-Packard      | ProDesk 405 G1 MT                                | 2013 | 4       | 35      | 0           |
| Hewlett-Packard      | Pro 3515 Series                                  | 2013 | 4       | 35      | 0           |
| Intel                | DZ68DB AAG27985-101                              | 2011 | 4       | 35      | 0           |
| Gigabyte Technology  | PH67-DS3-B3                                      | 2011 | 4       | 35      | 0           |
| Positivo             | POS-PQ45AU                                       | 2010 | 4       | 35      | 0           |
| Hewlett-Packard      | p6710f                                           | 2010 | 4       | 35      | 0           |
| Acer                 | Aspire M3400                                     | 2010 | 4       | 35      | 0           |
| ASUSTek Computer     | M4A88T-V EVO                                     | 2010 | 4       | 35      | 0           |
| Sapphire             | PI-AM3RS785G                                     | 2009 | 4       | 35      | 0           |
| Intel                | DG31PR AAD97573-301                              | 2008 | 4       | 35      | 0           |
| ASUSTek Computer     | PRIME B360M-D                                    | 2018 | 4       | 34      | 0           |
| ASRock               | H81M-ITX                                         | 2015 | 4       | 34      | 0           |
| ASRock               | B85M Pro3                                        | 2014 | 4       | 34      | 0           |
| Shuttle              | SH61R                                            | 2012 | 4       | 34      | 0           |
| Standard             | XS35                                             | 2010 | 4       | 34      | 0           |
| Hewlett-Packard      | Pro 3000/3080                                    | 2010 | 4       | 34      | 0           |
| ASUSTek Computer     | EB1501P                                          | 2010 | 4       | 34      | 0           |
| Hewlett-Packard      | Compaq 500B Microtower                           | 2009 | 4       | 34      | 0           |
| Acer                 | Aspire M3802                                     | 2009 | 4       | 34      | 0           |
| ASRock               | N68PV-GS                                         | 2009 | 4       | 34      | 0           |
| Foxconn              | G31MV/G31MV-K FAB                                | 2008 | 4       | 34      | 0           |
| K-Systems            | G31T-M                                           | 2007 | 4       | 34      | 0           |
| Gigabyte Technology  | 8I945P-G-RH                                      | 2006 | 4       | 34      | 0           |
| ASUSTek Computer     | P5GD1-VM                                         | 2004 | 4       | 34      | 0           |
| TSINGHUA TONGFANG... | E500                                             | 2020 | 4       | 33      | 0           |
| Gigabyte Technology  | H110M-S2V-CF                                     | 2016 | 4       | 33      | 0           |
| Intel                | CRESCENTBAY                                      | 2015 | 4       | 33      | 0           |
| Hewlett-Packard      | t620 PLUS Quad Core TC                           | 2014 | 4       | 33      | 0           |
| ASRock               | B85 Anniversary                                  | 2014 | 4       | 33      | 0           |
| ITMediaConsult       | Pentino G-Series                                 | 2013 | 4       | 33      | 0           |
| Gigabyte Technology  | Z87MX-D3H                                        | 2013 | 4       | 33      | 0           |
| ASRock               | FM2A55M-DGS R2.0                                 | 2013 | 4       | 33      | 0           |
| Dell                 | Vostro 470                                       | 2012 | 4       | 33      | 0           |
| MSI                  | MS-7676                                          | 2011 | 4       | 33      | 0           |
| MEGA                 | G41T-M7 LGT                                      | 2011 | 4       | 33      | 0           |
| ASUSTek Computer     | Maximus IV GENE-Z/GEN3                           | 2011 | 4       | 33      | 0           |
| Positivo             | POS-PIQ57BQ                                      | 2010 | 4       | 33      | 0           |
| Intel                | DG41WV AAE90316-103                              | 2010 | 4       | 33      | 0           |
| ASUSTek Computer     | P5G41T-M LX2/GB/LPT                              | 2010 | 4       | 33      | 0           |
| Fujitsu Siemens      | ESPRIMO Q5030                                    | 2009 | 4       | 33      | 0           |
| ASUSTek Computer     | P5G41-M LX2/GB                                   | 2009 | 4       | 33      | 0           |
| Fujitsu Siemens      | G31T-M2                                          | 2007 | 4       | 33      | 0           |
| ASUSTek Computer     | PRIME H310T R2.0                                 | 2018 | 4       | 32      | 0           |
| ASUSTek Computer     | PRIME H310M-D R2.0                               | 2018 | 4       | 32      | 0           |
| Biostar              | A68N-5600                                        | 2017 | 4       | 32      | 0           |
| Gigabyte Technology  | G1.Sniper B5                                     | 2014 | 4       | 32      | 0           |
| ASUSTek Computer     | VM42                                             | 2014 | 4       | 32      | 0           |
| Gigabyte Technology  | H61M-S2P-R3                                      | 2013 | 4       | 32      | 0           |
| ASRock               | H61M-ITX                                         | 2012 | 4       | 32      | 0           |
| Intel                | DQ67SW AAG12527-309                              | 2011 | 4       | 32      | 0           |
| ASRock               | G41M-GS                                          | 2009 | 4       | 32      | 0           |
| Gigabyte Technology  | EG31MF-S2                                        | 2008 | 4       | 32      | 0           |
| MSI                  | MS-7379                                          | 2007 | 4       | 32      | 0           |
| ASUSTek Computer     | P5GC-VM                                          | 2007 | 4       | 32      | 0           |
| Gigabyte Technology  | 8I945P-G                                         | 2005 | 4       | 32      | 0           |
| ASRock               | B150M Pro4                                       | 2015 | 4       | 31      | 0           |
| Hewlett-Packard      | t520 Flexible Series TC                          | 2014 | 4       | 31      | 0           |
| ASUSTek Computer     | BM6AD_BM1AD_BP1AD                                | 2014 | 4       | 31      | 0           |
| Acer                 | Aspire MC605                                     | 2012 | 4       | 31      | 0           |
| ASUSTek Computer     | P8Q77-M                                          | 2012 | 4       | 31      | 0           |
| Hewlett-Packard      | Pro 3300 Series SFF                              | 2011 | 4       | 31      | 0           |
| Fujitsu              | ESPRIMO P500                                     | 2011 | 4       | 31      | 0           |
| Biostar              | H61MLC                                           | 2011 | 4       | 31      | 0           |
| Pegatron             | IPX41-D3                                         | 2009 | 4       | 31      | 0           |
| ECS                  | G41T-M6                                          | 2009 | 4       | 31      | 0           |
| Biostar              | G41-M7                                           | 2009 | 4       | 31      | 0           |
| Acer                 | Aspire R3600                                     | 2009 | 4       | 31      | 0           |
| Hewlett-Packard      | Compaq dx7400 Microtower                         | 2007 | 4       | 31      | 0           |
| PCWare               | IPMH310G                                         | 2019 | 4       | 30      | 0           |
| Hewlett-Packard      | ProDesk 400 G5 Desktop Mini                      | 2019 | 4       | 30      | 0           |
| Google               | Guado                                            | 2019 | 4       | 30      | 0           |
| ASRock               | H310CM-HDV/M.2                                   | 2018 | 4       | 30      | 0           |
| Gigabyte Technology  | H110M-M.2                                        | 2017 | 4       | 30      | 0           |
| Lenovo               | H520S 2561                                       | 2012 | 4       | 30      | 0           |
| Intel                | DH61BF AAG81311-101                              | 2012 | 4       | 30      | 0           |
| Gigabyte Technology  | G1.Sniper M3-CF                                  | 2012 | 4       | 30      | 0           |
| Acer                 | Predator G3620                                   | 2012 | 4       | 30      | 0           |
| ASUSTek Computer     | ET1612I                                          | 2012 | 4       | 30      | 0           |
| Intel                | DH67CL AAG10212-208                              | 2011 | 4       | 30      | 0           |
| Hewlett-Packard      | Compaq dx2390 Microtower                         | 2008 | 4       | 30      | 0           |
| Intel                | D945GCPE AAD97209-201                            | 2007 | 4       | 30      | 0           |
| Foxconn              | 946 7MA Series                                   | 2006 | 4       | 30      | 0           |
| Intel                | DH61WW AAG23116-303                              | 2012 | 4       | 29      | 0           |
| Lenovo               | H420                                             | 2011 | 4       | 29      | 0           |
| Acer                 | Aspire X3960                                     | 2010 | 4       | 29      | 0           |
| Megaware             | MW-G31T-M7                                       | 2009 | 4       | 29      | 0           |
| MSI                  | MS-7610                                          | 2009 | 4       | 29      | 0           |
| Intel                | DG31PR AAD97573-300                              | 2008 | 4       | 29      | 0           |
| Acer                 | Veriton L460                                     | 2007 | 4       | 29      | 0           |
| Foxconn              | 945 7MC Series                                   | 2006 | 4       | 29      | 0           |
| Fujitsu              | CELSIUS W570                                     | 2017 | 4       | 28      | 0           |
| Intel                | Q3XXG4-P V1.0                                    | 2016 | 4       | 28      | 0           |
| Gigabyte Technology  | J1800M-D3P                                       | 2015 | 4       | 28      | 0           |
| ECS                  | BAT-I                                            | 2014 | 4       | 28      | 0           |
| DEPO Computers       | MS-7817                                          | 2014 | 4       | 28      | 0           |
| Medion               | H61H2-LM3                                        | 2012 | 4       | 28      | 0           |
| Intel                | DZ75ML-45K AAG75008-102                          | 2012 | 4       | 28      | 0           |
| Acer                 | Aspire X1900                                     | 2010 | 4       | 28      | 0           |
| Intel                | DG41TY AAE47335-300                              | 2009 | 4       | 28      | 0           |
| Gigabyte Technology  | EG31M-S2                                         | 2008 | 4       | 28      | 0           |
| Hewlett-Packard      | ProDesk 490 G3 MT Business PC                    | 2018 | 4       | 27      | 0           |
| Intel                | DN2820FYK H24582-204                             | 2014 | 4       | 27      | 0           |
| Intel                | DN2820FYK H24582-202                             | 2014 | 4       | 27      | 0           |
| Gigabyte Technology  | GB-BXBT-2807                                     | 2014 | 4       | 27      | 0           |
| Digibras             | MPxx                                             | 2014 | 4       | 27      | 0           |
| Fujitsu              | ESPRIMO P410                                     | 2013 | 4       | 27      | 0           |
| Gigabyte Technology  | C847N                                            | 2012 | 4       | 27      | 0           |
| Foxconn              | 8657MF-series                                    | 2006 | 4       | 27      | 0           |
| ASUSTek Computer     | PRIME J4005I-C                                   | 2019 | 4       | 26      | 0           |
| Lenovo               | ThinkCentre M800 10FW0004US                      | 2016 | 4       | 26      | 0           |
| CSL-Computer         | Narrow Box 4K 4GB                                | 2016 | 4       | 26      | 0           |
| Intel                | DH61CR AAG14064-209                              | 2012 | 4       | 26      | 0           |
| Foxconn              | 45CS/45CSX                                       | 2008 | 4       | 26      | 0           |
| Gigabyte Technology  | H510M S2H                                        | 2021 | 4       | 25      | 0           |
| Intel                | D34010WYK H14771-302                             | 2013 | 4       | 25      | 0           |
| MSI                  | MS-7A93                                          | 2018 | 3       | 98      | 0           |
| Huanan               | X79 249PC V2.2                                   | 2020 | 3       | 97      | 0           |
| Dell                 | Precision 5820 Tower X-Series                    | 2020 | 3       | 96      | 0           |
| ASRock               | X79 Extreme4-M                                   | 2012 | 3       | 90      | 0           |
| Gigabyte Technology  | X299X AORUS MASTER                               | 2019 | 3       | 89      | 0           |
| ASRock               | X79 Extreme9                                     | 2014 | 3       | 88      | 0           |
| Alienware            | Area-51 R5                                       | 2018 | 3       | 84      | 0           |
| ASUSTek Computer     | WS C422 PRO_SE                                   | 2018 | 3       | 84      | 0           |
| Sirona Dental Sys... | MS-7735                                          | 2013 | 3       | 83      | 0           |
| Fujitsu              | CELSIUS M720                                     | 2012 | 3       | 82      | 0           |
| Huanan               | X79 INTEL (INTEL Xeon E5/Corei7 DMI2 - C600/C... | 2018 | 3       | 79      | 0           |
| Gigabyte Technology  | EX58-EXTREME                                     | 2010 | 3       | 79      | 0           |
| Intel                | X79 V2.72B                                       | 2021 | 3       | 74      | 0           |
| Gigabyte Technology  | P55-UD5                                          | 2010 | 3       | 68      | 0           |
| Gigabyte Technology  | P55A-UD3P                                        | 2009 | 3       | 63      | 0           |
| Gigabyte Technology  | Z97X-UD7 TH                                      | 2015 | 3       | 61      | 0           |
| ASUSTek Computer     | SABERTOOTH 990FX R3.0                            | 2016 | 3       | 60      | 0           |
| ASRock               | P55M Pro                                         | 2009 | 3       | 60      | 0           |
| ASRock               | X570 Creator                                     | 2019 | 3       | 59      | 0           |
| ASRock               | Z370 Professional Gaming i7                      | 2017 | 3       | 59      | 0           |
| Lenovo               | ThinkStation D20 4158AF8                         | 2012 | 3       | 59      | 0           |
| ASUSTek Computer     | Maximus III Formula                              | 2010 | 3       | 57      | 0           |
| Hewlett-Packard      | EliteDesk 705 G4 SFF                             | 2018 | 3       | 56      | 0           |
| Lenovo               | ThinkStation P620 30E0CTO1WW                     | 2020 | 3       | 55      | 0           |
| ASRock               | 990FX Extreme6                                   | 2014 | 3       | 53      | 0           |
| AAEON                | UP-APL01                                         | 2017 | 3       | 52      | 0           |
| Gigabyte Technology  | Z170X-GamingG1                                   | 2015 | 3       | 52      | 0           |
| Intel                | DQ57TM AAE70931-402                              | 2010 | 3       | 52      | 0           |
| Fujitsu              | ESPRIMO E9900                                    | 2010 | 3       | 52      | 0           |
| ASUSTek Computer     | P7H55-M BR                                       | 2010 | 3       | 51      | 0           |
| ASRock               | X570 Phantom Gaming X                            | 2020 | 3       | 50      | 0           |
| Biostar              | H55 HD                                           | 2010 | 3       | 50      | 0           |
| ASRock               | H55M                                             | 2010 | 3       | 50      | 0           |
| Acer                 | Aspire M5810                                     | 2009 | 3       | 50      | 0           |
| ASUSTek Computer     | P5K WS                                           | 2008 | 3       | 50      | 0           |
| ASRock               | B550 Phantom Gaming 4/ac                         | 2021 | 3       | 49      | 0           |
| ASUSTek Computer     | Maximus VIII FORMULA                             | 2018 | 3       | 49      | 0           |
| MSI                  | MS-7A46                                          | 2016 | 3       | 49      | 0           |
| Hewlett-Packard      | 510-p127c                                        | 2016 | 3       | 49      | 0           |
| Intel                | DP55WB AAE64798-204                              | 2009 | 3       | 49      | 0           |
| Gigabyte Technology  | EP45-DS3P                                        | 2008 | 3       | 49      | 0           |
| ASRock               | B550AM Gaming                                    | 2019 | 3       | 48      | 0           |
| Fujitsu              | PRIMERGY TX100 S2                                | 2010 | 3       | 48      | 0           |
| Medion               | E62009                                           | 2019 | 3       | 47      | 0           |
| ASRock               | X370 Killer SLI/ac                               | 2017 | 3       | 47      | 0           |
| MSI                  | MS-7893                                          | 2015 | 3       | 47      | 0           |
| Lenovo               | IdeaCentre 720-18ASU 90H10003US                  | 2017 | 3       | 46      | 0           |
| ASRock               | Z270M Extreme4                                   | 2017 | 3       | 46      | 0           |
| Acer                 | Aspire TC-280                                    | 2016 | 3       | 46      | 0           |
| System76             | Wild Dog Pro                                     | 2015 | 3       | 46      | 0           |
| ASRock               | 770 Extreme3                                     | 2010 | 3       | 46      | 0           |
| Gigabyte Technology  | M61P-S3                                          | 2006 | 3       | 46      | 0           |
| ASUSTek Computer     | ROG STRIX Z590-E GAMING WIFI                     | 2021 | 3       | 45      | 0           |
| Gigabyte Technology  | A520 AORUS ELITE                                 | 2020 | 3       | 45      | 0           |
| SYWZ                 | S200 Series                                      | 2019 | 3       | 45      | 0           |
| ASUSTek Computer     | ROG STRIX B360-H GAMING                          | 2018 | 3       | 45      | 0           |
| ASRock               | AB350M                                           | 2017 | 3       | 45      | 0           |
| Gigabyte Technology  | Z87X-OC                                          | 2013 | 3       | 45      | 0           |
| Gigabyte Technology  | Z68X-UD7-B3                                      | 2011 | 3       | 45      | 0           |
| Hewlett-Packard      | xw6400 Workstation                               | 2007 | 3       | 45      | 0           |
| ASUSTek Computer     | PRIME B450M-K II                                 | 2020 | 3       | 44      | 0           |
| ASRock               | B550 Pro4                                        | 2020 | 3       | 44      | 0           |
| ASUSTek Computer     | SABERTOOTH Z170 MARK 1                           | 2016 | 3       | 44      | 0           |
| Lenovo               | H50-55 90BG003JUS                                | 2015 | 3       | 44      | 0           |
| Acer                 | Aspire M3300                                     | 2009 | 3       | 44      | 0           |
| Gigabyte Technology  | A520M H                                          | 2020 | 3       | 43      | 0           |
| Alienware            | Aurora R6                                        | 2018 | 3       | 43      | 0           |
| Gigabyte Technology  | GA-970-Gaming SLI-CF                             | 2016 | 3       | 43      | 0           |
| MSI                  | MS-7914                                          | 2014 | 3       | 43      | 0           |
| AMD                  | 970A-D3                                          | 2012 | 3       | 43      | 0           |
| ASUSTek Computer     | Crosshair IV Extreme                             | 2011 | 3       | 43      | 0           |
| ASUSTek Computer     | STRIKER II NSE                                   | 2008 | 3       | 43      | 0           |
| BESSTAR Tech         | DMAF5                                            | 2020 | 3       | 42      | 0           |
| Hewlett-Packard      | 290 G1 SFF Business PC                           | 2019 | 3       | 42      | 0           |
| Biostar              | B350ET2                                          | 2019 | 3       | 42      | 0           |
| Alienware            | Aurora R8                                        | 2018 | 3       | 42      | 0           |
| ASRock               | H270 Pro4                                        | 2017 | 3       | 42      | 0           |
| Gigabyte Technology  | Z87X-OC Force                                    | 2013 | 3       | 42      | 0           |
| Gigabyte Technology  | F2A85XN-WIFI                                     | 2013 | 3       | 42      | 0           |
| ASRock               | FM2A85X Extreme4                                 | 2013 | 3       | 42      | 0           |
| ASUSTek Computer     | P8P67 WS REVOLUTION                              | 2012 | 3       | 42      | 0           |
| K-Systems            | IRBIS                                            | 2010 | 3       | 42      | 0           |
| Hewlett-Packard      | Pro 3120 Microtower PC                           | 2010 | 3       | 42      | 0           |
| Hewlett-Packard      | AY747AA-ABA p6310y                               | 2009 | 3       | 42      | 0           |
| ECS                  | A785GM-M                                         | 2009 | 3       | 42      | 0           |
| ECS                  | GeForce 8000 series                              | 2008 | 3       | 42      | 0           |
| Gigabyte Technology  | 965P-DQ6                                         | 2006 | 3       | 42      | 0           |
| Gigabyte Technology  | A520I AC                                         | 2021 | 3       | 41      | 0           |
| ASRock               | B360M IB-R1                                      | 2019 | 3       | 41      | 0           |
| ASUSTek Computer     | G11DF                                            | 2017 | 3       | 41      | 0           |
| ECS                  | A68F2P-M4                                        | 2015 | 3       | 41      | 0           |
| Acer                 | Aspire TC-220                                    | 2014 | 3       | 41      | 0           |
| ASUSTek Computer     | G20AJ                                            | 2014 | 3       | 41      | 0           |
| MSI                  | MS-7637                                          | 2010 | 3       | 41      | 0           |
| Intel                | DH55TC AAE70932-205                              | 2009 | 3       | 41      | 0           |
| Gigabyte Technology  | GA-790XT-USB3                                    | 2009 | 3       | 41      | 0           |
| Intel                | DG33TL AAD89517-803                              | 2007 | 3       | 41      | 0           |
| ECS                  | GF7100/7050PVT-M3                                | 2007 | 3       | 41      | 0           |
| Hewlett-Packard      | xw9300 Workstation                               | 2005 | 3       | 41      | 0           |
| Alienware            | Aurora R5                                        | 2019 | 3       | 40      | 0           |
| Gigabyte Technology  | F2A88XM-DS2P                                     | 2015 | 3       | 40      | 0           |
| Lenovo               | ThinkCentre M78 10BR0005US                       | 2014 | 3       | 40      | 0           |
| ASUSTek Computer     | K30BF_M32BF_A_F_K31BF                            | 2014 | 3       | 40      | 0           |
| Gigabyte Technology  | Z77X-UP4 TH                                      | 2012 | 3       | 40      | 0           |
| ASUSTek Computer     | E45M1-M PRO                                      | 2012 | 3       | 40      | 0           |
| Intel                | DP43BF AAE78171-302                              | 2010 | 3       | 40      | 0           |
| ASUSTek Computer     | M4A88TD-M                                        | 2010 | 3       | 40      | 0           |
| ECS                  | IC780M-A2                                        | 2009 | 3       | 40      | 0           |
| Acer                 | Aspire M3203                                     | 2009 | 3       | 40      | 0           |
| Acer                 | Aspire M5620                                     | 2007 | 3       | 40      | 0           |
| Intel                | DG965WH AAD41692-306                             | 2006 | 3       | 40      | 0           |
| DEPO Computers       | C51GM03                                          | 2006 | 3       | 40      | 0           |
| ASUSTek Computer     | C51MCP51                                         | 2006 | 3       | 40      | 0           |
| Dell                 | Precision 3440                                   | 2020 | 3       | 39      | 0           |
| Maxtang              | FP30                                             | 2019 | 3       | 39      | 0           |
| Gigabyte Technology  | Z270X-UD3                                        | 2017 | 3       | 39      | 0           |
| Hewlett-Packard      | 285 G2 MT                                        | 2016 | 3       | 39      | 0           |
| Gigabyte Technology  | Z97X-Gaming 7                                    | 2015 | 3       | 39      | 0           |
| Gateway              | SX2110G                                          | 2013 | 3       | 39      | 0           |
| Acer                 | Veriton M4630G                                   | 2013 | 3       | 39      | 0           |
| ASUSTek Computer     | F1A55-M LE R2.0                                  | 2013 | 3       | 39      | 0           |
| ASRock               | Z75 Pro3                                         | 2013 | 3       | 39      | 0           |
| Gigabyte Technology  | GA-A75M-DS2                                      | 2012 | 3       | 39      | 0           |
| ASRock               | Q77M vPro                                        | 2012 | 3       | 39      | 0           |
| ASRock               | P67 Extreme4 Gen3                                | 2011 | 3       | 39      | 0           |
| Packard Bell         | imedia S3810                                     | 2010 | 3       | 39      | 0           |
| Acer                 | Aspire X3810                                     | 2009 | 3       | 39      | 0           |
| Acer                 | Aspire M5802/M3802                               | 2009 | 3       | 39      | 0           |
| ASUSTek Computer     | P5E64 WS EVOLUTION                               | 2008 | 3       | 39      | 0           |
| ABIT                 | IP35                                             | 2007 | 3       | 39      | 0           |
| ASUSTek Computer     | ROG STRIX H370-I GAMING                          | 2019 | 3       | 38      | 0           |
| Gigabyte Technology  | H170-Gaming 3                                    | 2016 | 3       | 38      | 0           |
| ASUSTek Computer     | Maximus VIII IMPACT                              | 2016 | 3       | 38      | 0           |
| ASRock               | Z97 Killer                                       | 2016 | 3       | 38      | 0           |
| MSI                  | MS-7980                                          | 2015 | 3       | 38      | 0           |
| MSI                  | MS-7979                                          | 2015 | 3       | 38      | 0           |
| MSI                  | MS-7818                                          | 2015 | 3       | 38      | 0           |
| MSI                  | MS-B05011                                        | 2013 | 3       | 38      | 0           |
| Hewlett-Packard      | CQ2930EA                                         | 2013 | 3       | 38      | 0           |
| Gigabyte Technology  | P67A-UD3P-B3                                     | 2011 | 3       | 38      | 0           |
| ASRock               | P67 Pro3 SE                                      | 2011 | 3       | 38      | 0           |
| Positivo             | POS-PIG43BC                                      | 2010 | 3       | 38      | 0           |
| Hewlett-Packard      | Compaq dc7800                                    | 2009 | 3       | 38      | 0           |
| ASUSTek Computer     | M4A78-VM                                         | 2009 | 3       | 38      | 0           |
| Medion               | MS-7501                                          | 2008 | 3       | 38      | 0           |
| Intel                | DQ45CB AAE30148-207                              | 2008 | 3       | 38      | 0           |
| Intel                | DQ35JO AAD82085-807                              | 2008 | 3       | 38      | 0           |
| Hewlett-Packard      | PPPPPPP-CCC#MMMMMMMM                             | 2008 | 3       | 38      | 0           |
| Intel                | DQ965GF AAD41676-402                             | 2007 | 3       | 38      | 0           |
| Gigabyte Technology  | GA-MA790X-DS4                                    | 2007 | 3       | 38      | 0           |
| Dell                 | Precision WorkStation 370                        | 2004 | 3       | 38      | 0           |
| MSI                  | H310 Gaming Infinite S                           | 2019 | 3       | 37      | 0           |
| ASUSTek Computer     | TUF Z390-PRO GAMING                              | 2018 | 3       | 37      | 0           |
| ASUSTek Computer     | STRIX H270I GAMING                               | 2018 | 3       | 37      | 0           |
| Hewlett-Packard      | EliteDesk 705 G3 Desktop Mini                    | 2017 | 3       | 37      | 0           |
| Gigabyte Technology  | G1.Sniper Z97                                    | 2014 | 3       | 37      | 0           |
| ASRock               | FM2A78M-HD+ R2.0                                 | 2014 | 3       | 37      | 0           |
| Packard Bell         | imedia S2185                                     | 2013 | 3       | 37      | 0           |
| ASUSTek Computer     | P8H61-M LX3 PLUS                                 | 2012 | 3       | 37      | 0           |
| ASUSTek Computer     | ET2012E                                          | 2012 | 3       | 37      | 0           |
| Hewlett-Packard      | p7-1010                                          | 2011 | 3       | 37      | 0           |
| Packard Bell         | imedia S1800                                     | 2010 | 3       | 37      | 0           |
| Hewlett-Packard      | Pro 3135 Microtower PC                           | 2010 | 3       | 37      | 0           |
| Foxconn              | A6GMV                                            | 2010 | 3       | 37      | 0           |
| Biostar              | N68SA-M2S                                        | 2010 | 3       | 37      | 0           |
| ASUSTek Computer     | M4N78-AM V2                                      | 2010 | 3       | 37      | 0           |
| ASUSTek Computer     | CM1630                                           | 2010 | 3       | 37      | 0           |
| eMachines            | EL1830                                           | 2009 | 3       | 37      | 0           |
| ASUSTek Computer     | P5QL-VM EPU                                      | 2009 | 3       | 37      | 0           |
| MSI                  | MS-7390                                          | 2008 | 3       | 37      | 0           |
| Foxconn              | G31MVP FAB:1.0                                   | 2008 | 3       | 37      | 0           |
| ABIT                 | AX78                                             | 2008 | 3       | 37      | 0           |
| ASUSTek Computer     | A8R32-MVP Deluxe                                 | 2006 | 3       | 37      | 0           |
| Gigabyte Technology  | H470M DS3H                                       | 2020 | 3       | 36      | 0           |
| ASUSTek Computer     | ROG STRIX B460-H GAMING                          | 2020 | 3       | 36      | 0           |
| MSI                  | MS-7C00                                          | 2019 | 3       | 36      | 0           |
| Acer                 | Predator PO3-600                                 | 2019 | 3       | 36      | 0           |
| ASUSTek Computer     | H170I-PRO                                        | 2016 | 3       | 36      | 0           |
| Gigabyte Technology  | G1.Sniper 5                                      | 2014 | 3       | 36      | 0           |
| Biostar              | A58MD                                            | 2014 | 3       | 36      | 0           |
| Lenovo               | IdeaCentre K450 10120                            | 2013 | 3       | 36      | 0           |
| ECS                  | A960M-MV                                         | 2013 | 3       | 36      | 0           |
| ASRock               | Z77 Performance                                  | 2013 | 3       | 36      | 0           |
| Itautec              | Infoway ST-4265                                  | 2012 | 3       | 36      | 0           |
| Biostar              | TZ68K+                                           | 2012 | 3       | 36      | 0           |
| ASRock               | 985GM-GS3 FX                                     | 2012 | 3       | 36      | 0           |
| Medion               | H67H2-EM                                         | 2011 | 3       | 36      | 0           |
| Hewlett-Packard      | p7-1110                                          | 2011 | 3       | 36      | 0           |
| ASUSTek Computer     | CM1730/CM1830                                    | 2011 | 3       | 36      | 0           |
| JW Technology        | JW-A61PM-D3 Ver1.0                               | 2010 | 3       | 36      | 0           |
| ASRock               | 939A790GMH                                       | 2010 | 3       | 36      | 0           |
| Acer                 | Aspire X3812                                     | 2009 | 3       | 36      | 0           |
| Acer                 | Aspire R1600                                     | 2009 | 3       | 36      | 0           |
| ASUSTek Computer     | P5Q TURBO                                        | 2009 | 3       | 36      | 0           |
| ASUSTek Computer     | M4A78-EM                                         | 2009 | 3       | 36      | 0           |
| K-Systems            | GeForce7050M-M                                   | 2008 | 3       | 36      | 0           |
| Acer                 | AL 5100 / VL410                                  | 2008 | 3       | 36      | 0           |
| ABIT                 | F-I90HD                                          | 2008 | 3       | 36      | 0           |
| ASRock               | ALiveXFire-eSATA2                                | 2007 | 3       | 36      | 0           |
| ABIT                 | IP35-E                                           | 2007 | 3       | 36      | 0           |
| ASRock               | H310M-HG4                                        | 2019 | 3       | 35      | 0           |
| Gigabyte Technology  | H370 WIFI                                        | 2018 | 3       | 35      | 0           |
| Gigabyte Technology  | Z270-HD3                                         | 2017 | 3       | 35      | 0           |
| ASRock               | Z270 Gaming-ITX/ac                               | 2017 | 3       | 35      | 0           |
| ASRock               | Z170M Pro4S                                      | 2015 | 3       | 35      | 0           |
| ASRock               | Z87 Pro3                                         | 2014 | 3       | 35      | 0           |
| ASRock               | H81M-ITX/WiFi                                    | 2014 | 3       | 35      | 0           |
| WYSE                 | DQ Class                                         | 2013 | 3       | 35      | 0           |
| Gigabyte Technology  | H61M-S2PH                                        | 2013 | 3       | 35      | 0           |
| Gigabyte Technology  | E350N WIN8                                       | 2013 | 3       | 35      | 0           |
| Gigabyte Technology  | B85M-D3PH                                        | 2013 | 3       | 35      | 0           |
| ASUSTek Computer     | A85XM-A                                          | 2013 | 3       | 35      | 0           |
| ASRock               | FM2A75M-DGS R2.0                                 | 2013 | 3       | 35      | 0           |
| ASRock               | FM2A85X Extreme4-M                               | 2012 | 3       | 35      | 0           |
| NCR                  | xxxx-xxxx-xxxx                                   | 2011 | 3       | 35      | 0           |
| Intel                | DH67BL AAG10189-209                              | 2011 | 3       | 35      | 0           |
| Gigabyte Technology  | GA-A55M-S2HP                                     | 2011 | 3       | 35      | 0           |
| Acer                 | Veriton M430G                                    | 2011 | 3       | 35      | 0           |
| ASRock               | 960GM-S3 FX                                      | 2011 | 3       | 35      | 0           |
| Hewlett-Packard      | p6653w                                           | 2010 | 3       | 35      | 0           |
| ASUSTek Computer     | M4A77D                                           | 2009 | 3       | 35      | 0           |
| Acer                 | Veriton N4660G                                   | 2018 | 3       | 34      | 0           |
| Fujitsu              | D3417-B2 S26361-D3417-B2                         | 2017 | 3       | 34      | 0           |
| ASUSTek Computer     | STRIX B250I GAMING                               | 2017 | 3       | 34      | 0           |
| Gigabyte Technology  | B150N Phoenix-WIFI                               | 2016 | 3       | 34      | 0           |
| ASUSTek Computer     | Z170M-PLUS/BR                                    | 2016 | 3       | 34      | 0           |
| ASRock               | Z170A-X1                                         | 2016 | 3       | 34      | 0           |
| ASUSTek Computer     | B150M-PLUS                                       | 2015 | 3       | 34      | 0           |
| Gigabyte Technology  | Z97X-SOC Force                                   | 2014 | 3       | 34      | 0           |
| DEPO Computers       | B85M-D3H                                         | 2014 | 3       | 34      | 0           |
| Biostar              | AM1ML                                            | 2014 | 3       | 34      | 0           |
| Gigabyte Technology  | GA-78LMT-S2PV                                    | 2013 | 3       | 34      | 0           |
| Hewlett-Packard      | p2-1343w                                         | 2012 | 3       | 34      | 0           |
| Lenovo               | IdeaCentre B320                                  | 2011 | 3       | 34      | 0           |
| Intel                | DH61BE AAG14062-206                              | 2011 | 3       | 34      | 0           |
| Gigabyte Technology  | PH67A-UD3-B3                                     | 2011 | 3       | 34      | 0           |
| ASUSTek Computer     | M3A78-T                                          | 2011 | 3       | 34      | 0           |
| ASRock               | H67M-GE                                          | 2011 | 3       | 34      | 0           |
| Positivo             | POS-PIG41BO                                      | 2010 | 3       | 34      | 0           |
| Intel                | DH55PJ AAE93812-301                              | 2010 | 3       | 34      | 0           |
| Biostar              | G41U3G                                           | 2010 | 3       | 34      | 0           |
| Biostar              | G31M+                                            | 2010 | 3       | 34      | 0           |
| Acer                 | Veriton S490G                                    | 2010 | 3       | 34      | 0           |
| ASUSTek Computer     | M3A78-EMH HDMI                                   | 2010 | 3       | 34      | 0           |
| Hewlett-Packard      | t5740                                            | 2009 | 3       | 34      | 0           |
| ASUSTek Computer     | M4A78-AM                                         | 2009 | 3       | 34      | 0           |
| Fujitsu Siemens      | ESPRIMO E5625                                    | 2008 | 3       | 34      | 0           |
| PCChips              | A13G                                             | 2007 | 3       | 34      | 0           |
| ECS                  | nForce4-A754                                     | 2005 | 3       | 34      | 0           |
| Acer                 | Aspire XC-886                                    | 2019 | 3       | 33      | 0           |
| ASUSTek Computer     | PRIME H310M-D                                    | 2018 | 3       | 33      | 0           |
| Gigabyte Technology  | B150M-DS3H                                       | 2017 | 3       | 33      | 0           |
| Fujitsu              | ESPRIMO Q556/2                                   | 2017 | 3       | 33      | 0           |
| ASUSTek Computer     | PRIME B250M-C                                    | 2017 | 3       | 33      | 0           |
| ASUSTek Computer     | B150 PRO GAMING                                  | 2016 | 3       | 33      | 0           |
| Gigabyte Technology  | Z170-HD3 DDR3-CF                                 | 2015 | 3       | 33      | 0           |
| Gigabyte Technology  | F2A58M-HD2                                       | 2014 | 3       | 33      | 0           |
| ASUSTek Computer     | VM60                                             | 2014 | 3       | 33      | 0           |
| ASRock               | FM2A58M-HD+                                      | 2014 | 3       | 33      | 0           |
| Lenovo               | H530s 10132                                      | 2013 | 3       | 33      | 0           |
| Dell                 | Vostro 3800                                      | 2013 | 3       | 33      | 0           |
| ASRock               | H81M-GL                                          | 2013 | 3       | 33      | 0           |
| ASRock               | H81M                                             | 2013 | 3       | 33      | 0           |
| Intel                | DH67BL AAG10189-207                              | 2011 | 3       | 33      | 0           |
| Hewlett-Packard      | Pro 3300 Series MT                               | 2011 | 3       | 33      | 0           |
| Gigabyte Technology  | H67A-D3H-B3                                      | 2011 | 3       | 33      | 0           |
| Foxconn              | 2A8C                                             | 2011 | 3       | 33      | 0           |
| ASUSTek Computer     | P8H67-I                                          | 2011 | 3       | 33      | 0           |
| Hewlett-Packard      | s5713w                                           | 2010 | 3       | 33      | 0           |
| Fujitsu              | ESPRIMO Q9000                                    | 2010 | 3       | 33      | 0           |
| ASRock               | H55M-GE                                          | 2010 | 3       | 33      | 0           |
| eMachines            | ET1331                                           | 2009 | 3       | 33      | 0           |
| Gigabyte Technology  | G41MT-ES2L                                       | 2009 | 3       | 33      | 0           |
| Gigabyte Technology  | EQ45M-S2                                         | 2009 | 3       | 33      | 0           |
| Acer                 | Veriton M265                                     | 2009 | 3       | 33      | 0           |
| ASRock               | ALiveNF7G-GLAN                                   | 2009 | 3       | 33      | 0           |
| Fujitsu Siemens      | ESPRIMO EDITION P2540                            | 2008 | 3       | 33      | 0           |
| Biostar              | N520D-A2                                         | 2007 | 3       | 33      | 0           |
| EPoX Computer        | NF550/570 DDR2: AF550/570 Series                 | 2006 | 3       | 33      | 0           |
| ASRock               | ConRoe945PL-GLAN                                 | 2006 | 3       | 33      | 0           |
| MSI                  | MS-7B29                                          | 2018 | 3       | 32      | 0           |
| Gigabyte Technology  | B360M GAMING HD                                  | 2018 | 3       | 32      | 0           |
| ASUSTek Computer     | STRIX B250H GAMING                               | 2017 | 3       | 32      | 0           |
| ASUSTek Computer     | PRIME B250M-PLUS/BR                              | 2017 | 3       | 32      | 0           |
| ASUSTek Computer     | M32CD4-K                                         | 2017 | 3       | 32      | 0           |
| ASUSTek Computer     | B150M-C D3/BR                                    | 2017 | 3       | 32      | 0           |
| ASRock               | N3700-ITX                                        | 2015 | 3       | 32      | 0           |
| Gigabyte Technology  | MSH87TN-00                                       | 2014 | 3       | 32      | 0           |
| Gigabyte Technology  | H81ND2H                                          | 2014 | 3       | 32      | 0           |
| ASUSTek Computer     | VM62                                             | 2014 | 3       | 32      | 0           |
| Shuttle              | SH87R                                            | 2013 | 3       | 32      | 0           |
| Intel                | DH87RL AAG74240-401                              | 2013 | 3       | 32      | 0           |
| Intel                | DB85FL AAG89861-201                              | 2013 | 3       | 32      | 0           |
| Hewlett-Packard      | Compaq Pro 6300 All-in-One PC                    | 2012 | 3       | 32      | 0           |
| Itautec              | Infoway ST-4272                                  | 2011 | 3       | 32      | 0           |
| Intel                | HURONRIVER                                       | 2011 | 3       | 32      | 0           |
| Acer                 | Aspire X1920                                     | 2011 | 3       | 32      | 0           |
| eMachines            | ET1850                                           | 2010 | 3       | 32      | 0           |
| Fujitsu              | ESPRIMO E5731                                    | 2010 | 3       | 32      | 0           |
| Foxconn              | nT-330i                                          | 2009 | 3       | 32      | 0           |
| ASUSTek Computer     | AT3N7A-I                                         | 2009 | 3       | 32      | 0           |
| Biostar              | R690A-M2T                                        | 2008 | 3       | 32      | 0           |
| Acer                 | ASM5630/ASM3630/VTM460/VTS460                    | 2007 | 3       | 32      | 0           |
| Hewlett-Packard      | dx5150 MT                                        | 2006 | 3       | 32      | 0           |
| ASUSTek Computer     | P5VDC-MX/2.x                                     | 2006 | 3       | 32      | 0           |
| ASUSTek Computer     | P5GD2-X                                          | 2005 | 3       | 32      | 0           |
| ASUSTek Computer     | K8V-X SE                                         | 2005 | 3       | 32      | 0           |
| Hewlett-Packard      | workstation xw6200                               | 2004 | 3       | 32      | 0           |
| ASRock               | H410M-ITX/ac                                     | 2020 | 3       | 31      | 0           |
| ASRock               | B460M-ITX/ac                                     | 2020 | 3       | 31      | 0           |
| Lenovo               | ThinkCentre M93p 10AB0011US                      | 2019 | 3       | 31      | 0           |
| Gigabyte Technology  | MZGLKAP-00                                       | 2019 | 3       | 31      | 0           |
| Hewlett-Packard      | EliteDesk 800 G4 DM 65W                          | 2018 | 3       | 31      | 0           |
| Gigabyte Technology  | GB-BPCE-3455                                     | 2018 | 3       | 31      | 0           |
| ECS                  | H310H5-M2                                        | 2018 | 3       | 31      | 0           |
| ASUSTek Computer     | ROG Strix GL12CP_GL12CP                          | 2018 | 3       | 31      | 0           |
| ASRock               | J3160-NUC                                        | 2016 | 3       | 31      | 0           |
| Fujitsu              | FUTRO S720                                       | 2014 | 3       | 31      | 0           |
| Fujitsu              | ESPRIMO E420                                     | 2014 | 3       | 31      | 0           |
| Biostar              | H81MLV3                                          | 2014 | 3       | 31      | 0           |
| Acer                 | Veriton L4630G                                   | 2014 | 3       | 31      | 0           |
| Acer                 | Veriton M6620G                                   | 2012 | 3       | 31      | 0           |
| ASUSTek Computer     | P8H61-M LE/CSM R2.0                              | 2012 | 3       | 31      | 0           |
| ASRock               | B75M-GL                                          | 2012 | 3       | 31      | 0           |
| VS Company           | VS-MCP61M                                        | 2011 | 3       | 31      | 0           |
| Gigabyte Technology  | P61-S3                                           | 2011 | 3       | 31      | 0           |
| Gigabyte Technology  | H67N-USB3-B3                                     | 2011 | 3       | 31      | 0           |
| Hewlett-Packard      | SG3-130FR                                        | 2010 | 3       | 31      | 0           |
| Gigabyte Technology  | G41MT-D3                                         | 2010 | 3       | 31      | 0           |
| Fujitsu              | ESPRIMO P3521                                    | 2010 | 3       | 31      | 0           |
| Lenovo               | ThinkCentre M58 7627AA9                          | 2009 | 3       | 31      | 0           |
| Hewlett-Packard      | Compaq 505B Microtower PC                        | 2009 | 3       | 31      | 0           |
| ASUSTek Computer     | P5G41TD-M PRO                                    | 2009 | 3       | 31      | 0           |
| Foxconn              | M61PMV                                           | 2008 | 3       | 31      | 0           |
| ASUSTek Computer     | P5L8L-SE                                         | 2008 | 3       | 31      | 0           |
| Intel                | D945GCCR AAD78647-300                            | 2006 | 3       | 31      | 0           |
| ASUSTek Computer     | A8N-SLI SE                                       | 2006 | 3       | 31      | 0           |
| Gigabyte Technology  | 8I915PC Duo                                      | 2005 | 3       | 31      | 0           |
| Fujitsu Siemens      | SCENIC W                                         | 2004 | 3       | 31      | 0           |
| ASUSTek Computer     | PRIME H310I-PLUS R2.0                            | 2019 | 3       | 30      | 0           |
| Hewlett-Packard      | ProDesk 600 G3 MT                                | 2018 | 3       | 30      | 0           |
| Dell                 | Vostro 3268                                      | 2017 | 3       | 30      | 0           |
| ASUSTek Computer     | H110M-C2                                         | 2017 | 3       | 30      | 0           |
| ASRock               | J3710-ITX                                        | 2016 | 3       | 30      | 0           |
| TSINGHUA TONGFANG... | H81M-CT                                          | 2015 | 3       | 30      | 0           |
| Hewlett-Packard      | 750-114                                          | 2015 | 3       | 30      | 0           |
| Lenovo               | ThinkCentre M83 10AM0007US                       | 2014 | 3       | 30      | 0           |
| Intel                | DH87MC AAG74242-401                              | 2013 | 3       | 30      | 0           |
| Intel                | DQ77MK AAG39642-500                              | 2012 | 3       | 30      | 0           |
| ECS                  | H61H2-M17                                        | 2012 | 3       | 30      | 0           |
| Biostar              | H61MHV                                           | 2012 | 3       | 30      | 0           |
| ASRock               | AD2700-ITX                                       | 2012 | 3       | 30      | 0           |
| Qbex                 | QBEX-H61H2-M2                                    | 2011 | 3       | 30      | 0           |
| Hewlett-Packard      | 315eu                                            | 2010 | 3       | 30      | 0           |
| Acer                 | Aspire R3700                                     | 2010 | 3       | 30      | 0           |
| ASUSTek Computer     | P5G41T-M LX PLUS                                 | 2010 | 3       | 30      | 0           |
| OEGStone             | DG41RQ                                           | 2009 | 3       | 30      | 0           |
| Megaware             | IPM31                                            | 2009 | 3       | 30      | 0           |
| Digitron             | G31T-M7                                          | 2009 | 3       | 30      | 0           |
| Dell                 | Inspiron 400                                     | 2009 | 3       | 30      | 0           |
| 3Q                   | IPP7A-CP                                         | 2009 | 3       | 30      | 0           |
| ECS                  | 945P/PL-A                                        | 2008 | 3       | 30      | 0           |
| EPoX Computer        | nForce4 DDR: 9NPA7I / 9NPAI / 9NPA7J / 9NPAJ-... | 2006 | 3       | 30      | 0           |
| Hewlett-Packard      | EliteDesk 800 G4 DM 35W                          | 2018 | 3       | 29      | 0           |
| Gigabyte Technology  | H310MS2P                                         | 2018 | 3       | 29      | 0           |
| ASUSTek Computer     | PRIME H310M-A                                    | 2018 | 3       | 29      | 0           |
| ASUSTek Computer     | H110T                                            | 2017 | 3       | 29      | 0           |
| ASUSTek Computer     | A_F_K20CE                                        | 2017 | 3       | 29      | 0           |
| ASUSTek Computer     | H110M-E                                          | 2016 | 3       | 29      | 0           |
| ASUSTek Computer     | B150I PRO GAMING/AURA                            | 2016 | 3       | 29      | 0           |
| Lenovo               | ThinkCentre M93p 10AB0010US                      | 2014 | 3       | 29      | 0           |
| Intel                | D34010WYK H14771-303                             | 2014 | 3       | 29      | 0           |
| Hewlett-Packard      | EliteDesk 700 G1 SFF                             | 2014 | 3       | 29      | 0           |
| Gigabyte Technology  | Z97M-DS3H                                        | 2014 | 3       | 29      | 0           |
| Gigabyte Technology  | J1800N-D2P                                       | 2014 | 3       | 29      | 0           |
| Fujitsu              | PRIMERGY TX1310 M1                               | 2014 | 3       | 29      | 0           |
| Acer                 | Aspire XC-703                                    | 2014 | 3       | 29      | 0           |
| Gigabyte Technology  | C1007UN                                          | 2013 | 3       | 29      | 0           |
| Fujitsu              | ESPRIMO E510                                     | 2013 | 3       | 29      | 0           |
| Biostar              | NM70I-1037U                                      | 2013 | 3       | 29      | 0           |
| Intel                | DB75EN AAG39650-303                              | 2012 | 3       | 29      | 0           |
| Acer                 | Aspire M1935                                     | 2012 | 3       | 29      | 0           |
| ASRock               | B75M-DGS                                         | 2012 | 3       | 29      | 0           |
| Fujitsu              | ESPRIMO Q900                                     | 2011 | 3       | 29      | 0           |
| Acer                 | Veriton Series                                   | 2011 | 3       | 29      | 0           |
| ASUSTek Computer     | P5G41-M LE                                       | 2009 | 3       | 29      | 0           |
| Foxconn              | 945 7MA Series                                   | 2006 | 3       | 29      | 0           |
| ECS                  | 945P-A                                           | 2005 | 3       | 29      | 0           |
| Intel                | D865PERL AAC27648-208                            | 2003 | 3       | 29      | 0           |
| ASUSTek Computer     | PRIME H510M-K                                    | 2021 | 3       | 28      | 0           |
| Gigabyte Technology  | Z390 M                                           | 2019 | 3       | 28      | 0           |
| ASRock               | B365M-HDV                                        | 2019 | 3       | 28      | 0           |
| Minix                | NEO J50C-4                                       | 2018 | 3       | 28      | 0           |
| ASRock               | J4005M                                           | 2018 | 3       | 28      | 0           |
| Lenovo               | ThinkCentre M92p 3209EK4                         | 2017 | 3       | 28      | 0           |
| Biostar              | IH61MF-Q5                                        | 2017 | 3       | 28      | 0           |
| Hewlett-Packard      | 280 G2 MT                                        | 2016 | 3       | 28      | 0           |
| Gigabyte Technology  | B150M-D2V-CF                                     | 2015 | 3       | 28      | 0           |
| Shuttle              | XH81V                                            | 2014 | 3       | 28      | 0           |
| ASRock               | H61M-VS3                                         | 2013 | 3       | 28      | 0           |
| ASRock               | H61M-DGS R2.0                                    | 2013 | 3       | 28      | 0           |
| MSI                  | MS-7677                                          | 2012 | 3       | 28      | 0           |
| Biostar              | NM70I-847                                        | 2012 | 3       | 28      | 0           |
| Foxconn              | H61MXV/H67MXV                                    | 2011 | 3       | 28      | 0           |
| Acer                 | Veriton X2610G                                   | 2011 | 3       | 28      | 0           |
| TYAN Computer        | S3992-E                                          | 2009 | 3       | 28      | 0           |
| Gigabyte Technology  | G31MX-S2                                         | 2007 | 3       | 28      | 0           |
| ECS                  | 945PL-A                                          | 2006 | 3       | 28      | 0           |
| DEPO Computers       | MS-7267                                          | 2006 | 3       | 28      | 0           |
| MSI                  | MS-7030                                          | 2005 | 3       | 28      | 0           |
| ASRock               | K8Upgrade-NF3                                    | 2005 | 3       | 28      | 0           |
| Tekram Technology    | P6B40-A4X-i440BX Rev                             | 2000 | 3       | 28      | 0           |
| ASRock               | H410M-HDV                                        | 2020 | 3       | 27      | 0           |
| Gigabyte Technology  | H310M D3H                                        | 2019 | 3       | 27      | 0           |
| Lenovo               | ThinkCentre M800 10FW001NUS                      | 2016 | 3       | 27      | 0           |
| ASUSTek Computer     | B150I PRO GAMING/WIFI/AURA                       | 2016 | 3       | 27      | 0           |
| Acer                 | Extensa X2610G                                   | 2015 | 3       | 27      | 0           |
| Gigabyte Technology  | GB-BXBT-1900                                     | 2014 | 3       | 27      | 0           |
| Positivo             | POS-EIH61CQ                                      | 2013 | 3       | 27      | 0           |
| Intel                | DH77DF AAG40293-300                              | 2012 | 3       | 27      | 0           |
| Positivo             | POS-EINM10CB                                     | 2011 | 3       | 27      | 0           |
| VS Company           | VS-G31T-M                                        | 2009 | 3       | 27      | 0           |
| Gigabyte Technology  | 945PLM-S2                                        | 2006 | 3       | 27      | 0           |
| ASUSTek Computer     | E520                                             | 2018 | 3       | 26      | 0           |
| ASUSTek Computer     | D520MT_D520SF_D320MT                             | 2016 | 3       | 26      | 0           |
| Jetway               | 1.0                                              | 2015 | 3       | 26      | 0           |
| Positivo             | POS-EIBTPDC                                      | 2014 | 3       | 26      | 0           |
| Megaware             | MW-H61HD-MA                                      | 2013 | 3       | 26      | 0           |
| Intel                | DCP847SKE G80890-105                             | 2013 | 3       | 26      | 0           |
| Intel                | D2700MUD AAG32419-602                            | 2012 | 3       | 26      | 0           |
| Gigabyte Technology  | H61M-DS2V                                        | 2012 | 3       | 26      | 0           |
| Gigabyte Technology  | B75MS                                            | 2017 | 3       | 25      | 0           |
| Biostar              | H110MHC                                          | 2017 | 3       | 25      | 0           |
| Acer                 | Aspire XC-704G                                   | 2015 | 3       | 25      | 0           |
| MSI                  | MS-7836                                          | 2013 | 3       | 25      | 0           |
| Login Informatica    | LOG-H61H2-M2                                     | 2013 | 3       | 25      | 0           |
| ASRock               | H61MV-ITX                                        | 2012 | 3       | 25      | 0           |
| MSI                  | KM400-8235                                       | 2003 | 3       | 25      | 0           |
| PCWare               | IPX4105G Pro                                     | 2018 | 3       | 24      | 0           |
| Gigabyte Technology  | GB-BACE-3000-SBE                                 | 2017 | 3       | 24      | 0           |
| ECS                  | BSWI-D2                                          | 2016 | 3       | 23      | 0           |
| Intel                | D33217GKE G76540-205                             | 2014 | 3       | 23      | 0           |
| Huanan               | X79-ZD3 INTEL (INTEL Xeon E5/Corei7 DMI2 - C6... | 2018 | 2       | 109     | 0           |
| ASUSTek Computer     | ROG Rampage VI EXTREME ENCORE                    | 2020 | 2       | 95      | 0           |
| ASUSTek Computer     | ROG STRIX X299-E GAMING II                       | 2020 | 2       | 91      | 0           |
| Hewlett-Packard      | ProLiant ML350p Gen8                             | 2012 | 2       | 91      | 0           |
| Gigabyte Technology  | X299 DESIGNARE EX                                | 2017 | 2       | 89      | 0           |
| Huanan               | X79 V13                                          | 2017 | 2       | 88      | 0           |
| ASRock               | X79 Extreme4                                     | 2012 | 2       | 88      | 0           |
| Huanan               | X79-ZD3                                          | 2020 | 2       | 86      | 0           |
| Gigabyte Technology  | X299 AORUS Ultra Gaming Pro                      | 2017 | 2       | 86      | 0           |
| Intel                | DX79SR AAG57199-200                              | 2012 | 2       | 84      | 0           |
| Huanan               | Board                                            | 2019 | 2       | 83      | 0           |
| ASUSTek Computer     | WS X299 SAGE/10G                                 | 2018 | 2       | 83      | 0           |
| Fujitsu              | D3128-B2                                         | 2013 | 2       | 83      | 0           |
| KLLISRE              | X79 V2.72S                                       | 2020 | 2       | 82      | 0           |
| Intel                | X79 V2.82                                        | 2018 | 2       | 82      | 0           |
| ASRock               | X299 Steel Legend                                | 2019 | 2       | 81      | 0           |
| MSI                  | MS-7760                                          | 2012 | 2       | 81      | 0           |
| Hewlett-Packard      | ProLiant ML370 G6                                | 2012 | 2       | 81      | 0           |
| Huanan               | X79 249PC V2.3                                   | 2021 | 2       | 80      | 0           |
| Fujitsu              | D3598-B1                                         | 2019 | 2       | 79      | 0           |
| MSI                  | MS-7666                                          | 2011 | 2       | 78      | 0           |
| ASUSTek Computer     | P6X58-E-WS                                       | 2011 | 2       | 78      | 0           |
| Huanan               | X79 249PC V2.1                                   | 2019 | 2       | 76      | 0           |
| Intel                | X79 V2.4F                                        | 2018 | 2       | 76      | 0           |
| Intel                | DX58SO2 AAG10925-207                             | 2011 | 2       | 75      | 0           |
| Alienware            | Area 51                                          | 2009 | 2       | 75      | 0           |
| Intel                | X79M-S                                           | 2019 | 2       | 73      | 0           |
| Gigabyte Technology  | X299 UD4                                         | 2017 | 2       | 73      | 0           |
| Fujitsu              | CELSIUS M770                                     | 2018 | 2       | 72      | 0           |
| Intel                | DX58SO AAE29331-703                              | 2010 | 2       | 72      | 0           |
| Gigabyte Technology  | P55A-UD4P                                        | 2009 | 2       | 72      | 0           |
| Gigabyte Technology  | X58A-OC                                          | 2011 | 2       | 71      | 0           |
| Fujitsu              | CELSIUS M470-2                                   | 2011 | 2       | 70      | 0           |
| Acer                 | Aspire M7720                                     | 2009 | 2       | 67      | 0           |
| ASUSTek Computer     | ProArt X570-CREATOR WIFI                         | 2021 | 2       | 66      | 0           |
| Fujitsu              | CELSIUS M470                                     | 2011 | 2       | 66      | 0           |
| Medion               | IPMTB-GS                                         | 2009 | 2       | 62      | 0           |
| Acer                 | Aspire M7721                                     | 2009 | 2       | 62      | 0           |
| Gigabyte Technology  | H55-USB3                                         | 2010 | 2       | 61      | 0           |
| Gigabyte Technology  | P55M-UD4                                         | 2009 | 2       | 61      | 0           |
| ASUSTek Computer     | P7P55D DELUXE                                    | 2012 | 2       | 60      | 0           |
| Gigabyte Technology  | P55-UD6                                          | 2009 | 2       | 60      | 0           |
| ASUSTek Computer     | ProArt B550-CREATOR                              | 2021 | 2       | 59      | 0           |
| ASRock               | P55 Extreme                                      | 2010 | 2       | 59      | 0           |
| ASUSTek Computer     | ROG CROSSHAIR VIII EXTREME                       | 2021 | 2       | 57      | 0           |
| Supermicro           | X8SIE                                            | 2012 | 2       | 57      | 0           |
| ASRock               | A75 Extreme6                                     | 2013 | 2       | 56      | 0           |
| MSI                  | MS-7581                                          | 2011 | 2       | 55      | 0           |
| Biostar              | TH55XE                                           | 2010 | 2       | 55      | 0           |
| MSI                  | MS-7D13                                          | 2021 | 2       | 54      | 0           |
| ASRock               | X570 Phantom Gaming 4 WiFi ax                    | 2020 | 2       | 54      | 0           |
| JW Technology        | H55M-L V1.0                                      | 2010 | 2       | 54      | 0           |
| Intel                | DH55TC AAE70932-301                              | 2010 | 2       | 54      | 0           |
| ASRock               | P67 Transformer                                  | 2010 | 2       | 54      | 0           |
| Hewlett-Packard      | EliteDesk 705 G4 MT                              | 2019 | 2       | 52      | 0           |
| Medion               | MD34140/2563                                     | 2018 | 2       | 52      | 0           |
| Hewlett-Packard      | p6750de                                          | 2010 | 2       | 52      | 0           |
| Gigabyte Technology  | B460M DS3H AC-Y1                                 | 2020 | 2       | 51      | 0           |
| MSI                  | MS-7B10                                          | 2019 | 2       | 51      | 0           |
| MSI                  | MS-7668                                          | 2010 | 2       | 51      | 0           |
| Acer                 | Veriton S680G                                    | 2010 | 2       | 51      | 0           |
| ASUSTek Computer     | P7H55-V                                          | 2010 | 2       | 51      | 0           |
| MSI                  | MS-7C71                                          | 2020 | 2       | 50      | 0           |
| Hewlett-Packard      | EliteDesk 705 G5 SFF                             | 2019 | 2       | 50      | 0           |
| Gigabyte Technology  | P67A-UD7-B3                                      | 2011 | 2       | 50      | 0           |
| ASUSTek Computer     | TUF GAMING B550-PRO                              | 2021 | 2       | 49      | 0           |
| ASRock               | B550 Steel Legend                                | 2020 | 2       | 49      | 0           |
| Biostar              | B350GT5                                          | 2018 | 2       | 49      | 0           |
| Intel                | DP55WG AAE57269-408                              | 2011 | 2       | 49      | 0           |
| Lenovo               | ThinkStation S20 4105O1U                         | 2010 | 2       | 49      | 0           |
| Gateway              | FX540XG                                          | 2008 | 2       | 49      | 0           |
| Hewlett-Packard      | EliteDesk 705 G4 DM 65W                          | 2020 | 2       | 48      | 0           |
| Gigabyte Technology  | Z490 AORUS PRO AX                                | 2020 | 2       | 48      | 0           |
| MicroElectronics     | G464                                             | 2019 | 2       | 48      | 0           |
| Intel                | DP43BF AAE78171-301                              | 2010 | 2       | 48      | 0           |
| EVGA                 | 132-YW-E178-FTW                                  | 2008 | 2       | 48      | 0           |
| ABIT                 | FP-IN9 SLI                                       | 2007 | 2       | 48      | 0           |
| MSI                  | MS-7C04                                          | 2020 | 2       | 47      | 0           |
| ASUSTek Computer     | Maximus VIII EXTREME                             | 2016 | 2       | 47      | 0           |
| Gigabyte Technology  | 990FXA-UD5 R5                                    | 2015 | 2       | 47      | 0           |
| Intel                | DH55HC AAE70933-502                              | 2011 | 2       | 47      | 0           |
| ASUSTek Computer     | Crosshair III Formula                            | 2011 | 2       | 47      | 0           |
| Fujitsu              | ESPRIMO P9900                                    | 2010 | 2       | 47      | 0           |
| EVGA                 | 132-CK-NF78                                      | 2008 | 2       | 47      | 0           |
| Gigabyte Technology  | Z490 AORUS ELITE                                 | 2020 | 2       | 46      | 0           |
| Hewlett-Packard      | EliteDesk 705 G3 MT                              | 2017 | 2       | 46      | 0           |
| Gigabyte Technology  | Z270X-Gaming 9                                   | 2017 | 2       | 46      | 0           |
| ASUSTek Computer     | SABERTOOTH 990FX/GEN3 R2.0                       | 2013 | 2       | 46      | 0           |
| Gigabyte Technology  | GA-MA790FX-DQ6                                   | 2009 | 2       | 46      | 0           |
| MSI                  | MS-7D07                                          | 2021 | 2       | 45      | 0           |
| Gigabyte Technology  | B560M AORUS PRO AX                               | 2021 | 2       | 45      | 0           |
| ASUSTek Computer     | ROG STRIX Z490-H GAMING                          | 2021 | 2       | 45      | 0           |
| Gigabyte Technology  | Z490 AORUS ULTRA                                 | 2020 | 2       | 45      | 0           |
| ASRock               | H470 Phantom Gaming 4                            | 2020 | 2       | 45      | 0           |
| ASRock               | A520M-ITX/ac                                     | 2020 | 2       | 45      | 0           |
| ASRock               | Z68 Extreme7 Gen3                                | 2012 | 2       | 45      | 0           |
| MSI                  | MS-7375                                          | 2010 | 2       | 45      | 0           |
| ABIT                 | KN9                                              | 2007 | 2       | 45      | 0           |
| MSI                  | MS-7C87                                          | 2020 | 2       | 44      | 0           |
| Hewlett-Packard      | OMEN by Desktop PC 880-p0xx                      | 2020 | 2       | 44      | 0           |
| CCL Computers        | Customised AMD Zen Bundles Motherboard Bundle    | 2019 | 2       | 44      | 0           |
| AZW                  | Gemini X55                                       | 2019 | 2       | 44      | 0           |
| Hewlett-Packard      | 750-267c                                         | 2016 | 2       | 44      | 0           |
| ASRock               | Z68 Professional Gen3                            | 2012 | 2       | 44      | 0           |
| Hewlett-Packard      | p6787c                                           | 2010 | 2       | 44      | 0           |
| ASRock               | AOD790GX/128M                                    | 2010 | 2       | 44      | 0           |
| NCS-Tech             | CK3-A348                                         | 2009 | 2       | 44      | 0           |
| Biostar              | GF8200C M2+                                      | 2009 | 2       | 44      | 0           |
| Intel                | DQ35JO AAD82085-800                              | 2008 | 2       | 44      | 0           |
| ASUSTek Computer     | M3N-H/HDMI                                       | 2008 | 2       | 44      | 0           |
| Dell                 | OptiPlex 7071                                    | 2019 | 2       | 43      | 0           |
| AZW                  | Gemini X45                                       | 2019 | 2       | 43      | 0           |
| Acer                 | Aspire TC-330                                    | 2018 | 2       | 43      | 0           |
| Gigabyte Technology  | Z170-HD3 DDR3                                    | 2017 | 2       | 43      | 0           |
| ASRock               | Z68 Extreme4                                     | 2012 | 2       | 43      | 0           |
| AMD                  | 970A-UD3                                         | 2012 | 2       | 43      | 0           |
| Acer                 | Aspire M1470                                     | 2011 | 2       | 43      | 0           |
| ASUSTek Computer     | M4N72-E                                          | 2011 | 2       | 43      | 0           |
| ASUSTek Computer     | E35M1-M                                          | 2011 | 2       | 43      | 0           |
| ASUSTek Computer     | CG1330                                           | 2010 | 2       | 43      | 0           |
| Fujitsu              | ESPRIMO P1500                                    | 2009 | 2       | 43      | 0           |
| ASUSTek Computer     | P5N32-E SLI                                      | 2009 | 2       | 43      | 0           |
| Intel                | DQ35JO AAD82085-804                              | 2008 | 2       | 43      | 0           |
| ECS                  | P35T-A                                           | 2007 | 2       | 43      | 0           |
| ASUSTek Computer     | P5K Deluxe                                       | 2007 | 2       | 43      | 0           |
| Hewlett-Packard      | OMEN by Obelisk Desktop 875-0xxx                 | 2020 | 2       | 42      | 0           |
| Acidanthera          | MacPro7,1                                        | 2020 | 2       | 42      | 0           |
| Gigabyte Technology  | Z370 AORUS ULTRA GAMING WIFI                     | 2018 | 2       | 42      | 0           |
| ASUSTek Computer     | WS Z390 PRO                                      | 2018 | 2       | 42      | 0           |
| ASUSTek Computer     | Z170-WS                                          | 2016 | 2       | 42      | 0           |
| Positivo             | POS-EAA75DE                                      | 2015 | 2       | 42      | 0           |
| Gigabyte Technology  | Z97X-Gaming GT                                   | 2015 | 2       | 42      | 0           |
| Gigabyte Technology  | Z170N-WIFI-CF                                    | 2015 | 2       | 42      | 0           |
| MSI                  | MS-7866                                          | 2013 | 2       | 42      | 0           |
| ASRock               | A75M                                             | 2012 | 2       | 42      | 0           |
| ASUSTek Computer     | E35M1-M PRO                                      | 2011 | 2       | 42      | 0           |
| ASUSTek Computer     | M4N75TD                                          | 2010 | 2       | 42      | 0           |
| ASUSTek Computer     | M4A88T-I DELUXE                                  | 2010 | 2       | 42      | 0           |
| Clevo                | L390T                                            | 2008 | 2       | 42      | 0           |
| Intel                | DG965RY AAD41691-301                             | 2007 | 2       | 42      | 0           |
| ASUSTek Computer     | P5N32-SLI-Deluxe                                 | 2006 | 2       | 42      | 0           |
| Gigabyte Technology  | B550 GAMING X                                    | 2020 | 2       | 41      | 0           |
| ASUSTek Computer     | TUF GAMING B450M-PRO II                          | 2020 | 2       | 41      | 0           |
| MSI                  | MS-7A75                                          | 2018 | 2       | 41      | 0           |
| ASRock               | Z390 Phantom Gaming SLI                          | 2018 | 2       | 41      | 0           |
| Gigabyte Technology  | AB350M-Gaming 3-CF                               | 2017 | 2       | 41      | 0           |
| Dell                 | Inspiron 3656                                    | 2016 | 2       | 41      | 0           |
| Hewlett-Packard      | 750-116                                          | 2015 | 2       | 41      | 0           |
| Biostar              | Hi-Fi A88ZN                                      | 2015 | 2       | 41      | 0           |
| EVGA                 | 152-HR-E979                                      | 2014 | 2       | 41      | 0           |
| ASUSTek Computer     | E2KM1I-DELUXE                                    | 2014 | 2       | 41      | 0           |
| Gigabyte Technology  | F2A75-D3H                                        | 2013 | 2       | 41      | 0           |
| ASUSTek Computer     | F1A75-M-PRO R2.0                                 | 2012 | 2       | 41      | 0           |
| ASUSTek Computer     | CM1855                                           | 2012 | 2       | 41      | 0           |
| ASRock               | A75M-ITX                                         | 2012 | 2       | 41      | 0           |
| Hewlett-Packard      | p6823w                                           | 2011 | 2       | 41      | 0           |
| ECS                  | A75F-M2                                          | 2011 | 2       | 41      | 0           |
| Gigabyte Technology  | H55M-S2HP                                        | 2010 | 2       | 41      | 0           |
| Supermicro           | H8DI3+                                           | 2009 | 2       | 41      | 0           |
| MSI                  | VR630                                            | 2009 | 2       | 41      | 0           |
| Gigabyte Technology  | GA-MA790GPT-UD3H                                 | 2009 | 2       | 41      | 0           |
| Gateway              | RS780                                            | 2009 | 2       | 41      | 0           |
| ASUSTek Computer     | M4N82 DELUXE                                     | 2009 | 2       | 41      | 0           |
| K-Systems            | MS-7253                                          | 2007 | 2       | 41      | 0           |
| Hewlett-Packard      | EX272AA-ABA a1520n                               | 2006 | 2       | 41      | 0           |
| ASRock               | 775XFire-RAID                                    | 2005 | 2       | 41      | 0           |
| ASRockRack           | X570D4U                                          | 2020 | 2       | 40      | 0           |
| ASRock               | A520M-HDV                                        | 2020 | 2       | 40      | 0           |
| PCWare               | APM-A320G                                        | 2019 | 2       | 40      | 0           |
| Biostar              | X370GT3                                          | 2019 | 2       | 40      | 0           |
| ASUSTek Computer     | PRO A320M-R WI-FI                                | 2019 | 2       | 40      | 0           |
| Gigabyte Technology  | Z270N-WIFI                                       | 2017 | 2       | 40      | 0           |
| Gigabyte Technology  | Z170N-Gaming 5                                   | 2017 | 2       | 40      | 0           |
| DEPO Computers       | MS-7846                                          | 2015 | 2       | 40      | 0           |
| ASRock               | 970 Performance                                  | 2014 | 2       | 40      | 0           |
| Hewlett-Packard      | 20-b010                                          | 2012 | 2       | 40      | 0           |
| ECS                  | Z77H2-A2X Deluxe                                 | 2012 | 2       | 40      | 0           |
| Biostar              | TA990FXE                                         | 2012 | 2       | 40      | 0           |
| Acer                 | Aspire M3985                                     | 2012 | 2       | 40      | 0           |
| ASUSTek Computer     | CM1740                                           | 2012 | 2       | 40      | 0           |
| Hewlett-Packard      | p7-1060br                                        | 2011 | 2       | 40      | 0           |
| Hewlett-Packard      | Elite 7200 MT Business PC                        | 2011 | 2       | 40      | 0           |
| ASUSTek Computer     | F1A55-M                                          | 2011 | 2       | 40      | 0           |
| ASRock               | 890GM Pro3 R2.0                                  | 2011 | 2       | 40      | 0           |
| Gigabyte Technology  | EP43T-USB3                                       | 2010 | 2       | 40      | 0           |
| Packard Bell         | imedia S1710                                     | 2009 | 2       | 40      | 0           |
| Intel                | DP35DP AAD81073-210                              | 2008 | 2       | 40      | 0           |
| Intel                | DP35DP AAD81073-209                              | 2008 | 2       | 40      | 0           |
| Gigabyte Technology  | EP43C-DS3                                        | 2008 | 2       | 40      | 0           |
| Acer                 | Aspire L5100                                     | 2008 | 2       | 40      | 0           |
| Intel                | DG33FB AAD81072-305                              | 2007 | 2       | 40      | 0           |
| Gigabyte Technology  | GA-73VM-S2                                       | 2007 | 2       | 40      | 0           |
| Intel                | DP965LT AAD41694-206                             | 2006 | 2       | 40      | 0           |
| ASUSTek Computer     | PRIME A520M-A                                    | 2020 | 2       | 39      | 0           |
| ASUSTek Computer     | Komplett PC                                      | 2020 | 2       | 39      | 0           |
| Hewlett-Packard      | ENVY TE01-0xxx                                   | 2019 | 2       | 39      | 0           |
| ASUSTek Computer     | PRIME B360M-C                                    | 2019 | 2       | 39      | 0           |
| Medion               | ERAZER X47004 MD34010/C562                       | 2017 | 2       | 39      | 0           |
| Entroware            | Poseidon                                         | 2017 | 2       | 39      | 0           |
| Hewlett-Packard      | 510-a010                                         | 2016 | 2       | 39      | 0           |
| Colorful Technology  | C.A68M-K PLUS V16                                | 2016 | 2       | 39      | 0           |
| Hewlett-Packard      | 23-q018a                                         | 2015 | 2       | 39      | 0           |
| Gigabyte Technology  | H170M-D3H-CF                                     | 2015 | 2       | 39      | 0           |
| Gigabyte Technology  | H110M-S2PV-CF                                    | 2015 | 2       | 39      | 0           |
| ASRock               | Z87 Professional                                 | 2015 | 2       | 39      | 0           |
| Lenovo               | ThinkCentre M78 10BTA00ELM                       | 2014 | 2       | 39      | 0           |
| ASRock               | T48EM1                                           | 2014 | 2       | 39      | 0           |
| Hewlett-Packard      | 500-008er                                        | 2013 | 2       | 39      | 0           |
| Biostar              | A78MD                                            | 2013 | 2       | 39      | 0           |
| ASRock               | Z87 Extreme6/ac                                  | 2013 | 2       | 39      | 0           |
| ASRock               | FM2A78M Pro4+                                    | 2013 | 2       | 39      | 0           |
| ASUSTek Computer     | CM1745                                           | 2012 | 2       | 39      | 0           |
| Hewlett-Packard      | p7-1254                                          | 2011 | 2       | 39      | 0           |
| eMachines            | ET1352                                           | 2010 | 2       | 39      | 0           |
| Hewlett-Packard      | 200-5120br                                       | 2010 | 2       | 39      | 0           |
| ASUSTek Computer     | P5Q Premium                                      | 2010 | 2       | 39      | 0           |
| ASRock               | N68-GE                                           | 2010 | 2       | 39      | 0           |
| Foxconn              | A74ML-K                                          | 2009 | 2       | 39      | 0           |
| ASRock               | P45DE                                            | 2009 | 2       | 39      | 0           |
| Intel                | DG965LV AAD36275-501                             | 2007 | 2       | 39      | 0           |
| ABIT                 | AN52                                             | 2007 | 2       | 39      | 0           |
| Hewlett-Packard      | ENVY TE01-1xxx                                   | 2021 | 2       | 38      | 0           |
| Hewlett-Packard      | OMEN 25L Desktop GT12-0xxx                       | 2020 | 2       | 38      | 0           |
| Hewlett-Packard      | 290 G4 Microtower PC                             | 2020 | 2       | 38      | 0           |
| Colorful Technology  | BATTLE-AX B450M-HD V14                           | 2020 | 2       | 38      | 0           |
| Gigabyte Technology  | Z370 AORUS Gaming 3                              | 2019 | 2       | 38      | 0           |
| Gigabyte Technology  | B365M H                                          | 2019 | 2       | 38      | 0           |
| Gigabyte Technology  | B365M DS3H WIFI                                  | 2019 | 2       | 38      | 0           |
| EVGA                 | 111-CS-E371                                      | 2019 | 2       | 38      | 0           |
| MSI                  | B360 Gaming Aegis 3 8                            | 2018 | 2       | 38      | 0           |
| Gigabyte Technology  | B250M-D3V                                        | 2018 | 2       | 38      | 0           |
| Dell                 | Inspiron 5680                                    | 2018 | 2       | 38      | 0           |
| ASUSTek Computer     | GD30CI                                           | 2017 | 2       | 38      | 0           |
| Gigabyte Technology  | H170-Gaming 3 DDR3                               | 2015 | 2       | 38      | 0           |
| Gigabyte Technology  | H97M-Gaming 3                                    | 2014 | 2       | 38      | 0           |
| Acer                 | Aspire XC-605G                                   | 2014 | 2       | 38      | 0           |
| ASRock               | FM2A78 Pro4+                                     | 2014 | 2       | 38      | 0           |
| Gigabyte Technology  | Z77M-D3H-MVP                                     | 2013 | 2       | 38      | 0           |
| ASRock               | A55M-VS                                          | 2013 | 2       | 38      | 0           |
| Medion               | MS-7667                                          | 2012 | 2       | 38      | 0           |
| Hewlett-Packard      | h9-1000cs                                        | 2012 | 2       | 38      | 0           |
| Gigabyte Technology  | Z68XP-UD5                                        | 2012 | 2       | 38      | 0           |
| Gigabyte Technology  | GA-A75M-D2H                                      | 2011 | 2       | 38      | 0           |
| ASRock               | HM55-MXM                                         | 2011 | 2       | 38      | 0           |
| Packard Bell         | IMEDIA S3810                                     | 2010 | 2       | 38      | 0           |
| Intel                | DP55WG AAE57269-407                              | 2010 | 2       | 38      | 0           |
| Hewlett-Packard      | HPE-410fr                                        | 2010 | 2       | 38      | 0           |
| Gigabyte Technology  | P67A-UD4                                         | 2010 | 2       | 38      | 0           |
| Foxconn              | 2A92                                             | 2010 | 2       | 38      | 0           |
| ASRock               | M3A785GM-LE/128M                                 | 2010 | 2       | 38      | 0           |
| Intel                | DG35EC AAE29266-210                              | 2009 | 2       | 38      | 0           |
| Hewlett-Packard      | VN433AA-ABF p6235fr                              | 2009 | 2       | 38      | 0           |
| Acer                 | Aspire X1300                                     | 2009 | 2       | 38      | 0           |
| Acer                 | Aspire M3201                                     | 2009 | 2       | 38      | 0           |
| ASRock               | N73V-S                                           | 2009 | 2       | 38      | 0           |
| Intel                | DP35DP AAD81073-208                              | 2008 | 2       | 38      | 0           |
| ECS                  | GF8100VM-M5                                      | 2008 | 2       | 38      | 0           |
| Acer                 | Veriton M464                                     | 2008 | 2       | 38      | 0           |
| Acer                 | Aspire M5201                                     | 2008 | 2       | 38      | 0           |
| ASRock               | 4CoreN73PV-HD720p                                | 2008 | 2       | 38      | 0           |
| T-Group              | computer                                         | 2007 | 2       | 38      | 0           |
| ABIT                 | IP35 PRO                                         | 2007 | 2       | 38      | 0           |
| Intel                | D946GZIS AAD66165-301                            | 2006 | 2       | 38      | 0           |
| ASUSTek Computer     | P5WD2-Premium                                    | 2006 | 2       | 38      | 0           |
| Dell                 | Dimension 5000                                   | 2005 | 2       | 38      | 0           |
| ASUSTek Computer     | A8V-MQ                                           | 2005 | 2       | 38      | 0           |
| ASUSTek Computer     | ROG STRIX Z490-I GAMING                          | 2020 | 2       | 37      | 0           |
| Lenovo               | ThinkCentre M93p 10A8S16X0J                      | 2018 | 2       | 37      | 0           |
| Lenovo               | IdeaCentre 510A-15ABR 90GU0007US                 | 2017 | 2       | 37      | 0           |
| Gigabyte Technology  | Z270M-D3H                                        | 2017 | 2       | 37      | 0           |
| Gigabyte Technology  | B250M-HD3                                        | 2017 | 2       | 37      | 0           |
| ASRock               | FM2A58M-VG3+                                     | 2016 | 2       | 37      | 0           |
| Gigabyte Technology  | F2A68HM-D3H                                      | 2015 | 2       | 37      | 0           |
| Biostar              | A68MD PRO                                        | 2015 | 2       | 37      | 0           |
| MSI                  | MS-7930                                          | 2014 | 2       | 37      | 0           |
| Gigabyte Technology  | Z97N-Gaming 5                                    | 2014 | 2       | 37      | 0           |
| ASUSTek Computer     | G30AB                                            | 2013 | 2       | 37      | 0           |
| Packard Bell         | imedia S2110                                     | 2012 | 2       | 37      | 0           |
| MSI                  | MS-7720                                          | 2012 | 2       | 37      | 0           |
| Gigabyte Technology  | Z77-HD4                                          | 2012 | 2       | 37      | 0           |
| eMachines            | EL1360G                                          | 2011 | 2       | 37      | 0           |
| Hewlett-Packard      | p2-1033w                                         | 2011 | 2       | 37      | 0           |
| Acer                 | Predator G5910                                   | 2011 | 2       | 37      | 0           |
| Acer                 | Aspire X1470                                     | 2011 | 2       | 37      | 0           |
| ASRock               | K8A780LM                                         | 2011 | 2       | 37      | 0           |
| Packard Bell         | imedia S3220                                     | 2010 | 2       | 37      | 0           |
| Intel                | DH57JG AAE70930-302                              | 2010 | 2       | 37      | 0           |
| Hewlett-Packard      | HPE-400y                                         | 2010 | 2       | 37      | 0           |
| Acer                 | Veriton M490G                                    | 2010 | 2       | 37      | 0           |
| ASUSTek Computer     | P5P43TD/USB3                                     | 2010 | 2       | 37      | 0           |
| Intel                | DH55TC AAE70932-204                              | 2009 | 2       | 37      | 0           |
| Intel                | DG43GT AAE62768-301                              | 2009 | 2       | 37      | 0           |
| Hewlett-Packard      | VC903AA-ABF p6145fr                              | 2009 | 2       | 37      | 0           |
| Foxconn              | A6VMX                                            | 2009 | 2       | 37      | 0           |
| ASRock               | N68-GS                                           | 2009 | 2       | 37      | 0           |
| ASRock               | A770CrossFire                                    | 2009 | 2       | 37      | 0           |
| MSI                  | MS-7276                                          | 2008 | 2       | 37      | 0           |
| Hewlett-Packard      | KJ378AA-ABA a6430f                               | 2008 | 2       | 37      | 0           |
| ABIT                 | IP35 Pro                                         | 2008 | 2       | 37      | 0           |
| Medion               | MS-7358                                          | 2007 | 2       | 37      | 0           |
| MSI                  | MS-7395                                          | 2007 | 2       | 37      | 0           |
| Fujitsu Siemens      | D2584-A1                                         | 2007 | 2       | 37      | 0           |
| Biostar              | P35D2-A7                                         | 2007 | 2       | 37      | 0           |
| ASUSTek Computer     | P5N-MX                                           | 2007 | 2       | 37      | 0           |
| Intel                | D945GNT AAC96324-402                             | 2006 | 2       | 37      | 0           |
| Intel                | D925XECV2 AAC83685-205                           | 2006 | 2       | 37      | 0           |
| ASUSTek Computer     | M2N8-VMX                                         | 2006 | 2       | 37      | 0           |
| ASRock               | ALiveNF4G-DVI                                    | 2006 | 2       | 37      | 0           |
| ASUSTek Computer     | P5ND2-SLI                                        | 2005 | 2       | 37      | 0           |
| ASRock               | Z490M-ITX/ac                                     | 2020 | 2       | 36      | 0           |
| Olidata              | T6010                                            | 2015 | 2       | 36      | 0           |
| Gigabyte Technology  | B85M-HD3 R4                                      | 2015 | 2       | 36      | 0           |
| Novatech             | PERSONAL COMPUTER                                | 2014 | 2       | 36      | 0           |
| Lenovo               | H50-50 90B7002PRS                                | 2014 | 2       | 36      | 0           |
| Hewlett-Packard      | ProDesk 405 G2 MT                                | 2014 | 2       | 36      | 0           |
| Gigabyte Technology  | GB-BXi3-5010                                     | 2014 | 2       | 36      | 0           |
| ASUSTek Computer     | A78M-E                                           | 2014 | 2       | 36      | 0           |
| ECS                  | H81H3-M3                                         | 2013 | 2       | 36      | 0           |
| ASUSTek Computer     | A55BM-A/USB3                                     | 2013 | 2       | 36      | 0           |
| Philco               | DTC-A55                                          | 2012 | 2       | 36      | 0           |
| Lenovo               | ThinkCentre M58p 3285A1G                         | 2012 | 2       | 36      | 0           |
| Hewlett-Packard      | 120-1136                                         | 2012 | 2       | 36      | 0           |
| Gigabyte Technology  | F2A55-DS3                                        | 2012 | 2       | 36      | 0           |
| ASUSTek Computer     | F2A55-M LK                                       | 2012 | 2       | 36      | 0           |
| MSI                  | MS-7678                                          | 2011 | 2       | 36      | 0           |
| Lenovo               | H405                                             | 2011 | 2       | 36      | 0           |
| Hewlett-Packard      | p6754y                                           | 2011 | 2       | 36      | 0           |
| Gigabyte Technology  | HA65M-UD3H-B3                                    | 2011 | 2       | 36      | 0           |
| Foxconn              | A55MX                                            | 2011 | 2       | 36      | 0           |
| Gigabyte Technology  | P67A-UD3                                         | 2010 | 2       | 36      | 0           |
| Foxconn              | H55MXV-LE                                        | 2010 | 2       | 36      | 0           |
| Compaq               | WE230AA-ABE CQ5315ES                             | 2010 | 2       | 36      | 0           |
| ASUSTek Computer     | P5Q-EM DO                                        | 2010 | 2       | 36      | 0           |
| Packard Bell         | ixtreme M5120                                    | 2009 | 2       | 36      | 0           |
| Hewlett-Packard      | NF335AA-ABZ a6664it                              | 2009 | 2       | 36      | 0           |
| ECS                  | P45T-A2R                                         | 2009 | 2       | 36      | 0           |
| ASUSTek Computer     | M4A78-HTPC                                       | 2009 | 2       | 36      | 0           |
| Intel                | DX38BT AAD85848-503                              | 2008 | 2       | 36      | 0           |
| Hewlett-Packard      | NF623AA-ABZ a6714it                              | 2008 | 2       | 36      | 0           |
| Fujitsu Siemens      | CELSIUS M460                                     | 2008 | 2       | 36      | 0           |
| Acer                 | Veriton X270                                     | 2008 | 2       | 36      | 0           |
| Acer                 | Extensa E420                                     | 2008 | 2       | 36      | 0           |
| ASUSTek Computer     | P5E3                                             | 2008 | 2       | 36      | 0           |
| Medion               | MS-7327                                          | 2007 | 2       | 36      | 0           |
| Intel                | Bearlake Fab D                                   | 2007 | 2       | 36      | 0           |
| ABIT                 | IC7/IC7-G/1.1                                    | 2004 | 2       | 36      | 0           |
| ASUSTek Computer     | TUF GAMING B560M-PLUS WIFI                       | 2021 | 2       | 35      | 0           |
| Dell                 | Vostro 3888                                      | 2020 | 2       | 35      | 0           |
| Dell                 | Inspiron 3471                                    | 2020 | 2       | 35      | 0           |
| MouseComputer        | B360M                                            | 2018 | 2       | 35      | 0           |
| Lenovo               | IdeaCentre 310S-08ASR 90G9007UGE                 | 2018 | 2       | 35      | 0           |
| ASRock               | C2750D4I                                         | 2018 | 2       | 35      | 0           |
| Hewlett-Packard      | EliteDesk 705 G2 SFF                             | 2016 | 2       | 35      | 0           |
| Gigabyte Technology  | F2A68HM-DS2H                                     | 2016 | 2       | 35      | 0           |
| Gigabyte Technology  | Z170M-D3H-CF                                     | 2015 | 2       | 35      | 0           |
| Biostar              | Hi-Fi A68U3P                                     | 2015 | 2       | 35      | 0           |
| ASRock               | Z97 Pro3                                         | 2015 | 2       | 35      | 0           |
| ASRock               | B85M DASH/OL R2.0                                | 2015 | 2       | 35      | 0           |
| Hyrican Informati... | GA-H81M-D2V                                      | 2014 | 2       | 35      | 0           |
| ASRock               | Z87 Extreme3                                     | 2014 | 2       | 35      | 0           |
| ASRock               | QC5000-ITX/WiFi                                  | 2014 | 2       | 35      | 0           |
| ASRock               | FM2A88M-HD+ R2.0                                 | 2014 | 2       | 35      | 0           |
| Hewlett-Packard      | 500-a60                                          | 2013 | 2       | 35      | 0           |
| Hewlett-Packard      | 23-h024                                          | 2013 | 2       | 35      | 0           |
| Fujitsu              | ESPRIMO P510                                     | 2013 | 2       | 35      | 0           |
| Intel                | DZ77SL-50K AAG55115-300                          | 2012 | 2       | 35      | 0           |
| Hewlett-Packard      | p7-1423w                                         | 2012 | 2       | 35      | 0           |
| ASRock               | H61iCafe                                         | 2012 | 2       | 35      | 0           |
| Positivo             | POS-PARS760GCD                                   | 2011 | 2       | 35      | 0           |
| Lenovo               | H330                                             | 2011 | 2       | 35      | 0           |
| Intel                | DH67GD AAG10206-208                              | 2011 | 2       | 35      | 0           |
| Hewlett-Packard      | p2-1113w                                         | 2011 | 2       | 35      | 0           |
| Gigabyte Technology  | PH67-UD3-B3                                      | 2011 | 2       | 35      | 0           |
| ASUSTek Computer     | M4A78LT                                          | 2011 | 2       | 35      | 0           |
| ASRock               | A75 Pro4-M                                       | 2011 | 2       | 35      | 0           |
| Hewlett-Packard      | s5610f                                           | 2010 | 2       | 35      | 0           |
| ECS                  | A780LM-M                                         | 2010 | 2       | 35      | 0           |
| ASUSTek Computer     | CM5671                                           | 2010 | 2       | 35      | 0           |
| ASRock               | 760GM-S3                                         | 2010 | 2       | 35      | 0           |
| MSI                  | MS-7383                                          | 2009 | 2       | 35      | 0           |
| Hewlett-Packard      | NE502AV-ABA a6750t                               | 2009 | 2       | 35      | 0           |
| Gigabyte Technology  | M85M-US2H                                        | 2009 | 2       | 35      | 0           |
| Fujitsu Siemens      | CELSIUS W370                                     | 2009 | 2       | 35      | 0           |
| ECS                  | A780GM-A Ultra                                   | 2009 | 2       | 35      | 0           |
| Compaq               | NR074AA-ABZ CQ5125IT                             | 2009 | 2       | 35      | 0           |
| Biostar              | A780L                                            | 2009 | 2       | 35      | 0           |
| Shuttle              | SG33                                             | 2008 | 2       | 35      | 0           |
| Hewlett-Packard      | FQ565AA-ABA a6700f                               | 2008 | 2       | 35      | 0           |
| Gigabyte Technology  | EG43M-S2H                                        | 2008 | 2       | 35      | 0           |
| ASUSTek Computer     | M2N68                                            | 2008 | 2       | 35      | 0           |
| Hewlett-Packard      | KJ253AA-ABD a6355.de                             | 2007 | 2       | 35      | 0           |
| Dell                 | Dimension 9100                                   | 2006 | 2       | 35      | 0           |
| ASUSTek Computer     | P5GPL                                            | 2005 | 2       | 35      | 0           |
| ABIT                 | IL8                                              | 2005 | 2       | 35      | 0           |
| Gigabyte Technology  | Z490M                                            | 2020 | 2       | 34      | 0           |
| Gigabyte Technology  | Z490 GAMING X                                    | 2020 | 2       | 34      | 0           |
| Acer                 | Veriton EN76G                                    | 2018 | 2       | 34      | 0           |
| Acer                 | Aspire GX-785                                    | 2016 | 2       | 34      | 0           |
| ASUSTek Computer     | UN62                                             | 2016 | 2       | 34      | 0           |
| ASRock               | SKL-NUC                                          | 2016 | 2       | 34      | 0           |
| ASRock               | B150 Gaming K4/Hyper                             | 2016 | 2       | 34      | 0           |
| Lenovo               | ThinkCentre M73 10B00005US                       | 2014 | 2       | 34      | 0           |
| Hewlett-Packard      | 400-314                                          | 2014 | 2       | 34      | 0           |
| Gigabyte Technology  | Z87M-HD3                                         | 2014 | 2       | 34      | 0           |
| Biostar              | A68N-5000                                        | 2014 | 2       | 34      | 0           |
| Acer                 | Aspire TC-606                                    | 2014 | 2       | 34      | 0           |
| ASUSTek Computer     | VM62N                                            | 2014 | 2       | 34      | 0           |
| ASUSTek Computer     | A55BM-E/BR                                       | 2014 | 2       | 34      | 0           |
| ASRock               | H87 Performance                                  | 2014 | 2       | 34      | 0           |
| ASRock               | FM2A58M-HD+ R2.0                                 | 2014 | 2       | 34      | 0           |
| PowerSpec            | B332                                             | 2013 | 2       | 34      | 0           |
| ASRock               | Z87 Pro4                                         | 2013 | 2       | 34      | 0           |
| Hewlett-Packard      | ProLiant ML310e Gen8                             | 2012 | 2       | 34      | 0           |
| CDM                  | DQ77MK-R01                                       | 2012 | 2       | 34      | 0           |
| Biostar              | A55MLC2                                          | 2012 | 2       | 34      | 0           |
| MSI                  | MS-AC71                                          | 2011 | 2       | 34      | 0           |
| Hewlett-Packard      | p6813w                                           | 2011 | 2       | 34      | 0           |
| Hewlett-Packard      | h8-1017c                                         | 2011 | 2       | 34      | 0           |
| Hewlett-Packard      | TouchSmart 7320 Lavaca-B EU L6 PC                | 2011 | 2       | 34      | 0           |
| ECS                  | A780LM-M2                                        | 2011 | 2       | 34      | 0           |
| ECS                  | A55F-M3                                          | 2011 | 2       | 34      | 0           |
| ASUSTek Computer     | M4A87T PLUS                                      | 2011 | 2       | 34      | 0           |
| ASRock               | N68-VGS3 FX                                      | 2011 | 2       | 34      | 0           |
| TYAN Computer        | S8230                                            | 2010 | 2       | 34      | 0           |
| Positivo             | POS-PIG41BA                                      | 2010 | 2       | 34      | 0           |
| ONKYO                | ONKYOPC                                          | 2010 | 2       | 34      | 0           |
| Intel                | DQ45CB AAE30148-302                              | 2010 | 2       | 34      | 0           |
| ITSUMI               | INCOM                                            | 2010 | 2       | 34      | 0           |
| ECS                  | G41T-MR23                                        | 2010 | 2       | 34      | 0           |
| ASUSTek Computer     | AT3IONT-I DELUXE                                 | 2010 | 2       | 34      | 0           |
| ASRock               | N68-S UCC                                        | 2010 | 2       | 34      | 0           |
| Packard Bell         | imedia S3710                                     | 2009 | 2       | 34      | 0           |
| MSI                  | MS-7615                                          | 2009 | 2       | 34      | 0           |
| Intel                | DP45SG AAE27733-404                              | 2009 | 2       | 34      | 0           |
| Foxconn              | A7VML/A76ML Series                               | 2009 | 2       | 34      | 0           |
| Acer                 | Veriton M670G/M670                               | 2009 | 2       | 34      | 0           |
| ASUSTek Computer     | P5KPL/EPU                                        | 2009 | 2       | 34      | 0           |
| ASUSTek Computer     | EB1501                                           | 2009 | 2       | 34      | 0           |
| ASRock               | A785GMH/128M                                     | 2009 | 2       | 34      | 0           |
| Intel                | DG31PR AAD97573-304                              | 2008 | 2       | 34      | 0           |
| Hewlett-Packard      | NC686AA-ABA a6700y                               | 2008 | 2       | 34      | 0           |
| Hewlett-Packard      | KX629AA-ABZ a6561.it                             | 2008 | 2       | 34      | 0           |
| Hewlett-Packard      | FZ116AA-ACP a6551.at                             | 2008 | 2       | 34      | 0           |
| Hewlett-Packard      | FZ083AA-ABF a6643fr                              | 2008 | 2       | 34      | 0           |
| Hewlett-Packard      | FR440AA-ABU a6652uk                              | 2008 | 2       | 34      | 0           |
| Fujitsu Siemens      | ESPRIMO P5625                                    | 2008 | 2       | 34      | 0           |
| Fujitsu Siemens      | ESPRIMO E5925                                    | 2008 | 2       | 34      | 0           |
| Hewlett-Packard      | KE493AA-ABV dx2290MT                             | 2007 | 2       | 34      | 0           |
| Biostar              | P31-A7                                           | 2007 | 2       | 34      | 0           |
| Acer                 | Aspire T690                                      | 2007 | 2       | 34      | 0           |
| Acer                 | Aspire M5630                                     | 2007 | 2       | 34      | 0           |
| ASUSTek Computer     | P5KPL-E                                          | 2007 | 2       | 34      | 0           |
| ASRock               | B365 Phantom Gaming 4                            | 2019 | 2       | 33      | 0           |
| OEGStone             | DQ77MK                                           | 2018 | 2       | 33      | 0           |
| MSI                  | MS-7B19                                          | 2018 | 2       | 33      | 0           |
| Gigabyte Technology  | Q270M-D3H                                        | 2018 | 2       | 33      | 0           |
| ASRock               | Z370 Gaming-ITX/ac                               | 2017 | 2       | 33      | 0           |
| Biostar              | A68N-5100                                        | 2016 | 2       | 33      | 0           |
| ASUSTek Computer     | G20CB                                            | 2016 | 2       | 33      | 0           |
| Gigabyte Technology  | Z170M-D3H DDR3-CF                                | 2015 | 2       | 33      | 0           |
| Gigabyte Technology  | H97M-DS3P                                        | 2015 | 2       | 33      | 0           |
| Hewlett-Packard      | ProDesk 490 G1 MT                                | 2014 | 2       | 33      | 0           |
| Hewlett-Packard      | 500-223w                                         | 2014 | 2       | 33      | 0           |
| Gigabyte Technology  | B85N PHOENIX                                     | 2014 | 2       | 33      | 0           |
| Foxconn              | H81MXV                                           | 2014 | 2       | 33      | 0           |
| Biostar              | H81MLC                                           | 2014 | 2       | 33      | 0           |
| ASRock               | AM1B-MH                                          | 2014 | 2       | 33      | 0           |
| Lenovo               | ThinkCentre M92p 32121Z7                         | 2013 | 2       | 33      | 0           |
| Hewlett-Packard      | 3397                                             | 2013 | 2       | 33      | 0           |
| ECS                  | H87H3-M                                          | 2013 | 2       | 33      | 0           |
| ASRock               | Z77E-ITX                                         | 2013 | 2       | 33      | 0           |
| Standard             | X50V2 Plus PL                                    | 2012 | 2       | 33      | 0           |
| Gigabyte Technology  | Z77-HD3                                          | 2012 | 2       | 33      | 0           |
| Foxconn              | H61M/H61M-S                                      | 2012 | 2       | 33      | 0           |
| DEPO Computers       | XS35                                             | 2012 | 2       | 33      | 0           |
| ASUSTek Computer     | CM1435                                           | 2012 | 2       | 33      | 0           |
| Viglen               | VIG625M                                          | 2010 | 2       | 33      | 0           |
| Intel                | DP67DE AAG10217-205                              | 2010 | 2       | 33      | 0           |
| Hewlett-Packard      | s5630br                                          | 2010 | 2       | 33      | 0           |
| Hewlett-Packard      | p6520y                                           | 2010 | 2       | 33      | 0           |
| Hewlett-Packard      | CQ5700F                                          | 2010 | 2       | 33      | 0           |
| Gateway              | DS10G                                            | 2010 | 2       | 33      | 0           |
| Acer                 | Veriton M480                                     | 2010 | 2       | 33      | 0           |
| ASRock               | A785GXH/128M                                     | 2010 | 2       | 33      | 0           |
| Shuttle              | SA76                                             | 2009 | 2       | 33      | 0           |
| ASUSTek Computer     | CG5270                                           | 2009 | 2       | 33      | 0           |
| eMachines            | EL1200                                           | 2008 | 2       | 33      | 0           |
| WinFast              | NF4UK8AA                                         | 2008 | 2       | 33      | 0           |
| Biostar              | NF520-A2 SE                                      | 2008 | 2       | 33      | 0           |
| Supermicro           | PDSMi                                            | 2007 | 2       | 33      | 0           |
| Hewlett-Packard      | GQ605AA-ABZ a6150.it                             | 2007 | 2       | 33      | 0           |
| Gateway              | T3646                                            | 2007 | 2       | 33      | 0           |
| ECS                  | P965T-A                                          | 2007 | 2       | 33      | 0           |
| ASUSTek Computer     | P5LD2-X/GBL                                      | 2007 | 2       | 33      | 0           |
| Gigabyte Technology  | GA-M55SLI-S4                                     | 2006 | 2       | 33      | 0           |
| ECS                  | RS480-M                                          | 2006 | 2       | 33      | 0           |
| ASUSTek Computer     | P5GPL-X SE                                       | 2006 | 2       | 33      | 0           |
| MSI                  | MS-7093                                          | 2005 | 2       | 33      | 0           |
| Intel                | D945PSN AAC96906-403                             | 2005 | 2       | 33      | 0           |
| ECS                  | nForce4-A939                                     | 2005 | 2       | 33      | 0           |
| ASRock               | B560M Pro4                                       | 2021 | 2       | 32      | 0           |
| ASUSTek Computer     | EX-B365M-V5                                      | 2019 | 2       | 32      | 0           |
| ASUSTek Computer     | EX-B250-V7                                       | 2018 | 2       | 32      | 0           |
| ASUSTek Computer     | B150M-C D3                                       | 2018 | 2       | 32      | 0           |
| ASRock               | Z390 Phantom Gaming 4-IB                         | 2018 | 2       | 32      | 0           |
| ASRock               | B360M-ITX/ac                                     | 2018 | 2       | 32      | 0           |
| ASRock               | B360M Performance                                | 2018 | 2       | 32      | 0           |
| AMD                  | Bettong CRB                                      | 2018 | 2       | 32      | 0           |
| Gigabyte Technology  | H110-D3                                          | 2016 | 2       | 32      | 0           |
| ASUSTek Computer     | Q170T                                            | 2016 | 2       | 32      | 0           |
| Medion               | Akoya P2120 D MD8836/2451                        | 2015 | 2       | 32      | 0           |
| Lenovo               | H50-30g 90AS0001BR                               | 2015 | 2       | 32      | 0           |
| Hewlett-Packard      | ProLiant ML10 v2                                 | 2015 | 2       | 32      | 0           |
| ASRock               | N3150M                                           | 2015 | 2       | 32      | 0           |
| ASRock               | N3150-ITX                                        | 2015 | 2       | 32      | 0           |
| Intel                | DQ87PG AAG74154-403                              | 2014 | 2       | 32      | 0           |
| Hewlett-Packard      | 500-321                                          | 2014 | 2       | 32      | 0           |
| Gigabyte Technology  | B85M-D2V-SI                                      | 2014 | 2       | 32      | 0           |
| Acer                 | Veriton M2631                                    | 2014 | 2       | 32      | 0           |
| ASRock               | H97M-ITX/ac                                      | 2014 | 2       | 32      | 0           |
| APD                  | ALDA+                                            | 2014 | 2       | 32      | 0           |
| Tarox                | Business                                         | 2013 | 2       | 32      | 0           |
| Olivetti             | PA200-30B                                        | 2013 | 2       | 32      | 0           |
| Shuttle              | SZ68R2                                           | 2012 | 2       | 32      | 0           |
| Lenovo               | ThinkCentre M81 5049P14                          | 2012 | 2       | 32      | 0           |
| Gigabyte Technology  | Z68MX-UD2H-B3                                    | 2012 | 2       | 32      | 0           |
| Foxconn              | H77M/H77M-S                                      | 2012 | 2       | 32      | 0           |
| Biostar              | A55MD2                                           | 2012 | 2       | 32      | 0           |
| ASRock               | Z77M                                             | 2012 | 2       | 32      | 0           |
| Wortmann AG          | IPMSB-GS                                         | 2011 | 2       | 32      | 0           |
| Shuttle              | SH67H3                                           | 2011 | 2       | 32      | 0           |
| Shuttle              | SH55J                                            | 2011 | 2       | 32      | 0           |
| JW Technology        | PH67A V1.0                                       | 2011 | 2       | 32      | 0           |
| Intel                | DH67GD AAG10206-206                              | 2011 | 2       | 32      | 0           |
| eMachines            | ET1840                                           | 2010 | 2       | 32      | 0           |
| Hewlett-Packard      | p6740f                                           | 2010 | 2       | 32      | 0           |
| Hewlett-Packard      | ProLiant Micro Server                            | 2010 | 2       | 32      | 0           |
| Gateway              | SX2801                                           | 2010 | 2       | 32      | 0           |
| Compaq               | BM411AA-ABA CQ5600F                              | 2010 | 2       | 32      | 0           |
| ASUSTek Computer     | P5P41T/USB3                                      | 2010 | 2       | 32      | 0           |
| ASUSTek Computer     | M2N68-VM                                         | 2010 | 2       | 32      | 0           |
| WINCOR NIXDORF       | BEETLE                                           | 2009 | 2       | 32      | 0           |
| Intel                | Eaglelake Fab D                                  | 2009 | 2       | 32      | 0           |
| Intel                | DG45FC AAE27730-308                              | 2009 | 2       | 32      | 0           |
| Intel                | D945GCLF2 AAE46416-103                           | 2009 | 2       | 32      | 0           |
| Hewlett-Packard      | Compaq dx2450 Microtower                         | 2009 | 2       | 32      | 0           |
| Hewlett-Packard      | AW009AV-ABA p6200z                               | 2009 | 2       | 32      | 0           |
| Biostar              | MCP6P3                                           | 2009 | 2       | 32      | 0           |
| ASUSTek Computer     | M4A3000E                                         | 2009 | 2       | 32      | 0           |
| ASUSTek Computer     | B206                                             | 2009 | 2       | 32      | 0           |
| ASRock               | A780GM-LE                                        | 2009 | 2       | 32      | 0           |
| RM                   | RM ECOQUIET 630A                                 | 2008 | 2       | 32      | 0           |
| Biostar              | NF61S-M2B                                        | 2007 | 2       | 32      | 0           |
| ASUSTek Computer     | P5B-VM DO                                        | 2007 | 2       | 32      | 0           |
| ABIT                 | NF-M2SV                                          | 2007 | 2       | 32      | 0           |
| Gigabyte Technology  | 8I945PL-G                                        | 2006 | 2       | 32      | 0           |
| Fujitsu Siemens      | CELSIUS M / W                                    | 2006 | 2       | 32      | 0           |
| Foxconn              | 945 7AD Series                                   | 2006 | 2       | 32      | 0           |
| Intel                | D945PLRN AAD32731-404                            | 2005 | 2       | 32      | 0           |
| EPoX Computer        | nForce4 DDR: 9NPA+ / 9NPA+Ultra / 9NPAJ / 9NP... | 2005 | 2       | 32      | 0           |
| Dell                 | Dimension XPS                                    | 2004 | 2       | 32      | 0           |
| ASRock               | H570M Pro4                                       | 2021 | 2       | 31      | 0           |
| JGINYUE              | H97I PLUS V2.0                                   | 2020 | 2       | 31      | 0           |
| INSYS                | ProB                                             | 2019 | 2       | 31      | 0           |
| AZW                  | Gemini T34                                       | 2019 | 2       | 31      | 0           |
| OEGStone             | All Series                                       | 2018 | 2       | 31      | 0           |
| Gigabyte Technology  | E2500N                                           | 2018 | 2       | 31      | 0           |
| Gigabyte Technology  | B360 M AORUS PRO                                 | 2018 | 2       | 31      | 0           |
| ASUSTek Computer     | ROG STRIX H370-F GAMING                          | 2018 | 2       | 31      | 0           |
| ASUSTek Computer     | PRIME H310T                                      | 2018 | 2       | 31      | 0           |
| ASRock               | H97 Performance                                  | 2018 | 2       | 31      | 0           |
| Gigabyte Technology  | H110M-D3H R2                                     | 2017 | 2       | 31      | 0           |
| Dell                 | Vostro 3668                                      | 2017 | 2       | 31      | 0           |
| ASUSTek Computer     | Z170 PRO GAMING/AURA                             | 2016 | 2       | 31      | 0           |
| ASUSTek Computer     | VC65R                                            | 2016 | 2       | 31      | 0           |
| Intel                | DQ77KB AAG81483-500                              | 2015 | 2       | 31      | 0           |
| ECS                  | H81M-C2H                                         | 2015 | 2       | 31      | 0           |
| Acer                 | Revo M1-601                                      | 2015 | 2       | 31      | 0           |
| ASUSTek Computer     | B150-PLUS                                        | 2015 | 2       | 31      | 0           |
| ASRock               | QC5000M-ITX/PH                                   | 2015 | 2       | 31      | 0           |
| Lenovo               | H50-50 90B60081IX                                | 2014 | 2       | 31      | 0           |
| Wibtek               | TH87G-SA                                         | 2013 | 2       | 31      | 0           |
| Samsung Electronics  | Samsung DeskTop System                           | 2013 | 2       | 31      | 0           |
| Biostar              | Hi-Fi Z87X 3D                                    | 2013 | 2       | 31      | 0           |
| Lenovo               | ThinkCentre M82 2929A77                          | 2012 | 2       | 31      | 0           |
| Lenovo               | ThinkCentre Edge71 1607P9G                       | 2012 | 2       | 31      | 0           |
| Fujitsu              | FujitsuTP7000                                    | 2012 | 2       | 31      | 0           |
| ASUSTek Computer     | ET1611PUT                                        | 2012 | 2       | 31      | 0           |
| Intel                | DH67CF AAG10215-207                              | 2011 | 2       | 31      | 0           |
| Hewlett-Packard      | p7-1267c                                         | 2011 | 2       | 31      | 0           |
| Biostar              | H61MHB                                           | 2011 | 2       | 31      | 0           |
| ASRock               | P67 Pro                                          | 2011 | 2       | 31      | 0           |
| NEC Computers        | Express5800/S70 [N8100-9021]                     | 2010 | 2       | 31      | 0           |
| Lenovo               | ThinkCentre M58p 6234DJ1                         | 2010 | 2       | 31      | 0           |
| Foxconn              | G41MD                                            | 2010 | 2       | 31      | 0           |
| Acer                 | Veriton L480                                     | 2009 | 2       | 31      | 0           |
| PCChips              | A15G                                             | 2008 | 2       | 31      | 0           |
| OEM                  | 45CMX/45GMX/45CMX-K                              | 2008 | 2       | 31      | 0           |
| Foxconn              | MCP61M05                                         | 2008 | 2       | 31      | 0           |
| Biostar              | GF7025-M2                                        | 2008 | 2       | 31      | 0           |
| ASRock               | 4Core1600-D800                                   | 2008 | 2       | 31      | 0           |
| MSI                  | MS-7258                                          | 2007 | 2       | 31      | 0           |
| MAXDATA              | Favorit 5000I                                    | 2007 | 2       | 31      | 0           |
| Lanix                | TITAN                                            | 2007 | 2       | 31      | 0           |
| Intel                | D945GCNL AAD97184-102                            | 2007 | 2       | 31      | 0           |
| ASRock               | ConRoe1333-DVI/H                                 | 2007 | 2       | 31      | 0           |
| ASRock               | ALiveNF7G-HDready                                | 2007 | 2       | 31      | 0           |
| MSI                  | MS-7135                                          | 2006 | 2       | 31      | 0           |
| Gigabyte Technology  | 965G-DS3                                         | 2006 | 2       | 31      | 0           |
| ASUSTek Computer     | P5L-VM 1394                                      | 2006 | 2       | 31      | 0           |
| MSI                  | MS-7191                                          | 2005 | 2       | 31      | 0           |
| EPoX Computer        | nForce4 DDR: 9NPA+ / 9NPA+Ultra / 9NPAJ Series   | 2005 | 2       | 31      | 0           |
| EPoX Computer        | nForce4 DDR: 8NPA7I / 8NPAI Series               | 2005 | 2       | 31      | 0           |
| ECS                  | 915PL-A2                                         | 2005 | 2       | 31      | 0           |
| Medion               | MS-7046                                          | 2004 | 2       | 31      | 0           |
| Hewlett-Packard      | dx2000 MT(PL091EA)                               | 2004 | 2       | 31      | 0           |
| Fujitsu Siemens      | SCENIC N / SCENICO N                             | 2004 | 2       | 31      | 0           |
| MSI                  | MS-7D16                                          | 2021 | 2       | 30      | 0           |
| AAEON                | GENE-APL5                                        | 2020 | 2       | 30      | 0           |
| OEGStone             | DQ67OW                                           | 2018 | 2       | 30      | 0           |
| MSI                  | H310 Gaming Nightblade MI3 8                     | 2018 | 2       | 30      | 0           |
| Supermicro           | SYS-5019S-MR                                     | 2017 | 2       | 30      | 0           |
| MSI                  | KBL-U Pro Cubi 3 Silent S                        | 2017 | 2       | 30      | 0           |
| Hewlett-Packard      | ENVY Desktop                                     | 2017 | 2       | 30      | 0           |
| Gigabyte Technology  | MZGLKCP-00                                       | 2017 | 2       | 30      | 0           |
| Logic Supply         | N3160                                            | 2016 | 2       | 30      | 0           |
| Fujitsu              | ESPRIMO Q556                                     | 2016 | 2       | 30      | 0           |
| ASUSTek Computer     | VM65                                             | 2016 | 2       | 30      | 0           |
| Hewlett-Packard      | 410-010                                          | 2015 | 2       | 30      | 0           |
| ASUSTek Computer     | G11CB                                            | 2015 | 2       | 30      | 0           |
| ASRock               | Z97 Extreme3                                     | 2015 | 2       | 30      | 0           |
| Lenovo               | 70A4001LUX ThinkServer TS140                     | 2014 | 2       | 30      | 0           |
| Gigabyte Technology  | GB-BXi5-5200                                     | 2014 | 2       | 30      | 0           |
| ASUSTek Computer     | A58M-E                                           | 2014 | 2       | 30      | 0           |
| Acer                 | Veriton X4630G                                   | 2013 | 2       | 30      | 0           |
| Acer                 | Aspire TC-603                                    | 2013 | 2       | 30      | 0           |
| Wibtek               | H61-M HDMI2                                      | 2012 | 2       | 30      | 0           |
| Pegatron             | IPPSB-DB                                         | 2012 | 2       | 30      | 0           |
| DNS                  | PC-B2105/0145198                                 | 2012 | 2       | 30      | 0           |
| Acer                 | Veriton M290                                     | 2012 | 2       | 30      | 0           |
| ASRock               | H61M-PS2                                         | 2012 | 2       | 30      | 0           |
| RM                   | RM DESKTOP 310                                   | 2011 | 2       | 30      | 0           |
| Intel                | DH61BE AAG14062-204                              | 2011 | 2       | 30      | 0           |
| ASRock               | HM65-MXM                                         | 2011 | 2       | 30      | 0           |
| Supermicro           | X7SPA-HF                                         | 2010 | 2       | 30      | 0           |
| Biostar              | G41D3                                            | 2010 | 2       | 30      | 0           |
| ASUSTek Computer     | M4N68T LE V2                                     | 2010 | 2       | 30      | 0           |
| MSI                  | MS-7527                                          | 2009 | 2       | 30      | 0           |
| MSI                  | MS-6638                                          | 2009 | 2       | 30      | 0           |
| Gigabyte Technology  | EP41T-UD3L                                       | 2009 | 2       | 30      | 0           |
| Acer                 | Veriton L480G                                    | 2009 | 2       | 30      | 0           |
| ASRock               | G41M-VS2                                         | 2009 | 2       | 30      | 0           |
| PC Factory           | G31M-S2L                                         | 2008 | 2       | 30      | 0           |
| Fujitsu Siemens      | ESPRIMO P3510                                    | 2008 | 2       | 30      | 0           |
| ASUSTek Computer     | IP4BL-ME_S                                       | 2008 | 2       | 30      | 0           |
| ASUSTek Computer     | A8R-MVP                                          | 2008 | 2       | 30      | 0           |
| Wortmann AG          | OEM                                              | 2007 | 2       | 30      | 0           |
| Positivo             | MS-7267                                          | 2007 | 2       | 30      | 0           |
| ECS-USA              | GeForce6100PM-M2                                 | 2007 | 2       | 30      | 0           |
| IBM                  | 8215ZCL                                          | 2006 | 2       | 30      | 0           |
| Foxconn              | 945 7AE Series                                   | 2006 | 2       | 30      | 0           |
| LG Electronics       | LG PC                                            | 2005 | 2       | 30      | 0           |
| ASRock               | P4Dual-915GL                                     | 2005 | 2       | 30      | 0           |
| ABIT                 | NF8/NF8-V                                        | 2005 | 2       | 30      | 0           |
| ABIT                 | IS7/IS7-G/IS7-E                                  | 2004 | 2       | 30      | 0           |
| MSI                  | MS-6570                                          | 2003 | 2       | 30      | 0           |
| Dell                 | Dimension 4600                                   | 2003 | 2       | 30      | 0           |
| ASRock               | H310CM-ITX/ac                                    | 2019 | 2       | 29      | 0           |
| MSI                  | MS-7B25                                          | 2018 | 2       | 29      | 0           |
| Gigabyte Technology  | B360M-D2V                                        | 2018 | 2       | 29      | 0           |
| Gigabyte Technology  | B360 AORUS GAMING 3                              | 2018 | 2       | 29      | 0           |
| ASUSTek Computer     | GR8 II                                           | 2018 | 2       | 29      | 0           |
| ASRock               | J4005B-ITX                                       | 2018 | 2       | 29      | 0           |
| PCWare               | IPMH110Pro                                       | 2017 | 2       | 29      | 0           |
| Dell                 | Vostro 3667                                      | 2017 | 2       | 29      | 0           |
| Hewlett-Packard      | 750-129c                                         | 2016 | 2       | 29      | 0           |
| Hewlett-Packard      | 260-a105ng                                       | 2016 | 2       | 29      | 0           |
| Gigabyte Technology  | N3150ND3V                                        | 2016 | 2       | 29      | 0           |
| Gigabyte Technology  | B150M-HD3                                        | 2016 | 2       | 29      | 0           |
| Fujitsu              | ESPRIMO_D756                                     | 2016 | 2       | 29      | 0           |
| ASRock               | J3160DC-ITX                                      | 2016 | 2       | 29      | 0           |
| Lenovo               | ThinkCentre M83 10AHS1CC00                       | 2015 | 2       | 29      | 0           |
| Lenovo               | ThinkCentre M82 29293V4                          | 2014 | 2       | 29      | 0           |
| ASUSTek Computer     | UN42                                             | 2014 | 2       | 29      | 0           |
| Intel                | DH61BF AAG81311-102                              | 2013 | 2       | 29      | 0           |
| Lenovo               | H430 10091                                       | 2012 | 2       | 29      | 0           |
| Intel                | DG41TX AAE78178-303                              | 2010 | 2       | 29      | 0           |
| Fujitsu              | D3041-A1                                         | 2010 | 2       | 29      | 0           |
| eMachines            | EL1320                                           | 2009 | 2       | 29      | 0           |
| Gigabyte Technology  | M68M-S2                                          | 2009 | 2       | 29      | 0           |
| Gigabyte Technology  | GC330UD                                          | 2009 | 2       | 29      | 0           |
| ECS                  | G41T-M2                                          | 2009 | 2       | 29      | 0           |
| Acer                 | Extensa E270                                     | 2009 | 2       | 29      | 0           |
| ASUSTek Computer     | V-P5G41E                                         | 2009 | 2       | 29      | 0           |
| NEC Computers        | POWERMATE_VL280                                  | 2008 | 2       | 29      | 0           |
| Intel                | DG31PR AAD97573-305                              | 2008 | 2       | 29      | 0           |
| ASUSTek Computer     | M2N-VM HDMI                                      | 2008 | 2       | 29      | 0           |
| Intel                | DG31PR AAD97573-203                              | 2007 | 2       | 29      | 0           |
| Dell                 | PowerEdge SC440                                  | 2007 | 2       | 29      | 0           |
| Biostar              | 945GC Micro 775                                  | 2007 | 2       | 29      | 0           |
| WinFast              | 761GXK8MC                                        | 2006 | 2       | 29      | 0           |
| WinFast              | 760GXK8MC                                        | 2005 | 2       | 29      | 0           |
| Fujitsu Siemens      | D1931                                            | 2005 | 2       | 29      | 0           |
| Intel                | H61 V124                                         | 2020 | 2       | 28      | 0           |
| Hewlett-Packard      | EliteDesk 800 G5 TWR                             | 2020 | 2       | 28      | 0           |
| Acer                 | Aspire XC-895                                    | 2020 | 2       | 28      | 0           |
| Lenovo               | ThinkCentre M920t 10SFCTO1WW                     | 2019 | 2       | 28      | 0           |
| Lenovo               | ThinkCentre M920s 10SJ001JPB                     | 2019 | 2       | 28      | 0           |
| Gigabyte Technology  | GA-G41M-ES2L                                     | 2019 | 2       | 28      | 0           |
| Shuttle              | XH310                                            | 2018 | 2       | 28      | 0           |
| Gigabyte Technology  | H310M DS2                                        | 2018 | 2       | 28      | 0           |
| Lenovo               | V520-15IKL Desktop 10NK002NGE                    | 2017 | 2       | 28      | 0           |
| Gigabyte Technology  | MZGLKDP-00                                       | 2017 | 2       | 28      | 0           |
| Dell                 | Inspiron 3252                                    | 2017 | 2       | 28      | 0           |
| MiTAC                | PD14RI                                           | 2016 | 2       | 28      | 0           |
| Hewlett-Packard      | ProDesk 400 G3 MT                                | 2016 | 2       | 28      | 0           |
| Fujitsu              | ESPRIMO_P756                                     | 2016 | 2       | 28      | 0           |
| Acer                 | Veriton X2640G                                   | 2016 | 2       | 28      | 0           |
| ASRock               | J3160-ITX                                        | 2016 | 2       | 28      | 0           |
| Packard Bell         | iMedia S2984                                     | 2015 | 2       | 28      | 0           |
| Medion               | Akoya P2120 D MD8836/2454                        | 2015 | 2       | 28      | 0           |
| Hewlett-Packard      | ProDesk 400 G2.5 SFF                             | 2015 | 2       | 28      | 0           |
| Gigabyte Technology  | H110M-A-CF                                       | 2015 | 2       | 28      | 0           |
| ASRock               | N3150B-ITX                                       | 2015 | 2       | 28      | 0           |
| ASRock               | B85M-DGS                                         | 2015 | 2       | 28      | 0           |
| Acer                 | Veriton X2632G                                   | 2014 | 2       | 28      | 0           |
| Lenovo               | ThinkCentre M93p 10A9000WBP                      | 2013 | 2       | 28      | 0           |
| Intel                | DH77EB AAG39073-305                              | 2013 | 2       | 28      | 0           |
| Intel                | DB85FL AAG89861-203                              | 2013 | 2       | 28      | 0           |
| Fujitsu              | ESPRIMO Q910                                     | 2013 | 2       | 28      | 0           |
| Fujitsu              | ESPRIMO C720                                     | 2013 | 2       | 28      | 0           |
| ASRock               | H61M-HP4                                         | 2013 | 2       | 28      | 0           |
| Lenovo               | ThinkCentre M91p 7033DU1                         | 2012 | 2       | 28      | 0           |
| Intel                | DH67CF AAG10215-204                              | 2012 | 2       | 28      | 0           |
| Foxconn              | AT-7000 Series                                   | 2012 | 2       | 28      | 0           |
| Acer                 | Aspire X1935                                     | 2012 | 2       | 28      | 0           |
| ASRock               | H61M-DG3/USB3                                    | 2012 | 2       | 28      | 0           |
| Lenovo               | H310                                             | 2011 | 2       | 28      | 0           |
| Intel                | DH61CR AAG14064-204                              | 2011 | 2       | 28      | 0           |
| Intel                | D525MW AAE93082-402                              | 2011 | 2       | 28      | 0           |
| Gigabyte Technology  | H61M-D2H                                         | 2011 | 2       | 28      | 0           |
| Acer                 | Veriton N280G                                    | 2011 | 2       | 28      | 0           |
| STSS                 | Flagman WP110                                    | 2010 | 2       | 28      | 0           |
| PCChips              | P49G                                             | 2010 | 2       | 28      | 0           |
| ASUSTek Computer     | AT3IONT-I                                        | 2010 | 2       | 28      | 0           |
| OEM                  | G31MV/G31MV-K FAB                                | 2009 | 2       | 28      | 0           |
| Fujitsu              | PRIMERGY TX100 S1                                | 2009 | 2       | 28      | 0           |
| Foxconn              | G41M/G41M-S/G41M-V                               | 2009 | 2       | 28      | 0           |
| ASUSTek Computer     | VS-G31-VA                                        | 2009 | 2       | 28      | 0           |
| ASUSTek Computer     | P5GC-MX/GBL                                      | 2009 | 2       | 28      | 0           |
| ASUSTek Computer     | P5G41-M LX2/GB/LPT                               | 2009 | 2       | 28      | 0           |
| ASRock               | G41M-VS                                          | 2009 | 2       | 28      | 0           |
| Intel                | DG31PR AAD97573-202                              | 2007 | 2       | 28      | 0           |
| Fujitsu Siemens      | ESPRIMO P3500                                    | 2007 | 2       | 28      | 0           |
| Acer                 | Aspire T660                                      | 2006 | 2       | 28      | 0           |
| ASUSTek Computer     | P5L8L                                            | 2006 | 2       | 28      | 0           |
| Dell                 | Dimension 4700                                   | 2005 | 2       | 28      | 0           |
| Dell                 | Dimension 4600i                                  | 2003 | 2       | 28      | 0           |
| Lenovo               | ThinkCentre M83 10AHS0FM00                       | 2021 | 2       | 27      | 0           |
| Gigabyte Technology  | H410M S2H V3                                     | 2021 | 2       | 27      | 0           |
| Gigabyte Technology  | H410M S2                                         | 2020 | 2       | 27      | 0           |
| Biostar              | H61MHV2                                          | 2020 | 2       | 27      | 0           |
| Gigabyte Technology  | H310M M.2                                        | 2018 | 2       | 27      | 0           |
| Fujitsu              | CELSIUS W580                                     | 2018 | 2       | 27      | 0           |
| ASRock               | H310M-STX/COM                                    | 2018 | 2       | 27      | 0           |
| Biostar              | J3060NH                                          | 2016 | 2       | 27      | 0           |
| APD                  | ALDA-CE                                          | 2016 | 2       | 27      | 0           |
| MSI                  | MS-7877                                          | 2015 | 2       | 27      | 0           |
| Intel                | D33217GKE G76540-202                             | 2015 | 2       | 27      | 0           |
| Hewlett-Packard      | ProDesk 600 G2 MT                                | 2015 | 2       | 27      | 0           |
| EAGLE EYE NETWORKS   | BayTrail-D                                       | 2015 | 2       | 27      | 0           |
| Biostar              | H110MHV3                                         | 2015 | 2       | 27      | 0           |
| ASUSTek Computer     | A_F_K31AN                                        | 2015 | 2       | 27      | 0           |
| ASRock               | B150M-HDV                                        | 2015 | 2       | 27      | 0           |
| Foxconn              | nT-iBT18/nT-iBT19/nT-iBT29 FAB                   | 2014 | 2       | 27      | 0           |
| Biostar              | J1800NH3                                         | 2014 | 2       | 27      | 0           |
| ASRock               | Q1900DC-ITX                                      | 2014 | 2       | 27      | 0           |
| Biostar              | H61MLV3                                          | 2013 | 2       | 27      | 0           |
| Aquarius             | Pro P30 S75                                      | 2013 | 2       | 27      | 0           |
| Lenovo               | H430                                             | 2012 | 2       | 27      | 0           |
| Intel                | DQ67OW AAG12528-309                              | 2012 | 2       | 27      | 0           |
| Hewlett-Packard      | s5-1224                                          | 2012 | 2       | 27      | 0           |
| ASRock               | H71M-DGS                                         | 2012 | 2       | 27      | 0           |
| Lenovo               | ThinkCentre M71e 3167A46                         | 2011 | 2       | 27      | 0           |
| Acer                 | Veriton N282G                                    | 2011 | 2       | 27      | 0           |
| Fujitsu              | ESPRIMO E3521                                    | 2010 | 2       | 27      | 0           |
| Foxconn              | TPS01                                            | 2010 | 2       | 27      | 0           |
| eMachines            | EL1600                                           | 2009 | 2       | 27      | 0           |
| Intel                | DG41KR AAE62839-304                              | 2009 | 2       | 27      | 0           |
| Intel                | D945GCLF AAE27042-405                            | 2009 | 2       | 27      | 0           |
| ASUSTek Computer     | ITX-220                                          | 2009 | 2       | 27      | 0           |
| Gigabyte Technology  | 945GCM-S2                                        | 2007 | 2       | 27      | 0           |
| ECS                  | P4M800PRO-M2                                     | 2006 | 2       | 27      | 0           |
| ASUSTek Computer     | RC410-SB450                                      | 2006 | 2       | 27      | 0           |
| ASUSTek Computer     | P4R8L                                            | 2004 | 2       | 27      | 0           |
| Fujitsu              | D3644-B1 S26361-D3644-B1                         | 2019 | 2       | 26      | 0           |
| Biostar              | H310MHC                                          | 2018 | 2       | 26      | 0           |
| OEM                  | H81U                                             | 2017 | 2       | 26      | 0           |
| Gigabyte Technology  | B150M-Gaming 3                                   | 2017 | 2       | 26      | 0           |
| ASUSTek Computer     | PRIME H110M-P                                    | 2017 | 2       | 26      | 0           |
| Lenovo               | ThinkCentre M600 10G9001MRU                      | 2016 | 2       | 26      | 0           |
| Dell                 | OptiPlex 3046                                    | 2016 | 2       | 26      | 0           |
| AMI                  | Cherry Trail FFD                                 | 2016 | 2       | 26      | 0           |
| Hewlett-Packard      | 251-a123w                                        | 2015 | 2       | 26      | 0           |
| Colorful Technology  | C.Q1900M PRO V20                                 | 2015 | 2       | 26      | 0           |
| PCWare               | IPX1800G1                                        | 2014 | 2       | 26      | 0           |
| ASUSTek Computer     | H61M-CS                                          | 2014 | 2       | 26      | 0           |
| PCWare               | IPMH61R3 8MB                                     | 2012 | 2       | 26      | 0           |
| PCWare               | CAPE7 847                                        | 2012 | 2       | 26      | 0           |
| Lenovo               | ThinkCentre M91 2491A3G                          | 2012 | 2       | 26      | 0           |
| Intel                | DH61CR AAG14064-207                              | 2012 | 2       | 26      | 0           |
| Lenovo               | ThinkCentre M81 5048W4M                          | 2011 | 2       | 26      | 0           |
| Intel                | DH61DL AAG14066-202                              | 2011 | 2       | 26      | 0           |
| Biostar              | H61MH                                            | 2011 | 2       | 26      | 0           |
| PCWare               | IPX525-D3                                        | 2010 | 2       | 26      | 0           |
| Lenovo               | ThinkCentre A58 7522P1U                          | 2010 | 2       | 26      | 0           |
| Biostar              | G31M                                             | 2010 | 2       | 26      | 0           |
| CompuLab             | SBC-FITPC2                                       | 2009 | 2       | 26      | 0           |
| Intel                | D945GCLF AAE27042-400                            | 2008 | 2       | 26      | 0           |
| FLEX Industries      | EMAX 945GC-M2                                    | 2008 | 2       | 26      | 0           |
| Gigabyte Technology  | GA-8S661FXM-775                                  | 2005 | 2       | 26      | 0           |
| Compaq               | Evo D510 SFF                                     | 2002 | 2       | 26      | 0           |
| ASUSTek Computer     | P4PE                                             | 2002 | 2       | 26      | 0           |
| Kraftway             | KWH310                                           | 2020 | 2       | 25      | 0           |
| Lenovo               | IdeaCentre 510A-15ICB 90HV001MUS                 | 2019 | 2       | 25      | 0           |
| ASUSTek Computer     | EX-H310M-V3 R2.0                                 | 2019 | 2       | 25      | 0           |
| Lenovo               | ThinkCentre M910t-N000 10N9CTO1WW                | 2018 | 2       | 25      | 0           |
| Gigabyte Technology  | H110MSTX-HD3                                     | 2018 | 2       | 25      | 0           |
| Lenovo               | ThinkCentre M72e 3598E3U                         | 2017 | 2       | 25      | 0           |
| ASUSTek Computer     | UN45                                             | 2017 | 2       | 25      | 0           |
| ASUSTek Computer     | B150M-K                                          | 2016 | 2       | 25      | 0           |
| Gigabyte Technology  | GB-BSi3-6100                                     | 2015 | 2       | 25      | 0           |
| TONK                 | TN1900                                           | 2014 | 2       | 25      | 0           |
| Hewlett-Packard      | Pro3500 G2 MT PC                                 | 2014 | 2       | 25      | 0           |
| Foxconn              | H61S                                             | 2014 | 2       | 25      | 0           |
| Biostar              | J1800NH2                                         | 2014 | 2       | 25      | 0           |
| Lenovo               | H520e 10159                                      | 2013 | 2       | 25      | 0           |
| Itautec              | Infoway ST-4256                                  | 2013 | 2       | 25      | 0           |
| ITMediaConsult       | Pentino G-Series MT                              | 2013 | 2       | 25      | 0           |
| ECS                  | H67H2-M2                                         | 2013 | 2       | 25      | 0           |
| Fujitsu              | ESPRIMO E400                                     | 2012 | 2       | 25      | 0           |
| SiComputer           | Productiva                                       | 2011 | 2       | 25      | 0           |
| Lenovo               | ThinkCentre M91p 7033A25                         | 2011 | 2       | 25      | 0           |
| Lenovo               | IdeaCentre K330B                                 | 2011 | 2       | 25      | 0           |
| ASRock               | AD510PV                                          | 2010 | 2       | 25      | 0           |
| MSI                  | 0A90                                             | 2007 | 2       | 25      | 0           |
| Intel                | D945GCPE AAD97209-200                            | 2007 | 2       | 25      | 0           |
| ASRock               | 775Twins-HDTV                                    | 2006 | 2       | 25      | 0           |
| MSI                  | MS-7177                                          | 2005 | 2       | 25      | 0           |
| DEPO Computers       | DPH110S                                          | 2019 | 2       | 24      | 0           |
| BESSTAR Tech         | Z83-W                                            | 2017 | 2       | 24      | 0           |
| ASUSTek Computer     | UN45H                                            | 2017 | 2       | 24      | 0           |
| ALDO                 | C2016-BSWI-D2                                    | 2016 | 2       | 24      | 0           |
| DEXP                 | Aquilon O126                                     | 2015 | 2       | 24      | 0           |
| ASRock               | N3000-NUC                                        | 2015 | 2       | 24      | 0           |
| Positivo             | POS-EIBTDB                                       | 2014 | 2       | 24      | 0           |
| QTQD                 | Board                                            | 2013 | 2       | 24      | 0           |
| ASRock               | AD2550B-ITX                                      | 2013 | 2       | 24      | 0           |
| Fujitsu              | ESPRIMO PH300                                    | 2012 | 2       | 24      | 0           |
| Fujitsu              | ESPRIMO P557                                     | 2019 | 2       | 23      | 0           |
| OEM                  | H110                                             | 2017 | 2       | 23      | 0           |
| Acer                 | Veriton N2510G                                   | 2016 | 2       | 23      | 0           |
| NCR                  | 7616-1200-8801                                   | 2013 | 2       | 23      | 0           |
| Lenovo               | ThinkCentre M91p 4518A13                         | 2012 | 2       | 23      | 0           |
| Intel                | D33217CK G76541-301                              | 2012 | 2       | 23      | 0           |
| Foxconn              | H61MXE                                           | 2012 | 2       | 23      | 0           |
| Gateway              | SX2855                                           | 2011 | 2       | 23      | 0           |
| ASUSTek Computer     | P4SGX-MX                                         | 2003 | 2       | 23      | 0           |
| MSI                  | MS-7890                                          | 2015 | 2       | 22      | 0           |
| Lenovo               | 1S7033H45MJDMVVK                                 | 2014 | 2       | 22      | 0           |
| Lenovo               | ThinkCentre M72e 0896A2G                         | 2012 | 2       | 22      | 0           |
| Lenovo               | 62 2124A4P                                       | 2012 | 2       | 22      | 0           |
| Foxconn              | 661MXPlus                                        | 2007 | 2       | 22      | 0           |
| Gigabyte Technology  | GA-7VT600                                        | 2003 | 2       | 21      | 0           |
| MSI                  | MS-6712                                          | 2001 | 2       | 21      | 0           |
| ECS                  | P4VMM2                                           | 2001 | 2       | 21      | 0           |
| Intel                | STK1AW32SC                                       | 2016 | 2       | 20      | 0           |
| Intel                | D33217GKE G76540-204                             | 2013 | 2       | 20      | 0           |
| Gigabyte Technology  | B450M DS3H                                       | 2018 | 208     | 55      | 1           |
| MSI                  | MS-7C02                                          | 2018 | 189     | 52      | 1           |
| ASUSTek Computer     | PRIME A320M-K                                    | 2017 | 185     | 53      | 1           |
| ASUSTek Computer     | M5A97 R2.0                                       | 2012 | 159     | 50      | 1           |
| Dell                 | OptiPlex 780                                     | 2009 | 159     | 43      | 1           |
| Gigabyte Technology  | A320M-S2H                                        | 2017 | 115     | 49      | 1           |
| Gigabyte Technology  | H61M-S1                                          | 2011 | 113     | 35      | 1           |
| ASUSTek Computer     | M5A78L-M LX3                                     | 2012 | 111     | 42      | 1           |
| Hewlett-Packard      | Compaq Elite 8300 SFF                            | 2012 | 107     | 36      | 1           |
| ASUSTek Computer     | PRIME B350-PLUS                                  | 2017 | 106     | 68      | 1           |
| ASUSTek Computer     | P8Z77-V LX                                       | 2012 | 100     | 43      | 1           |
| ASRock               | G31M-S                                           | 2008 | 99      | 37      | 1           |
| Dell                 | OptiPlex 760                                     | 2008 | 94      | 45      | 1           |
| Dell                 | OptiPlex 990                                     | 2011 | 93      | 41      | 1           |
| ASUSTek Computer     | PRIME X370-PRO                                   | 2017 | 90      | 64      | 1           |
| MSI                  | MS-7B89                                          | 2018 | 89      | 60      | 1           |
| ASUSTek Computer     | P5K                                              | 2007 | 85      | 57      | 1           |
| Gigabyte Technology  | H61M-S2PV                                        | 2011 | 81      | 40      | 1           |
| MSI                  | MS-7788                                          | 2011 | 80      | 38      | 1           |
| Dell                 | OptiPlex 745                                     | 2006 | 78      | 41      | 1           |
| Gigabyte Technology  | B450 AORUS M                                     | 2018 | 77      | 54      | 1           |
| MSI                  | MS-7641                                          | 2011 | 77      | 45      | 1           |
| ASRock               | G41M-VS3                                         | 2010 | 75      | 36      | 1           |
| ASUSTek Computer     | P5KPL-AM SE                                      | 2008 | 74      | 37      | 1           |
| MSI                  | MS-7529                                          | 2008 | 74      | 34      | 1           |
| Gigabyte Technology  | X470 AORUS ULTRA GAMING                          | 2018 | 71      | 63      | 1           |
| ASUSTek Computer     | M5A99FX PRO R2.0                                 | 2012 | 71      | 58      | 1           |
| Gigabyte Technology  | G41M-Combo                                       | 2010 | 68      | 44      | 1           |
| ASUSTek Computer     | H110M-R                                          | 2016 | 67      | 34      | 1           |
| Gigabyte Technology  | GA-78LMT-S2P                                     | 2011 | 66      | 43      | 1           |
| ASRock               | 970 Pro3 R2.0                                    | 2012 | 65      | 48      | 1           |
| MSI                  | MS-7C91                                          | 2020 | 64      | 56      | 1           |
| Gigabyte Technology  | Z77-DS3H                                         | 2012 | 64      | 46      | 1           |
| ASUSTek Computer     | PRIME B450-PLUS                                  | 2018 | 61      | 60      | 1           |
| ASUSTek Computer     | P5KPL-AM                                         | 2008 | 61      | 37      | 1           |
| ASUSTek Computer     | P5GC-MX/1333                                     | 2007 | 61      | 34      | 1           |
| MSI                  | MS-7850                                          | 2013 | 60      | 49      | 1           |
| Gigabyte Technology  | GA-78LMT-S2                                      | 2012 | 60      | 45      | 1           |
| ASRock               | B450M Steel Legend                               | 2019 | 58      | 52      | 1           |
| Gigabyte Technology  | H61M-DS2                                         | 2011 | 58      | 34      | 1           |
| Intel                | H61                                              | 2017 | 58      | 32      | 1           |
| ASUSTek Computer     | P5B                                              | 2006 | 56      | 41      | 1           |
| Gigabyte Technology  | B450M S2H                                        | 2018 | 55      | 48      | 1           |
| ASUSTek Computer     | H110M-K                                          | 2016 | 55      | 37      | 1           |
| Dell                 | OptiPlex 380                                     | 2009 | 55      | 34      | 1           |
| Dell                 | XPS 8700                                         | 2013 | 54      | 50      | 1           |
| ASUSTek Computer     | P8H77-V LE                                       | 2012 | 53      | 41      | 1           |
| ASUSTek Computer     | H61M-K                                           | 2013 | 53      | 33      | 1           |
| Gigabyte Technology  | G41M-ES2L                                        | 2009 | 53      | 33      | 1           |
| Gigabyte Technology  | B450 I AORUS PRO WIFI                            | 2018 | 52      | 66      | 1           |
| ASUSTek Computer     | ROG STRIX B350-F GAMING                          | 2017 | 52      | 53      | 1           |
| ASUSTek Computer     | A88XM-A                                          | 2013 | 52      | 45      | 1           |
| Hewlett-Packard      | Compaq 6005 Pro SFF PC                           | 2009 | 52      | 41      | 1           |
| ASRock               | G31M-GS                                          | 2008 | 51      | 43      | 1           |
| ASUSTek Computer     | P5KPL-AM IN/ROEM/SI                              | 2009 | 50      | 35      | 1           |
| ASUSTek Computer     | PRIME B350M-A                                    | 2017 | 49      | 56      | 1           |
| ASUSTek Computer     | P5G41T-M LX2/GB                                  | 2009 | 49      | 35      | 1           |
| ASUSTek Computer     | TUF GAMING B550-PLUS                             | 2020 | 48      | 58      | 1           |
| ASUSTek Computer     | M5A78L-M LX                                      | 2011 | 47      | 41      | 1           |
| Dell                 | OptiPlex 7040                                    | 2015 | 47      | 38      | 1           |
| ASRock               | 970M Pro3                                        | 2015 | 46      | 56      | 1           |
| Gigabyte Technology  | AB350-Gaming 3                                   | 2017 | 46      | 55      | 1           |
| ASUSTek Computer     | P8Z77-V                                          | 2012 | 45      | 52      | 1           |
| MSI                  | MS-7B85                                          | 2018 | 44      | 59      | 1           |
| Gigabyte Technology  | 990FXA-UD3                                       | 2013 | 44      | 58      | 1           |
| ASUSTek Computer     | M5A78L LE                                        | 2011 | 44      | 44      | 1           |
| ASRock               | FM2A68M-DG3+                                     | 2015 | 44      | 43      | 1           |
| ASUSTek Computer     | A68HM-K                                          | 2014 | 44      | 43      | 1           |
| ASRock               | X570 Taichi                                      | 2019 | 43      | 63      | 1           |
| Gigabyte Technology  | GA-870A-UD3                                      | 2009 | 43      | 53      | 1           |
| Gigabyte Technology  | B75M-D3V                                         | 2012 | 43      | 48      | 1           |
| MSI                  | MS-7599                                          | 2009 | 43      | 43      | 1           |
| ASUSTek Computer     | P5KPL-AM EPU                                     | 2009 | 43      | 38      | 1           |
| Gigabyte Technology  | H77M-D3H                                         | 2012 | 43      | 37      | 1           |
| ASUSTek Computer     | ROG CROSSHAIR VII HERO                           | 2018 | 42      | 57      | 1           |
| MSI                  | MS-7A32                                          | 2017 | 42      | 57      | 1           |
| MSI                  | MS-7C56                                          | 2020 | 42      | 52      | 1           |
| ASUSTek Computer     | P8Z77-V LK                                       | 2012 | 42      | 51      | 1           |
| Gigabyte Technology  | Z77-D3H                                          | 2012 | 42      | 49      | 1           |
| ASUSTek Computer     | PRIME B450M-GAMING/BR                            | 2018 | 42      | 48      | 1           |
| Gigabyte Technology  | GA-78LMT-USB3 R2                                 | 2017 | 42      | 47      | 1           |
| MSI                  | MS-7821                                          | 2013 | 42      | 43      | 1           |
| Dell                 | Vostro 200                                       | 2007 | 42      | 42      | 1           |
| ASUSTek Computer     | M2N                                              | 2006 | 42      | 42      | 1           |
| Apple                | MacPro5,1                                        | 2010 | 41      | 105     | 1           |
| ASUSTek Computer     | TUF B450M-PLUS GAMING                            | 2018 | 41      | 56      | 1           |
| Gigabyte Technology  | GA-MA770-UD3                                     | 2008 | 41      | 43      | 1           |
| Hewlett-Packard      | Compaq 8000 Elite SFF PC                         | 2009 | 41      | 39      | 1           |
| Hewlett-Packard      | Compaq dc7900 Small Form Factor                  | 2008 | 41      | 39      | 1           |
| Hewlett-Packard      | Compaq 6200 Pro SFF PC                           | 2011 | 41      | 36      | 1           |
| Gigabyte Technology  | G31M-ES2C                                        | 2008 | 41      | 33      | 1           |
| Gigabyte Technology  | X570 AORUS PRO                                   | 2019 | 40      | 66      | 1           |
| Gigabyte Technology  | GA-970A-UD3                                      | 2011 | 40      | 53      | 1           |
| ASUSTek Computer     | P5K SE                                           | 2007 | 40      | 41      | 1           |
| Hewlett-Packard      | Compaq 6200 Pro MT PC                            | 2011 | 40      | 39      | 1           |
| Kraftway             | GEG                                              | 2004 | 40      | 37      | 1           |
| ASUSTek Computer     | M2N68-AM SE2                                     | 2009 | 40      | 35      | 1           |
| ASUSTek Computer     | P8H67                                            | 2010 | 39      | 51      | 1           |
| Dell                 | OptiPlex 7020                                    | 2014 | 39      | 50      | 1           |
| ASRock               | A320M-HD                                         | 2018 | 39      | 45      | 1           |
| Hewlett-Packard      | Compaq 6000 Pro SFF PC                           | 2009 | 39      | 43      | 1           |
| ASUSTek Computer     | P5G41T-M LX3                                     | 2010 | 39      | 41      | 1           |
| Hewlett-Packard      | ProLiant MicroServer                             | 2010 | 39      | 38      | 1           |
| ASUSTek Computer     | P8Z77-V DELUXE                                   | 2012 | 38      | 49      | 1           |
| ASUSTek Computer     | P8B75-M                                          | 2012 | 38      | 42      | 1           |
| ASUSTek Computer     | M2N-E                                            | 2006 | 38      | 40      | 1           |
| ASUSTek Computer     | P5KPL-CM                                         | 2008 | 38      | 35      | 1           |
| ASRock               | G31M-VS2                                         | 2010 | 38      | 34      | 1           |
| ASRock               | X399 Taichi                                      | 2017 | 37      | 66      | 1           |
| ASRock               | B450 Gaming K4                                   | 2018 | 37      | 53      | 1           |
| Gigabyte Technology  | A320M-H                                          | 2018 | 37      | 49      | 1           |
| Hewlett-Packard      | Compaq Elite 8300 CMT                            | 2012 | 37      | 46      | 1           |
| ASRock               | N68-S3 UCC                                       | 2010 | 37      | 40      | 1           |
| ASUSTek Computer     | P5QPL-AM                                         | 2009 | 37      | 35      | 1           |
| ECS                  | G31T-M7                                          | 2008 | 37      | 33      | 1           |
| Gigabyte Technology  | G31M-S2L                                         | 2007 | 37      | 33      | 1           |
| Gigabyte Technology  | X58A-UD3R                                        | 2009 | 36      | 85      | 1           |
| Hewlett-Packard      | Z600 Workstation                                 | 2009 | 36      | 78      | 1           |
| ASUSTek Computer     | ROG STRIX X570-F GAMING                          | 2019 | 36      | 61      | 1           |
| ASUSTek Computer     | SABERTOOTH 990FX                                 | 2011 | 36      | 51      | 1           |
| Gigabyte Technology  | H77-DS3H                                         | 2012 | 36      | 42      | 1           |
| Gigabyte Technology  | P35-DS3L                                         | 2007 | 36      | 42      | 1           |
| ASUSTek Computer     | B75M-A                                           | 2013 | 36      | 41      | 1           |
| Semp Toshiba         | STI                                              | 2007 | 36      | 39      | 1           |
| MSI                  | MS-7A33                                          | 2017 | 35      | 57      | 1           |
| Gigabyte Technology  | AB350M-DS3H V2                                   | 2018 | 35      | 52      | 1           |
| ASUSTek Computer     | PRIME Z370-A                                     | 2017 | 35      | 48      | 1           |
| ASUSTek Computer     | P8H77-V                                          | 2012 | 35      | 45      | 1           |
| ASUSTek Computer     | P5KC                                             | 2007 | 35      | 43      | 1           |
| Apple                | MacPro3,1                                        | 2008 | 34      | 61      | 1           |
| ASUSTek Computer     | P7P55D                                           | 2009 | 34      | 59      | 1           |
| ASUSTek Computer     | TUF B450-PLUS GAMING                             | 2018 | 34      | 56      | 1           |
| ASUSTek Computer     | M4A87TD/USB3                                     | 2010 | 34      | 54      | 1           |
| ASUSTek Computer     | P5K SE/EPU                                       | 2008 | 34      | 41      | 1           |
| Dell                 | Precision WorkStation T5500                      | 2010 | 33      | 78      | 1           |
| MSI                  | MS-7A40                                          | 2018 | 33      | 53      | 1           |
| ASUSTek Computer     | PRIME Z270-A                                     | 2016 | 33      | 52      | 1           |
| Gigabyte Technology  | 970A-UD3P                                        | 2013 | 33      | 52      | 1           |
| Gigabyte Technology  | Z77X-D3H                                         | 2012 | 33      | 49      | 1           |
| Gigabyte Technology  | GA-970A-D3                                       | 2011 | 33      | 46      | 1           |
| Hewlett-Packard      | Compaq dc7900 Convertible Minitower              | 2008 | 33      | 43      | 1           |
| ASRock               | N68C-GS FX                                       | 2012 | 33      | 40      | 1           |
| ASUSTek Computer     | M5A97 EVO R2.0                                   | 2013 | 32      | 51      | 1           |
| MSI                  | MS-7519                                          | 2008 | 32      | 43      | 1           |
| Gigabyte Technology  | M68M-S2P                                         | 2009 | 32      | 41      | 1           |
| Dell                 | OptiPlex 960                                     | 2008 | 32      | 41      | 1           |
| Hewlett-Packard      | Compaq Pro 6300 MT                               | 2012 | 32      | 39      | 1           |
| ASUSTek Computer     | P8H61-M LX                                       | 2011 | 32      | 37      | 1           |
| Dell                 | Precision T3600                                  | 2012 | 31      | 92      | 1           |
| Intel                | H55                                              | 2015 | 31      | 55      | 1           |
| ASRock               | 970 Extreme4                                     | 2011 | 31      | 55      | 1           |
| ASRock               | B450 Gaming-ITX/ac                               | 2018 | 31      | 51      | 1           |
| Gigabyte Technology  | 970A-D3P                                         | 2013 | 31      | 50      | 1           |
| ASUSTek Computer     | A68HM-PLUS                                       | 2014 | 31      | 49      | 1           |
| Gigabyte Technology  | B85M-D3H                                         | 2013 | 31      | 46      | 1           |
| ASUSTek Computer     | Z170-P                                           | 2015 | 31      | 45      | 1           |
| ASUSTek Computer     | P8P67                                            | 2010 | 31      | 45      | 1           |
| ASUSTek Computer     | P8P67 LE                                         | 2011 | 31      | 44      | 1           |
| Hewlett-Packard      | Compaq Pro 6305 SFF                              | 2012 | 31      | 42      | 1           |
| ASUSTek Computer     | P5QL PRO                                         | 2008 | 31      | 42      | 1           |
| ASUSTek Computer     | M2N-MX SE Plus                                   | 2006 | 31      | 40      | 1           |
| ASUSTek Computer     | P5LD2-SE                                         | 2006 | 31      | 38      | 1           |
| Hewlett-Packard      | Z800 Workstation                                 | 2010 | 30      | 87      | 1           |
| ASUSTek Computer     | M5A99X EVO                                       | 2011 | 30      | 55      | 1           |
| Dell                 | OptiPlex 980                                     | 2010 | 30      | 54      | 1           |
| ASRock               | 990FX Extreme3                                   | 2012 | 30      | 49      | 1           |
| Gigabyte Technology  | F2A68HM-S1                                       | 2014 | 30      | 41      | 1           |
| ASUSTek Computer     | P8B75-M LE                                       | 2012 | 30      | 41      | 1           |
| ASUSTek Computer     | P8B75-M LX                                       | 2012 | 30      | 40      | 1           |
| Gigabyte Technology  | G41MT-S2                                         | 2010 | 30      | 38      | 1           |
| ASUSTek Computer     | M2N68-AM Plus                                    | 2009 | 30      | 38      | 1           |
| Dell                 | OptiPlex 3050                                    | 2017 | 30      | 35      | 1           |
| Gigabyte Technology  | X570 AORUS PRO WIFI                              | 2019 | 29      | 60      | 1           |
| Gigabyte Technology  | B450 AORUS PRO                                   | 2018 | 29      | 53      | 1           |
| Gigabyte Technology  | F2A88XM-D3H                                      | 2013 | 29      | 48      | 1           |
| Dell                 | Precision T1700                                  | 2013 | 29      | 42      | 1           |
| Hewlett-Packard      | Compaq dc5800 Small Form Factor                  | 2008 | 29      | 37      | 1           |
| Gigabyte Technology  | X570 AORUS ULTRA                                 | 2019 | 28      | 59      | 1           |
| ASRock               | AB350M Pro4                                      | 2017 | 28      | 58      | 1           |
| ASUSTek Computer     | P8P67 PRO                                        | 2010 | 28      | 55      | 1           |
| ASUSTek Computer     | ROG STRIX Z390-E GAMING                          | 2018 | 28      | 54      | 1           |
| Gigabyte Technology  | Z77X-UD5H                                        | 2012 | 28      | 51      | 1           |
| Gigabyte Technology  | GA-MA770T-UD3                                    | 2009 | 28      | 45      | 1           |
| Dell                 | XPS 8300                                         | 2011 | 28      | 42      | 1           |
| MSI                  | MS-7808                                          | 2012 | 28      | 40      | 1           |
| Gigabyte Technology  | M61PME-S2P                                       | 2008 | 28      | 40      | 1           |
| Gigabyte Technology  | G41MT-S2PT                                       | 2011 | 28      | 39      | 1           |
| ASUSTek Computer     | M4N68T-M-LE-V2                                   | 2010 | 28      | 39      | 1           |
| Gigabyte Technology  | B85M-HD3                                         | 2013 | 28      | 38      | 1           |
| Hewlett-Packard      | Compaq dc7800 Small Form Factor                  | 2007 | 28      | 38      | 1           |
| Dell                 | OptiPlex 7050                                    | 2017 | 28      | 37      | 1           |
| ASUSTek Computer     | P8H61-MX R2.0                                    | 2012 | 28      | 33      | 1           |
| ASRock               | X570 Phantom Gaming 4                            | 2019 | 27      | 58      | 1           |
| Gigabyte Technology  | B550M DS3H                                       | 2020 | 27      | 51      | 1           |
| ASUSTek Computer     | M5A78L-M LE/USB3                                 | 2015 | 27      | 47      | 1           |
| Gigabyte Technology  | GA-880GM-UD2H                                    | 2010 | 27      | 45      | 1           |
| ASUSTek Computer     | P5K PRO                                          | 2008 | 27      | 42      | 1           |
| Gigabyte Technology  | H81M-S2PV                                        | 2013 | 27      | 41      | 1           |
| Dell                 | Precision WorkStation T3400                      | 2008 | 27      | 41      | 1           |
| ASUSTek Computer     | PRIME B250M-A                                    | 2017 | 27      | 39      | 1           |
| ASUSTek Computer     | M2N-MX                                           | 2006 | 27      | 39      | 1           |
| ASUSTek Computer     | P8H61-M LX2 R2.0                                 | 2012 | 27      | 37      | 1           |
| Gigabyte Technology  | M68MT-S2                                         | 2010 | 27      | 36      | 1           |
| ASUSTek Computer     | P8H61-MX                                         | 2011 | 27      | 34      | 1           |
| Dell                 | OptiPlex 360                                     | 2008 | 27      | 33      | 1           |
| ASUSTek Computer     | P6X58D-E                                         | 2010 | 26      | 81      | 1           |
| ASUSTek Computer     | P7H55-M                                          | 2010 | 26      | 53      | 1           |
| MSI                  | MS-7A39                                          | 2017 | 26      | 50      | 1           |
| ASUSTek Computer     | P8H61                                            | 2011 | 26      | 48      | 1           |
| ASUSTek Computer     | P5Q SE                                           | 2008 | 26      | 42      | 1           |
| Gigabyte Technology  | B250M-DS3H                                       | 2016 | 26      | 41      | 1           |
| ASRock               | 960GM-GS3 FX                                     | 2011 | 26      | 41      | 1           |
| ASUSTek Computer     | M2N-E SLI                                        | 2006 | 26      | 39      | 1           |
| ASUSTek Computer     | P8Z77-M                                          | 2012 | 26      | 38      | 1           |
| ASUSTek Computer     | B75M-PLUS                                        | 2013 | 26      | 36      | 1           |
| Gigabyte Technology  | 945GCM-S2C                                       | 2007 | 26      | 36      | 1           |
| Gigabyte Technology  | H81M-DS2                                         | 2013 | 26      | 34      | 1           |
| ASUSTek Computer     | P8H61-M LX3 PLUS R2.0                            | 2012 | 26      | 34      | 1           |
| ASUSTek Computer     | P6T                                              | 2009 | 25      | 71      | 1           |
| ASRock               | X570M Pro4                                       | 2019 | 25      | 57      | 1           |
| ASUSTek Computer     | P8Z68-V                                          | 2011 | 25      | 55      | 1           |
| MSI                  | MS-7B17                                          | 2018 | 25      | 54      | 1           |
| ASUSTek Computer     | ROG STRIX B450-I GAMING                          | 2018 | 25      | 53      | 1           |
| ASRock               | FM2A68M-HD+                                      | 2014 | 25      | 48      | 1           |
| MSI                  | MS-7978                                          | 2015 | 25      | 46      | 1           |
| Dell                 | Inspiron 660                                     | 2012 | 25      | 43      | 1           |
| Gigabyte Technology  | H310M S2H 2.0                                    | 2018 | 25      | 38      | 1           |
| Gigabyte Technology  | M68MT-S2P                                        | 2010 | 25      | 37      | 1           |
| ASRock               | H81M-VG4 R2.0                                    | 2014 | 25      | 33      | 1           |
| ASUSTek Computer     | M5A88-V EVO                                      | 2011 | 24      | 58      | 1           |
| ASUSTek Computer     | ROG STRIX B550-I GAMING                          | 2020 | 24      | 56      | 1           |
| ASUSTek Computer     | Maximus VIII RANGER                              | 2015 | 24      | 50      | 1           |
| ASUSTek Computer     | M4A89GTD-PRO/USB3                                | 2010 | 24      | 48      | 1           |
| Gigabyte Technology  | B365M DS3H                                       | 2019 | 24      | 46      | 1           |
| Gigabyte Technology  | Z390 UD                                          | 2018 | 24      | 45      | 1           |
| Gigabyte Technology  | F2A68HM-H                                        | 2015 | 24      | 45      | 1           |
| Gigabyte Technology  | GA-780T-D3L                                      | 2012 | 24      | 43      | 1           |
| Hewlett-Packard      | Compaq 6005 Pro MT PC                            | 2009 | 24      | 41      | 1           |
| ASUSTek Computer     | M4N68T-M LE                                      | 2010 | 24      | 40      | 1           |
| Hewlett-Packard      | Compaq dc7800p Small Form Factor                 | 2007 | 24      | 37      | 1           |
| Gigabyte Technology  | P31-DS3L                                         | 2007 | 24      | 36      | 1           |
| Gigabyte Technology  | H110M-H                                          | 2016 | 24      | 35      | 1           |
| ASRock               | G31M-VS                                          | 2009 | 24      | 35      | 1           |
| ASRock               | Q1900M                                           | 2014 | 24      | 34      | 1           |
| ASUSTek Computer     | P5KPL-AM IN/GB                                   | 2009 | 24      | 33      | 1           |
| Hewlett-Packard      | Z820 Workstation                                 | 2012 | 23      | 111     | 1           |
| Gigabyte Technology  | X570 AORUS ELITE WIFI                            | 2019 | 23      | 59      | 1           |
| Hewlett-Packard      | Compaq 8100 Elite CMT PC                         | 2010 | 23      | 56      | 1           |
| MSI                  | MS-7636                                          | 2009 | 23      | 53      | 1           |
| CSL-Computer         | A0000001                                         | 2018 | 23      | 51      | 1           |
| MSI                  | MS-7C51                                          | 2019 | 23      | 48      | 1           |
| ASUSTek Computer     | ROG Maximus XI HERO                              | 2018 | 23      | 48      | 1           |
| Hewlett-Packard      | Z230 Tower Workstation                           | 2013 | 23      | 45      | 1           |
| Dell                 | XPS 8930                                         | 2017 | 23      | 44      | 1           |
| MSI                  | MS-7250                                          | 2006 | 23      | 43      | 1           |
| Dell                 | Inspiron 560                                     | 2009 | 23      | 42      | 1           |
| MSI                  | MS-7895                                          | 2015 | 23      | 41      | 1           |
| ASUSTek Computer     | P8H77-M PRO                                      | 2012 | 23      | 41      | 1           |
| MSI                  | MS-7596                                          | 2009 | 23      | 41      | 1           |
| ASUSTek Computer     | P5QL/EPU                                         | 2009 | 23      | 41      | 1           |
| MSI                  | MS-7623                                          | 2010 | 23      | 40      | 1           |
| Gigabyte Technology  | H81M-H                                           | 2013 | 23      | 36      | 1           |
| ASUSTek Computer     | H110M-A/M.2                                      | 2016 | 23      | 35      | 1           |
| MSI                  | MS-7267                                          | 2006 | 23      | 35      | 1           |
| Gigabyte Technology  | H110M-S2                                         | 2016 | 23      | 34      | 1           |
| ASRock               | H61M-VG3                                         | 2012 | 23      | 33      | 1           |
| ASUSTek Computer     | P9X79                                            | 2012 | 22      | 89      | 1           |
| ASUSTek Computer     | P7H55                                            | 2010 | 22      | 61      | 1           |
| Gigabyte Technology  | AB350M-Gaming 3                                  | 2017 | 22      | 50      | 1           |
| ASUSTek Computer     | M4A88TD-V EVO/USB3                               | 2010 | 22      | 48      | 1           |
| ASRock               | B450M Pro4-F                                     | 2019 | 22      | 46      | 1           |
| Hewlett-Packard      | EliteDesk 800 G1 TWR                             | 2013 | 22      | 46      | 1           |
| ASUSTek Computer     | P8Z68-V PRO GEN3                                 | 2011 | 22      | 46      | 1           |
| ASUSTek Computer     | P8H67-M LE                                       | 2011 | 22      | 46      | 1           |
| Biostar              | A320MH                                           | 2018 | 22      | 44      | 1           |
| MSI                  | MS-7851                                          | 2013 | 22      | 44      | 1           |
| ASUSTek Computer     | ROG STRIX Z390-F GAMING                          | 2018 | 22      | 43      | 1           |
| Gigabyte Technology  | Z97X-Gaming 3                                    | 2014 | 22      | 42      | 1           |
| ASRock               | Z77 Pro3                                         | 2012 | 22      | 42      | 1           |
| Gigabyte Technology  | P43-ES3G                                         | 2009 | 22      | 41      | 1           |
| ASRock               | N68-VS3 FX                                       | 2011 | 22      | 39      | 1           |
| MSI                  | MS-7360                                          | 2007 | 22      | 39      | 1           |
| ASRock               | 960GM-VGS3 FX                                    | 2012 | 22      | 38      | 1           |
| ASUSTek Computer     | P5KPL-SE                                         | 2008 | 22      | 38      | 1           |
| Dell                 | OptiPlex 3040                                    | 2016 | 22      | 35      | 1           |
| Dell                 | OptiPlex 3060                                    | 2018 | 22      | 34      | 1           |
| Gigabyte Technology  | 945GZM-S2                                        | 2007 | 22      | 33      | 1           |
| Gigabyte Technology  | X570 UD                                          | 2019 | 21      | 57      | 1           |
| Gigabyte Technology  | Z68X-UD3H-B3                                     | 2011 | 21      | 50      | 1           |
| Gigabyte Technology  | B450M DS3H V2                                    | 2020 | 21      | 48      | 1           |
| ASUSTek Computer     | TUF B450-PRO GAMING                              | 2019 | 21      | 48      | 1           |
| ASUSTek Computer     | P5Q DELUXE                                       | 2008 | 21      | 48      | 1           |
| ASUSTek Computer     | M4A78T-E                                         | 2009 | 21      | 45      | 1           |
| Gigabyte Technology  | P75-D3                                           | 2012 | 21      | 44      | 1           |
| Gigabyte Technology  | P67A-D3-B3                                       | 2011 | 21      | 43      | 1           |
| ASRock               | 960GC-GS FX                                      | 2013 | 21      | 42      | 1           |
| ASUSTek Computer     | M5A78L-M LX V2                                   | 2011 | 21      | 42      | 1           |
| ASUSTek Computer     | M2N-SLI DELUXE                                   | 2006 | 21      | 42      | 1           |
| ASUSTek Computer     | M3A78-EM                                         | 2008 | 21      | 41      | 1           |
| ASUSTek Computer     | PRIME Z270-P                                     | 2016 | 21      | 39      | 1           |
| ASUSTek Computer     | M5A78L-M LE                                      | 2011 | 21      | 39      | 1           |
| Fujitsu              | ESPRIMO P720                                     | 2013 | 21      | 37      | 1           |
| ASUSTek Computer     | P5G41C-M LX                                      | 2010 | 21      | 36      | 1           |
| Gigabyte Technology  | M56S-S3                                          | 2007 | 21      | 36      | 1           |
| Hewlett-Packard      | Compaq dc7700 Small Form Factor                  | 2006 | 21      | 32      | 1           |
| ASRock               | H470M-HVS                                        | 2021 | 21      | 30      | 1           |
| ASUSTek Computer     | Rampage IV EXTREME                               | 2012 | 20      | 95      | 1           |
| Gigabyte Technology  | P55-USB3                                         | 2010 | 20      | 66      | 1           |
| Gigabyte Technology  | H55M-UD2H                                        | 2009 | 20      | 65      | 1           |
| Dell                 | Studio XPS 8100                                  | 2009 | 20      | 59      | 1           |
| Gigabyte Technology  | X470 AORUS GAMING 7 WIFI                         | 2018 | 20      | 54      | 1           |
| ASUSTek Computer     | P5Q3                                             | 2009 | 20      | 53      | 1           |
| ASRock               | B550M Pro4                                       | 2020 | 20      | 51      | 1           |
| ASUSTek Computer     | Crosshair IV Formula                             | 2010 | 20      | 50      | 1           |
| Hewlett-Packard      | Pavilion Desktop PC 570-p0xx                     | 2017 | 20      | 49      | 1           |
| ASRock               | AB350 Gaming-ITX/ac                              | 2018 | 20      | 48      | 1           |
| ASRock               | A320M-HDV                                        | 2017 | 20      | 48      | 1           |
| Gigabyte Technology  | GA-890GPA-UD3H                                   | 2010 | 20      | 47      | 1           |
| Gigabyte Technology  | H97M-D3H                                         | 2014 | 20      | 46      | 1           |
| ASUSTek Computer     | M5A97 PRO                                        | 2011 | 20      | 45      | 1           |
| MSI                  | MS-7793                                          | 2012 | 20      | 44      | 1           |
| Gigabyte Technology  | Z97-HD3                                          | 2014 | 20      | 43      | 1           |
| ASUSTek Computer     | P5B-VM SE                                        | 2007 | 20      | 43      | 1           |
| Gigabyte Technology  | GA-MA785GM-US2H                                  | 2009 | 20      | 42      | 1           |
| Gigabyte Technology  | P35-S3G                                          | 2007 | 20      | 42      | 1           |
| MSI                  | MS-7846                                          | 2013 | 20      | 40      | 1           |
| Gigabyte Technology  | F2A88XM-HD3                                      | 2013 | 20      | 40      | 1           |
| ASRock               | N68-VS3 UCC                                      | 2011 | 20      | 40      | 1           |
| Gigabyte Technology  | H61M-D2-B3                                       | 2011 | 20      | 38      | 1           |
| Gigabyte Technology  | GA-MA74GM-S2H                                    | 2008 | 20      | 38      | 1           |
| Hewlett-Packard      | 500B Microtower                                  | 2010 | 20      | 36      | 1           |
| MSI                  | MS-7798                                          | 2012 | 20      | 35      | 1           |
| ASUSTek Computer     | P8H61-M LX3                                      | 2012 | 20      | 33      | 1           |
| Gigabyte Technology  | H61M-S2-B3                                       | 2011 | 20      | 30      | 1           |
| ECS                  | H61H2-M12                                        | 2011 | 20      | 28      | 1           |
| ASUSTek Computer     | SABERTOOTH X58                                   | 2010 | 19      | 76      | 1           |
| ASUSTek Computer     | ROG STRIX B550-E GAMING                          | 2020 | 19      | 68      | 1           |
| ASRock               | B450 Steel Legend                                | 2019 | 19      | 60      | 1           |
| System76             | Thelio                                           | 2018 | 19      | 60      | 1           |
| ASUSTek Computer     | TUF X470-PLUS GAMING                             | 2018 | 19      | 55      | 1           |
| ASRock               | X570 Pro4                                        | 2019 | 19      | 54      | 1           |
| MSI                  | MS-7A37                                          | 2017 | 19      | 54      | 1           |
| Gigabyte Technology  | AX370-Gaming 5                                   | 2017 | 19      | 54      | 1           |
| Dell                 | Precision WorkStation T7400                      | 2008 | 19      | 52      | 1           |
| Gigabyte Technology  | GA-990XA-UD3                                     | 2011 | 19      | 46      | 1           |
| ASUSTek Computer     | P8Z68-V PRO                                      | 2011 | 19      | 46      | 1           |
| ASUSTek Computer     | P5QL-E                                           | 2008 | 19      | 46      | 1           |
| ASRock               | X300M-STX                                        | 2020 | 19      | 44      | 1           |
| Gigabyte Technology  | F2A68HM-DS2                                      | 2014 | 19      | 44      | 1           |
| Gigabyte Technology  | EP43-UD3L                                        | 2009 | 19      | 43      | 1           |
| Gigabyte Technology  | Z390 AORUS PRO WIFI                              | 2018 | 19      | 42      | 1           |
| Acer                 | Aspire XC-830                                    | 2018 | 19      | 42      | 1           |
| Gigabyte Technology  | Z77P-D3                                          | 2012 | 19      | 40      | 1           |
| ASUSTek Computer     | P5QC                                             | 2008 | 19      | 40      | 1           |
| ASRock               | N68-GS4 FX R2.0                                  | 2015 | 19      | 39      | 1           |
| Gigabyte Technology  | GA-MA770T-UD3P                                   | 2009 | 19      | 39      | 1           |
| Gigabyte Technology  | B75M-D2V                                         | 2012 | 19      | 38      | 1           |
| Dell                 | Precision Tower 3620                             | 2017 | 19      | 36      | 1           |
| Gigabyte Technology  | Z77M-D3H                                         | 2012 | 19      | 36      | 1           |
| Gigabyte Technology  | M61PME-S2                                        | 2008 | 19      | 36      | 1           |
| ASUSTek Computer     | P5LD2-VM                                         | 2005 | 19      | 36      | 1           |
| ASUSTek Computer     | P5G41T-M LE                                      | 2009 | 19      | 35      | 1           |
| Hewlett-Packard      | Compaq 8000 Elite USDT PC                        | 2009 | 19      | 34      | 1           |
| Gigabyte Technology  | 945GCM-S2L                                       | 2007 | 19      | 33      | 1           |
| ASUSTek Computer     | P5GC-MX                                          | 2007 | 19      | 33      | 1           |
| ASRock               | H61M-DGS                                         | 2012 | 19      | 31      | 1           |
| Dell                 | Precision 5820 Tower                             | 2018 | 18      | 88      | 1           |
| Supermicro           | X8STi                                            | 2010 | 18      | 72      | 1           |
| ASUSTek Computer     | ROG ZENITH EXTREME                               | 2017 | 18      | 68      | 1           |
| MSI                  | MS-7B78                                          | 2018 | 18      | 63      | 1           |
| ASUSTek Computer     | P7H55-M PRO                                      | 2009 | 18      | 57      | 1           |
| ASRock               | B550M Steel Legend                               | 2020 | 18      | 54      | 1           |
| ASRock               | 970 Extreme3                                     | 2012 | 18      | 54      | 1           |
| ASUSTek Computer     | STRIX Z270E GAMING                               | 2016 | 18      | 52      | 1           |
| Gigabyte Technology  | B550 AORUS ELITE V2                              | 2020 | 18      | 51      | 1           |
| ASUSTek Computer     | P5W DH Deluxe                                    | 2007 | 18      | 50      | 1           |
| ASUSTek Computer     | P5K-E                                            | 2007 | 18      | 47      | 1           |
| MSI                  | MS-7918                                          | 2014 | 18      | 46      | 1           |
| Gigabyte Technology  | F2A88X-D3H                                       | 2013 | 18      | 46      | 1           |
| Dell                 | Inspiron 620                                     | 2011 | 18      | 45      | 1           |
| ASUSTek Computer     | EX-A320M-GAMING                                  | 2018 | 18      | 44      | 1           |
| Gigabyte Technology  | M720-US3                                         | 2009 | 18      | 44      | 1           |
| Gigabyte Technology  | B360M-DS3H                                       | 2018 | 18      | 42      | 1           |
| ASUSTek Computer     | M4A78                                            | 2009 | 18      | 42      | 1           |
| ASUSTek Computer     | M4A78LT-M                                        | 2010 | 18      | 41      | 1           |
| Gigabyte Technology  | GA-MA74GM-S2                                     | 2008 | 18      | 41      | 1           |
| Gigabyte Technology  | EP45-DS3L                                        | 2008 | 18      | 41      | 1           |
| ASUSTek Computer     | M3A78-CM                                         | 2008 | 18      | 38      | 1           |
| Hewlett-Packard      | ProLiant MicroServer Gen8                        | 2013 | 18      | 36      | 1           |
| Hewlett-Packard      | EliteDesk 800 G1 USDT                            | 2013 | 18      | 36      | 1           |
| MSI                  | MS-7752                                          | 2012 | 18      | 36      | 1           |
| ECS                  | H61H2-MV                                         | 2012 | 18      | 35      | 1           |
| Hewlett-Packard      | EliteDesk 800 G2 SFF                             | 2016 | 18      | 34      | 1           |
| Biostar              | N68S3B                                           | 2010 | 18      | 34      | 1           |
| Hewlett-Packard      | Compaq 8200 Elite USDT PC                        | 2011 | 18      | 33      | 1           |
| Gigabyte Technology  | H110M-S2V                                        | 2016 | 18      | 32      | 1           |
| Biostar              | G41D3C                                           | 2010 | 18      | 32      | 1           |
| ASUSTek Computer     | P8H61-M LX R2.0                                  | 2012 | 18      | 31      | 1           |
| ASRock               | H61M-VS                                          | 2011 | 18      | 31      | 1           |
| Intel                | Thurley                                          | 2009 | 17      | 72      | 1           |
| ASUSTek Computer     | TUF GAMING X570-PRO                              | 2020 | 17      | 60      | 1           |
| Dell                 | Inspiron 580                                     | 2010 | 17      | 56      | 1           |
| ASUSTek Computer     | P5E                                              | 2007 | 17      | 49      | 1           |
| Gigabyte Technology  | B450M GAMING                                     | 2018 | 17      | 48      | 1           |
| Gigabyte Technology  | Z390 AORUS PRO                                   | 2018 | 17      | 47      | 1           |
| Gigabyte Technology  | GA-MA78GM-S2H                                    | 2008 | 17      | 47      | 1           |
| MSI                  | MS-7B51                                          | 2018 | 17      | 45      | 1           |
| ASRock               | M3A770DE                                         | 2009 | 17      | 43      | 1           |
| ASUSTek Computer     | P5Q SE PLUS                                      | 2008 | 17      | 43      | 1           |
| MSI                  | MS-7972                                          | 2015 | 17      | 42      | 1           |
| ASUSTek Computer     | H110M-C                                          | 2015 | 17      | 42      | 1           |
| Medion               | MS-7502                                          | 2007 | 17      | 42      | 1           |
| ASUSTek Computer     | P5K-VM                                           | 2007 | 17      | 41      | 1           |
| ASUSTek Computer     | P8H77-M                                          | 2012 | 17      | 40      | 1           |
| Gigabyte Technology  | GA-MA69VM-S2                                     | 2007 | 17      | 39      | 1           |
| Gigabyte Technology  | EP35-DS3                                         | 2007 | 17      | 39      | 1           |
| ASUSTek Computer     | Z170I PRO GAMING                                 | 2015 | 17      | 38      | 1           |
| Gigabyte Technology  | H81M-S2H                                         | 2014 | 17      | 38      | 1           |
| Hewlett-Packard      | EliteDesk 800 G1 DM                              | 2014 | 17      | 36      | 1           |
| MSI                  | MS-7994                                          | 2015 | 17      | 35      | 1           |
| Gigabyte Technology  | P31-ES3G                                         | 2008 | 17      | 35      | 1           |
| ASUSTek Computer     | P5L-MX                                           | 2006 | 17      | 33      | 1           |
| Positivo             | POS-EIH61CE                                      | 2011 | 17      | 29      | 1           |
| ASUSTek Computer     | X79-DELUXE                                       | 2013 | 16      | 100     | 1           |
| Gigabyte Technology  | P55A-UD3                                         | 2009 | 16      | 67      | 1           |
| MSI                  | MS-7681                                          | 2011 | 16      | 58      | 1           |
| ASUSTek Computer     | P5Q-E                                            | 2008 | 16      | 58      | 1           |
| Gigabyte Technology  | Z97X-SLI                                         | 2014 | 16      | 52      | 1           |
| Gigabyte Technology  | F2A78M-HD2                                       | 2014 | 16      | 52      | 1           |
| Gigabyte Technology  | F2A88XM-D3HP                                     | 2015 | 16      | 51      | 1           |
| MSI                  | MS-7B87                                          | 2019 | 16      | 50      | 1           |
| Dell                 | Precision WorkStation 490                        | 2006 | 16      | 50      | 1           |
| Gigabyte Technology  | Z390 AORUS MASTER                                | 2018 | 16      | 49      | 1           |
| ASRock               | A75M-HVS                                         | 2011 | 16      | 49      | 1           |
| Gigabyte Technology  | B150M-D3H                                        | 2016 | 16      | 48      | 1           |
| ASUSTek Computer     | M4A87TD EVO                                      | 2010 | 16      | 47      | 1           |
| MSI                  | MS-7B22                                          | 2018 | 16      | 45      | 1           |
| ASRock               | H97M Pro4                                        | 2014 | 16      | 45      | 1           |
| ASUSTek Computer     | STRIX Z270F GAMING                               | 2017 | 16      | 44      | 1           |
| ASUSTek Computer     | A58M-K                                           | 2014 | 16      | 42      | 1           |
| ASRock               | N68-GS3 UCC                                      | 2010 | 16      | 38      | 1           |
| ASUSTek Computer     | M2N-SLI                                          | 2007 | 16      | 37      | 1           |
| HARDKERNEL           | ODROID-H2                                        | 2018 | 16      | 36      | 1           |
| Gigabyte Technology  | B85M-DS3H-A                                      | 2015 | 16      | 36      | 1           |
| Hewlett-Packard      | Pro 3400 Series MT                               | 2011 | 16      | 35      | 1           |
| Dell                 | OptiPlex GX520                                   | 2005 | 16      | 35      | 1           |
| ASUSTek Computer     | P5KPL                                            | 2007 | 16      | 33      | 1           |
| ASUSTek Computer     | PRIME H310M-K                                    | 2018 | 16      | 32      | 1           |
| ECS                  | H61H2-M13                                        | 2011 | 16      | 32      | 1           |
| ECS                  | G41T-M7                                          | 2011 | 16      | 32      | 1           |
| ASUSTek Computer     | P5N-D                                            | 2007 | 15      | 63      | 1           |
| Gigabyte Technology  | AX370-Gaming K7                                  | 2017 | 15      | 56      | 1           |
| Gigabyte Technology  | AB350-Gaming                                     | 2017 | 15      | 53      | 1           |
| ASUSTek Computer     | ROG STRIX X470-I GAMING                          | 2018 | 15      | 52      | 1           |
| ASRock               | FM2A88X Extreme6+                                | 2013 | 15      | 50      | 1           |
| Gigabyte Technology  | GA-880GA-UD3H                                    | 2010 | 15      | 50      | 1           |
| Gigabyte Technology  | H81M-D2V                                         | 2013 | 15      | 49      | 1           |
| ASUSTek Computer     | P8Z68-V GEN3                                     | 2011 | 15      | 46      | 1           |
| Gigabyte Technology  | Z97X-UD3H                                        | 2014 | 15      | 45      | 1           |
| ASUSTek Computer     | M4A785-M                                         | 2009 | 15      | 44      | 1           |
| Gigabyte Technology  | Z370 HD3                                         | 2017 | 15      | 43      | 1           |
| ASUSTek Computer     | M11BB                                            | 2013 | 15      | 42      | 1           |
| Gigabyte Technology  | Z97N-WIFI                                        | 2014 | 15      | 41      | 1           |
| Dell                 | Inspiron 660s                                    | 2012 | 15      | 41      | 1           |
| ASRock               | B85M Pro4                                        | 2013 | 15      | 40      | 1           |
| ECS                  | MCP61M-M3                                        | 2009 | 15      | 40      | 1           |
| Biostar              | G31-M7 TE                                        | 2008 | 15      | 40      | 1           |
| ASRock               | A55M-HVS                                         | 2011 | 15      | 38      | 1           |
| MSI                  | MS-7A74                                          | 2016 | 15      | 37      | 1           |
| ASUSTek Computer     | PRIME B360M-K                                    | 2018 | 15      | 36      | 1           |
| ASUSTek Computer     | PRIME B365M-A                                    | 2019 | 15      | 35      | 1           |
| Hewlett-Packard      | Pro3500 Series                                   | 2013 | 15      | 35      | 1           |
| Hewlett-Packard      | Compaq dx2400 Microtower PC                      | 2008 | 15      | 35      | 1           |
| Hewlett-Packard      | Compaq dc7600 Convertible Minitower              | 2005 | 15      | 35      | 1           |
| Dell                 | PowerEdge T30                                    | 2016 | 15      | 34      | 1           |
| ASUSTek Computer     | PRIME H310M-E R2.0                               | 2018 | 15      | 33      | 1           |
| Foxconn              | 45CMX/45GMX/45CMX-K                              | 2007 | 15      | 33      | 1           |
| MSI                  | MS-7C09                                          | 2018 | 15      | 32      | 1           |
| AAEON                | MF-001                                           | 2017 | 15      | 31      | 1           |
| ASRock               | H61M-HVS                                         | 2011 | 15      | 30      | 1           |
| ASRock               | Q1900B-ITX                                       | 2014 | 15      | 28      | 1           |
| Hewlett-Packard      | ProLiant ML350 G6                                | 2009 | 14      | 99      | 1           |
| ASUSTek Computer     | P9X79 PRO                                        | 2012 | 14      | 97      | 1           |
| Gigabyte Technology  | EX58-UD5                                         | 2008 | 14      | 83      | 1           |
| Gigabyte Technology  | GA-MA770-DS3                                     | 2007 | 14      | 55      | 1           |
| Gigabyte Technology  | GA-MA790XT-UD4P                                  | 2009 | 14      | 52      | 1           |
| Intel                | H55 INTEL                                        | 2014 | 14      | 49      | 1           |
| ASRock               | 980DE3/U3S3                                      | 2013 | 14      | 47      | 1           |
| ASUSTek Computer     | PRIME X370-A                                     | 2017 | 14      | 46      | 1           |
| Gigabyte Technology  | Z77X-UD3H                                        | 2012 | 14      | 46      | 1           |
| MSI                  | MS-7B45                                          | 2017 | 14      | 43      | 1           |
| ASUSTek Computer     | PRIME B250M-PLUS                                 | 2016 | 14      | 43      | 1           |
| Fujitsu Siemens      | ESPRIMO P                                        | 2005 | 14      | 42      | 1           |
| ASRock               | N68C-GS4 FX                                      | 2014 | 14      | 41      | 1           |
| Gigabyte Technology  | H81M-HD3                                         | 2013 | 14      | 41      | 1           |
| ASUSTek Computer     | P8H61-MX USB3                                    | 2012 | 14      | 40      | 1           |
| ASUSTek Computer     | P8B75-M LX PLUS                                  | 2012 | 14      | 40      | 1           |
| ASRock               | FM2A75M-DGS                                      | 2012 | 14      | 40      | 1           |
| ASRock               | H81M-HDS                                         | 2013 | 14      | 39      | 1           |
| Fujitsu Siemens      | ESPRIMO E5730                                    | 2009 | 14      | 39      | 1           |
| ASUSTek Computer     | M4A78 PRO                                        | 2009 | 14      | 39      | 1           |
| ASUSTek Computer     | B250 MINING EXPERT                               | 2017 | 14      | 38      | 1           |
| ASUSTek Computer     | M4N68T-M                                         | 2009 | 14      | 38      | 1           |
| Hewlett-Packard      | Compaq dc5750 Microtower                         | 2006 | 14      | 38      | 1           |
| ASUSTek Computer     | M2N4-SLI                                         | 2006 | 14      | 37      | 1           |
| ASUSTek Computer     | H110M-E/M.2                                      | 2016 | 14      | 36      | 1           |
| ASUSTek Computer     | H110M-A                                          | 2015 | 14      | 35      | 1           |
| Dell                 | OptiPlex 3020M                                   | 2014 | 14      | 34      | 1           |
| Gigabyte Technology  | P41-ES3G                                         | 2009 | 14      | 34      | 1           |
| ASUSTek Computer     | M2N-MX SE                                        | 2007 | 14      | 33      | 1           |
| AZW                  | Z83 II                                           | 2016 | 14      | 23      | 1           |
| Dell                 | Precision Tower 7810                             | 2015 | 13      | 119     | 1           |
| ASUSTek Computer     | Rampage III Extreme                              | 2010 | 13      | 78      | 1           |
| ASUSTek Computer     | P6X58D PREMIUM                                   | 2010 | 13      | 77      | 1           |
| ASRock               | X570 Phantom Gaming-ITX/TB3                      | 2019 | 13      | 66      | 1           |
| ASUSTek Computer     | PRIME B550-PLUS                                  | 2020 | 13      | 63      | 1           |
| ASUSTek Computer     | P7P55D-E LX                                      | 2009 | 13      | 63      | 1           |
| ASUSTek Computer     | ROG STRIX X399-E GAMING                          | 2017 | 13      | 62      | 1           |
| Hewlett-Packard      | xw8600 Workstation                               | 2008 | 13      | 60      | 1           |
| ASRock               | B450M/ac                                         | 2019 | 13      | 49      | 1           |
| ASRock               | A320M-HDV R3.0                                   | 2018 | 13      | 49      | 1           |
| Gigabyte Technology  | Z87X-UD3H                                        | 2013 | 13      | 49      | 1           |
| ASUSTek Computer     | M2NPV-VM                                         | 2006 | 13      | 48      | 1           |
| ASUSTek Computer     | M2N32-SLI DELUXE                                 | 2006 | 13      | 48      | 1           |
| ASRock               | FM2A88M Extreme4+                                | 2013 | 13      | 46      | 1           |
| Gigabyte Technology  | B360M-D3H                                        | 2018 | 13      | 43      | 1           |
| ASRock               | Z97 Extreme4                                     | 2014 | 13      | 43      | 1           |
| ASUSTek Computer     | M4N78 SE                                         | 2009 | 13      | 43      | 1           |
| ASRock               | B75 Pro3-M                                       | 2012 | 13      | 42      | 1           |
| Gigabyte Technology  | P35C-DS3R                                        | 2007 | 13      | 42      | 1           |
| ASUSTek Computer     | P5Q-EM                                           | 2008 | 13      | 41      | 1           |
| MSI                  | MS-7786                                          | 2012 | 13      | 40      | 1           |
| ASUSTek Computer     | M5A87                                            | 2011 | 13      | 39      | 1           |
| Fujitsu              | ESPRIMO P2560                                    | 2010 | 13      | 39      | 1           |
| Hewlett-Packard      | Compaq dc7800 Convertible Minitower              | 2007 | 13      | 39      | 1           |
| Acer                 | Aspire T180                                      | 2006 | 13      | 39      | 1           |
| Gigabyte Technology  | H87M-D3H                                         | 2013 | 13      | 38      | 1           |
| ASRock               | N68-GS4 FX                                       | 2013 | 13      | 38      | 1           |
| Dell                 | Inspiron 570                                     | 2009 | 13      | 38      | 1           |
| Fujitsu              | ESPRIMO P710                                     | 2012 | 13      | 37      | 1           |
| Gigabyte Technology  | H67MA-USB3-B3                                    | 2011 | 13      | 37      | 1           |
| Acer                 | Veriton M430                                     | 2010 | 13      | 37      | 1           |
| Hewlett-Packard      | Compaq dc5750 Small Form Factor                  | 2007 | 13      | 37      | 1           |
| Hewlett-Packard      | Z230 SFF Workstation                             | 2013 | 13      | 36      | 1           |
| eMachines            | EL1352                                           | 2010 | 13      | 36      | 1           |
| Foxconn              | G31MXP FAB:1.1                                   | 2009 | 13      | 36      | 1           |
| Dell                 | Precision T1600                                  | 2011 | 13      | 35      | 1           |
| Hewlett-Packard      | Compaq dc7900 Ultra-Slim Desktop                 | 2008 | 13      | 35      | 1           |
| Gigabyte Technology  | M52L-S3                                          | 2007 | 13      | 35      | 1           |
| Hewlett-Packard      | Compaq dc7700 Convertible Minitower              | 2006 | 13      | 35      | 1           |
| Intel                | H81                                              | 2019 | 13      | 34      | 1           |
| Hewlett-Packard      | Compaq dc5850 Small Form Factor                  | 2008 | 13      | 34      | 1           |
| Hewlett-Packard      | Compaq dc5700 Microtower                         | 2006 | 13      | 34      | 1           |
| ASUSTek Computer     | P4P800SE                                         | 2004 | 13      | 34      | 1           |
| Dell                 | Inspiron 3650                                    | 2015 | 13      | 33      | 1           |
| Hewlett-Packard      | ProDesk 400 G2 MT                                | 2014 | 13      | 32      | 1           |
| ASUSTek Computer     | P8H61-M LE/BR                                    | 2011 | 13      | 32      | 1           |
| Pegatron             | IPM41-D3                                         | 2010 | 13      | 32      | 1           |
| Hewlett-Packard      | Compaq dx2300 Microtower                         | 2007 | 13      | 32      | 1           |
| Gigabyte Technology  | P31-S3G                                          | 2007 | 13      | 31      | 1           |
| AZW                  | Z83-V                                            | 2017 | 13      | 23      | 1           |
| Gigabyte Technology  | X99-UD4-CF                                       | 2014 | 12      | 105     | 1           |
| Gigabyte Technology  | EX58-UD3R                                        | 2008 | 12      | 75      | 1           |
| Gigabyte Technology  | TRX40 AORUS MASTER                               | 2020 | 12      | 73      | 1           |
| Intel                | Board                                            | 2014 | 12      | 65      | 1           |
| Gigabyte Technology  | P55M-UD2                                         | 2009 | 12      | 62      | 1           |
| Gigabyte Technology  | 990FXA-UD5                                       | 2013 | 12      | 58      | 1           |
| ASUSTek Computer     | P7P55D LE                                        | 2009 | 12      | 57      | 1           |
| ASUSTek Computer     | P5N-E SLI                                        | 2007 | 12      | 57      | 1           |
| ASUSTek Computer     | P7H55-M LX                                       | 2010 | 12      | 54      | 1           |
| Dell                 | XPS 8920                                         | 2018 | 12      | 53      | 1           |
| Gigabyte Technology  | AB350M-DS3H                                      | 2018 | 12      | 52      | 1           |
| Gigabyte Technology  | AX370-Gaming K5                                  | 2017 | 12      | 52      | 1           |
| Gigabyte Technology  | B450 GAMING X                                    | 2019 | 12      | 51      | 1           |
| ASUSTek Computer     | P5K Premium                                      | 2007 | 12      | 51      | 1           |
| ASUSTek Computer     | P5K/EPU                                          | 2008 | 12      | 50      | 1           |
| ASUSTek Computer     | P5QL-EM                                          | 2008 | 12      | 45      | 1           |
| ASRock               | A320M-DVS R4.0                                   | 2018 | 12      | 44      | 1           |
| Dell                 | OptiPlex 740 Enhanced                            | 2008 | 12      | 44      | 1           |
| ASUSTek Computer     | M4A88TD-M EVO                                    | 2010 | 12      | 43      | 1           |
| Gigabyte Technology  | Z97X-Gaming 5                                    | 2014 | 12      | 42      | 1           |
| Gigabyte Technology  | H61M-D2H-USB3                                    | 2011 | 12      | 42      | 1           |
| Gigabyte Technology  | P35-DS3                                          | 2007 | 12      | 42      | 1           |
| ASUSTek Computer     | M4A88T-V EVO/USB3                                | 2010 | 12      | 41      | 1           |
| ASRock               | P43DE                                            | 2008 | 12      | 41      | 1           |
| MSI                  | MS-7C08                                          | 2019 | 12      | 40      | 1           |
| ASUSTek Computer     | PRIME Z370-A II                                  | 2018 | 12      | 40      | 1           |
| MSI                  | MS-7B49                                          | 2017 | 12      | 40      | 1           |
| Hewlett-Packard      | EliteDesk 705 G1 SFF                             | 2014 | 12      | 40      | 1           |
| Gigabyte Technology  | H87M-HD3                                         | 2013 | 12      | 39      | 1           |
| Alienware            | X51 R2                                           | 2013 | 12      | 39      | 1           |
| ASUSTek Computer     | M4A77T                                           | 2010 | 12      | 39      | 1           |
| Gigabyte Technology  | B360M AORUS Gaming 3                             | 2018 | 12      | 38      | 1           |
| ASRock               | B250M Pro4                                       | 2016 | 12      | 38      | 1           |
| Gigabyte Technology  | F2A55M-S1                                        | 2013 | 12      | 38      | 1           |
| Gigabyte Technology  | EP31-DS3L                                        | 2008 | 12      | 38      | 1           |
| ASUSTek Computer     | P5B SE                                           | 2007 | 12      | 38      | 1           |
| MSI                  | MS-7C88                                          | 2020 | 12      | 37      | 1           |
| Hewlett-Packard      | Compaq dx2400 Microtower                         | 2008 | 12      | 37      | 1           |
| Gigabyte Technology  | H110-D3A                                         | 2017 | 12      | 34      | 1           |
| ASUSTek Computer     | PRIME H310M-K R2.0                               | 2018 | 12      | 33      | 1           |
| ECS                  | G31T-M                                           | 2007 | 12      | 33      | 1           |
| Dell                 | OptiPlex 320                                     | 2007 | 12      | 31      | 1           |
| ASUSTek Computer     | PRIME H410M-E                                    | 2020 | 12      | 28      | 1           |
| HCL Infosystems L... | HCL Desktop                                      | 2010 | 12      | 28      | 1           |
| Dell                 | Studio XPS 9100                                  | 2010 | 11      | 78      | 1           |
| ASUSTek Computer     | P7P55D-E PRO                                     | 2009 | 11      | 69      | 1           |
| ASRock               | X370 Gaming X                                    | 2017 | 11      | 67      | 1           |
| ASRock               | X399 Professional Gaming                         | 2017 | 11      | 64      | 1           |
| ECS                  | H55H-M                                           | 2010 | 11      | 61      | 1           |
| Gigabyte Technology  | H55M-S2H                                         | 2010 | 11      | 59      | 1           |
| Apple                | MacPro2,1                                        | 2007 | 11      | 59      | 1           |
| MSI                  | MS-7A62                                          | 2016 | 11      | 57      | 1           |
| Hewlett-Packard      | PPPPP-CCC#MMMMMMMM                               | 2010 | 11      | 54      | 1           |
| Gigabyte Technology  | Z390 AORUS ULTRA                                 | 2018 | 11      | 53      | 1           |
| Gigabyte Technology  | B450M DS3H WIFI                                  | 2019 | 11      | 52      | 1           |
| ASUSTek Computer     | ROG STRIX X370-F GAMING                          | 2018 | 11      | 52      | 1           |
| Intel                | DH55TC AAE70932-302                              | 2010 | 11      | 52      | 1           |
| MSI                  | MS-7B38                                          | 2017 | 11      | 50      | 1           |
| ASRock               | H97 Anniversary                                  | 2014 | 11      | 50      | 1           |
| ASUSTek Computer     | TUF B450M-PRO GAMING                             | 2019 | 11      | 49      | 1           |
| ASUSTek Computer     | M4A79T Deluxe                                    | 2009 | 11      | 48      | 1           |
| ASUSTek Computer     | F2A85-V PRO                                      | 2012 | 11      | 47      | 1           |
| ASUSTek Computer     | SABERTOOTH P67                                   | 2011 | 11      | 46      | 1           |
| Hewlett-Packard      | ProLiant ML110 G6                                | 2009 | 11      | 46      | 1           |
| ASUSTek Computer     | PRIME A320M-R                                    | 2018 | 11      | 44      | 1           |
| MSI                  | MS-7974                                          | 2015 | 11      | 44      | 1           |
| Gigabyte Technology  | EP45-UD3R                                        | 2008 | 11      | 44      | 1           |
| Dell                 | Precision 3630 Tower                             | 2018 | 11      | 43      | 1           |
| ASUSTek Computer     | PRIME H270-PLUS                                  | 2017 | 11      | 43      | 1           |
| ASUSTek Computer     | P5KR                                             | 2007 | 11      | 43      | 1           |
| ASUSTek Computer     | TUF Z390-PLUS GAMING                             | 2018 | 11      | 42      | 1           |
| ASUSTek Computer     | P5E-VM DO                                        | 2007 | 11      | 42      | 1           |
| Gigabyte Technology  | F2A85XM-D3H                                      | 2012 | 11      | 41      | 1           |
| Intel                | DQ67SW AAG12527-310                              | 2011 | 11      | 41      | 1           |
| Hewlett-Packard      | Compaq dc5800 Microtower                         | 2008 | 11      | 41      | 1           |
| MSI                  | MS-7C79                                          | 2020 | 11      | 40      | 1           |
| MSI                  | MS-7922                                          | 2014 | 11      | 40      | 1           |
| ASUSTek Computer     | P5QL-CM                                          | 2008 | 11      | 40      | 1           |
| ASUSTek Computer     | PRIME Z490-P                                     | 2020 | 11      | 39      | 1           |
| ASRock               | Z170 Gaming K4                                   | 2015 | 11      | 39      | 1           |
| ASRock               | 760GM-GS3                                        | 2010 | 11      | 39      | 1           |
| Gigabyte Technology  | 965P-S3                                          | 2007 | 11      | 39      | 1           |
| ASRock               | Z170 Pro4S                                       | 2015 | 11      | 37      | 1           |
| Dell                 | Precision T1650                                  | 2012 | 11      | 37      | 1           |
| ASUSTek Computer     | M4A77TD PRO                                      | 2009 | 11      | 37      | 1           |
| ASUSTek Computer     | P5QL-ASUS-SE                                     | 2008 | 11      | 37      | 1           |
| Biostar              | A10N-8800E                                       | 2019 | 11      | 36      | 1           |
| ASUSTek Computer     | PRIME B460-PLUS                                  | 2020 | 11      | 35      | 1           |
| Gigabyte Technology  | H81M-S                                           | 2014 | 11      | 35      | 1           |
| Gigabyte Technology  | Z68MA-D2H-B3                                     | 2011 | 11      | 35      | 1           |
| Gigabyte Technology  | GA-MA78LMT-US2H                                  | 2009 | 11      | 35      | 1           |
| Biostar              | N61PB-M2S                                        | 2008 | 11      | 35      | 1           |
| Acer                 | Aspire X3990                                     | 2011 | 11      | 33      | 1           |
| Biostar              | G31D-M7                                          | 2008 | 11      | 33      | 1           |
| Hewlett-Packard      | 280 G1 MT                                        | 2015 | 11      | 32      | 1           |
| Hewlett-Packard      | Compaq dc5850 Microtower                         | 2008 | 11      | 32      | 1           |
| Hewlett-Packard      | ProDesk 600 G1 DM                                | 2014 | 11      | 31      | 1           |
| Fujitsu              | ESPRIMO E700                                     | 2011 | 11      | 31      | 1           |
| ECS                  | G31T-M9                                          | 2009 | 11      | 31      | 1           |
| ASUSTek Computer     | PRIME H310M-E R2.0/BR                            | 2019 | 11      | 30      | 1           |
| ASUSTek Computer     | P5GZ-MX                                          | 2006 | 11      | 30      | 1           |
| ASUSTek Computer     | P9X79 DELUXE                                     | 2011 | 10      | 93      | 1           |
| Gigabyte Technology  | X58A-UD5                                         | 2009 | 10      | 90      | 1           |
| ASRock               | AB350 Gaming K4                                  | 2017 | 10      | 58      | 1           |
| ASUSTek Computer     | P7P55-M                                          | 2009 | 10      | 55      | 1           |
| ASRock               | 990FX Extreme4                                   | 2011 | 10      | 54      | 1           |
| ASUSTek Computer     | ROG STRIX Z370-H GAMING                          | 2017 | 10      | 53      | 1           |
| ASRock               | X370 Gaming K4                                   | 2013 | 10      | 53      | 1           |
| MSI                  | MS-7C92                                          | 2020 | 10      | 52      | 1           |
| MSI                  | MS-7A59                                          | 2016 | 10      | 51      | 1           |
| ASRock               | Z170 Extreme4                                    | 2016 | 10      | 51      | 1           |
| ASUSTek Computer     | Commando                                         | 2008 | 10      | 51      | 1           |
| Gigabyte Technology  | Z270-HD3P                                        | 2017 | 10      | 50      | 1           |
| Gigabyte Technology  | Z170XP-SLI                                       | 2016 | 10      | 50      | 1           |
| ASRock               | X470 Gaming-ITX/ac                               | 2018 | 10      | 48      | 1           |
| Intel                | DH55PJ AAE93812-302                              | 2010 | 10      | 48      | 1           |
| ASUSTek Computer     | PRIME Z390M-PLUS                                 | 2018 | 10      | 47      | 1           |
| MSI                  | MS-7B07                                          | 2017 | 10      | 46      | 1           |
| ASUSTek Computer     | F2A85-M LE                                       | 2012 | 10      | 46      | 1           |
| WinFast              | 6100M2MA                                         | 2006 | 10      | 46      | 1           |
| ASRock               | K10N78D                                          | 2010 | 10      | 45      | 1           |
| ASUSTek Computer     | PRIME A320M-A                                    | 2017 | 10      | 44      | 1           |
| Gigabyte Technology  | B75M-HD3                                         | 2012 | 10      | 43      | 1           |
| MSI                  | MS-7A63                                          | 2016 | 10      | 42      | 1           |
| MSI                  | MS-7695                                          | 2012 | 10      | 42      | 1           |
| Hewlett-Packard      | Elite 7300 Series MT                             | 2011 | 10      | 42      | 1           |
| Dell                 | OptiPlex 7070                                    | 2019 | 10      | 41      | 1           |
| ASUSTek Computer     | P8P67-M                                          | 2011 | 10      | 41      | 1           |
| ASUSTek Computer     | M5A78L-M LX PLUS                                 | 2011 | 10      | 41      | 1           |
| MSI                  | MS-7235                                          | 2006 | 10      | 41      | 1           |
| ASUSTek Computer     | M5A97 PLUS                                       | 2015 | 10      | 40      | 1           |
| Dell                 | Inspiron 545                                     | 2009 | 10      | 39      | 1           |
| ECS                  | GeForce7050M-M                                   | 2007 | 10      | 39      | 1           |
| MSI                  | MS-7260                                          | 2006 | 10      | 39      | 1           |
| MSI                  | MS-7C83                                          | 2020 | 10      | 38      | 1           |
| Gigabyte Technology  | 8IPE1000-G                                       | 2004 | 10      | 38      | 1           |
| Gigabyte Technology  | E3000N                                           | 2018 | 10      | 37      | 1           |
| ASUSTek Computer     | A55BM-E                                          | 2013 | 10      | 37      | 1           |
| Gigabyte Technology  | GA-880GM-D2H                                     | 2010 | 10      | 37      | 1           |
| ASUSTek Computer     | P5P41T-LE                                        | 2009 | 10      | 37      | 1           |
| ASUSTek Computer     | M2A74-AM SE                                      | 2009 | 10      | 37      | 1           |
| Alienware            | X51                                              | 2012 | 10      | 35      | 1           |
| Dell                 | Vostro 220 Series                                | 2008 | 10      | 35      | 1           |
| Hewlett-Packard      | Compaq dc7700p Small Form Factor                 | 2006 | 10      | 35      | 1           |
| ASUSTek Computer     | TUF B360M-PLUS GAMING/BR                         | 2018 | 10      | 34      | 1           |
| ASRock               | G41C-VS                                          | 2009 | 10      | 34      | 1           |
| Positivo             | POS-MI945AA                                      | 2007 | 10      | 34      | 1           |
| ASRock               | ConRoe945G-DVI                                   | 2006 | 10      | 34      | 1           |
| ZOTAC                | NM10                                             | 2010 | 10      | 33      | 1           |
| ASUSTek Computer     | P5PL2                                            | 2005 | 10      | 33      | 1           |
| ASUSTek Computer     | P5GV-MX                                          | 2005 | 10      | 33      | 1           |
| Gigabyte Technology  | H310M S2 2.0                                     | 2018 | 10      | 32      | 1           |
| ASUSTek Computer     | M32CD_A_F_K20CD_K31CD                            | 2016 | 10      | 32      | 1           |
| ASRock               | J3455-ITX                                        | 2016 | 10      | 32      | 1           |
| ASRock               | H110M-HDV R3.0                                   | 2016 | 10      | 32      | 1           |
| ASRock               | G41M-S3                                          | 2010 | 10      | 32      | 1           |
| ASUSTek Computer     | P4P800-VM                                        | 2004 | 10      | 32      | 1           |
| Foxconn              | G41MX/G41MX-K 2.0 1.0                            | 2009 | 10      | 31      | 1           |
| Gigabyte Technology  | 8I945GZME-RH                                     | 2006 | 10      | 30      | 1           |
| ASUSTek Computer     | P5PE-VM                                          | 2006 | 10      | 30      | 1           |
| ASRock               | J4205-ITX                                        | 2016 | 10      | 29      | 1           |
| ASUSTek Computer     | P9X79 LE                                         | 2012 | 9       | 86      | 1           |
| Intel                | X79                                              | 2016 | 9       | 80      | 1           |
| Gigabyte Technology  | H57M-USB3                                        | 2010 | 9       | 69      | 1           |
| MSI                  | MS-7C80                                          | 2020 | 9       | 57      | 1           |
| Gigabyte Technology  | H55M-USB3                                        | 2010 | 9       | 57      | 1           |
| Gigabyte Technology  | B550 AORUS MASTER                                | 2020 | 9       | 55      | 1           |
| ASUSTek Computer     | P7H55-M LE                                       | 2010 | 9       | 55      | 1           |
| Gigabyte Technology  | B550M AORUS ELITE                                | 2020 | 9       | 53      | 1           |
| ASRock               | Z390 Taichi Ultimate                             | 2019 | 9       | 52      | 1           |
| Gigabyte Technology  | Z77X-UP5 TH-CF                                   | 2012 | 9       | 52      | 1           |
| Gigabyte Technology  | P67X-UD3-B3                                      | 2011 | 9       | 49      | 1           |
| Gigabyte Technology  | AX370-Gaming                                     | 2017 | 9       | 48      | 1           |
| ASUSTek Computer     | Z170-PRO                                         | 2017 | 9       | 48      | 1           |
| ASUSTek Computer     | A88X-PRO                                         | 2015 | 9       | 48      | 1           |
| ASUSTek Computer     | Crosshair V Formula                              | 2012 | 9       | 48      | 1           |
| Dell                 | OptiPlex 740                                     | 2007 | 9       | 48      | 1           |
| MSI                  | MS-7C81                                          | 2020 | 9       | 47      | 1           |
| ASUSTek Computer     | F2A55                                            | 2012 | 9       | 47      | 1           |
| Intel                | SKYBAY                                           | 2016 | 9       | 45      | 1           |
| Hewlett-Packard      | Compaq Pro 6305 MT                               | 2012 | 9       | 45      | 1           |
| ASUSTek Computer     | F2A85-M PRO                                      | 2012 | 9       | 45      | 1           |
| ASUSTek Computer     | Rampage Formula                                  | 2008 | 9       | 45      | 1           |
| ECS                  | A320AM4-M3D                                      | 2017 | 9       | 44      | 1           |
| ASUSTek Computer     | P8P67-M PRO                                      | 2011 | 9       | 44      | 1           |
| ASRock               | Z270 Pro4                                        | 2017 | 9       | 43      | 1           |
| ASUSTek Computer     | PRIME B550M-K                                    | 2020 | 9       | 42      | 1           |
| Gigabyte Technology  | B250M-D2V                                        | 2016 | 9       | 42      | 1           |
| ASUSTek Computer     | Z170-AR                                          | 2015 | 9       | 42      | 1           |
| ASUSTek Computer     | TUF Z270 MARK 2                                  | 2017 | 9       | 41      | 1           |
| ASRock               | H77 Pro4/MVP                                     | 2012 | 9       | 41      | 1           |
| MSI                  | MS-7551                                          | 2008 | 9       | 41      | 1           |
| Gigabyte Technology  | EP43-DS3L                                        | 2008 | 9       | 41      | 1           |
| MSI                  | MS-7916                                          | 2015 | 9       | 40      | 1           |
| MSI                  | MS-7845                                          | 2013 | 9       | 40      | 1           |
| Gigabyte Technology  | F2A78M-D3H                                       | 2013 | 9       | 40      | 1           |
| Gigabyte Technology  | F2A88XM-HD3P                                     | 2015 | 9       | 39      | 1           |
| ASUSTek Computer     | ROG STRIX B360-F GAMING                          | 2018 | 9       | 38      | 1           |
| Gigabyte Technology  | Z370M AORUS Gaming                               | 2017 | 9       | 38      | 1           |
| ASUSTek Computer     | PRIME H270M-PLUS                                 | 2016 | 9       | 38      | 1           |
| Itautec              | Infoway                                          | 2006 | 9       | 38      | 1           |
| ASRock               | P67 Pro3                                         | 2011 | 9       | 37      | 1           |
| Navigator            | Impression                                       | 2006 | 9       | 37      | 1           |
| ASRock               | Z97E-ITX/ac                                      | 2014 | 9       | 36      | 1           |
| Medion               | MS-7797                                          | 2012 | 9       | 36      | 1           |
| Hewlett-Packard      | D5468AT-ABA ALONPAV                              | 2007 | 9       | 36      | 1           |
| ASUSTek Computer     | P5B-MX/WiFi-AP                                   | 2007 | 9       | 36      | 1           |
| ASUSTek Computer     | P5L1394                                          | 2006 | 9       | 36      | 1           |
| ASUSTek Computer     | PRIME H370M-PLUS                                 | 2018 | 9       | 35      | 1           |
| ASRock               | H110M-DGS                                        | 2016 | 9       | 35      | 1           |
| ASUSTek Computer     | M51AC                                            | 2013 | 9       | 35      | 1           |
| Intel                | ChiefRiver                                       | 2012 | 9       | 35      | 1           |
| Hewlett-Packard      | Z220 SFF Workstation                             | 2012 | 9       | 35      | 1           |
| Gigabyte Technology  | M55S-S3                                          | 2006 | 9       | 35      | 1           |
| MSI                  | MS-7A69                                          | 2016 | 9       | 34      | 1           |
| PCWare               | IPMH81G1                                         | 2014 | 9       | 34      | 1           |
| Gigabyte Technology  | H81M-DS2V                                        | 2013 | 9       | 34      | 1           |
| ASUSTek Computer     | P4P800                                           | 2003 | 9       | 34      | 1           |
| Hewlett-Packard      | Compaq dx2200 MT                                 | 2006 | 9       | 33      | 1           |
| Gigabyte Technology  | 8IPE1000-G/L                                     | 2005 | 9       | 33      | 1           |
| Intel                | DH67CL AAG10212-210                              | 2011 | 9       | 32      | 1           |
| Gigabyte Technology  | G31M-S2C                                         | 2008 | 9       | 32      | 1           |
| ASUSTek Computer     | P8H61 R2.0                                       | 2012 | 9       | 31      | 1           |
| Pegatron             | IPMH61P1                                         | 2012 | 9       | 30      | 1           |
| Acer                 | Aspire XC-603                                    | 2014 | 9       | 29      | 1           |
| Hewlett-Packard      | 260 G2 DM                                        | 2016 | 9       | 28      | 1           |
| Gigabyte Technology  | J1800N-D2H                                       | 2014 | 9       | 26      | 1           |
| Fujitsu              | ESPRIMO C700                                     | 2011 | 9       | 25      | 1           |
| Gigabyte Technology  | P55A-UD4                                         | 2009 | 8       | 73      | 1           |
| Dell                 | Precision WorkStation T5400                      | 2008 | 8       | 61      | 1           |
| ASRock               | Z390 Taichi                                      | 2018 | 8       | 59      | 1           |
| MSI                  | MS-7586                                          | 2009 | 8       | 57      | 1           |
| ASRock               | 990FX Extreme9                                   | 2013 | 8       | 56      | 1           |
| ASUSTek Computer     | P7H55-M/USB3                                     | 2010 | 8       | 56      | 1           |
| Dell                 | XPS 630i                                         | 2008 | 8       | 52      | 1           |
| ASUSTek Computer     | ROG STRIX Z370-G GAMING                          | 2017 | 8       | 51      | 1           |
| Intel                | DH55HC AAE70933-501                              | 2009 | 8       | 51      | 1           |
| ASRock               | Z390 Extreme4                                    | 2018 | 8       | 49      | 1           |
| Gigabyte Technology  | Z87X-UD4H                                        | 2013 | 8       | 49      | 1           |
| ASRock               | Z87 Extreme6                                     | 2013 | 8       | 46      | 1           |
| Gigabyte Technology  | GA-78LMT-S2PT                                    | 2012 | 8       | 46      | 1           |
| ASRock               | Z68 Pro3                                         | 2011 | 8       | 46      | 1           |
| Gigabyte Technology  | Z270X-Ultra Gaming                               | 2016 | 8       | 45      | 1           |
| Gigabyte Technology  | GA-A75-UD4H                                      | 2011 | 8       | 45      | 1           |
| Gigabyte Technology  | A320MA-M.2                                       | 2017 | 8       | 44      | 1           |
| ASRock               | 870 Extreme3 R2.0                                | 2011 | 8       | 44      | 1           |
| Supermicro           | H8DM8-2                                          | 2009 | 8       | 44      | 1           |
| ASRock               | M3N78D FX                                        | 2012 | 8       | 43      | 1           |
| Acer                 | Aspire M1200/3200/5200                           | 2008 | 8       | 43      | 1           |
| ASUSTek Computer     | M3N78-VM                                         | 2008 | 8       | 43      | 1           |
| ASRock               | A320M Pro4                                       | 2017 | 8       | 42      | 1           |
| Gigabyte Technology  | Z170X-UD3                                        | 2015 | 8       | 42      | 1           |
| Gigabyte Technology  | GA-880GMA-UD2H                                   | 2010 | 8       | 42      | 1           |
| ASRock               | G41MH/USB3                                       | 2010 | 8       | 42      | 1           |
| Gigabyte Technology  | EP43-DS3                                         | 2008 | 8       | 42      | 1           |
| MSI                  | MS-7A68                                          | 2017 | 8       | 41      | 1           |
| Gigabyte Technology  | H61MA-D3V                                        | 2011 | 8       | 41      | 1           |
| Hewlett-Packard      | 290 G2 MT Business PC                            | 2018 | 8       | 40      | 1           |
| ASUSTek Computer     | TUF Z370-PLUS GAMING                             | 2017 | 8       | 40      | 1           |
| ASUSTek Computer     | H110-PLUS                                        | 2016 | 8       | 40      | 1           |
| Fujitsu              | ESPRIMO E520                                     | 2013 | 8       | 40      | 1           |
| Gigabyte Technology  | PH67A-D3-B3                                      | 2011 | 8       | 39      | 1           |
| ASUSTek Computer     | M4A87TD                                          | 2010 | 8       | 39      | 1           |
| ASRock               | B460M Pro4                                       | 2020 | 8       | 38      | 1           |
| Dell                 | Vostro 3670                                      | 2018 | 8       | 38      | 1           |
| Gigabyte Technology  | M52S-S3P                                         | 2007 | 8       | 38      | 1           |
| ASRock               | FM2A58M-DG3+                                     | 2014 | 8       | 37      | 1           |
| ASRock               | 880GM-LE                                         | 2010 | 8       | 37      | 1           |
| Gigabyte Technology  | M52L-S3P                                         | 2008 | 8       | 37      | 1           |
| ASUSTek Computer     | P5LD2-X/1333                                     | 2007 | 8       | 37      | 1           |
| ASRock               | H370M-ITX/ac                                     | 2018 | 8       | 36      | 1           |
| Gigabyte Technology  | B250-HD3                                         | 2017 | 8       | 36      | 1           |
| Gigabyte Technology  | G33-DS3R                                         | 2007 | 8       | 36      | 1           |
| Intel                | D946GZIS AAD66165-302                            | 2006 | 8       | 36      | 1           |
| Acer                 | Aspire TC-895                                    | 2020 | 8       | 35      | 1           |
| Dell                 | Inspiron 531                                     | 2007 | 8       | 35      | 1           |
| ASRock               | 775Dual-VSTA                                     | 2006 | 8       | 35      | 1           |
| Dell                 | DV051                                            | 2005 | 8       | 35      | 1           |
| Dell                 | PowerEdge T40                                    | 2019 | 8       | 34      | 1           |
| DEPO Computers       | H61H2-M6                                         | 2011 | 8       | 34      | 1           |
| Dell                 | OptiPlex GX270                                   | 2003 | 8       | 34      | 1           |
| ASUSTek Computer     | B150M-A/M.2                                      | 2016 | 8       | 33      | 1           |
| ASUSTek Computer     | B150M-A D3                                       | 2015 | 8       | 33      | 1           |
| Hewlett-Packard      | Compaq Pro 4300 SFF PC                           | 2012 | 8       | 33      | 1           |
| ASRock               | H77M                                             | 2012 | 8       | 33      | 1           |
| eMachines            | EL1358G                                          | 2011 | 8       | 33      | 1           |
| Medion               | MS-7707                                          | 2011 | 8       | 33      | 1           |
| Intel                | DG41RQ AAE54511-205                              | 2009 | 8       | 33      | 1           |
| Intel                | DH67BL AAG10189-211                              | 2011 | 8       | 32      | 1           |
| Pegatron             | IPMSB-H61                                        | 2010 | 8       | 32      | 1           |
| Hewlett-Packard      | xw4400 Workstation                               | 2006 | 8       | 32      | 1           |
| Supermicro           | X9SCL/X9SCM                                      | 2011 | 8       | 31      | 1           |
| Intel                | D525MW AAE93082-401                              | 2011 | 8       | 31      | 1           |
| Gigabyte Technology  | M61SME-S2L                                       | 2007 | 8       | 31      | 1           |
| ECS                  | 945GCT-M                                         | 2007 | 8       | 31      | 1           |
| MSI                  | MS-7236                                          | 2006 | 8       | 31      | 1           |
| ASRock               | H110M-DGS R3.0                                   | 2016 | 8       | 30      | 1           |
| Google               | Panther                                          | 2014 | 8       | 30      | 1           |
| Acer                 | Aspire XC-603G                                   | 2013 | 8       | 30      | 1           |
| Acer                 | Aspire XC600                                     | 2012 | 8       | 29      | 1           |
| Gigabyte Technology  | D525TUD                                          | 2010 | 8       | 29      | 1           |
| ASRock               | H110M-HG4                                        | 2016 | 8       | 28      | 1           |
| Positivo             | POS-EINM70CS                                     | 2013 | 8       | 28      | 1           |
| Dell                 | Precision T7600                                  | 2014 | 7       | 107     | 1           |
| Dell                 | Precision T5600                                  | 2014 | 7       | 90      | 1           |
| ASUSTek Computer     | P6TD DELUXE                                      | 2010 | 7       | 72      | 1           |
| ASUSTek Computer     | PRIME TRX40-PRO                                  | 2020 | 7       | 59      | 1           |
| Gigabyte Technology  | TRX40 AORUS PRO WIFI                             | 2019 | 7       | 56      | 1           |
| ASRock               | X399 Phantom Gaming 6                            | 2018 | 7       | 56      | 1           |
| ASRock               | 990FX Killer                                     | 2013 | 7       | 55      | 1           |
| ASRock               | X370 Pro4                                        | 2018 | 7       | 54      | 1           |
| ASRock               | B550 Phantom Gaming-ITX/ax                       | 2020 | 7       | 53      | 1           |
| Hewlett-Packard      | Pavilion Gaming Desktop TG01-0xxx                | 2019 | 7       | 53      | 1           |
| Gigabyte Technology  | AB350N-Gaming WIFI                               | 2017 | 7       | 53      | 1           |
| Hewlett-Packard      | Z200 Workstation                                 | 2010 | 7       | 53      | 1           |
| ASUSTek Computer     | ROG Maximus XIII HERO                            | 2021 | 7       | 52      | 1           |
| Gigabyte Technology  | X399 DESIGNARE EX                                | 2018 | 7       | 52      | 1           |
| Pegatron             | IPMIP-GS                                         | 2010 | 7       | 52      | 1           |
| Intel                | DH55TC AAE70932-206                              | 2009 | 7       | 51      | 1           |
| Gigabyte Technology  | F2A85X-UP4                                       | 2012 | 7       | 50      | 1           |
| Gigabyte Technology  | B550 AORUS ELITE AX V2                           | 2020 | 7       | 49      | 1           |
| Hewlett-Packard      | OMEN 30L Desktop GT13-0xxx                       | 2020 | 7       | 48      | 1           |
| Gigabyte Technology  | Z87X-UD5H                                        | 2013 | 7       | 48      | 1           |
| WYSE                 | Z CLASS                                          | 2012 | 7       | 48      | 1           |
| Gigabyte Technology  | B450M S2H V2                                     | 2020 | 7       | 46      | 1           |
| ASRock               | A320M-DGS                                        | 2018 | 7       | 46      | 1           |
| ASUSTek Computer     | TUF Z270 MARK 1                                  | 2017 | 7       | 46      | 1           |
| ASUSTek Computer     | Maximus IV Extreme                               | 2011 | 7       | 46      | 1           |
| Medion               | P961x                                            | 2010 | 7       | 46      | 1           |
| Acer                 | Aspire X3900                                     | 2009 | 7       | 45      | 1           |
| Medion               | MS-7366                                          | 2007 | 7       | 45      | 1           |
| Gigabyte Technology  | A520M S2H                                        | 2020 | 7       | 44      | 1           |
| Gigabyte Technology  | Z270X-Gaming K5                                  | 2016 | 7       | 44      | 1           |
| ASUSTek Computer     | M3N78 PRO                                        | 2008 | 7       | 44      | 1           |
| Hewlett-Packard      | Slim Desktop 290-p0xxx                           | 2018 | 7       | 43      | 1           |
| Gigabyte Technology  | GA-78LMT-S2 R2                                   | 2017 | 7       | 43      | 1           |
| Gigabyte Technology  | Z97MX-Gaming 5                                   | 2014 | 7       | 43      | 1           |
| ASUSTek Computer     | P8P67 EVO                                        | 2012 | 7       | 43      | 1           |
| MSI                  | MS-7750                                          | 2011 | 7       | 43      | 1           |
| ASUSTek Computer     | M4A88TD-M/USB3                                   | 2010 | 7       | 43      | 1           |
| ASUSTek Computer     | M4N78-AM                                         | 2009 | 7       | 43      | 1           |
| ASRock               | P43DE3                                           | 2009 | 7       | 43      | 1           |
| ASRock               | Z77 Extreme6                                     | 2012 | 7       | 42      | 1           |
| PRIMINFO             | UNLOCK INSTALL                                   | 2008 | 7       | 42      | 1           |
| ASUSTek Computer     | PRIME B250-PRO                                   | 2016 | 7       | 41      | 1           |
| ASRock               | E350M1                                           | 2011 | 7       | 41      | 1           |
| ASUSTek Computer     | M4A77T/USB3                                      | 2010 | 7       | 41      | 1           |
| Gigabyte Technology  | X48-DS5                                          | 2008 | 7       | 41      | 1           |
| Shuttle              | XH61V                                            | 2012 | 7       | 40      | 1           |
| Gigabyte Technology  | H170-D3HP                                        | 2015 | 7       | 39      | 1           |
| Fujitsu              | ESPRIMO E920                                     | 2013 | 7       | 39      | 1           |
| Gigabyte Technology  | GA-A75M-UD2H                                     | 2011 | 7       | 39      | 1           |
| ASUSTek Computer     | P8H61-M LE/CSM                                   | 2011 | 7       | 39      | 1           |
| Gigabyte Technology  | GA-MA790X-UD3P                                   | 2009 | 7       | 39      | 1           |
| ECS                  | P43T-A2                                          | 2008 | 7       | 39      | 1           |
| Dell                 | DXP051                                           | 2005 | 7       | 39      | 1           |
| Gigabyte Technology  | H97M-HD3                                         | 2014 | 7       | 38      | 1           |
| Biostar              | A780LB                                           | 2012 | 7       | 38      | 1           |
| ASUSTek Computer     | P8H61-M                                          | 2011 | 7       | 38      | 1           |
| MSI                  | MS-7253                                          | 2007 | 7       | 38      | 1           |
| ASUSTek Computer     | PRIME H310M-A R2.0                               | 2019 | 7       | 37      | 1           |
| Acer                 | Predator G3-605                                  | 2013 | 7       | 37      | 1           |
| ASUSTek Computer     | Z170-P D3                                        | 2012 | 7       | 37      | 1           |
| Biostar              | A880G+                                           | 2010 | 7       | 37      | 1           |
| Acer                 | Aspire X3950                                     | 2010 | 7       | 37      | 1           |
| Biostar              | P4M900-M7 FE                                     | 2007 | 7       | 37      | 1           |
| ASUSTek Computer     | P5VD2-VM SE                                      | 2007 | 7       | 37      | 1           |
| ASUSTek Computer     | P5E-VM HDMI                                      | 2007 | 7       | 37      | 1           |
| MSI                  | MS-7D18                                          | 2021 | 7       | 36      | 1           |
| MSI                  | MS-7C31                                          | 2019 | 7       | 36      | 1           |
| ASUSTek Computer     | TUF Z390M-PRO GAMING                             | 2018 | 7       | 36      | 1           |
| Gigabyte Technology  | B85M-D3H-A                                       | 2015 | 7       | 36      | 1           |
| ASRock               | AM1H-ITX                                         | 2014 | 7       | 36      | 1           |
| ASUSTek Computer     | M4N68T                                           | 2010 | 7       | 36      | 1           |
| ASUSTek Computer     | M2N68-AM                                         | 2008 | 7       | 36      | 1           |
| ASRock               | 4Core1600-GLAN                                   | 2008 | 7       | 36      | 1           |
| MSI                  | MS-7B54                                          | 2018 | 7       | 35      | 1           |
| Gigabyte Technology  | H170-D3H                                         | 2015 | 7       | 35      | 1           |
| Gigabyte Technology  | P41T-D3                                          | 2010 | 7       | 35      | 1           |
| MSI                  | MS-7302                                          | 2008 | 7       | 35      | 1           |
| ASRock               | ALiveNF6G-GLAN                                   | 2007 | 7       | 35      | 1           |
| ASRock               | J3455M                                           | 2017 | 7       | 34      | 1           |
| ASUSTek Computer     | M11AD                                            | 2013 | 7       | 34      | 1           |
| ASRock               | Z77 Pro4-M                                       | 2012 | 7       | 34      | 1           |
| ASRock               | Z77 Extreme3                                     | 2012 | 7       | 34      | 1           |
| ASRock               | 945GCM-S                                         | 2008 | 7       | 34      | 1           |
| Hewlett-Packard      | Compaq dc7800p Ultra-slim Desktop                | 2007 | 7       | 34      | 1           |
| ASUSTek Computer     | P5PL2-E                                          | 2006 | 7       | 34      | 1           |
| ASUSTek Computer     | TUF B360-PLUS GAMING                             | 2018 | 7       | 32      | 1           |
| Hewlett-Packard      | t620 Dual Core TC                                | 2013 | 7       | 32      | 1           |
| Acer                 | Revo RL80                                        | 2012 | 7       | 32      | 1           |
| Hewlett-Packard      | rp5800                                           | 2011 | 7       | 32      | 1           |
| ASUSTek Computer     | P5QPL-VM EPU                                     | 2009 | 7       | 32      | 1           |
| eMachines            | EL1850                                           | 2010 | 7       | 31      | 1           |
| Dell                 | Inspiron 537s                                    | 2009 | 7       | 31      | 1           |
| Dell                 | Inspiron 531s                                    | 2007 | 7       | 31      | 1           |
| ASUSTek Computer     | PRIME H310M-E                                    | 2018 | 7       | 30      | 1           |
| Dell                 | Inspiron 3646                                    | 2014 | 7       | 30      | 1           |
| Biostar              | H61MGV3                                          | 2013 | 7       | 30      | 1           |
| ASRock               | H61M-HG4                                         | 2013 | 7       | 29      | 1           |
| ASUSTek Computer     | C8HM70-I/HDMI                                    | 2012 | 7       | 29      | 1           |
| PCWare               | PW-945GCX                                        | 2008 | 7       | 29      | 1           |
| Positivo             | POS-MIG31AG                                      | 2008 | 7       | 28      | 1           |
| PCWare               | IPX1800E2                                        | 2017 | 7       | 27      | 1           |
| Acer                 | Aspire M1610                                     | 2007 | 7       | 27      | 1           |
| ASRock               | X99 Taichi                                       | 2016 | 6       | 133     | 1           |
| ASUSTek Computer     | PRIME X299-DELUXE                                | 2017 | 6       | 110     | 1           |
| Alienware            | Area-51 R2                                       | 2014 | 6       | 106     | 1           |
| Intel                | X99 V1.0                                         | 2020 | 6       | 99      | 1           |
| ASUSTek Computer     | WS X299 SAGE                                     | 2018 | 6       | 91      | 1           |
| Gigabyte Technology  | X299 UD4 Pro                                     | 2017 | 6       | 81      | 1           |
| ASUSTek Computer     | TUF X299 MARK 1                                  | 2018 | 6       | 79      | 1           |
| ASUSTek Computer     | TUF X299 MARK 2                                  | 2017 | 6       | 79      | 1           |
| ASUSTek Computer     | ROG ZENITH II EXTREME ALPHA                      | 2020 | 6       | 75      | 1           |
| MSI                  | MS-7593                                          | 2009 | 6       | 69      | 1           |
| Dell                 | Studio XPS 435T/9000                             | 2009 | 6       | 69      | 1           |
| Dell                 | Studio XPS 8000                                  | 2009 | 6       | 59      | 1           |
| Gigabyte Technology  | X570 AORUS XTREME                                | 2019 | 6       | 57      | 1           |
| Gigabyte Technology  | X470 AORUS GAMING 5 WIFI                         | 2018 | 6       | 57      | 1           |
| ASUSTek Computer     | P7H55-M SI                                       | 2010 | 6       | 57      | 1           |
| MSI                  | MS-7350                                          | 2007 | 6       | 56      | 1           |
| ASUSTek Computer     | PRIME Z590-P                                     | 2021 | 6       | 55      | 1           |
| Gigabyte Technology  | TRX40 DESIGNARE                                  | 2020 | 6       | 54      | 1           |
| ASRock               | H55DE3                                           | 2010 | 6       | 54      | 1           |
| Gigabyte Technology  | Z370 AORUS Gaming K3                             | 2017 | 6       | 53      | 1           |
| Gigabyte Technology  | GA-990FX-GAMING                                  | 2015 | 6       | 52      | 1           |
| Hewlett-Packard      | Pro 3130 Small Form Factor PC                    | 2010 | 6       | 52      | 1           |
| ASUSTek Computer     | ROG CROSSHAIR VIII IMPACT                        | 2020 | 6       | 51      | 1           |
| Gigabyte Technology  | GA-990FXA-UD5                                    | 2011 | 6       | 51      | 1           |
| ASUSTek Computer     | P5QLD PRO                                        | 2009 | 6       | 51      | 1           |
| ASUSTek Computer     | M3A79-T DELUXE                                   | 2008 | 6       | 51      | 1           |
| ASUSTek Computer     | Maximus VIII HERO ALPHA                          | 2016 | 6       | 50      | 1           |
| Alienware            | Aurora R12                                       | 2021 | 6       | 47      | 1           |
| Gigabyte Technology  | B450M H                                          | 2019 | 6       | 47      | 1           |
| ASRock               | Z97 Pro4                                         | 2015 | 6       | 47      | 1           |
| Hewlett-Packard      | 550-153w                                         | 2015 | 6       | 46      | 1           |
| Gigabyte Technology  | 990FXA-UD3 R5                                    | 2015 | 6       | 46      | 1           |
| Fujitsu              | CELSIUS W420                                     | 2012 | 6       | 46      | 1           |
| MSI                  | MS-7976                                          | 2015 | 6       | 45      | 1           |
| Gigabyte Technology  | GA-790XTA-UD4                                    | 2009 | 6       | 45      | 1           |
| ASUSTek Computer     | M3N78-EMH HDMI                                   | 2008 | 6       | 44      | 1           |
| MSI                  | MS-7900                                          | 2015 | 6       | 43      | 1           |
| Gigabyte Technology  | B250-FinTech                                     | 2018 | 6       | 42      | 1           |
| Biostar              | A960D+V3                                         | 2018 | 6       | 42      | 1           |
| Hewlett-Packard      | Pavilion Desktop PC 570-p0XX                     | 2017 | 6       | 42      | 1           |
| Gigabyte Technology  | Z270P-D3                                         | 2017 | 6       | 42      | 1           |
| ASUSTek Computer     | STRIX H270F GAMING                               | 2016 | 6       | 42      | 1           |
| Gigabyte Technology  | Z87-D3HP                                         | 2013 | 6       | 42      | 1           |
| Acer                 | Aspire M3420                                     | 2012 | 6       | 42      | 1           |
| Fujitsu Siemens      | ESPRIMO P5925                                    | 2007 | 6       | 42      | 1           |
| ASUSTek Computer     | PRIME A320M-E                                    | 2018 | 6       | 41      | 1           |
| ASRock               | Z370M Pro4                                       | 2017 | 6       | 41      | 1           |
| Gigabyte Technology  | EP43T-UD3L                                       | 2009 | 6       | 41      | 1           |
| ASUSTek Computer     | M4N78                                            | 2009 | 6       | 41      | 1           |
| Gigabyte Technology  | Z490 AORUS ELITE AC                              | 2020 | 6       | 40      | 1           |
| ASUSTek Computer     | ROG STRIX B460-I GAMING                          | 2020 | 6       | 40      | 1           |
| Gigabyte Technology  | Z170MX-Gaming 5                                  | 2016 | 6       | 40      | 1           |
| Gigabyte Technology  | Z87N-WIFI                                        | 2013 | 6       | 40      | 1           |
| ASUSTek Computer     | M4A77                                            | 2010 | 6       | 40      | 1           |
| Dell                 | OptiPlex 7080                                    | 2020 | 6       | 39      | 1           |
| Gigabyte Technology  | B85M-DS3H                                        | 2014 | 6       | 39      | 1           |
| AMD                  | 970A-DS3                                         | 2012 | 6       | 39      | 1           |
| ASUSTek Computer     | M5A78L                                           | 2011 | 6       | 39      | 1           |
| ASRock               | H61M/U3S3                                        | 2011 | 6       | 39      | 1           |
| ASUSTek Computer     | M3N78                                            | 2008 | 6       | 39      | 1           |
| ASUSTek Computer     | M3A78 PRO                                        | 2008 | 6       | 39      | 1           |
| ASRock               | B250M-HDV                                        | 2017 | 6       | 38      | 1           |
| Lenovo               | K450e 10181                                      | 2014 | 6       | 38      | 1           |
| ASUSTek Computer     | F1A55-M LX R2.0                                  | 2012 | 6       | 38      | 1           |
| ASRock               | 939A785GMH/128M                                  | 2009 | 6       | 38      | 1           |
| ECS                  | P4M900T-M2                                       | 2007 | 6       | 38      | 1           |
| Dell                 | Inspiron 3670                                    | 2019 | 6       | 37      | 1           |
| ASRock               | Z390M Pro4                                       | 2018 | 6       | 37      | 1           |
| Gigabyte Technology  | H87-D3H                                          | 2013 | 6       | 37      | 1           |
| Gigabyte Technology  | A55M-DS2                                         | 2012 | 6       | 37      | 1           |
| ASUSTek Computer     | P8H61/USB3 R2.0                                  | 2012 | 6       | 37      | 1           |
| MSI                  | MS-7549                                          | 2009 | 6       | 37      | 1           |
| Intel                | DG35EC AAE29266-205                              | 2008 | 6       | 37      | 1           |
| ASUSTek Computer     | PRIME Z370-P II                                  | 2018 | 6       | 36      | 1           |
| Hewlett-Packard      | EliteDesk 800 G3 SFF                             | 2017 | 6       | 36      | 1           |
| Gigabyte Technology  | F2A58M-S1                                        | 2014 | 6       | 36      | 1           |
| ASUSTek Computer     | A55BM-K                                          | 2013 | 6       | 36      | 1           |
| ASUSTek Computer     | F2A55-M LE                                       | 2012 | 6       | 36      | 1           |
| Gigabyte Technology  | M61PM-S2                                         | 2006 | 6       | 36      | 1           |
| Gigabyte Technology  | GA-K8NF-9                                        | 2005 | 6       | 36      | 1           |
| ASUSTek Computer     | Maximus V GENE                                   | 2012 | 6       | 35      | 1           |
| ASUSTek Computer     | P5G41T-M LX V2                                   | 2011 | 6       | 35      | 1           |
| Biostar              | NF560-A2G                                        | 2007 | 6       | 35      | 1           |
| ASUSTek Computer     | TUF H310M-PLUS GAMING/BR                         | 2019 | 6       | 34      | 1           |
| Acer                 | Aspire XC-704                                    | 2015 | 6       | 34      | 1           |
| Hewlett-Packard      | Compaq 8200 Elite MT PC                          | 2011 | 6       | 34      | 1           |
| Acer                 | Predator G3610                                   | 2011 | 6       | 34      | 1           |
| Acer                 | Aspire M1930                                     | 2011 | 6       | 34      | 1           |
| Acer                 | Veriton M420                                     | 2008 | 6       | 34      | 1           |
| Gigabyte Technology  | 8I915P Duo                                       | 2004 | 6       | 34      | 1           |
| Gigabyte Technology  | GA-E6010N                                        | 2020 | 6       | 33      | 1           |
| ASRock               | B85M                                             | 2013 | 6       | 33      | 1           |
| Hewlett-Packard      | Compaq dx2420 Microtower                         | 2009 | 6       | 33      | 1           |
| ASRock               | Wolfdale1333-D667                                | 2007 | 6       | 33      | 1           |
| Dell                 | Inspiron 3880                                    | 2020 | 6       | 32      | 1           |
| ASUSTek Computer     | P8H61-I R2.0                                     | 2013 | 6       | 32      | 1           |
| Biostar              | NF61S-M2A                                        | 2006 | 6       | 32      | 1           |
| Gigabyte Technology  | M68MT-D3P                                        | 2010 | 6       | 31      | 1           |
| Intel                | DG31PR AAD97573-205                              | 2007 | 6       | 31      | 1           |
| ECS                  | nVidia-nForce                                    | 2003 | 6       | 31      | 1           |
| Gigabyte Technology  | J4005ND2P-CF                                     | 2018 | 6       | 29      | 1           |
| AMI                  | Cherry Trail CR                                  | 2016 | 6       | 26      | 1           |
| PCWare               | IPMH61R3                                         | 2011 | 6       | 26      | 1           |
| ASUSTek Computer     | P4S800-MX SE                                     | 2005 | 6       | 25      | 1           |
| ASRock               | J4105B-ITX                                       | 2018 | 6       | 24      | 1           |
| DALCO AG Switzerland | +41 44 908 38 38                                 | 2008 | 6       | 24      | 1           |
| Hewlett-Packard      | Z640 Workstation                                 | 2015 | 5       | 127     | 1           |
| MSI                  | MS-7C06                                          | 2018 | 5       | 100     | 1           |
| Huanan               | X99-8M-F V1.1                                    | 2020 | 5       | 96      | 1           |
| ASUSTek Computer     | PRIME X299-A                                     | 2017 | 5       | 82      | 1           |
| Huanan               | X79 INTEL (INTEL Xeon E5/Corei7 DMI2 - C600/C... | 2018 | 5       | 81      | 1           |
| ASUSTek Computer     | P6T7 WS SUPERCOMPUTER                            | 2010 | 5       | 74      | 1           |
| Gigabyte Technology  | EX58-UD4P                                        | 2009 | 5       | 72      | 1           |
| Gigabyte Technology  | H55N-USB3                                        | 2010 | 5       | 63      | 1           |
| ASUSTek Computer     | ROG CROSSHAIR VIII FORMULA                       | 2019 | 5       | 61      | 1           |
| ASRock               | X370 Professional Gaming                         | 2017 | 5       | 56      | 1           |
| ASUSTek Computer     | ROG STRIX Z490-F GAMING                          | 2020 | 5       | 52      | 1           |
| ASUSTek Computer     | P8Z68 DELUXE/GEN3                                | 2012 | 5       | 52      | 1           |
| Gigabyte Technology  | Z490 VISION D                                    | 2020 | 5       | 51      | 1           |
| Intel                | DQ57TM AAE70931-403                              | 2010 | 5       | 51      | 1           |
| Supermicro           | X8SIL                                            | 2009 | 5       | 51      | 1           |
| ASUSTek Computer     | PRIME A320I-K                                    | 2019 | 5       | 50      | 1           |
| Intel                | DH55HC AAE70933-505                              | 2010 | 5       | 50      | 1           |
| Acer                 | Aspire M5910                                     | 2010 | 5       | 50      | 1           |
| EVGA                 | 122-CK-NF68                                      | 2007 | 5       | 50      | 1           |
| ASRock               | 890FX Deluxe4                                    | 2012 | 5       | 49      | 1           |
| Gigabyte Technology  | H370M-D3H                                        | 2018 | 5       | 48      | 1           |
| HC                   | HCAR357-MI                                       | 2020 | 5       | 47      | 1           |
| Gigabyte Technology  | Z390 GAMING SLI                                  | 2018 | 5       | 46      | 1           |
| ASUSTek Computer     | STRIX Z270G GAMING                               | 2018 | 5       | 46      | 1           |
| ASUSTek Computer     | Maximus IX FORMULA                               | 2016 | 5       | 46      | 1           |
| ASUSTek Computer     | M5A78L-M LX3 PLUS                                | 2013 | 5       | 46      | 1           |
| Gigabyte Technology  | GA-M55PLUS-S3G                                   | 2006 | 5       | 45      | 1           |
| Medion               | MS-7091                                          | 2005 | 5       | 45      | 1           |
| ASUSTek Computer     | A8N32-SLI-Deluxe                                 | 2005 | 5       | 45      | 1           |
| Gigabyte Technology  | B460HD3                                          | 2020 | 5       | 44      | 1           |
| Gigabyte Technology  | AX370M-Gaming 3                                  | 2018 | 5       | 44      | 1           |
| MSI                  | MS-7D25                                          | 2021 | 5       | 43      | 1           |
| Gigabyte Technology  | B550M S2H                                        | 2020 | 5       | 43      | 1           |
| MSI                  | MS-7926                                          | 2016 | 5       | 43      | 1           |
| Hewlett-Packard      | Elite Slice                                      | 2016 | 5       | 43      | 1           |
| Gigabyte Technology  | Z68XP-UD4                                        | 2012 | 5       | 43      | 1           |
| eMachines            | ET1831                                           | 2009 | 5       | 43      | 1           |
| ASUSTek Computer     | M2R32-MVP                                        | 2007 | 5       | 43      | 1           |
| ASUSTek Computer     | A88XM-A/USB                                      | 2016 | 5       | 42      | 1           |
| Gigabyte Technology  | H67A-UD3H-B3                                     | 2011 | 5       | 42      | 1           |
| Hewlett-Packard      | Slim Desktop 290-a0xxx                           | 2017 | 5       | 41      | 1           |
| Gigabyte Technology  | Gaming B8                                        | 2017 | 5       | 41      | 1           |
| Gigabyte Technology  | Z68X-UD4-B3                                      | 2011 | 5       | 41      | 1           |
| Acer                 | Aspire M1641                                     | 2008 | 5       | 41      | 1           |
| ASUSTek Computer     | P5B-E                                            | 2006 | 5       | 41      | 1           |
| ASUSTek Computer     | A8N-SLI Premium                                  | 2005 | 5       | 41      | 1           |
| MSI                  | MS-7C96                                          | 2020 | 5       | 40      | 1           |
| ASUSTek Computer     | TUF B365M-PLUS GAMING                            | 2019 | 5       | 40      | 1           |
| ASUSTek Computer     | Maximus VIII GENE                                | 2016 | 5       | 40      | 1           |
| ASRock               | H97M Anniversary                                 | 2014 | 5       | 40      | 1           |
| Hewlett-Packard      | Pavilion P6000 Series                            | 2010 | 5       | 40      | 1           |
| Acer                 | Aspire X3200                                     | 2008 | 5       | 40      | 1           |
| Hewlett-Packard      | Pavilion Desktop TP01-1xxx                       | 2020 | 5       | 39      | 1           |
| ASRock               | Z390 Phantom Gaming-ITX/ac                       | 2018 | 5       | 39      | 1           |
| Biostar              | Hi-Fi A70U3P                                     | 2015 | 5       | 39      | 1           |
| Gigabyte Technology  | GA-E350N-USB3                                    | 2011 | 5       | 39      | 1           |
| Acer                 | Aspire X3470                                     | 2011 | 5       | 39      | 1           |
| Hewlett-Packard      | ProLiant ML110 G5                                | 2009 | 5       | 39      | 1           |
| Gigabyte Technology  | GA-MA78GM-US2H                                   | 2009 | 5       | 39      | 1           |
| ASRock               | 4CoreDual-SATA2                                  | 2007 | 5       | 39      | 1           |
| MSI                  | MS-7C39                                          | 2019 | 5       | 38      | 1           |
| Intel                | DH77KC AAG39641-401                              | 2012 | 5       | 38      | 1           |
| ASRock               | Z68 Pro3-M                                       | 2011 | 5       | 38      | 1           |
| ASRock               | G41M-VGS3                                        | 2011 | 5       | 38      | 1           |
| Gateway              | DX4840                                           | 2010 | 5       | 38      | 1           |
| Biostar              | A770L3                                           | 2010 | 5       | 38      | 1           |
| Gigabyte Technology  | EP45-DS4                                         | 2008 | 5       | 38      | 1           |
| Gigabyte Technology  | 965P-DS4                                         | 2006 | 5       | 38      | 1           |
| AWOW                 | AK41                                             | 2020 | 5       | 37      | 1           |
| ASRock               | J3355B-ITX                                       | 2016 | 5       | 37      | 1           |
| Fujitsu              | ESPRIMO P420                                     | 2013 | 5       | 37      | 1           |
| ECS                  | A55F2-M3                                         | 2013 | 5       | 37      | 1           |
| Biostar              | H77MU3                                           | 2012 | 5       | 37      | 1           |
| ASUSTek Computer     | CM6330_CM6630_CM6730_CM6830-8                    | 2012 | 5       | 37      | 1           |
| ASRock               | M3A785GMH/128M                                   | 2009 | 5       | 37      | 1           |
| ASRock               | B250 Pro4                                        | 2017 | 5       | 36      | 1           |
| Gigabyte Technology  | H81-D3                                           | 2014 | 5       | 36      | 1           |
| eMachines            | ET1331G                                          | 2009 | 5       | 36      | 1           |
| Biostar              | NF520D3                                          | 2009 | 5       | 36      | 1           |
| ASRock               | P4i945GC                                         | 2009 | 5       | 36      | 1           |
| MSI                  | MS-7B47                                          | 2018 | 5       | 35      | 1           |
| Hewlett-Packard      | Z240 SFF Workstation                             | 2016 | 5       | 35      | 1           |
| ASRock               | QC5000M                                          | 2015 | 5       | 35      | 1           |
| Intel                | DH77KC AAG39641-400                              | 2012 | 5       | 35      | 1           |
| ASUSTek Computer     | F2A55-M LK2 PLUS                                 | 2012 | 5       | 35      | 1           |
| Gigabyte Technology  | G41MT-USB3                                       | 2011 | 5       | 35      | 1           |
| ASRock               | A780LM-S                                         | 2009 | 5       | 35      | 1           |
| Fujitsu              | ESPRIMO P920                                     | 2014 | 5       | 34      | 1           |
| Gateway              | SX2185                                           | 2013 | 5       | 34      | 1           |
| ASUSTek Computer     | M4N68T V2                                        | 2010 | 5       | 34      | 1           |
| MSI                  | MS-7368                                          | 2007 | 5       | 34      | 1           |
| Gigabyte Technology  | 945PL-S3                                         | 2006 | 5       | 34      | 1           |
| ECS                  | P4M890T-M                                        | 2006 | 5       | 34      | 1           |
| Hewlett-Packard      | OMEN by HP Desktop PC 870-2XX                    | 2018 | 5       | 33      | 1           |
| Fujitsu              | ESPRIMO E720                                     | 2014 | 5       | 33      | 1           |
| ASRock               | Z77 Extreme4-M                                   | 2012 | 5       | 33      | 1           |
| ASRock               | FM2A55M-DGS                                      | 2012 | 5       | 33      | 1           |
| ASRock               | H67M/SI                                          | 2010 | 5       | 33      | 1           |
| ASRock               | B460 Phantom Gaming 4                            | 2020 | 5       | 32      | 1           |
| ASUSTek Computer     | PRIME H310M-E/BR                                 | 2018 | 5       | 32      | 1           |
| MSI                  | MS-7923                                          | 2014 | 5       | 32      | 1           |
| MSI                  | MS-7756                                          | 2012 | 5       | 32      | 1           |
| PCWare               | IPMH61R2                                         | 2010 | 5       | 32      | 1           |
| Positivo             | P5VD2-MX                                         | 2007 | 5       | 32      | 1           |
| ASUSTek Computer     | K8N                                              | 2005 | 5       | 32      | 1           |
| Dell                 | Dimension 8400                                   | 2004 | 5       | 32      | 1           |
| Dell                 | OptiPlex 3080                                    | 2020 | 5       | 31      | 1           |
| ASUSTek Computer     | PRIME H410M-A                                    | 2020 | 5       | 31      | 1           |
| Itautec              | Infoway ST-4254                                  | 2009 | 5       | 31      | 1           |
| INTELBRAS            | IE-G31TM7                                        | 2009 | 5       | 31      | 1           |
| PCChips              | P17G                                             | 2007 | 5       | 31      | 1           |
| ASUSTek Computer     | P5GC-MX/CKD/POST/SI                              | 2007 | 5       | 31      | 1           |
| ASRock               | ALiveNF6P-VSTA                                   | 2007 | 5       | 31      | 1           |
| Gigabyte Technology  | nVidia-nForce2                                   | 2003 | 5       | 31      | 1           |
| OEM                  | G41 775 ICH7 8712                                | 2019 | 5       | 30      | 1           |
| LORD ELECTRONICS     | LORD G4x 775 ICH7 8712 As Design                 | 2016 | 5       | 30      | 1           |
| Hewlett-Packard      | ProDesk 400 G2 MINI                              | 2016 | 5       | 30      | 1           |
| Gigabyte Technology  | 8PE800                                           | 2003 | 5       | 30      | 1           |
| PCWare               | IPMH61G1                                         | 2012 | 5       | 29      | 1           |
| ASRock               | H110M-DVS R3.0                                   | 2016 | 5       | 28      | 1           |
| PCWare               | IPMH61R1                                         | 2011 | 5       | 28      | 1           |
| Intel                | D510MO AAE76523-403                              | 2010 | 5       | 28      | 1           |
| ASUSTek Computer     | P5RD1-VM                                         | 2005 | 5       | 28      | 1           |
| ASUSTek Computer     | PRIME H410M-K                                    | 2020 | 5       | 27      | 1           |
| Acer                 | Veriton ES2710G                                  | 2017 | 5       | 27      | 1           |
| Lenovo               | ThinkCentre M91p 0266RZ1                         | 2012 | 5       | 26      | 1           |
| ASUSTek Computer     | P5SD2-VM                                         | 2007 | 5       | 23      | 1           |
| ASRock               | EP2C602-4L/D16                                   | 2014 | 4       | 107     | 1           |
| Huanan               | X79-8D VAA31                                     | 2019 | 4       | 102     | 1           |
| Alienware            | Aurora-R4                                        | 2013 | 4       | 85      | 1           |
| Supermicro           | SYS-5038MD-H24TRF-OS012                          | 2018 | 4       | 82      | 1           |
| Intel                | DX58SO2 AAG10925-205                             | 2010 | 4       | 77      | 1           |
| ASRock               | X58 Extreme3                                     | 2010 | 4       | 76      | 1           |
| MSI                  | MS-7C59                                          | 2019 | 4       | 70      | 1           |
| ASUSTek Computer     | P6T DELUXE                                       | 2008 | 4       | 70      | 1           |
| Foxconn              | H55M-S                                           | 2010 | 4       | 61      | 1           |
| ASUSTek Computer     | Pro WS WRX80E-SAGE SE WIFI                       | 2020 | 4       | 60      | 1           |
| ASUSTek Computer     | ROG STRIX Z590-F GAMING WIFI                     | 2021 | 4       | 58      | 1           |
| Biostar              | TH55B HD                                         | 2009 | 4       | 57      | 1           |
| MSI                  | MS-7B50                                          | 2019 | 4       | 56      | 1           |
| ASUSTek Computer     | M51BC                                            | 2013 | 4       | 56      | 1           |
| ASUSTek Computer     | ROG Maximus XII FORMULA                          | 2020 | 4       | 53      | 1           |
| MSI                  | MS-7346                                          | 2007 | 4       | 52      | 1           |
| Gigabyte Technology  | Z490I AORUS ULTRA                                | 2020 | 4       | 51      | 1           |
| ASRock               | AB350M-HDV R3.0                                  | 2018 | 4       | 51      | 1           |
| Foxconn              | H55MXV Series                                    | 2010 | 4       | 51      | 1           |
| Acer                 | Veriton M680G                                    | 2010 | 4       | 51      | 1           |
| Gigabyte Technology  | X38-DQ6                                          | 2008 | 4       | 51      | 1           |
| MSI                  | MS-7B12                                          | 2018 | 4       | 50      | 1           |
| ASUSTek Computer     | M4A89TD PRO USB3                                 | 2010 | 4       | 50      | 1           |
| MSI                  | MS-7992                                          | 2016 | 4       | 49      | 1           |
| Gigabyte Technology  | Z270X-UD5                                        | 2016 | 4       | 49      | 1           |
| ASRock               | 990FX Professional                               | 2011 | 4       | 49      | 1           |
| MSI                  | A320M-HDV R4.0                                   | 2019 | 4       | 48      | 1           |
| MSI                  | MS-7921                                          | 2014 | 4       | 48      | 1           |
| ASUSTek Computer     | P8C WS                                           | 2012 | 4       | 48      | 1           |
| MSI                  | MS-7815                                          | 2013 | 4       | 47      | 1           |
| Gigabyte Technology  | P67A-UD4-B3                                      | 2012 | 4       | 47      | 1           |
| Dell                 | Precision WorkStation 690                        | 2007 | 4       | 47      | 1           |
| Hewlett-Packard      | EliteDesk 705 G3 SFF                             | 2017 | 4       | 46      | 1           |
| Hewlett-Packard      | 505B Microtower PC                               | 2010 | 4       | 45      | 1           |
| Gigabyte Technology  | Z270X-Gaming 7                                   | 2017 | 4       | 44      | 1           |
| ASRock               | FM2A88X Pro3+                                    | 2014 | 4       | 44      | 1           |
| ASUSTek Computer     | P8Z77-V PRO/THUNDERBOLT                          | 2012 | 4       | 44      | 1           |
| ASUSTek Computer     | Maximus IV Extreme-Z                             | 2011 | 4       | 44      | 1           |
| ASRock               | A790GXH/128M                                     | 2009 | 4       | 44      | 1           |
| Intel                | DG965RY AAD41691-205                             | 2006 | 4       | 43      | 1           |
| Gigabyte Technology  | H170-HD3-CF                                      | 2015 | 4       | 42      | 1           |
| Gigabyte Technology  | Z590 AORUS ELITE AX                              | 2021 | 4       | 41      | 1           |
| Hewlett-Packard      | Pavilion Desktop 590-a0xxx                       | 2018 | 4       | 41      | 1           |
| Fujitsu              | ESPRIMO P7935                                    | 2010 | 4       | 41      | 1           |
| ASUSTek Computer     | P5Q-WS                                           | 2008 | 4       | 41      | 1           |
| MSI                  | MS-7C76                                          | 2020 | 4       | 40      | 1           |
| ASRock               | Z370 Killer SLI/ac                               | 2017 | 4       | 40      | 1           |
| Gigabyte Technology  | H81M-D3H                                         | 2014 | 4       | 40      | 1           |
| Lenovo               | 70A4001MUX ThinkServer TS140                     | 2013 | 4       | 40      | 1           |
| ASRock               | A770DE+                                          | 2009 | 4       | 40      | 1           |
| ASUSTek Computer     | ROG STRIX B460-F GAMING                          | 2020 | 4       | 39      | 1           |
| Gigabyte Technology  | B365MHD3                                         | 2019 | 4       | 39      | 1           |
| ASRock               | Z87M Extreme4                                    | 2013 | 4       | 39      | 1           |
| ASRock               | Z77 Professional                                 | 2013 | 4       | 39      | 1           |
| ASUSTek Computer     | P8H61/USB3                                       | 2011 | 4       | 39      | 1           |
| ASUSTek Computer     | ET2700I                                          | 2011 | 4       | 39      | 1           |
| Gigabyte Technology  | EP45-UD3L                                        | 2008 | 4       | 39      | 1           |
| Foxconn              | OEM                                              | 2006 | 4       | 39      | 1           |
| Gigabyte Technology  | B360M-HD3                                        | 2018 | 4       | 38      | 1           |
| Gigabyte Technology  | Z170M-D3H                                        | 2016 | 4       | 38      | 1           |
| Gigabyte Technology  | Z87P-D3                                          | 2013 | 4       | 38      | 1           |
| Dell                 | Inspiron 560s                                    | 2010 | 4       | 38      | 1           |
| ASUSTek Computer     | PRIME H570-PLUS                                  | 2021 | 4       | 37      | 1           |
| Hewlett-Packard      | Z2 Tower G4 Workstation                          | 2019 | 4       | 37      | 1           |
| Hewlett-Packard      | EliteDesk 800 G5 Desktop Mini                    | 2019 | 4       | 37      | 1           |
| ASRock               | Z97X Killer                                      | 2014 | 4       | 37      | 1           |
| ASRock               | P45DE3                                           | 2010 | 4       | 37      | 1           |
| Acer                 | Aspire M1201                                     | 2008 | 4       | 37      | 1           |
| Intel                | DG31PR AAD97573-204                              | 2007 | 4       | 37      | 1           |
| Hewlett-Packard      | EliteDesk 705 G2 MINI                            | 2018 | 4       | 36      | 1           |
| Acer                 | Aspire XC-710                                    | 2015 | 4       | 36      | 1           |
| Positivo             | POS-PIH81DI                                      | 2014 | 4       | 36      | 1           |
| Intel                | DH55PJ AAE93812-303                              | 2010 | 4       | 36      | 1           |
| ECS                  | GF7050VT-M5                                      | 2008 | 4       | 36      | 1           |
| Hewlett-Packard      | Slim Desktop S01-pF1xxx                          | 2020 | 4       | 35      | 1           |
| Chuwi                | LarkBox Pro                                      | 2020 | 4       | 35      | 1           |
| Biostar              | A68MHE                                           | 2019 | 4       | 35      | 1           |
| ASRock               | H370M Pro4                                       | 2018 | 4       | 35      | 1           |
| ASUSTek Computer     | B150 PRO GAMING/AURA                             | 2016 | 4       | 35      | 1           |
| Dell                 | OptiPlex XE2                                     | 2014 | 4       | 35      | 1           |
| Pegatron             | NM70-P1/ODM                                      | 2013 | 4       | 35      | 1           |
| Gigabyte Technology  | B85M-D3V                                         | 2013 | 4       | 35      | 1           |
| ASUSTek Computer     | A55M-E                                           | 2013 | 4       | 35      | 1           |
| Lenovo               | ThinkCentre M91p 4480B1U                         | 2012 | 4       | 35      | 1           |
| Hewlett-Packard      | ProLiant ML110 G7                                | 2011 | 4       | 35      | 1           |
| ASUSTek Computer     | P8H61-V                                          | 2011 | 4       | 35      | 1           |
| Nvidia               | MCP79                                            | 2009 | 4       | 35      | 1           |
| Fujitsu              | ESPRIMO P2550                                    | 2009 | 4       | 35      | 1           |
| Medion               | MS-7255                                          | 2006 | 4       | 35      | 1           |
| Acer                 | ASE380/AST180/APM8                               | 2006 | 4       | 35      | 1           |
| Hewlett-Packard      | Z2 Tower G5 Workstation                          | 2020 | 4       | 34      | 1           |
| ASUSTek Computer     | TUF GAMING B460-PRO                              | 2020 | 4       | 34      | 1           |
| Gigabyte Technology  | G1.Sniper B6                                     | 2015 | 4       | 34      | 1           |
| Gigabyte Technology  | F2A58M-DS2                                       | 2014 | 4       | 34      | 1           |
| ASUSTek Computer     | M52AD_M12AD_A_F_K31AD                            | 2014 | 4       | 34      | 1           |
| Hewlett-Packard      | Pro 3330 MT                                      | 2012 | 4       | 34      | 1           |
| Gigabyte Technology  | P67A-UD3-B3                                      | 2011 | 4       | 34      | 1           |
| Thirdwave            | Prime Series                                     | 2010 | 4       | 34      | 1           |
| Fujitsu Siemens      | D2841-A1                                         | 2009 | 4       | 34      | 1           |
| Hewlett-Packard      | EG194AA-ABA A1250N                               | 2005 | 4       | 34      | 1           |
| ASUSTek Computer     | A8N-SLI DELUXE                                   | 2005 | 4       | 34      | 1           |
| ASUSTek Computer     | A8N-SLI                                          | 2005 | 4       | 34      | 1           |
| ASUSTek Computer     | K8V-MX                                           | 2004 | 4       | 34      | 1           |
| ASUSTek Computer     | A7N8X-X                                          | 2004 | 4       | 34      | 1           |
| MSI                  | MS-7C86                                          | 2020 | 4       | 33      | 1           |
| ASUSTek Computer     | TUF GAMING B460-PLUS                             | 2020 | 4       | 33      | 1           |
| Hewlett-Packard      | ProDesk 400 G4 SFF                               | 2017 | 4       | 33      | 1           |
| ASUSTek Computer     | H61M-F                                           | 2013 | 4       | 33      | 1           |
| PCWare               | APMCP61-D3                                       | 2011 | 4       | 33      | 1           |
| Biostar              | N68S3+                                           | 2010 | 4       | 33      | 1           |
| Dell                 | Vostro 320                                       | 2009 | 4       | 33      | 1           |
| Hewlett-Packard      | Compaq dx2450 Microtower PC                      | 2008 | 4       | 33      | 1           |
| WinFast              | NF4K8MC                                          | 2005 | 4       | 33      | 1           |
| ASUSTek Computer     | STRIX B250G GAMING                               | 2017 | 4       | 32      | 1           |
| MSI                  | MS-7995                                          | 2016 | 4       | 32      | 1           |
| Gigabyte Technology  | H61M-USB3H                                       | 2013 | 4       | 32      | 1           |
| MSI                  | MS-7507                                          | 2008 | 4       | 32      | 1           |
| Gigabyte Technology  | 8I915PL-G                                        | 2005 | 4       | 32      | 1           |
| ASUSTek Computer     | P5GL-MX                                          | 2005 | 4       | 32      | 1           |
| Gigabyte Technology  | 8IG1000MK                                        | 2004 | 4       | 32      | 1           |
| ASRock               | J5040-ITX                                        | 2020 | 4       | 31      | 1           |
| ASUSTek Computer     | H110M-K D3                                       | 2015 | 4       | 31      | 1           |
| Hewlett-Packard      | ProDesk 490 G2 MT                                | 2014 | 4       | 31      | 1           |
| Lenovo               | IdeaCentre K330                                  | 2010 | 4       | 31      | 1           |
| Hewlett-Packard      | rp5700 Business System                           | 2007 | 4       | 31      | 1           |
| Intel                | DH87RL AAG74240-402                              | 2013 | 4       | 30      | 1           |
| ASRock               | H77M-ITX                                         | 2012 | 4       | 30      | 1           |
| eMachines            | EL1852G                                          | 2011 | 4       | 30      | 1           |
| Gigabyte Technology  | H110M-S2-CF                                      | 2016 | 4       | 29      | 1           |
| ASUSTek Computer     | K5130                                            | 2013 | 4       | 29      | 1           |
| Gigabyte Technology  | H61M-DS2 REV 1.2                                 | 2011 | 4       | 29      | 1           |
| Megaware             | MW-G41T-M7                                       | 2010 | 4       | 29      | 1           |
| Intel                | D525MW AAE93082-301                              | 2010 | 4       | 29      | 1           |
| ECS                  | G41T-R3                                          | 2010 | 4       | 29      | 1           |
| ASRock               | 775i65G                                          | 2009 | 4       | 29      | 1           |
| ASRock               | 775VM800                                         | 2005 | 4       | 29      | 1           |
| Gigabyte Technology  | H410M S2H                                        | 2020 | 4       | 28      | 1           |
| ASRock               | H110M-HDS R3.0                                   | 2016 | 4       | 28      | 1           |
| Acer                 | Elena                                            | 2011 | 4       | 28      | 1           |
| ASUSTek Computer     | H110M-CS                                         | 2016 | 4       | 26      | 1           |
| Aquarius             | S20 K29                                          | 2020 | 4       | 25      | 1           |
| Intel                | DH61HO AAG62445-102                              | 2012 | 4       | 25      | 1           |
| SiS Technology       | SiS-648FX                                        | 2004 | 4       | 24      | 1           |
| ASRock               | X99 Professional Gaming i7                       | 2016 | 3       | 112     | 1           |
| ASRock               | X99 Extreme11                                    | 2014 | 3       | 111     | 1           |
| MSI                  | MS-7A20                                          | 2018 | 3       | 106     | 1           |
| Intel                | X99 V102A                                        | 2020 | 3       | 103     | 1           |
| Gigabyte Technology  | X99P-SLI-CF                                      | 2015 | 3       | 103     | 1           |
| ASRock               | X99 Extreme3                                     | 2014 | 3       | 96      | 1           |
| Huanan               | X79 V2.47                                        | 2017 | 3       | 92      | 1           |
| Huanan               | X58                                              | 2019 | 3       | 85      | 1           |
| Intel                | DX79SI AAG28808-600                              | 2012 | 3       | 80      | 1           |
| Gigabyte Technology  | G1.Assassin                                      | 2011 | 3       | 76      | 1           |
| ASUSTek Computer     | P7P55 WS SUPERCOMPUTER                           | 2010 | 3       | 70      | 1           |
| ASUSTek Computer     | P7H57D-V EVO                                     | 2009 | 3       | 62      | 1           |
| MSI                  | MS-7A57                                          | 2017 | 3       | 59      | 1           |
| Gigabyte Technology  | P55-UD3R                                         | 2010 | 3       | 59      | 1           |
| Lenovo               | ThinkStation D20 415892G                         | 2014 | 3       | 58      | 1           |
| Gigabyte Technology  | Z490 AORUS XTREME                                | 2020 | 3       | 55      | 1           |
| Dell                 | XPS720                                           | 2007 | 3       | 55      | 1           |
| ASUSTek Computer     | StrikerExtreme                                   | 2009 | 3       | 54      | 1           |
| ASUSTek Computer     | ROG ZENITH EXTREME ALPHA                         | 2019 | 3       | 53      | 1           |
| Alienware            | Aurora R11                                       | 2020 | 3       | 52      | 1           |
| Hewlett-Packard      | Pavilion Power Desktop 580-1xx                   | 2017 | 3       | 51      | 1           |
| ASUSTek Computer     | M4N78-VM                                         | 2009 | 3       | 51      | 1           |
| MSI                  | MS-7588                                          | 2009 | 3       | 50      | 1           |
| ASRock               | Z370 Taichi                                      | 2019 | 3       | 49      | 1           |
| MSI                  | MS-7577                                          | 2010 | 3       | 49      | 1           |
| Gigabyte Technology  | GA-N650SLI-DS4L                                  | 2007 | 3       | 49      | 1           |
| MSI                  | MS-7697                                          | 2011 | 3       | 48      | 1           |
| ASUSTek Computer     | P5Q3 DELUXE                                      | 2009 | 3       | 48      | 1           |
| Gigabyte Technology  | A520M DS3H                                       | 2020 | 3       | 47      | 1           |
| ASUSTek Computer     | Maximus IX CODE                                  | 2017 | 3       | 47      | 1           |
| ASRock               | Z170 OC Formula                                  | 2016 | 3       | 47      | 1           |
| MSI                  | MS-7612                                          | 2010 | 3       | 47      | 1           |
| Gigabyte Technology  | B550 VISION D                                    | 2020 | 3       | 46      | 1           |
| ASUSTek Computer     | M2N32 WS Professional                            | 2006 | 3       | 46      | 1           |
| T-bao                | MINI PC                                          | 2020 | 3       | 45      | 1           |
| Hewlett-Packard      | 1155 18.5 AiO Business PC                        | 2013 | 3       | 45      | 1           |
| Biostar              | A880GU3                                          | 2010 | 3       | 45      | 1           |
| Acer                 | Aspire M7811                                     | 2009 | 3       | 45      | 1           |
| Hewlett-Packard      | Pavilion Gaming Desktop 790-00xx                 | 2019 | 3       | 44      | 1           |
| Hewlett-Packard      | OMEN by HP Desktop PC 880-p1xx                   | 2017 | 3       | 44      | 1           |
| MSI                  | MS-7968                                          | 2015 | 3       | 44      | 1           |
| Intel                | DZ77GA-70K AAG39009-401                          | 2013 | 3       | 44      | 1           |
| Fujitsu              | ESPRIMO E705                                     | 2011 | 3       | 44      | 1           |
| Gigabyte Technology  | GA-M51GM-S2G                                     | 2006 | 3       | 44      | 1           |
| ASUSTek Computer     | A8V-MX                                           | 2005 | 3       | 44      | 1           |
| WeiBu                | WTGLKC1R120 SD-BS-CJ41G-M-101-B 12/09/2019 18... | 2019 | 3       | 43      | 1           |
| Hewlett-Packard      | Slim Desktop S01-aF0xxx                          | 2019 | 3       | 43      | 1           |
| Intel                | DQ965GF AAD41676-601                             | 2007 | 3       | 43      | 1           |
| Intel                | DQ35JO AAD82085-801                              | 2007 | 3       | 43      | 1           |
| MSI                  | MS-7D15                                          | 2021 | 3       | 42      | 1           |
| ASRock               | A320M                                            | 2017 | 3       | 42      | 1           |
| Alienware            | Aurora-R3                                        | 2011 | 3       | 42      | 1           |
| ASRock               | 890GX Extreme3                                   | 2010 | 3       | 42      | 1           |
| Gigabyte Technology  | GA-MA790X-UD4P                                   | 2009 | 3       | 42      | 1           |
| ATComputers          | TRILINE PROFI                                    | 2009 | 3       | 42      | 1           |
| Supermicro           | X7DVL                                            | 2008 | 3       | 42      | 1           |
| Gigabyte Technology  | X48-DQ6                                          | 2008 | 3       | 42      | 1           |
| Gigabyte Technology  | P35-DQ6                                          | 2008 | 3       | 42      | 1           |
| Intel                | DG965SS AAD41678-304                             | 2006 | 3       | 42      | 1           |
| Lenovo               | H50-55 90BG003LUS                                | 2015 | 3       | 41      | 1           |
| Hewlett-Packard      | 700-216                                          | 2014 | 3       | 41      | 1           |
| ASRock               | N68-GS4/USB3 FX                                  | 2014 | 3       | 41      | 1           |
| Hewlett-Packard      | p6-2120                                          | 2012 | 3       | 41      | 1           |
| Foxconn              | NT-A2400&NT-A3500                                | 2011 | 3       | 41      | 1           |
| Gateway              | LX6810-01                                        | 2008 | 3       | 41      | 1           |
| Acer                 | Aspire M7711                                     | 2008 | 3       | 41      | 1           |
| ASRock               | A780GXE/128M                                     | 2008 | 3       | 41      | 1           |
| Gigabyte Technology  | B460MAORUSPRO                                    | 2020 | 3       | 40      | 1           |
| ASUSTek Computer     | ProArt Z490-CREATOR 10G                          | 2020 | 3       | 40      | 1           |
| Gigabyte Technology  | AB350M-D3V                                       | 2017 | 3       | 40      | 1           |
| Acer                 | Aspire XC-105                                    | 2013 | 3       | 40      | 1           |
| Gigabyte Technology  | F2A85X-D3H                                       | 2012 | 3       | 40      | 1           |
| ECS                  | A55F-M4                                          | 2012 | 3       | 40      | 1           |
| Hewlett-Packard      | p6-2133w                                         | 2011 | 3       | 40      | 1           |
| Supermicro           | C2SBA                                            | 2008 | 3       | 40      | 1           |
| Dell                 | XPS430                                           | 2008 | 3       | 40      | 1           |
| Fujitsu Siemens      | MS-7293VP                                        | 2006 | 3       | 40      | 1           |
| BESSTAR Tech         | UM300                                            | 2020 | 3       | 39      | 1           |
| ASUSTek Computer     | PRO B460M-C                                      | 2020 | 3       | 39      | 1           |
| ASUSTek Computer     | A55BM-PLUS                                       | 2015 | 3       | 39      | 1           |
| Positivo             | Master D570a                                     | 2014 | 3       | 39      | 1           |
| Lenovo               | ThinkCentre M93p 10A8S0CE11                      | 2014 | 3       | 39      | 1           |
| Hewlett-Packard      | 9100                                             | 2009 | 3       | 39      | 1           |
| Biostar              | TA785GE 128M                                     | 2009 | 3       | 39      | 1           |
| Foxconn              | P35                                              | 2008 | 3       | 39      | 1           |
| Biostar              | TF570 SLI A2+                                    | 2007 | 3       | 39      | 1           |
| ECS                  | K8M800-M2                                        | 2005 | 3       | 39      | 1           |
| Lenovo               | IdeaCentre K300                                  |      | 3       | 39      | 1           |
| ASUSTek Computer     | PRIME B560M-A                                    | 2021 | 3       | 38      | 1           |
| Google               | Teemo                                            | 2020 | 3       | 38      | 1           |
| Gigabyte Technology  | H270M-D3H                                        | 2016 | 3       | 38      | 1           |
| Gigabyte Technology  | B85-HD3-A                                        | 2015 | 3       | 38      | 1           |
| ASUSTek Computer     | Maximus IV GENE-Z                                | 2012 | 3       | 38      | 1           |
| Itautec              | Infoway ST-4271                                  | 2010 | 3       | 38      | 1           |
| Intel                | DG41WV AAE90316-104                              | 2010 | 3       | 38      | 1           |
| Gigabyte Technology  | GA-MA785G-UD3H                                   | 2009 | 3       | 38      | 1           |
| Gigabyte Technology  | G33M-S2                                          | 2008 | 3       | 38      | 1           |
| ASUSTek Computer     | IPN73-BA                                         | 2008 | 3       | 38      | 1           |
| MSI                  | MS-7327                                          | 2007 | 3       | 38      | 1           |
| Hewlett-Packard      | Compaq dc5100 SFF(PM215AV)                       | 2004 | 3       | 38      | 1           |
| ASRock               | FM2A55 Pro+                                      | 2014 | 3       | 37      | 1           |
| Hewlett-Packard      | p6533w                                           | 2010 | 3       | 37      | 1           |
| ASRock               | ION3D-HT                                         | 2010 | 3       | 37      | 1           |
| ASRock               | M3A785GXH/128M                                   | 2009 | 3       | 37      | 1           |
| Dell                 | Studio Slim 540s                                 | 2008 | 3       | 37      | 1           |
| Gigabyte Technology  | GA-VM900M                                        | 2007 | 3       | 37      | 1           |
| Gigabyte Technology  | GA-73PVM-S2                                      | 2007 | 3       | 37      | 1           |
| ASRock               | H310M-ITX/ac                                     | 2018 | 3       | 36      | 1           |
| Biostar              | A68N-5545                                        | 2016 | 3       | 36      | 1           |
| MSI                  | MS-7733                                          | 2013 | 3       | 36      | 1           |
| Intel                | DG43GT AAE62768-300                              | 2009 | 3       | 36      | 1           |
| ASRock               | P4VM900-SATA2                                    | 2007 | 3       | 36      | 1           |
| Gigabyte Technology  | BRi7(H)-10710                                    | 2019 | 3       | 35      | 1           |
| ASUSTek Computer     | TUF H370-PRO GAMING                              | 2018 | 3       | 35      | 1           |
| ASUSTek Computer     | TUF B360M-E GAMING                               | 2018 | 3       | 35      | 1           |
| ASRock               | H170 Pro4                                        | 2016 | 3       | 35      | 1           |
| ASRock               | E3C226D2I                                        | 2014 | 3       | 35      | 1           |
| Intel                | DP67DE AAG10217-300                              | 2011 | 3       | 35      | 1           |
| Hewlett-Packard      | Pro 3405 Series                                  | 2011 | 3       | 35      | 1           |
| ASUSTek Computer     | M4A78LT-M LX                                     | 2011 | 3       | 35      | 1           |
| ASRock               | H61DE/S3                                         | 2011 | 3       | 35      | 1           |
| langchao             | 12345                                            | 2010 | 3       | 35      | 1           |
| Dell                 | Inspiron 545s                                    | 2009 | 3       | 35      | 1           |
| ASRock               | G965M-S                                          | 2009 | 3       | 35      | 1           |
| ASRock               | 4Core1600-GLAN/M                                 | 2009 | 3       | 35      | 1           |
| ATComputers          | AC OFFICEPRO                                     | 2008 | 3       | 35      | 1           |
| MSI                  | MS-7312                                          | 2007 | 3       | 35      | 1           |
| ASUSTek Computer     | P5VD2-MX SE                                      | 2007 | 3       | 35      | 1           |
| ASUSTek Computer     | P5V-VM-ULTRA                                     | 2006 | 3       | 35      | 1           |
| Gigabyte Technology  | Z590 UD AC                                       | 2021 | 3       | 34      | 1           |
| Intel                | KBL-R MRD                                        | 2018 | 3       | 34      | 1           |
| Dell                 | Inspiron 3470                                    | 2018 | 3       | 34      | 1           |
| ASRock               | Z370 Killer SLI                                  | 2017 | 3       | 34      | 1           |
| Intel                | DB75EN AAG39650-400                              | 2012 | 3       | 34      | 1           |
| ASUSTek Computer     | P5G41T-M                                         | 2010 | 3       | 34      | 1           |
| TYAN Computer        | S2925                                            | 2007 | 3       | 34      | 1           |
| Foxconn              | P4M890-8237                                      | 2006 | 3       | 34      | 1           |
| Gigabyte Technology  | K8M800-8237                                      | 2004 | 3       | 34      | 1           |
| Protectli            | FW6                                              | 2019 | 3       | 33      | 1           |
| Alienware            | ASM201                                           | 2017 | 3       | 33      | 1           |
| Gigabyte Technology  | Z170M-D3H DDR3                                   | 2016 | 3       | 33      | 1           |
| ASRock               | Z97M Pro4                                        | 2014 | 3       | 33      | 1           |
| ASRock               | AM1B-M                                           | 2014 | 3       | 33      | 1           |
| Foxconn              | H61MXE/-S/-V/-K                                  | 2012 | 3       | 33      | 1           |
| Pegatron             | PEGATRON                                         | 2011 | 3       | 33      | 1           |
| Gigabyte Technology  | P67-DS3-B3                                       | 2011 | 3       | 33      | 1           |
| Intel                | DG41CN AAE82429-102                              | 2009 | 3       | 33      | 1           |
| Gigabyte Technology  | EP41-US3L                                        | 2009 | 3       | 33      | 1           |
| Digitron             | IPM31G                                           | 2009 | 3       | 33      | 1           |
| Compaq               | AY030AA-ABA CQ5320F                              | 2009 | 3       | 33      | 1           |
| ASRock               | AM2NF3-VSTA                                      | 2009 | 3       | 33      | 1           |
| Hewlett-Packard      | KJ379AA-ABA a6400f                               | 2008 | 3       | 33      | 1           |
| Samsung Electronics  | DeskTop System                                   | 2007 | 3       | 33      | 1           |
| Gigabyte Technology  | M61VME-S2                                        | 2006 | 3       | 33      | 1           |
| ASRock               | K8Upgrade-VM800                                  | 2005 | 3       | 33      | 1           |
| ASUSTek Computer     | PRIME B560-PLUS                                  | 2021 | 3       | 32      | 1           |
| Hewlett-Packard      | 300-0xx                                          | 2014 | 3       | 32      | 1           |
| MSI                  | CU-7592                                          | 2011 | 3       | 32      | 1           |
| Acer                 | Veriton S670G                                    | 2009 | 3       | 32      | 1           |
| Biostar              | NF61D-A2                                         | 2007 | 3       | 32      | 1           |
| ASRock               | ConRoe1333-GLAN                                  | 2007 | 3       | 32      | 1           |
| EPoX Computer        | nForce3 DDR: 8KDA3I Series                       | 2005 | 3       | 32      | 1           |
| ASUSTek Computer     | P5GD1                                            | 2005 | 3       | 32      | 1           |
| ASUSTek Computer     | P4P800-MX                                        | 2004 | 3       | 32      | 1           |
| Hewlett-Packard      | ProDesk 400 G5 MT                                | 2018 | 3       | 31      | 1           |
| Fujitsu              | ESPRIMO_D556                                     | 2018 | 3       | 31      | 1           |
| Hewlett-Packard      | ProDesk 400 G4 MT                                | 2017 | 3       | 31      | 1           |
| ASUSTek Computer     | P10S-I Series                                    | 2017 | 3       | 31      | 1           |
| Gigabyte Technology  | B150M-HD3-CF                                     | 2016 | 3       | 31      | 1           |
| ASUSTek Computer     | Z170M-E D3                                       | 2015 | 3       | 31      | 1           |
| Shuttle              | XS35V3                                           | 2012 | 3       | 31      | 1           |
| Fujitsu              | ESPRIMO E910                                     | 2012 | 3       | 31      | 1           |
| Qbex                 | QBEX-G41T-M7                                     | 2011 | 3       | 31      | 1           |
| Shuttle              | SG41                                             | 2010 | 3       | 31      | 1           |
| ASUSTek Computer     | P5RD2-VM                                         | 2006 | 3       | 31      | 1           |
| EPoX Computer        | nForce4 DDR: 9NPA7I / 9NPAI Series               | 2005 | 3       | 31      | 1           |
| ASRock               | 775Dual-880Pro                                   | 2005 | 3       | 31      | 1           |
| Gigabyte Technology  | H110M-DS2 DDR3-CF                                | 2015 | 3       | 30      | 1           |
| Acer                 | Veriton M200-H81                                 | 2014 | 3       | 30      | 1           |
| ASUSTek Computer     | A58M-A/BR                                        | 2014 | 3       | 30      | 1           |
| ASRock               | B75M                                             | 2012 | 3       | 30      | 1           |
| Hewlett-Packard      | 120-1026la                                       | 2011 | 3       | 30      | 1           |
| Acer                 | Aspire X1420                                     | 2011 | 3       | 30      | 1           |
| Intel                | DG41TY AAE47335-302                              | 2009 | 3       | 30      | 1           |
| DEPO Computers       | 945GCT-M5                                        | 2008 | 3       | 30      | 1           |
| ASUSTek Computer     | M2N68-CM                                         | 2008 | 3       | 30      | 1           |
| EPoX Computer        | MCP61S DDR2: AGF6110-M Series                    | 2006 | 3       | 30      | 1           |
| Intel                | D865PERL AAC27648-211                            | 2005 | 3       | 30      | 1           |
| SiS Technology       | SiS-661                                          | 2004 | 3       | 30      | 1           |
| ASUSTek Computer     | H61M-D                                           | 2013 | 3       | 29      | 1           |
| Intel                | D945GCLF2 AAE46416-106                           | 2010 | 3       | 29      | 1           |
| Foxconn              | G41MXP/G41MXP-V                                  | 2010 | 3       | 29      | 1           |
| eMachines            | EL1300                                           | 2009 | 3       | 29      | 1           |
| Intel                | D410PT AAE76528-400                              | 2009 | 3       | 29      | 1           |
| Positivo             | POS-EC945AL                                      | 2008 | 3       | 29      | 1           |
| Hewlett-Packard      | RX900AA-ABA a6010n                               | 2007 | 3       | 29      | 1           |
| Gigabyte Technology  | 8I915GMF                                         | 2004 | 3       | 29      | 1           |
| Biostar              | H310MHP                                          | 2019 | 3       | 28      | 1           |
| Positivo             | C6200                                            | 2018 | 3       | 28      | 1           |
| Intel                | DH67BL AAG10189-213                              | 2012 | 3       | 28      | 1           |
| ECS                  | G41T-M12                                         | 2010 | 3       | 28      | 1           |
| Arbyte Computers     | Arbyte Forte                                     | 2009 | 3       | 28      | 1           |
| Intel                | DG31GL AAE33912-200                              | 2008 | 3       | 28      | 1           |
| Gigabyte Technology  | 945PL-S3P                                        | 2007 | 3       | 28      | 1           |
| MSI                  | MS-7104                                          | 2005 | 3       | 28      | 1           |
| ASRock               | H470M-HDV                                        | 2021 | 3       | 27      | 1           |
| Google               | Zako                                             | 2020 | 3       | 27      | 1           |
| Hewlett-Packard      | 260-a010                                         | 2018 | 3       | 27      | 1           |
| American Megatrends  | K7S41GX                                          | 2006 | 3       | 27      | 1           |
| ASUSTek Computer     | ASUSPRO D642MF_D642MF                            | 2019 | 3       | 26      | 1           |
| Gigabyte Technology  | MZGLKBP-00                                       | 2018 | 3       | 26      | 1           |
| Gigabyte Technology  | J1900M-D2P                                       | 2014 | 3       | 26      | 1           |
| ASUSTek Computer     | H61M-A                                           | 2013 | 3       | 26      | 1           |
| Foxconn              | H61MXL-K                                         | 2012 | 3       | 26      | 1           |
| Gigabyte Technology  | GA-8SIMLH                                        | 2003 | 3       | 26      | 1           |
| ASUSTek Computer     | KFSN4-DRE/RS161                                  | 2007 | 3       | 24      | 1           |
| JINGSHA              | Board                                            | 2020 | 2       | 119     | 1           |
| Huanan               | X99 F8D V1.0                                     | 2020 | 2       | 119     | 1           |
| Gigabyte Technology  | X79-UD5                                          | 2012 | 2       | 116     | 1           |
| Huanan               | X99 F8D V2.2                                     | 2021 | 2       | 115     | 1           |
| Gigabyte Technology  | X99-UD7 WIFI-CF                                  | 2014 | 2       | 112     | 1           |
| ASRockRack           | EP2C612 WS                                       | 2016 | 2       | 108     | 1           |
| Supermicro           | SYS-7048A-T                                      | 2018 | 2       | 107     | 1           |
| ASRock               | X99 Extreme6                                     | 2018 | 2       | 107     | 1           |
| MSI                  | MS-7A54                                          | 2018 | 2       | 104     | 1           |
| ASRock               | X99X Killer                                      | 2015 | 2       | 104     | 1           |
| ASUSTek Computer     | ROG STRIX X299-XE GAMING                         | 2018 | 2       | 101     | 1           |
| Gigabyte Technology  | X99-UD3P-CF                                      | 2016 | 2       | 101     | 1           |
| ASRock               | X99M Extreme4                                    | 2016 | 2       | 100     | 1           |
| Gigabyte Technology  | X99M-Gaming 5                                    | 2018 | 2       | 99      | 1           |
| Gigabyte Technology  | G1.Assassin2                                     | 2012 | 2       | 95      | 1           |
| Fujitsu              | CELSIUS M740                                     | 2015 | 2       | 92      | 1           |
| ASUSTek Computer     | WS X299 PRO                                      | 2018 | 2       | 88      | 1           |
| Intel                | E5 M2L-8D                                        | 2019 | 2       | 87      | 1           |
| Intel                | DX79TO AAG28805-402                              | 2012 | 2       | 76      | 1           |
| Supermicro           | X8SAX                                            | 2010 | 2       | 73      | 1           |
| ASUSTek Computer     | P7P55D-E DELUXE                                  | 2009 | 2       | 70      | 1           |
| Intel                | DX58SO AAE29331-404                              | 2008 | 2       | 67      | 1           |
| Dell                 | XPS 730                                          | 2008 | 2       | 65      | 1           |
| PC Specailist        | Amd Am4 Gen3                                     | 2019 | 2       | 59      | 1           |
| ASUSTek Computer     | P7H55-USB3                                       | 2010 | 2       | 58      | 1           |
| ASUSTek Computer     | ROG STRIX TRX40-E GAMING                         | 2020 | 2       | 55      | 1           |
| Fujitsu              | CELSIUS W280                                     | 2010 | 2       | 55      | 1           |
| ASUSTek Computer     | P7Q57-M DO                                       | 2010 | 2       | 54      | 1           |
| ASRock               | Z270 Taichi                                      | 2018 | 2       | 53      | 1           |
| Mustek               | Mecer_X102                                       | 2010 | 2       | 53      | 1           |
| ASUSTek Computer     | ROG STRIX Z690-A GAMING WIFI D4                  | 2021 | 2       | 51      | 1           |
| ASUSTek Computer     | P7F-X Series                                     | 2010 | 2       | 51      | 1           |
| ASRockRack           | EPYCD8-2T                                        | 2019 | 2       | 50      | 1           |
| RM                   | RM DESKTOP 300                                   | 2010 | 2       | 50      | 1           |
| MSI                  | MS-7578                                          | 2010 | 2       | 50      | 1           |
| ASUSTek Computer     | KGPE-D16                                         | 2012 | 2       | 49      | 1           |
| Hewlett-Packard      | ProLiant ML150 G5                                | 2010 | 2       | 49      | 1           |
| ASUSTek Computer     | PRIME TRX40-PRO S                                | 2020 | 2       | 48      | 1           |
| Hewlett-Packard      | ProDesk 405 G4 Desktop Mini                      | 2019 | 2       | 48      | 1           |
| Alienware            | Aurora R9                                        | 2019 | 2       | 48      | 1           |
| ASUSTek Computer     | E3 PRO GAMING V5                                 | 2015 | 2       | 48      | 1           |
| Gigabyte Technology  | Z590 VISION G                                    | 2021 | 2       | 47      | 1           |
| Complet              | MY8313                                           | 2019 | 2       | 47      | 1           |
| Hewlett-Packard      | HPE-510f                                         | 2010 | 2       | 47      | 1           |
| ASRock               | 880GXH/USB3                                      | 2010 | 2       | 47      | 1           |
| Hewlett-Packard      | KC880AA-ABA m9150f                               | 2007 | 2       | 47      | 1           |
| ECS                  | LIVA ONE A320                                    | 2019 | 2       | 46      | 1           |
| Gigabyte Technology  | G1.Sniper Z87                                    | 2013 | 2       | 46      | 1           |
| ASUSTek Computer     | CROSSHAIR II FORMULA                             | 2011 | 2       | 46      | 1           |
| Hewlett-Packard      | 18-2003LA                                        | 2012 | 2       | 45      | 1           |
| Packard Bell         | Vegas2                                           | 2007 | 2       | 45      | 1           |
| BESSTAR Tech         | HM80                                             | 2021 | 2       | 44      | 1           |
| ASUSTek Computer     | ROG STRIX B365-G GAMING                          | 2019 | 2       | 44      | 1           |
| ASRock               | Z270M Pro4                                       | 2018 | 2       | 44      | 1           |
| Hewlett-Packard      | RF243AA-ABA M7640n                               | 2006 | 2       | 44      | 1           |
| ASRock               | Z490 Steel Legend                                | 2020 | 2       | 43      | 1           |
| ASRock               | A320M Pro4 R2.0                                  | 2019 | 2       | 43      | 1           |
| ASRock               | H87M                                             | 2013 | 2       | 43      | 1           |
| ASRock               | M3A790GXH/128M                                   | 2010 | 2       | 43      | 1           |
| MSI                  | MS-7329                                          | 2008 | 2       | 43      | 1           |
| MSI                  | MS-7228                                          | 2006 | 2       | 43      | 1           |
| WinFast              | 6100K8MB                                         | 2005 | 2       | 43      | 1           |
| Intel                | DZ68BC AAG30742-401                              | 2011 | 2       | 42      | 1           |
| Foxconn              | A85GM                                            | 2009 | 2       | 42      | 1           |
| Supermicro           | X7DB8                                            | 2008 | 2       | 42      | 1           |
| Hewlett-Packard      | FK792AA-ABA a6600f                               | 2008 | 2       | 42      | 1           |
| Gigabyte Technology  | GA-MA69G-S3H                                     | 2008 | 2       | 42      | 1           |
| Shuttle              | SP35                                             | 2007 | 2       | 42      | 1           |
| Chuwi                | LarkBox                                          | 2020 | 2       | 41      | 1           |
| ASRock               | A320M-DVS R3.0                                   | 2018 | 2       | 41      | 1           |
| Thirdwave            | Diginnos PC                                      | 2016 | 2       | 41      | 1           |
| ASUSTek Computer     | F1A75-I DELUXE                                   | 2014 | 2       | 41      | 1           |
| Hewlett-Packard      | NY537AA-ABA 300-1025                             | 2009 | 2       | 41      | 1           |
| Hewlett-Packard      | BK139AA-ABA 600-1120                             | 2009 | 2       | 41      | 1           |
| Gateway              | DX4200-UB001A                                    | 2008 | 2       | 41      | 1           |
| ASUSTek Computer     | M3N-HD/HDMI                                      | 2008 | 2       | 41      | 1           |
| DEPO Computers       | P4M900T-M2                                       | 2007 | 2       | 41      | 1           |
| MSI                  | MS-7176                                          | 2005 | 2       | 41      | 1           |
| ASRock               | NUC-8265U                                        | 2019 | 2       | 40      | 1           |
| MSI                  | MS-7A58                                          | 2017 | 2       | 40      | 1           |
| Dell                 | XPS 8910                                         | 2016 | 2       | 40      | 1           |
| MSI                  | Z1-7641                                          | 2014 | 2       | 40      | 1           |
| Hewlett-Packard      | 700-406                                          | 2014 | 2       | 40      | 1           |
| ASUSTek Computer     | A88X-GAMER                                       | 2014 | 2       | 40      | 1           |
| Intel                | DG43RK AAE78175-403                              | 2010 | 2       | 40      | 1           |
| ASRock               | K10N78FullHD-hSLI                                | 2010 | 2       | 40      | 1           |
| Intel                | DP965LT AAD41694-209                             | 2007 | 2       | 40      | 1           |
| Hewlett-Packard      | GN553AA-ABA m9040n                               | 2007 | 2       | 40      | 1           |
| Gateway              | GT5408                                           | 2006 | 2       | 40      | 1           |
| ASUSTek Computer     | TUF Z370-PLUS GAMING II                          | 2019 | 2       | 39      | 1           |
| ASRock               | 760GM-HD                                         | 2019 | 2       | 39      | 1           |
| MSI                  | MS-7B16                                          | 2018 | 2       | 39      | 1           |
| ASRock               | Z270 Killer SLI                                  | 2016 | 2       | 39      | 1           |
| MSI                  | MS-7913                                          | 2014 | 2       | 39      | 1           |
| ASRock               | FM2A75 Pro4+                                     | 2014 | 2       | 39      | 1           |
| Gigabyte Technology  | GA-990FXA-UD7                                    | 2012 | 2       | 39      | 1           |
| Acer                 | Veriton S6610G                                   | 2011 | 2       | 39      | 1           |
| MSI                  | MS-7642                                          | 2010 | 2       | 39      | 1           |
| Lenovo               | IdeaCentre K320 10031                            | 2010 | 2       | 39      | 1           |
| XFX                  | MI-A78S-8209 Ver1.1                              | 2009 | 2       | 39      | 1           |
| ASUSTek Computer     | A8V-VM SE                                        | 2006 | 2       | 39      | 1           |
| Dell                 | PowerEdge SC1420                                 | 2005 | 2       | 39      | 1           |
| ASRock               | B450M Pro4-F R2.0                                | 2020 | 2       | 38      | 1           |
| Gigabyte Technology  | Z170X-Designare                                  | 2018 | 2       | 38      | 1           |
| Hewlett-Packard      | 550-a114                                         | 2015 | 2       | 38      | 1           |
| ASRock               | H170A-X1                                         | 2015 | 2       | 38      | 1           |
| Dell                 | OptiPlex XE                                      | 2010 | 2       | 38      | 1           |
| Acer                 | Aspire X3400G                                    | 2010 | 2       | 38      | 1           |
| Foxconn              | H55MX-S Series                                   | 2009 | 2       | 38      | 1           |
| Hewlett-Packard      | KJ382AA-ABA m9250f                               | 2008 | 2       | 38      | 1           |
| ASRock               | P43D1600Twins-1394                               | 2008 | 2       | 38      | 1           |
| Gigabyte Technology  | C51-MCP51                                        | 2005 | 2       | 38      | 1           |
| Hewlett-Packard      | Z2 SFF G4 Workstation                            | 2019 | 2       | 37      | 1           |
| Gigabyte Technology  | B365M D2V                                        | 2019 | 2       | 37      | 1           |
| ASRock               | FM2A68M-HD+ R2.0                                 | 2019 | 2       | 37      | 1           |
| MSI                  | 2AE0                                             | 2012 | 2       | 37      | 1           |
| Intel                | DQ45CB AAE30148-206                              | 2009 | 2       | 37      | 1           |
| Fujitsu Siemens      | P5GD1-FM                                         | 2005 | 2       | 37      | 1           |
| ASUSTek Computer     | P5AD2-E-Premium                                  | 2005 | 2       | 37      | 1           |
| ASRockRack           | B450D4U-V1L                                      | 2020 | 2       | 36      | 1           |
| Gigabyte Technology  | E3800N                                           | 2017 | 2       | 36      | 1           |
| WYSE                 | ZQ Class                                         | 2014 | 2       | 36      | 1           |
| ECS                  | KBN-I                                            | 2013 | 2       | 36      | 1           |
| Sapphire             | EDGE VS series FP2M3038                          | 2012 | 2       | 36      | 1           |
| Acer                 | Aspire X1430G                                    | 2011 | 2       | 36      | 1           |
| Lenovo               | ThinkCentre M58p 6137E61                         | 2010 | 2       | 36      | 1           |
| Hewlett-Packard      | p6510f                                           | 2010 | 2       | 36      | 1           |
| Positivo             | POS-AT SERIES D                                  | 2009 | 2       | 36      | 1           |
| Hewlett-Packard      | FL213AA-ACB a6561.ru                             | 2009 | 2       | 36      | 1           |
| Gigabyte Technology  | GA-MA770T-ES3                                    | 2009 | 2       | 36      | 1           |
| ECS                  | IC780M-A                                         | 2009 | 2       | 36      | 1           |
| Packard Bell         | IMEDIA D3610 FR                                  | 2008 | 2       | 36      | 1           |
| ASUSTek Computer     | P5KPL-C/1600                                     | 2008 | 2       | 36      | 1           |
| ASUSTek Computer     | M3N18L T-M3N8200                                 | 2008 | 2       | 36      | 1           |
| K-Systems            | MS-7255                                          | 2007 | 2       | 36      | 1           |
| Biostar              | P4M90-M7                                         | 2007 | 2       | 36      | 1           |
| ECS                  | K8M890M-M                                        | 2006 | 2       | 36      | 1           |
| Qbex                 | QBEX-HDC-M                                       |      | 2       | 36      | 1           |
| MSI                  | MS-7D08                                          | 2021 | 2       | 35      | 1           |
| ASUSTek Computer     | Pro WS 565-ACE                                   | 2021 | 2       | 35      | 1           |
| Hewlett-Packard      | Slimline Desktop PC 270-p0xx                     | 2018 | 2       | 35      | 1           |
| Lenovo               | IdeaCentre 310S-08ASR 90G9002VUK                 | 2017 | 2       | 35      | 1           |
| ECS                  | H61H2-I3                                         | 2013 | 2       | 35      | 1           |
| Lenovo               | H505S 3230                                       | 2012 | 2       | 35      | 1           |
| ASUSTek Computer     | P5KPL-AM IN                                      | 2010 | 2       | 35      | 1           |
| Hewlett-Packard      | AY643AA-ABA s5310f                               | 2009 | 2       | 35      | 1           |
| Foxconn              | ALOE                                             | 2009 | 2       | 35      | 1           |
| Gateway              | GT5453E                                          | 2007 | 2       | 35      | 1           |
| Medion               | MS-7012                                          | 2003 | 2       | 35      | 1           |
| ASRock               | B365M IB-R                                       | 2019 | 2       | 34      | 1           |
| Colorful Technology  | C.H81A-BTC V20                                   | 2017 | 2       | 34      | 1           |
| AMD                  | A88                                              | 2017 | 2       | 34      | 1           |
| MSI                  | MS-7A44                                          | 2016 | 2       | 34      | 1           |
| Gigabyte Technology  | B85M-D3V-A                                       | 2015 | 2       | 34      | 1           |
| Lenovo               | 70A50022UK ThinkServer TS140                     | 2014 | 2       | 34      | 1           |
| ASUSTek Computer     | ET2221I-B85                                      | 2013 | 2       | 34      | 1           |
| Lenovo               | ThinkCentre M77 2209A12                          | 2012 | 2       | 34      | 1           |
| Intel                | DQ77MK AAG39642-302                              | 2012 | 2       | 34      | 1           |
| Intel                | DH61BE AAG14062-209                              | 2012 | 2       | 34      | 1           |
| ASUSTek Computer     | CM6340                                           | 2012 | 2       | 34      | 1           |
| Acer                 | Aspire X1420G                                    | 2011 | 2       | 34      | 1           |
| ECS                  | H67H2-A3                                         | 2010 | 2       | 34      | 1           |
| ASRock               | A790GMH/128M                                     | 2010 | 2       | 34      | 1           |
| Hewlett-Packard      | NC890AA-ABA a6803w                               | 2009 | 2       | 34      | 1           |
| Hewlett-Packard      | GS211AA-ABA m9077c                               | 2007 | 2       | 34      | 1           |
| Biostar              | P4M90-M7A                                        | 2007 | 2       | 34      | 1           |
| WinFast              | K8M890-8237                                      | 2006 | 2       | 34      | 1           |
| MSI                  | MS-7181                                          | 2006 | 2       | 34      | 1           |
| EPoX Computer        | nForce4 DDR2: MF4-J3, MF4-J3/G, AF4-J3, AF4-J... | 2006 | 2       | 34      | 1           |
| ASUSTek Computer     | P5WD2                                            | 2006 | 2       | 34      | 1           |
| MSI                  | MS-7C77                                          | 2020 | 2       | 33      | 1           |
| Intel                | Server Board M10JNP2SB                           | 2019 | 2       | 33      | 1           |
| Dell                 | Precision Tower 3430                             | 2019 | 2       | 33      | 1           |
| Gigabyte Technology  | H110M-A                                          | 2018 | 2       | 33      | 1           |
| ASRock               | Z270M-ITX/ac                                     | 2016 | 2       | 33      | 1           |
| WYSE                 | D CLASS                                          | 2013 | 2       | 33      | 1           |
| ASRock               | 870iCafe R2.0                                    | 2013 | 2       | 33      | 1           |
| Lenovo               | IdeaCentre K410 10089                            | 2012 | 2       | 33      | 1           |
| ASRock               | P75 Pro3                                         | 2012 | 2       | 33      | 1           |
| Biostar              | TH67+                                            | 2011 | 2       | 33      | 1           |
| ASUSTek Computer     | P5QP18L/T5-P5G41E                                | 2010 | 2       | 33      | 1           |
| Foxconn              | ETON                                             | 2009 | 2       | 33      | 1           |
| ASUSTek Computer     | V-M3N8200                                        | 2009 | 2       | 33      | 1           |
| PCChips              | P53G                                             | 2008 | 2       | 33      | 1           |
| Foxconn              | 45CMV-K/45CMV                                    | 2008 | 2       | 33      | 1           |
| Compaq               | KJ393AA-ABA SR5413WM                             | 2008 | 2       | 33      | 1           |
| KLONDIKE COMPUTERS   | KLONDIKE                                         | 2006 | 2       | 33      | 1           |
| Intel                | D946GZIS AAD83227-402                            | 2006 | 2       | 33      | 1           |
| Intel                | D915PGN AAC64142-403                             | 2005 | 2       | 33      | 1           |
| Fujitsu Siemens      | D1607                                            | 2004 | 2       | 33      | 1           |
| ASUSTek Computer     | PRIME H410I-PLUS                                 | 2020 | 2       | 32      | 1           |
| Lenovo               | ThinkCentre M93p 10A7000BUS                      | 2018 | 2       | 32      | 1           |
| Fujitsu              | ESPRIMO Q920                                     | 2018 | 2       | 32      | 1           |
| ASRock               | C2550D4I                                         | 2018 | 2       | 32      | 1           |
| ASRock               | QC5000-ITX/PH                                    | 2014 | 2       | 32      | 1           |
| Bluechip Computer    | BUSINESSline                                     | 2013 | 2       | 32      | 1           |
| Hewlett-Packard      | p6-2040fr                                        | 2011 | 2       | 32      | 1           |
| eMachines            | ET1330/ET1331                                    | 2009 | 2       | 32      | 1           |
| Packard Bell         | imedia S3712                                     | 2009 | 2       | 32      | 1           |
| Hewlett-Packard      | VS342AA-AB6 m9801af                              | 2009 | 2       | 32      | 1           |
| Hewlett-Packard      | NC689AA-ABA s3700y                               | 2008 | 2       | 32      | 1           |
| ASUSTek Computer     | TS300-E5                                         | 2008 | 2       | 32      | 1           |
| ASUSTek Computer     | P5K31-VM                                         | 2008 | 2       | 32      | 1           |
| ASRock               | 939NF6G-VSTA                                     | 2007 | 2       | 32      | 1           |
| Acer                 | Veriton 7800                                     | 2006 | 2       | 32      | 1           |
| ASUSTek Computer     | P4P800-X                                         | 2005 | 2       | 32      | 1           |
| Gigabyte Technology  | 8PEMT4                                           | 2003 | 2       | 32      | 1           |
| Gigabyte Technology  | B560M H                                          | 2021 | 2       | 31      | 1           |
| MSI                  | MS-7A65                                          | 2017 | 2       | 31      | 1           |
| Dell                 | Wyse 7040                                        | 2017 | 2       | 31      | 1           |
| Hewlett-Packard      | ProLiant ML30 Gen9                               | 2016 | 2       | 31      | 1           |
| ASRock               | H81 Pro BTC                                      | 2016 | 2       | 31      | 1           |
| ASUSTek Computer     | H170I-PLUS D3                                    | 2015 | 2       | 31      | 1           |
| CIARA                | Q87M-XA                                          | 2013 | 2       | 31      | 1           |
| Intel                | Tiger Hill                                       | 2012 | 2       | 31      | 1           |
| Standard             | AHV                                              | 2011 | 2       | 31      | 1           |
| OEGStone             | DG31PR                                           | 2009 | 2       | 31      | 1           |
| Hewlett-Packard      | AY691AA-ABA p6367c                               | 2009 | 2       | 31      | 1           |
| Hewlett-Packard      | KJ269AA-ABA s3407c                               | 2008 | 2       | 31      | 1           |
| Hewlett-Packard      | DC5800                                           | 2008 | 2       | 31      | 1           |
| Fujitsu Siemens      | ESPRIMO E5720                                    | 2008 | 2       | 31      | 1           |
| ASRock               | P4VM890                                          | 2007 | 2       | 31      | 1           |
| Acer                 | Aspire E500                                      | 2005 | 2       | 31      | 1           |
| ASUSTek Computer     | P4P800-E                                         | 2005 | 2       | 31      | 1           |
| ASUSTek Computer     | A7N8X-E                                          | 2004 | 2       | 31      | 1           |
| Dell                 | Vostro 3471                                      | 2020 | 2       | 30      | 1           |
| ASUSTek Computer     | PRIME H310-PLUS R2.0                             | 2020 | 2       | 30      | 1           |
| ASRock               | ZH77 Pro3                                        | 2013 | 2       | 30      | 1           |
| Shuttle              | SH67H                                            | 2012 | 2       | 30      | 1           |
| Intel                | SandyBridge Platform                             | 2012 | 2       | 30      | 1           |
| 3NOD                 | ALH236SB2_HDMI_DNS                               | 2012 | 2       | 30      | 1           |
| Intel                | D525MWV AAE93081-401                             | 2011 | 2       | 30      | 1           |
| ECS                  | H67H2-M3                                         | 2011 | 2       | 30      | 1           |
| eMachines            | EL1352G                                          | 2010 | 2       | 30      | 1           |
| Lenovo               | ThinkCentre M58p 7479RS2                         | 2010 | 2       | 30      | 1           |
| Hewlett-Packard      | Pro 3120 Small Form Factor PC                    | 2010 | 2       | 30      | 1           |
| ECS                  | G41T-M                                           | 2009 | 2       | 30      | 1           |
| Compaq               | NP185AA-ABA CQ5110F                              | 2009 | 2       | 30      | 1           |
| MSI                  | MS-7418                                          | 2008 | 2       | 30      | 1           |
| Lenovo               | ThinkCentre M57p 6073BA4                         | 2008 | 2       | 30      | 1           |
| Hewlett-Packard      | Compaq dc5850 SFF PC                             | 2008 | 2       | 30      | 1           |
| ASRock               | P4i65GV                                          | 2008 | 2       | 30      | 1           |
| Hewlett-Packard      | GU609AA-ACB a6230.ru                             | 2007 | 2       | 30      | 1           |
| Hewlett-Packard      | GN699AA-ABA a6212n                               | 2007 | 2       | 30      | 1           |
| Hewlett-Packard      | Compaq dx7300 Microtower                         | 2007 | 2       | 30      | 1           |
| ASUSTek Computer     | P4GPL-X                                          | 2005 | 2       | 30      | 1           |
| Gigabyte Technology  | H410M H V3                                       | 2021 | 2       | 29      | 1           |
| ASUSTek Computer     | PRIME H310M-CS R2.0                              | 2019 | 2       | 29      | 1           |
| ECS                  | H310CH5-M2                                       | 2018 | 2       | 29      | 1           |
| ASUSTek Computer     | B150 PRO GAMING D3                               | 2015 | 2       | 29      | 1           |
| Acer                 | Veriton E430                                     | 2013 | 2       | 29      | 1           |
| Foxconn              | 945 7MD Series                                   | 2008 | 2       | 29      | 1           |
| ECS                  | 761GXM-M                                         | 2007 | 2       | 29      | 1           |
| Gigabyte Technology  | M1689D                                           | 2005 | 2       | 29      | 1           |
| Gigabyte Technology  | 8IPE1000                                         | 2005 | 2       | 29      | 1           |
| ECS                  | P4M800-M                                         | 2005 | 2       | 29      | 1           |
| Hewlett-Packard      | d330 uT(DC579AV)                                 | 2003 | 2       | 29      | 1           |
| ASUSTek Computer     | P4PE-X                                           | 2003 | 2       | 29      | 1           |
| Shuttle              | SH61R4                                           | 2011 | 2       | 28      | 1           |
| Hewlett-Packard      | t5540                                            | 2010 | 2       | 28      | 1           |
| Acer                 | Veriton X275                                     | 2010 | 2       | 28      | 1           |
| Lenovo               | 3000 IPM31                                       | 2009 | 2       | 28      | 1           |
| ECS                  | G41T-M5                                          | 2009 | 2       | 28      | 1           |
| Intel                | DG31PR AAE58249-301                              | 2008 | 2       | 28      | 1           |
| Hewlett-Packard      | GC670AA-ABA a6120n                               | 2007 | 2       | 28      | 1           |
| Gigabyte Technology  | 8I915ME                                          | 2005 | 2       | 28      | 1           |
| ECS                  | nForce3-A                                        | 2005 | 2       | 28      | 1           |
| ASUSTek Computer     | P5P800-MX                                        | 2005 | 2       | 28      | 1           |
| ASRock               | 775i65GV                                         | 2005 | 2       | 28      | 1           |
| ASUSTek Computer     | P4R800-VM                                        | 2003 | 2       | 28      | 1           |
| Gigabyte Technology  | H370M D3H GSM                                    | 2018 | 2       | 27      | 1           |
| Lenovo               | IdeaCentre 510S-08ISH 90FN006NUS                 | 2016 | 2       | 27      | 1           |
| ECS                  | H110M4-C2H                                       | 2016 | 2       | 27      | 1           |
| Pegatron             | IPXCR_VN1                                        | 2013 | 2       | 27      | 1           |
| Lenovo               | 3000                                             | 2010 | 2       | 27      | 1           |
| Intel                | DH67BL AAG10189-206                              | 2010 | 2       | 27      | 1           |
| Gigabyte Technology  | 946GMX-S2                                        | 2007 | 2       | 27      | 1           |
| Foxconn              | 915MH Series                                     | 2005 | 2       | 27      | 1           |
| Dell                 | DC051                                            | 2005 | 2       | 27      | 1           |
| Compaq               | Evo D310                                         | 2002 | 2       | 27      | 1           |
| Acer                 | Veriton X4620G                                   | 2012 | 2       | 26      | 1           |
| Pegatron             | IPPPV-CP                                         | 2009 | 2       | 26      | 1           |
| CCE                  | G31T-M7                                          | 2009 | 2       | 26      | 1           |
| Dell                 | OptiPlex 170L                                    | 2005 | 2       | 26      | 1           |
| VIA Technologies     | P4M266A-8235                                     | 2003 | 2       | 26      | 1           |
| Shuttle              | NC03U                                            | 2018 | 2       | 25      | 1           |
| Intel                | DN2800MT AAG23738-600                            | 2011 | 2       | 25      | 1           |
| ECS                  | 671T-M                                           | 2007 | 2       | 25      | 1           |
| PCChips              | P23G                                             | 2006 | 2       | 25      | 1           |
| SiS Technology       | SiS661FX + SiS964                                | 2005 | 2       | 25      | 1           |
| Gigabyte Technology  | GA-8SQ800                                        | 2003 | 2       | 25      | 1           |
| Intel                | DN2800MT AAG23738-801                            | 2013 | 2       | 24      | 1           |
| Acer                 | Aspire X1930                                     | 2011 | 2       | 24      | 1           |
| ASRock               | AD525PV3                                         | 2011 | 2       | 24      | 1           |
| Compaq               | RX894AA-ABA SR5013WM                             | 2007 | 2       | 24      | 1           |
| SiS Technology       | SiS-650GX                                        | 2003 | 2       | 24      | 1           |
| Acer                 | AC100                                            | 2013 | 2       | 23      | 1           |
| ASUSTek Computer     | P4S8X                                            | 2003 | 2       | 23      | 1           |
| ISYNC                | ISYNC-19-ECH61                                   | 2020 | 2       | 22      | 1           |
| ASUSTek Computer     | P5S-MX SE                                        | 2007 | 2       | 22      | 1           |
| Gigabyte Technology  | GA-7VA                                           | 2002 | 2       | 22      | 1           |
| Intel                | DN2800MT AAG81515-900                            | 2013 | 2       | 21      | 1           |
| Dell                 | OptiPlex FX160                                   | 2009 | 2       | 21      | 1           |
| ASUSTek Computer     | TS10                                             | 2016 | 2       | 20      | 1           |
| Dell                 | OptiPlex 7010                                    | 2012 | 224     | 44      | 2           |
| Gigabyte Technology  | 970A-DS3P                                        | 2013 | 193     | 52      | 2           |
| MSI                  | MS-7C37                                          | 2019 | 190     | 67      | 2           |
| ASUSTek Computer     | TUF GAMING X570-PLUS                             | 2019 | 184     | 62      | 2           |
| MSI                  | MS-7817                                          | 2013 | 183     | 44      | 2           |
| ASUSTek Computer     | M5A78L-M/USB3                                    | 2011 | 172     | 51      | 2           |
| Dell                 | OptiPlex 790                                     | 2011 | 142     | 42      | 2           |
| MSI                  | MS-7B86                                          | 2018 | 132     | 58      | 2           |
| Gigabyte Technology  | G31M-ES2L                                        | 2008 | 132     | 37      | 2           |
| ASRock               | B450M Pro4                                       | 2018 | 131     | 54      | 2           |
| ASUSTek Computer     | PRIME B450M-A                                    | 2018 | 123     | 64      | 2           |
| ASRock               | N68C-S UCC                                       | 2010 | 121     | 46      | 2           |
| MSI                  | MS-7A38                                          | 2017 | 115     | 58      | 2           |
| ASUSTek Computer     | ROG STRIX B450-F GAMING                          | 2018 | 114     | 59      | 2           |
| ASUSTek Computer     | M5A97 LE R2.0                                    | 2012 | 112     | 56      | 2           |
| MSI                  | MS-7A34                                          | 2017 | 97      | 63      | 2           |
| ASUSTek Computer     | M5A78L-M PLUS/USB3                               | 2016 | 95      | 54      | 2           |
| ASUSTek Computer     | PRIME X470-PRO                                   | 2018 | 94      | 90      | 2           |
| Dell                 | OptiPlex 390                                     | 2011 | 94      | 39      | 2           |
| Dell                 | OptiPlex 3010                                    | 2012 | 92      | 33      | 2           |
| Gigabyte Technology  | GA-78LMT-USB3 6.0                                | 2014 | 91      | 48      | 2           |
| Hewlett-Packard      | EliteDesk 800 G1 SFF                             | 2013 | 88      | 45      | 2           |
| ASUSTek Computer     | P8H61-M LX3 R2.0                                 | 2012 | 87      | 36      | 2           |
| Gigabyte Technology  | B75M-D3H                                         | 2012 | 86      | 43      | 2           |
| Gigabyte Technology  | X570 AORUS ELITE                                 | 2019 | 84      | 62      | 2           |
| ASUSTek Computer     | P5G41T-M LX                                      | 2010 | 81      | 38      | 2           |
| Hewlett-Packard      | Compaq 8200 Elite SFF PC                         | 2011 | 78      | 39      | 2           |
| MSI                  | MS-7758                                          | 2012 | 77      | 42      | 2           |
| Gigabyte Technology  | GA-78LMT-USB3                                    | 2012 | 76      | 46      | 2           |
| MSI                  | MS-7309                                          | 2006 | 76      | 43      | 2           |
| ASUSTek Computer     | ROG STRIX B550-F GAMING                          | 2020 | 70      | 59      | 2           |
| MSI                  | MS-7816                                          | 2013 | 70      | 48      | 2           |
| MSI                  | MS-7996                                          | 2015 | 70      | 39      | 2           |
| Gigabyte Technology  | B450 AORUS ELITE                                 | 2018 | 69      | 60      | 2           |
| Hewlett-Packard      | Compaq Pro 6300 SFF                              | 2012 | 62      | 41      | 2           |
| ASUSTek Computer     | Z170-A                                           | 2015 | 60      | 52      | 2           |
| ASRock               | A320M-HDV R4.0                                   | 2018 | 59      | 50      | 2           |
| ASUSTek Computer     | M5A99X EVO R2.0                                  | 2012 | 58      | 62      | 2           |
| ASUSTek Computer     | PRIME X570-P                                     | 2019 | 55      | 60      | 2           |
| ASUSTek Computer     | P8H61-M LE                                       | 2011 | 54      | 41      | 2           |
| Dell                 | OptiPlex GX620                                   | 2005 | 54      | 35      | 2           |
| ASUSTek Computer     | P5Q                                              | 2008 | 53      | 50      | 2           |
| MSI                  | MS-7C52                                          | 2019 | 51      | 52      | 2           |
| Hewlett-Packard      | ProDesk 600 G1 SFF                               | 2013 | 51      | 48      | 2           |
| MSI                  | MS-7369                                          | 2007 | 49      | 39      | 2           |
| MSI                  | MS-7C84                                          | 2020 | 48      | 59      | 2           |
| ASUSTek Computer     | PRIME X570-PRO                                   | 2019 | 48      | 59      | 2           |
| ASRock               | AB350 Pro4                                       | 2017 | 47      | 54      | 2           |
| MSI                  | MS-7680                                          | 2011 | 46      | 38      | 2           |
| Gigabyte Technology  | A320M-S2H V2                                     | 2018 | 44      | 52      | 2           |
| Dell                 | Inspiron 3847                                    | 2013 | 44      | 49      | 2           |
| ASUSTek Computer     | TUF GAMING B550M-PLUS                            | 2020 | 43      | 54      | 2           |
| MSI                  | MS-7B98                                          | 2018 | 43      | 50      | 2           |
| Hewlett-Packard      | Compaq 8200 Elite CMT PC                         | 2011 | 43      | 43      | 2           |
| Gigabyte Technology  | H81M-S1                                          | 2013 | 41      | 37      | 2           |
| ASUSTek Computer     | ROG CROSSHAIR VIII HERO                          | 2019 | 40      | 66      | 2           |
| ASUSTek Computer     | P8Z68-V LX                                       | 2011 | 40      | 42      | 2           |
| Hewlett-Packard      | Z440 Workstation                                 | 2015 | 39      | 115     | 2           |
| ASUSTek Computer     | P5Q SE2                                          | 2008 | 39      | 47      | 2           |
| ASUSTek Computer     | PRIME A320M-K/BR                                 | 2017 | 39      | 44      | 2           |
| ASUSTek Computer     | M5A78L-M LX/BR                                   | 2011 | 39      | 43      | 2           |
| MSI                  | MS-7597                                          | 2009 | 38      | 41      | 2           |
| ASUSTek Computer     | ROG STRIX X470-F GAMING                          | 2018 | 37      | 57      | 2           |
| Gigabyte Technology  | B450 AORUS PRO WIFI                              | 2018 | 37      | 55      | 2           |
| ASUSTek Computer     | P6T SE                                           | 2009 | 36      | 82      | 2           |
| Hewlett-Packard      | Compaq 8100 Elite SFF PC                         | 2009 | 36      | 56      | 2           |
| ASUSTek Computer     | PRIME Z390-A                                     | 2018 | 36      | 53      | 2           |
| ASRock               | N68-S                                            | 2009 | 36      | 37      | 2           |
| ASRock               | Z77 Extreme4                                     | 2012 | 35      | 51      | 2           |
| ASUSTek Computer     | P8B75-V                                          | 2012 | 35      | 40      | 2           |
| ASRock               | A300M-STX                                        | 2019 | 34      | 59      | 2           |
| MSI                  | MS-7C94                                          | 2020 | 33      | 56      | 2           |
| ASUSTek Computer     | P5Q-PRO                                          | 2008 | 33      | 46      | 2           |
| MSI                  | MS-7C95                                          | 2020 | 32      | 53      | 2           |
| MSI                  | MS-7917                                          | 2014 | 32      | 47      | 2           |
| ASUSTek Computer     | PRIME Z390-P                                     | 2018 | 32      | 45      | 2           |
| Hewlett-Packard      | Compaq 6000 Pro MT PC                            | 2009 | 32      | 40      | 2           |
| Gigabyte Technology  | H110M-S2H                                        | 2016 | 31      | 38      | 2           |
| Dell                 | OptiPlex 330                                     | 2007 | 31      | 37      | 2           |
| Hewlett-Packard      | Z620 Workstation                                 | 2012 | 30      | 99      | 2           |
| ASUSTek Computer     | P6T DELUXE V2                                    | 2009 | 29      | 78      | 2           |
| ASUSTek Computer     | P7P55 LX                                         | 2009 | 29      | 60      | 2           |
| ASUSTek Computer     | PRIME B450M-K                                    | 2018 | 29      | 49      | 2           |
| ASUSTek Computer     | Maximus VIII HERO                                | 2015 | 29      | 47      | 2           |
| Gigabyte Technology  | Z87-HD3                                          | 2013 | 29      | 42      | 2           |
| MSI                  | MS-7A70                                          | 2016 | 29      | 40      | 2           |
| ASUSTek Computer     | P8H61-M LX2                                      | 2011 | 29      | 39      | 2           |
| Gigabyte Technology  | GA-990FXA-UD3                                    | 2011 | 28      | 62      | 2           |
| MSI                  | MS-7640                                          | 2011 | 28      | 53      | 2           |
| MSI                  | MS-7823                                          | 2013 | 28      | 49      | 2           |
| ASRock               | 970 Extreme3 R2.0                                | 2012 | 28      | 47      | 2           |
| ASUSTek Computer     | CROSSHAIR V FORMULA-Z                            | 2012 | 26      | 63      | 2           |
| Gigabyte Technology  | GA-970A-DS3                                      | 2012 | 26      | 60      | 2           |
| ASUSTek Computer     | P8H67-M PRO                                      | 2011 | 26      | 44      | 2           |
| ASRock               | G41C-GS                                          | 2009 | 26      | 44      | 2           |
| ASUSTek Computer     | 970 PRO GAMING/AURA                              | 2016 | 25      | 49      | 2           |
| ASUSTek Computer     | ROG STRIX Z370-F GAMING                          | 2017 | 25      | 47      | 2           |
| ASUSTek Computer     | Z170-K                                           | 2015 | 25      | 44      | 2           |
| MSI                  | MS-7388                                          | 2008 | 25      | 42      | 2           |
| Gigabyte Technology  | Z68P-DS3                                         | 2011 | 25      | 35      | 2           |
| ASUSTek Computer     | PRIME Z370-P                                     | 2017 | 24      | 44      | 2           |
| MSI                  | MS-7B09                                          | 2017 | 23      | 66      | 2           |
| ASRock               | N68-S3 FX                                        | 2011 | 23      | 39      | 2           |
| Gigabyte Technology  | H81M-S2V                                         | 2014 | 23      | 35      | 2           |
| Hewlett-Packard      | Pavilion Desktop 590-p0xxx                       | 2018 | 22      | 51      | 2           |
| Intel                | B75                                              | 2018 | 22      | 35      | 2           |
| ASUSTek Computer     | P5LD2                                            | 2006 | 22      | 33      | 2           |
| Wortmann AG          | TERRA_PC                                         | 2012 | 21      | 115     | 2           |
| ASUSTek Computer     | ROG STRIX Z370-E GAMING                          | 2017 | 21      | 56      | 2           |
| MSI                  | MS-7A72                                          | 2016 | 21      | 49      | 2           |
| ASUSTek Computer     | P8H67-M                                          | 2011 | 21      | 40      | 2           |
| Gigabyte Technology  | Z68AP-D3                                         | 2011 | 21      | 39      | 2           |
| Gigabyte Technology  | B75-D3V                                          | 2012 | 21      | 37      | 2           |
| ASRock               | ConRoe1333-D667                                  | 2007 | 21      | 37      | 2           |
| ASUSTek Computer     | P8H61-M LE R2.0                                  | 2012 | 21      | 36      | 2           |
| Gigabyte Technology  | 990XA-UD3                                        | 2013 | 20      | 47      | 2           |
| Hewlett-Packard      | Compaq Elite 8300 USDT                           | 2012 | 20      | 35      | 2           |
| ASRock               | H61M-VG4                                         | 2013 | 20      | 33      | 2           |
| Huanan               | X99-F8                                           | 2019 | 19      | 120     | 2           |
| Intel                | X99                                              | 2019 | 19      | 112     | 2           |
| Gigabyte Technology  | Z370P D3                                         | 2017 | 19      | 50      | 2           |
| Gigabyte Technology  | B85-HD3                                          | 2013 | 19      | 46      | 2           |
| Gigabyte Technology  | H87-HD3                                          | 2013 | 19      | 43      | 2           |
| Dell                 | DM061                                            | 2006 | 19      | 37      | 2           |
| MSI                  | MS-7982                                          | 2015 | 19      | 34      | 2           |
| Dell                 | Inspiron 530s                                    | 2007 | 19      | 34      | 2           |
| ASUSTek Computer     | H61M-A/BR                                        | 2013 | 19      | 32      | 2           |
| ASUSTek Computer     | PRIME X399-A                                     | 2017 | 18      | 61      | 2           |
| ASUSTek Computer     | Pro WS X570-ACE                                  | 2019 | 18      | 60      | 2           |
| ASUSTek Computer     | PRIME B550M-A                                    | 2020 | 18      | 52      | 2           |
| ASRock               | H81 Pro BTC R2.0                                 | 2014 | 18      | 38      | 2           |
| Hewlett-Packard      | ProDesk 600 G2 SFF                               | 2015 | 18      | 30      | 2           |
| Gigabyte Technology  | P55-UD3L                                         | 2009 | 17      | 59      | 2           |
| ASUSTek Computer     | ROG Maximus X HERO                               | 2017 | 17      | 49      | 2           |
| ASUSTek Computer     | PRIME B360-PLUS                                  | 2018 | 17      | 44      | 2           |
| MSI                  | MS-7A71                                          | 2016 | 17      | 44      | 2           |
| Gigabyte Technology  | Z390 GAMING X                                    | 2019 | 17      | 42      | 2           |
| Dell                 | Inspiron 3647                                    | 2014 | 17      | 37      | 2           |
| ASRock               | P4i65G                                           | 2006 | 17      | 32      | 2           |
| ASRock               | TRX40 Creator                                    | 2019 | 16      | 57      | 2           |
| Gigabyte Technology  | Z390 M GAMING                                    | 2018 | 16      | 49      | 2           |
| ASUSTek Computer     | P5QD TURBO                                       | 2009 | 16      | 44      | 2           |
| ASUSTek Computer     | PRIME B450M-A II                                 | 2020 | 16      | 43      | 2           |
| ASUSTek Computer     | M4A88T-M                                         | 2010 | 16      | 40      | 2           |
| Gigabyte Technology  | H97-D3H                                          | 2014 | 16      | 38      | 2           |
| Acer                 | Aspire TC-885                                    | 2018 | 16      | 36      | 2           |
| ASUSTek Computer     | H170M-PLUS                                       | 2015 | 16      | 34      | 2           |
| ASUSTek Computer     | H110I-PLUS                                       | 2016 | 16      | 29      | 2           |
| MSI                  | MS-7B24                                          | 2018 | 15      | 53      | 2           |
| Gigabyte Technology  | Z170X-Gaming 3                                   | 2015 | 15      | 45      | 2           |
| Hewlett-Packard      | Z210 Workstation                                 | 2011 | 15      | 42      | 2           |
| Gigabyte Technology  | M57SLI-S4                                        | 2007 | 15      | 41      | 2           |
| Dell                 | DXP061                                           | 2006 | 15      | 38      | 2           |
| ASUSTek Computer     | PRIME B460M-A                                    | 2020 | 15      | 37      | 2           |
| Gigabyte Technology  | B85M-D2V                                         | 2013 | 15      | 37      | 2           |
| ASUSTek Computer     | K30AD_M31AD_M51AD                                | 2013 | 15      | 37      | 2           |
| ECS                  | GeForce6100PM-M2                                 | 2007 | 15      | 35      | 2           |
| Apple                | MacPro1,1                                        | 2007 | 14      | 62      | 2           |
| Gigabyte Technology  | B550 AORUS PRO                                   | 2020 | 14      | 58      | 2           |
| ASRock               | 970A-G                                           | 2015 | 14      | 48      | 2           |
| ASUSTek Computer     | P8Z77-M PRO                                      | 2012 | 14      | 44      | 2           |
| ASUSTek Computer     | M2A-VM HDMI                                      | 2007 | 14      | 42      | 2           |
| ASUSTek Computer     | M3A78                                            | 2008 | 14      | 41      | 2           |
| ASUSTek Computer     | P5VD2-MX                                         | 2006 | 14      | 40      | 2           |
| Gigabyte Technology  | 945P-S3                                          | 2006 | 14      | 35      | 2           |
| Gigabyte Technology  | 945GM-S2                                         | 2006 | 14      | 34      | 2           |
| Gigabyte Technology  | H310M H 2.0                                      | 2018 | 14      | 33      | 2           |
| ASUSTek Computer     | H110M-D                                          | 2015 | 14      | 30      | 2           |
| Gigabyte Technology  | B550M AORUS PRO-P                                | 2018 | 13      | 56      | 2           |
| Hewlett-Packard      | Desktop M01-F0xxx                                | 2019 | 13      | 51      | 2           |
| ASRock               | M3N78D                                           | 2010 | 13      | 49      | 2           |
| Gigabyte Technology  | H77-D3H                                          | 2012 | 13      | 46      | 2           |
| ASUSTek Computer     | STRIX Z270H GAMING                               | 2017 | 13      | 43      | 2           |
| Gigabyte Technology  | Z370M D3H                                        | 2017 | 13      | 39      | 2           |
| Gigabyte Technology  | EP41-UD3L                                        | 2009 | 13      | 35      | 2           |
| Dell                 | DM051                                            | 2005 | 13      | 35      | 2           |
| Intel                | DH61WW AAG23116-204                              | 2011 | 13      | 26      | 2           |
| Huanan               | X99-TF                                           | 2019 | 12      | 126     | 2           |
| ASRock               | X470 Master SLI                                  | 2018 | 12      | 61      | 2           |
| Gigabyte Technology  | Z390 DESIGNARE                                   | 2019 | 12      | 60      | 2           |
| Gigabyte Technology  | GA-990X-Gaming SLI-CF                            | 2016 | 12      | 53      | 2           |
| ASUSTek Computer     | Z170-DELUXE                                      | 2016 | 12      | 53      | 2           |
| Gigabyte Technology  | Z170X-Gaming 5                                   | 2015 | 12      | 47      | 2           |
| Dell                 | XPS420                                           | 2007 | 12      | 47      | 2           |
| MSI                  | MS-7D09                                          | 2021 | 12      | 45      | 2           |
| ASUSTek Computer     | H170-PRO                                         | 2015 | 12      | 41      | 2           |
| ASUSTek Computer     | ROG CROSSHAIR VIII DARK HERO                     | 2020 | 11      | 64      | 2           |
| ASRock               | FM2A88X Extreme4+                                | 2014 | 11      | 55      | 2           |
| Gigabyte Technology  | Z370 AORUS Gaming 7                              | 2017 | 11      | 51      | 2           |
| Gigabyte Technology  | GA-990FXA-D3                                     | 2011 | 11      | 49      | 2           |
| Gigabyte Technology  | Z68XP-UD3                                        | 2011 | 11      | 42      | 2           |
| Acer                 | Aspire TC-780                                    | 2016 | 11      | 40      | 2           |
| ASRock               | X99 Extreme4                                     | 2014 | 10      | 109     | 2           |
| Gigabyte Technology  | X399 AORUS Gaming 7                              | 2017 | 10      | 59      | 2           |
| Acer                 | Aspire M3910                                     | 2010 | 10      | 55      | 2           |
| Hewlett-Packard      | Pavilion Desktop TP01-0xxx                       | 2019 | 10      | 54      | 2           |
| Hewlett-Packard      | Pavilion Gaming Desktop 690-00xx                 | 2018 | 10      | 52      | 2           |
| ASRock               | 760GM-HDV                                        | 2018 | 10      | 50      | 2           |
| ASRock               | Z68 Extreme4 Gen3                                | 2011 | 10      | 50      | 2           |
| Hewlett-Packard      | xw6600 Workstation                               | 2008 | 10      | 48      | 2           |
| Gigabyte Technology  | 970A-UD3                                         | 2013 | 10      | 44      | 2           |
| Gigabyte Technology  | F2A55M-HD2                                       | 2012 | 10      | 42      | 2           |
| Gigabyte Technology  | nForce                                           | 2003 | 10      | 42      | 2           |
| ASRock               | B85 Pro4                                         | 2014 | 10      | 40      | 2           |
| Dell                 | OptiPlex 7060                                    | 2018 | 10      | 39      | 2           |
| ASUSTek Computer     | B150M-A                                          | 2015 | 10      | 39      | 2           |
| Gigabyte Technology  | Z97X-UD3H-BK                                     | 2014 | 10      | 38      | 2           |
| Dell                 | PowerEdge T20                                    | 2013 | 10      | 38      | 2           |
| ASUSTek Computer     | B150M-C                                          | 2015 | 10      | 37      | 2           |
| Medion               | MS-7621                                          | 2009 | 10      | 36      | 2           |
| Dell                 | OptiPlex GX280                                   | 2004 | 10      | 33      | 2           |
| ASRock               | H110M-HDV                                        | 2015 | 10      | 29      | 2           |
| ASUSTek Computer     | P7H55D-M EVO                                     | 2009 | 9       | 63      | 2           |
| Dell                 | Precision T1500                                  | 2009 | 9       | 52      | 2           |
| Gigabyte Technology  | B550M AORUS PRO                                  | 2020 | 9       | 50      | 2           |
| Seco                 | C40                                              | 2019 | 9       | 49      | 2           |
| Hewlett-Packard      | Pavilion Gaming Desktop TG01-1xxx                | 2020 | 9       | 47      | 2           |
| Gigabyte Technology  | P35-DS4                                          | 2007 | 9       | 47      | 2           |
| ASUSTek Computer     | M4N78 PRO                                        | 2009 | 9       | 46      | 2           |
| Dell                 | Dimension E521                                   | 2006 | 9       | 46      | 2           |
| Dell                 | XPS 8940                                         | 2020 | 9       | 42      | 2           |
| Dell                 | Studio 540                                       | 2008 | 9       | 41      | 2           |
| Gigabyte Technology  | M61SME-S2                                        | 2007 | 9       | 40      | 2           |
| Dell                 | OptiPlex 3070                                    | 2019 | 9       | 37      | 2           |
| Gigabyte Technology  | H110M-S2H-CF                                     | 2015 | 9       | 36      | 2           |
| MSI                  | MS-7C82                                          | 2020 | 9       | 35      | 2           |
| Acer                 | Veriton M275                                     | 2010 | 9       | 31      | 2           |
| ASRock               | X470 Taichi Ultimate                             | 2018 | 8       | 65      | 2           |
| ASUSTek Computer     | ROG ZENITH II EXTREME                            | 2019 | 8       | 62      | 2           |
| ASUSTek Computer     | PRIME B350M-K                                    | 2017 | 8       | 45      | 2           |
| MSI                  | P35 Platinum(MS-7345)                            | 2009 | 8       | 44      | 2           |
| ASRock               | P67 Extreme4                                     | 2011 | 8       | 43      | 2           |
| ASUSTek Computer     | M3N78-EM                                         | 2008 | 8       | 42      | 2           |
| Fujitsu Siemens      | ESPRIMO E                                        | 2005 | 8       | 38      | 2           |
| Gigabyte Technology  | Z170-Gaming K3                                   | 2015 | 8       | 37      | 2           |
| Intel                | BTC-T37                                          | 2020 | 8       | 36      | 2           |
| ASRock               | Q1900-ITX                                        | 2014 | 8       | 33      | 2           |
| ASUSTek Computer     | ROG CROSSHAIR VI EXTREME                         | 2017 | 7       | 61      | 2           |
| MSI                  | MS-7C60                                          | 2019 | 7       | 55      | 2           |
| MSI                  | MS-7C90                                          | 2020 | 7       | 54      | 2           |
| ASRock               | 980DE3/U3S3 R2.0                                 | 2015 | 7       | 53      | 2           |
| ASUSTek Computer     | P5Q PRO TURBO                                    | 2009 | 7       | 50      | 2           |
| Gigabyte Technology  | Z170-D3H                                         | 2015 | 7       | 47      | 2           |
| Hewlett-Packard      | Desktop Pro A MT                                 | 2018 | 7       | 46      | 2           |
| MSI                  | MS-7984                                          | 2015 | 7       | 41      | 2           |
| ASUSTek Computer     | A8N-E                                            | 2005 | 7       | 40      | 2           |
| Dell                 | XPS 8900                                         | 2016 | 7       | 39      | 2           |
| Gigabyte Technology  | B560M DS3H                                       | 2021 | 7       | 37      | 2           |
| ASUSTek Computer     | PRIME H270-PRO                                   | 2017 | 7       | 35      | 2           |
| Gigabyte Technology  | H310M M.2 2.0                                    | 2019 | 7       | 31      | 2           |
| ASRock               | J4105-ITX                                        | 2018 | 7       | 31      | 2           |
| Positivo             | POS-AG31AP                                       | 2009 | 7       | 31      | 2           |
| Gigabyte Technology  | H410M H                                          | 2020 | 7       | 30      | 2           |
| Gigabyte Technology  | X99-UD3-CF                                       | 2015 | 6       | 110     | 2           |
| Apple                | MacPro4,1                                        | 2009 | 6       | 103     | 2           |
| MSI                  | MS-7C34                                          | 2019 | 6       | 71      | 2           |
| ASRock               | TRX40 Taichi                                     | 2020 | 6       | 70      | 2           |
| Dell                 | Vostro 430                                       | 2011 | 6       | 53      | 2           |
| ASRockRack           | X470D4U                                          | 2019 | 6       | 51      | 2           |
| ASUSTek Computer     | ROG STRIX Z490-E GAMING                          | 2020 | 6       | 50      | 2           |
| Gigabyte Technology  | 970A-D3                                          | 2012 | 6       | 46      | 2           |
| Acer                 | Aspire X1301                                     | 2009 | 6       | 44      | 2           |
| MSI                  | MS-7970                                          | 2016 | 6       | 43      | 2           |
| Lenovo               | Product                                          | 2010 | 6       | 43      | 2           |
| ASUSTek Computer     | TUF GAMING Z590-PLUS WIFI                        | 2021 | 6       | 42      | 2           |
| Gigabyte Technology  | GA-870A-USB3L                                    | 2011 | 6       | 40      | 2           |
| ASRock               | Z590M-ITX/ax                                     | 2021 | 6       | 38      | 2           |
| ASRock               | B85M-HDS                                         | 2013 | 6       | 38      | 2           |
| Gigabyte Technology  | B360M H                                          | 2018 | 6       | 31      | 2           |
| Lenovo               | Board                                            | 2007 | 6       | 30      | 2           |
| Pegatron             | IPM31G                                           | 2009 | 6       | 28      | 2           |
| Gigabyte Technology  | B360 HD3P-LM                                     | 2019 | 6       | 23      | 2           |
| Fujitsu              | D3401-H1                                         | 2015 | 6       | 20      | 2           |
| Intel                | X99 V102                                         | 2019 | 5       | 111     | 2           |
| MSI                  | MS-7B92                                          | 2019 | 5       | 66      | 2           |
| AZW                  | AP35                                             | 2019 | 5       | 55      | 2           |
| Hewlett-Packard      | Desktop M01-F1xxx                                | 2021 | 5       | 48      | 2           |
| ASUSTek Computer     | SABERTOOTH Z170 S                                | 2017 | 5       | 42      | 2           |
| ASRock               | 4CoreDual-VSTA                                   | 2007 | 5       | 41      | 2           |
| ASUSTek Computer     | Maximus V FORMULA                                | 2013 | 5       | 40      | 2           |
| Acer                 | Aspire M1100                                     | 2007 | 5       | 40      | 2           |
| Hewlett-Packard      | Pavilion Desktop 595-p0xxx                       | 2018 | 5       | 39      | 2           |
| Acer                 | Aspire M3900                                     | 2010 | 5       | 39      | 2           |
| Gigabyte Technology  | EP45-DS3R                                        | 2008 | 5       | 39      | 2           |
| ASRock               | B250 Gaming K4                                   | 2017 | 5       | 38      | 2           |
| ASUSTek Computer     | G11CD                                            | 2016 | 5       | 38      | 2           |
| Gigabyte Technology  | B250-HD3P                                        | 2016 | 5       | 37      | 2           |
| ASUSTek Computer     | P5VD2-VM                                         | 2007 | 5       | 36      | 2           |
| Acer                 | Aspire E380                                      | 2006 | 5       | 35      | 2           |
| ASUSTek Computer     | P5GPL-X                                          | 2005 | 5       | 34      | 2           |
| Gigabyte Technology  | H510M H                                          | 2021 | 5       | 27      | 2           |
| ASUSTek Computer     | P5S800-VM                                        | 2005 | 5       | 27      | 2           |
| ASRock               | X99E-ITX/ac                                      | 2016 | 4       | 110     | 2           |
| Supermicro           | X9DAi                                            | 2013 | 4       | 100     | 2           |
| Huanan               | X79                                              | 2019 | 4       | 83      | 2           |
| ASUSTek Computer     | Rampage IV GENE                                  | 2011 | 4       | 83      | 2           |
| ASUSTek Computer     | V-P7H55E                                         | 2009 | 4       | 57      | 2           |
| ASRockRack           | ROMED8-2T                                        | 2020 | 4       | 55      | 2           |
| ASRock               | X570 PG Velocita                                 | 2020 | 4       | 51      | 2           |
| Biostar              | X470NH                                           | 2020 | 4       | 48      | 2           |
| Hewlett-Packard      | 8433 11                                          | 2018 | 4       | 46      | 2           |
| ASRock               | 970 Pro3                                         | 2012 | 4       | 44      | 2           |
| ASRock               | 890GM Pro3                                       | 2010 | 4       | 44      | 2           |
| Supermicro           | X9SCI/X9SCA                                      | 2012 | 4       | 43      | 2           |
| ASUSTek Computer     | E45M1-I DELUXE                                   | 2011 | 4       | 43      | 2           |
| ASRock               | Z170 Pro4                                        | 2018 | 4       | 42      | 2           |
| ASUSTek Computer     | A88XM-E/USB                                      | 2016 | 4       | 42      | 2           |
| ASUSTek Computer     | A8V Deluxe                                       | 2004 | 4       | 42      | 2           |
| Acer                 | Revo 70                                          | 2011 | 4       | 41      | 2           |
| Gigabyte Technology  | GB-BRR7H-4800                                    | 2021 | 4       | 40      | 2           |
| ASUSTek Computer     | P5K-V                                            | 2007 | 4       | 40      | 2           |
| Supermicro           | C2SBC-Q                                          | 2008 | 4       | 39      | 2           |
| Acer                 | Veriton M480G                                    | 2009 | 4       | 38      | 2           |
| MSI                  | MS-7B23                                          | 2018 | 4       | 36      | 2           |
| Foxconn              | nT435/nT535                                      | 2011 | 4       | 35      | 2           |
| MSI                  | MS-7142                                          | 2005 | 4       | 34      | 2           |
| Foxconn              | H61MXL/H61MXL-K                                  | 2011 | 4       | 33      | 2           |
| Gigabyte Technology  | GA-K8N-SLi                                       | 2005 | 4       | 33      | 2           |
| ASRock               | B360M Pro4                                       | 2018 | 4       | 32      | 2           |
| Hewlett-Packard      | AY627AA-ABA a4313w                               | 2009 | 4       | 32      | 2           |
| ASRock               | B560M-HDV                                        | 2021 | 4       | 28      | 2           |
| MSI                  | MS-7D22                                          | 2021 | 4       | 27      | 2           |
| Gigabyte Technology  | GB-BACE-3000                                     | 2017 | 4       | 27      | 2           |
| Fujitsu              | D3401-H2 S26361-D3401-H2                         | 2017 | 4       | 19      | 2           |
| MSI                  | MS-7883                                          | 2016 | 3       | 111     | 2           |
| ASUSTek Computer     | Z10PA-D8 Series                                  | 2018 | 3       | 107     | 2           |
| Gigabyte Technology  | X99-UD5 WIFI-CF                                  | 2017 | 3       | 102     | 2           |
| ASUSTek Computer     | Rampage II Extreme                               | 2011 | 3       | 68      | 2           |
| Hewlett-Packard      | EliteDesk 805 G6 Small Form Factor PC            | 2020 | 3       | 54      | 2           |
| Biostar              | TB250-BTC PRO                                    | 2017 | 3       | 38      | 2           |
| Gigabyte Technology  | P67A-UD3R-B3                                     | 2012 | 3       | 38      | 2           |
| ARLT Computer        | Mr. Business V                                   | 2010 | 3       | 38      | 2           |
| Gigabyte Technology  | B360 HD3                                         | 2018 | 3       | 36      | 2           |
| Dell                 | Inspiron 3662                                    | 2017 | 3       | 36      | 2           |
| Compaq               | KQ514AA-ABA SR5550F                              | 2008 | 3       | 36      | 2           |
| ASUSTek Computer     | PRIME B460M-K                                    | 2020 | 3       | 35      | 2           |
| Biostar              | P4M89-M7B                                        | 2007 | 3       | 35      | 2           |
| ASRock               | B560M-ITX/ac                                     | 2021 | 3       | 34      | 2           |
| Gigabyte Technology  | H67A-USB3-B3                                     | 2011 | 3       | 34      | 2           |
| Fujitsu              | FUTRO S900                                       | 2011 | 3       | 34      | 2           |
| Equus Computer Sy... | Nobilis                                          | 2009 | 3       | 34      | 2           |
| ASUSTek Computer     | P5V800-MX                                        | 2006 | 3       | 34      | 2           |
| ASUSTek Computer     | PRIME Q270M-C                                    | 2017 | 3       | 32      | 2           |
| ASUSTek Computer     | H170-PLUS D3                                     | 2015 | 3       | 32      | 2           |
| Lenovo               | H520S 10093                                      | 2012 | 3       | 32      | 2           |
| Positivo             | POS-VVCN896BD                                    | 2009 | 3       | 32      | 2           |
| Hewlett-Packard      | d530 SFF(DG059A)                                 | 2003 | 3       | 32      | 2           |
| Lanix                | MINI-TORRE                                       | 2011 | 3       | 30      | 2           |
| ASUSTek Computer     | PRIME H410M-R                                    | 2020 | 3       | 28      | 2           |
| ASUSTek Computer     | J3455M-E                                         | 2016 | 3       | 28      | 2           |
| ASRock               | P4VM8                                            | 2004 | 3       | 28      | 2           |
| Foxconn              | 45GM/45CM/45CM-S                                 | 2007 | 3       | 27      | 2           |
| Gigabyte Technology  | H110M-DS2-CF                                     | 2015 | 3       | 26      | 2           |
| Fujitsu              | D3417-B1                                         | 2016 | 3       | 25      | 2           |
| ASRock               | B360M-HDV                                        | 2018 | 3       | 24      | 2           |
| ASUSTek Computer     | Z10PE-D8 WS                                      | 2018 | 2       | 135     | 2           |
| Hewlett-Packard      | ProLiant ML350 Gen9                              | 2014 | 2       | 122     | 2           |
| Supermicro           | SYS-7038A-I                                      | 2019 | 2       | 118     | 2           |
| Supermicro           | PIO-618U-TR4T+-ST031                             | 2019 | 2       | 107     | 2           |
| ASRock               | X99 WS                                           | 2014 | 2       | 103     | 2           |
| ASUSTek Computer     | RS720Q-E8-RS12                                   | 2019 | 2       | 101     | 2           |
| Gigabyte Technology  | X99-SLI-CF                                       | 2016 | 2       | 96      | 2           |
| Dell                 | Precision 7820 Tower                             | 2018 | 2       | 88      | 2           |
| ASRock               | X58 Extreme                                      | 2010 | 2       | 64      | 2           |
| Gigabyte Technology  | X170-Extreme ECC                                 | 2016 | 2       | 63      | 2           |
| Hewlett-Packard      | Pavilion Gaming Desktop TG01-2xxx                | 2021 | 2       | 48      | 2           |
| ASUSTek Computer     | ROG STRIX Z490-A GAMING                          | 2020 | 2       | 44      | 2           |
| Sun Microsystems     | Ultra 24                                         | 2008 | 2       | 44      | 2           |
| Hyrican Informati... | Hyrican PC                                       | 2017 | 2       | 43      | 2           |
| Acer                 | Extensa M2610                                    | 2015 | 2       | 43      | 2           |
| ASUSTek Computer     | TUF GAMING H570-PRO                              | 2021 | 2       | 42      | 2           |
| Hewlett-Packard      | EliteDesk 705 G2 MT                              | 2016 | 2       | 42      | 2           |
| Fujitsu Siemens      | P35T-FB                                          | 2007 | 2       | 42      | 2           |
| Pegatron             | DB                                               | 2010 | 2       | 41      | 2           |
| Acer                 | Aspire X1800                                     | 2009 | 2       | 41      | 2           |
| Gigabyte Technology  | GA-MA78GPM-DS2H                                  | 2008 | 2       | 40      | 2           |
| Lenovo               | IdeaCentre B505 10039                            | 2010 | 2       | 39      | 2           |
| ASUSTek Computer     | NCL-DS                                           | 2005 | 2       | 38      | 2           |
| Pegatron             | C15B                                             | 2014 | 2       | 37      | 2           |
| Gigabyte Technology  | EP45T-DS3                                        | 2008 | 2       | 37      | 2           |
| MSI                  | MS-7270                                          | 2006 | 2       | 37      | 2           |
| ASUSTek Computer     | PRIME H470M-PLUS                                 | 2020 | 2       | 36      | 2           |
| Hewlett-Packard      | s5710t                                           | 2011 | 2       | 35      | 2           |
| Acer                 | Veriton X490G                                    | 2010 | 2       | 33      | 2           |
| Medion               | Akoya P5371 H/B727                               | 2015 | 2       | 32      | 2           |
| Intel                | DP45SG AAE27733-405                              | 2009 | 2       | 32      | 2           |
| Gigabyte Technology  | B85M-Gaming 3                                    | 2014 | 2       | 30      | 2           |
| Dell                 | PowerEdge T105                                   | 2008 | 2       | 30      | 2           |
| Shuttle              | DX30D                                            | 2017 | 2       | 29      | 2           |
| ASRock               | H170M-ITX/DL                                     | 2015 | 2       | 29      | 2           |
| TYAN Computer        | S5517                                            | 2013 | 2       | 29      | 2           |
| Hewlett-Packard      | GX705AA-ABM a6300la                              | 2007 | 2       | 29      | 2           |
| MSI                  | MS-7211                                          | 2005 | 2       | 28      | 2           |
| DEPO Computers       | P4M800-M                                         | 2005 | 2       | 28      | 2           |
| Gigabyte Technology  | N3160TN                                          | 2017 | 2       | 26      | 2           |
| Gigabyte Technology  | XP-M5S661GX                                      | 2005 | 2       | 22      | 2           |
| MSI                  | MS-7693                                          | 2011 | 158     | 52      | 3           |
| MSI                  | MS-7721                                          | 2012 | 147     | 57      | 3           |
| Dell                 | OptiPlex 755                                     | 2007 | 121     | 42      | 3           |
| Dell                 | OptiPlex 3020                                    | 2013 | 109     | 41      | 3           |
| MSI                  | MS-7B79                                          | 2018 | 108     | 68      | 3           |
| ASUSTek Computer     | SABERTOOTH 990FX R2.0                            | 2012 | 107     | 59      | 3           |
| Gigabyte Technology  | X570 AORUS MASTER                                | 2019 | 62      | 62      | 3           |
| ASRock               | B450 Pro4                                        | 2018 | 60      | 59      | 3           |
| Hewlett-Packard      | Z400 Workstation                                 | 2010 | 56      | 78      | 3           |
| ASUSTek Computer     | P8Z77-V PRO                                      | 2012 | 54      | 59      | 3           |
| Dell                 | Inspiron 530                                     | 2007 | 52      | 39      | 3           |
| MSI                  | MS-7C35                                          | 2019 | 51      | 67      | 3           |
| MSI                  | MS-7A15                                          | 2016 | 47      | 33      | 3           |
| Hewlett-Packard      | Z420 Workstation                                 | 2012 | 46      | 101     | 3           |
| MSI                  | MS-7971                                          | 2015 | 43      | 53      | 3           |
| ASUSTek Computer     | PRIME H310M-R R2.0                               | 2018 | 42      | 35      | 3           |
| Gigabyte Technology  | B550I AORUS PRO AX                               | 2020 | 39      | 59      | 3           |
| ASUSTek Computer     | M5A97                                            | 2011 | 39      | 50      | 3           |
| ASUSTek Computer     | CROSSHAIR VI HERO                                | 2017 | 34      | 54      | 3           |
| Gigabyte Technology  | X570 GAMING X                                    | 2019 | 29      | 58      | 3           |
| ASUSTek Computer     | SABERTOOTH X79                                   | 2012 | 26      | 98      | 3           |
| MSI                  | MS-7C75                                          | 2020 | 25      | 57      | 3           |
| Gigabyte Technology  | F2A55M-DS2                                       | 2012 | 25      | 38      | 3           |
| ASUSTek Computer     | P7P55D-E                                         | 2010 | 24      | 67      | 3           |
| MSI                  | MS-7A36                                          | 2018 | 24      | 46      | 3           |
| ASRock               | H110 Pro BTC+                                    | 2017 | 24      | 46      | 3           |
| MSI                  | MS-7B48                                          | 2017 | 24      | 45      | 3           |
| Dell                 | Precision WorkStation T7500                      | 2011 | 23      | 91      | 3           |
| ASUSTek Computer     | M5A78L/USB3                                      | 2011 | 23      | 65      | 3           |
| MSI                  | MS-7673                                          | 2011 | 23      | 53      | 3           |
| Gigabyte Technology  | P35-DS3R                                         | 2007 | 23      | 45      | 3           |
| ASRock               | X570 Steel Legend                                | 2019 | 22      | 60      | 3           |
| ASUSTek Computer     | M4A785TD-M EVO                                   | 2009 | 22      | 44      | 3           |
| ASUSTek Computer     | SABERTOOTH Z77                                   | 2012 | 21      | 44      | 3           |
| Gigabyte Technology  | GA-MA785GT-UD3H                                  | 2009 | 21      | 43      | 3           |
| Gigabyte Technology  | Z97P-D3                                          | 2014 | 20      | 40      | 3           |
| ASUSTek Computer     | P8Z77-V LX2                                      | 2012 | 20      | 38      | 3           |
| ASUSTek Computer     | P5KPL-VM                                         | 2007 | 20      | 34      | 3           |
| Gigabyte Technology  | B550 AORUS ELITE                                 | 2020 | 19      | 58      | 3           |
| Dell                 | XPS 8500                                         | 2012 | 19      | 40      | 3           |
| ASUSTek Computer     | P5P43TD                                          | 2009 | 18      | 52      | 3           |
| ASUSTek Computer     | A88X-PLUS                                        | 2013 | 18      | 47      | 3           |
| Apple                | MacPro6,1                                        | 2018 | 15      | 132     | 3           |
| ASUSTek Computer     | P9X79 WS                                         | 2013 | 15      | 91      | 3           |
| Gigabyte Technology  | B550 AORUS PRO AC                                | 2020 | 13      | 55      | 3           |
| ASUSTek Computer     | P8Z77-V LE PLUS                                  | 2012 | 13      | 44      | 3           |
| MSI                  | MS-7B46                                          | 2017 | 12      | 45      | 3           |
| Dell                 | Precision WorkStation 390                        | 2006 | 12      | 35      | 3           |
| ASUSTek Computer     | ROG STRIX Z390-H GAMING                          | 2019 | 11      | 52      | 3           |
| ASUSTek Computer     | M4A89GTD-PRO                                     | 2010 | 11      | 42      | 3           |
| ASRock               | Z77 Pro4                                         | 2012 | 11      | 38      | 3           |
| Gigabyte Technology  | H87N-WIFI                                        | 2013 | 9       | 44      | 3           |
| Gigabyte Technology  | F2A68HM-HD2                                      | 2014 | 9       | 39      | 3           |
| ASUSTek Computer     | P5LD2-Deluxe                                     | 2005 | 8       | 46      | 3           |
| Dell                 | PowerEdge FC630                                  | 2016 | 7       | 106     | 3           |
| MSI                  | MS-7B18                                          | 2018 | 7       | 39      | 3           |
| Acer                 | Aspire X3995                                     | 2012 | 7       | 37      | 3           |
| Gigabyte Technology  | M68MT-D3                                         | 2010 | 7       | 37      | 3           |
| Dell                 | Inspiron 3668                                    | 2017 | 7       | 35      | 3           |
| ASUSTek Computer     | Rampage IV BLACK EDITION                         | 2013 | 6       | 94      | 3           |
| Alienware            | Aurora Ryzen Edition                             | 2020 | 6       | 58      | 3           |
| ASRock               | H55M Pro                                         | 2010 | 6       | 49      | 3           |
| ASUSTek Computer     | PRIME Z590M-PLUS                                 | 2021 | 6       | 42      | 3           |
| Intel                | DH61WW AAG23116-300                              | 2012 | 6       | 29      | 3           |
| ASUSTek Computer     | PRIME Z590-A                                     | 2021 | 5       | 47      | 3           |
| Hewlett-Packard      | Pavilion Desktop TP01-2xxx                       | 2021 | 5       | 42      | 3           |
| OEM                  | Intel H81                                        | 2017 | 5       | 32      | 3           |
| ASUSTek Computer     | H61M-A/USB3                                      | 2013 | 5       | 31      | 3           |
| Intel                | DH61WW AAG23116-206                              | 2012 | 5       | 31      | 3           |
| Gigabyte Technology  | B560M AORUS ELITE                                | 2021 | 4       | 41      | 3           |
| HPE                  | ProLiant MicroServer Gen10 Plus                  | 2020 | 4       | 35      | 3           |
| Supermicro           | SYS-7048GR-TR                                    | 2015 | 3       | 126     | 3           |
| Gigabyte Technology  | X99-Ultra Gaming-CF                              | 2016 | 3       | 109     | 3           |
| Gigabyte Technology  | X99-Gaming 5                                     | 2014 | 3       | 106     | 3           |
| Supermicro           | X9DR3-F                                          | 2012 | 3       | 81      | 3           |
| Intel                | DP35DP AAD81073-205                              | 2009 | 3       | 44      | 3           |
| ASUSTek Computer     | P5GC-MX/MEDION/SI                                | 2007 | 3       | 31      | 3           |
| Gigabyte Technology  | EP45T-UD3R                                       | 2010 | 2       | 52      | 3           |
| PROLINE              | ProlinePartner                                   | 2019 | 2       | 41      | 3           |
| Hewlett-Packard      | 500-054                                          | 2013 | 2       | 40      | 3           |
| Dell                 | Precision 3240 Compact                           | 2020 | 2       | 33      | 3           |
| ASRock               | G41M-LE                                          | 2008 | 2       | 30      | 3           |
| ASUSTek Computer     | AT5NM10-I                                        | 2010 | 2       | 25      | 3           |
| MSI                  | MS-7592                                          | 2009 | 103     | 38      | 4           |
| Dell                 | Precision WorkStation T3500                      | 2009 | 71      | 69      | 4           |
| Dell                 | OptiPlex 9010                                    | 2012 | 63      | 46      | 4           |
| ASRock               | B450M-HDV R4.0                                   | 2018 | 52      | 50      | 4           |
| MSI                  | MS-7B84                                          | 2012 | 45      | 77      | 4           |
| Aquarius             | Pro, Std, Elt Series                             | 2005 | 42      | 95      | 4           |
| Dell                 | Precision Tower 5810                             | 2015 | 36      | 119     | 4           |
| Intel                | MAHOBAY                                          | 2012 | 22      | 32      | 4           |
| ASUSTek Computer     | TUF GAMING Z490-PLUS                             | 2020 | 19      | 50      | 4           |
| Dell                 | Precision T3610                                  | 2014 | 17      | 85      | 4           |
| ASUSTek Computer     | P8P67 DELUXE                                     | 2010 | 16      | 45      | 4           |
| Gigabyte Technology  | P85-D3                                           | 2013 | 16      | 44      | 4           |
| ASRock               | Z97 Extreme6                                     | 2014 | 11      | 53      | 4           |
| Gigabyte Technology  | H270-HD3                                         | 2016 | 11      | 40      | 4           |
| Hewlett-Packard      | Z840 Workstation                                 | 2015 | 10      | 125     | 4           |
| MSI                  | MS-7998                                          | 2015 | 9       | 57      | 4           |
| Hewlett-Packard      | t620 Quad Core TC                                | 2014 | 7       | 36      | 4           |
| Gigabyte Technology  | AM1M-S2H                                         | 2014 | 7       | 36      | 4           |
| Intel                | DG31PR AAE58249-306                              | 2009 | 7       | 36      | 4           |
| Fujitsu              | CELSIUS W410                                     | 2011 | 6       | 42      | 4           |
| ASRock               | B75M-DGS R2.0                                    | 2013 | 6       | 35      | 4           |
| ASUSTek Computer     | Z10PE-D16 WS                                     | 2017 | 5       | 154     | 4           |
| ASUSTek Computer     | P4C800-E                                         | 2003 | 5       | 45      | 4           |
| Medion               | MS-7204                                          | 2005 | 4       | 72      | 4           |
| ASUSTek Computer     | P5GC-MX/CKD/SI                                   | 2008 | 4       | 38      | 4           |
| Supermicro           | X9DA7/E                                          | 2016 | 3       | 117     | 4           |
| EVGA                 | 132-BL-E758 Tylersburg                           | 2010 | 3       | 77      | 4           |
| ASRock               | Z390M-ITX/ac                                     | 2018 | 3       | 33      | 4           |
| Dell                 | Precision 3650 Tower                             | 2021 | 3       | 32      | 4           |
| ASRockRack           | E3C242D4U2-2T                                    | 2019 | 3       | 30      | 4           |
| ECS                  | 945GZ/CT-M                                       | 2007 | 3       | 30      | 4           |
| Packard Bell         | imedia S3840                                     | 2011 | 3       | 29      | 4           |
| ASUSTek Computer     | Z10PA-U8 Series                                  | 2018 | 2       | 90      | 4           |
| ASUSTek Computer     | ROG STRIX H470-I GAMING                          | 2020 | 2       | 32      | 4           |
| Hewlett-Packard      | ProDesk 400 G7 Microtower PC                     | 2021 | 2       | 31      | 4           |
| Dell                 | Inspiron 3268                                    | 2018 | 2       | 30      | 4           |
| Dell                 | OptiPlex 9020                                    | 2013 | 154     | 45      | 5           |
| Gigabyte Technology  | X570 I AORUS PRO WIFI                            | 2019 | 43      | 67      | 5           |
| MSI                  | MS-7B93                                          | 2019 | 28      | 60      | 5           |
| MSI                  | MS-7751                                          | 2012 | 18      | 42      | 5           |
| Hewlett-Packard      | ProDesk 600 G1 TWR                               | 2013 | 17      | 47      | 5           |
| ASUSTek Computer     | PRIME Z490-A                                     | 2020 | 11      | 44      | 5           |
| ASRock               | AM2NF6G-VSTA                                     | 2006 | 10      | 41      | 5           |
| ASUSTek Computer     | P8Z77-I DELUXE                                   | 2012 | 8       | 33      | 5           |
| Shuttle              | DS81D                                            | 2014 | 6       | 42      | 5           |
| ASUSTek Computer     | K30BF_M32BF_A_F_K31BF_6                          | 2015 | 6       | 41      | 5           |
| ASUSTek Computer     | PRIME B350M-E                                    | 2017 | 5       | 48      | 5           |
| Gigabyte Technology  | Z490 UD                                          | 2020 | 3       | 39      | 5           |
| MSI                  | MS-7A21                                          | 2016 | 2       | 129     | 5           |
| Hewlett-Packard      | Compaq dc7100 SFF(DX878AV)                       | 2004 | 2       | 30      | 5           |
| ASUSTek Computer     | All Series                                       | 2012 | 1399    | 132     | 6           |
| ASUSTek Computer     | ROG STRIX X570-E GAMING                          | 2019 | 68      | 73      | 6           |
| ASUSTek Computer     | Z170 PRO GAMING                                  | 2015 | 61      | 51      | 6           |
| ASRock               | X470 Taichi                                      | 2018 | 28      | 67      | 6           |
| ASUSTek Computer     | PRIME B360M-A                                    | 2018 | 24      | 43      | 6           |
| MSI                  | MS-7522                                          | 2009 | 17      | 74      | 6           |
| MSI                  | MS-7977                                          | 2016 | 17      | 41      | 6           |
| ASUSTek Computer     | PRIME Z270-K                                     | 2016 | 10      | 36      | 6           |
| Gigabyte Technology  | B360 AORUS GAMING 3 WIFI                         | 2018 | 5       | 38      | 6           |
| Gigabyte Technology  | B250M-Gaming 3                                   | 2017 | 2       | 37      | 6           |
| MSI                  | MS-7885                                          | 2015 | 37      | 129     | 7           |
| ASUSTek Computer     | M4A785TD-V EVO                                   | 2009 | 33      | 45      | 7           |
| Dell                 | Precision Tower 7910                             | 2015 | 11      | 140     | 7           |
| Dell                 | Precision T7610                                  | 2014 | 11      | 104     | 7           |
| Hewlett-Packard      | Z8 G4 Workstation                                | 2019 | 3       | 89      | 7           |
| Gigabyte Technology  | 945GCMX-S2                                       | 2007 | 26      | 34      | 8           |
| ASUSTek Computer     | ROG Maximus XII HERO                             | 2020 | 7       | 62      | 8           |

By Vendor
---------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Samples | Devices | Non-working |
|----------------------|---------|---------|-------------|
| ASUSTek Computer     | 14300   | 38      | 1           |
| Gigabyte Technology  | 9484    | 38      | 1           |
| MSI                  | 5206    | 39      | 1           |
| ASRock               | 5163    | 37      | 1           |
| Hewlett-Packard      | 3990    | 37      | 1           |
| Dell                 | 3506    | 36      | 1           |
| Lenovo               | 1322    | 33      | 1           |
| Intel                | 1281    | 36      | 1           |
| Acer                 | 855     | 33      | 1           |
| Biostar              | 541     | 34      | 1           |
| ECS                  | 516     | 31      | 1           |
| Fujitsu              | 425     | 33      | 1           |
| Medion               | 287     | 35      | 1           |
| Foxconn              | 261     | 32      | 1           |
| Fujitsu Siemens      | 189     | 34      | 1           |
| Positivo             | 136     | 29      | 1           |
| Supermicro           | 136     | 60      | 1           |
| Apple                | 122     | 72      | 1           |
| Pegatron             | 116     | 30      | 1           |
| Packard Bell         | 112     | 37      | 1           |
| Gateway              | 104     | 35      | 1           |
| Huanan               | 102     | 92      | 1           |
| Shuttle              | 92      | 31      | 1           |
| PCWare               | 88      | 27      | 1           |
| Alienware            | 87      | 48      | 1           |
| eMachines            | 87      | 32      | 1           |
| Compaq               | 87      | 31      | 1           |
| DEPO Computers       | 66      | 32      | 1           |
| Aquarius             | 58      | 31      | 1           |
| ABIT                 | 51      | 34      | 1           |
| EVGA                 | 46      | 54      | 1           |
| Kraftway             | 45      | 30      | 1           |
| AZW                  | 45      | 28      | 1           |
| IBM                  | 42      | 32      | 1           |
| Semp Toshiba         | 38      | 30      | 1           |
| System76             | 35      | 49      | 1           |
| ASRockRack           | 35      | 48      | 1           |
| AMD                  | 34      | 37      | 1           |
| CSL-Computer         | 33      | 41      | 1           |
| WinFast              | 33      | 36      | 1           |
| EPoX Computer        | 32      | 32      | 1           |
| OEM                  | 32      | 27      | 1           |
| Wortmann AG          | 30      | 40      | 1           |
| Itautec              | 29      | 31      | 1           |
| Nvidia               | 26      | 34      | 1           |
| ZOTAC                | 24      | 34      | 1           |
| AAEON                | 23      | 30      | 1           |
| Megaware             | 23      | 27      | 1           |
| PCChips              | 23      | 28      | 1           |
| Google               | 22      | 29      | 1           |
| K-Systems            | 20      | 33      | 1           |
| NEC Computers        | 20      | 30      | 1           |
| OEGStone             | 19      | 34      | 1           |
| TYAN Computer        | 18      | 32      | 1           |
| ATComputers          | 17      | 33      | 1           |
| HARDKERNEL           | 16      | 31      | 1           |
| WYSE                 | 15      | 35      | 1           |
| SiS Technology       | 15      | 24      | 1           |
| VIA Technologies     | 14      | 23      | 1           |
| HCL Infosystems L... | 14      | 26      | 1           |
| Colorful Technology  | 13      | 32      | 1           |
| Hyrican Informati... | 13      | 36      | 1           |
| Qbex                 | 13      | 30      | 1           |
| BESSTAR Tech         | 13      | 37      | 1           |
| Sapphire             | 13      | 36      | 1           |
| Minix                | 12      | 24      | 0           |
| RM                   | 12      | 34      | 1           |
| Viglen               | 11      | 31      | 0           |
| Novatech             | 11      | 37      | 1           |
| Login Informatica    | 11      | 25      | 1           |
| AMI                  | 11      | 22      | 1           |
| LattePanda           | 10      | 39      | 0           |
| JW Technology        | 10      | 35      | 0           |
| ITMediaConsult       | 10      | 31      | 0           |
| Arbyte Computers     | 10      | 30      | 1           |
| TSINGHUA TONGFANG... | 9       | 29      | 0           |
| Seco                 | 9       | 43      | 1           |
| Thirdwave            | 9       | 41      | 1           |
| Navigator            | 9       | 30      | 1           |
| AOpen                | 9       | 30      | 1           |
| PowerSpec            | 9       | 36      | 1           |
| NT Computer          | 9       | 31      | 1           |
| Sony                 | 9       | 32      | 1           |
| MicroElectronics     | 8       | 43      | 0           |
| Advent               | 8       | 31      | 0           |
| PC Engines           | 8       | 28      | 0           |
| Tarox                | 8       | 45      | 1           |
| XFX                  | 8       | 44      | 1           |
| PRIMINFO             | 8       | 34      | 1           |
| Standard             | 8       | 32      | 1           |
| ZOOSTORM             | 8       | 36      | 1           |
| Lanix                | 8       | 31      | 1           |
| DFI                  | 7       | 38      | 0           |
| SYWZ                 | 7       | 33      | 0           |
| Philco               | 7       | 33      | 0           |
| VS Company           | 7       | 29      | 0           |
| Jetway               | 7       | 27      | 0           |
| NCR                  | 7       | 25      | 0           |
| PC Specialist        | 7       | 41      | 1           |
| Samsung Electronics  | 7       | 36      | 1           |
| Chuwi                | 7       | 33      | 1           |
| Olidata              | 6       | 32      | 0           |
| Seneca               | 6       | 31      | 0           |
| 3Q                   | 6       | 31      | 0           |
| Wibtek               | 6       | 29      | 0           |
| Information Compu... | 6       | 28      | 0           |
| DEXP                 | 6       | 26      | 0           |
| Digibras             | 6       | 26      | 0           |
| LORD ELECTRONICS     | 6       | 29      | 1           |
| Digitron             | 6       | 29      | 1           |
| Packard Bell NEC     | 6       | 26      | 1           |
| CCL Computers        | 6       | 39      | 1           |
| MouseComputer        | 6       | 32      | 1           |
| MiTAC                | 6       | 27      | 1           |
| ARLT Computer        | 6       | 37      | 1           |
| ICP / iEi            | 6       | 31      | 1           |
| INTELBRAS            | 6       | 28      | 1           |
| DALCO AG Switzerland | 6       | 24      | 1           |
| PC Factory           | 5       | 33      | 0           |
| DNS                  | 5       | 33      | 0           |
| ELSA                 | 5       | 32      | 0           |
| LG Electronics       | 5       | 32      | 0           |
| CyberPowerPC         | 5       | 32      | 0           |
| extracomputer        | 5       | 30      | 0           |
| SiComputer           | 5       | 28      | 0           |
| HC                   | 5       | 44      | 1           |
| ECT                  | 5       | 36      | 1           |
| American Megatrends  | 5       | 36      | 1           |
| AWOW                 | 5       | 35      | 1           |
| iEi                  | 5       | 33      | 1           |
| CCE                  | 5       | 27      | 1           |
| iRU                  | 5       | 26      | 1           |
| HPE                  | 5       | 35      | 1           |
| Maxtang              | 4       | 39      | 0           |
| T-Group              | 4       | 36      | 0           |
| XENTA                | 4       | 35      | 0           |
| ACTION               | 4       | 32      | 0           |
| Daten Tecnologia     | 4       | 31      | 0           |
| MEGA                 | 4       | 30      | 0           |
| APD                  | 4       | 29      | 0           |
| Centrium             | 4       | 27      | 0           |
| NCS-Tech             | 4       | 35      | 1           |
| Bluechip Computer    | 4       | 34      | 1           |
| ONDA                 | 4       | 32      | 1           |
| MAXSUN               | 4       | 32      | 1           |
| TONK                 | 4       | 25      | 1           |
| CdcPointSpa          | 4       | 36      | 1           |
| Punch Technology     | 4       | 32      | 1           |
| Protectli            | 4       | 27      | 1           |
| Insyde               | 4       | 31      | 1           |
| Phitronics           | 4       | 30      | 1           |
| Brunen IT            | 4       | 35      | 1           |
| Sirona Dental Sys... | 3       | 82      | 0           |
| Entroware            | 3       | 35      | 0           |
| CDM                  | 3       | 33      | 0           |
| DataLogic            | 3       | 33      | 0           |
| MAXDATA              | 3       | 30      | 0           |
| BANGHO               | 3       | 29      | 0           |
| WINCOR NIXDORF       | 3       | 29      | 0           |
| JGINYUE              | 3       | 28      | 0           |
| ADLINK Technology    | 3       | 28      | 0           |
| LTD Delovoy Office   | 3       | 27      | 0           |
| ICL                  | 3       | 27      | 0           |
| Tekram Technology    | 3       | 24      | 0           |
| KLLISRE              | 3       | 80      | 1           |
| MCJ                  | 3       | 44      | 1           |
| WeiBu                | 3       | 41      | 1           |
| NCA Group            | 3       | 38      | 1           |
| Acidanthera          | 3       | 38      | 1           |
| BCM                  | 3       | 31      | 1           |
| Xi3                  | 3       | 30      | 1           |
| Sun Microsystems     | 3       | 44      | 1           |
| T-bao                | 3       | 42      | 1           |
| Excimer DM           | 3       | 34      | 1           |
| Equus Computer Sy... | 3       | 32      | 1           |
| langchao             | 3       | 31      | 1           |
| CompuLab             | 3       | 25      | 1           |
| JINGSHA              | 3       | 108     | 1           |
| Soyo                 | 3       | 26      | 2           |
| Clevo                | 2       | 42      | 0           |
| SeeedStudio          | 2       | 42      | 0           |
| PDS                  | 2       | 41      | 0           |
| OriginPC             | 2       | 40      | 0           |
| Megaport             | 2       | 40      | 0           |
| Exertis_CapTech      | 2       | 40      | 0           |
| DIEBOLD              | 2       | 37      | 0           |
| BUYMPC               | 2       | 36      | 0           |
| ASL                  | 2       | 36      | 0           |
| Mecer                | 2       | 35      | 0           |
| Mikrolog             | 2       | 34      | 0           |
| NETAXP               | 2       | 33      | 0           |
| IPEX                 | 2       | 33      | 0           |
| R-StyleComputers     | 2       | 32      | 0           |
| ITSUMI               | 2       | 31      | 0           |
| Olivetti             | 2       | 31      | 0           |
| ONKYO                | 2       | 30      | 0           |
| KOUZIRO              | 2       | 30      | 0           |
| INSYS                | 2       | 30      | 0           |
| Dixonsxp             | 2       | 30      | 0           |
| EPSON DIRECT         | 2       | 29      | 0           |
| ECS-USA              | 2       | 29      | 0           |
| Logic Supply         | 2       | 29      | 0           |
| Exo                  | 2       | 29      | 0           |
| Datto                | 2       | 29      | 0           |
| Casper               | 2       | 29      | 0           |
| STSS                 | 2       | 28      | 0           |
| JOOYON               | 2       | 28      | 0           |
| Canyon               | 2       | 28      | 0           |
| SiYW                 | 2       | 28      | 0           |
| EAGLE EYE NETWORKS   | 2       | 27      | 0           |
| TriGem Computer      | 2       | 27      | 0           |
| SYS                  | 2       | 27      | 0           |
| HOUTER               | 2       | 27      | 0           |
| Panasonic            | 2       | 26      | 0           |
| YANYU                | 2       | 26      | 0           |
| FLEX Industries      | 2       | 25      | 0           |
| Yanling              | 2       | 24      | 0           |
| ELSKY                | 2       | 24      | 0           |
| QTQD                 | 2       | 24      | 0           |
| Optima               | 2       | 23      | 0           |
| ALDO                 | 2       | 23      | 0           |
| Cisco Systems        | 2       | 89      | 1           |
| PC Specailist        | 2       | 54      | 1           |
| Complet              | 2       | 46      | 1           |
| Mustek               | 2       | 45      | 1           |
| CIARA                | 2       | 31      | 1           |
| Advantech            | 2       | 30      | 1           |
| 3NOD                 | 2       | 29      | 1           |
| iQon.ie              | 2       | 28      | 1           |
| THEIS-Computer       | 2       | 28      | 1           |
| Elo TouchSystems     | 2       | 28      | 1           |
| OPTIMUS              | 2       | 26      | 1           |
| ISYNC                | 2       | 22      | 1           |
| Helios Business C... | 2       | 39      | 1           |
| KLONDIKE COMPUTERS   | 2       | 33      | 1           |
| Varian Medical Sy... | 2       | 28      | 1           |
| PROLINE              | 2       | 37      | 2           |
| QDI                  | 2       | 27      | 2           |

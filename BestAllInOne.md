Best Linux All In One
=====================

This is a list of all in ones with best Linux-compatibility in the terms of
maximal number of Linux-compatible devices on board and maximal overall
popularity of a model.

Everyone can contribute to this report by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Contents
--------

1. [ By Model ](#by-model)
2. [ By Vendor ](#by-vendor)

By Model
--------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Model                                            | Year | Samples | Devices | Non-working |
|----------------------|--------------------------------------------------|------|---------|---------|-------------|
| Apple                | iMac18,2                                         | 2017 | 21      | 48      | 0           |
| Apple                | iMac13,1                                         | 2015 | 20      | 44      | 0           |
| Apple                | iMac11,3                                         | 2010 | 19      | 66      | 0           |
| Dell                 | OptiPlex 9020 AIO                                | 2013 | 9       | 40      | 0           |
| Samsung Electronics  | DP700A3D/DM700A3D/DB701A3D/DP700A7D              | 2013 | 9       | 38      | 0           |
| Dell                 | Inspiron One 2330                                | 2012 | 9       | 35      | 0           |
| Apple                | iMac15,1                                         | 2018 | 8       | 46      | 0           |
| Dell                 | Inspiron 2350                                    | 2014 | 8       | 40      | 0           |
| Lenovo               | C440 10104                                       | 2012 | 7       | 39      | 0           |
| Dell                 | Inspiron 3464 AIO                                | 2017 | 7       | 36      | 0           |
| Acer                 | Aspire C22-820                                   | 2018 | 7       | 26      | 0           |
| Hewlett-Packard      | Pavilion All-in-One 27-xa0xxx                    | 2019 | 6       | 42      | 0           |
| Hewlett-Packard      | ProOne 400 G1 AiO                                | 2013 | 6       | 34      | 0           |
| Hewlett-Packard      | Pavilion All-in-One 24-xa0xxx                    | 2019 | 5       | 42      | 0           |
| Apple                | iMac16,1                                         | 2017 | 5       | 33      | 0           |
| BESSTAR Tech         | U700                                             | 2020 | 5       | 30      | 0           |
| Lenovo               | C260 10160                                       | 2014 | 5       | 30      | 0           |
| Lenovo               | C240 10113                                       | 2012 | 5       | 30      | 0           |
| Acer                 | Aspire Z1-601                                    | 2014 | 5       | 29      | 0           |
| Acer                 | Aspire C22-865                                   | 2018 | 5       | 28      | 0           |
| Acer                 | Aspire C27-962                                   | 2019 | 4       | 36      | 0           |
| Lenovo               | C560 10150                                       | 2014 | 4       | 36      | 0           |
| Acer                 | Aspire ZC-606                                    | 2014 | 4       | 35      | 0           |
| Acer                 | Aspire Z3-715                                    | 2016 | 4       | 34      | 0           |
| ASUSTek Computer     | ASUS Vivo AIO V241EA_V241EA                      | 2020 | 4       | 33      | 0           |
| MSI                  | MS-B06211                                        | 2013 | 4       | 33      | 0           |
| Apple                | iMac14,4                                         | 2018 | 4       | 32      | 0           |
| LG Electronics       | 22V280-L.BJ31P1                                  | 2018 | 4       | 31      | 0           |
| Acer                 | Aspire C24-865                                   | 2018 | 4       | 31      | 0           |
| ASUSTek Computer     | Vivo AIO 22 V222GA_V222GA                        | 2019 | 4       | 30      | 0           |
| Dell                 | Inspiron 27 7775                                 | 2017 | 3       | 44      | 0           |
| Lenovo               | C245 10114                                       | 2012 | 3       | 40      | 0           |
| Acer                 | Aspire Z3100                                     | 2011 | 3       | 40      | 0           |
| Dell                 | Inspiron 24 5475                                 | 2017 | 3       | 39      | 0           |
| Dell                 | Inspiron 24-3455                                 | 2016 | 3       | 38      | 0           |
| Apple                | iMac14,3                                         | 2018 | 3       | 36      | 0           |
| Apple                | iMac18,1                                         | 2018 | 3       | 35      | 0           |
| Positivo             | DH8BW01                                          | 2016 | 3       | 34      | 0           |
| Dell                 | Inspiron 20 Model 3048                           | 2015 | 3       | 34      | 0           |
| Acer                 | Aspire ZC-700G                                   | 2015 | 3       | 34      | 0           |
| Hewlett-Packard      | 200 Pro G4 22 All-in-One PC                      | 2020 | 3       | 32      | 0           |
| ASUSTek Computer     | Vivo AIO 24 V241FA_V241FA                        | 2019 | 3       | 32      | 0           |
| Dell                 | Inspiron One 2305                                | 2010 | 3       | 32      | 0           |
| Dell                 | Inspiron 20 Model 3043                           | 2014 | 3       | 31      | 0           |
| LG Electronics       | 22V280-L.BY31P1                                  | 2018 | 3       | 30      | 0           |
| LG Electronics       | 22V270-L.BJ31P1                                  | 2017 | 3       | 30      | 0           |
| Dell                 | OptiPlex 7440 AIO                                | 2015 | 3       | 30      | 0           |
| Positivo             | DC8BT11                                          | 2014 | 3       | 30      | 0           |
| LG Electronics       | 22V240-L.BJ34P1                                  | 2016 | 3       | 29      | 0           |
| Acidanthera          | iMac17,1                                         | 2020 | 2       | 46      | 0           |
| Hewlett-Packard      | Pavilion All-in-One 24-xa1xxx                    | 2019 | 2       | 44      | 0           |
| Acer                 | Aspire Z3171                                     | 2011 | 2       | 43      | 0           |
| Sony                 | VPCL22Z1R                                        | 2011 | 2       | 42      | 0           |
| MSI                  | MS-AA53                                          | 2011 | 2       | 42      | 0           |
| Dell                 | XPS 7760 AIO                                     | 2019 | 2       | 41      | 0           |
| Hewlett-Packard      | Z1 Workstation                                   | 2018 | 2       | 40      | 0           |
| Wortmann AG          | TERRA_PC                                         | 2016 | 2       | 40      | 0           |
| Dell                 | Precision 5720 AIO                               | 2016 | 2       | 40      | 0           |
| Itautec              | Infoway AL2010                                   | 2012 | 2       | 40      | 0           |
| Hewlett-Packard      | Compaq 6000 Pro AiO Business PC                  | 2010 | 2       | 40      | 0           |
| Acer                 | Veriton Z4860G                                   | 2019 | 2       | 39      | 0           |
| Acer                 | Aspire C24-320                                   | 2018 | 2       | 39      | 0           |
| Dell                 | Inspiron 20 Model 3045                           | 2014 | 2       | 39      | 0           |
| Lenovo               | ThinkCentre M90z 2471W18                         | 2013 | 2       | 39      | 0           |
| Hewlett-Packard      | 20-c020                                          | 2016 | 2       | 38      | 0           |
| T-Platforms          | Tavolga                                          | 2015 | 2       | 38      | 0           |
| Dell                 | OptiPlex 9030 AIO                                | 2015 | 2       | 38      | 0           |
| Sony                 | VPCJ21S1R                                        | 2011 | 2       | 38      | 0           |
| Samsung Electronics  | DP505/DM515                                      | 2014 | 2       | 37      | 0           |
| Lenovo               | S40-40 F0AX00EAPB                                | 2014 | 2       | 37      | 0           |
| Dell                 | OptiPlex 7460 AIO                                | 2020 | 2       | 36      | 0           |
| Hewlett-Packard      | 23-r191la                                        | 2015 | 2       | 36      | 0           |
| Lenovo               | IdeaCentre B550 F0A60003BR                       | 2013 | 2       | 36      | 0           |
| Samsung Electronics  | P500A2D                                          | 2012 | 2       | 36      | 0           |
| Acer                 | Aspire Z1620                                     | 2012 | 2       | 36      | 0           |
| Acidanthera          | iMac13,2                                         | 2020 | 2       | 35      | 0           |
| Acer                 | Veriton Z4660G                                   | 2019 | 2       | 35      | 0           |
| ASUSTek Computer     | Vivo AIO 24 V241FF_V241FF                        | 2019 | 2       | 35      | 0           |
| ASUSTek Computer     | Z240IC-H170                                      | 2016 | 2       | 35      | 0           |
| Hewlett-Packard      | 20-r118                                          | 2015 | 2       | 35      | 0           |
| Acer                 | Aspire U5-710                                    | 2015 | 2       | 35      | 0           |
| Hewlett-Packard      | 18-5200br                                        | 2014 | 2       | 35      | 0           |
| Dell                 | XPS One 2710                                     | 2012 | 2       | 35      | 0           |
| Apple                | iMac6,1                                          | 2007 | 2       | 35      | 0           |
| Lenovo               | IdeaCentre A340-22IWL F0EB006RRI                 | 2019 | 2       | 34      | 0           |
| ASUSTek Computer     | Zen AIO 24 ZN242GD_ZN242GD                       | 2018 | 2       | 34      | 0           |
| Lenovo               | IdeaCentre B550 F0A60004BR                       | 2013 | 2       | 34      | 0           |
| Dell                 | Vostro 360                                       | 2011 | 2       | 33      | 0           |
| Hewlett-Packard      | 24-b201nt                                        | 2018 | 2       | 32      | 0           |
| Dell                 | OptiPlex 7450 AIO                                | 2018 | 2       | 32      | 0           |
| ASUSTek Computer     | V241IC-R                                         | 2018 | 2       | 32      | 0           |
| Hewlett-Packard      | 24-b015a                                         | 2017 | 2       | 32      | 0           |
| ASUSTek Computer     | V230IC-DDR4                                      | 2016 | 2       | 32      | 0           |
| Lenovo               | C470 10170                                       | 2014 | 2       | 32      | 0           |
| Hewlett-Packard      | ProOne 600 G1 AiO                                | 2014 | 2       | 32      | 0           |
| Hewlett-Packard      | ProOne 440 G4 23.8-in NT AiO                     | 2020 | 2       | 31      | 0           |
| Hewlett-Packard      | ProOne 600 G5 21.5-in All-in-One                 | 2019 | 2       | 31      | 0           |
| ASUSTek Computer     | Vivo AIO 16 V161GA_A41GA                         | 2018 | 2       | 31      | 0           |
| ASUSTek Computer     | V221IC                                           | 2017 | 2       | 31      | 0           |
| Dell                 | Inspiron 5400 AIO                                | 2021 | 2       | 30      | 0           |
| BESSTAR Tech         | GN41                                             | 2018 | 2       | 30      | 0           |
| ASUSTek Computer     | Vivo AIO 16 V161GA_V161GA                        | 2018 | 2       | 30      | 0           |
| LG Electronics       | 24V550-G.BJ31P1                                  | 2016 | 2       | 30      | 0           |
| MSI                  | MS-A912                                          | 2010 | 2       | 29      | 0           |
| ASUSTek Computer     | Vivo AIO 22 V222UA                               | 2019 | 2       | 28      | 0           |
| Lenovo               | IdeaCentre AIO 310-20IAP F0CL0014LD              | 2016 | 2       | 28      | 0           |
| Dell                 | OptiPlex 3011 AIO                                | 2013 | 2       | 28      | 0           |
| ASUSTek Computer     | EB1035                                           | 2012 | 2       | 28      | 0           |
| Acer                 | Aspire C24-963                                   | 2020 | 2       | 27      | 0           |
| MSI                  | MS-B09611                                        | 2015 | 2       | 27      | 0           |
| Acer                 | Aspire Z1-611                                    | 2015 | 2       | 27      | 0           |
| Lenovo               | N300 10161                                       | 2014 | 2       | 27      | 0           |
| MiTAC                | AIO M650                                         | 2011 | 2       | 27      | 0           |
| Apple                | iMac7,1                                          | 2007 | 44      | 48      | 1           |
| Apple                | iMac9,1                                          | 2009 | 38      | 42      | 1           |
| Apple                | iMac10,1                                         | 2009 | 35      | 43      | 1           |
| Apple                | iMac17,1                                         | 2019 | 15      | 50      | 1           |
| Apple                | iMac11,1                                         | 2010 | 14      | 63      | 1           |
| Apple                | iMac18,3                                         | 2019 | 14      | 47      | 1           |
| Apple                | iMac19,1                                         | 2019 | 13      | 52      | 1           |
| Apple                | iMac14,1                                         | 2016 | 13      | 36      | 1           |
| Apple                | iMac5,1                                          | 2007 | 12      | 39      | 1           |
| Dell                 | OptiPlex 9010 AIO                                | 2013 | 12      | 33      | 1           |
| Hewlett-Packard      | Pro 3420 AiO PC                                  | 2011 | 11      | 31      | 1           |
| Apple                | iMac13,2                                         | 2018 | 10      | 48      | 1           |
| Hewlett-Packard      | All-in-One                                       | 2018 | 10      | 40      | 1           |
| Hewlett-Packard      | All-in-One 22-c0xx                               | 2018 | 7       | 42      | 1           |
| Lenovo               | C540 10110                                       | 2013 | 6       | 41      | 1           |
| Apple                | iMac4,1                                          | 2006 | 6       | 34      | 1           |
| Dell                 | Inspiron 20-3052                                 | 2015 | 6       | 32      | 1           |
| Dell                 | OptiPlex 3030 AIO                                | 2014 | 6       | 32      | 1           |
| Dell                 | Inspiron 23 Model 5348                           | 2014 | 5       | 37      | 1           |
| Acidanthera          | iMac19,1                                         | 2020 | 4       | 46      | 1           |
| Lenovo               | IdeaCentre B300 10051                            | 2010 | 4       | 42      | 1           |
| Dell                 | Inspiron One 2310                                | 2010 | 4       | 38      | 1           |
| Acer                 | Aspire Z3-615                                    | 2014 | 4       | 37      | 1           |
| Lenovo               | C340 10102                                       | 2012 | 4       | 36      | 1           |
| Lenovo               | C200                                             | 2011 | 4       | 31      | 1           |
| Hewlett-Packard      | All-in-One 22-df0xxx                             | 2019 | 3       | 44      | 1           |
| Sony                 | VPCL22S1R                                        | 2011 | 3       | 43      | 1           |
| Hewlett-Packard      | Pavilion All-in-One 24-r0xx                      | 2017 | 3       | 42      | 1           |
| Lenovo               | C460 10149                                       | 2014 | 3       | 42      | 1           |
| Acer                 | Aspire Z5101                                     | 2011 | 3       | 42      | 1           |
| Packard Bell         | oneTwo S3230                                     | 2012 | 3       | 40      | 1           |
| ASUSTek Computer     | ET2010AG                                         | 2010 | 3       | 40      | 1           |
| Packard Bell         | ONETWO M3700                                     | 2009 | 3       | 38      | 1           |
| Pegatron             | H81-P2                                           | 2014 | 3       | 37      | 1           |
| Hewlett-Packard      | HP3520 Aio                                       | 2012 | 3       | 35      | 1           |
| Lenovo               | HORIZON 2s F0AT0003US                            | 2014 | 3       | 33      | 1           |
| LG Electronics       | V320-M.BG31P1                                    | 2013 | 3       | 33      | 1           |
| Hewlett-Packard      | Compaq 8200 Elite AiO Business PC                | 2011 | 3       | 32      | 1           |
| Dell                 | Inspiron 3477 AIO                                | 2018 | 3       | 31      | 1           |
| Hewlett-Packard      | Pavilion All-in-One 27-d0xxx                     | 2020 | 2       | 47      | 1           |
| Lenovo               | IdeaCentre B540p 3363                            | 2012 | 2       | 47      | 1           |
| Lenovo               | C225                                             | 2011 | 2       | 43      | 1           |
| Acer                 | Aspire Z3751                                     | 2010 | 2       | 43      | 1           |
| Hewlett-Packard      | All-in-One 22-c1xx                               | 2019 | 2       | 42      | 1           |
| Lenovo               | IdeaCentre A310 10056                            | 2010 | 2       | 40      | 1           |
| Lenovo               | C205                                             | 2011 | 2       | 39      | 1           |
| Acer                 | Aspire Z3771                                     | 2011 | 2       | 39      | 1           |
| Acer                 | Aspire Z5710                                     | 2010 | 2       | 39      | 1           |
| Pegatron             | H81-P1                                           | 2014 | 2       | 37      | 1           |
| MSI                  | MS-AE6711                                        | 2014 | 2       | 37      | 1           |
| MSI                  | MS-AC1511                                        | 2014 | 2       | 37      | 1           |
| 3NOD                 | TGS215DB                                         | 2013 | 2       | 37      | 1           |
| Acer                 | Aspire Z5771                                     | 2011 | 2       | 36      | 1           |
| MSI                  | MS-6657                                          | 2009 | 2       | 35      | 1           |
| Acer                 | Aspire Z3730                                     | 2010 | 2       | 34      | 1           |
| Apple                | iMac5,2                                          | 2007 | 2       | 34      | 1           |
| Dell                 | Inspiron 24-5459                                 | 2015 | 2       | 31      | 1           |
| Acer                 | Aspire Z5761                                     | 2011 | 2       | 31      | 1           |
| Hewlett-Packard      | EliteOne 800 G2 23-in Non-Touch AiO              | 2016 | 2       | 30      | 1           |
| Acer                 | Aspire Z1-622                                    | 2015 | 2       | 30      | 1           |
| ASUSTek Computer     | V200IB                                           | 2015 | 2       | 29      | 1           |
| AIO                  | H61H-G11                                         | 2013 | 2       | 28      | 1           |
| Apple                | iMac8,1                                          | 2008 | 50      | 41      | 2           |
| Hewlett-Packard      | All-in-One 24-f0xx                               | 2018 | 21      | 45      | 2           |
| Apple                | iMac19,2                                         | 2020 | 14      | 50      | 2           |
| Dell                 | Inspiron One 2320                                | 2011 | 12      | 34      | 2           |
| Acer                 | Aspire Z5610                                     | 2009 | 8       | 46      | 2           |
| Apple                | iMac16,2                                         | 2019 | 7       | 39      | 2           |
| Hewlett-Packard      | All-in-One 24-df0xxx                             | 2020 | 6       | 50      | 2           |
| Dell                 | XPS 2720                                         | 2018 | 4       | 39      | 2           |
| Hewlett-Packard      | All-in-One 24-e0XX                               | 2017 | 4       | 39      | 2           |
| Apple                | iMacPro1,1                                       | 2020 | 2       | 84      | 2           |
| Dell                 | Inspiron 5490 AIO                                | 2020 | 2       | 37      | 2           |
| Lenovo               | A740 F0AM                                        | 2014 | 2       | 34      | 2           |
| Apple                | iMac12,2                                         | 2011 | 41      | 47      | 3           |
| Apple                | iMac11,2                                         | 2010 | 18      | 48      | 3           |
| Hewlett-Packard      | 200 G3 AiO                                       | 2018 | 3       | 30      | 3           |
| Dell                 | Inspiron 7790 AIO                                | 2019 | 2       | 31      | 3           |
| Acidanthera          | iMacPro1,1                                       | 2020 | 7       | 129     | 4           |
| Dell                 | OptiPlex 7480 AIO                                | 2020 | 2       | 30      | 5           |
| Apple                | iMac14,2                                         | 2017 | 19      | 37      | 6           |
| Apple                | iMac12,1                                         | 2011 | 45      | 48      | 9           |
| Hewlett-Packard      | EliteOne 800 G1 AiO                              | 2013 | 6       | 40      | 9           |

By Vendor
---------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Samples | Devices | Non-working |
|----------------------|---------|---------|-------------|
| Apple                | 497     | 40      | 1           |
| Hewlett-Packard      | 213     | 34      | 1           |
| Lenovo               | 183     | 35      | 1           |
| Dell                 | 148     | 33      | 1           |
| Acer                 | 112     | 33      | 1           |
| ASUSTek Computer     | 68      | 33      | 1           |
| MSI                  | 31      | 34      | 1           |
| Sony                 | 28      | 38      | 1           |
| LG Electronics       | 22      | 30      | 1           |
| Samsung Electronics  | 16      | 33      | 0           |
| Acidanthera          | 16      | 52      | 1           |
| Packard Bell         | 12      | 36      | 1           |
| Positivo             | 8       | 32      | 0           |
| BESSTAR Tech         | 7       | 29      | 0           |
| Gateway              | 6       | 32      | 1           |
| Pegatron             | 6       | 34      | 1           |
| Toshiba              | 4       | 32      | 1           |
| Wortmann AG          | 3       | 35      | 0           |
| eMachines            | 3       | 31      | 0           |
| Medion               | 3       | 30      | 0           |
| MiTAC                | 3       | 29      | 0           |
| 3NOD                 | 3       | 35      | 1           |
| NEC Computers        | 3       | 34      | 1           |
| CSL-Computer         | 2       | 46      | 0           |
| Itautec              | 2       | 40      | 0           |
| T-Platforms          | 2       | 35      | 0           |
| iRU                  | 2       | 34      | 0           |
| VIZIO                | 2       | 34      | 0           |
| ICP / iEi            | 2       | 30      | 0           |
| ECS                  | 2       | 27      | 0           |
| Gigabyte Technology  | 2       | 30      | 1           |
| AIO                  | 2       | 27      | 1           |

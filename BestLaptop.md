Best Linux Laptop
=================

This is a list of laptops with best Linux-compatibility in the terms of
maximal number of Linux-compatible devices on board and maximal overall
popularity of a model.

See also [Best Desktop](/BestDesktop.md), [Best All In One](/BestAllInOne.md),
[Best Mini PC](/BestMiniPc.md) and [Best Convertible](/BestConvertible.md).

Everyone can contribute to this report by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Contents
--------

1. [ By Model ](#by-model)
2. [ By Vendor ](#by-vendor)

By Model
--------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Model                                            | Year | Samples | Devices | Non-working |
|----------------------|--------------------------------------------------|------|---------|---------|-------------|
| Dell                 | XPS 13 9370                                      | 2018 | 104     | 55      | 0           |
| Acer                 | Aspire A315-53                                   | 2018 | 73      | 34      | 0           |
| Hewlett-Packard      | G42                                              | 2010 | 53      | 41      | 0           |
| Google               | Enguarde                                         | 2014 | 51      | 26      | 0           |
| ASUSTek Computer     | X101CH                                           | 2012 | 48      | 32      | 0           |
| Acer                 | AO722                                            | 2011 | 46      | 40      | 0           |
| Hewlett-Packard      | 15 Notebook PC                                   | 2015 | 41      | 41      | 0           |
| Lenovo               | G560 20042                                       | 2009 | 39      | 45      | 0           |
| Acer                 | Aspire A315-51                                   | 2017 | 37      | 34      | 0           |
| Lenovo               | Z50-75 80EC                                      | 2014 | 34      | 44      | 0           |
| Acer                 | Aspire 5920G                                     | 2007 | 31      | 43      | 0           |
| Acer                 | Aspire A315-31                                   | 2017 | 29      | 32      | 0           |
| System76             | Lemur Pro                                        | 2020 | 28      | 55      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop X505ZA_X505ZA               | 2018 | 28      | 43      | 0           |
| eMachines            | E725                                             | 2009 | 28      | 38      | 0           |
| Dell                 | Inspiron 13-5378                                 | 2016 | 28      | 34      | 0           |
| Acer                 | Aspire E1-572                                    | 2013 | 28      | 34      | 0           |
| Acer                 | Aspire 5250                                      | 2011 | 27      | 41      | 0           |
| Lenovo               | IdeaPad 330-15IGM 81D1                           | 2018 | 27      | 32      | 0           |
| Toshiba              | Satellite L750                                   | 2011 | 26      | 37      | 0           |
| Acer                 | Aspire E1-572G                                   | 2013 | 26      | 36      | 0           |
| Acer                 | Extensa 2540                                     | 2016 | 26      | 35      | 0           |
| Packard Bell         | DOT S                                            | 2011 | 25      | 36      | 0           |
| Dell                 | Vostro 3480                                      | 2018 | 24      | 41      | 0           |
| Toshiba              | Satellite L655                                   | 2010 | 24      | 41      | 0           |
| Samsung Electronics  | N150/N210/N220                                   | 2010 | 24      | 34      | 0           |
| ASUSTek Computer     | 1001PX                                           | 2010 | 24      | 33      | 0           |
| eMachines            | eM350                                            | 2010 | 24      | 32      | 0           |
| ASUSTek Computer     | X51RL                                            | 2007 | 23      | 39      | 0           |
| Toshiba              | Satellite L855                                   | 2012 | 23      | 38      | 0           |
| Lenovo               | IdeaPad 330S-14IKB 81F4                          | 2018 | 22      | 37      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X507UAR                  | 2018 | 22      | 37      | 0           |
| Acer                 | Aspire one 1-431                                 | 2015 | 22      | 32      | 0           |
| Framework            | Laptop                                           | 2021 | 21      | 62      | 0           |
| Acer                 | Aspire A315-21G                                  | 2017 | 21      | 47      | 0           |
| Acer                 | Aspire E5-553G                                   | 2016 | 21      | 44      | 0           |
| ASUSTek Computer     | 1215B                                            | 2011 | 21      | 42      | 0           |
| Samsung Electronics  | NC110P/NC108P/NC111P                             | 2012 | 21      | 32      | 0           |
| Acer                 | AO751h                                           | 2009 | 21      | 28      | 0           |
| Lenovo               | ThinkPad S1 Yoga                                 | 2014 | 20      | 43      | 0           |
| Dell                 | Inspiron 1564                                    | 2009 | 20      | 43      | 0           |
| Lenovo               | IdeaPad 300-15ISK 80Q7                           | 2015 | 20      | 35      | 0           |
| Lenovo               | ThinkPad Edge E330                               | 2012 | 19      | 40      | 0           |
| Acer                 | Aspire 5733                                      | 2011 | 19      | 40      | 0           |
| ASUSTek Computer     | X550EA                                           | 2013 | 19      | 38      | 0           |
| Acer                 | Aspire 5715Z                                     | 2007 | 19      | 38      | 0           |
| Acer                 | Aspire A315-54K                                  | 2019 | 19      | 35      | 0           |
| Dell                 | Latitude E5400                                   | 2008 | 18      | 53      | 0           |
| Samsung Electronics  | RV415/RV515                                      | 2011 | 18      | 45      | 0           |
| Dell                 | Inspiron 5555                                    | 2015 | 18      | 43      | 0           |
| Dell                 | Inspiron N5040                                   | 2011 | 18      | 42      | 0           |
| Hewlett-Packard      | 1000                                             | 2012 | 18      | 41      | 0           |
| Dell                 | Inspiron 5370                                    | 2018 | 18      | 38      | 0           |
| ASUSTek Computer     | 1025C                                            | 2012 | 18      | 30      | 0           |
| Dell                 | XPS 13 9305                                      | 2021 | 17      | 62      | 0           |
| Toshiba              | Satellite A505                                   | 2009 | 17      | 57      | 0           |
| Dell                 | Inspiron M5110                                   | 2011 | 17      | 51      | 0           |
| Dell                 | Inspiron 5565                                    | 2016 | 17      | 46      | 0           |
| Acer                 | Aspire V3-551G                                   | 2012 | 17      | 46      | 0           |
| Samsung Electronics  | R425D/R525D                                      | 2010 | 17      | 41      | 0           |
| Toshiba              | Satellite L650                                   | 2010 | 17      | 40      | 0           |
| Acer                 | Aspire 5551G                                     | 2010 | 17      | 39      | 0           |
| Toshiba              | Satellite L305                                   | 2008 | 17      | 38      | 0           |
| Hewlett-Packard      | Compaq 610                                       | 2009 | 17      | 37      | 0           |
| Intel                | Intel powered classmate PC                       | 2010 | 17      | 35      | 0           |
| Samsung Electronics  | RV420/RV520/RV720/E3530/S3530/E3420/E3520        | 2011 | 17      | 32      | 0           |
| Lenovo               | G570 4334                                        | 2011 | 17      | 32      | 0           |
| Dell                 | Inspiron ME051                                   | 2005 | 17      | 31      | 0           |
| ASUSTek Computer     | K52Je                                            | 2010 | 16      | 46      | 0           |
| Toshiba              | Satellite L755D                                  | 2011 | 16      | 42      | 0           |
| Hewlett-Packard      | Compaq 6730s                                     | 2008 | 16      | 41      | 0           |
| Packard Bell         | EasyNote TE69KB                                  | 2013 | 16      | 39      | 0           |
| Hewlett-Packard      | 250 G4 Notebook PC                               | 2015 | 16      | 36      | 0           |
| Dell                 | Latitude D520                                    | 2006 | 16      | 36      | 0           |
| Apple                | MacBook2,1                                       | 2007 | 16      | 35      | 0           |
| Acer                 | Aspire A315-33                                   | 2018 | 16      | 33      | 0           |
| ASUSTek Computer     | K55A                                             | 2012 | 16      | 33      | 0           |
| ASUSTek Computer     | UX305FA                                          | 2014 | 16      | 28      | 0           |
| Lenovo               | IdeaPad Y550 20017                               | 2009 | 15      | 45      | 0           |
| ASUSTek Computer     | X102BA                                           | 2013 | 15      | 40      | 0           |
| Samsung Electronics  | RV411/RV511/E3511/S3511/RV711                    | 2011 | 15      | 39      | 0           |
| Fujitsu              | LIFEBOOK A530                                    | 2010 | 15      | 39      | 0           |
| Acer                 | Aspire 5740                                      | 2009 | 15      | 39      | 0           |
| Positivo             | C14CR21                                          | 2012 | 15      | 38      | 0           |
| Samsung Electronics  | R530/R730/R540                                   | 2010 | 15      | 37      | 0           |
| Dell                 | Inspiron 5447                                    | 2015 | 15      | 36      | 0           |
| Packard Bell         | EasyNote LS11HR                                  | 2011 | 15      | 35      | 0           |
| ASUSTek Computer     | K54L                                             | 2011 | 15      | 34      | 0           |
| Hewlett-Packard      | Stream Laptop 11-y0XX                            | 2016 | 15      | 32      | 0           |
| Acer                 | Aspire ES1-711                                   | 2014 | 15      | 32      | 0           |
| Lenovo               | G400s VILG1                                      | 2013 | 15      | 32      | 0           |
| Acer                 | TravelMate B117-M                                | 2016 | 15      | 29      | 0           |
| ASUSTek Computer     | F5VL                                             | 2007 | 15      | 28      | 0           |
| ASUSTek Computer     | K53TA                                            | 2011 | 14      | 45      | 0           |
| ASUSTek Computer     | ROG Strix G531GT_G531GT                          | 2019 | 14      | 44      | 0           |
| Lenovo               | IdeaPad S145-15API 81UT                          | 2019 | 14      | 43      | 0           |
| Acer                 | Aspire 4820TG                                    | 2010 | 14      | 42      | 0           |
| Toshiba              | Satellite P755                                   | 2011 | 14      | 39      | 0           |
| Acer                 | Aspire 7551                                      | 2010 | 14      | 39      | 0           |
| Hewlett-Packard      | ProBook 4710s                                    | 2009 | 14      | 39      | 0           |
| Acer                 | Aspire F5-573                                    | 2016 | 14      | 37      | 0           |
| Dell                 | Inspiron 5759                                    | 2015 | 14      | 37      | 0           |
| eMachines            | eME728                                           | 2010 | 14      | 37      | 0           |
| Samsung Electronics  | 300E5K/300E5Q                                    | 2016 | 14      | 36      | 0           |
| Lenovo               | S10-3                                            | 2010 | 14      | 36      | 0           |
| Lenovo               | G500s 20245                                      | 2013 | 14      | 35      | 0           |
| ASUSTek Computer     | UX31A                                            | 2012 | 14      | 35      | 0           |
| Dell                 | Studio XPS 1640                                  | 2008 | 13      | 44      | 0           |
| ASUSTek Computer     | 1225B                                            | 2011 | 13      | 43      | 0           |
| Dell                 | Latitude 3410                                    | 2020 | 13      | 42      | 0           |
| Dell                 | Inspiron M5040                                   | 2011 | 13      | 42      | 0           |
| Apple                | MacBookPro5,4                                    | 2009 | 13      | 42      | 0           |
| Dell                 | Inspiron 3582                                    | 2019 | 13      | 41      | 0           |
| Acer                 | Aspire A515-41G                                  | 2017 | 13      | 41      | 0           |
| Acer                 | Aspire 3820                                      | 2010 | 13      | 41      | 0           |
| Acer                 | Aspire 4810T                                     | 2009 | 13      | 41      | 0           |
| Dell                 | Inspiron 1750                                    | 2009 | 13      | 40      | 0           |
| Toshiba              | Satellite C655D                                  | 2011 | 13      | 38      | 0           |
| Acer                 | Aspire 5552G                                     | 2010 | 13      | 38      | 0           |
| Dell                 | Inspiron 14-3467                                 | 2017 | 13      | 37      | 0           |
| Hewlett-Packard      | 255 G2                                           | 2013 | 13      | 37      | 0           |
| Dell                 | Inspiron 5720                                    | 2012 | 13      | 37      | 0           |
| Acer                 | Aspire 4750                                      | 2011 | 13      | 37      | 0           |
| ASUSTek Computer     | K54LY                                            | 2011 | 13      | 37      | 0           |
| Samsung Electronics  | R520/R522/R620                                   | 2009 | 13      | 37      | 0           |
| Acer                 | Extensa 5635Z                                    | 2009 | 13      | 37      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540UB                   | 2018 | 13      | 36      | 0           |
| Dell                 | Inspiron N4110                                   | 2011 | 13      | 36      | 0           |
| Lenovo               | IdeaPad 100S-14IBR 80R9                          | 2015 | 13      | 35      | 0           |
| Lenovo               | G50-80 80L0                                      | 2015 | 13      | 35      | 0           |
| Samsung Electronics  | 530U3BI/530U4BI/530U4BH                          | 2012 | 13      | 35      | 0           |
| Lenovo               | IdeaPad 310-14ISK 80UG                           | 2016 | 13      | 34      | 0           |
| Acer                 | Aspire ES1-132                                   | 2016 | 13      | 34      | 0           |
| ASUSTek Computer     | E402NA                                           | 2017 | 13      | 33      | 0           |
| Lenovo               | IdeaPad S110 20126                               | 2012 | 13      | 33      | 0           |
| Fujitsu              | LIFEBOOK AH531                                   | 2011 | 13      | 33      | 0           |
| Samsung Electronics  | 300E5EV/300E4EV/270E5EV/270E4EV/2470EV           | 2013 | 13      | 32      | 0           |
| Samsung Electronics  | N100SP                                           | 2012 | 13      | 32      | 0           |
| ASUSTek Computer     | 1001PXD                                          | 2010 | 13      | 31      | 0           |
| Toshiba              | Satellite A500                                   | 2009 | 12      | 47      | 0           |
| ECS                  | SF20PA2                                          | 2017 | 12      | 46      | 0           |
| ASUSTek Computer     | TUF Gaming FX505DV_FX505DV                       | 2019 | 12      | 45      | 0           |
| Lenovo               | G585 20137                                       | 2012 | 12      | 45      | 0           |
| Lenovo               | V110-15AST 80TD                                  | 2017 | 12      | 43      | 0           |
| Acer                 | Aspire 7520                                      | 2007 | 12      | 43      | 0           |
| Lenovo               | IdeaPad 310-15ISK 80UH                           | 2016 | 12      | 42      | 0           |
| Hewlett-Packard      | 255 G5 Notebook PC                               | 2016 | 12      | 40      | 0           |
| ASUSTek Computer     | K50ID                                            | 2009 | 12      | 40      | 0           |
| Lenovo               | IdeaPad S340-14IIL 81VV                          | 2019 | 12      | 39      | 0           |
| Lenovo               | IdeaPad 120S-11IAP 81A4                          | 2017 | 12      | 39      | 0           |
| Hewlett-Packard      | Presario CQ61                                    | 2009 | 12      | 39      | 0           |
| Dell                 | Inspiron 5379                                    | 2017 | 12      | 37      | 0           |
| Acer                 | Aspire E5-576                                    | 2017 | 12      | 37      | 0           |
| ASUSTek Computer     | 1015BX                                           | 2011 | 12      | 37      | 0           |
| Google               | Celes                                            | 2017 | 12      | 36      | 0           |
| Lenovo               | IdeaPad S10-2 20027                              | 2009 | 12      | 36      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X507UF                   | 2018 | 12      | 35      | 0           |
| Lenovo               | ThinkPad 11e                                     | 2014 | 12      | 35      | 0           |
| Lenovo               | G40-70 80GA                                      | 2014 | 12      | 33      | 0           |
| Acer                 | Aspire ES1-511                                   | 2014 | 12      | 33      | 0           |
| Dell                 | Latitude 3440                                    | 2013 | 12      | 33      | 0           |
| ASUSTek Computer     | 1015PN                                           | 2010 | 12      | 33      | 0           |
| Dell                 | XPS 13 9333                                      | 2013 | 12      | 32      | 0           |
| Acer                 | E1-510                                           | 2013 | 12      | 30      | 0           |
| Acer                 | Aspire SW5-012                                   | 2014 | 12      | 24      | 0           |
| Dell                 | Inspiron 7375                                    | 2018 | 11      | 51      | 0           |
| Timi                 | A35S                                             | 2021 | 11      | 47      | 0           |
| ASUSTek Computer     | K52Jr                                            | 2009 | 11      | 47      | 0           |
| Acer                 | Aspire A315-41G                                  | 2019 | 11      | 46      | 0           |
| Samsung Electronics  | 305V4A/305V5A                                    | 2011 | 11      | 46      | 0           |
| ASUSTek Computer     | K53TK                                            | 2011 | 11      | 45      | 0           |
| Lenovo               | G460 20041                                       | 2010 | 11      | 45      | 0           |
| Acer                 | Aspire V5-552G                                   | 2013 | 11      | 43      | 0           |
| Dell                 | Inspiron N5030                                   | 2010 | 11      | 43      | 0           |
| Samsung Electronics  | 905S3G/906S3G/915S3G/9305SG                      | 2014 | 11      | 41      | 0           |
| ASUSTek Computer     | G74Sx                                            | 2011 | 11      | 41      | 0           |
| Dell                 | Inspiron 3584                                    | 2019 | 11      | 40      | 0           |
| Acer                 | Aspire V5-121                                    | 2012 | 11      | 40      | 0           |
| Packard Bell         | EasyNote TJ65                                    | 2009 | 11      | 40      | 0           |
| Acer                 | Aspire E1-421                                    | 2012 | 11      | 39      | 0           |
| ASUSTek Computer     | K53SK                                            | 2011 | 11      | 38      | 0           |
| Toshiba              | Satellite Pro C660                               | 2010 | 11      | 38      | 0           |
| Toshiba              | Satellite L850                                   | 2012 | 11      | 37      | 0           |
| Hewlett-Packard      | Compaq Presario CQ71                             | 2009 | 11      | 37      | 0           |
| ASUSTek Computer     | F5R                                              | 2007 | 11      | 37      | 0           |
| Lenovo               | Z51-70 80K6                                      | 2015 | 11      | 36      | 0           |
| Acer                 | Aspire V3-371                                    | 2014 | 11      | 36      | 0           |
| Acer                 | Aspire M5-481T                                   | 2012 | 11      | 34      | 0           |
| ASUSTek Computer     | X751MA                                           | 2014 | 11      | 33      | 0           |
| Packard Bell         | EasyNote TS44HR                                  | 2011 | 11      | 33      | 0           |
| Lenovo               | G40-80 80JE                                      | 2015 | 11      | 32      | 0           |
| Positivo             | S14BW01                                          | 2016 | 11      | 31      | 0           |
| Lenovo               | V110-15IAP 80TG                                  | 2016 | 11      | 30      | 0           |
| ASUSTek Computer     | 1011CX                                           | 2012 | 11      | 30      | 0           |
| ASUSTek Computer     | X540SAA                                          | 2016 | 11      | 29      | 0           |
| Acer                 | Swift SF314-43                                   | 2021 | 10      | 49      | 0           |
| MSI                  | GF63 Thin 9RCX                                   | 2019 | 10      | 43      | 0           |
| Alienware            | 14                                               | 2013 | 10      | 43      | 0           |
| Philco               | 14I                                              | 2012 | 10      | 43      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X430FA_S430FA                | 2018 | 10      | 41      | 0           |
| ASUSTek Computer     | X555QG                                           | 2017 | 10      | 41      | 0           |
| Samsung Electronics  | 275E4E/275E5E                                    | 2013 | 10      | 41      | 0           |
| Toshiba              | NB550D                                           | 2010 | 10      | 41      | 0           |
| ASUSTek Computer     | X542UN                                           | 2017 | 10      | 40      | 0           |
| Toshiba              | Satellite C55D-B                                 | 2014 | 10      | 40      | 0           |
| Samsung Electronics  | R430/R480/R440                                   | 2010 | 10      | 40      | 0           |
| Lenovo               | IdeaPad 500-15ISK 80NT                           | 2015 | 10      | 39      | 0           |
| Positivo             | C14CR01                                          | 2012 | 10      | 39      | 0           |
| Acer                 | Aspire 5820TG                                    | 2010 | 10      | 39      | 0           |
| ASUSTek Computer     | X51L                                             | 2007 | 10      | 39      | 0           |
| Lenovo               | ThinkPad Twist                                   | 2012 | 10      | 38      | 0           |
| Acer                 | Aspire V5-123                                    | 2013 | 10      | 37      | 0           |
| ASUSTek Computer     | K55VM                                            | 2012 | 10      | 37      | 0           |
| Acer                 | Extensa 5235                                     | 2009 | 10      | 37      | 0           |
| eMachines            | E510                                             | 2008 | 10      | 37      | 0           |
| Positivo             | SW6H                                             | 2011 | 10      | 36      | 0           |
| Hewlett-Packard      | Compaq 6820s                                     | 2007 | 10      | 36      | 0           |
| ASUSTek Computer     | 1201N                                            | 2010 | 10      | 35      | 0           |
| ASUSTek Computer     | 1000H                                            | 2008 | 10      | 35      | 0           |
| Acer                 | Aspire A114-32                                   | 2018 | 10      | 34      | 0           |
| Acer                 | Aspire A517-51                                   | 2017 | 10      | 34      | 0           |
| Fujitsu              | LIFEBOOK A555                                    | 2015 | 10      | 34      | 0           |
| Acer                 | Aspire E3-112                                    | 2014 | 10      | 34      | 0           |
| Fujitsu              | LIFEBOOK A512                                    | 2013 | 10      | 33      | 0           |
| Samsung Electronics  | N102SP/N100SP/N101SP                             | 2012 | 10      | 33      | 0           |
| Apple                | MacBookAir5,1                                    | 2015 | 10      | 32      | 0           |
| Lenovo               | Yoga 2 13 20344                                  | 2014 | 10      | 32      | 0           |
| Dell                 | Latitude 3340                                    | 2014 | 10      | 32      | 0           |
| Acer                 | Aspire E1-471                                    | 2012 | 10      | 32      | 0           |
| Lenovo               | IdeaPad U310                                     | 2012 | 10      | 31      | 0           |
| ASUSTek Computer     | 1015PE                                           | 2010 | 10      | 31      | 0           |
| ASUSTek Computer     | UX305CA                                          | 2015 | 10      | 30      | 0           |
| Acer                 | Aspire ES1-311                                   | 2014 | 10      | 29      | 0           |
| Google               | Cyan                                             | 2019 | 10      | 27      | 0           |
| Alienware            | 15 R3                                            | 2016 | 9       | 53      | 0           |
| Acer                 | Aspire 7740                                      | 2009 | 9       | 53      | 0           |
| Dell                 | Studio 1537                                      | 2008 | 9       | 47      | 0           |
| ASUSTek Computer     | ZenBook UX325EA_UX325EA                          | 2020 | 9       | 46      | 0           |
| Dell                 | Inspiron 5402                                    | 2021 | 9       | 44      | 0           |
| GPD                  | MicroPC                                          | 2019 | 9       | 44      | 0           |
| Dell                 | Inspiron 3480                                    | 2019 | 9       | 43      | 0           |
| Dell                 | Vostro 3580                                      | 2018 | 9       | 42      | 0           |
| Toshiba              | Satellite C55D-A                                 | 2013 | 9       | 42      | 0           |
| Samsung Electronics  | RV415                                            | 2012 | 9       | 41      | 0           |
| ASUSTek Computer     | K55N                                             | 2012 | 9       | 41      | 0           |
| Acer                 | Aspire 5920                                      | 2007 | 9       | 41      | 0           |
| Dell                 | Inspiron 15-3565                                 | 2017 | 9       | 40      | 0           |
| Toshiba              | Satellite C50D-B                                 | 2014 | 9       | 40      | 0           |
| ASUSTek Computer     | F5Z                                              | 2008 | 9       | 40      | 0           |
| Notebook             | NL40_50CU                                        | 2019 | 9       | 39      | 0           |
| Hewlett-Packard      | 255 G5                                           | 2016 | 9       | 39      | 0           |
| Acer                 | Aspire 5553G                                     | 2010 | 9       | 39      | 0           |
| Samsung Electronics  | R510/P510                                        | 2008 | 9       | 39      | 0           |
| ASUSTek Computer     | G751JT                                           | 2014 | 9       | 38      | 0           |
| Hewlett-Packard      | ENVY Sleekbook 6 PC                              | 2012 | 9       | 38      | 0           |
| Dell                 | Inspiron 5421                                    | 2012 | 9       | 38      | 0           |
| eMachines            | E520                                             | 2008 | 9       | 38      | 0           |
| Hewlett-Packard      | Compaq 6735s                                     | 2008 | 9       | 38      | 0           |
| Dell                 | Latitude D530                                    | 2008 | 9       | 38      | 0           |
| MSI                  | GL63 8RD                                         | 2018 | 9       | 37      | 0           |
| Acer                 | Aspire M5-481PT                                  | 2012 | 9       | 37      | 0           |
| ASUSTek Computer     | K46CA                                            | 2012 | 9       | 37      | 0           |
| Packard Bell         | EasyNote TM86                                    | 2010 | 9       | 37      | 0           |
| Acer                 | Aspire 5736Z                                     | 2010 | 9       | 37      | 0           |
| ASUSTek Computer     | UL30A                                            | 2009 | 9       | 37      | 0           |
| ASUSTek Computer     | N61Vg                                            | 2009 | 9       | 37      | 0           |
| Toshiba              | Satellite L350                                   | 2008 | 9       | 37      | 0           |
| Fujitsu Siemens      | AMILO Li 2727                                    | 2007 | 9       | 37      | 0           |
| Dell                 | Inspiron 3437                                    | 2013 | 9       | 36      | 0           |
| Hewlett-Packard      | ProBook 430 G7                                   | 2020 | 9       | 35      | 0           |
| Dell                 | Venue 11 Pro 7130 vPro                           | 2015 | 9       | 35      | 0           |
| Acer                 | Aspire V5-531                                    | 2012 | 9       | 35      | 0           |
| Hewlett-Packard      | 250 G2                                           | 2014 | 9       | 34      | 0           |
| MSI                  | MS-16Y1                                          | 2011 | 9       | 34      | 0           |
| DEXP                 | NH4BT58                                          | 2014 | 9       | 33      | 0           |
| Medion               | E6234                                            | 2012 | 9       | 33      | 0           |
| ASUSTek Computer     | X401A1                                           | 2012 | 9       | 33      | 0           |
| ASUSTek Computer     | E502SA                                           | 2015 | 9       | 32      | 0           |
| Lenovo               | IdeaPad Flex 15 20309                            | 2013 | 9       | 32      | 0           |
| Samsung Electronics  | 300E5EV/300E4EV/270E5EV/270E4EV                  | 2013 | 9       | 31      | 0           |
| Positivo             | C14CU51                                          | 2013 | 9       | 31      | 0           |
| Hewlett-Packard      | Stream Laptop 14-cb1xxx                          | 2019 | 9       | 30      | 0           |
| ASUSTek Computer     | Z550SA                                           | 2016 | 9       | 30      | 0           |
| ASUSTek Computer     | 1101HA                                           | 2009 | 9       | 27      | 0           |
| Google               | Banon                                            | 2016 | 9       | 25      | 0           |
| Lenovo               | IdeaPad 100S-11IBY 80R2                          | 2015 | 9       | 25      | 0           |
| Hewlett-Packard      | Pavilion x2 Detachable PC 10                     | 2014 | 9       | 22      | 0           |
| ASUSTek Computer     | N61Jq                                            | 2010 | 8       | 58      | 0           |
| Hewlett-Packard      | ZBook Power G7 Mobile Workstation                | 2020 | 8       | 51      | 0           |
| Dell                 | G7 7700                                          | 2020 | 8       | 51      | 0           |
| ASUSTek Computer     | GL553VW                                          | 2016 | 8       | 51      | 0           |
| MSI                  | GP73 Leopard 8RE                                 | 2018 | 8       | 47      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FB_X509FB                | 2019 | 8       | 43      | 0           |
| Dell                 | Inspiron 15-5578                                 | 2018 | 8       | 43      | 0           |
| ASUSTek Computer     | UX331UA                                          | 2017 | 8       | 43      | 0           |
| Star Labs            | LabTop                                           | 2019 | 8       | 42      | 0           |
| ASUSTek Computer     | X551CA                                           | 2013 | 8       | 42      | 0           |
| Lenovo               | IdeaPad S340-15IIL 81VW                          | 2019 | 8       | 41      | 0           |
| Acer                 | Aspire V5-572G                                   | 2013 | 8       | 41      | 0           |
| Dell                 | System Inspiron N7110                            | 2011 | 8       | 41      | 0           |
| Itautec              | Infoway w7430                                    | 2010 | 8       | 41      | 0           |
| Acer                 | Aspire A315-22                                   | 2019 | 8       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X530FA_S530FA                | 2018 | 8       | 40      | 0           |
| ASUSTek Computer     | Z450LA                                           | 2015 | 8       | 40      | 0           |
| Lenovo               | IdeaPad G485 QAWGE                               | 2013 | 8       | 40      | 0           |
| ASUSTek Computer     | S500CA                                           | 2012 | 8       | 40      | 0           |
| Dell                 | XPS 15Z                                          | 2011 | 8       | 40      | 0           |
| Toshiba              | Satellite L300D                                  | 2008 | 8       | 40      | 0           |
| Dell                 | Latitude 3510                                    | 2020 | 8       | 39      | 0           |
| ASUSTek Computer     | X705UDR                                          | 2017 | 8       | 39      | 0           |
| Packard Bell         | EasyNote LE69KB                                  | 2013 | 8       | 39      | 0           |
| Samsung Electronics  | R425/R525                                        | 2010 | 8       | 39      | 0           |
| MSI                  | CR610                                            | 2009 | 8       | 39      | 0           |
| Fujitsu Siemens      | ESPRIMO Mobile V5505                             | 2007 | 8       | 39      | 0           |
| Dell                 | Inspiron 3493                                    | 2019 | 8       | 38      | 0           |
| ASUSTek Computer     | VivoBook 12_ASUS Laptop E203MAS_E203MA           | 2018 | 8       | 38      | 0           |
| Dell                 | Latitude 3330                                    | 2013 | 8       | 38      | 0           |
| Acer                 | Aspire V5-122P                                   | 2013 | 8       | 38      | 0           |
| Acer                 | Aspire One 753                                   | 2010 | 8       | 38      | 0           |
| ASUSTek Computer     | X51R                                             | 2007 | 8       | 38      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X560UD                   | 2018 | 8       | 37      | 0           |
| ASUSTek Computer     | X541UA                                           | 2016 | 8       | 37      | 0           |
| ASUSTek Computer     | UX410UAK                                         | 2016 | 8       | 37      | 0           |
| Lenovo               | ThinkPad Helix                                   | 2014 | 8       | 37      | 0           |
| Acer                 | TravelMate 5744                                  | 2011 | 8       | 37      | 0           |
| Acer                 | Aspire One 522                                   | 2011 | 8       | 37      | 0           |
| Acer                 | Aspire 5251                                      | 2010 | 8       | 37      | 0           |
| Acer                 | Aspire 5536                                      | 2009 | 8       | 37      | 0           |
| Acer                 | Nitro AN515-31                                   | 2017 | 8       | 36      | 0           |
| Packard Bell         | EasyNote TE69HW                                  | 2013 | 8       | 36      | 0           |
| Acer                 | Aspire E1-570                                    | 2013 | 8       | 36      | 0           |
| Hewlett-Packard      | G56                                              | 2010 | 8       | 36      | 0           |
| Dell                 | Inspiron 1110                                    | 2009 | 8       | 36      | 0           |
| Acer                 | Aspire A114-31                                   | 2017 | 8       | 35      | 0           |
| Lenovo               | B50-70 80EU                                      | 2014 | 8       | 35      | 0           |
| Acer                 | Aspire E1-731                                    | 2013 | 8       | 35      | 0           |
| ASUSTek Computer     | K45A                                             | 2012 | 8       | 35      | 0           |
| Samsung Electronics  | R19/R20/R21                                      | 2007 | 8       | 35      | 0           |
| Dell                 | Inspiron 13-5368                                 | 2018 | 8       | 34      | 0           |
| Lenovo               | G50-80 80R0                                      | 2015 | 8       | 34      | 0           |
| Lenovo               | U310                                             | 2013 | 8       | 34      | 0           |
| Acer                 | Aspire E1-530                                    | 2013 | 8       | 34      | 0           |
| ASUSTek Computer     | S301LA                                           | 2013 | 8       | 34      | 0           |
| ASUSTek Computer     | UX32A                                            | 2012 | 8       | 34      | 0           |
| MSI                  | U90/U100                                         | 2009 | 8       | 34      | 0           |
| Dell                 | Inspiron 1011                                    | 2009 | 8       | 34      | 0           |
| Acer                 | Aspire 1810TZ                                    | 2009 | 8       | 34      | 0           |
| Lenovo               | IdeaPad 330-15IKB 81FD                           | 2018 | 8       | 33      | 0           |
| Lenovo               | Yoga 300-11IBY 80M0                              | 2015 | 8       | 33      | 0           |
| ASUSTek Computer     | K84L                                             | 2011 | 8       | 33      | 0           |
| Hewlett-Packard      | Compaq Mini 110c-1100                            | 2009 | 8       | 33      | 0           |
| Dell                 | Inspiron 3420                                    | 2012 | 8       | 32      | 0           |
| Acer                 | V5-131                                           | 2012 | 8       | 32      | 0           |
| ASUSTek Computer     | X501A1                                           | 2012 | 8       | 32      | 0           |
| Samsung Electronics  | R40/R41                                          | 2006 | 8       | 32      | 0           |
| Lenovo               | IdeaPad Yoga 11S 20246                           | 2013 | 8       | 31      | 0           |
| Timi                 | TM1607                                           | 2017 | 8       | 30      | 0           |
| Packard Bell         | EasyNote TE69BM                                  | 2013 | 8       | 30      | 0           |
| ASUSTek Computer     | E205SA                                           | 2015 | 8       | 28      | 0           |
| ASUSTek Computer     | 1201HA                                           | 2009 | 8       | 27      | 0           |
| Google               | Kip                                              | 2019 | 8       | 26      | 0           |
| Dell                 | XPS L501X                                        | 2011 | 7       | 59      | 0           |
| Lenovo               | Legion 7 15IMH05 81YT                            | 2020 | 7       | 54      | 0           |
| Hewlett-Packard      | EliteBook 850 G8 Notebook PC                     | 2020 | 7       | 48      | 0           |
| Lenovo               | ThinkPad E555                                    | 2015 | 7       | 46      | 0           |
| Lenovo               | ThinkPad E485                                    | 2018 | 7       | 45      | 0           |
| Acer                 | Aspire V5-591G                                   | 2015 | 7       | 45      | 0           |
| Acer                 | Nitro AN517-52                                   | 2020 | 7       | 44      | 0           |
| Lenovo               | V130-15IGM 81HL                                  | 2018 | 7       | 44      | 0           |
| Alienware            | 18                                               | 2013 | 7       | 44      | 0           |
| ASUSTek Computer     | N53Ta                                            | 2011 | 7       | 44      | 0           |
| Lenovo               | ThinkPad SL410                                   | 2010 | 7       | 44      | 0           |
| ASUSTek Computer     | K42F                                             | 2010 | 7       | 44      | 0           |
| Fujitsu Siemens      | ESPRIMO Mobile V6535                             | 2008 | 7       | 44      | 0           |
| ASUSTek Computer     | ZenBook UX333FA_UX333FA                          | 2019 | 7       | 43      | 0           |
| ASUSTek Computer     | X555DG                                           | 2015 | 7       | 43      | 0           |
| Samsung Electronics  | 535U4C                                           | 2012 | 7       | 43      | 0           |
| Samsung Electronics  | 535U3C                                           | 2012 | 7       | 42      | 0           |
| MSI                  | U270 series                                      | 2011 | 7       | 42      | 0           |
| GPD                  | P2 MAX                                           | 2019 | 7       | 41      | 0           |
| Complet              | MY8312                                           | 2018 | 7       | 41      | 0           |
| ASUSTek Computer     | GL552VX                                          | 2016 | 7       | 41      | 0           |
| Lenovo               | B550 20053                                       | 2010 | 7       | 41      | 0           |
| ASUSTek Computer     | ZenBook S UX391UA                                | 2019 | 7       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X403FA_X403FA                | 2019 | 7       | 40      | 0           |
| ASUSTek Computer     | X510UR                                           | 2017 | 7       | 40      | 0           |
| ASUSTek Computer     | X550EP                                           | 2013 | 7       | 40      | 0           |
| Clevo                | W55xEU                                           | 2012 | 7       | 40      | 0           |
| Samsung Electronics  | R580/R590                                        | 2010 | 7       | 40      | 0           |
| Acer                 | Aspire 5730                                      | 2008 | 7       | 40      | 0           |
| ASUSTek Computer     | X58L                                             | 2008 | 7       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E406MA_E406MA               | 2018 | 7       | 39      | 0           |
| ASUSTek Computer     | X505BA                                           | 2017 | 7       | 39      | 0           |
| Lenovo               | IdeaPad Y700-15ACZ 80NY                          | 2015 | 7       | 39      | 0           |
| ASUSTek Computer     | X555YI                                           | 2015 | 7       | 39      | 0           |
| Lenovo               | ThinkPad S1 Yoga 12                              | 2014 | 7       | 39      | 0           |
| eMachines            | eME732G                                          | 2010 | 7       | 39      | 0           |
| MSI                  | X370                                             | 2011 | 7       | 38      | 0           |
| Toshiba              | Satellite R630                                   | 2010 | 7       | 38      | 0           |
| ASUSTek Computer     | K72F                                             | 2009 | 7       | 38      | 0           |
| Hewlett-Packard      | Compaq Presario CQ70                             | 2008 | 7       | 38      | 0           |
| Fujitsu Siemens      | ESPRIMO Mobile D9500                             | 2007 | 7       | 38      | 0           |
| ASUSTek Computer     | A6JC                                             | 2006 | 7       | 38      | 0           |
| System76             | Lemur                                            | 2018 | 7       | 37      | 0           |
| ASUSTek Computer     | G55VW                                            | 2012 | 7       | 37      | 0           |
| Acer                 | Aspire V5-531G                                   | 2012 | 7       | 36      | 0           |
| ASUSTek Computer     | UX51VZA                                          | 2012 | 7       | 36      | 0           |
| ASUSTek Computer     | K73SD                                            | 2011 | 7       | 36      | 0           |
| eMachines            | eME732Z                                          | 2010 | 7       | 36      | 0           |
| Samsung Electronics  | R430/P430/R480                                   | 2010 | 7       | 36      | 0           |
| Acer                 | Aspire 5336                                      | 2010 | 7       | 36      | 0           |
| Toshiba              | Satellite L505D                                  | 2009 | 7       | 36      | 0           |
| Apple                | MacBookPro1,1                                    | 2006 | 7       | 36      | 0           |
| Lenovo               | IdeaPad 3 14IIL05 81WD                           | 2020 | 7       | 35      | 0           |
| Hewlett-Packard      | 245 G5 Notebook PC                               | 2016 | 7       | 35      | 0           |
| Dell                 | Latitude 3350                                    | 2016 | 7       | 35      | 0           |
| Positivo             | EC10IS1                                          | 2014 | 7       | 35      | 0           |
| Lenovo               | G480 20149                                       | 2012 | 7       | 35      | 0           |
| Fujitsu              | LIFEBOOK AH532                                   | 2012 | 7       | 34      | 0           |
| Gateway              | NV53A                                            | 2010 | 7       | 34      | 0           |
| ASUSTek Computer     | 901                                              | 2008 | 7       | 34      | 0           |
| Dell                 | Venue 11 Pro 7140                                | 2017 | 7       | 33      | 0           |
| ASUSTek Computer     | E203NA                                           | 2017 | 7       | 33      | 0           |
| Acer                 | Aspire ES1-131                                   | 2015 | 7       | 33      | 0           |
| Lenovo               | Flex 2-14 20404                                  | 2014 | 7       | 33      | 0           |
| ASUSTek Computer     | X455LAB                                          | 2014 | 7       | 33      | 0           |
| Samsung Electronics  | 530U4E/540U4E                                    | 2013 | 7       | 33      | 0           |
| Toshiba              | Satellite C855                                   | 2012 | 7       | 33      | 0           |
| ASUSTek Computer     | K40IN                                            | 2009 | 7       | 33      | 0           |
| Lenovo               | IdeaPad 110-17IKB 80VK                           | 2016 | 7       | 32      | 0           |
| Samsung Electronics  | N150P                                            | 2010 | 7       | 32      | 0           |
| ASUSTek Computer     | K70IC                                            | 2010 | 7       | 32      | 0           |
| ASUSTek Computer     | K61IC                                            | 2009 | 7       | 32      | 0           |
| ASUSTek Computer     | 1000HE                                           | 2009 | 7       | 32      | 0           |
| ASUSTek Computer     | X441NA                                           | 2017 | 7       | 31      | 0           |
| Fujitsu              | LIFEBOOK A514                                    | 2015 | 7       | 31      | 0           |
| Acer                 | Aspire ES1-731                                   | 2015 | 7       | 31      | 0           |
| ASUSTek Computer     | X55CR                                            | 2012 | 7       | 31      | 0           |
| eMachines            | eM355                                            | 2011 | 7       | 31      | 0           |
| ASUSTek Computer     | 1015P                                            | 2010 | 7       | 31      | 0           |
| Packard Bell         | EasyNote ENTF71BM                                | 2014 | 7       | 30      | 0           |
| Google               | Peppy                                            | 2014 | 7       | 29      | 0           |
| Acer                 | Aspire 5749Z                                     | 2011 | 7       | 28      | 0           |
| MSI                  | CX600                                            | 2009 | 7       | 28      | 0           |
| Lenovo               | IdeaPad Flex 10 20324                            | 2013 | 7       | 25      | 0           |
| Google               | Swanky                                           | 2014 | 7       | 24      | 0           |
| Notebook             | NV4XMB,ME,MZ                                     | 2020 | 6       | 55      | 0           |
| Acer                 | Predator PH517-61                                | 2018 | 6       | 54      | 0           |
| Google               | Akemi                                            | 2020 | 6       | 52      | 0           |
| Acer                 | Aspire 7745G                                     | 2010 | 6       | 50      | 0           |
| GPU Company          | GWTC116-2                                        | 2021 | 6       | 49      | 0           |
| Dell                 | Inspiron 7501                                    | 2020 | 6       | 48      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E410MA_E410MA               | 2020 | 6       | 48      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop X509UA                      | 2019 | 6       | 48      | 0           |
| Dell                 | Vostro 1014                                      | 2009 | 6       | 48      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X571LH_K571LH                | 2020 | 6       | 46      | 0           |
| MSI                  | GP63 Leopard 8RE                                 | 2018 | 6       | 46      | 0           |
| Acer                 | Swift SF114-33                                   | 2019 | 6       | 45      | 0           |
| Sony                 | VGN-FZ31Z                                        | 2007 | 6       | 45      | 0           |
| Acer                 | Aspire 2920                                      | 2007 | 6       | 45      | 0           |
| Hewlett-Packard      | OMEN by Laptop 15-dc1xxx                         | 2020 | 6       | 44      | 0           |
| ASUSTek Computer     | ZenBook UX450FDX_UX480FD                         | 2019 | 6       | 44      | 0           |
| ASUSTek Computer     | G751JY                                           | 2014 | 6       | 44      | 0           |
| ASUSTek Computer     | K42Jr                                            | 2010 | 6       | 44      | 0           |
| ASUSTek Computer     | X441BA                                           | 2018 | 6       | 43      | 0           |
| Lenovo               | IdeaPad Z585 20152                               | 2013 | 6       | 43      | 0           |
| Sony                 | VPCEA1S1E                                        | 2010 | 6       | 43      | 0           |
| ASUSTek Computer     | N53Jf                                            | 2010 | 6       | 43      | 0           |
| ASUSTek Computer     | K52JB                                            | 2010 | 6       | 43      | 0           |
| Toshiba              | Satellite A350                                   | 2008 | 6       | 43      | 0           |
| Dell                 | Inspiron 14 5410                                 | 2021 | 6       | 42      | 0           |
| MSI                  | Modern 15 A11M                                   | 2020 | 6       | 42      | 0           |
| ASUSTek Computer     | TUF GAMING FX504GE_FX80GE                        | 2019 | 6       | 42      | 0           |
| Hewlett-Packard      | OMEN by Laptop 15-ce0xx                          | 2017 | 6       | 42      | 0           |
| Lenovo               | ThinkPad Edge E545                               | 2013 | 6       | 42      | 0           |
| ASUSTek Computer     | K73BY                                            | 2011 | 6       | 42      | 0           |
| ASUSTek Computer     | N61Da                                            | 2010 | 6       | 42      | 0           |
| Acer                 | Aspire 7720Z                                     | 2007 | 6       | 42      | 0           |
| ASUSTek Computer     | GL502VMK                                         | 2016 | 6       | 41      | 0           |
| ASUSTek Computer     | K43SJ                                            | 2011 | 6       | 41      | 0           |
| Toshiba              | Satellite P200                                   | 2007 | 6       | 41      | 0           |
| ASUSTek Computer     | ZenBook UX425JA_UX425JA                          | 2020 | 6       | 40      | 0           |
| ASUSTek Computer     | X510UA                                           | 2017 | 6       | 40      | 0           |
| Samsung Electronics  | R540/SA41/E452                                   | 2010 | 6       | 40      | 0           |
| Gateway              | NV55C                                            | 2010 | 6       | 40      | 0           |
| Lenovo               | IdeaPad 3 14IML05 81WA                           | 2020 | 6       | 39      | 0           |
| HUAWEI               | NBLB-WAX9N                                       | 2020 | 6       | 39      | 0           |
| ASUSTek Computer     | TUF Gaming FX705GM_FX705GM                       | 2019 | 6       | 39      | 0           |
| Fujitsu              | LIFEBOOK A532                                    | 2012 | 6       | 39      | 0           |
| Acer                 | Aspire 5742Z                                     | 2010 | 6       | 39      | 0           |
| ASUSTek Computer     | X555DA                                           | 2015 | 6       | 38      | 0           |
| Lenovo               | G475 20080                                       | 2011 | 6       | 38      | 0           |
| Toshiba              | Satellite L650D                                  | 2010 | 6       | 38      | 0           |
| Samsung Electronics  | Q330                                             | 2010 | 6       | 38      | 0           |
| Dell                 | Inspiron M5030                                   | 2010 | 6       | 38      | 0           |
| Medion               | P6618                                            | 2009 | 6       | 38      | 0           |
| Acer                 | Aspire 7535                                      | 2009 | 6       | 38      | 0           |
| Toshiba              | Satellite L350D                                  | 2008 | 6       | 38      | 0           |
| Lenovo               | IdeaPad Y530                                     | 2008 | 6       | 38      | 0           |
| ASUSTek Computer     | X542URR                                          | 2017 | 6       | 37      | 0           |
| Dell                 | Vostro 3559                                      | 2016 | 6       | 37      | 0           |
| ASUSTek Computer     | X541UV                                           | 2016 | 6       | 37      | 0           |
| Toshiba              | Satellite C50D-A                                 | 2013 | 6       | 37      | 0           |
| Notebook             | W65_67SH                                         | 2013 | 6       | 37      | 0           |
| ASUSTek Computer     | K84C                                             | 2011 | 6       | 37      | 0           |
| eMachines            | eME642G                                          | 2010 | 6       | 37      | 0           |
| Samsung Electronics  | RV411/RV511/E3511/S3511                          | 2010 | 6       | 37      | 0           |
| Acer                 | TravelMate 5735Z                                 | 2010 | 6       | 37      | 0           |
| Acer                 | Aspire 4820T                                     | 2010 | 6       | 37      | 0           |
| Hewlett-Packard      | G71                                              | 2009 | 6       | 37      | 0           |
| Lenovo               | ThinkPad R61e                                    | 2007 | 6       | 37      | 0           |
| Dell                 | Inspiron MXC061                                  | 2006 | 6       | 37      | 0           |
| ASUSTek Computer     | K45VD                                            | 2012 | 6       | 36      | 0           |
| Dell                 | Inspiron 1018                                    | 2010 | 6       | 36      | 0           |
| Toshiba              | Satellite T110                                   | 2009 | 6       | 36      | 0           |
| Acer                 | Aspire 7540                                      | 2009 | 6       | 36      | 0           |
| ASUSTek Computer     | K70IO                                            | 2009 | 6       | 36      | 0           |
| Toshiba              | Satellite Pro L300                               | 2008 | 6       | 36      | 0           |
| Dell                 | Inspiron 5579                                    | 2017 | 6       | 35      | 0           |
| ASUSTek Computer     | X510UQR                                          | 2017 | 6       | 35      | 0           |
| Dell                 | Inspiron 5468                                    | 2016 | 6       | 35      | 0           |
| Acer                 | Aspire V3-372                                    | 2015 | 6       | 35      | 0           |
| Dell                 | Latitude 3540                                    | 2013 | 6       | 35      | 0           |
| Clevo                | W210CUQ                                          | 2012 | 6       | 35      | 0           |
| LG Electronics       | S460-G.BG31P1                                    | 2012 | 6       | 34      | 0           |
| Toshiba              | NB505                                            | 2011 | 6       | 34      | 0           |
| Acer                 | Aspire 4830T                                     | 2011 | 6       | 34      | 0           |
| Acer                 | Ferrari One 200                                  | 2009 | 6       | 34      | 0           |
| Apple                | MacBookAir2,1                                    | 2008 | 6       | 34      | 0           |
| ASUSTek Computer     | A6R                                              | 2006 | 6       | 34      | 0           |
| Lenovo               | N22 80S6                                         | 2016 | 6       | 33      | 0           |
| Lenovo               | B5400 80B6QB0                                    | 2013 | 6       | 33      | 0           |
| ASUSTek Computer     | X202EP                                           | 2012 | 6       | 33      | 0           |
| ASUSTek Computer     | 1225C                                            | 2012 | 6       | 33      | 0           |
| Intel                | SharkBay Platform                                | 2014 | 6       | 32      | 0           |
| Dell                 | Vostro 3446                                      | 2014 | 6       | 32      | 0           |
| Toshiba              | Satellite C50-A                                  | 2013 | 6       | 32      | 0           |
| Acer                 | AOHAPPY                                          | 2010 | 6       | 32      | 0           |
| Hewlett-Packard      | Mini 110-3800                                    | 2011 | 6       | 31      | 0           |
| Clevo                | W240HU/W250HUQ                                   | 2011 | 6       | 31      | 0           |
| ASUSTek Computer     | K73E                                             | 2011 | 6       | 31      | 0           |
| Dell                 | Latitude 120L                                    | 2006 | 6       | 31      | 0           |
| Lenovo               | IdeaPad 330-15IGM 81FN                           | 2018 | 6       | 30      | 0           |
| Lenovo               | B320-14IKB 81CC                                  | 2017 | 6       | 30      | 0           |
| Toshiba              | Satellite Pro R50-B                              | 2014 | 6       | 30      | 0           |
| Lenovo               | E10-30 20424                                     | 2014 | 6       | 30      | 0           |
| Toshiba              | NB510                                            | 2012 | 6       | 30      | 0           |
| Lenovo               | IdeaPad S145-15IGM 81WT                          | 2019 | 6       | 29      | 0           |
| Packard Bell         | EasyNote ENTG81BA                                | 2015 | 6       | 29      | 0           |
| Acer                 | Peppy                                            | 2014 | 6       | 29      | 0           |
| ASUSTek Computer     | X205TA                                           | 2014 | 6       | 25      | 0           |
| Google               | Gnawty                                           | 2017 | 6       | 22      | 0           |
| Dell                 | Studio XPS 1645                                  | 2010 | 5       | 62      | 0           |
| MSI                  | GT70 2OC/2OD                                     | 2013 | 5       | 61      | 0           |
| Razer                | Blade 15 Advanced Model (Early 2020) - RZ09-033  | 2020 | 5       | 54      | 0           |
| Dell                 | Inspiron 3185                                    | 2018 | 5       | 52      | 0           |
| Lenovo               | IdeaPad 3 15ALC6 82KU                            | 2021 | 5       | 51      | 0           |
| Compaq               | Presario CQ-17                                   | 2017 | 5       | 49      | 0           |
| TUXEDO               | Book BA1510                                      | 2020 | 5       | 48      | 0           |
| HUAWEI               | WRTB-WXX9                                        | 2020 | 5       | 48      | 0           |
| TrekStor             | Notebook Slim S130                               | 2018 | 5       | 48      | 0           |
| Lenovo               | ThinkPad E475                                    | 2019 | 5       | 47      | 0           |
| TrekStor             | Surfbook A13B                                    | 2018 | 5       | 47      | 0           |
| ASUSTek Computer     | Strix GL703GM_GL703GM                            | 2018 | 5       | 46      | 0           |
| Chuwi                | NA123                                            | 2017 | 5       | 45      | 0           |
| ASUSTek Computer     | GL502VSK                                         | 2017 | 5       | 45      | 0           |
| eMachines            | G730                                             | 2010 | 5       | 45      | 0           |
| Medion               | E7214                                            | 2010 | 5       | 45      | 0           |
| MSI                  | GS66 Stealth 10SE                                | 2020 | 5       | 44      | 0           |
| ASUSTek Computer     | G771JW                                           | 2015 | 5       | 44      | 0           |
| Lenovo               | IdeaPad Z560 20060                               | 2010 | 5       | 44      | 0           |
| Fujitsu Siemens      | ESPRIMO Mobile V6505                             | 2008 | 5       | 44      | 0           |
| ASUSTek Computer     | F7Z                                              | 2008 | 5       | 44      | 0           |
| Dell                 | Latitude 3520                                    | 2021 | 5       | 43      | 0           |
| Lenovo               | IdeaPad 1 14IGL05 81VU                           | 2020 | 5       | 43      | 0           |
| Star Labs            | Lite                                             | 2017 | 5       | 43      | 0           |
| Lenovo               | IdeaPad 320S-14IKB 81BN                          | 2017 | 5       | 43      | 0           |
| Medion               | Akoya P6638                                      | 2012 | 5       | 43      | 0           |
| ASUSTek Computer     | K52JV                                            | 2011 | 5       | 43      | 0           |
| Samsung Electronics  | R780                                             | 2010 | 5       | 43      | 0           |
| Quanta               | TW9/SW9                                          | 2010 | 5       | 43      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509MA                       | 2020 | 5       | 42      | 0           |
| MOTILE               | M141                                             | 2019 | 5       | 42      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FB_X512FB                | 2019 | 5       | 42      | 0           |
| Toshiba              | Satellite U300                                   | 2007 | 5       | 42      | 0           |
| ASUSTek Computer     | TUF Gaming FX505GE_FX505GE                       | 2018 | 5       | 41      | 0           |
| Acer                 | Aspire E5-523G                                   | 2016 | 5       | 41      | 0           |
| Dell                 | Inspiron 7352                                    | 2015 | 5       | 41      | 0           |
| Hewlett-Packard      | Compaq CQ45                                      | 2013 | 5       | 41      | 0           |
| Dell                 | XPS L521X                                        | 2013 | 5       | 41      | 0           |
| LG Electronics       | A410-K.BE47P1                                    | 2011 | 5       | 41      | 0           |
| LG Electronics       | R510                                             | 2008 | 5       | 41      | 0           |
| Medion               | P15648                                           | 2019 | 5       | 40      | 0           |
| ASUSTek Computer     | ASUS Gaming FX570UD                              | 2019 | 5       | 40      | 0           |
| Hewlett-Packard      | Pavilion Laptop 15-cc0xx                         | 2017 | 5       | 40      | 0           |
| MSI                  | GP70 2PE                                         | 2014 | 5       | 40      | 0           |
| Toshiba              | Satellite L875D                                  | 2012 | 5       | 40      | 0           |
| Packard Bell         | EasyNote TK11BZ                                  | 2011 | 5       | 40      | 0           |
| Itautec              | Infoway a7420                                    | 2011 | 5       | 40      | 0           |
| Acer                 | Aspire xxxx                                      | 2010 | 5       | 40      | 0           |
| ASUSTek Computer     | K52De                                            | 2010 | 5       | 40      | 0           |
| Packard Bell         | EasyNote TJ75                                    | 2009 | 5       | 40      | 0           |
| Hewlett-Packard      | ProBook 4515s                                    | 2009 | 5       | 40      | 0           |
| Hewlett-Packard      | Pavilion dv2                                     | 2009 | 5       | 40      | 0           |
| Avell High Perfor... | A62 LIV                                          | 2020 | 5       | 39      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop X505ZA_A505ZA               | 2018 | 5       | 39      | 0           |
| Dell                 | Inspiron 5576                                    | 2017 | 5       | 39      | 0           |
| Lenovo               | Flex 2-14D 20376                                 | 2014 | 5       | 39      | 0           |
| Hewlett-Packard      | EliteBook Revolve 810 G2                         | 2014 | 5       | 39      | 0           |
| ASUSTek Computer     | S550CB                                           | 2013 | 5       | 39      | 0           |
| Toshiba              | Satellite P775                                   | 2011 | 5       | 39      | 0           |
| Packard Bell         | EasyNote TK81                                    | 2010 | 5       | 39      | 0           |
| Packard Bell         | EasyNote TK36                                    | 2010 | 5       | 39      | 0           |
| Medion               | P8614                                            | 2010 | 5       | 39      | 0           |
| Hewlett-Packard      | Presario CQ42                                    | 2010 | 5       | 39      | 0           |
| ASUSTek Computer     | K72Dr                                            | 2010 | 5       | 39      | 0           |
| Samsung Electronics  | R610                                             | 2008 | 5       | 39      | 0           |
| Medion               | P8610                                            | 2008 | 5       | 39      | 0           |
| HUAWEI               | MACHD-WXX9                                       | 2021 | 5       | 38      | 0           |
| ASUSTek Computer     | FX503VM                                          | 2017 | 5       | 38      | 0           |
| Acer                 | Aspire E5-772G                                   | 2015 | 5       | 38      | 0           |
| Acer                 | Aspire E5-522                                    | 2015 | 5       | 38      | 0           |
| System76             | Galago UltraPro                                  | 2013 | 5       | 38      | 0           |
| Hewlett-Packard      | Pavilion TS 15                                   | 2013 | 5       | 38      | 0           |
| Lenovo               | IdeaPad Z565 20066                               | 2010 | 5       | 38      | 0           |
| Compal               | NCL60/61                                         | 2010 | 5       | 38      | 0           |
| Sony                 | VPCCW1S1E                                        | 2009 | 5       | 38      | 0           |
| eMachines            | D620                                             | 2008 | 5       | 38      | 0           |
| ASUSTek Computer     | F3JP                                             | 2007 | 5       | 38      | 0           |
| Toshiba              | Satellite A105                                   | 2006 | 5       | 38      | 0           |
| Notebook             | N2x0WU                                           | 2017 | 5       | 37      | 0           |
| Hewlett-Packard      | Spectre Laptop 13-af0xx                          | 2017 | 5       | 37      | 0           |
| Lenovo               | G40-80 80E4                                      | 2015 | 5       | 37      | 0           |
| Lenovo               | IdeaPad Z400 VIWZ1                               | 2013 | 5       | 37      | 0           |
| Dell                 | Inspiron N411Z                                   | 2012 | 5       | 37      | 0           |
| Dell                 | Vostro V130                                      | 2010 | 5       | 37      | 0           |
| Acer                 | Aspire 5741                                      | 2010 | 5       | 37      | 0           |
| Toshiba              | Satellite Pro L500                               | 2009 | 5       | 37      | 0           |
| ASUSTek Computer     | U50Vg                                            | 2009 | 5       | 37      | 0           |
| ASUSTek Computer     | K60IJ                                            | 2009 | 5       | 37      | 0           |
| Fujitsu Siemens      | ESPRIMO Mobile U9200                             | 2008 | 5       | 37      | 0           |
| Notebook             | NJ50_70CU                                        | 2019 | 5       | 36      | 0           |
| Dell                 | Inspiron 15-7579                                 | 2019 | 5       | 36      | 0           |
| Acer                 | Aspire E5-772                                    | 2015 | 5       | 36      | 0           |
| Acer                 | Aspire V5-572P                                   | 2013 | 5       | 36      | 0           |
| Toshiba              | Satellite R830                                   | 2011 | 5       | 36      | 0           |
| Acer                 | Aspire 4739                                      | 2011 | 5       | 36      | 0           |
| Toshiba              | Satellite L670D                                  | 2010 | 5       | 36      | 0           |
| Acer                 | Extensa 7630EZ                                   | 2009 | 5       | 36      | 0           |
| Acer                 | Aspire 7735                                      | 2009 | 5       | 36      | 0           |
| Lenovo               | IdeaPad S130-14IGM 81J2                          | 2018 | 5       | 35      | 0           |
| Lenovo               | IdeaPad 300-17ISK 80QH                           | 2016 | 5       | 35      | 0           |
| Lenovo               | G70-80 80FF                                      | 2015 | 5       | 35      | 0           |
| Dell                 | Inspiron 7558                                    | 2015 | 5       | 35      | 0           |
| Samsung Electronics  | 770Z5E/780Z5E                                    | 2014 | 5       | 35      | 0           |
| ASUSTek Computer     | X555LDB                                          | 2014 | 5       | 35      | 0           |
| Packard Bell         | EasyNote TV44HC                                  | 2012 | 5       | 35      | 0           |
| Medion               | Akoya P7818                                      | 2012 | 5       | 35      | 0           |
| Lenovo               | G480                                             | 2012 | 5       | 35      | 0           |
| Dell                 | XPS L421X                                        | 2012 | 5       | 35      | 0           |
| ASUSTek Computer     | Q500A                                            | 2012 | 5       | 35      | 0           |
| Sony                 | VPCEH1S1E                                        | 2011 | 5       | 35      | 0           |
| MSI                  | GL62 7QF                                         | 2016 | 5       | 34      | 0           |
| Toshiba              | Satellite S55t-B                                 | 2014 | 5       | 34      | 0           |
| ASUSTek Computer     | X751LA                                           | 2014 | 5       | 34      | 0           |
| Samsung Electronics  | 730U3E/740U3E                                    | 2013 | 5       | 34      | 0           |
| Medion               | E6228                                            | 2012 | 5       | 34      | 0           |
| Standard             | AHV                                              | 2011 | 5       | 34      | 0           |
| ViewSonic            | ViewBook                                         | 2010 | 5       | 34      | 0           |
| ASUSTek Computer     | X510UN                                           | 2017 | 5       | 33      | 0           |
| Lenovo               | IdeaPad 100-14IBD 80RK                           | 2016 | 5       | 33      | 0           |
| ASUSTek Computer     | X302LA                                           | 2015 | 5       | 33      | 0           |
| Coradir              | Coradir/ES10IS5                                  | 2013 | 5       | 33      | 0           |
| Medion               | E122X                                            | 2010 | 5       | 33      | 0           |
| ASUSTek Computer     | 701                                              | 2008 | 5       | 33      | 0           |
| SECO                 | UDOO x86                                         | 2017 | 5       | 32      | 0           |
| Hewlett-Packard      | 240 G4 Notebook PC                               | 2015 | 5       | 32      | 0           |
| DEXP                 | NH5BT15                                          | 2015 | 5       | 32      | 0           |
| Toshiba              | Satellite C55t-A                                 | 2013 | 5       | 32      | 0           |
| Acer                 | Aspire V5-571                                    | 2012 | 5       | 32      | 0           |
| ASUSTek Computer     | X45C                                             | 2012 | 5       | 32      | 0           |
| ASUSTek Computer     | U47A                                             | 2012 | 5       | 32      | 0           |
| Intel                | Intel                                            | 2011 | 5       | 32      | 0           |
| Hewlett-Packard      | Mini 210-3000                                    | 2011 | 5       | 32      | 0           |
| Hewlett-Packard      | Mini 110-1100                                    | 2009 | 5       | 32      | 0           |
| ASUSTek Computer     | X751NA                                           | 2017 | 5       | 31      | 0           |
| Acer                 | Aspire E5-475                                    | 2016 | 5       | 31      | 0           |
| Medion               | Akoya E4214 MD99570                              | 2015 | 5       | 31      | 0           |
| Semp Toshiba         | NI 1403                                          | 2014 | 5       | 31      | 0           |
| Packard Bell         | EasyNote ENTG71BM                                | 2014 | 5       | 31      | 0           |
| Acer                 | Extensa 2509                                     | 2014 | 5       | 31      | 0           |
| ASUSTek Computer     | X451CA                                           | 2013 | 5       | 31      | 0           |
| Samsung Electronics  | 700T                                             | 2012 | 5       | 31      | 0           |
| Lenovo               | G480 20156                                       | 2012 | 5       | 31      | 0           |
| Toshiba              | Satellite L775                                   | 2011 | 5       | 31      | 0           |
| Hewlett-Packard      | Mini 110-4100                                    | 2011 | 5       | 31      | 0           |
| Acer                 | Aspire 5750Z                                     | 2011 | 5       | 31      | 0           |
| ASUSTek Computer     | 1005PX                                           | 2010 | 5       | 31      | 0           |
| Samsung Electronics  | NC10/N110                                        | 2009 | 5       | 31      | 0           |
| Samsung Electronics  | N130                                             | 2009 | 5       | 31      | 0           |
| Hewlett-Packard      | Compaq Presario A900                             | 2007 | 5       | 31      | 0           |
| Itautec              | Infoway w7535                                    |      | 5       | 31      | 0           |
| ASUSTek Computer     | X453SA                                           | 2015 | 5       | 30      | 0           |
| Acer                 | TravelMate B115-M                                | 2014 | 5       | 30      | 0           |
| Toshiba              | Satellite C45-A                                  | 2013 | 5       | 30      | 0           |
| Toshiba              | Satellite S855                                   | 2012 | 5       | 30      | 0           |
| Acer                 | Aspire 7750                                      | 2011 | 5       | 30      | 0           |
| Acer                 | Aspire 5749                                      | 2011 | 5       | 30      | 0           |
| ASUSTek Computer     | A6Rp                                             | 2006 | 5       | 30      | 0           |
| Compaq               | Presario CQ-31                                   | 2018 | 5       | 29      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X507LA                   | 2018 | 5       | 29      | 0           |
| Acer                 | Aspire A314-31                                   | 2017 | 5       | 29      | 0           |
| Timi                 | TM1612                                           | 2016 | 5       | 29      | 0           |
| Lenovo               | IdeaPad 710S-13IKB 80VQ                          | 2016 | 5       | 29      | 0           |
| Acer                 | Aspire E5-532                                    | 2015 | 5       | 29      | 0           |
| Hewlett-Packard      | Mini 5103                                        | 2010 | 5       | 29      | 0           |
| Intel                | H81U                                             | 2019 | 5       | 28      | 0           |
| Dell                 | System XPS L321X                                 | 2012 | 5       | 28      | 0           |
| Hewlett-Packard      | Stream Laptop 14-cb1XX                           | 2011 | 5       | 28      | 0           |
| Prestigio            | Visconte Quad 3GK                                | 2015 | 5       | 24      | 0           |
| ASUSTek Computer     | X205TAW                                          | 2015 | 5       | 22      | 0           |
| ASUSTek Computer     | G73Jw                                            | 2010 | 4       | 59      | 0           |
| Alienware            | 15 R4                                            | 2019 | 4       | 57      | 0           |
| Alienware            | m17 R4                                           | 2021 | 4       | 55      | 0           |
| LG Electronics       | 17Z90N-R.AAC8U1                                  | 2020 | 4       | 55      | 0           |
| Dell                 | XPS L701X                                        | 2010 | 4       | 55      | 0           |
| ASUSTek Computer     | ROG Zephyrus GX550LXS_GX550LXS                   | 2020 | 4       | 53      | 0           |
| Gigabyte Technology  | P65                                              | 2017 | 4       | 53      | 0           |
| Schenker             | XMG FUSION 15                                    | 2020 | 4       | 52      | 0           |
| Alienware            | m17                                              | 2019 | 4       | 52      | 0           |
| Notebook             | NL40_50GU                                        | 2019 | 4       | 51      | 0           |
| ASUSTek Computer     | G752VT                                           | 2016 | 4       | 51      | 0           |
| Teclast              | F15S                                             | 2020 | 4       | 49      | 0           |
| MSI                  | GE75 Raider 10SE                                 | 2020 | 4       | 49      | 0           |
| ASUSTek Computer     | ASUSPRO P1440FAC_P1440FA                         | 2020 | 4       | 49      | 0           |
| Lenovo               | Legion 5 15IMH05H 82CF                           | 2020 | 4       | 47      | 0           |
| Hungaro Flotta Kft   | Navon Loop 360                                   | 2018 | 4       | 47      | 0           |
| Dell                 | XPS 15 9575                                      | 2018 | 4       | 47      | 0           |
| ASUSTek Computer     | GL702ZC                                          | 2017 | 4       | 47      | 0           |
| Acer                 | Aspire ES1-732                                   | 2016 | 4       | 47      | 0           |
| Lenovo               | Yoga S940-14IWL 81Q7                             | 2019 | 4       | 46      | 0           |
| ASUSTek Computer     | N61Ja                                            | 2010 | 4       | 46      | 0           |
| Lenovo               | IdeaPad Y450                                     | 2009 | 4       | 46      | 0           |
| Dell                 | Vostro 1400                                      | 2007 | 4       | 46      | 0           |
| ASUSTek Computer     | F3T                                              | 2006 | 4       | 46      | 0           |
| Acer                 | Aspire A314-22                                   | 2020 | 4       | 45      | 0           |
| Notebook             | W740SU                                           | 2013 | 4       | 45      | 0           |
| ASUSTek Computer     | K52JK                                            | 2010 | 4       | 45      | 0           |
| Google               | Delbin                                           | 2021 | 4       | 44      | 0           |
| Hewlett-Packard      | OMEN Laptop 15-ek0xxx                            | 2020 | 4       | 44      | 0           |
| Chuwi                | LapBook Pro                                      | 2019 | 4       | 44      | 0           |
| ASUSTek Computer     | ROG Strix G531GU_G531GU                          | 2019 | 4       | 44      | 0           |
| Clevo                | W240BL_W250BZ_W270BZQ                            | 2012 | 4       | 44      | 0           |
| Medion               | P6624                                            | 2010 | 4       | 44      | 0           |
| ASUSTek Computer     | F3Ke                                             | 2007 | 4       | 44      | 0           |
| Schenker             | SCHENKER VIA 15 Pro                              | 2020 | 4       | 43      | 0           |
| Notebook             | NS50MU                                           | 2020 | 4       | 43      | 0           |
| MSI                  | GL65 Leopard 10SDK                               | 2020 | 4       | 43      | 0           |
| MSI                  | GP75 Leopard 9SD                                 | 2019 | 4       | 43      | 0           |
| Acer                 | Aspire 7250G                                     | 2011 | 4       | 43      | 0           |
| Sony                 | VPCEB1E1R                                        | 2010 | 4       | 43      | 0           |
| Lenovo               | IdeaPad Y460                                     | 2010 | 4       | 43      | 0           |
| MSI                  | VR630                                            | 2008 | 4       | 43      | 0           |
| MSI                  | GF63 8RC                                         | 2018 | 4       | 42      | 0           |
| MSI                  | GE72MVR 7RG                                      | 2018 | 4       | 42      | 0           |
| ASUSTek Computer     | X555YA                                           | 2015 | 4       | 42      | 0           |
| Toshiba              | Satellite P50t-B-10T                             | 2014 | 4       | 42      | 0           |
| Quanta               | TWS                                              | 2013 | 4       | 42      | 0           |
| Sony                 | VPCEH2J1E                                        | 2012 | 4       | 42      | 0           |
| ASUSTek Computer     | K45DR                                            | 2012 | 4       | 42      | 0           |
| Lenovo               | ThinkPad X120e                                   | 2011 | 4       | 42      | 0           |
| Lenovo               | IdeaPad Z575 Sabine                              | 2011 | 4       | 42      | 0           |
| Clevo                | W760T/M740T/M760T                                | 2009 | 4       | 42      | 0           |
| Lenovo               | IdeaPad 3 14ALC6 82KT                            | 2021 | 4       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E210MA_E210MA               | 2020 | 4       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FJ_X509FJ                | 2019 | 4       | 41      | 0           |
| MSI                  | GS63 Stealth 8RE                                 | 2018 | 4       | 41      | 0           |
| MSI                  | GE70 2QE                                         | 2014 | 4       | 41      | 0           |
| Casper               | CASPER NIRVANA NOTEBOOK                          | 2013 | 4       | 41      | 0           |
| Acer                 | Aspire V5-552P                                   | 2013 | 4       | 41      | 0           |
| Positivo             | C14RV01                                          | 2012 | 4       | 41      | 0           |
| Medion               | E6220                                            | 2011 | 4       | 41      | 0           |
| Acer                 | Aspire 7560                                      | 2011 | 4       | 41      | 0           |
| ASUSTek Computer     | K43U                                             | 2011 | 4       | 41      | 0           |
| Toshiba              | Satellite L645                                   | 2010 | 4       | 41      | 0           |
| Compal               | NBLBX                                            | 2010 | 4       | 41      | 0           |
| Acer                 | Aspire A317-32                                   | 2020 | 4       | 40      | 0           |
| ASUSTek Computer     | ZenBook UX325JA_UX325JA                          | 2020 | 4       | 40      | 0           |
| MSI                  | Modern 15 A10M                                   | 2019 | 4       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FAC_X512FA               | 2019 | 4       | 40      | 0           |
| Hewlett-Packard      | EliteBook Folio G1                               | 2018 | 4       | 40      | 0           |
| ASUSTek Computer     | TUF Gaming FX505GM_FX505GM                       | 2018 | 4       | 40      | 0           |
| ASUSTek Computer     | Strix GL703GS_GL703GS                            | 2018 | 4       | 40      | 0           |
| Lenovo               | IdeaPad 320-17ABR 80YN                           | 2017 | 4       | 40      | 0           |
| Hewlett-Packard      | Pavilion Laptop 17-ar0xx                         | 2017 | 4       | 40      | 0           |
| Acer                 | Aspire E5-575T                                   | 2017 | 4       | 40      | 0           |
| Toshiba              | Satellite P855                                   | 2012 | 4       | 40      | 0           |
| Lenovo               | G485 20136                                       | 2012 | 4       | 40      | 0           |
| ASUSTek Computer     | X45U                                             | 2012 | 4       | 40      | 0           |
| Lenovo               | G450 2949                                        | 2010 | 4       | 40      | 0           |
| Acer                 | Aspire 6930                                      | 2008 | 4       | 40      | 0           |
| Acer                 | Aspire 4730Z                                     | 2008 | 4       | 40      | 0           |
| Samsung Electronics  | SR70S/SR71S                                      | 2007 | 4       | 40      | 0           |
| MSI                  | GF75 Thin 9SC                                    | 2019 | 4       | 39      | 0           |
| ASUSTek Computer     | X505BP                                           | 2017 | 4       | 39      | 0           |
| ASUSTek Computer     | X550IU                                           | 2016 | 4       | 39      | 0           |
| Acer                 | Aspire E1-532P                                   | 2014 | 4       | 39      | 0           |
| System76             | Gazelle Professional                             | 2012 | 4       | 39      | 0           |
| Sony                 | VPCYB3Q1R                                        | 2011 | 4       | 39      | 0           |
| Toshiba              | Satellite Pro C650                               | 2010 | 4       | 39      | 0           |
| Toshiba              | Satellite L635                                   | 2010 | 4       | 39      | 0           |
| Samsung Electronics  | R430/P430                                        | 2010 | 4       | 39      | 0           |
| Acer                 | Aspire 5745                                      | 2010 | 4       | 39      | 0           |
| Toshiba              | Satellite A305                                   | 2008 | 4       | 39      | 0           |
| Hewlett-Packard      | Presario V5000                                   | 2006 | 4       | 39      | 0           |
| Lenovo               | V340-17IWL 81RG                                  | 2019 | 4       | 38      | 0           |
| ASUSTek Computer     | ZenBook UX331FA_UX331FA                          | 2019 | 4       | 38      | 0           |
| Toshiba              | Satellite C55D-C                                 | 2015 | 4       | 38      | 0           |
| Lenovo               | B50-45 80F0                                      | 2014 | 4       | 38      | 0           |
| Lenovo               | IdeaPad Y410P 20216                              | 2013 | 4       | 38      | 0           |
| Acer                 | Iconia W700                                      | 2012 | 4       | 38      | 0           |
| MSI                  | CR650                                            | 2011 | 4       | 38      | 0           |
| Hewlett-Packard      | 430                                              | 2011 | 4       | 38      | 0           |
| ASUSTek Computer     | K93SV                                            | 2011 | 4       | 38      | 0           |
| IDEALMAX             | H34                                              | 2010 | 4       | 38      | 0           |
| Dell                 | Inspiron M5010                                   | 2010 | 4       | 38      | 0           |
| Acer                 | Aspire 5741ZG                                    | 2010 | 4       | 38      | 0           |
| Acer                 | Aspire 1830T                                     | 2010 | 4       | 38      | 0           |
| Toshiba              | Satellite L450                                   | 2009 | 4       | 38      | 0           |
| Dell                 | Studio 1450                                      | 2009 | 4       | 38      | 0           |
| Fujitsu Siemens      | AMILO Xi 3650                                    | 2008 | 4       | 38      | 0           |
| Acer                 | T                                                | 2008 | 4       | 38      | 0           |
| Acer                 | Extensa 7620                                     | 2007 | 4       | 38      | 0           |
| Acer                 | Aspire 7220                                      | 2007 | 4       | 38      | 0           |
| Hewlett-Packard      | EliteBook Revolve 810 G3                         | 2018 | 4       | 37      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MA_D540MA            | 2018 | 4       | 37      | 0           |
| ASUSTek Computer     | VivoBook 14_ASUS Laptop X441UAR                  | 2018 | 4       | 37      | 0           |
| Hewlett-Packard      | Laptop 17-bs1xx                                  | 2017 | 4       | 37      | 0           |
| Toshiba              | PORTEGE Z20t-C                                   | 2016 | 4       | 37      | 0           |
| Dell                 | Inspiron 17 7000 Series 7746                     | 2014 | 4       | 37      | 0           |
| Samsung Electronics  | 700G7A                                           | 2013 | 4       | 37      | 0           |
| eMachines            | eMD443                                           | 2011 | 4       | 37      | 0           |
| Toshiba              | Satellite C645                                   | 2011 | 4       | 37      | 0           |
| LG Electronics       | C400-G.BC22P1                                    | 2011 | 4       | 37      | 0           |
| ASUSTek Computer     | N43SN                                            | 2011 | 4       | 37      | 0           |
| Packard Bell         | EasyNote TM98                                    | 2010 | 4       | 37      | 0           |
| Packard Bell         | EasyNote LJ75                                    | 2010 | 4       | 37      | 0           |
| Lenovo               | G555 0873                                        | 2010 | 4       | 37      | 0           |
| Hewlett-Packard      | Mini 311-1000                                    | 2010 | 4       | 37      | 0           |
| ASUSTek Computer     | K70IJ                                            | 2009 | 4       | 37      | 0           |
| Samsung Electronics  | R508                                             | 2008 | 4       | 37      | 0           |
| Dell                 | Inspiron 3481                                    | 2019 | 4       | 36      | 0           |
| Hewlett-Packard      | Pavilion Laptop 14-ce1xxx                        | 2018 | 4       | 36      | 0           |
| Dell                 | Inspiron 17-7779                                 | 2018 | 4       | 36      | 0           |
| ASUSTek Computer     | X555BA                                           | 2018 | 4       | 36      | 0           |
| ASUSTek Computer     | VivoBook S14 X430UA                              | 2018 | 4       | 36      | 0           |
| ASUSTek Computer     | VivoBook 12_ASUS Laptop E203MA_E203MA            | 2018 | 4       | 36      | 0           |
| ASUSTek Computer     | Q524UQ                                           | 2016 | 4       | 36      | 0           |
| Toshiba              | QOSMIO X70-B                                     | 2014 | 4       | 36      | 0           |
| Medion               | E7222                                            | 2012 | 4       | 36      | 0           |
| ASUSTek Computer     | K56CA                                            | 2012 | 4       | 36      | 0           |
| Toshiba              | NB520                                            | 2011 | 4       | 36      | 0           |
| Lenovo               | G560e 20107                                      | 2011 | 4       | 36      | 0           |
| Lenovo               | B460e                                            | 2011 | 4       | 36      | 0           |
| Acer                 | Aspire 5552                                      | 2010 | 4       | 36      | 0           |
| ASUSTek Computer     | UL30VT                                           | 2010 | 4       | 36      | 0           |
| Toshiba              | Satellite L515                                   | 2009 | 4       | 36      | 0           |
| Acer                 | Aspire 5517                                      | 2009 | 4       | 36      | 0           |
| Acer                 | Aspire 4732Z                                     | 2009 | 4       | 36      | 0           |
| Acer                 | Aspire 3810TZ                                    | 2009 | 4       | 36      | 0           |
| ASUSTek Computer     | K70AB                                            | 2009 | 4       | 36      | 0           |
| Samsung Electronics  | R410P                                            | 2008 | 4       | 36      | 0           |
| ASUSTek Computer     | F80L                                             | 2008 | 4       | 36      | 0           |
| ASUSTek Computer     | A8He                                             | 2007 | 4       | 36      | 0           |
| Hewlett-Packard      | Pavilion dv8000                                  | 2006 | 4       | 36      | 0           |
| Lenovo               | IdeaPad L340-17IWL 81M0                          | 2019 | 4       | 35      | 0           |
| Dell                 | Latitude 3480                                    | 2018 | 4       | 35      | 0           |
| ASUSTek Computer     | X456URK                                          | 2016 | 4       | 35      | 0           |
| Dell                 | Latitude 3150                                    | 2015 | 4       | 35      | 0           |
| Notebook             | W51XTU                                           | 2014 | 4       | 35      | 0           |
| Acer                 | TravelMate P276-MG                               | 2014 | 4       | 35      | 0           |
| Acer                 | Aspire E5-471                                    | 2014 | 4       | 35      | 0           |
| ASUSTek Computer     | X750JA                                           | 2013 | 4       | 35      | 0           |
| Semp Toshiba         | STI NI 1401                                      | 2012 | 4       | 35      | 0           |
| ASUSTek Computer     | U24E                                             | 2012 | 4       | 35      | 0           |
| Acer                 | Aspire 4755                                      | 2011 | 4       | 35      | 0           |
| eMachines            | E527                                             | 2010 | 4       | 35      | 0           |
| eMachines            | D640                                             | 2010 | 4       | 35      | 0           |
| Acer                 | AO521                                            | 2010 | 4       | 35      | 0           |
| Toshiba              | Satellite L455                                   | 2009 | 4       | 35      | 0           |
| Advent               | Roma                                             | 2009 | 4       | 35      | 0           |
| HUAWEI               | MRC-WX0                                          | 2018 | 4       | 34      | 0           |
| Positivo Bahia - ... | VJC141F11X-B0111L                                | 2017 | 4       | 34      | 0           |
| Notebook             | N130BU                                           | 2017 | 4       | 34      | 0           |
| Google               | Link                                             | 2017 | 4       | 34      | 0           |
| Acer                 | Extensa 2530                                     | 2016 | 4       | 34      | 0           |
| Lenovo               | E4325 20306                                      | 2015 | 4       | 34      | 0           |
| Lenovo               | B70-80 80MR                                      | 2015 | 4       | 34      | 0           |
| Gateway              | NE570                                            | 2013 | 4       | 34      | 0           |
| ASUSTek Computer     | P55VA                                            | 2013 | 4       | 34      | 0           |
| Positivo             | C14CR21TV                                        | 2012 | 4       | 34      | 0           |
| ASUSTek Computer     | N76VZ                                            | 2012 | 4       | 34      | 0           |
| Sony                 | VPCEH10EB                                        | 2011 | 4       | 34      | 0           |
| ASUSTek Computer     | K50IP                                            | 2010 | 4       | 34      | 0           |
| Packard Bell         | EasyNote TJ66                                    | 2009 | 4       | 34      | 0           |
| Fujitsu Siemens      | AMILO Pi 3540                                    | 2008 | 4       | 34      | 0           |
| Hewlett-Packard      | Stream Laptop 11-ak0xxx                          | 2019 | 4       | 33      | 0           |
| Lenovo               | ThinkPad 11e 5th Gen                             | 2018 | 4       | 33      | 0           |
| Dell                 | Latitude 3570                                    | 2016 | 4       | 33      | 0           |
| Acer                 | Extensa 2520G                                    | 2016 | 4       | 33      | 0           |
| Toshiba              | Satellite L55-B                                  | 2014 | 4       | 33      | 0           |
| Medion               | AKOYA THE TOUCH 10                               | 2014 | 4       | 33      | 0           |
| Toshiba              | Satellite U940                                   | 2012 | 4       | 33      | 0           |
| Toshiba              | NB500                                            | 2011 | 4       | 33      | 0           |
| Samsung Electronics  | RF712                                            | 2011 | 4       | 33      | 0           |
| Acer                 | Aspire 5541                                      | 2010 | 4       | 33      | 0           |
| Toshiba              | Satellite L455D                                  | 2009 | 4       | 33      | 0           |
| Samsung Electronics  | R518                                             | 2009 | 4       | 33      | 0           |
| Gateway              | NV52 Series                                      | 2009 | 4       | 33      | 0           |
| Hewlett-Packard      | 2140                                             | 2008 | 4       | 33      | 0           |
| Hewlett-Packard      | Compaq nx6310                                    | 2006 | 4       | 33      | 0           |
| Acer                 | Aspire A315-32                                   | 2018 | 4       | 32      | 0           |
| SLIMBOOK             | PRO                                              | 2017 | 4       | 32      | 0           |
| Acer                 | TravelMate P238-M                                | 2016 | 4       | 32      | 0           |
| Fujitsu              | LIFEBOOK A544                                    | 2013 | 4       | 32      | 0           |
| Toshiba              | Satellite C850-C3K                               | 2012 | 4       | 32      | 0           |
| MouseComputer        | H116K                                            | 2012 | 4       | 32      | 0           |
| Lenovo               | G480 20150                                       | 2012 | 4       | 32      | 0           |
| Acer                 | TravelMate P243                                  | 2012 | 4       | 32      | 0           |
| DNS                  | MB50IA1                                          | 2011 | 4       | 32      | 0           |
| ASUSTek Computer     | P53E                                             | 2011 | 4       | 32      | 0           |
| Acer                 | AO533                                            | 2010 | 4       | 32      | 0           |
| ASUSTek Computer     | 1215P                                            | 2010 | 4       | 32      | 0           |
| Fujitsu Siemens      | AMILO Pi 2530                                    | 2007 | 4       | 32      | 0           |
| Lenovo               | V130-14IKB 81HQ                                  | 2018 | 4       | 31      | 0           |
| Lenovo               | IdeaPad 330-17IKB 81DK                           | 2018 | 4       | 31      | 0           |
| Lenovo               | IdeaPad 320-14ISK 80XG                           | 2017 | 4       | 31      | 0           |
| ASUSTek Computer     | X540UA                                           | 2017 | 4       | 31      | 0           |
| Lenovo               | Yoga 500-14IBD 80NE                              | 2015 | 4       | 31      | 0           |
| Toshiba              | Satellite L15W-B                                 | 2014 | 4       | 31      | 0           |
| Samsung Electronics  | 940X3G/930X3G                                    | 2014 | 4       | 31      | 0           |
| Lenovo               | IdeaPad U430p 20269                              | 2014 | 4       | 31      | 0           |
| Hewlett-Packard      | Falco                                            | 2014 | 4       | 31      | 0           |
| Acer                 | Extensa 2510                                     | 2014 | 4       | 31      | 0           |
| Acer                 | Aspire S7-391                                    | 2013 | 4       | 31      | 0           |
| Samsung Electronics  | RV420/RV520/RV720/E3530/S3530                    | 2011 | 4       | 31      | 0           |
| Medion               | E7218                                            | 2011 | 4       | 31      | 0           |
| ASUSTek Computer     | U46E                                             | 2011 | 4       | 31      | 0           |
| Packard Bell         | DOT SE                                           | 2010 | 4       | 31      | 0           |
| ASUSTek Computer     | 1005PE                                           | 2010 | 4       | 31      | 0           |
| Lenovo               | IdeaPad 710S-13ISK 80SW                          | 2017 | 4       | 30      | 0           |
| Lenovo               | Yoga 3 14 80JH                                   | 2015 | 4       | 30      | 0           |
| ASUSTek Computer     | E402MA                                           | 2015 | 4       | 30      | 0           |
| Lenovo               | IdeaPad S400 Touch VIUS3                         | 2013 | 4       | 30      | 0           |
| ASUSTek Computer     | K73SJ                                            | 2011 | 4       | 30      | 0           |
| Chuwi                | Hero Book                                        | 2018 | 4       | 29      | 0           |
| Notebook             | W9x0LU                                           | 2015 | 4       | 29      | 0           |
| DNS                  | W510LU                                           | 2015 | 4       | 29      | 0           |
| Dell                 | Inspiron 3451                                    | 2014 | 4       | 29      | 0           |
| Wortmann AG          | TERRA_MOBILE_1512/1712                           | 2013 | 4       | 29      | 0           |
| Hewlett-Packard      | ENVY Sleekbook 4 PC                              | 2012 | 4       | 29      | 0           |
| ASUSTek Computer     | 1015CX                                           | 2012 | 4       | 29      | 0           |
| Lenovo               | IdeaPad S145-15IKB 81XM                          | 2019 | 4       | 28      | 0           |
| Lenovo               | IdeaPad S145-14IIL 81W6                          | 2019 | 4       | 28      | 0           |
| Novastar             | KL55                                             | 2018 | 4       | 28      | 0           |
| Positivo             | H14BU08                                          | 2016 | 4       | 28      | 0           |
| Insyde               | Braswell                                         | 2015 | 4       | 28      | 0           |
| ASUSTek Computer     | X451MA                                           | 2014 | 4       | 28      | 0           |
| Lenovo               | V110-14IAP 80TF                                  | 2017 | 4       | 27      | 0           |
| Lenovo               | 1.01UL F40-30                                    | 2014 | 4       | 27      | 0           |
| ASUSTek Computer     | Q302LA                                           | 2014 | 4       | 27      | 0           |
| Prestigio            | Multipad Visconte V                              | 2016 | 4       | 24      | 0           |
| Acer                 | One S1002                                        | 2015 | 4       | 24      | 0           |
| Acer                 | Aspire SW3-013                                   | 2015 | 4       | 24      | 0           |
| Linx                 | LINX1010B                                        | 2015 | 4       | 23      | 0           |
| ASUSTek Computer     | T100TAF                                          | 2014 | 4       | 22      | 0           |
| Positivo             | WCBT1013                                         | 2014 | 4       | 21      | 0           |
| Alienware            | m15 R2                                           | 2020 | 3       | 63      | 0           |
| Sony                 | VPCF115FM                                        | 2010 | 3       | 58      | 0           |
| Dell                 | Studio 1557                                      | 2009 | 3       | 58      | 0           |
| Lenovo               | Legion Y545 81Q6                                 | 2019 | 3       | 56      | 0           |
| GPU Company          | GWTN156-1                                        | 2020 | 3       | 53      | 0           |
| Lenovo               | IdeaPad Slim 9 14ITL5 82D2                       | 2020 | 3       | 52      | 0           |
| Dell                 | Precision 5760                                   | 2021 | 3       | 50      | 0           |
| ONE-NETBOOK TECHN... | One-Mix3 Pro                                     | 2019 | 3       | 50      | 0           |
| Lenovo               | Legion R9000P2021H 82JQ                          | 2021 | 3       | 49      | 0           |
| GPU Company          | GWTN156-11                                       | 2021 | 3       | 49      | 0           |
| ASUSTek Computer     | G752VS                                           | 2017 | 3       | 49      | 0           |
| Acer                 | TravelMate P215-53                               | 2021 | 3       | 48      | 0           |
| Alienware            | m15 R3                                           | 2020 | 3       | 48      | 0           |
| AWOW                 | AK41                                             | 2020 | 3       | 48      | 0           |
| Schenker             | SCHENKER_SLIM14_SSL14L19                         | 2019 | 3       | 48      | 0           |
| Prestigio            | PSB133S01ZFH                                     | 2018 | 3       | 48      | 0           |
| ASUSTek Computer     | U32U                                             | 2011 | 3       | 48      | 0           |
| Sony                 | VPCEB3L1E                                        | 2010 | 3       | 48      | 0           |
| Dell                 | Vostro 5515                                      | 2021 | 3       | 47      | 0           |
| Eluktronics          | RP-17                                            | 2020 | 3       | 47      | 0           |
| Schenker             | XMG CORE 17(M20, RTX 2060)                       | 2020 | 3       | 46      | 0           |
| Schenker             | XMG CORE                                         | 2019 | 3       | 46      | 0           |
| MSI                  | GE63 Raider RGB 9SE                              | 2019 | 3       | 46      | 0           |
| ASUSTek Computer     | TUF Gaming FX505DY_TUF505                        | 2019 | 3       | 46      | 0           |
| Dell                 | Latitude 7275                                    | 2016 | 3       | 46      | 0           |
| MSI                  | GX70 3CC                                         | 2013 | 3       | 46      | 0           |
| ASUSTek Computer     | K53BE                                            | 2012 | 3       | 46      | 0           |
| Toshiba              | QOSMIO F50                                       | 2008 | 3       | 46      | 0           |
| Hewlett-Packard      | Laptop 14-fq0xxx                                 | 2021 | 3       | 45      | 0           |
| TrekStor             | SURFBOOK E11B                                    | 2018 | 3       | 45      | 0           |
| Prestigio            | PSB141C02                                        | 2018 | 3       | 45      | 0           |
| ASUSTek Computer     | F7SR                                             | 2007 | 3       | 45      | 0           |
| ASUSTek Computer     | A6M                                              | 2007 | 3       | 45      | 0           |
| TUXEDO               | InfinityBook S 14 v5                             | 2020 | 3       | 44      | 0           |
| ONE-NETBOOK TECHN... | A1                                               | 2020 | 3       | 44      | 0           |
| Lenovo               | Legion 7 15IMHg05 81YU                           | 2020 | 3       | 44      | 0           |
| ASUSTek Computer     | ROG Zephyrus G14 GA401IH_GA401IH                 | 2020 | 3       | 44      | 0           |
| Dell                 | Inspiron 5775                                    | 2019 | 3       | 44      | 0           |
| Samsung Electronics  | 800G5H/800G5S                                    | 2018 | 3       | 44      | 0           |
| ASUSTek Computer     | GL702VMK                                         | 2017 | 3       | 44      | 0           |
| Toshiba              | Satellite C850D-119                              | 2012 | 3       | 44      | 0           |
| ASUSTek Computer     | K73TA                                            | 2011 | 3       | 44      | 0           |
| ASUSTek Computer     | K43TA                                            | 2011 | 3       | 44      | 0           |
| Sony                 | VPCEA3S1E                                        | 2010 | 3       | 44      | 0           |
| Medion               | P6630                                            | 2010 | 3       | 44      | 0           |
| Hewlett-Packard      | Presario V3000                                   | 2007 | 3       | 44      | 0           |
| ASUSTek Computer     | G1S                                              | 2007 | 3       | 44      | 0           |
| Purism               | Librem 14                                        | 1970 | 3       | 44      | 0           |
| Mediacom             | GTZS                                             | 2021 | 3       | 43      | 0           |
| Acer                 | Swift SFX14-41G                                  | 2021 | 3       | 43      | 0           |
| MSI                  | GE75 Raider 10SF                                 | 2020 | 3       | 43      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X521EQ_S533EQ                | 2020 | 3       | 43      | 0           |
| MSI                  | GL65 9SD                                         | 2019 | 3       | 43      | 0           |
| Dell                 | Inspiron 3785                                    | 2019 | 3       | 43      | 0           |
| Akstron              | G1417                                            | 2019 | 3       | 43      | 0           |
| MSI                  | GT72 2QD                                         | 2014 | 3       | 43      | 0           |
| Lenovo               | IdeaPad Y500 9541                                | 2013 | 3       | 43      | 0           |
| Toshiba              | Satellite C850D-11K                              | 2012 | 3       | 43      | 0           |
| Sony                 | SVE1112M1EW                                      | 2012 | 3       | 43      | 0           |
| Lenovo               | IdeaPad Z585                                     | 2012 | 3       | 43      | 0           |
| Sony                 | VPCEB2M1R                                        | 2010 | 3       | 43      | 0           |
| Sony                 | VPCCW21FX                                        | 2009 | 3       | 43      | 0           |
| Medion               | E7212                                            | 2009 | 3       | 43      | 0           |
| Hewlett-Packard      | Compaq Presario CQ45                             | 2009 | 3       | 43      | 0           |
| Sony                 | VGN-FW21M                                        | 2008 | 3       | 43      | 0           |
| Sony                 | VGN-FW21E                                        | 2008 | 3       | 43      | 0           |
| Lenovo               | ThinkPad E455                                    | 2018 | 3       | 42      | 0           |
| Lenovo               | Flex 2-15D 20377                                 | 2014 | 3       | 42      | 0           |
| Toshiba              | Satellite L775-A1W                               | 2012 | 3       | 42      | 0           |
| Samsung Electronics  | RV413/RV513/E3413                                | 2012 | 3       | 42      | 0           |
| ASUSTek Computer     | N53TK                                            | 2012 | 3       | 42      | 0           |
| ASUSTek Computer     | K53Z                                             | 2011 | 3       | 42      | 0           |
| Sony                 | VPCEB4L1E                                        | 2010 | 3       | 42      | 0           |
| Sony                 | VPCEB1E9R                                        | 2010 | 3       | 42      | 0           |
| Sony                 | VPCEA47FX                                        | 2010 | 3       | 42      | 0           |
| Sony                 | VGN-AW41MF_H                                     | 2009 | 3       | 42      | 0           |
| Medion               | P7612                                            | 2009 | 3       | 42      | 0           |
| Acer                 | Aspire 2930Z                                     | 2008 | 3       | 42      | 0           |
| Toshiba              | Satellite P200D                                  | 2007 | 3       | 42      | 0           |
| Fujitsu Siemens      | AMILO Xi 2528                                    | 2007 | 3       | 42      | 0           |
| Fujitsu Siemens      | AMILO Si 1520                                    | 2007 | 3       | 42      | 0           |
| ASUSTek Computer     | ASUS TUF Gaming F15 FX506LU_FX506LU              | 2021 | 3       | 41      | 0           |
| MSI                  | GL65 Leopard 10SDR                               | 2020 | 3       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X421FAY_X413FA               | 2020 | 3       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X510QR_X510QR                | 2019 | 3       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FL_X509FL                | 2019 | 3       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop X505ZA_F505ZA               | 2019 | 3       | 41      | 0           |
| ASUSTek Computer     | ASUS EXPERTBOOK B9450FA_B9450FA                  | 2019 | 3       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X570ZD_R570ZD                | 2018 | 3       | 41      | 0           |
| ASUSTek Computer     | X455LA                                           | 2014 | 3       | 41      | 0           |
| Sony                 | SVE1113M1EW                                      | 2013 | 3       | 41      | 0           |
| Gateway              | NV55S                                            | 2012 | 3       | 41      | 0           |
| Sony                 | VPCEB3J1E                                        | 2010 | 3       | 41      | 0           |
| ASUSTek Computer     | P52F                                             | 2010 | 3       | 41      | 0           |
| Dell                 | Inspiron 14 5410 2-in-1                          | 2021 | 3       | 40      | 0           |
| Acer                 | Swift SF514-55T                                  | 2020 | 3       | 40      | 0           |
| Dell                 | Inspiron 3595                                    | 2019 | 3       | 40      | 0           |
| Teclast              | F6 Pro                                           | 2018 | 3       | 40      | 0           |
| ASUSTek Computer     | VivoBook E14 E402WAS                             | 2018 | 3       | 40      | 0           |
| ASUSTek Computer     | X751BP                                           | 2017 | 3       | 40      | 0           |
| Notebook             | P64_HJ,HK1                                       | 2016 | 3       | 40      | 0           |
| ASUSTek Computer     | UX310UA                                          | 2016 | 3       | 40      | 0           |
| Toshiba              | Satellite S40Dt-A                                | 2013 | 3       | 40      | 0           |
| Acer                 | Aspire E1-521                                    | 2012 | 3       | 40      | 0           |
| ASUSTek Computer     | S550CM                                           | 2012 | 3       | 40      | 0           |
| Medion               | E7220                                            | 2011 | 3       | 40      | 0           |
| MSI                  | MS-N0E1                                          | 2011 | 3       | 40      | 0           |
| Lenovo               | IdeaPad Z460 0913                                | 2011 | 3       | 40      | 0           |
| Dell                 | Vostro 1540                                      | 2011 | 3       | 40      | 0           |
| Acer                 | TravelMate 5744Z                                 | 2011 | 3       | 40      | 0           |
| HCL Infosystems L... | HCL ME Laptop                                    | 2010 | 3       | 40      | 0           |
| Fujitsu              | LIFEBOOK AH530/HD6                               | 2010 | 3       | 40      | 0           |
| Sony                 | VGN-NW21MF_W                                     | 2009 | 3       | 40      | 0           |
| Lenovo               | 3000 G530 444622G                                | 2009 | 3       | 40      | 0           |
| Hewlett-Packard      | Pavilion                                         | 2005 | 3       | 40      | 0           |
| MSI                  | GF63 Thin 9SCSR                                  | 2020 | 3       | 39      | 0           |
| ASUSTek Computer     | X442URR                                          | 2019 | 3       | 39      | 0           |
| Dell                 | Venue 11 Pro 7130 MS                             | 2018 | 3       | 39      | 0           |
| Entroware            | Apollo                                           | 2017 | 3       | 39      | 0           |
| ASUSTek Computer     | X705UNR                                          | 2017 | 3       | 39      | 0           |
| ASUSTek Computer     | GL702VSK                                         | 2017 | 3       | 39      | 0           |
| Lenovo               | G51-35 80M8                                      | 2015 | 3       | 39      | 0           |
| ASUSTek Computer     | UX303LA                                          | 2014 | 3       | 39      | 0           |
| Toshiba              | Satellite C70D-A                                 | 2013 | 3       | 39      | 0           |
| Dell                 | Inspiron 5735                                    | 2013 | 3       | 39      | 0           |
| Acer                 | Aspire 5333                                      | 2011 | 3       | 39      | 0           |
| Toshiba              | Satellite Pro L630                               | 2010 | 3       | 39      | 0           |
| Sony                 | VPCYB2L1R                                        | 2010 | 3       | 39      | 0           |
| Samsung Electronics  | R440                                             | 2010 | 3       | 39      | 0           |
| MSI                  | MS-1738                                          | 2010 | 3       | 39      | 0           |
| Clevo                | E512xQ/E4129                                     | 2010 | 3       | 39      | 0           |
| Toshiba              | Satellite L510                                   | 2009 | 3       | 39      | 0           |
| Acer                 | Aspire 3410                                      | 2009 | 3       | 39      | 0           |
| Samsung Electronics  | R505                                             | 2008 | 3       | 39      | 0           |
| realme               | RMNBXXXX                                         | 2021 | 3       | 38      | 0           |
| TQ-Group             | TQMxE39S                                         | 2019 | 3       | 38      | 0           |
| LG Electronics       | 14Z990-V.AR52A2                                  | 2019 | 3       | 38      | 0           |
| Hewlett-Packard      | 340S G7                                          | 2019 | 3       | 38      | 0           |
| Monster              | ABRA A5 V11.1                                    | 2018 | 3       | 38      | 0           |
| MSI                  | GS43VR 7RE                                       | 2018 | 3       | 38      | 0           |
| Dell                 | Inspiron 5481                                    | 2018 | 3       | 38      | 0           |
| MSI                  | GV72 7RD                                         | 2017 | 3       | 38      | 0           |
| Toshiba              | Satellite P70-A                                  | 2014 | 3       | 38      | 0           |
| Toshiba              | Satellite L840                                   | 2012 | 3       | 38      | 0           |
| Toshiba              | Satellite R850                                   | 2011 | 3       | 38      | 0           |
| Compal               | PCW20                                            | 2011 | 3       | 38      | 0           |
| Semp Toshiba         | IS 1817HD                                        | 2010 | 3       | 38      | 0           |
| Semp Toshiba         | IS 1414                                          | 2010 | 3       | 38      | 0           |
| Samsung Electronics  | Q430/Q530                                        | 2010 | 3       | 38      | 0           |
| Fujitsu              | LIFEBOOK LH530                                   | 2010 | 3       | 38      | 0           |
| Dell                 | Inspiron 1464                                    | 2010 | 3       | 38      | 0           |
| ASUSTek Computer     | F83VF                                            | 2010 | 3       | 38      | 0           |
| Toshiba              | Satellite Pro L450                               | 2009 | 3       | 38      | 0           |
| ASUSTek Computer     | N71Vn                                            | 2009 | 3       | 38      | 0           |
| ASUSTek Computer     | N61Vn                                            | 2009 | 3       | 38      | 0           |
| ASUSTek Computer     | N60Dp                                            | 2009 | 3       | 38      | 0           |
| Sony                 | VGN-FW31E                                        | 2008 | 3       | 38      | 0           |
| Acer                 | TravelMate 5310                                  | 2007 | 3       | 38      | 0           |
| ASUSTek Computer     | F3JA                                             | 2006 | 3       | 38      | 0           |
| Hewlett-Packard      | Laptop 14s-fq1xxx                                | 2021 | 3       | 37      | 0           |
| Lenovo               | IdeaPad 1 14ADA05 82GW                           | 2020 | 3       | 37      | 0           |
| HUAWEI               | EUL-WX9                                          | 2020 | 3       | 37      | 0           |
| Dell                 | Inspiron 5493                                    | 2020 | 3       | 37      | 0           |
| MSI                  | GS63VR 6RF                                       | 2018 | 3       | 37      | 0           |
| Haier                | Y11C                                             | 2018 | 3       | 37      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X507MA_F507MA            | 2018 | 3       | 37      | 0           |
| Notebook             | N85_N87HCHNHZ                                    | 2017 | 3       | 37      | 0           |
| Lenovo               | Y520-15IKBA 80WY                                 | 2017 | 3       | 37      | 0           |
| Lenovo               | IdeaPad 320S-13IKB 81AK                          | 2017 | 3       | 37      | 0           |
| Lenovo               | IdeaPad 110-15AST 80TR                           | 2017 | 3       | 37      | 0           |
| ASUSTek Computer     | GL702VI                                          | 2017 | 3       | 37      | 0           |
| ASUSTek Computer     | E402BA                                           | 2017 | 3       | 37      | 0           |
| Notebook             | N750BU                                           | 2016 | 3       | 37      | 0           |
| Alienware            | 13 R2                                            | 2016 | 3       | 37      | 0           |
| Acer                 | Aspire E5-553                                    | 2016 | 3       | 37      | 0           |
| Toshiba              | Satellite P70-B                                  | 2014 | 3       | 37      | 0           |
| Acer                 | Aspire V5-122                                    | 2013 | 3       | 37      | 0           |
| Sony                 | VPCF23S1E                                        | 2011 | 3       | 37      | 0           |
| Samsung Electronics  | R469/P469                                        | 2010 | 3       | 37      | 0           |
| Medion               | E6214                                            | 2010 | 3       | 37      | 0           |
| ASUSTek Computer     | UL20FT                                           | 2010 | 3       | 37      | 0           |
| Toshiba              | Satellite U505                                   | 2009 | 3       | 37      | 0           |
| Acer                 | Aspire 5538G                                     | 2009 | 3       | 37      | 0           |
| Acer                 | Aspire 4740                                      | 2009 | 3       | 37      | 0           |
| ASUSTek Computer     | K70AD                                            | 2009 | 3       | 37      | 0           |
| Sony                 | VGN-NR430E                                       | 2008 | 3       | 37      | 0           |
| Fujitsu Siemens      | AMILO Pa 2510                                    | 2007 | 3       | 37      | 0           |
| Lenovo               | Yoga Slim 7 15ITL05 82AC                         | 2021 | 3       | 36      | 0           |
| Hewlett-Packard      | Stream Laptop 14-ds0xxx                          | 2019 | 3       | 36      | 0           |
| Hewlett-Packard      | 470 G7 Notebook PC                               | 2019 | 3       | 36      | 0           |
| Dell                 | Inspiron 3581                                    | 2019 | 3       | 36      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FA_A509FA                | 2019 | 3       | 36      | 0           |
| Notebook             | N8xxEP6                                          | 2018 | 3       | 36      | 0           |
| ASUSTek Computer     | X405UA                                           | 2017 | 3       | 36      | 0           |
| Acer                 | Aspire F5-572G                                   | 2016 | 3       | 36      | 0           |
| ASUSTek Computer     | X756UVK                                          | 2016 | 3       | 36      | 0           |
| ASUSTek Computer     | X751LB                                           | 2015 | 3       | 36      | 0           |
| Semp Toshiba         | NA 1402                                          | 2014 | 3       | 36      | 0           |
| BANGHO               | MAX G0101                                        | 2014 | 3       | 36      | 0           |
| Toshiba              | QOSMIO X870                                      | 2013 | 3       | 36      | 0           |
| Toshiba              | PORTEGE Z10T-A                                   | 2013 | 3       | 36      | 0           |
| Medion               | AKOYA E1318T                                     | 2013 | 3       | 36      | 0           |
| Sony                 | SVE1713Y1EB                                      | 2012 | 3       | 36      | 0           |
| Pegatron             | A24                                              | 2011 | 3       | 36      | 0           |
| Medion               | P6634                                            | 2011 | 3       | 36      | 0           |
| BANGHO               | W240HU/W250HUQ                                   | 2011 | 3       | 36      | 0           |
| Toshiba              | PORTEGE R705                                     | 2010 | 3       | 36      | 0           |
| ASUSTek Computer     | K40AD                                            | 2010 | 3       | 36      | 0           |
| ASUSTek Computer     | K51AE                                            | 2009 | 3       | 36      | 0           |
| Samsung Electronics  | R460                                             | 2008 | 3       | 36      | 0           |
| Samsung Electronics  | Q310                                             | 2008 | 3       | 36      | 0           |
| Samsung Electronics  | Q210/P210                                        | 2008 | 3       | 36      | 0           |
| HONOR                | NBR-WAX9                                         | 2021 | 3       | 35      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X403JA_X403JA                | 2020 | 3       | 35      | 0           |
| Maibenben            | XiaoMai6 Plus                                    | 2019 | 3       | 35      | 0           |
| Maibenben            | XiaoMai5                                         | 2019 | 3       | 35      | 0           |
| Lenovo               | IdeaPad S940-14IWL 81R0                          | 2019 | 3       | 35      | 0           |
| Notebook             | N14xWU                                           | 2018 | 3       | 35      | 0           |
| MSI                  | GL73 8RD                                         | 2018 | 3       | 35      | 0           |
| Lenovo               | IdeaPad 330S-15IKB 81JN                          | 2018 | 3       | 35      | 0           |
| ASUSTek Computer     | ZenBook 13 UX331UAL                              | 2018 | 3       | 35      | 0           |
| MSI                  | GS63 7RD                                         | 2017 | 3       | 35      | 0           |
| MSI                  | GL72M 7REX                                       | 2017 | 3       | 35      | 0           |
| Hewlett-Packard      | ProBook 640 G3                                   | 2017 | 3       | 35      | 0           |
| System76             | Kudu                                             | 2016 | 3       | 35      | 0           |
| MSI                  | GE72 7RE                                         | 2016 | 3       | 35      | 0           |
| Acer                 | Aspire VN7-572G                                  | 2015 | 3       | 35      | 0           |
| Acer                 | Aspire ES1-731G                                  | 2015 | 3       | 35      | 0           |
| ASUSTek Computer     | X556UF                                           | 2015 | 3       | 35      | 0           |
| Samsung Electronics  | 870Z5G/880Z5F                                    | 2014 | 3       | 35      | 0           |
| Acer                 | Aspire E5-471G                                   | 2014 | 3       | 35      | 0           |
| VIT                  | P3400                                            | 2013 | 3       | 35      | 0           |
| Samsung Electronics  | 700T1C                                           | 2013 | 3       | 35      | 0           |
| Pegatron             | C17A                                             | 2013 | 3       | 35      | 0           |
| Sony                 | SVE1712C5E                                       | 2012 | 3       | 35      | 0           |
| Samsung Electronics  | Q470C/500P4C                                     | 2012 | 3       | 35      | 0           |
| MSI                  | GE60 0NC/GE60 0ND                                | 2012 | 3       | 35      | 0           |
| Acer                 | Aspire V5-551                                    | 2012 | 3       | 35      | 0           |
| Sony                 | VPCEH3J1R                                        | 2011 | 3       | 35      | 0           |
| Dell                 | System Inspiron N4110                            | 2011 | 3       | 35      | 0           |
| Clevo                | W270HU                                           | 2011 | 3       | 35      | 0           |
| ASUSTek Computer     | K84LY                                            | 2011 | 3       | 35      | 0           |
| eMachines            | eME442                                           | 2010 | 3       | 35      | 0           |
| eMachines            | E430                                             | 2010 | 3       | 35      | 0           |
| Samsung Electronics  | RV410/RV510                                      | 2010 | 3       | 35      | 0           |
| Packard Bell         | EasyNote TH36                                    | 2010 | 3       | 35      | 0           |
| eMachines            | G627                                             | 2009 | 3       | 35      | 0           |
| Toshiba              | Satellite T130                                   | 2009 | 3       | 35      | 0           |
| Samsung Electronics  | R580                                             | 2009 | 3       | 35      | 0           |
| Packard Bell         | EasyNote TJ71                                    | 2009 | 3       | 35      | 0           |
| Acer                 | Extensa 5635                                     | 2009 | 3       | 35      | 0           |
| eMachines            | E620                                             | 2008 | 3       | 35      | 0           |
| Dell                 | Inspiron 910                                     | 2008 | 3       | 35      | 0           |
| Acer                 | Ferrari 4000                                     | 2005 | 3       | 35      | 0           |
| Acer                 | Aspire 5670                                      | 2005 | 3       | 35      | 0           |
| Acer                 | Aspire A317-52                                   | 2020 | 3       | 34      | 0           |
| Samsung Electronics  | 850XBD                                           | 2019 | 3       | 34      | 0           |
| ASUSTek Computer     | ZenBook UX434FAC_UX434FA                         | 2019 | 3       | 34      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X712FA_X712FA                | 2019 | 3       | 34      | 0           |
| ASUSTek Computer     | VivoBook S15 X530UA                              | 2018 | 3       | 34      | 0           |
| Lenovo               | IdeaPad 110-14ISK 80UC                           | 2016 | 3       | 34      | 0           |
| Dell                 | Latitude 3460                                    | 2016 | 3       | 34      | 0           |
| Notebook             | N24_25JU                                         | 2015 | 3       | 34      | 0           |
| Acer                 | Aspire E1-772                                    | 2014 | 3       | 34      | 0           |
| Lenovo               | IdeaPad U530 Touch 20289                         | 2013 | 3       | 34      | 0           |
| Lenovo               | G400s 20244                                      | 2013 | 3       | 34      | 0           |
| Acer                 | Aspire V5-473P                                   | 2013 | 3       | 34      | 0           |
| Toshiba              | Satellite C845                                   | 2012 | 3       | 34      | 0           |
| Lenovo               | IdeaPad U510 4941                                | 2012 | 3       | 34      | 0           |
| Hewlett-Packard      | Folio 13 - 2000                                  | 2012 | 3       | 34      | 0           |
| Hewlett-Packard      | ENVY Sleekbook 6                                 | 2012 | 3       | 34      | 0           |
| Gateway              | LT40                                             | 2012 | 3       | 34      | 0           |
| Acer                 | Aspire V5-471G                                   | 2012 | 3       | 34      | 0           |
| Toshiba              | Satellite L735                                   | 2011 | 3       | 34      | 0           |
| Toshiba              | Satellite C665                                   | 2011 | 3       | 34      | 0           |
| Sony                 | VPCEH40EB                                        | 2011 | 3       | 34      | 0           |
| eMachines            | G625                                             | 2009 | 3       | 34      | 0           |
| Samsung Electronics  | SR58P                                            | 2008 | 3       | 34      | 0           |
| Fujitsu Siemens      | AMILO Li 2732                                    | 2008 | 3       | 34      | 0           |
| Samsung Electronics  | R40P/R41P                                        | 2007 | 3       | 34      | 0           |
| Fujitsu Siemens      | AMILO Pi 2512                                    | 2007 | 3       | 34      | 0           |
| Toshiba              | TECRA A8                                         | 2006 | 3       | 34      | 0           |
| ASUSTek Computer     | A6J                                              | 2006 | 3       | 34      | 0           |
| Lenovo               | IdeaPad S145-15IGM 81MX                          | 2019 | 3       | 33      | 0           |
| Hewlett-Packard      | Pavilion Laptop 13-an1xxx                        | 2019 | 3       | 33      | 0           |
| ASUSTek Computer     | ZenBook UX431FAC_UX431FA                         | 2019 | 3       | 33      | 0           |
| MSI                  | GV62 8RD                                         | 2018 | 3       | 33      | 0           |
| Dell                 | Vostro 3581                                      | 2018 | 3       | 33      | 0           |
| ASUSTek Computer     | VivoBook 14_ASUS Laptop X407UF                   | 2018 | 3       | 33      | 0           |
| ASUSTek Computer     | VivoBook 14_ASUS Laptop X407UAR                  | 2018 | 3       | 33      | 0           |
| Hewlett-Packard      | Spectre Notebook                                 | 2017 | 3       | 33      | 0           |
| Acer                 | TravelMate P249-G2-M                             | 2017 | 3       | 33      | 0           |
| Sony                 | SVE1512C6EW                                      | 2016 | 3       | 33      | 0           |
| Samsung Electronics  | 500R4K/500R5H/5400RK/501R5H/5500RH/500R5S        | 2016 | 3       | 33      | 0           |
| MSI                  | GP62 6QE                                         | 2016 | 3       | 33      | 0           |
| Positivo             | POSITIVO MASTER N130i                            | 2015 | 3       | 33      | 0           |
| Notebook             | W65_W67RZ                                        | 2015 | 3       | 33      | 0           |
| ASUSTek Computer     | X751LAB                                          | 2015 | 3       | 33      | 0           |
| Lenovo               | G40-70 20369                                     | 2014 | 3       | 33      | 0           |
| Hewlett-Packard      | ENVY m7                                          | 2014 | 3       | 33      | 0           |
| Dell                 | Vostro 3445                                      | 2014 | 3       | 33      | 0           |
| Acer                 | Aspire E5-771                                    | 2014 | 3       | 33      | 0           |
| ASUSTek Computer     | Q502LA                                           | 2014 | 3       | 33      | 0           |
| Lenovo               | IdeaPad P400 Touch 20211                         | 2013 | 3       | 33      | 0           |
| Gateway              | NV570P                                           | 2013 | 3       | 33      | 0           |
| Toshiba              | Satellite U920t                                  | 2012 | 3       | 33      | 0           |
| Positivo             | W540EU                                           | 2012 | 3       | 33      | 0           |
| Philco               | 14H                                              | 2012 | 3       | 33      | 0           |
| Hewlett-Packard      | Spectre XT Ultrabook PC                          | 2012 | 3       | 33      | 0           |
| Hewlett-Packard      | ENVY Spectre XT Ultrabook PC                     | 2012 | 3       | 33      | 0           |
| Fujitsu              | LIFEBOOK AH552/SL                                | 2012 | 3       | 33      | 0           |
| ASUSTek Computer     | UX21E                                            | 2012 | 3       | 33      | 0           |
| Samsung Electronics  | SF311/SF411/SF511                                | 2011 | 3       | 33      | 0           |
| Toshiba              | NB255                                            | 2010 | 3       | 33      | 0           |
| Hewlett-Packard      | Mini 110-3000                                    | 2010 | 3       | 33      | 0           |
| Hannspree            | SN12E200                                         | 2010 | 3       | 33      | 0           |
| Clevo                | M1110M                                           | 2010 | 3       | 33      | 0           |
| Acer                 | Aspire 4745Z                                     | 2010 | 3       | 33      | 0           |
| Toshiba              | Satellite L450D                                  | 2009 | 3       | 33      | 0           |
| Lenovo               | IdeaPad S10-2 2957                               | 2009 | 3       | 33      | 0           |
| Acer                 | Aspire 5532                                      | 2009 | 3       | 33      | 0           |
| Fujitsu Siemens      | AMILO Pi 2540                                    | 2008 | 3       | 33      | 0           |
| Dixonsxp             | F71IX1                                           | 2008 | 3       | 33      | 0           |
| Acer                 | Aspire 1640Z                                     | 2006 | 3       | 33      | 0           |
| Notebook             | N7x0WU                                           | 2018 | 3       | 32      | 0           |
| Dell                 | Latitude 3590                                    | 2018 | 3       | 32      | 0           |
| Lenovo               | IdeaPad 320-17ISK 80XJ                           | 2017 | 3       | 32      | 0           |
| Medion               | Akoya E6424 MD99850                              | 2016 | 3       | 32      | 0           |
| Lenovo               | Yoga 510-14ISK 80S7                              | 2016 | 3       | 32      | 0           |
| Lenovo               | ThinkPad 11e 3rd Gen                             | 2016 | 3       | 32      | 0           |
| Fujitsu              | LIFEBOOK A556                                    | 2016 | 3       | 32      | 0           |
| Dell                 | Inspiron 5459                                    | 2016 | 3       | 32      | 0           |
| Compaq               | PRESARIO CQ-23                                   | 2016 | 3       | 32      | 0           |
| ASUSTek Computer     | UX360CA                                          | 2016 | 3       | 32      | 0           |
| Lenovo               | B50-50 80S2                                      | 2015 | 3       | 32      | 0           |
| Toshiba              | Satellite L70-B                                  | 2014 | 3       | 32      | 0           |
| Toshiba              | Satellite C70-A                                  | 2014 | 3       | 32      | 0           |
| Medion               | Akoya E6416                                      | 2014 | 3       | 32      | 0           |
| Toshiba              | Satellite E55-A                                  | 2013 | 3       | 32      | 0           |
| Medion               | S621xT                                           | 2013 | 3       | 32      | 0           |
| Lenovo               | IdeaPadFlex 14 20308                             | 2013 | 3       | 32      | 0           |
| Acer                 | Aspire V5-472                                    | 2013 | 3       | 32      | 0           |
| Intel                | powered classmate PC                             | 2011 | 3       | 32      | 0           |
| HCL Infosystems L... | HCL ME LAPTOP                                    | 2011 | 3       | 32      | 0           |
| Hewlett-Packard      | Compaq Mini                                      | 2008 | 3       | 32      | 0           |
| Toshiba              | EQUIUM A100                                      | 2006 | 3       | 32      | 0           |
| MSI                  | Modern 14 B10MW                                  | 2020 | 3       | 31      | 0           |
| Samsung Electronics  | 500R5L/501R5L/500R5P                             | 2017 | 3       | 31      | 0           |
| Acer                 | Aspire ES1-332                                   | 2017 | 3       | 31      | 0           |
| Lenovo               | IdeaPad 110-15IBR 80W2                           | 2016 | 3       | 31      | 0           |
| Notebook             | W65_W67RB                                        | 2015 | 3       | 31      | 0           |
| Dell                 | Inspiron 5551                                    | 2015 | 3       | 31      | 0           |
| Acer                 | Aspire V3-575                                    | 2015 | 3       | 31      | 0           |
| Lenovo               | Yoga 2 11 20428                                  | 2014 | 3       | 31      | 0           |
| Acer                 | TravelMate P256-M                                | 2014 | 3       | 31      | 0           |
| ASUSTek Computer     | TAICHI21                                         | 2014 | 3       | 31      | 0           |
| ASUSTek Computer     | X202EV                                           | 2013 | 3       | 31      | 0           |
| Medion               | S4216                                            | 2012 | 3       | 31      | 0           |
| Lenovo               | IdeaPad P500 20210                               | 2012 | 3       | 31      | 0           |
| Hewlett-Packard      | Mini 210-4000                                    | 2012 | 3       | 31      | 0           |
| Sony                 | VPCEH2Q1E                                        | 2011 | 3       | 31      | 0           |
| Sony                 | VPCEH25FM                                        | 2011 | 3       | 31      | 0           |
| Packard Bell         | DOTS2                                            | 2010 | 3       | 31      | 0           |
| Toshiba              | Satellite T135D                                  | 2009 | 3       | 31      | 0           |
| Medion               | E1210                                            | 2008 | 3       | 31      | 0           |
| TUXEDO               | N8xxEZ                                           | 2018 | 3       | 30      | 0           |
| ASUSTek Computer     | E203NAS                                          | 2017 | 3       | 30      | 0           |
| ASUSTek Computer     | TP201SA                                          | 2016 | 3       | 30      | 0           |
| Positivo             | H14SU08                                          | 2015 | 3       | 30      | 0           |
| Lenovo               | Yoga 700-14ISK 80QD                              | 2015 | 3       | 30      | 0           |
| Toshiba              | Satellite_C50-A                                  | 2014 | 3       | 30      | 0           |
| Intel                | CedarTrail Platform                              | 2014 | 3       | 30      | 0           |
| Acer                 | TMP255-M                                         | 2014 | 3       | 30      | 0           |
| ASUSTek Computer     | UX32LA                                           | 2014 | 3       | 30      | 0           |
| Lenovo               | IdeaPad S210 Touch 20257                         | 2013 | 3       | 30      | 0           |
| Gigabyte Technology  | MMLP5AP-00                                       | 2013 | 3       | 30      | 0           |
| Lenovo               | QIWG5                                            | 2012 | 3       | 30      | 0           |
| Lenovo               | IdeaPad S400 20195                               | 2012 | 3       | 30      | 0           |
| ASUSTek Computer     | 1025CE                                           | 2012 | 3       | 30      | 0           |
| Sony                 | VPCEH1AFX                                        | 2011 | 3       | 30      | 0           |
| Acer                 | Aspire 4352                                      | 2011 | 3       | 30      | 0           |
| Samsung Electronics  | N128                                             | 2010 | 3       | 30      | 0           |
| ASUSTek Computer     | 1201PN                                           | 2010 | 3       | 30      | 0           |
| MSI                  | MS-N033                                          | 2009 | 3       | 30      | 0           |
| Hewlett-Packard      | Mini 110-1000                                    | 2009 | 3       | 30      | 0           |
| Clevo                | M770SUA                                          | 2009 | 3       | 30      | 0           |
| ASUSTek Computer     | 1001HA                                           | 2009 | 3       | 30      | 0           |
| Dell                 | Latitude 3310                                    | 2019 | 3       | 29      | 0           |
| Lenovo               | V110-15IKB 80TH                                  | 2017 | 3       | 29      | 0           |
| Lenovo               | IdeaPad 310-14ISK 80SL                           | 2016 | 3       | 29      | 0           |
| ASUSTek Computer     | X441SA                                           | 2016 | 3       | 29      | 0           |
| ASUSTek Computer     | X540SC                                           | 2015 | 3       | 29      | 0           |
| Lenovo               | B40-30 80F1                                      | 2014 | 3       | 29      | 0           |
| Toshiba              | Satellite C50-A-19T                              | 2013 | 3       | 29      | 0           |
| Samsung Electronics  | 3570R/370R/470R/450R/510R                        | 2013 | 3       | 29      | 0           |
| Toshiba              | Satellite L845                                   | 2012 | 3       | 29      | 0           |
| Samsung Electronics  | RV418/RV518/RV718                                | 2011 | 3       | 29      | 0           |
| ASUSTek Computer     | VivoBook 14_ASUS Laptop E406SAS                  | 2019 | 3       | 28      | 0           |
| Dell                 | Latitude 3470                                    | 2016 | 3       | 28      | 0           |
| Acer                 | Aspire ES1-331                                   | 2015 | 3       | 28      | 0           |
| Medion               | Akoya THE TOUCH 10                               | 2014 | 3       | 28      | 0           |
| Google               | Terra                                            | 2019 | 3       | 27      | 0           |
| NOBLEX               | SF20BA                                           | 2017 | 3       | 27      | 0           |
| Google               | Reks                                             | 2017 | 3       | 27      | 0           |
| Shuttle              | DS57U                                            | 2015 | 3       | 27      | 0           |
| Dell                 | Inspiron 3551                                    | 2014 | 3       | 27      | 0           |
| Samsung Electronics  | RV419                                            | 2011 | 3       | 26      | 0           |
| Insignia             | NS-P11W7100                                      | 2016 | 3       | 25      | 0           |
| VIT                  | M2421                                            | 2011 | 3       | 25      | 0           |
| Prestigio            | Smartbook PSB116A                                | 2016 | 3       | 24      | 0           |
| TrekStor             | SurfTab wintron 7.0 ST70416-6                    | 2015 | 3       | 24      | 0           |
| Foxconn              | Kangaroo Mobile Desktop                          | 2015 | 3       | 24      | 0           |
| ASUSTek Computer     | T100TAL                                          | 2015 | 3       | 24      | 0           |
| ASUSTek Computer     | T100TAM                                          | 2014 | 3       | 24      | 0           |
| Radxa                | ROCK Pi X                                        | 2020 | 3       | 23      | 0           |
| Google               | Banjo                                            | 2019 | 3       | 22      | 0           |
| Shenzhen Da&Fong ... | S107I                                            | 2015 | 3       | 22      | 0           |
| Hewlett-Packard      | kip                                              | 2014 | 3       | 22      | 0           |
| AMI                  | Board                                            | 2017 | 3       | 21      | 0           |
| Acer                 | Aspire SW5-011                                   | 2014 | 3       | 21      | 0           |
| ASUSTek Computer     | T100TAS                                          | 2014 | 3       | 20      | 0           |
| Eluktronics          | MAG-15 2070                                      | 2020 | 2       | 58      | 0           |
| ASUSTek Computer     | N82JQ                                            | 2010 | 2       | 57      | 0           |
| MSI                  | GS65 Stealth 9SE                                 | 2019 | 2       | 56      | 0           |
| ASUSTek Computer     | N53Jq                                            | 2011 | 2       | 56      | 0           |
| MSI                  | MS-16F1                                          | 2010 | 2       | 56      | 0           |
| ASUSTek Computer     | G53JW                                            | 2010 | 2       | 56      | 0           |
| ASUSTek Computer     | N71Jq                                            | 2009 | 2       | 56      | 0           |
| Lenovo               | Legion 7 16ACHg6 82N6                            | 2021 | 2       | 53      | 0           |
| ASUSTek Computer     | ROG Zephyrus G14 GA401IV                         | 2020 | 2       | 53      | 0           |
| MSI                  | Alpha 17 A4DEK                                   | 2020 | 2       | 50      | 0           |
| Microtech            | e-tab Pro                                        | 2019 | 2       | 50      | 0           |
| Dell                 | Studio 1458                                      | 2010 | 2       | 50      | 0           |
| MSI                  | GP75 Leopard 10SFK                               | 2020 | 2       | 49      | 0           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506IV_TUF506IV             | 2020 | 2       | 49      | 0           |
| Apple                | MacBookPro13,3                                   | 2019 | 2       | 49      | 0           |
| Alienware            | x17 R1                                           | 2021 | 2       | 48      | 0           |
| Lenovo               | Legion 5 17ARH05H 82GN                           | 2020 | 2       | 48      | 0           |
| Dell                 | G7 7500                                          | 2020 | 2       | 48      | 0           |
| Acer                 | Aspire A715-41G                                  | 2020 | 2       | 48      | 0           |
| Hewlett-Packard      | OMEN by Laptop 17-cb0xxx                         | 2019 | 2       | 48      | 0           |
| Hewlett-Packard      | OMEN by Laptop 15-dh0xxx                         | 2019 | 2       | 48      | 0           |
| Jumper               | Ezbook X3                                        | 2018 | 2       | 48      | 0           |
| GEO                  | GeoBook3                                         | 2018 | 2       | 48      | 0           |
| Acer                 | Sabine Platform                                  | 2011 | 2       | 48      | 0           |
| CSL-Computer         | R Evolve C14i                                    | 2021 | 2       | 47      | 0           |
| Acer                 | Aspire A515-46                                   | 2021 | 2       | 47      | 0           |
| AVITA                | NE14A2                                           | 2021 | 2       | 47      | 0           |
| ASUSTek Computer     | ROG Strix G512LU_G512LU                          | 2021 | 2       | 47      | 0           |
| Fujitsu Siemens      | AMILO Xi 2550                                    | 2007 | 2       | 47      | 0           |
| GPU Company          | GWTN116-3                                        | 2021 | 2       | 46      | 0           |
| GPD                  | G1618-03                                         | 2021 | 2       | 46      | 0           |
| Samsung Electronics  | 850XBC                                           | 2020 | 2       | 46      | 0           |
| GPU Company          | GWTN141-4                                        | 2020 | 2       | 46      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X421FA_S433FA                | 2020 | 2       | 46      | 0           |
| MSI                  | GF65 Thin 9SE                                    | 2019 | 2       | 46      | 0           |
| Sony                 | VPCS131FM                                        | 2010 | 2       | 46      | 0           |
| Sony                 | VPCEA36FA                                        | 2010 | 2       | 46      | 0           |
| Wortmann AG          | MOBILE 1510                                      | 2009 | 2       | 46      | 0           |
| MSI                  | GX610                                            | 2007 | 2       | 46      | 0           |
| Samsung Electronics  | 950XDB/951XDB/950XDY                             | 2021 | 2       | 45      | 0           |
| Positivo Bahia - ... | VJFE43F11X-XXXXXX                                | 2021 | 2       | 45      | 0           |
| Lenovo               | ThinkPad X1 Extreme Gen 4i                       | 2021 | 2       | 45      | 0           |
| Lenovo               | XiaoXinPro 14ITL 2021 82GH                       | 2020 | 2       | 45      | 0           |
| Intel Client Systems | LAPQC71A                                         | 2020 | 2       | 45      | 0           |
| Lenovo               | Legion Y740-15IRHg 81UH                          | 2019 | 2       | 45      | 0           |
| Dell                 | Latitude 3190                                    | 2019 | 2       | 45      | 0           |
| ASUSTek Computer     | Strix GL504GS_GL504GS                            | 2019 | 2       | 45      | 0           |
| TrekStor             | Surfbook E11B                                    | 2018 | 2       | 45      | 0           |
| DEXP                 | NAVIS P100                                       | 2018 | 2       | 45      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X705FN_N705FN                | 2018 | 2       | 45      | 0           |
| Complet              | MY8306                                           | 2017 | 2       | 45      | 0           |
| Lenovo               | IdeaPad Y470 0855                                | 2011 | 2       | 45      | 0           |
| Alienware            | M18xR1                                           | 2011 | 2       | 45      | 0           |
| Acer                 | Aspire 4935                                      | 2008 | 2       | 45      | 0           |
| ASUSTek Computer     | F7Se                                             | 2008 | 2       | 45      | 0           |
| Teclast              | F15 Plus                                         | 2021 | 2       | 44      | 0           |
| Pixus                | Rise                                             | 2021 | 2       | 44      | 0           |
| Acer                 | TravelMate P214-53                               | 2021 | 2       | 44      | 0           |
| MSI                  | GL63 9SEK                                        | 2020 | 2       | 44      | 0           |
| MSI                  | GE75 Raider 10SFS                                | 2020 | 2       | 44      | 0           |
| Lenovo               | Legion Y540-15IRH 81RJ                           | 2020 | 2       | 44      | 0           |
| Lenovo               | IdeaPad Flex 3 11IGL05 82B2                      | 2020 | 2       | 44      | 0           |
| Gigabyte Technology  | AERO 15 KB                                       | 2020 | 2       | 44      | 0           |
| Dell                 | Latitude 3580                                    | 2020 | 2       | 44      | 0           |
| Acer                 | TravelMate B311-31                               | 2020 | 2       | 44      | 0           |
| MSI                  | GL65 9SEK                                        | 2019 | 2       | 44      | 0           |
| Gigabyte Technology  | B450M DS3H                                       | 2019 | 2       | 44      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412FA_A412FA                | 2019 | 2       | 44      | 0           |
| ASUSTek Computer     | ASUSPRO P5440FA_P5440FA                          | 2019 | 2       | 44      | 0           |
| Prestigio            | PSB141S01                                        | 2018 | 2       | 44      | 0           |
| Medion               | E4254 MD62100                                    | 2018 | 2       | 44      | 0           |
| MSI                  | GE62VR 6RF                                       | 2018 | 2       | 44      | 0           |
| TrekStor             | Primebook P14B                                   | 2017 | 2       | 44      | 0           |
| Toshiba              | Satellite C850D-11R                              | 2012 | 2       | 44      | 0           |
| Packard Bell         | ENLK11BZ                                         | 2011 | 2       | 44      | 0           |
| Sony                 | VPCF13E1R                                        | 2010 | 2       | 44      | 0           |
| Sony                 | VPCEB4M1E                                        | 2010 | 2       | 44      | 0           |
| Sony                 | VPCEB3Z1R                                        | 2010 | 2       | 44      | 0           |
| MSI                  | EX610                                            | 2008 | 2       | 44      | 0           |
| ASUSTek Computer     | F3Ka                                             | 2007 | 2       | 44      | 0           |
| Acer                 | Aspire 7000                                      | 2006 | 2       | 44      | 0           |
| Lenovo               | V15 G2 ALC 82KD                                  | 2021 | 2       | 43      | 0           |
| Timi                 | Mi Gaming Laptop 15.6                            | 2019 | 2       | 43      | 0           |
| Schenker             | XMG NEO                                          | 2019 | 2       | 43      | 0           |
| Positivo             | Presley                                          | 2019 | 2       | 43      | 0           |
| Lenovo               | Legion Y7000 2019 81NS                           | 2019 | 2       | 43      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X712DA_D712DA                | 2019 | 2       | 43      | 0           |
| ASUSTek Computer     | ASUSPRO P3540FB_P3540FB                          | 2019 | 2       | 43      | 0           |
| TrekStor             | Primebook P14                                    | 2017 | 2       | 43      | 0           |
| Hewlett-Packard      | ENVY Notebook 13-ab0XX                           | 2017 | 2       | 43      | 0           |
| Medion               | P6670 MD99960                                    | 2016 | 2       | 43      | 0           |
| Toshiba              | QOSMIO X70-A                                     | 2014 | 2       | 43      | 0           |
| Gigabyte Technology  | M1M3XBP-00                                       | 2014 | 2       | 43      | 0           |
| MSI                  | GX60 1AC/GX60 3AE/GX60 3BE                       | 2013 | 2       | 43      | 0           |
| Hewlett-Packard      | 435                                              | 2011 | 2       | 43      | 0           |
| Sony                 | VPCEC4C5E                                        | 2010 | 2       | 43      | 0           |
| Sony                 | VPCEB4S1R                                        | 2010 | 2       | 43      | 0           |
| Sony                 | VPCEB3E4E                                        | 2010 | 2       | 43      | 0           |
| Sony                 | VPCEB1M1R                                        | 2010 | 2       | 43      | 0           |
| Sony                 | VPCCW2S1R                                        | 2010 | 2       | 43      | 0           |
| ASUSTek Computer     | G50V                                             | 2009 | 2       | 43      | 0           |
| Sony                 | VGN-AW11M_H                                      | 2008 | 2       | 43      | 0           |
| MSI                  | ER710                                            | 2007 | 2       | 43      | 0           |
| Maibenben            | E5100                                            | 2020 | 2       | 42      | 0           |
| MSI                  | GF63 Thin 10SCXR                                 | 2020 | 2       | 42      | 0           |
| Dell                 | Vostro 3405                                      | 2020 | 2       | 42      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E410MA_R429MA               | 2020 | 2       | 42      | 0           |
| Lenovo               | Legion Y7000 81FW                                | 2019 | 2       | 42      | 0           |
| Hewlett-Packard      | OMEN by Laptop 17-an0xx                          | 2019 | 2       | 42      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X571GT_X571GT                | 2019 | 2       | 42      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E406MAS_L406MA              | 2019 | 2       | 42      | 0           |
| Avell High Perfor... | G1750/C65 Fox                                    | 2018 | 2       | 42      | 0           |
| ASUSTek Computer     | P751JA                                           | 2015 | 2       | 42      | 0           |
| MSI                  | GT60 2OC/2OD                                     | 2013 | 2       | 42      | 0           |
| ASUSTek Computer     | N56DY                                            | 2013 | 2       | 42      | 0           |
| Sony                 | VPCEL2S1E                                        | 2012 | 2       | 42      | 0           |
| Gateway              | NE71B                                            | 2012 | 2       | 42      | 0           |
| ASUSTek Computer     | U41JF                                            | 2011 | 2       | 42      | 0           |
| Sony                 | VPCS111FM                                        | 2010 | 2       | 42      | 0           |
| Sony                 | VPCEB3B4R                                        | 2010 | 2       | 42      | 0           |
| Sony                 | VPCEA45FG                                        | 2010 | 2       | 42      | 0           |
| MSI                  | FX610                                            | 2010 | 2       | 42      | 0           |
| ASUSTek Computer     | U53Jc                                            | 2010 | 2       | 42      | 0           |
| Lenovo               | 3000 G430 4153/200                               | 2009 | 2       | 42      | 0           |
| Sony                 | VGN-AR71S                                        | 2008 | 2       | 42      | 0           |
| Sony                 | VGN-NR11SR_S                                     | 2007 | 2       | 42      | 0           |
| Lenovo               | IdeaPad Y510                                     | 2007 | 2       | 42      | 0           |
| Fujitsu Siemens      | ESPRIMO Mobile V5545                             | 2007 | 2       | 42      | 0           |
| Dell                 | XPS MXC062                                       | 2007 | 2       | 42      | 0           |
| Lenovo               | V14 G2 ITL 82KA                                  | 2021 | 2       | 41      | 0           |
| LG Electronics       | 17Z90P-G.AD88B                                   | 2021 | 2       | 41      | 0           |
| Hewlett-Packard      | ENVY Laptop 14-eb0xxx                            | 2021 | 2       | 41      | 0           |
| Chuwi                | HeroBook Air                                     | 2021 | 2       | 41      | 0           |
| ASUSTek Computer     | ZenBook UX425UG_Q408UG                           | 2021 | 2       | 41      | 0           |
| ASUSTek Computer     | ASUS EXPERTBOOK B1400CEPEY_B1400CEPE             | 2021 | 2       | 41      | 0           |
| PC Specialist        | NH5x_7xDCx_DDx                                   | 2020 | 2       | 41      | 0           |
| Notebook             | NHx0DB,DE                                        | 2020 | 2       | 41      | 0           |
| Hewlett-Packard      | Pavilion Laptop 15z-eh000                        | 2020 | 2       | 41      | 0           |
| Hewlett-Packard      | Laptop 15s-gr0xxx                                | 2020 | 2       | 41      | 0           |
| ASUSTek Computer     | ROG Zephyrus S17 GX701LWS_GX701LWS               | 2020 | 2       | 41      | 0           |
| ASUSTek Computer     | ROG Strix G712LW_G712LW                          | 2020 | 2       | 41      | 0           |
| SLIMBOOK             | PROX15                                           | 2019 | 2       | 41      | 0           |
| Dell                 | Inspiron 3782                                    | 2019 | 2       | 41      | 0           |
| Avell High Perfor... | Avell G1750 MUV / C65 MUV                        | 2019 | 2       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X411QA_X411QA                | 2019 | 2       | 41      | 0           |
| ASUSTek Computer     | TUF Gaming FX705DD_FX705DD                       | 2019 | 2       | 41      | 0           |
| Acer                 | Nitro AN515-41                                   | 2018 | 2       | 41      | 0           |
| ASUSTek Computer     | ZenBook UX433FN_U4300FN                          | 2018 | 2       | 41      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop X505ZA_R504ZA               | 2018 | 2       | 41      | 0           |
| MSI                  | GT75VR 7RF                                       | 2017 | 2       | 41      | 0           |
| MSI                  | GP72MVR 7RFX                                     | 2017 | 2       | 41      | 0           |
| ASUSTek Computer     | G752VSK                                          | 2017 | 2       | 41      | 0           |
| MSI                  | GE72 2QF                                         | 2016 | 2       | 41      | 0           |
| ASUSTek Computer     | GL502VY                                          | 2016 | 2       | 41      | 0           |
| System76             | Kudu Professional                                | 2014 | 2       | 41      | 0           |
| ASUSTek Computer     | X555LPB                                          | 2014 | 2       | 41      | 0           |
| Packard Bell         | ENLE11BZ                                         | 2012 | 2       | 41      | 0           |
| ASUSTek Computer     | N56DP                                            | 2012 | 2       | 41      | 0           |
| Medion               | E7216                                            | 2011 | 2       | 41      | 0           |
| Acer                 | Aspire 7739ZG                                    | 2011 | 2       | 41      | 0           |
| Acer                 | Aspire 7739G                                     | 2011 | 2       | 41      | 0           |
| Sony                 | VPCEB3E1R                                        | 2010 | 2       | 41      | 0           |
| Sony                 | VPCCW2S8E                                        | 2010 | 2       | 41      | 0           |
| Pegatron             | H36ST                                            | 2010 | 2       | 41      | 0           |
| Packard Bell         | EN Butterfly m                                   | 2010 | 2       | 41      | 0           |
| MSI                  | MS-1675                                          | 2010 | 2       | 41      | 0           |
| Sony                 | VGN-FW510F                                       | 2009 | 2       | 41      | 0           |
| Lenovo               | IdeaPad Y550 4186                                | 2009 | 2       | 41      | 0           |
| Lenovo               | G450 20022                                       | 2009 | 2       | 41      | 0           |
| Fujitsu Siemens      | AMILO Si 2636                                    | 2009 | 2       | 41      | 0           |
| ASUSTek Computer     | G50VT                                            | 2009 | 2       | 41      | 0           |
| Sony                 | VGN-NS31ER_S                                     | 2008 | 2       | 41      | 0           |
| Sony                 | VGN-FW235J                                       | 2008 | 2       | 41      | 0           |
| Sony                 | VGN-FW21Z                                        | 2008 | 2       | 41      | 0           |
| Lenovo               | 3000 N500 423338G                                | 2008 | 2       | 41      | 0           |
| Acer                 | Aspire 7730Z                                     | 2008 | 2       | 41      | 0           |
| Acer                 | Aspire 7720G                                     | 2008 | 2       | 41      | 0           |
| Dell                 | Inspiron 5415                                    | 2021 | 2       | 40      | 0           |
| Dell                 | Inspiron 13 5310                                 | 2021 | 2       | 40      | 0           |
| Avell High Perfor... | A70 LIV                                          | 2021 | 2       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X521EA_S533EA                | 2021 | 2       | 40      | 0           |
| MSI                  | GL75 Leopard 10SCSR                              | 2020 | 2       | 40      | 0           |
| Lenovo               | IdeaPad S340-15IIL 81WL                          | 2020 | 2       | 40      | 0           |
| Dell                 | Inspiron 7391                                    | 2020 | 2       | 40      | 0           |
| Dell                 | Inspiron 5405                                    | 2020 | 2       | 40      | 0           |
| Dell                 | Inspiron 3502                                    | 2020 | 2       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509DAP_P1510CDA             | 2020 | 2       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509DAP_M509DA               | 2020 | 2       | 40      | 0           |
| ASUSTek Computer     | TUF Gaming FX506LU_FX506LU                       | 2020 | 2       | 40      | 0           |
| Notebook             | L140CU                                           | 2019 | 2       | 40      | 0           |
| Lenovo               | Legion Y7000P 81LD                               | 2019 | 2       | 40      | 0           |
| Avell High Perfor... | A75 RTX MUV/ G1575 RTX MUV                       | 2019 | 2       | 40      | 0           |
| ASUSTek Computer     | ZenBook UX434FLC_UX433FLC                        | 2019 | 2       | 40      | 0           |
| ASUSTek Computer     | X751YI                                           | 2019 | 2       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FL_A512FL                | 2019 | 2       | 40      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FL_R521FL                | 2019 | 2       | 40      | 0           |
| ASUSTek Computer     | ASUSPRO P2540FB_P2540FB                          | 2019 | 2       | 40      | 0           |
| MSI                  | GE72 6QD                                         | 2016 | 2       | 40      | 0           |
| Acer                 | Predator G5-793                                  | 2016 | 2       | 40      | 0           |
| MSI                  | GS70 2PC Stealth                                 | 2015 | 2       | 40      | 0           |
| ASUSTek Computer     | X555LNB                                          | 2014 | 2       | 40      | 0           |
| Toshiba              | Satellite P50t-A                                 | 2013 | 2       | 40      | 0           |
| Philco               | 14M-W549                                         | 2013 | 2       | 40      | 0           |
| Toshiba              | Satellite L850D-BNK                              | 2012 | 2       | 40      | 0           |
| MSI                  | U270DX                                           | 2012 | 2       | 40      | 0           |
| Lenovo               | IdeaPad N585                                     | 2012 | 2       | 40      | 0           |
| Sony                 | VPCSB3M1R                                        | 2011 | 2       | 40      | 0           |
| Sony                 | VPCEB490X                                        | 2011 | 2       | 40      | 0           |
| Lenovo               | IdeaPad Z460 20059                               | 2011 | 2       | 40      | 0           |
| ASUSTek Computer     | B121                                             | 2011 | 2       | 40      | 0           |
| Toshiba              | Satellite L630                                   | 2010 | 2       | 40      | 0           |
| Sony                 | VPCEB11FM                                        | 2010 | 2       | 40      | 0           |
| Sony                 | VPCEA36FM                                        | 2010 | 2       | 40      | 0           |
| Hewlett-Packard      | Compaq Presario CQ42                             | 2010 | 2       | 40      | 0           |
| Dell                 | XPS L401X                                        | 2010 | 2       | 40      | 0           |
| Sony                 | VGN-NW320F                                       | 2009 | 2       | 40      | 0           |
| ASUSTek Computer     | N51Vg                                            | 2009 | 2       | 40      | 0           |
| Sony                 | VGN-NR310FH                                      | 2008 | 2       | 40      | 0           |
| Sony                 | VGN-FW270J                                       | 2008 | 2       | 40      | 0           |
| ASUSTek Computer     | X71A                                             | 2008 | 2       | 40      | 0           |
| ASUSTek Computer     | F7L                                              | 2008 | 2       | 40      | 0           |
| Acer                 | Aspire 9920                                      | 2007 | 2       | 40      | 0           |
| Timi                 | RedmiBook Pro 14S                                | 2021 | 2       | 39      | 0           |
| Google               | Kohaku                                           | 2021 | 2       | 39      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X515JA_F515JA                | 2021 | 2       | 39      | 0           |
| Notebook             | NV4XMZ                                           | 2020 | 2       | 39      | 0           |
| Notebook             | NL4x_NL5xLU                                      | 2020 | 2       | 39      | 0           |
| MSI                  | GL75 Leopard 10SER                               | 2020 | 2       | 39      | 0           |
| Google               | Pantheon                                         | 2020 | 2       | 39      | 0           |
| Google               | Ekko                                             | 2020 | 2       | 39      | 0           |
| Dell                 | Vostro 3481                                      | 2020 | 2       | 39      | 0           |
| Daten Tecnologia     | ESTELAR                                          | 2020 | 2       | 39      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X513EA_X513EA                | 2020 | 2       | 39      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512JP_X512JP                | 2020 | 2       | 39      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FJC_X512FJC              | 2020 | 2       | 39      | 0           |
| Multilaser           | PC150                                            | 2019 | 2       | 39      | 0           |
| MSI                  | GE65 Raider 9SE                                  | 2019 | 2       | 39      | 0           |
| Lenovo               | IdeaPad S340-15IWLTouch 81QF                     | 2019 | 2       | 39      | 0           |
| Acer                 | Aspire A314-21                                   | 2019 | 2       | 39      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X712FA_F712FA                | 2019 | 2       | 39      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X545FA_X545FA                | 2019 | 2       | 39      | 0           |
| ASUSTek Computer     | GL702VM                                          | 2019 | 2       | 39      | 0           |
| Acer                 | Predator PH517-51                                | 2018 | 2       | 39      | 0           |
| ASUSTek Computer     | Strix GL504GS                                    | 2018 | 2       | 39      | 0           |
| TUXEDO               | N130BU                                           | 2017 | 2       | 39      | 0           |
| Notebook             | N85_N870HL                                       | 2017 | 2       | 39      | 0           |
| MSI                  | MS-7A38                                          | 2017 | 2       | 39      | 0           |
| ASUSTek Computer     | K401UB                                           | 2016 | 2       | 39      | 0           |
| Hewlett-Packard      | Beats 15 Notebook PC                             | 2015 | 2       | 39      | 0           |
| Acer                 | Aspire E5-522G                                   | 2015 | 2       | 39      | 0           |
| Toshiba              | Satellite P50-B                                  | 2014 | 2       | 39      | 0           |
| Acer                 | Aspire V3-572PG                                  | 2014 | 2       | 39      | 0           |
| Medion               | AKOYA E1317T                                     | 2013 | 2       | 39      | 0           |
| Gateway              | NE722                                            | 2013 | 2       | 39      | 0           |
| Acer                 | Aspire E1-451G                                   | 2013 | 2       | 39      | 0           |
| Toshiba              | Satellite L850D-BJS                              | 2012 | 2       | 39      | 0           |
| ASUSTek Computer     | U38DT                                            | 2012 | 2       | 39      | 0           |
| eMachines            | eME443                                           | 2011 | 2       | 39      | 0           |
| Philco               | 14F                                              | 2011 | 2       | 39      | 0           |
| Medion               | P6812                                            | 2011 | 2       | 39      | 0           |
| LG Electronics       | C400-G.BC31P1                                    | 2011 | 2       | 39      | 0           |
| Sony                 | VPCEB3Z1E                                        | 2010 | 2       | 39      | 0           |
| Sony                 | VPCEB3PGX                                        | 2010 | 2       | 39      | 0           |
| Sony                 | VPCEA20FB                                        | 2010 | 2       | 39      | 0           |
| Lenovo               | IdeaPad Z560 0914                                | 2010 | 2       | 39      | 0           |
| Dell                 | Inspiron N4020                                   | 2010 | 2       | 39      | 0           |
| Dell                 | Inspiron M301Z                                   | 2010 | 2       | 39      | 0           |
| Dell                 | Inspiron 1121                                    | 2010 | 2       | 39      | 0           |
| Acer                 | Aspire 4553                                      | 2010 | 2       | 39      | 0           |
| ASUSTek Computer     | U30Jc                                            | 2010 | 2       | 39      | 0           |
| Acer                 | Aspire 7715Z                                     | 2009 | 2       | 39      | 0           |
| Acer                 | Aspire 6530                                      | 2009 | 2       | 39      | 0           |
| Acer                 | Aspire 4736                                      | 2009 | 2       | 39      | 0           |
| ASUSTek Computer     | G60VX                                            | 2009 | 2       | 39      | 0           |
| Dell                 | Vostro A840                                      | 2008 | 2       | 39      | 0           |
| ASUSTek Computer     | X71Q                                             | 2008 | 2       | 39      | 0           |
| Sony                 | VGN-NR21MR_S                                     | 2007 | 2       | 39      | 0           |
| Sony                 | VGN-FZ31S                                        | 2007 | 2       | 39      | 0           |
| BenQ                 | Joybook S41                                      | 2007 | 2       | 39      | 0           |
| ASUSTek Computer     | F3L                                              | 2007 | 2       | 39      | 0           |
| Sony                 | VGN-SZ1HRP_B                                     | 2006 | 2       | 39      | 0           |
| MSI                  | GF63 Thin 10SC                                   | 2021 | 2       | 38      | 0           |
| Lenovo               | V14-ARE 82DQ                                     | 2020 | 2       | 38      | 0           |
| ASUSTek Computer     | TUF Gaming FX505GT_FX505GT                       | 2020 | 2       | 38      | 0           |
| Hewlett-Packard      | Laptop 15s-dr1xxx                                | 2019 | 2       | 38      | 0           |
| Hewlett-Packard      | Laptop 14-cf1xxx                                 | 2019 | 2       | 38      | 0           |
| Dell                 | Latitude 3379                                    | 2019 | 2       | 38      | 0           |
| Acer                 | Aspire A115-31                                   | 2019 | 2       | 38      | 0           |
| ASUSTek Computer     | X756UB                                           | 2019 | 2       | 38      | 0           |
| ASUSTek Computer     | ROG Strix G731GV_G731GV                          | 2019 | 2       | 38      | 0           |
| Hewlett-Packard      | Laptop 15q-bu1xx                                 | 2018 | 2       | 38      | 0           |
| Dell                 | Inspiron 3476                                    | 2018 | 2       | 38      | 0           |
| ASUSTek Computer     | ZenBook Pro 15 UX550GE_UX550GE                   | 2018 | 2       | 38      | 0           |
| ASUSTek Computer     | ZenBook 13 UX331FAL_UX331FAL                     | 2018 | 2       | 38      | 0           |
| MSI                  | GS73VR 7RG                                       | 2017 | 2       | 38      | 0           |
| ASUSTek Computer     | X580VN                                           | 2017 | 2       | 38      | 0           |
| Hewlett-Packard      | Spectre Pro x360 G2                              | 2016 | 2       | 38      | 0           |
| MSI                  | GT80 2QE                                         | 2015 | 2       | 38      | 0           |
| MSI                  | GT72 6QD                                         | 2015 | 2       | 38      | 0           |
| ASUSTek Computer     | Q552UB                                           | 2015 | 2       | 38      | 0           |
| Hewlett-Packard      | mt41                                             | 2014 | 2       | 38      | 0           |
| DNS                  | NH5KS02                                          | 2014 | 2       | 38      | 0           |
| Toshiba              | Satellite L75D-A                                 | 2013 | 2       | 38      | 0           |
| Sony                 | SVF1521Q1RW                                      | 2013 | 2       | 38      | 0           |
| Medion               | Akoya P7627                                      | 2013 | 2       | 38      | 0           |
| Toshiba              | Satellite L850-CJK                               | 2012 | 2       | 38      | 0           |
| Toshiba              | Satellite C870D-11T                              | 2012 | 2       | 38      | 0           |
| Toshiba              | Satellite C870D-119                              | 2012 | 2       | 38      | 0           |
| Sony                 | SVE14A35CXH                                      | 2012 | 2       | 38      | 0           |
| eMachines            | eME644                                           | 2011 | 2       | 38      | 0           |
| Medion               | X781X                                            | 2011 | 2       | 38      | 0           |
| Hewlett-Packard      | 420                                              | 2011 | 2       | 38      | 0           |
| Acer                 | ICONIA Tab W500                                  | 2011 | 2       | 38      | 0           |
| Acer                 | Aspire 3830G                                     | 2011 | 2       | 38      | 0           |
| ASUSTek Computer     | VX6                                              | 2011 | 2       | 38      | 0           |
| eMachines            | E727                                             | 2010 | 2       | 38      | 0           |
| Toshiba              | Satellite T215D                                  | 2010 | 2       | 38      | 0           |
| Sony                 | VPCYB1S1E                                        | 2010 | 2       | 38      | 0           |
| Sony                 | VPCEA33FB                                        | 2010 | 2       | 38      | 0           |
| Samsung Electronics  | R440/R480                                        | 2010 | 2       | 38      | 0           |
| Packard Bell         | EasyNote LM85                                    | 2010 | 2       | 38      | 0           |
| Packard Bell         | EasyNote LM81                                    | 2010 | 2       | 38      | 0           |
| Medion               | P6622                                            | 2010 | 2       | 38      | 0           |
| Acer                 | Aspire 5745DG                                    | 2010 | 2       | 38      | 0           |
| Acer                 | Aspire 5740D                                     | 2010 | 2       | 38      | 0           |
| Sony                 | VGN-NR11Z_T                                      | 2008 | 2       | 38      | 0           |
| Packard Bell         | EasyNote TN65                                    | 2008 | 2       | 38      | 0           |
| Medion               | P6613                                            | 2008 | 2       | 38      | 0           |
| Dell                 | Inspiron 1318                                    | 2008 | 2       | 38      | 0           |
| Sony                 | VGN-FZ18M                                        | 2007 | 2       | 38      | 0           |
| Sony                 | VGN-AR790U                                       | 2007 | 2       | 38      | 0           |
| Hewlett-Packard      | Presario F500                                    | 2007 | 2       | 38      | 0           |
| Acer                 | TravelMate 7720                                  | 2007 | 2       | 38      | 0           |
| ASUSTek Computer     | T12Eg                                            | 2007 | 2       | 38      | 0           |
| ASUSTek Computer     | F3U                                              | 2007 | 2       | 38      | 0           |
| Sony                 | VGN-N11M_W                                       | 2006 | 2       | 38      | 0           |
| Samsung Electronics  | R55S                                             | 2006 | 2       | 38      | 0           |
| Lenovo               | 3000 N100 0768BNG                                | 2006 | 2       | 38      | 0           |
| ICL                  | RAYbook Si1514                                   | 2021 | 2       | 37      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X513EA_F513EA                | 2021 | 2       | 37      | 0           |
| MSI                  | GL65 Leopard 9SCXR                               | 2020 | 2       | 37      | 0           |
| LG Electronics       | 15Z95N-G.AA78B                                   | 2020 | 2       | 37      | 0           |
| TUXEDO               | InfinityBook Pro 15 v4                           | 2019 | 2       | 37      | 0           |
| Dell                 | Vostro 3490                                      | 2019 | 2       | 37      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X510QA_A510QA                | 2019 | 2       | 37      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E406MAS_R420MA              | 2019 | 2       | 37      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E406MAS_E406MA              | 2019 | 2       | 37      | 0           |
| Fujitsu              | LIFEBOOK E558                                    | 2018 | 2       | 37      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop X510UFO                     | 2018 | 2       | 37      | 0           |
| MSI                  | GE62MVR 7RG                                      | 2017 | 2       | 37      | 0           |
| Gigabyte Technology  | P35V3                                            | 2016 | 2       | 37      | 0           |
| Toshiba              | Satellite Radius 12 P20W-C-106                   | 2015 | 2       | 37      | 0           |
| ASUSTek Computer     | P751JF                                           | 2015 | 2       | 37      | 0           |
| Toshiba              | Satellite P50-B-116                              | 2014 | 2       | 37      | 0           |
| Toshiba              | Satellite L50-A-19N                              | 2014 | 2       | 37      | 0           |
| MSI                  | GS60 2PC Ghost                                   | 2014 | 2       | 37      | 0           |
| ASUSTek Computer     | G771JM                                           | 2014 | 2       | 37      | 0           |
| speedmaster          | E131x series                                     | 2013 | 2       | 37      | 0           |
| Toshiba              | Satellite C55Dt-A                                | 2013 | 2       | 37      | 0           |
| Toshiba              | Satellite C50D-A-133                             | 2013 | 2       | 37      | 0           |
| Dell                 | Inspiron 5535                                    | 2013 | 2       | 37      | 0           |
| Toshiba              | Satellite S855D                                  | 2012 | 2       | 37      | 0           |
| Toshiba              | Satellite P745                                   | 2012 | 2       | 37      | 0           |
| Toshiba              | Satellite C845D                                  | 2012 | 2       | 37      | 0           |
| Sony                 | SVE1712S1RB                                      | 2012 | 2       | 37      | 0           |
| Lenovo               | IdeaPad Y560p                                    | 2012 | 2       | 37      | 0           |
| Lenovo               | B575e 36852EG                                    | 2012 | 2       | 37      | 0           |
| Toshiba              | Satellite C670-12K                               | 2011 | 2       | 37      | 0           |
| Sony                 | VPCYB35AB                                        | 2011 | 2       | 37      | 0           |
| Lenovo               | IdeaPad S205 10382EG                             | 2011 | 2       | 37      | 0           |
| Lenovo               | G460 0677                                        | 2011 | 2       | 37      | 0           |
| Clevo                | W2xxHSQ                                          | 2011 | 2       | 37      | 0           |
| Samsung Electronics  | Q320/P320                                        | 2010 | 2       | 37      | 0           |
| Lenovo               | B550 0880                                        | 2010 | 2       | 37      | 0           |
| Dell                 | Vostro V13                                       | 2010 | 2       | 37      | 0           |
| Acer                 | TravelMate 5742ZG                                | 2010 | 2       | 37      | 0           |
| Acer                 | TravelMate 5740                                  | 2010 | 2       | 37      | 0           |
| ASUSTek Computer     | K40AF                                            | 2010 | 2       | 37      | 0           |
| ASUSTek Computer     | F83VD                                            | 2010 | 2       | 37      | 0           |
| eMachines            | G525                                             | 2009 | 2       | 37      | 0           |
| Sony                 | VGN-NW270F                                       | 2009 | 2       | 37      | 0           |
| Panasonic            | CF-19CHB23BE                                     | 2009 | 2       | 37      | 0           |
| Packard Bell         | EasyNote TN36                                    | 2009 | 2       | 37      | 0           |
| Packard Bell         | EasyNote LJ71                                    | 2009 | 2       | 37      | 0           |
| Packard Bell         | ENBFXS                                           | 2009 | 2       | 37      | 0           |
| Lenovo               | 3000 G430                                        | 2009 | 2       | 37      | 0           |
| ASUSTek Computer     | K51AC                                            | 2009 | 2       | 37      | 0           |
| eMachines            | D520                                             | 2008 | 2       | 37      | 0           |
| Medion               | WIM2120                                          | 2007 | 2       | 37      | 0           |
| Lenovo               | 3000 N200 0769ALU                                | 2007 | 2       | 37      | 0           |
| Fujitsu Siemens      | AMILO Xi 2428                                    | 2007 | 2       | 37      | 0           |
| Acer                 | Aspire 4720G                                     | 2007 | 2       | 37      | 0           |
| Fujitsu Siemens      | AMILO Pro V3205                                  | 2006 | 2       | 37      | 0           |
| LG Electronics       | 17Z90P-K.AAC8U1                                  | 2021 | 2       | 36      | 0           |
| Hewlett-Packard      | Laptop 14s-cf3xxx                                | 2020 | 2       | 36      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509JP_X509JP                | 2020 | 2       | 36      | 0           |
| MSI                  | GL63 9SDK                                        | 2019 | 2       | 36      | 0           |
| LG Electronics       | 15Z990-A.AAS7U1                                  | 2019 | 2       | 36      | 0           |
| LG Electronics       | 14Z980-G.BH51P1                                  | 2019 | 2       | 36      | 0           |
| ASUSTek Computer     | X556UR                                           | 2019 | 2       | 36      | 0           |
| ASUSTek Computer     | X455YA                                           | 2019 | 2       | 36      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412UA                       | 2019 | 2       | 36      | 0           |
| ASUSTek Computer     | VivoBook 17_ASUS Laptop X705MA_A705MA            | 2019 | 2       | 36      | 0           |
| MSI                  | GS60 6QD                                         | 2018 | 2       | 36      | 0           |
| MSI                  | GP72 7RE                                         | 2018 | 2       | 36      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540BP                   | 2018 | 2       | 36      | 0           |
| ASUSTek Computer     | X756UV                                           | 2017 | 2       | 36      | 0           |
| ASUSTek Computer     | X411UNV                                          | 2017 | 2       | 36      | 0           |
| Notebook             | N650DU                                           | 2016 | 2       | 36      | 0           |
| Notebook             | N24_25BU                                         | 2016 | 2       | 36      | 0           |
| Lenovo               | IdeaPad 110-17ACL 80UM                           | 2016 | 2       | 36      | 0           |
| ASUSTek Computer     | X556UV                                           | 2016 | 2       | 36      | 0           |
| ASUSTek Computer     | X540UP                                           | 2016 | 2       | 36      | 0           |
| ASUSTek Computer     | UX310UQK                                         | 2016 | 2       | 36      | 0           |
| Lenovo               | ThinkPad E450c                                   | 2015 | 2       | 36      | 0           |
| Acer                 | TravelMate P277-MG                               | 2015 | 2       | 36      | 0           |
| Acer                 | Aspire ES1-420                                   | 2015 | 2       | 36      | 0           |
| Toshiba              | Satellite C55Dt-B                                | 2014 | 2       | 36      | 0           |
| MSI                  | GS60 2QE                                         | 2014 | 2       | 36      | 0           |
| Getac                | V110                                             | 2014 | 2       | 36      | 0           |
| Dell                 | XPS 11 9P33                                      | 2014 | 2       | 36      | 0           |
| DNS                  | NH5KB11                                          | 2014 | 2       | 36      | 0           |
| Toshiba              | Satellite S75Dt-A                                | 2013 | 2       | 36      | 0           |
| Toshiba              | Satellite C50-A-19U                              | 2013 | 2       | 36      | 0           |
| Sony                 | SVF1521D1RB                                      | 2013 | 2       | 36      | 0           |
| Lenovo               | IdeaPad S215 20258                               | 2013 | 2       | 36      | 0           |
| Lenovo               | G405                                             | 2013 | 2       | 36      | 0           |
| Gateway              | NE-522                                           | 2013 | 2       | 36      | 0           |
| Exo                  | C14C                                             | 2013 | 2       | 36      | 0           |
| ASUSTek Computer     | Q501LA                                           | 2013 | 2       | 36      | 0           |
| Sony                 | VPCEH2M1R                                        | 2012 | 2       | 36      | 0           |
| Sony                 | SVT1313X9RS                                      | 2012 | 2       | 36      | 0           |
| Sony                 | SVE1713A1EW                                      | 2012 | 2       | 36      | 0           |
| Sony                 | SVE1711V1RB                                      | 2012 | 2       | 36      | 0           |
| Sony                 | SVE1512N1RW                                      | 2012 | 2       | 36      | 0           |
| Quanta               | JW2                                              | 2012 | 2       | 36      | 0           |
| Lenovo               | IdeaPad S206 2638                                | 2012 | 2       | 36      | 0           |
| ASUSTek Computer     | VX6S                                             | 2012 | 2       | 36      | 0           |
| Toshiba              | Satellite C600                                   | 2011 | 2       | 36      | 0           |
| OEGStone             | C4100/C5100                                      | 2011 | 2       | 36      | 0           |
| MSI                  | GX780R/GT780R/GT780DXR/GT783R                    | 2011 | 2       | 36      | 0           |
| MSI                  | GX780/GT780/GT780DX/GT783                        | 2011 | 2       | 36      | 0           |
| LG Electronics       | A410-G.BC48P1                                    | 2011 | 2       | 36      | 0           |
| ASUSTek Computer     | K72JT                                            | 2011 | 2       | 36      | 0           |
| ASUSTek Computer     | 1215T                                            | 2011 | 2       | 36      | 0           |
| eMachines            | eME528                                           | 2010 | 2       | 36      | 0           |
| eMachines            | eMD732                                           | 2010 | 2       | 36      | 0           |
| Toshiba              | Satellite Pro L670                               | 2010 | 2       | 36      | 0           |
| Lenovo               | ThinkPad X201s                                   | 2010 | 2       | 36      | 0           |
| Acer                 | TravelMate 5740G                                 | 2010 | 2       | 36      | 0           |
| Acer                 | Aspire 7552                                      | 2010 | 2       | 36      | 0           |
| Acer                 | Aspire 5551                                      | 2010 | 2       | 36      | 0           |
| Acer                 | Aspire 4738Z                                     | 2010 | 2       | 36      | 0           |
| Acer                 | Aspire 4738                                      | 2010 | 2       | 36      | 0           |
| Samsung Electronics  | R720                                             | 2009 | 2       | 36      | 0           |
| Samsung Electronics  | R410                                             | 2009 | 2       | 36      | 0           |
| LG Electronics       | R460-L.BG21P1                                    | 2009 | 2       | 36      | 0           |
| Irbis                | A2101                                            | 2009 | 2       | 36      | 0           |
| ASUSTek Computer     | P81IJ                                            | 2009 | 2       | 36      | 0           |
| Toshiba              | Satellite L305D                                  | 2008 | 2       | 36      | 0           |
| Packard Bell         | EasyNote_BG46-T-001RU                            | 2008 | 2       | 36      | 0           |
| Fujitsu Siemens      | AMILO Li 2735                                    | 2008 | 2       | 36      | 0           |
| Acer                 | Extensa 5210                                     | 2007 | 2       | 36      | 0           |
| Acer                 | TravelMate 2480                                  | 2006 | 2       | 36      | 0           |
| Samsung Electronics  | SX20S                                            | 2005 | 2       | 36      | 0           |
| Timi                 | Mi NoteBook Ultra                                | 2021 | 2       | 35      | 0           |
| MSI                  | Modern 14 B10RBSW                                | 2020 | 2       | 35      | 0           |
| MSI                  | GF75 Thin 10SCSR                                 | 2020 | 2       | 35      | 0           |
| Chuwi                | CoreBook Pro                                     | 2020 | 2       | 35      | 0           |
| Apple                | MacBookPro13,1                                   | 2020 | 2       | 35      | 0           |
| Acer                 | TravelMate P215-52                               | 2020 | 2       | 35      | 0           |
| Schenker             | SCHENKER_SLIM_SERIES_L18                         | 2019 | 2       | 35      | 0           |
| Fujitsu              | LIFEBOOK E459                                    | 2019 | 2       | 35      | 0           |
| Dell                 | Inspiron 15-7569                                 | 2019 | 2       | 35      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X430FA                       | 2019 | 2       | 35      | 0           |
| Lenovo               | IdeaPad 330-14IKB 81G2                           | 2018 | 2       | 35      | 0           |
| AVITA                | NS14A1US                                         | 2018 | 2       | 35      | 0           |
| ASUSTek Computer     | VivoBook 14_ASUS Laptop X407MA_X407MA            | 2018 | 2       | 35      | 0           |
| MSI                  | GV62 7RD                                         | 2017 | 2       | 35      | 0           |
| Dell                 | Inspiron 3459                                    | 2017 | 2       | 35      | 0           |
| Dell                 | Inspiron 15-3555                                 | 2017 | 2       | 35      | 0           |
| ASUSTek Computer     | X441UVK                                          | 2017 | 2       | 35      | 0           |
| ASUSTek Computer     | VivoBook E14 E402WA                              | 2017 | 2       | 35      | 0           |
| Lenovo               | Yoga 510-14ISK 80UK                              | 2016 | 2       | 35      | 0           |
| Toshiba              | Satellite E45W-C                                 | 2015 | 2       | 35      | 0           |
| Toshiba              | Satellite C75D-C                                 | 2015 | 2       | 35      | 0           |
| MSI                  | GE62 6QF                                         | 2015 | 2       | 35      | 0           |
| Acer                 | Aspire R7-371T                                   | 2015 | 2       | 35      | 0           |
| Toshiba              | Satellite Radius P55W-B                          | 2014 | 2       | 35      | 0           |
| Toshiba              | Satellite M50D-A                                 | 2013 | 2       | 35      | 0           |
| Toshiba              | Satellite C50D-A-12R                             | 2013 | 2       | 35      | 0           |
| Toshiba              | Satellite C40D-A                                 | 2013 | 2       | 35      | 0           |
| Sony                 | SVF1521A4RW                                      | 2013 | 2       | 35      | 0           |
| Semp Toshiba         | STI NA 1402                                      | 2013 | 2       | 35      | 0           |
| Pegatron             | B34C                                             | 2013 | 2       | 35      | 0           |
| Notebook             | W230ST                                           | 2013 | 2       | 35      | 0           |
| Lenovo               | G405 20239                                       | 2013 | 2       | 35      | 0           |
| Acer                 | Aspire V5-572                                    | 2013 | 2       | 35      | 0           |
| Acer                 | Aspire E1-530G                                   | 2013 | 2       | 35      | 0           |
| Toshiba              | Satellite L850-150                               | 2012 | 2       | 35      | 0           |
| Sony                 | VPCSB1S1E                                        | 2012 | 2       | 35      | 0           |
| Sony                 | VPCEJ1M1R                                        | 2012 | 2       | 35      | 0           |
| Acer                 | Aspire V3-531G                                   | 2012 | 2       | 35      | 0           |
| eMachines            | eME529                                           | 2011 | 2       | 35      | 0           |
| Sony                 | VPCF22KFX                                        | 2011 | 2       | 35      | 0           |
| Sony                 | VPCEJ1L1E                                        | 2011 | 2       | 35      | 0           |
| Sony                 | VPCCB17FG                                        | 2011 | 2       | 35      | 0           |
| Dell                 | System Inspiron N411Z                            | 2011 | 2       | 35      | 0           |
| Acer                 | Aspire 3830T                                     | 2011 | 2       | 35      | 0           |
| ASUSTek Computer     | G53SW                                            | 2011 | 2       | 35      | 0           |
| Toshiba              | Satellite L645D                                  | 2010 | 2       | 35      | 0           |
| Sony                 | VPCYB15AB                                        | 2010 | 2       | 35      | 0           |
| Sony                 | VPCEE2E1E                                        | 2010 | 2       | 35      | 0           |
| MSI                  | MS-1454                                          | 2010 | 2       | 35      | 0           |
| MSI                  | MS-1244                                          | 2010 | 2       | 35      | 0           |
| Lenovo               | IdeaPad U260 20067                               | 2010 | 2       | 35      | 0           |
| Acer                 | Aspire 1551                                      | 2010 | 2       | 35      | 0           |
| ASUSTek Computer     | U52F                                             | 2010 | 2       | 35      | 0           |
| Toshiba              | Satellite Pro S300L                              | 2009 | 2       | 35      | 0           |
| Toshiba              | Satellite L355                                   | 2009 | 2       | 35      | 0           |
| Semp Toshiba         | IS 1462B                                         | 2009 | 2       | 35      | 0           |
| Packard Bell         | EN Butterfly s                                   | 2009 | 2       | 35      | 0           |
| OEM                  | I42IL1                                           | 2009 | 2       | 35      | 0           |
| MSI                  | X300/X340/X350/X400 series                       | 2009 | 2       | 35      | 0           |
| ASUSTek Computer     | N71Vg                                            | 2009 | 2       | 35      | 0           |
| ASUSTek Computer     | 1003HAG                                          | 2009 | 2       | 35      | 0           |
| Sony                 | VGN-NR330FE                                      | 2008 | 2       | 35      | 0           |
| LG Electronics       | P310-S.CBRAG                                     | 2008 | 2       | 35      | 0           |
| Lenovo               | ThinkPad R61e/R61i                               | 2007 | 2       | 35      | 0           |
| Packard Bell         | EasyNote_GN45                                    | 2006 | 2       | 35      | 0           |
| MSI                  | MS-1034                                          | 2006 | 2       | 35      | 0           |
| Apple                | MacBookPro1,2                                    | 2006 | 2       | 35      | 0           |
| Hewlett-Packard      | ProBook 640 G8 Notebook PC                       | 2021 | 2       | 34      | 0           |
| IP3 Tech             | X30                                              | 2020 | 2       | 34      | 0           |
| Acer                 | Aspire A514-52                                   | 2020 | 2       | 34      | 0           |
| Alcor                | Flashbook D1423I                                 | 2019 | 2       | 34      | 0           |
| Acer                 | Extensa 215-51                                   | 2019 | 2       | 34      | 0           |
| ASUSTek Computer     | X556UA                                           | 2019 | 2       | 34      | 0           |
| ASUSTek Computer     | VivoBook S14 X411UF                              | 2019 | 2       | 34      | 0           |
| ASUSTek Computer     | UX410UQK                                         | 2019 | 2       | 34      | 0           |
| ASUSTek Computer     | X507UB                                           | 2018 | 2       | 34      | 0           |
| Dell                 | Inspiron 3559                                    | 2017 | 2       | 34      | 0           |
| Acer                 | Aspire SW5-173                                   | 2017 | 2       | 34      | 0           |
| Dell                 | Inspiron 5457                                    | 2016 | 2       | 34      | 0           |
| ASUSTek Computer     | X441UV                                           | 2016 | 2       | 34      | 0           |
| ASUSTek Computer     | X441UA                                           | 2016 | 2       | 34      | 0           |
| Medion               | Erazer X7841 MD99556                             | 2015 | 2       | 34      | 0           |
| Casper               | NIRVANA NOTEBOOK                                 | 2015 | 2       | 34      | 0           |
| Acer                 | Aspire E5-473                                    | 2015 | 2       | 34      | 0           |
| ASUSTek Computer     | TP300LJ                                          | 2015 | 2       | 34      | 0           |
| Panasonic            | CF-53SALZYLM                                     | 2014 | 2       | 34      | 0           |
| Packard Bell         | EasyNote ME69BMP                                 | 2014 | 2       | 34      | 0           |
| ASUSTek Computer     | TP550LA                                          | 2014 | 2       | 34      | 0           |
| ASUSTek Computer     | TAICHI31                                         | 2014 | 2       | 34      | 0           |
| Toshiba              | Satellite L50-A-158                              | 2013 | 2       | 34      | 0           |
| Toshiba              | Satellite C850-D2K                               | 2013 | 2       | 34      | 0           |
| MSI                  | GE70 0NC\0ND                                     | 2013 | 2       | 34      | 0           |
| Lenovo               | IdeaPad Flex14 20308                             | 2013 | 2       | 34      | 0           |
| Lenovo               | G780 2182                                        | 2013 | 2       | 34      | 0           |
| ASUSTek Computer     | X450EA                                           | 2013 | 2       | 34      | 0           |
| ASUSTek Computer     | N56VV                                            | 2013 | 2       | 34      | 0           |
| ASUSTek Computer     | K95VM                                            | 2013 | 2       | 34      | 0           |
| ASUSTek Computer     | G46VW                                            | 2013 | 2       | 34      | 0           |
| Sony                 | VPCSC31FM                                        | 2012 | 2       | 34      | 0           |
| Sony                 | SVE1512H1RW                                      | 2012 | 2       | 34      | 0           |
| Sony                 | SVE1512G1RW                                      | 2012 | 2       | 34      | 0           |
| Sony                 | SVE14A2V1EW                                      | 2012 | 2       | 34      | 0           |
| Sony                 | SVE14113ELW                                      | 2012 | 2       | 34      | 0           |
| Lenovo               | G580 26897JJ                                     | 2012 | 2       | 34      | 0           |
| Acer                 | Aspire V3-531                                    | 2012 | 2       | 34      | 0           |
| Sony                 | VPCEH28FG                                        | 2011 | 2       | 34      | 0           |
| Sony                 | VPCCB2S1E                                        | 2011 | 2       | 34      | 0           |
| Exo                  | HR14                                             | 2011 | 2       | 34      | 0           |
| Acer                 | Aspire 4739Z                                     | 2011 | 2       | 34      | 0           |
| Acer                 | Aspire 3750G                                     | 2011 | 2       | 34      | 0           |
| TPVAOC               | AA183M                                           | 2010 | 2       | 34      | 0           |
| MSI                  | U230                                             | 2010 | 2       | 34      | 0           |
| Lenovo               | IdeaPad Z565 4311                                | 2010 | 2       | 34      | 0           |
| Acer                 | Aspire 5334                                      | 2010 | 2       | 34      | 0           |
| Acer                 | Aspire 5252                                      | 2010 | 2       | 34      | 0           |
| ASUSTek Computer     | K50AD                                            | 2010 | 2       | 34      | 0           |
| ASUSTek Computer     | 1201NL                                           | 2010 | 2       | 34      | 0           |
| eMachines            | D525                                             | 2009 | 2       | 34      | 0           |
| Samsung Electronics  | X120/X170/X171                                   | 2009 | 2       | 34      | 0           |
| Samsung Electronics  | R468/R418                                        | 2009 | 2       | 34      | 0           |
| MSI                  | X300/X340/X400 series                            | 2009 | 2       | 34      | 0           |
| Samsung Electronics  | R59/R60/R61                                      | 2008 | 2       | 34      | 0           |
| Acer                 | Aspire 5535                                      | 2008 | 2       | 34      | 0           |
| BenQ                 | Joybook P41                                      | 2006 | 2       | 34      | 0           |
| Acer                 | TravelMate 4220                                  | 2006 | 2       | 34      | 0           |
| ASUSTek Computer     | A3F                                              | 2006 | 2       | 34      | 0           |
| Acer                 | Aspire 1690                                      | 2005 | 2       | 34      | 0           |
| Timi                 | Mi Laptop Air 12.5                               | 2019 | 2       | 33      | 0           |
| Acer                 | Aspire A317-51                                   | 2019 | 2       | 33      | 0           |
| ASUSTek Computer     | X507UA                                           | 2019 | 2       | 33      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X403FA_S403FA                | 2019 | 2       | 33      | 0           |
| Notebook             | N230WU                                           | 2018 | 2       | 33      | 0           |
| MSI                  | GF72 7RE                                         | 2018 | 2       | 33      | 0           |
| Lenovo               | B330-15IKBR 81M1                                 | 2018 | 2       | 33      | 0           |
| ASUSTek Computer     | VivoBook_ASUS Laptop X510UAO                     | 2018 | 2       | 33      | 0           |
| ASUSTek Computer     | VivoBook 17_ASUS Laptop X705UF                   | 2018 | 2       | 33      | 0           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MA_F540MA            | 2018 | 2       | 33      | 0           |
| Notebook             | N85_N87HCHN                                      | 2017 | 2       | 33      | 0           |
| Medion               | E6436 MD61150                                    | 2017 | 2       | 33      | 0           |
| HUAWEI               | MateBook X                                       | 2017 | 2       | 33      | 0           |
| Entroware            | Orion                                            | 2017 | 2       | 33      | 0           |
| TUXEDO               | N24_25BU                                         | 2016 | 2       | 33      | 0           |
| Lenovo               | IdeaPad Y700-14ISK 80NU                          | 2016 | 2       | 33      | 0           |
| Lenovo               | IdeaPad 305-15IBD 80NJ                           | 2016 | 2       | 33      | 0           |
| Hewlett-Packard      | ProBook 650 G3                                   | 2016 | 2       | 33      | 0           |
| Fujitsu              | LIFEBOOK A556/G                                  | 2016 | 2       | 33      | 0           |
| Panasonic            | CF-54-1                                          | 2015 | 2       | 33      | 0           |
| Lenovo               | Z40-70 80E6                                      | 2015 | 2       | 33      | 0           |
| BOX                  | W54_W94_W955TU,-T,-C                             | 2015 | 2       | 33      | 0           |
| Acer                 | Aspire V3-574                                    | 2015 | 2       | 33      | 0           |
| Lenovo               | Y40-70 20347                                     | 2014 | 2       | 33      | 0           |
| Acer                 | Aspire R3-471T                                   | 2014 | 2       | 33      | 0           |
| Sony                 | SVE14A2X1EW                                      | 2013 | 2       | 33      | 0           |
| Hewlett-Packard      | ENVY dv4                                         | 2013 | 2       | 33      | 0           |
| ASUSTek Computer     | X451CAP                                          | 2013 | 2       | 33      | 0           |
| iRU                  | PC-B1005                                         | 2012 | 2       | 33      | 0           |
| Toshiba              | Satellite C855-112                               | 2012 | 2       | 33      | 0           |
| Sony                 | SVT13115FBS                                      | 2012 | 2       | 33      | 0           |
| Sony                 | SVE14126CXB                                      | 2012 | 2       | 33      | 0           |
| Medion               | P7815                                            | 2012 | 2       | 33      | 0           |
| Medion               | A17                                              | 2012 | 2       | 33      | 0           |
| Lenovo               | IdeaPad S400u 20213                              | 2012 | 2       | 33      | 0           |
| Lenovo               | IdeaPad N580 20182                               | 2012 | 2       | 33      | 0           |
| Hewlett-Packard      | SpectreXT Pro 13-b000 PC                         | 2012 | 2       | 33      | 0           |
| Hewlett-Packard      | ENVY Pro 4-b000 Ultrabook PC                     | 2012 | 2       | 33      | 0           |
| Dell                 | XPS 12 9Q23                                      | 2012 | 2       | 33      | 0           |
| Acer                 | Aspire V3-471G                                   | 2012 | 2       | 33      | 0           |
| Acer                 | Aspire M3-481                                    | 2012 | 2       | 33      | 0           |
| ASUSTek Computer     | X55VDR                                           | 2012 | 2       | 33      | 0           |
| Sony                 | VPCEH2F1E                                        | 2011 | 2       | 33      | 0           |
| Packard Bell         | EasyNote TSX62HR                                 | 2011 | 2       | 33      | 0           |
| Hewlett-Packard      | ENVY 14 SPECTRE                                  | 2011 | 2       | 33      | 0           |
| Fujitsu              | LIFEBOOK LH531                                   | 2011 | 2       | 33      | 0           |
| Acer                 | Aspire 7739Z                                     | 2011 | 2       | 33      | 0           |
| eMachines            | eMD528                                           | 2010 | 2       | 33      | 0           |
| Samsung Electronics  | R428/P428                                        | 2010 | 2       | 33      | 0           |
| Samsung Electronics  | NF110/NF210/NF310                                | 2010 | 2       | 33      | 0           |
| Samsung Electronics  | N248P                                            | 2010 | 2       | 33      | 0           |
| Samsung Electronics  | N150                                             | 2010 | 2       | 33      | 0           |
| Samsung Electronics  | N148P/N208P/N218P/NB28P                          | 2010 | 2       | 33      | 0           |
| LG Electronics       | X140-G.BG12P1                                    | 2010 | 2       | 33      | 0           |
| Haier                | T68D                                             | 2010 | 2       | 33      | 0           |
| Hewlett-Packard      | ProBook 5310m                                    | 2009 | 2       | 33      | 0           |
| Gateway              | LT20                                             | 2009 | 2       | 33      | 0           |
| Clevo                | M815P                                            | 2009 | 2       | 33      | 0           |
| ASUSTek Computer     | UL50VT                                           | 2009 | 2       | 33      | 0           |
| MSI                  | PR601/VR603                                      | 2008 | 2       | 33      | 0           |
| Notebook             | RIM2000                                          | 2005 | 2       | 33      | 0           |
| Hewlett-Packard      | Pavilion ze2000                                  | 2005 | 2       | 33      | 0           |
| Fujitsu Siemens      | LIFEBOOK S7020                                   | 2005 | 2       | 33      | 0           |
| Fujitsu Siemens      | AMILO A1650G                                     | 2005 | 2       | 33      | 0           |
| Lenovo               | IdeaPad 3 15IGL05 81WQ                           | 2020 | 2       | 32      | 0           |
| Google               | Chell                                            | 2019 | 2       | 32      | 0           |
| Acer                 | Extensa 215-51K                                  | 2019 | 2       | 32      | 0           |
| Acer                 | Aspier XXXX                                      | 2019 | 2       | 32      | 0           |
| Lenovo               | V130-14IGM 81HM                                  | 2018 | 2       | 32      | 0           |
| Acer                 | TravelMate P2410-G2-M                            | 2018 | 2       | 32      | 0           |
| AVITA                | NS13A2                                           | 2018 | 2       | 32      | 0           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X420UA                       | 2018 | 2       | 32      | 0           |
| Samsung Electronics  | 940Z5L                                           | 2016 | 2       | 32      | 0           |
| ASUSTek Computer     | Z450UA                                           | 2016 | 2       | 32      | 0           |
| ASUSTek Computer     | X556UAM                                          | 2016 | 2       | 32      | 0           |
| ASUSTek Computer     | UX330UA                                          | 2016 | 2       | 32      | 0           |
| Acer                 | Aspire F5-571T                                   | 2015 | 2       | 32      | 0           |
| Toshiba              | Satellite L45-B                                  | 2014 | 2       | 32      | 0           |
| LG Electronics       | 23V545-G.BK31P1                                  | 2014 | 2       | 32      | 0           |
| Dell                 | XPS 12-9Q33                                      | 2014 | 2       | 32      | 0           |
| Acer                 | TravelMate P256-MG                               | 2014 | 2       | 32      | 0           |
| Acer                 | TravelMate P246-M                                | 2014 | 2       | 32      | 0           |
| Acer                 | Gateway NE46Rs                                   | 2014 | 2       | 32      | 0           |
| ASUSTek Computer     | UX302LA                                          | 2014 | 2       | 32      | 0           |
| Toshiba              | Satellite P55t-A                                 | 2013 | 2       | 32      | 0           |
| Toshiba              | Satellite L70-A-K6S                              | 2013 | 2       | 32      | 0           |
| MSI                  | CR610M                                           | 2013 | 2       | 32      | 0           |
| Dell                 | Vostro 5460                                      | 2013 | 2       | 32      | 0           |
| Dell                 | Vostro 2521                                      | 2013 | 2       | 32      | 0           |
| Toshiba              | Satellite C850-B6W                               | 2012 | 2       | 32      | 0           |
| Sony                 | SVE1712T1RB                                      | 2012 | 2       | 32      | 0           |
| Samsung Electronics  | 370R4E/370R4V/370R5E/3570RE/370R5V               | 2012 | 2       | 32      | 0           |
| Pegatron             | H90K                                             | 2012 | 2       | 32      | 0           |
| Intel                | Cedar Trail                                      | 2012 | 2       | 32      | 0           |
| Gigabyte Technology  | Q2006                                            | 2012 | 2       | 32      | 0           |
| Dell                 | Inspiron 5323                                    | 2012 | 2       | 32      | 0           |
| DNS                  | PC-B1811                                         | 2012 | 2       | 32      | 0           |
| Acer                 | Aspire M3-581T                                   | 2012 | 2       | 32      | 0           |
| Toshiba              | Satellite L730                                   | 2011 | 2       | 32      | 0           |
| Sony                 | VPCCB3S1E                                        | 2011 | 2       | 32      | 0           |
| Sony                 | VPCCA1S1E                                        | 2011 | 2       | 32      | 0           |
| Gateway              | NV47H                                            | 2011 | 2       | 32      | 0           |
| DNS                  | DNSNB                                            | 2011 | 2       | 32      | 0           |
| ASUSTek Computer     | K43SV                                            | 2011 | 2       | 32      | 0           |
| Lenovo               | IdeaPad S10-3t 20040M18                          | 2010 | 2       | 32      | 0           |
| Exo                  | Exomate X352                                     | 2010 | 2       | 32      | 0           |
| Dell                 | Inspiron 1546                                    | 2010 | 2       | 32      | 0           |
| ASUSTek Computer     | 1016P                                            | 2010 | 2       | 32      | 0           |
| ASUSTek Computer     | 1008P                                            | 2010 | 2       | 32      | 0           |
| Toshiba              | NB 105                                           | 2009 | 2       | 32      | 0           |
| ONKYO                | ONKYOPC                                          | 2009 | 2       | 32      | 0           |
| Hewlett-Packard      | Mini                                             | 2009 | 2       | 32      | 0           |
| ASUSTek Computer     | U80A                                             | 2009 | 2       | 32      | 0           |
| Samsung Electronics  | SQ1US                                            | 2007 | 2       | 32      | 0           |
| Toshiba              | QOSMIO F20                                       | 2005 | 2       | 32      | 0           |
| Acer                 | TravelMate 4060                                  | 2005 | 2       | 32      | 0           |
| HONOR                | NBD-WXX9                                         | 2021 | 2       | 31      | 0           |
| Hewlett-Packard      | Laptop 14-cf3xxx                                 | 2020 | 2       | 31      | 0           |
| Lenovo               | IdeaPad 130-14IKB 81H6                           | 2019 | 2       | 31      | 0           |
| ASUSTek Computer     | VivoBook 14_ASUS Laptop X407UA                   | 2019 | 2       | 31      | 0           |
| Lenovo               | IdeaPad 330S-15IKB D 81F5                        | 2018 | 2       | 31      | 0           |
| Lenovo               | IdeaPad 330S-14IKB 81JM                          | 2018 | 2       | 31      | 0           |
| Hewlett-Packard      | Pavilion Laptop 15-cc6xx                         | 2018 | 2       | 31      | 0           |
| Dell                 | Inspiron 7437                                    | 2018 | 2       | 31      | 0           |
| ASUSTek Computer     | X705NA                                           | 2018 | 2       | 31      | 0           |
| Monster              | ABRA A5 V12.1                                    | 2017 | 2       | 31      | 0           |
| Lenovo               | IdeaPad 510S-14IKB 80UV                          | 2017 | 2       | 31      | 0           |
| Lenovo               | IdeaPad 310-14IKB 80TU                           | 2017 | 2       | 31      | 0           |
| BANGHO               | MAX G5                                           | 2017 | 2       | 31      | 0           |
| Acer                 | Swift SF114-31                                   | 2017 | 2       | 31      | 0           |
| Acer                 | TravelMate B116-M                                | 2016 | 2       | 31      | 0           |
| Acer                 | Aspire E5-774                                    | 2016 | 2       | 31      | 0           |
| Toshiba              | Satellite S55t-C                                 | 2015 | 2       | 31      | 0           |
| Toshiba              | Satellite C50t-B                                 | 2015 | 2       | 31      | 0           |
| Acer                 | One Z1402                                        | 2015 | 2       | 31      | 0           |
| Acer                 | Aspire E5-432                                    | 2015 | 2       | 31      | 0           |
| Lenovo               | Yoga 3 11 80J8                                   | 2014 | 2       | 31      | 0           |
| Lenovo               | IdeaPad S410 20301                               | 2014 | 2       | 31      | 0           |
| Google               | Falco                                            | 2014 | 2       | 31      | 0           |
| Acer                 | Aspire E1-410                                    | 2014 | 2       | 31      | 0           |
| VIT                  | P2402                                            | 2013 | 2       | 31      | 0           |
| Toshiba              | PLCSF                                            | 2013 | 2       | 31      | 0           |
| Medion               | P6640                                            | 2013 | 2       | 31      | 0           |
| Lenovo               | IdeaPad S400 VIUS3                               | 2013 | 2       | 31      | 0           |
| Hewlett-Packard      | EliteBook Revolve 810 G1                         | 2013 | 2       | 31      | 0           |
| Fujitsu              | LIFEBOOK AH502                                   | 2013 | 2       | 31      | 0           |
| Acer                 | NC-E1-572G-54208                                 | 2013 | 2       | 31      | 0           |
| ASUSTek Computer     | X201E                                            | 2013 | 2       | 31      | 0           |
| Toshiba              | Satellite C850-F117                              | 2012 | 2       | 31      | 0           |
| Philco               | 10D                                              | 2012 | 2       | 31      | 0           |
| Login Informatica    | LOG-QAL30                                        | 2012 | 2       | 31      | 0           |
| Lenovo               | IdeaPad U510 20191                               | 2012 | 2       | 31      | 0           |
| Lenovo               | IdeaPad S300 20197                               | 2012 | 2       | 31      | 0           |
| Acer                 | Aspire S7-191                                    | 2012 | 2       | 31      | 0           |
| Acer                 | Aspire M5-581T                                   | 2012 | 2       | 31      | 0           |
| Sony                 | VPCEH30EB                                        | 2011 | 2       | 31      | 0           |
| Sony                 | VPCEH2N1E                                        | 2011 | 2       | 31      | 0           |
| Sony                 | VPCEH1L8E                                        | 2011 | 2       | 31      | 0           |
| Samsung Electronics  | RC512                                            | 2011 | 2       | 31      | 0           |
| MSI                  | CR643                                            | 2011 | 2       | 31      | 0           |
| Lenovo               | IdeaPad Z570 10243VU                             | 2011 | 2       | 31      | 0           |
| DNS                  | MB50II1                                          | 2011 | 2       | 31      | 0           |
| Acer                 | TravelMate 5360                                  | 2011 | 2       | 31      | 0           |
| MSI                  | MS-N051                                          | 2010 | 2       | 31      | 0           |
| Toshiba              | NB205                                            | 2009 | 2       | 31      | 0           |
| Hewlett-Packard      | Compaq 510                                       | 2009 | 2       | 31      | 0           |
| ASUSTek Computer     | 900HA                                            | 2009 | 2       | 31      | 0           |
| Hewlett-Packard      | G5000                                            | 2007 | 2       | 31      | 0           |
| ASUSTek Computer     | X51H                                             | 2007 | 2       | 31      | 0           |
| Sony                 | VGN-FS115ZR                                      | 2005 | 2       | 31      | 0           |
| Hewlett-Packard      | Presario V2000                                   | 2005 | 2       | 31      | 0           |
| Acer                 | Aspire 1640                                      | 2005 | 2       | 31      | 0           |
| ASUSTek Computer     | A6VC                                             | 2005 | 2       | 31      | 0           |
| ASUSTek Computer     | A3V                                              | 2005 | 2       | 31      | 0           |
| Hewlett-Packard      | Laptop 14s-dq2xxx                                | 2021 | 2       | 30      | 0           |
| Lenovo               | BS145-15IIL 82HB                                 | 2020 | 2       | 30      | 0           |
| Hewlett-Packard      | Stream Laptop 14-CB1xxx                          | 2020 | 2       | 30      | 0           |
| Hewlett-Packard      | 348 G7                                           | 2020 | 2       | 30      | 0           |
| Dell                 | Vostro 3584                                      | 2019 | 2       | 30      | 0           |
| Compaq               | Presario CQ-32                                   | 2018 | 2       | 30      | 0           |
| Lenovo               | IdeaPad 320-14IKB 80XK                           | 2017 | 2       | 30      | 0           |
| Acer                 | Aspire XXXX                                      | 2016 | 2       | 30      | 0           |
| Wortmann AG          | TERRA_MOBILE_1513P                               | 2015 | 2       | 30      | 0           |
| Packard Bell         | EasyNote ENLG81BA                                | 2015 | 2       | 30      | 0           |
| Fujitsu              | LIFEBOOK E746                                    | 2015 | 2       | 30      | 0           |
| ASUSTek Computer     | Z550MA                                           | 2015 | 2       | 30      | 0           |
| Acer                 | Extensa 2508                                     | 2014 | 2       | 30      | 0           |
| Acer                 | Aspire E5-731                                    | 2014 | 2       | 30      | 0           |
| Acer                 | Aspire E5-511P                                   | 2014 | 2       | 30      | 0           |
| ASUSTek Computer     | X450LA                                           | 2014 | 2       | 30      | 0           |
| Toshiba              | Dakar10FW8                                       | 2013 | 2       | 30      | 0           |
| Sony                 | SVP1322M1RBI                                     | 2013 | 2       | 30      | 0           |
| Samsung Electronics  | 520U4C/520U4X                                    | 2013 | 2       | 30      | 0           |
| Medion               | Akoya E6240T                                     | 2013 | 2       | 30      | 0           |
| MSI                  | CR42 2M/GE40 2OC                                 | 2013 | 2       | 30      | 0           |
| Lenovo               | IdeaPad S210 20256                               | 2013 | 2       | 30      | 0           |
| Acer                 | Aspire V5-132P                                   | 2013 | 2       | 30      | 0           |
| Acer                 | Aspire R7-572                                    | 2013 | 2       | 30      | 0           |
| Acer                 | Aspire M5-583P                                   | 2013 | 2       | 30      | 0           |
| Toshiba              | Satellite U840-B7S                               | 2012 | 2       | 30      | 0           |
| Sony                 | VPCEH2L9E                                        | 2012 | 2       | 30      | 0           |
| Sony                 | SVE1713F1EW                                      | 2012 | 2       | 30      | 0           |
| MSI                  | U180                                             | 2012 | 2       | 30      | 0           |
| Dell                 | Vostro 2420                                      | 2012 | 2       | 30      | 0           |
| Acer                 | Aspire V3-471                                    | 2012 | 2       | 30      | 0           |
| Sony                 | VPCCB25FX                                        | 2011 | 2       | 30      | 0           |
| Samsung Electronics  | 300V3Z/300V4Z/300V5Z/200A4Y/200A5Y               | 2011 | 2       | 30      | 0           |
| Philco               | 14E                                              | 2011 | 2       | 30      | 0           |
| Lenovo               | IdeaPad Z570 10249UU                             | 2011 | 2       | 30      | 0           |
| Lenovo               | BP PV CLASSMATE+                                 | 2011 | 2       | 30      | 0           |
| LG Electronics       | S425-G.BC34P1                                    | 2011 | 2       | 30      | 0           |
| Hewlett-Packard      | Mini 5102                                        | 2011 | 2       | 30      | 0           |
| Compal               | PBL10                                            | 2011 | 2       | 30      | 0           |
| Acer                 | Aspire 7750ZG                                    | 2011 | 2       | 30      | 0           |
| Sony                 | VPCP11S1R                                        | 2010 | 2       | 30      | 0           |
| Sony                 | VPCM13M1E                                        | 2010 | 2       | 30      | 0           |
| Exo                  | Intel powered classmate PC                       | 2010 | 2       | 30      | 0           |
| ASUSTek Computer     | 900AX                                            | 2010 | 2       | 30      | 0           |
| ASUSTek Computer     | 1001P                                            | 2010 | 2       | 30      | 0           |
| Samsung Electronics  | N140                                             | 2009 | 2       | 30      | 0           |
| ASUSTek Computer     | UL20A                                            | 2009 | 2       | 30      | 0           |
| Sony                 | VGN-N31Z_W                                       | 2007 | 2       | 30      | 0           |
| BenQ                 | Joybook A52                                      | 2007 | 2       | 30      | 0           |
| Fujitsu Siemens      | AMILO M1451G Series                              | 2005 | 2       | 30      | 0           |
| Fujitsu Siemens      | AMILO M1437 Series                               | 2005 | 2       | 30      | 0           |
| ASUSTek Computer     | Q304UA                                           | 2019 | 2       | 29      | 0           |
| BANGHO               | MAX G5 i1                                        | 2017 | 2       | 29      | 0           |
| Acer                 | Swift SF713-51                                   | 2017 | 2       | 29      | 0           |
| ASUSTek Computer     | E502NA                                           | 2017 | 2       | 29      | 0           |
| Lenovo               | ThinkPad Yoga 11e 3rd Gen                        | 2016 | 2       | 29      | 0           |
| Hewlett-Packard      | 240 G5 Notebook PC                               | 2016 | 2       | 29      | 0           |
| Gigabyte Technology  | GB-BSi7A-6500                                    | 2016 | 2       | 29      | 0           |
| Compaq               | Presario CQ-25                                   | 2016 | 2       | 29      | 0           |
| Compaq               | Presario CQ-21                                   | 2016 | 2       | 29      | 0           |
| Lenovo               | 100-14IBY 80R7                                   | 2015 | 2       | 29      | 0           |
| ASUSTek Computer     | T300CHI                                          | 2015 | 2       | 29      | 0           |
| Medion               | Akoya S6214T                                     | 2014 | 2       | 29      | 0           |
| Acer                 | Aspire SW5-271                                   | 2014 | 2       | 29      | 0           |
| Acer                 | Aspire ES1-711G                                  | 2014 | 2       | 29      | 0           |
| Hewlett-Packard      | Spectre 13 Ultrabook                             | 2013 | 2       | 29      | 0           |
| Hewlett-Packard      | ENVY TouchSmart Sleekbook 4                      | 2013 | 2       | 29      | 0           |
| Toshiba              | dynabook R731/E                                  | 2012 | 2       | 29      | 0           |
| Toshiba              | Satellite L830                                   | 2012 | 2       | 29      | 0           |
| Lenovo               | IdeaPad Yoga 13 2191                             | 2012 | 2       | 29      | 0           |
| Lenovo               | G470 4328                                        | 2012 | 2       | 29      | 0           |
| Hewlett-Packard      | Mini 1104                                        | 2011 | 2       | 29      | 0           |
| Acer                 | TravelMate 5760Z                                 | 2011 | 2       | 29      | 0           |
| Sony                 | VPCX11Z1R                                        | 2009 | 2       | 29      | 0           |
| ASUSTek Computer     | 1002HA                                           | 2009 | 2       | 29      | 0           |
| Hewlett-Packard      | 530 Notebook PC(KD092AA#ACQ)                     | 2007 | 2       | 29      | 0           |
| Toshiba              | Satellite Pro A120                               | 2006 | 2       | 29      | 0           |
| Coconics Private ... | CNBIC-AA01                                       | 2019 | 2       | 28      | 0           |
| Acer                 | One 14 Z2-485                                    | 2019 | 2       | 28      | 0           |
| Samsung Electronics  | 900X3J                                           | 2018 | 2       | 28      | 0           |
| Getac                | S410G2                                           | 2018 | 2       | 28      | 0           |
| Entroware            | Aether                                           | 2018 | 2       | 28      | 0           |
| Acer                 | Aspire A314-32                                   | 2018 | 2       | 28      | 0           |
| Sony                 | VJF155F11X-B0211B                                | 2017 | 2       | 28      | 0           |
| Intel                | Kabylake Platform                                | 2017 | 2       | 28      | 0           |
| Samsung Electronics  | 370E4J/370E4Q                                    | 2016 | 2       | 28      | 0           |
| Gigabyte Technology  | GB-BSi3A-6100                                    | 2016 | 2       | 28      | 0           |
| Toshiba              | Satellite L10W-B-102                             | 2015 | 2       | 28      | 0           |
| Lenovo               | Yoga 500-15IHW 80N7                              | 2015 | 2       | 28      | 0           |
| Acer                 | Aspire E3-112M                                   | 2014 | 2       | 28      | 0           |
| ASUSTek Computer     | X453MA                                           | 2014 | 2       | 28      | 0           |
| Toshiba              | Satellite Z930                                   | 2013 | 2       | 28      | 0           |
| Itautec              | Infoway w7510                                    | 2013 | 2       | 28      | 0           |
| Compal               | VAW70                                            | 2013 | 2       | 28      | 0           |
| Toshiba              | Satellite C875                                   | 2012 | 2       | 28      | 0           |
| Toshiba              | Satellite C850                                   | 2012 | 2       | 28      | 0           |
| Sony                 | SVE1713A6EW                                      | 2012 | 2       | 28      | 0           |
| Samsung Electronics  | 900X3C/900X4C/900X4D                             | 2012 | 2       | 28      | 0           |
| ASUSTek Computer     | R11CX                                            | 2012 | 2       | 28      | 0           |
| Toshiba              | Satellite C640                                   | 2011 | 2       | 28      | 0           |
| LG Electronics       | S425-L.BC22P1                                    | 2011 | 2       | 28      | 0           |
| ASUSTek Computer     | 1015PW                                           | 2011 | 2       | 28      | 0           |
| MTC                  | Montara-GML                                      | 2005 | 2       | 28      | 0           |
| Hewlett-Packard      | Stream Laptop                                    | 2020 | 2       | 27      | 0           |
| Lenovo               | V320-17IKB 81AH                                  | 2017 | 2       | 27      | 0           |
| Acer                 | Aspire MM1-571                                   | 2016 | 2       | 27      | 0           |
| Lenovo               | IdeaPad 300-14IBR 80M2                           | 2015 | 2       | 27      | 0           |
| Positivo             | W940TU                                           | 2014 | 2       | 27      | 0           |
| Medion               | Akoya E7226                                      | 2014 | 2       | 27      | 0           |
| Medion               | Akoya E6239                                      | 2014 | 2       | 27      | 0           |
| Essentiel B          | SmartMOUV series                                 | 2014 | 2       | 27      | 0           |
| Lenovo               | IdeaPad S100c 20194                              | 2012 | 2       | 27      | 0           |
| Sony                 | VPCEH2H1E                                        | 2011 | 2       | 27      | 0           |
| Samsung Electronics  | 350U2A/350U2B/300U1A/351U2A/351U2B               | 2011 | 2       | 27      | 0           |
| Acer                 | Aspire 4349                                      | 2011 | 2       | 27      | 0           |
| Samsung Electronics  | P28                                              | 2005 | 2       | 27      | 0           |
| Standard             | MB45II/MB45IN                                    |      | 2       | 27      | 0           |
| Google               | Setzer                                           | 2019 | 2       | 26      | 0           |
| Standard             | SF20BA2                                          | 2018 | 2       | 26      | 0           |
| MLS                  | Magic 11.6                                       | 2017 | 2       | 26      | 0           |
| Lenovo               | IdeaPad 320-14IAP 80XQ                           | 2017 | 2       | 26      | 0           |
| Haier                | Y11B                                             | 2016 | 2       | 26      | 0           |
| LNV                  | L40-30                                           | 2014 | 2       | 26      | 0           |
| AXDIA International  | wintab 9 plus 3G                                 | 2014 | 2       | 26      | 0           |
| Lenovo               | IdeaPad U330p 20267                              | 2013 | 2       | 26      | 0           |
| Garbarino SAIC       | A24                                              | 2011 | 2       | 26      | 0           |
| Hewlett-Packard      | Stream Laptop 11-ak1xxx                          | 2019 | 2       | 25      | 0           |
| Google               | Auron_Yuna                                       | 2017 | 2       | 25      | 0           |
| Jumper               | EZpad6                                           | 2016 | 2       | 25      | 0           |
| Tangent Computer     | Medix T19B                                       | 2013 | 2       | 25      | 0           |
| Intel                | Shark Bay Client platform                        | 2013 | 2       | 25      | 0           |
| Fujitsu Siemens      | AMILO Li 1718                                    | 2007 | 2       | 25      | 0           |
| Quanta               | MW1/HW1                                          | 2006 | 2       | 25      | 0           |
| Multilaser           | PC301                                            | 2020 | 2       | 24      | 0           |
| Google               | Glimmer                                          | 2020 | 2       | 24      | 0           |
| Fusion5              | T90B_Pro                                         | 2019 | 2       | 24      | 0           |
| YiFang               | NX16W11264                                       | 2016 | 2       | 24      | 0           |
| ASUSTek Computer     | T200TAC                                          | 2015 | 2       | 24      | 0           |
| Google               | Squawks                                          | 2019 | 2       | 23      | 0           |
| LIVEFAN              | Cherry Trail CR                                  | 2017 | 2       | 23      | 0           |
| Acer                 | Aspire SW5-015                                   | 2015 | 2       | 23      | 0           |
| Direkt-Tek           | DTLAPY116-1                                      | 2017 | 2       | 22      | 0           |
| Dell                 | Venue 8 Pro 5830                                 | 2016 | 2       | 22      | 0           |
| 4Good                | Light AM500                                      | 2016 | 2       | 22      | 0           |
| Teclast              | X98Pro                                           | 2015 | 2       | 22      | 0           |
| Insyde               | i101c                                            | 2015 | 2       | 22      | 0           |
| Google               | Winky                                            | 2019 | 2       | 21      | 0           |
| Dell                 | Venue 10 Pro 5055                                | 2018 | 2       | 21      | 0           |
| Digma                | CITI E220 ES2006EW                               | 2017 | 2       | 21      | 0           |
| Advantec             | CX23200W                                         | 2017 | 2       | 21      | 0           |
| LAMINA               | T-1012B NORD                                     | 2016 | 2       | 21      | 0           |
| Dell                 | Venue 11 Pro 5130                                | 2016 | 2       | 21      | 0           |
| Insyde               | i86C                                             | 2015 | 2       | 21      | 0           |
| Insyde               | TW36                                             | 2016 | 2       | 20      | 0           |
| Irbis                | i101c                                            | 2016 | 2       | 19      | 0           |
| Irbis                | TW81                                             | 2015 | 2       | 19      | 0           |
| MODECOM              | FreeONE ALIBABA                                  | 2018 | 2       | 17      | 0           |
| Apple                | MacBookPro9,2                                    | 2012 | 120     | 50      | 1           |
| Dell                 | Inspiron 1545                                    | 2008 | 119     | 43      | 1           |
| Dell                 | Inspiron 5570                                    | 2017 | 116     | 45      | 1           |
| Dell                 | Inspiron 3521                                    | 2012 | 106     | 38      | 1           |
| Toshiba              | Satellite C660                                   | 2010 | 100     | 42      | 1           |
| Lenovo               | IdeaPad 330-15IKB 81DE                           | 2018 | 97      | 43      | 1           |
| Lenovo               | G50-30 80G0                                      | 2014 | 95      | 33      | 1           |
| Dell                 | XPS 13 7390                                      | 2019 | 93      | 59      | 1           |
| Dell                 | XPS 13 9380                                      | 2018 | 92      | 58      | 1           |
| Dell                 | Inspiron 1525                                    | 2008 | 89      | 47      | 1           |
| ASUSTek Computer     | X541NA                                           | 2017 | 77      | 35      | 1           |
| Lenovo               | ThinkPad X201                                    | 2010 | 69      | 48      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540UAR                  | 2018 | 68      | 36      | 1           |
| Lenovo               | G50-70 20351                                     | 2013 | 67      | 41      | 1           |
| Packard Bell         | EasyNote TE11HC                                  | 2012 | 67      | 37      | 1           |
| Apple                | MacBook4,1                                       | 2008 | 65      | 41      | 1           |
| Hewlett-Packard      | Pavilion g4                                      | 2011 | 64      | 40      | 1           |
| Hewlett-Packard      | 250 G7 Notebook PC                               | 2018 | 64      | 38      | 1           |
| Acer                 | Nitro AN515-43                                   | 2019 | 63      | 47      | 1           |
| Dell                 | Inspiron N5010                                   | 2010 | 63      | 45      | 1           |
| Dell                 | Inspiron 5567                                    | 2016 | 62      | 41      | 1           |
| Acer                 | Aspire A315-21                                   | 2017 | 61      | 48      | 1           |
| Apple                | MacBookPro12,1                                   | 2016 | 60      | 49      | 1           |
| Hewlett-Packard      | Laptop 15-bs1xx                                  | 2017 | 60      | 38      | 1           |
| ASUSTek Computer     | K50IJ                                            | 2009 | 60      | 37      | 1           |
| Lenovo               | B590 20206                                       | 2012 | 60      | 36      | 1           |
| Hewlett-Packard      | Compaq Presario CQ60                             | 2008 | 59      | 43      | 1           |
| Dell                 | Inspiron 3583                                    | 2019 | 59      | 42      | 1           |
| ASUSTek Computer     | UX31E                                            | 2011 | 58      | 33      | 1           |
| ASUSTek Computer     | K52F                                             | 2009 | 56      | 45      | 1           |
| Toshiba              | Satellite A200                                   | 2007 | 56      | 45      | 1           |
| Acer                 | Nitro AN517-51                                   | 2019 | 55      | 45      | 1           |
| ASUSTek Computer     | K53U                                             | 2011 | 55      | 44      | 1           |
| Samsung Electronics  | 300V3A/300V4A/300V5A/200A4B/200A5B               | 2011 | 55      | 34      | 1           |
| Lenovo               | IdeaPad 100-15IBY 80MJ                           | 2015 | 55      | 33      | 1           |
| Acer                 | Aspire E5-571                                    | 2014 | 53      | 37      | 1           |
| Acer                 | Aspire E1-531                                    | 2012 | 53      | 35      | 1           |
| ASUSTek Computer     | X553MA                                           | 2014 | 53      | 32      | 1           |
| Dell                 | XPS 13 9300                                      | 2020 | 52      | 60      | 1           |
| Acer                 | Aspire A515-51                                   | 2017 | 52      | 44      | 1           |
| Dell                 | Inspiron 7520                                    | 2012 | 52      | 41      | 1           |
| Lenovo               | G505s 20255                                      | 2013 | 51      | 44      | 1           |
| Hewlett-Packard      | 620                                              | 2010 | 51      | 40      | 1           |
| Dell                 | Precision 5540                                   | 2019 | 50      | 64      | 1           |
| Samsung Electronics  | 340XAA/350XAA/550XAA                             | 2018 | 50      | 42      | 1           |
| ASUSTek Computer     | K53E                                             | 2011 | 50      | 37      | 1           |
| Lenovo               | G580 20157                                       | 2012 | 50      | 35      | 1           |
| Hewlett-Packard      | 250 G1                                           | 2013 | 50      | 33      | 1           |
| Hewlett-Packard      | Presario CQ56                                    | 2010 | 49      | 38      | 1           |
| Acer                 | Extensa 5220                                     | 2007 | 48      | 41      | 1           |
| Acer                 | Aspire A315-34                                   | 2019 | 48      | 39      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MA_X540MA            | 2018 | 48      | 38      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540BA                   | 2018 | 47      | 41      | 1           |
| Acer                 | Aspire E1-571                                    | 2012 | 47      | 35      | 1           |
| Lenovo               | G50-80 80E5                                      | 2014 | 46      | 36      | 1           |
| ASUSTek Computer     | X551MA                                           | 2013 | 46      | 33      | 1           |
| Hewlett-Packard      | 250 G5 Notebook PC                               | 2016 | 45      | 38      | 1           |
| Lenovo               | V145-15AST 81MT                                  | 2018 | 44      | 43      | 1           |
| Hewlett-Packard      | ProBook 6560b                                    | 2011 | 44      | 36      | 1           |
| Dell                 | XPS 13 9350                                      | 2016 | 43      | 46      | 1           |
| Toshiba              | Satellite L500                                   | 2009 | 43      | 43      | 1           |
| Hewlett-Packard      | ProBook 6570b                                    | 2012 | 43      | 42      | 1           |
| Hewlett-Packard      | ProBook 450 G7                                   | 2019 | 42      | 47      | 1           |
| Apple                | MacBookPro7,1                                    | 2010 | 42      | 44      | 1           |
| ASUSTek Computer     | K53SV                                            | 2011 | 42      | 38      | 1           |
| Hewlett-Packard      | ProBook 6460b                                    | 2011 | 42      | 37      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MA_X543MA            | 2019 | 42      | 36      | 1           |
| Acer                 | Aspire E5-573                                    | 2015 | 42      | 35      | 1           |
| ASUSTek Computer     | K54C                                             | 2011 | 42      | 35      | 1           |
| Acer                 | Aspire one                                       | 2009 | 42      | 35      | 1           |
| Dell                 | Inspiron 3442                                    | 2014 | 41      | 42      | 1           |
| Toshiba              | Satellite L300                                   | 2008 | 41      | 38      | 1           |
| ASUSTek Computer     | X540SA                                           | 2015 | 41      | 32      | 1           |
| Dell                 | Latitude 7400                                    | 2019 | 40      | 60      | 1           |
| Dell                 | Latitude E6330                                   | 2012 | 40      | 53      | 1           |
| Dell                 | Inspiron MM061                                   | 2006 | 40      | 42      | 1           |
| Acer                 | Aspire ES1-572                                   | 2016 | 40      | 40      | 1           |
| Samsung Electronics  | 550P5C/550P7C                                    | 2012 | 40      | 40      | 1           |
| Hewlett-Packard      | Pavilion dm4                                     | 2010 | 40      | 38      | 1           |
| Itautec              | Infoway                                          | 2007 | 40      | 38      | 1           |
| Dell                 | Inspiron N4050                                   | 2011 | 40      | 37      | 1           |
| Dell                 | Inspiron N5050                                   | 2011 | 40      | 36      | 1           |
| Razer                | Blade                                            | 2013 | 39      | 56      | 1           |
| Lenovo               | G550 20023                                       | 2009 | 39      | 44      | 1           |
| Lenovo               | IdeaPad S145-15IWL 81S9                          | 2019 | 39      | 42      | 1           |
| Hewlett-Packard      | EliteBook 2570p                                  | 2012 | 39      | 42      | 1           |
| Samsung Electronics  | RV411/RV511/E3511/S3511/RV711/E3411              | 2011 | 39      | 40      | 1           |
| ASUSTek Computer     | K53SD                                            | 2011 | 39      | 39      | 1           |
| Lenovo               | IdeaPad 330-15IKB 81FE                           | 2018 | 39      | 34      | 1           |
| Lenovo               | IdeaPad 5 14ARE05 81YM                           | 2020 | 38      | 50      | 1           |
| ASUSTek Computer     | K55VD                                            | 2012 | 38      | 44      | 1           |
| Hewlett-Packard      | Pavilion dm1                                     | 2009 | 38      | 42      | 1           |
| Lenovo               | ThinkPad X1 Carbon 3rd                           | 2015 | 38      | 40      | 1           |
| HUAWEI               | MACH-WX9                                         | 2018 | 37      | 55      | 1           |
| Lenovo               | G700 20251                                       | 2013 | 37      | 47      | 1           |
| Lenovo               | IdeaPad 330-15ARR 81D2                           | 2018 | 37      | 45      | 1           |
| Dell                 | Inspiron 7577                                    | 2017 | 37      | 45      | 1           |
| Hewlett-Packard      | ProBook 6470b                                    | 2012 | 37      | 41      | 1           |
| Lenovo               | IdeaPad 110-15ACL 80TJ                           | 2016 | 37      | 39      | 1           |
| Samsung Electronics  | R519/R719                                        | 2009 | 37      | 38      | 1           |
| Dell                 | Inspiron 3558                                    | 2015 | 37      | 36      | 1           |
| Lenovo               | IdeaPad 110-15IBR 80T7                           | 2016 | 37      | 35      | 1           |
| Acer                 | AOD257                                           | 2011 | 37      | 32      | 1           |
| ASUSTek Computer     | K50C                                             | 2009 | 37      | 28      | 1           |
| Lenovo               | IdeaPad S145-15API 81V7                          | 2019 | 36      | 46      | 1           |
| Samsung Electronics  | R540/R580/R780/SA41/E452/E852                    | 2010 | 36      | 43      | 1           |
| Dell                 | Latitude E6230                                   | 2012 | 36      | 42      | 1           |
| Hewlett-Packard      | Compaq CQ58                                      | 2012 | 36      | 41      | 1           |
| ASUSTek Computer     | X555LAB                                          | 2014 | 36      | 34      | 1           |
| ASUSTek Computer     | X540NA                                           | 2017 | 36      | 31      | 1           |
| Samsung Electronics  | 355V4C/355V4X/355V5C/355V5X/356V4C/356V4X/356... | 2012 | 35      | 42      | 1           |
| Dell                 | Inspiron 3576                                    | 2017 | 35      | 40      | 1           |
| Lenovo               | G580                                             | 2012 | 35      | 39      | 1           |
| Dell                 | Vostro 15-3568                                   | 2016 | 35      | 38      | 1           |
| ASUSTek Computer     | X541UAK                                          | 2016 | 35      | 38      | 1           |
| Lenovo               | ThinkPad X1 Carbon 5th                           | 2017 | 34      | 60      | 1           |
| Acer                 | Aspire ES1-523                                   | 2016 | 34      | 42      | 1           |
| Lenovo               | IdeaPad 3 15IIL05 81WE                           | 2020 | 34      | 37      | 1           |
| Hewlett-Packard      | Pavilion x2 Detachable                           | 2015 | 34      | 37      | 1           |
| Lenovo               | IdeaPad 320-15ISK 80XH                           | 2017 | 34      | 36      | 1           |
| Lenovo               | IdeaPad 320-15IKB 80XL                           | 2017 | 34      | 36      | 1           |
| Apple                | MacBook5,1                                       | 2009 | 34      | 36      | 1           |
| Acer                 | Aspire ES1-512                                   | 2014 | 34      | 34      | 1           |
| Hewlett-Packard      | 635                                              | 2011 | 33      | 43      | 1           |
| Dell                 | Inspiron 5537                                    | 2013 | 33      | 37      | 1           |
| Samsung Electronics  | RF511/RF411/RF711                                | 2011 | 33      | 37      | 1           |
| Dell                 | Inspiron 5423                                    | 2012 | 33      | 36      | 1           |
| ASUSTek Computer     | X540LA                                           | 2015 | 33      | 35      | 1           |
| Acer                 | Aspire 5750                                      | 2011 | 33      | 35      | 1           |
| Acer                 | AOD270                                           | 2012 | 33      | 34      | 1           |
| ASUSTek Computer     | X200MA                                           | 2014 | 33      | 32      | 1           |
| Acer                 | Aspire F5-573G                                   | 2016 | 32      | 47      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540UBR                  | 2018 | 32      | 40      | 1           |
| Samsung Electronics  | N150P/N210P/N220P                                | 2010 | 32      | 34      | 1           |
| Dell                 | G5 5590                                          | 2019 | 31      | 54      | 1           |
| Lenovo               | ThinkPad E595                                    | 2019 | 31      | 51      | 1           |
| ASUSTek Computer     | TUF Gaming FX505DY_FX505DY                       | 2019 | 31      | 47      | 1           |
| Toshiba              | Satellite L755                                   | 2011 | 31      | 43      | 1           |
| Dell                 | XPS 13 9343                                      | 2015 | 31      | 42      | 1           |
| Acer                 | Aspire A515-54G                                  | 2019 | 31      | 40      | 1           |
| Dell                 | Inspiron 5437                                    | 2013 | 31      | 40      | 1           |
| Hewlett-Packard      | 255 G6 Notebook PC                               | 2017 | 31      | 39      | 1           |
| Acer                 | AOA150                                           | 2008 | 31      | 39      | 1           |
| ASUSTek Computer     | N56VZ                                            | 2012 | 31      | 38      | 1           |
| Samsung Electronics  | R530/R730                                        | 2010 | 31      | 36      | 1           |
| Samsung Electronics  | RC530/RC730                                      | 2011 | 31      | 35      | 1           |
| Hewlett-Packard      | Compaq 6720s                                     | 2007 | 31      | 35      | 1           |
| Acer                 | Extensa 2519                                     | 2015 | 31      | 33      | 1           |
| ASUSTek Computer     | X55A                                             | 2012 | 31      | 33      | 1           |
| Hewlett-Packard      | Stream Laptop 14-ax0XX                           | 2016 | 31      | 29      | 1           |
| Fujitsu Siemens      | ESPRIMO Mobile V5535                             | 2007 | 31      | 27      | 1           |
| Acer                 | Aspire 5755G                                     | 2011 | 30      | 44      | 1           |
| Hewlett-Packard      | Laptop 15s-eq2xxx                                | 2021 | 30      | 43      | 1           |
| Hewlett-Packard      | ProBook 4520s                                    | 2010 | 30      | 41      | 1           |
| Hewlett-Packard      | Laptop 15-dw0xxx                                 | 2019 | 30      | 39      | 1           |
| Acer                 | Aspire E1-522                                    | 2013 | 30      | 39      | 1           |
| Acer                 | Aspire 5741G                                     | 2010 | 30      | 39      | 1           |
| Hewlett-Packard      | G60                                              | 2008 | 30      | 39      | 1           |
| System76             | Gazelle                                          | 2016 | 29      | 55      | 1           |
| Dell                 | Vostro 5490                                      | 2019 | 29      | 46      | 1           |
| Lenovo               | IdeaPad 320-15ABR 80XS                           | 2017 | 29      | 43      | 1           |
| Acer                 | Aspire 5735                                      | 2008 | 29      | 43      | 1           |
| ASUSTek Computer     | X540LJ                                           | 2015 | 29      | 39      | 1           |
| Dell                 | Inspiron 7720                                    | 2012 | 29      | 37      | 1           |
| ASUSTek Computer     | P50IJ                                            | 2009 | 29      | 37      | 1           |
| Acer                 | Aspire ES1-531                                   | 2015 | 29      | 32      | 1           |
| ASUSTek Computer     | 1011PX                                           | 2011 | 29      | 32      | 1           |
| Acer                 | Aspire 7741                                      | 2010 | 28      | 49      | 1           |
| Apple                | MacBookPro14,1                                   | 2017 | 28      | 47      | 1           |
| HUAWEI               | KPL-W0X                                          | 2018 | 28      | 43      | 1           |
| Dell                 | Inspiron 3541                                    | 2014 | 28      | 40      | 1           |
| Dell                 | Inspiron N7110                                   | 2011 | 28      | 38      | 1           |
| Lenovo               | IdeaPad 330S-15IKB 81F5                          | 2018 | 28      | 37      | 1           |
| ASUSTek Computer     | X510UAR                                          | 2017 | 28      | 35      | 1           |
| Samsung Electronics  | 300E5M/300E5L                                    | 2017 | 28      | 33      | 1           |
| ASUSTek Computer     | F5SL                                             | 2008 | 28      | 29      | 1           |
| Dell                 | Latitude E5410                                   | 2010 | 27      | 43      | 1           |
| Acer                 | Aspire 5733Z                                     | 2011 | 27      | 40      | 1           |
| Acer                 | Aspire 5315                                      | 2007 | 27      | 38      | 1           |
| Apple                | MacBookAir6,2                                    | 2013 | 27      | 37      | 1           |
| Toshiba              | Satellite L40                                    | 2007 | 27      | 37      | 1           |
| Hewlett-Packard      | 250 G4                                           | 2015 | 27      | 36      | 1           |
| Hewlett-Packard      | ProBook 4730s                                    | 2011 | 26      | 55      | 1           |
| Acer                 | Predator PH315-52                                | 2019 | 26      | 50      | 1           |
| Dell                 | Inspiron 5593                                    | 2019 | 26      | 45      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X580GD_N580GD                | 2018 | 26      | 44      | 1           |
| Apple                | MacBook7,1                                       | 2010 | 26      | 43      | 1           |
| Acer                 | TravelMate 5720                                  | 2007 | 26      | 42      | 1           |
| Lenovo               | ThinkPad E470                                    | 2017 | 26      | 41      | 1           |
| Toshiba              | Satellite L50-B                                  | 2014 | 26      | 41      | 1           |
| Apple                | MacBook3,1                                       | 2008 | 26      | 41      | 1           |
| Hewlett-Packard      | Laptop 15-dy1xxx                                 | 2019 | 26      | 40      | 1           |
| Lenovo               | IdeaPad 700-15ISK 80RU                           | 2015 | 26      | 38      | 1           |
| Dell                 | Inspiron 5547                                    | 2014 | 26      | 37      | 1           |
| Samsung Electronics  | NC210/NC110                                      | 2011 | 26      | 36      | 1           |
| Lenovo               | B50-10 80QR                                      | 2015 | 26      | 32      | 1           |
| Positivo             | S14CT01                                          | 2016 | 26      | 28      | 1           |
| System76             | Galago Pro                                       | 2017 | 25      | 57      | 1           |
| Lenovo               | ThinkPad Edge                                    | 2010 | 25      | 52      | 1           |
| HUAWEI               | WRT-WX9                                          | 2018 | 25      | 50      | 1           |
| ASUSTek Computer     | X556UQK                                          | 2016 | 25      | 44      | 1           |
| Acer                 | AO725                                            | 2012 | 25      | 42      | 1           |
| Samsung Electronics  | R528/R728                                        | 2009 | 25      | 38      | 1           |
| Lenovo               | IdeaPad 110-15ISK 80UD                           | 2016 | 25      | 35      | 1           |
| Samsung Electronics  | 300E4Z/300E5Z/300E7Z                             | 2011 | 25      | 33      | 1           |
| Hewlett-Packard      | Compaq Presario C700                             | 2007 | 25      | 32      | 1           |
| Digibras             | NH4CU53                                          | 2013 | 25      | 31      | 1           |
| Gateway              | NE56R                                            | 2012 | 24      | 46      | 1           |
| Hewlett-Packard      | EliteBook 850 G3                                 | 2016 | 24      | 42      | 1           |
| Lenovo               | ThinkPad X230 Tablet                             | 2013 | 24      | 42      | 1           |
| Lenovo               | ThinkPad X131e                                   | 2012 | 24      | 42      | 1           |
| Lenovo               | IdeaPad 330-15ICH 81FK                           | 2018 | 24      | 40      | 1           |
| Packard Bell         | EasyNote TK85                                    | 2010 | 24      | 40      | 1           |
| Dell                 | Vostro 1000                                      | 2006 | 24      | 40      | 1           |
| Lenovo               | IdeaPad Y510P 20217                              | 2013 | 24      | 39      | 1           |
| Lenovo               | Yoga 2 Pro 20266                                 | 2013 | 24      | 34      | 1           |
| ASUSTek Computer     | X541SA                                           | 2016 | 24      | 31      | 1           |
| ASUSTek Computer     | E200HA                                           | 2016 | 24      | 25      | 1           |
| Lenovo               | IdeaPad S340-15API 81NC                          | 2019 | 23      | 50      | 1           |
| Dell                 | Inspiron 5770                                    | 2017 | 23      | 50      | 1           |
| Dell                 | Latitude 5590                                    | 2018 | 23      | 49      | 1           |
| Hewlett-Packard      | ProBook 440 G6                                   | 2019 | 23      | 44      | 1           |
| ASUSTek Computer     | X540YA                                           | 2016 | 23      | 44      | 1           |
| Apple                | MacBookPro9,1                                    | 2015 | 23      | 44      | 1           |
| Toshiba              | Satellite C650                                   | 2010 | 23      | 43      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X507MA_X507MA            | 2018 | 23      | 41      | 1           |
| Lenovo               | V130-15IKB 81HN                                  | 2018 | 23      | 40      | 1           |
| Hewlett-Packard      | 625                                              | 2010 | 23      | 40      | 1           |
| Hewlett-Packard      | ProBook 4510s                                    | 2009 | 23      | 40      | 1           |
| Hewlett-Packard      | Compaq 15                                        | 2014 | 23      | 39      | 1           |
| Hewlett-Packard      | Compaq nx7400                                    | 2006 | 23      | 37      | 1           |
| Dell                 | Inspiron 11-3168                                 | 2015 | 23      | 33      | 1           |
| ASUSTek Computer     | T100TA                                           | 2013 | 23      | 28      | 1           |
| Dell                 | Latitude 7390                                    | 2018 | 22      | 48      | 1           |
| Lenovo               | ThinkPad SL510                                   | 2009 | 22      | 48      | 1           |
| Lenovo               | ThinkPad R500                                    | 2008 | 22      | 48      | 1           |
| Hewlett-Packard      | ProBook 450 G8 Notebook PC                       | 2020 | 22      | 45      | 1           |
| Lenovo               | Legion Y540-15IRH-PG0 81SY                       | 2019 | 22      | 44      | 1           |
| Dell                 | Inspiron 5577                                    | 2018 | 22      | 44      | 1           |
| Hewlett-Packard      | G72                                              | 2010 | 22      | 43      | 1           |
| Hewlett-Packard      | EliteBook Folio 1040 G3                          | 2015 | 22      | 42      | 1           |
| Hewlett-Packard      | Laptop 17-bs0xx                                  | 2017 | 22      | 41      | 1           |
| Dell                 | Inspiron 5458                                    | 2015 | 22      | 41      | 1           |
| Acer                 | Aspire E5-521                                    | 2014 | 22      | 40      | 1           |
| Dell                 | Inspiron 5590                                    | 2019 | 22      | 39      | 1           |
| Lenovo               | IdeaPad 320-15AST 80XV                           | 2017 | 22      | 39      | 1           |
| Lenovo               | V110-15ISK 80TL                                  | 2016 | 22      | 39      | 1           |
| Apple                | MacBook5,2                                       | 2009 | 22      | 39      | 1           |
| Dell                 | Inspiron 5557                                    | 2016 | 22      | 38      | 1           |
| Dell                 | Inspiron 5548                                    | 2015 | 22      | 38      | 1           |
| Hewlett-Packard      | Compaq Presario CQ61                             | 2009 | 22      | 37      | 1           |
| Hewlett-Packard      | Laptop 14-ck0xxx                                 | 2018 | 22      | 34      | 1           |
| Toshiba              | Satellite C55-C                                  | 2015 | 22      | 34      | 1           |
| Acer                 | AOD255                                           | 2010 | 22      | 34      | 1           |
| Hewlett-Packard      | OMEN by HP Laptop 15-dc0xxx                      | 2018 | 21      | 51      | 1           |
| Lenovo               | ThinkPad R61                                     | 2007 | 21      | 44      | 1           |
| Acer                 | Aspire 5742                                      | 2010 | 21      | 42      | 1           |
| Acer                 | Aspire V3-772G                                   | 2013 | 21      | 41      | 1           |
| Toshiba              | Satellite C660D                                  | 2010 | 21      | 41      | 1           |
| Lenovo               | G575 20081                                       | 2011 | 21      | 40      | 1           |
| Hewlett-Packard      | Compaq Presario CQ50                             | 2008 | 21      | 40      | 1           |
| ASUSTek Computer     | X550VX                                           | 2017 | 21      | 38      | 1           |
| Samsung Electronics  | RV410/RV510/S3510/E3510                          | 2010 | 21      | 37      | 1           |
| ASUSTek Computer     | X510UNR                                          | 2017 | 21      | 36      | 1           |
| Lenovo               | Flex 2-15 20405                                  | 2014 | 21      | 36      | 1           |
| Hewlett-Packard      | Laptop 17-by0xxx                                 | 2018 | 21      | 34      | 1           |
| ASUSTek Computer     | X502CA                                           | 2013 | 21      | 33      | 1           |
| ASUSTek Computer     | X751SA                                           | 2015 | 21      | 32      | 1           |
| Toshiba              | Satellite C50-B                                  | 2014 | 21      | 32      | 1           |
| Pine Microsystems    | Pine64 Pinebook Pro                              | 2021 | 21      | 21      | 1           |
| Dell                 | XPS 15 9510                                      | 2021 | 20      | 58      | 1           |
| Acer                 | Aspire A315-42G                                  | 2019 | 20      | 47      | 1           |
| Lenovo               | ThinkPad L390                                    | 2018 | 20      | 47      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X570ZD_X570ZD                | 2018 | 20      | 44      | 1           |
| Hewlett-Packard      | Pavilion dv9700                                  | 2007 | 20      | 42      | 1           |
| Toshiba              | Satellite C855D                                  | 2012 | 20      | 41      | 1           |
| Acer                 | Aspire ES1-520                                   | 2015 | 20      | 39      | 1           |
| eMachines            | E525                                             | 2009 | 20      | 38      | 1           |
| Lenovo               | IdeaPad 310-15IKB 80TV                           | 2016 | 20      | 37      | 1           |
| Samsung Electronics  | R59P/R60P/R61P                                   | 2007 | 20      | 37      | 1           |
| Acer                 | Aspire A315-54                                   | 2019 | 20      | 35      | 1           |
| ASUSTek Computer     | K53SJ                                            | 2011 | 20      | 35      | 1           |
| Samsung Electronics  | N145P/N250P/N260P                                | 2010 | 20      | 34      | 1           |
| ASUSTek Computer     | X551CAP                                          | 2013 | 20      | 33      | 1           |
| Dell                 | Inspiron 14-3452                                 | 2015 | 20      | 31      | 1           |
| Acer                 | Aspire A517-51G                                  | 2017 | 19      | 48      | 1           |
| Hewlett-Packard      | EliteBook 8770w                                  | 2012 | 19      | 46      | 1           |
| Dell                 | Inspiron 1720                                    | 2007 | 19      | 46      | 1           |
| Dell                 | Studio 1737                                      | 2008 | 19      | 45      | 1           |
| Lenovo               | ThinkPad L440                                    | 2014 | 19      | 44      | 1           |
| Dell                 | Inspiron MP061                                   | 2005 | 19      | 44      | 1           |
| Acer                 | Swift SF314-56                                   | 2018 | 19      | 43      | 1           |
| Fujitsu              | LIFEBOOK AH530                                   | 2010 | 19      | 43      | 1           |
| Hewlett-Packard      | ProBook 4720s                                    | 2010 | 19      | 42      | 1           |
| Acer                 | Aspire 4752                                      | 2011 | 19      | 41      | 1           |
| Acer                 | Aspire ES1-521                                   | 2015 | 19      | 40      | 1           |
| Samsung Electronics  | R530/R730/P530                                   | 2010 | 19      | 40      | 1           |
| Acer                 | Aspire A315-56                                   | 2020 | 19      | 39      | 1           |
| Acer                 | Swift SF314-52                                   | 2017 | 19      | 39      | 1           |
| Notebook             | W54_55SU1,SUW                                    | 2013 | 19      | 38      | 1           |
| Apple                | MacBookAir4,2                                    | 2012 | 19      | 37      | 1           |
| Dell                 | Inspiron 5748                                    | 2014 | 19      | 36      | 1           |
| Acer                 | Aspire E1-570G                                   | 2013 | 19      | 36      | 1           |
| Acer                 | TravelMate P253                                  | 2012 | 19      | 36      | 1           |
| Samsung Electronics  | 370E4K                                           | 2015 | 19      | 35      | 1           |
| Acer                 | AO756                                            | 2012 | 19      | 34      | 1           |
| Acer                 | AOD260                                           | 2010 | 19      | 34      | 1           |
| ASUSTek Computer     | 1005HA                                           | 2009 | 19      | 34      | 1           |
| Acer                 | Aspire ES1-571                                   | 2016 | 19      | 33      | 1           |
| Dell                 | Studio 1558                                      | 2010 | 18      | 58      | 1           |
| Acer                 | Aspire VX5-591G                                  | 2017 | 18      | 47      | 1           |
| ASUSTek Computer     | GL753VD                                          | 2018 | 18      | 45      | 1           |
| Dell                 | Inspiron 1520                                    | 2007 | 18      | 45      | 1           |
| Dell                 | Inspiron 3580                                    | 2018 | 18      | 44      | 1           |
| Acer                 | Aspire 6930G                                     | 2008 | 18      | 44      | 1           |
| ASUSTek Computer     | K72Jr                                            | 2009 | 18      | 43      | 1           |
| ASUSTek Computer     | ZenBook UX433FA_UX433FA                          | 2018 | 18      | 42      | 1           |
| Dell                 | Vostro 5471                                      | 2017 | 18      | 42      | 1           |
| Lenovo               | IdeaPad S340-15IWL 81N8                          | 2019 | 18      | 41      | 1           |
| ASUSTek Computer     | TUF GAMING FX504GD_FX80GD                        | 2018 | 18      | 41      | 1           |
| Acer                 | Aspire E5-576G                                   | 2017 | 18      | 41      | 1           |
| Timi                 | TM1613                                           | 2016 | 18      | 41      | 1           |
| ASUSTek Computer     | N550JK                                           | 2014 | 18      | 41      | 1           |
| ASUSTek Computer     | G75VW                                            | 2012 | 18      | 39      | 1           |
| Lenovo               | IdeaPad S145-15IIL 81W8                          | 2019 | 18      | 37      | 1           |
| ASUSTek Computer     | X541UVK                                          | 2017 | 18      | 37      | 1           |
| Hewlett-Packard      | Laptop 15-dw1xxx                                 | 2019 | 18      | 36      | 1           |
| Apple                | MacBookAir5,2                                    | 2015 | 18      | 35      | 1           |
| Acer                 | Aspire ES1-111M                                  | 2014 | 18      | 35      | 1           |
| ASUSTek Computer     | K50IN                                            | 2009 | 18      | 35      | 1           |
| Lenovo               | V15-IIL 82C5                                     | 2020 | 18      | 34      | 1           |
| Positivo             | H14BT58                                          | 2014 | 18      | 33      | 1           |
| Acer                 | AOD255E                                          | 2010 | 18      | 33      | 1           |
| Hewlett-Packard      | 530                                              | 2007 | 18      | 32      | 1           |
| ASUSTek Computer     | X580VD                                           | 2017 | 17      | 56      | 1           |
| Jumper               | EZbook                                           | 2016 | 17      | 49      | 1           |
| Hewlett-Packard      | ProBook 4330s                                    | 2011 | 17      | 49      | 1           |
| Dell                 | Inspiron 5575                                    | 2018 | 17      | 47      | 1           |
| ASUSTek Computer     | N61Jv                                            | 2010 | 17      | 47      | 1           |
| Dell                 | Inspiron 5584                                    | 2019 | 17      | 46      | 1           |
| Hewlett-Packard      | EliteBook 840 G4                                 | 2017 | 17      | 46      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FA_X509FA                | 2019 | 17      | 45      | 1           |
| Lenovo               | ThinkBook 14-IML 20RV                            | 2019 | 17      | 44      | 1           |
| ASUSTek Computer     | K52N                                             | 2010 | 17      | 44      | 1           |
| Dell                 | Latitude E5510                                   | 2010 | 17      | 43      | 1           |
| Hewlett-Packard      | Compaq Presario CQ40                             | 2008 | 17      | 43      | 1           |
| Lenovo               | V15-ADA 82C7                                     | 2020 | 17      | 42      | 1           |
| Lenovo               | ThinkBook 14-IIL 20SL                            | 2019 | 17      | 42      | 1           |
| Lenovo               | ThinkPad X270 W10DG                              | 2017 | 17      | 42      | 1           |
| Hewlett-Packard      | 655                                              | 2012 | 17      | 42      | 1           |
| Lenovo               | IdeaPad 5 14IIL05 81YH                           | 2020 | 17      | 41      | 1           |
| Dell                 | Inspiron 3180                                    | 2018 | 17      | 41      | 1           |
| Dell                 | Inspiron 7460                                    | 2017 | 17      | 41      | 1           |
| Lenovo               | ThinkPad E420                                    | 2011 | 17      | 41      | 1           |
| Acer                 | Aspire 4736Z                                     | 2009 | 17      | 41      | 1           |
| Dell                 | Vostro 3578                                      | 2018 | 17      | 40      | 1           |
| Apple                | MacBookPro11,4                                   | 2017 | 17      | 40      | 1           |
| Samsung Electronics  | R560                                             | 2008 | 17      | 39      | 1           |
| Dell                 | Vostro A860                                      | 2008 | 17      | 39      | 1           |
| Lenovo               | ThinkPad L430                                    | 2012 | 17      | 38      | 1           |
| ASUSTek Computer     | F5RL                                             | 2007 | 17      | 38      | 1           |
| Apple                | MacBook6,1                                       | 2009 | 17      | 36      | 1           |
| Samsung Electronics  | 270E5J/2570EJ                                    | 2014 | 17      | 35      | 1           |
| Lenovo               | Yoga 3 Pro-1370 80HE                             | 2014 | 17      | 32      | 1           |
| Lenovo               | IdeaPad 100-14IBY 80MH                           | 2015 | 17      | 31      | 1           |
| Samsung Electronics  | 530U3C/530U4C/532U3C                             | 2012 | 17      | 31      | 1           |
| System76             | Darter Pro                                       | 2019 | 16      | 52      | 1           |
| Hewlett-Packard      | EliteBook 850 G7 Notebook PC                     | 2020 | 16      | 47      | 1           |
| Dell                 | Inspiron N4030                                   | 2010 | 16      | 45      | 1           |
| Dell                 | Inspiron N4010                                   | 2010 | 16      | 44      | 1           |
| Dell                 | Vostro 1500                                      | 2007 | 16      | 42      | 1           |
| HUAWEI               | MACHC-WAX9                                       | 2020 | 16      | 40      | 1           |
| Hewlett-Packard      | Laptop 15s-fq1xxx                                | 2019 | 16      | 39      | 1           |
| Dell                 | Inspiron 5448                                    | 2015 | 16      | 39      | 1           |
| Acer                 | Aspire ES1-522                                   | 2016 | 16      | 38      | 1           |
| Dell                 | Vostro 3558                                      | 2015 | 16      | 38      | 1           |
| Samsung Electronics  | 550XBE/350XBE                                    | 2019 | 16      | 36      | 1           |
| Hewlett-Packard      | Mini 110-3500                                    | 2010 | 16      | 36      | 1           |
| ASUSTek Computer     | K53SM                                            | 2011 | 16      | 35      | 1           |
| ASUSTek Computer     | E402SA                                           | 2015 | 16      | 33      | 1           |
| Dell                 | Inspiron 11 - 3147                               | 2014 | 16      | 29      | 1           |
| Positivo             | Q232A                                            | 2018 | 16      | 26      | 1           |
| Positivo             | CHT14B                                           | 2017 | 16      | 26      | 1           |
| Dell                 | Precision M3800                                  | 2014 | 15      | 46      | 1           |
| Dell                 | Studio 1555                                      | 2009 | 15      | 46      | 1           |
| Dell                 | Inspiron 3505                                    | 2020 | 15      | 45      | 1           |
| Razer                | Blade Stealth                                    | 2015 | 15      | 45      | 1           |
| Hewlett-Packard      | ProBook 4320s                                    | 2010 | 15      | 45      | 1           |
| Dell                 | Latitude 5420                                    | 2020 | 15      | 44      | 1           |
| Lenovo               | ThinkPad X61s                                    | 2007 | 15      | 44      | 1           |
| Lenovo               | Yoga Slim 7 14ARE05 82A2                         | 2020 | 15      | 43      | 1           |
| Acer                 | Aspire 7720                                      | 2007 | 15      | 43      | 1           |
| ASUSTek Computer     | ZenBook UX433FN_UX433FN                          | 2018 | 15      | 42      | 1           |
| Acer                 | Predator G3-572                                  | 2017 | 15      | 42      | 1           |
| Lenovo               | ThinkPad R400                                    | 2009 | 15      | 41      | 1           |
| Hewlett-Packard      | Compaq 615                                       | 2009 | 15      | 40      | 1           |
| Hewlett-Packard      | Compaq nx7300                                    | 2006 | 15      | 40      | 1           |
| Hewlett-Packard      | Pavilion Power Laptop 15-cb0xx                   | 2017 | 15      | 39      | 1           |
| Timi                 | RedmiBook 16                                     | 2020 | 15      | 38      | 1           |
| Dell                 | Inspiron 5758                                    | 2015 | 15      | 38      | 1           |
| Apple                | MacBookAir4,1                                    | 2012 | 15      | 36      | 1           |
| Samsung Electronics  | 3570R/370R/470R/450R/510R/4450RV                 | 2013 | 15      | 33      | 1           |
| Samsung Electronics  | 300E4A/300E5A/300E7A                             | 2011 | 15      | 33      | 1           |
| Acer                 | V5-171                                           | 2012 | 15      | 32      | 1           |
| Lenovo               | G470 20078                                       | 2011 | 15      | 31      | 1           |
| ASUSTek Computer     | T100HAN                                          | 2015 | 15      | 25      | 1           |
| Dell                 | Studio 1747                                      | 2010 | 14      | 67      | 1           |
| Dell                 | Vostro 3700                                      | 2010 | 14      | 56      | 1           |
| Lenovo               | IdeaPad Gaming 3 15IMH05 81Y4                    | 2020 | 14      | 55      | 1           |
| Lenovo               | ThinkBook 14 G2 ITL 20VD                         | 2020 | 14      | 51      | 1           |
| Chuwi                | LapBook SE                                       | 2018 | 14      | 51      | 1           |
| Hewlett-Packard      | ZBook 17 G2                                      | 2014 | 14      | 50      | 1           |
| ASUSTek Computer     | ZenBook UX425EA_UX425EA                          | 2020 | 14      | 48      | 1           |
| ASUSTek Computer     | ZenBook UX533FD_UX533FD                          | 2018 | 14      | 48      | 1           |
| Hewlett-Packard      | Pavilion Laptop 15-eh0xxx                        | 2020 | 14      | 44      | 1           |
| ASUSTek Computer     | K52JU                                            | 2010 | 14      | 44      | 1           |
| Hewlett-Packard      | OMEN by HP Laptop 15-ce0xx                       | 2017 | 14      | 43      | 1           |
| Acer                 | Predator PH315-51                                | 2018 | 14      | 42      | 1           |
| Acer                 | Nitro AN515-53                                   | 2018 | 14      | 42      | 1           |
| ASUSTek Computer     | K52Dr                                            | 2010 | 14      | 42      | 1           |
| Hewlett-Packard      | Pavilion Sleekbook 15                            | 2012 | 14      | 41      | 1           |
| Lenovo               | ThinkPad L520                                    | 2011 | 14      | 40      | 1           |
| ASUSTek Computer     | N53SM                                            | 2011 | 14      | 40      | 1           |
| Lenovo               | G560 0679                                        | 2010 | 14      | 40      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MA_R540MA            | 2018 | 14      | 39      | 1           |
| ASUSTek Computer     | X550LN                                           | 2014 | 14      | 39      | 1           |
| ASUSTek Computer     | 1015B                                            | 2011 | 14      | 39      | 1           |
| Acer                 | Aspire 4720Z                                     | 2007 | 14      | 39      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MB_X540MB            | 2018 | 14      | 38      | 1           |
| Pegatron             | A15                                              | 2011 | 14      | 37      | 1           |
| Pegatron             | A15W8                                            | 2012 | 14      | 35      | 1           |
| Packard Bell         | EasyNote TV11HC                                  | 2012 | 14      | 35      | 1           |
| Lenovo               | IdeaPad 330-15IKB 81DC                           | 2018 | 14      | 34      | 1           |
| Lenovo               | Yoga 300-11IBR 80M1                              | 2015 | 14      | 34      | 1           |
| ASUSTek Computer     | X202E                                            | 2012 | 14      | 33      | 1           |
| Intel                | HuronRiver Platform                              | 2011 | 14      | 33      | 1           |
| Acer                 | Aspire E5-574                                    | 2015 | 14      | 32      | 1           |
| ASUSTek Computer     | X553SA                                           | 2015 | 14      | 32      | 1           |
| Samsung Electronics  | 270E5G/270E5U                                    | 2013 | 14      | 32      | 1           |
| Hewlett-Packard      | Mini 210-1000                                    | 2009 | 14      | 32      | 1           |
| Acer                 | AO532h                                           | 2009 | 14      | 32      | 1           |
| Lenovo               | IdeaPad Z470                                     | 2011 | 14      | 31      | 1           |
| ASUSTek Computer     | X58C                                             | 2008 | 14      | 28      | 1           |
| ASUSTek Computer     | G73Jh                                            | 2010 | 13      | 58      | 1           |
| Samsung Electronics  | RF510/RF410/RF710                                | 2010 | 13      | 56      | 1           |
| Alienware            | 17 R4                                            | 2017 | 13      | 55      | 1           |
| Hewlett-Packard      | EliteBook 8530p                                  | 2008 | 13      | 51      | 1           |
| Lenovo               | IdeaPad Slim 1-14AST-05 81VS                     | 2019 | 13      | 47      | 1           |
| Hewlett-Packard      | Laptop 17-by3xxx                                 | 2020 | 13      | 46      | 1           |
| Chuwi                | GemiBook Pro                                     | 2020 | 13      | 46      | 1           |
| Dell                 | Vostro 5402                                      | 2020 | 13      | 45      | 1           |
| Dell                 | Inspiron 1420                                    | 2007 | 13      | 45      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X530FN_S530FN                | 2018 | 13      | 44      | 1           |
| Acer                 | Swift SF113-31                                   | 2017 | 13      | 43      | 1           |
| ASUSTek Computer     | N551JM                                           | 2014 | 13      | 43      | 1           |
| Samsung Electronics  | RV415/RV515/E3415                                | 2011 | 13      | 43      | 1           |
| Samsung Electronics  | RC410/RC510/RC710                                | 2011 | 13      | 43      | 1           |
| Acer                 | Nitro AN515-55                                   | 2020 | 13      | 42      | 1           |
| Lenovo               | ThinkPad L380                                    | 2018 | 13      | 42      | 1           |
| Acer                 | Aspire E5-551G                                   | 2014 | 13      | 42      | 1           |
| Medion               | P7624                                            | 2011 | 13      | 42      | 1           |
| Hewlett-Packard      | OMEN by Laptop                                   | 2016 | 13      | 41      | 1           |
| ASUSTek Computer     | G750JX                                           | 2013 | 13      | 41      | 1           |
| Acer                 | Extensa 5635ZG                                   | 2009 | 13      | 41      | 1           |
| Dell                 | Inspiron 7572                                    | 2018 | 13      | 40      | 1           |
| ASUSTek Computer     | N53SN                                            | 2011 | 13      | 40      | 1           |
| Lenovo               | ThinkPad X200 Tablet                             | 2009 | 13      | 40      | 1           |
| Lenovo               | IdeaPad 330-17IKB 81DM                           | 2018 | 13      | 39      | 1           |
| Lenovo               | E41-25 81FS                                      | 2018 | 13      | 39      | 1           |
| Lenovo               | ThinkPad E550                                    | 2014 | 13      | 39      | 1           |
| ASUSTek Computer     | UX32VD                                           | 2012 | 13      | 39      | 1           |
| ASUSTek Computer     | K50AB                                            | 2009 | 13      | 39      | 1           |
| Acer                 | Aspire 5520                                      | 2007 | 13      | 39      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X542UF                   | 2018 | 13      | 37      | 1           |
| Hewlett-Packard      | Stream Notebook PC 13                            | 2014 | 13      | 37      | 1           |
| Pegatron             | A35                                              | 2012 | 13      | 37      | 1           |
| Dell                 | System XPS L502X                                 | 2011 | 13      | 37      | 1           |
| ASUSTek Computer     | X751LJ                                           | 2015 | 13      | 36      | 1           |
| Samsung Electronics  | RV408/RV508                                      | 2010 | 13      | 36      | 1           |
| ASUSTek Computer     | K50IE                                            | 2010 | 13      | 36      | 1           |
| Fujitsu Siemens      | LIFEBOOK S7110                                   | 2006 | 13      | 36      | 1           |
| ASUSTek Computer     | X406UAR                                          | 2017 | 13      | 35      | 1           |
| ASUSTek Computer     | N56VJ                                            | 2012 | 13      | 35      | 1           |
| ASUSTek Computer     | F5N                                              | 2007 | 13      | 35      | 1           |
| Apple                | MacBook1,1                                       | 2006 | 13      | 35      | 1           |
| Lenovo               | IdeaPad 320-17IKB 80XM                           | 2017 | 13      | 33      | 1           |
| Lenovo               | IdeaPad Yoga 13 20175                            | 2012 | 13      | 33      | 1           |
| Notebook             | W54_W94_W955TU,-T,-C                             | 2014 | 13      | 32      | 1           |
| Hewlett-Packard      | 240 G6 Notebook PC                               | 2017 | 13      | 31      | 1           |
| Lenovo               | G40-30 80FY                                      | 2014 | 13      | 31      | 1           |
| AMI                  | Cherry Trail CR                                  | 2016 | 13      | 30      | 1           |
| Lenovo               | ThinkPad Yoga 11e                                | 2014 | 13      | 30      | 1           |
| Aquarius             | NS585                                            | 2020 | 13      | 29      | 1           |
| Google               | Edgar                                            | 2016 | 13      | 29      | 1           |
| Hewlett-Packard      | Stream Notebook PC 11                            | 2014 | 13      | 28      | 1           |
| Lenovo               | Legion Y530-15ICH-1060 81LB                      | 2018 | 12      | 48      | 1           |
| Hewlett-Packard      | 355 G2                                           | 2014 | 12      | 47      | 1           |
| Hewlett-Packard      | Pavilion Sleekbook 14 PC                         | 2012 | 12      | 46      | 1           |
| Dell                 | Inspiron 5502                                    | 2020 | 12      | 45      | 1           |
| Acer                 | Aspire 5720Z                                     | 2007 | 12      | 45      | 1           |
| Dell                 | 500                                              | 2008 | 12      | 44      | 1           |
| Lenovo               | IdeaPad 130-15AST 81H5                           | 2018 | 12      | 43      | 1           |
| Hewlett-Packard      | Pavilion Laptop 15-cc1xx                         | 2017 | 12      | 43      | 1           |
| Hewlett-Packard      | EliteBook 745 G2                                 | 2014 | 12      | 43      | 1           |
| Lenovo               | ThinkPad SL500                                   | 2008 | 12      | 43      | 1           |
| Acer                 | Aspire 7730G                                     | 2008 | 12      | 43      | 1           |
| Lenovo               | IdeaPad 530S-14ARR 81H1                          | 2018 | 12      | 42      | 1           |
| Samsung Electronics  | 305U1A                                           | 2011 | 12      | 42      | 1           |
| Lenovo               | 3000 G530 4151/200                               | 2008 | 12      | 42      | 1           |
| HUAWEI               | HLY-WX9XX                                        | 2019 | 12      | 41      | 1           |
| Acer                 | Predator G3-571                                  | 2017 | 12      | 41      | 1           |
| ASUSTek Computer     | VivoBook S15 X510UF                              | 2017 | 12      | 41      | 1           |
| Lenovo               | IdeaPad 310-15ABR 80ST                           | 2016 | 12      | 41      | 1           |
| ASUSTek Computer     | K56CM                                            | 2012 | 12      | 41      | 1           |
| ASUSTek Computer     | N750JK                                           | 2014 | 12      | 40      | 1           |
| Lenovo               | IdeaPad Y580                                     | 2012 | 12      | 40      | 1           |
| Hewlett-Packard      | 255 G3                                           | 2014 | 12      | 39      | 1           |
| Lenovo               | IdeaPad S340-14IWL 81N7                          | 2019 | 12      | 38      | 1           |
| Hewlett-Packard      | Laptop 14-df0xxx                                 | 2018 | 12      | 38      | 1           |
| Lenovo               | B50-80 80EW                                      | 2015 | 12      | 38      | 1           |
| ASUSTek Computer     | K73SV                                            | 2011 | 12      | 38      | 1           |
| Hewlett-Packard      | ProBook 470 G2                                   | 2014 | 12      | 37      | 1           |
| Samsung Electronics  | 530U3C/530U4C                                    | 2012 | 12      | 37      | 1           |
| Acer                 | Aspire 1410                                      | 2004 | 12      | 37      | 1           |
| Lenovo               | ThinkBook 13s-IWL 20R9                           | 2019 | 12      | 36      | 1           |
| Lenovo               | IdeaPad 120S-14IAP 81A5                          | 2017 | 12      | 36      | 1           |
| Lenovo               | V510-15IKB 80WQ                                  | 2016 | 12      | 36      | 1           |
| Hewlett-Packard      | ProBook 4430s                                    | 2011 | 12      | 36      | 1           |
| Hewlett-Packard      | Laptop 14-bp0xx                                  | 2017 | 12      | 35      | 1           |
| Lenovo               | Z70-80 80FG                                      | 2014 | 12      | 35      | 1           |
| Samsung Electronics  | 700Z3C/700Z5C                                    | 2012 | 12      | 34      | 1           |
| Hewlett-Packard      | Laptop 15-bs2xx                                  | 2018 | 12      | 33      | 1           |
| Positivo             | S14SL01                                          | 2017 | 12      | 33      | 1           |
| ASUSTek Computer     | UX303UB                                          | 2015 | 12      | 33      | 1           |
| Acer                 | AOHAPPY2                                         | 2011 | 12      | 33      | 1           |
| MSI                  | MS-N014                                          | 2010 | 12      | 31      | 1           |
| Acer                 | Aspire R3-131T                                   | 2015 | 12      | 30      | 1           |
| Hewlett-Packard      | EliteBook 8740w                                  | 2010 | 11      | 66      | 1           |
| Toshiba              | Satellite A665                                   | 2010 | 11      | 58      | 1           |
| Lenovo               | ThinkPad E585                                    | 2018 | 11      | 52      | 1           |
| Lenovo               | Legion 5 15IMH05H 81Y6                           | 2020 | 11      | 48      | 1           |
| Dell                 | Inspiron 15-3573                                 | 2018 | 11      | 47      | 1           |
| Hewlett-Packard      | Pavilion Aero Laptop 13-be0xxx                   | 2021 | 11      | 46      | 1           |
| Dell                 | Inspiron 7591                                    | 2019 | 11      | 46      | 1           |
| Acer                 | Aspire 5720                                      | 2007 | 11      | 46      | 1           |
| Acer                 | Aspire A515-44G                                  | 2020 | 11      | 45      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FJ_X512FJ                | 2019 | 11      | 45      | 1           |
| Dell                 | Inspiron N7010                                   | 2010 | 11      | 45      | 1           |
| Dell                 | Studio 1535                                      | 2008 | 11      | 45      | 1           |
| Hewlett-Packard      | Pavilion dv2000                                  | 2006 | 11      | 45      | 1           |
| ASUSTek Computer     | X510UQ                                           | 2017 | 11      | 44      | 1           |
| Dell                 | Latitude E4200                                   | 2009 | 11      | 44      | 1           |
| Toshiba              | Satellite A215                                   | 2007 | 11      | 44      | 1           |
| Lenovo               | IdeaPad 5 14ALC05 82LM                           | 2021 | 11      | 43      | 1           |
| Samsung Electronics  | RV413/RV513                                      | 2011 | 11      | 43      | 1           |
| Lenovo               | ThinkPad X301                                    | 2009 | 11      | 43      | 1           |
| Lenovo               | IdeaPad L340-15IRH Gaming 81TR                   | 2019 | 11      | 42      | 1           |
| Alienware            | M17xR3                                           | 2011 | 11      | 42      | 1           |
| Dell                 | Inspiron 5767                                    | 2016 | 11      | 41      | 1           |
| Hewlett-Packard      | EliteBook 2170p                                  | 2012 | 11      | 41      | 1           |
| Hewlett-Packard      | TouchSmart tm2                                   | 2010 | 11      | 41      | 1           |
| ASUSTek Computer     | K40IJ                                            | 2009 | 11      | 41      | 1           |
| Timi                 | RedmiBook 14 II                                  | 2020 | 11      | 40      | 1           |
| Dell                 | Inspiron 7580                                    | 2019 | 11      | 40      | 1           |
| Notebook             | W65_67SB                                         | 2015 | 11      | 40      | 1           |
| Toshiba              | Satellite C75D-B                                 | 2014 | 11      | 40      | 1           |
| MSI                  | GE60 2OC\2OD\2OE                                 | 2013 | 11      | 40      | 1           |
| Acer                 | Aspire 7736                                      | 2009 | 11      | 40      | 1           |
| Lenovo               | IdeaPad L340-15IWL 81LG                          | 2019 | 11      | 39      | 1           |
| Dell                 | Precision 3520                                   | 2017 | 11      | 39      | 1           |
| Toshiba              | Satellite C70D-B                                 | 2014 | 11      | 39      | 1           |
| Acer                 | Aspire V3-572G                                   | 2014 | 11      | 39      | 1           |
| Hewlett-Packard      | ProBook 4525s                                    | 2010 | 11      | 39      | 1           |
| Toshiba              | Satellite L500D                                  | 2009 | 11      | 39      | 1           |
| Acer                 | Aspire 7738                                      | 2009 | 11      | 39      | 1           |
| Hewlett-Packard      | Pavilion Laptop 15-cs2xxx                        | 2019 | 11      | 38      | 1           |
| Hewlett-Packard      | ENVY Laptop 13-ah0xxx                            | 2018 | 11      | 38      | 1           |
| ASUSTek Computer     | X542UAR                                          | 2017 | 11      | 38      | 1           |
| Acer                 | Extensa 2511G                                    | 2015 | 11      | 38      | 1           |
| Dell                 | Latitude E5250                                   | 2014 | 11      | 38      | 1           |
| Lenovo               | ThinkPad X100e                                   | 2010 | 11      | 38      | 1           |
| Lenovo               | G565 20071                                       | 2010 | 11      | 38      | 1           |
| Acer                 | Aspire 5810T                                     | 2009 | 11      | 38      | 1           |
| Fujitsu Siemens      | AMILO Pro Edition V3505                          | 2006 | 11      | 38      | 1           |
| Acer                 | Aspire 3680                                      | 2006 | 11      | 38      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MA_A540MA            | 2018 | 11      | 37      | 1           |
| Dell                 | Inspiron 5420                                    | 2012 | 11      | 37      | 1           |
| Dell                 | Latitude XT3                                     | 2011 | 11      | 37      | 1           |
| Hewlett-Packard      | ProBook 470 G3                                   | 2016 | 11      | 36      | 1           |
| Clevo                | W240EU/W250EUQ/W270EUQ                           | 2012 | 11      | 36      | 1           |
| Dell                 | Inspiron 3443                                    | 2014 | 11      | 35      | 1           |
| Dell                 | Latitude 7370                                    | 2017 | 11      | 34      | 1           |
| Lenovo               | G70-70 80HW                                      | 2014 | 11      | 34      | 1           |
| Dell                 | Inspiron 7348                                    | 2014 | 11      | 34      | 1           |
| Apple                | MacBookAir6,1                                    | 2014 | 11      | 34      | 1           |
| Packard Bell         | EasyNote TE69CX                                  | 2013 | 11      | 34      | 1           |
| Samsung Electronics  | N100                                             | 2011 | 11      | 34      | 1           |
| Hewlett-Packard      | ProBook 6360b                                    | 2011 | 11      | 34      | 1           |
| ASUSTek Computer     | K43E                                             | 2011 | 11      | 34      | 1           |
| Hewlett-Packard      | ProBook 440 G4                                   | 2016 | 11      | 33      | 1           |
| ASUSTek Computer     | 1015PX                                           | 2011 | 11      | 33      | 1           |
| Lenovo               | S10-3c 20074                                     | 2010 | 11      | 33      | 1           |
| Samsung Electronics  | 900X3C/900X3D/900X4C/900X4D                      | 2012 | 11      | 32      | 1           |
| Hewlett-Packard      | G7000                                            | 2007 | 11      | 32      | 1           |
| Lenovo               | B50-30 80ES                                      | 2014 | 11      | 31      | 1           |
| Samsung Electronics  | 300V3A/300V4A/300V5A                             | 2011 | 11      | 31      | 1           |
| Intel                | ChiefRiver                                       | 2012 | 11      | 30      | 1           |
| Toshiba              | Satellite L30                                    | 2006 | 11      | 29      | 1           |
| ASUSTek Computer     | F5V                                              | 2007 | 11      | 28      | 1           |
| Digma                | CITI E401 ET4007EW                               | 2017 | 11      | 26      | 1           |
| Dell                 | Inspiron 11-3162                                 | 2016 | 11      | 26      | 1           |
| Toshiba              | Satellite A660                                   | 2010 | 10      | 61      | 1           |
| Hewlett-Packard      | ZBook Studio G3                                  | 2016 | 10      | 53      | 1           |
| Hewlett-Packard      | EliteBook 8530w                                  | 2008 | 10      | 52      | 1           |
| Dell                 | G7 7790                                          | 2019 | 10      | 51      | 1           |
| Razer                | Blade 14 - RZ09-0370                             | 2021 | 10      | 48      | 1           |
| Dell                 | Latitude 5510                                    | 2020 | 10      | 48      | 1           |
| Lenovo               | ThinkPad L512                                    | 2010 | 10      | 48      | 1           |
| Hewlett-Packard      | ZBook 14u G6                                     | 2019 | 10      | 47      | 1           |
| Dell                 | Precision M2800                                  | 2015 | 10      | 46      | 1           |
| Hewlett-Packard      | ProBook 6475b                                    | 2012 | 10      | 46      | 1           |
| Dell                 | Inspiron 3585                                    | 2018 | 10      | 45      | 1           |
| Acer                 | Aspire 6920                                      | 2008 | 10      | 45      | 1           |
| Dell                 | Vostro 3400                                      | 2010 | 10      | 44      | 1           |
| Acer                 | Aspire 5745G                                     | 2010 | 10      | 44      | 1           |
| Hewlett-Packard      | Compaq 6715s                                     | 2007 | 10      | 44      | 1           |
| Samsung Electronics  | R540/R580/R780/SA41/E452                         | 2010 | 10      | 43      | 1           |
| Lenovo               | ThinkPad L540                                    | 2014 | 10      | 42      | 1           |
| ASUSTek Computer     | X751LD                                           | 2014 | 10      | 42      | 1           |
| Dell                 | Latitude E6430s                                  | 2013 | 10      | 42      | 1           |
| Hewlett-Packard      | Pavilion dv9500                                  | 2007 | 10      | 42      | 1           |
| Lenovo               | ThinkBook 13s G2 ITL 20V9                        | 2020 | 10      | 41      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FL_X512FL                | 2019 | 10      | 41      | 1           |
| eMachines            | G725                                             | 2009 | 10      | 41      | 1           |
| Hewlett-Packard      | Compaq 2510p                                     | 2007 | 10      | 41      | 1           |
| Hewlett-Packard      | Laptop 17-by2xxx                                 | 2019 | 10      | 40      | 1           |
| Toshiba              | Satellite L50D-B                                 | 2014 | 10      | 40      | 1           |
| Acer                 | Aspire V3-551                                    | 2012 | 10      | 40      | 1           |
| Toshiba              | Satellite C655                                   | 2010 | 10      | 40      | 1           |
| Hewlett-Packard      | G70                                              | 2008 | 10      | 40      | 1           |
| Lenovo               | ThinkPad X61                                     | 2007 | 10      | 40      | 1           |
| Lenovo               | ThinkBook 14s-IWL 20RM                           | 2019 | 10      | 39      | 1           |
| MSI                  | GL63 8RC                                         | 2018 | 10      | 39      | 1           |
| ASUSTek Computer     | K501UX                                           | 2015 | 10      | 39      | 1           |
| ASUSTek Computer     | X555LA                                           | 2014 | 10      | 39      | 1           |
| Dell                 | Inspiron 1440                                    | 2009 | 10      | 39      | 1           |
| Acer                 | AOA110                                           | 2008 | 10      | 39      | 1           |
| Acer                 | Swift SF314-59                                   | 2020 | 10      | 38      | 1           |
| Dell                 | Inspiron 7347                                    | 2014 | 10      | 38      | 1           |
| Acer                 | Extensa 2510G                                    | 2014 | 10      | 38      | 1           |
| ASUSTek Computer     | K45VM                                            | 2012 | 10      | 38      | 1           |
| Samsung Electronics  | RV409/RV509/RV709                                | 2011 | 10      | 38      | 1           |
| Dell                 | Vostro 3750                                      | 2011 | 10      | 38      | 1           |
| Lenovo               | ThinkPad X200s                                   | 2009 | 10      | 38      | 1           |
| ASUSTek Computer     | X541NC                                           | 2016 | 10      | 37      | 1           |
| ASUSTek Computer     | K75VJ                                            | 2012 | 10      | 37      | 1           |
| Toshiba              | Satellite L745                                   | 2011 | 10      | 37      | 1           |
| Lenovo               | ThinkBook 13s-IML 20RR                           | 2019 | 10      | 36      | 1           |
| Dell                 | Latitude 3490                                    | 2018 | 10      | 36      | 1           |
| Lenovo               | ThinkPad E450                                    | 2014 | 10      | 36      | 1           |
| Hewlett-Packard      | 350 G2                                           | 2014 | 10      | 36      | 1           |
| Clevo                | W240EL/W250ELQ/W270ELQ                           | 2012 | 10      | 36      | 1           |
| Acer                 | Aspire E5-574G                                   | 2015 | 10      | 35      | 1           |
| ASUSTek Computer     | U36SD                                            | 2011 | 10      | 35      | 1           |
| eMachines            | eM250                                            | 2009 | 10      | 35      | 1           |
| Acer                 | Aspire E1-532                                    | 2013 | 10      | 34      | 1           |
| ASUSTek Computer     | X501A                                            | 2012 | 10      | 34      | 1           |
| Samsung Electronics  | N102                                             | 2011 | 10      | 34      | 1           |
| Acer                 | Aspire V5-471                                    | 2012 | 10      | 33      | 1           |
| Acer                 | TravelMate 5760                                  | 2011 | 10      | 33      | 1           |
| MSI                  | U-100                                            | 2008 | 10      | 32      | 1           |
| Lenovo               | IdeaPad 320-14IKB 80YF                           | 2017 | 10      | 31      | 1           |
| Acer                 | Aspire S3                                        | 2011 | 10      | 30      | 1           |
| Hewlett-Packard      | Presario C500                                    | 2006 | 10      | 30      | 1           |
| Lenovo               | Yoga 2 11 20332                                  | 2013 | 10      | 28      | 1           |
| Hewlett-Packard      | ElitePad 1000 G2                                 | 2014 | 10      | 25      | 1           |
| Insyde               | CherryTrail                                      | 2015 | 10      | 23      | 1           |
| Lenovo               | Legion Y540-17IRH 81Q4                           | 2019 | 9       | 59      | 1           |
| System76             | Serval WS                                        | 2015 | 9       | 58      | 1           |
| Lenovo               | ThinkBook 14 G2 ARE 20VF                         | 2020 | 9       | 57      | 1           |
| ASUSTek Computer     | ZenBook UX391FA_UX391FA                          | 2019 | 9       | 52      | 1           |
| Dell                 | Precision 7720                                   | 2017 | 9       | 50      | 1           |
| Hewlett-Packard      | Laptop 15-ef0xxx                                 | 2020 | 9       | 47      | 1           |
| MSI                  | GS65 Stealth Thin 8RF                            | 2018 | 9       | 47      | 1           |
| Dell                 | XPS 15 9530                                      | 2014 | 9       | 47      | 1           |
| Dell                 | Studio 1735                                      | 2008 | 9       | 47      | 1           |
| ASUSTek Computer     | M51Vr                                            | 2008 | 9       | 47      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X570ZD_F570ZD                | 2018 | 9       | 46      | 1           |
| TUXEDO               | Aura 15 Gen1                                     | 2020 | 9       | 45      | 1           |
| Hewlett-Packard      | ZBook 14                                         | 2014 | 9       | 45      | 1           |
| Dell                 | Vostro 1700                                      | 2007 | 9       | 45      | 1           |
| Dell                 | Inspiron 5515                                    | 2021 | 9       | 44      | 1           |
| AMI                  | Intel                                            | 2020 | 9       | 44      | 1           |
| MSI                  | MS-1688                                          | 2010 | 9       | 44      | 1           |
| Toshiba              | Satellite A210                                   | 2007 | 9       | 44      | 1           |
| MSI                  | GF63 Thin 9SC                                    | 2019 | 9       | 43      | 1           |
| ASUSTek Computer     | GL503VD                                          | 2018 | 9       | 43      | 1           |
| Lenovo               | G70-35 80Q5                                      | 2015 | 9       | 43      | 1           |
| Pegatron             | C15B                                             | 2013 | 9       | 43      | 1           |
| Hewlett-Packard      | Laptop 15-dw3xxx                                 | 2020 | 9       | 42      | 1           |
| Hewlett-Packard      | 255 G1                                           | 2013 | 9       | 42      | 1           |
| Hewlett-Packard      | Compaq 6715b                                     | 2007 | 9       | 42      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming F15 FX506LI_FX506LI              | 2020 | 9       | 41      | 1           |
| ASUSTek Computer     | TUF GAMING FX504GM_FX80GM                        | 2018 | 9       | 41      | 1           |
| Acer                 | Aspire 7250                                      | 2011 | 9       | 41      | 1           |
| Hewlett-Packard      | Laptop 14-bw0xx                                  | 2017 | 9       | 40      | 1           |
| ASUSTek Computer     | UX490UA                                          | 2017 | 9       | 40      | 1           |
| Hewlett-Packard      | ProBook 4440s                                    | 2012 | 9       | 40      | 1           |
| Hewlett-Packard      | Presario CQ58                                    | 2012 | 9       | 40      | 1           |
| Hewlett-Packard      | Laptop 14s-dq1xxx                                | 2019 | 9       | 39      | 1           |
| Hewlett-Packard      | Laptop 14-cf0xxx                                 | 2018 | 9       | 39      | 1           |
| ASUSTek Computer     | TUF Gaming FX505GD_FX505GD                       | 2018 | 9       | 39      | 1           |
| Lenovo               | Y520-15IKBM 80YY                                 | 2017 | 9       | 39      | 1           |
| ASUSTek Computer     | G750JM                                           | 2014 | 9       | 39      | 1           |
| Hewlett-Packard      | ENVY TS m6 Sleekbook                             | 2013 | 9       | 39      | 1           |
| Packard Bell         | EasyNote TM85                                    | 2010 | 9       | 39      | 1           |
| Semp Toshiba         | IS-1462                                          | 2008 | 9       | 39      | 1           |
| Hewlett-Packard      | Compaq 6830s                                     | 2008 | 9       | 39      | 1           |
| Hewlett-Packard      | 255 G4 Notebook PC                               | 2015 | 9       | 38      | 1           |
| ASUSTek Computer     | GL552JX                                          | 2015 | 9       | 38      | 1           |
| Acer                 | Aspire VN7-591G                                  | 2014 | 9       | 38      | 1           |
| Clevo                | W35_37ET                                         | 2012 | 9       | 38      | 1           |
| ASUSTek Computer     | N56VM                                            | 2012 | 9       | 38      | 1           |
| Fujitsu              | LIFEBOOK AH531/GFO                               | 2011 | 9       | 38      | 1           |
| eMachines            | eME732ZG                                         | 2010 | 9       | 38      | 1           |
| Lenovo               | IdeaPad 320-15IKB 81BT                           | 2018 | 9       | 37      | 1           |
| Dell                 | Vostro 14-3468                                   | 2017 | 9       | 37      | 1           |
| Hewlett-Packard      | Stream Notebook PC 14                            | 2014 | 9       | 37      | 1           |
| Acer                 | Aspire A515-55                                   | 2020 | 9       | 36      | 1           |
| Lenovo               | V310-15ISK 80SY                                  | 2016 | 9       | 36      | 1           |
| Hewlett-Packard      | Pavilion Gaming Notebook                         | 2015 | 9       | 36      | 1           |
| ASUSTek Computer     | K43SD                                            | 2011 | 9       | 36      | 1           |
| Fujitsu Siemens      | AMILO Li1705                                     | 2007 | 9       | 36      | 1           |
| Hewlett-Packard      | Pavilion dv5000                                  | 2005 | 9       | 36      | 1           |
| ASUSTek Computer     | VivoBook 17_ASUS Laptop X705MA_X705MA            | 2018 | 9       | 35      | 1           |
| Toshiba              | Satellite L50-C                                  | 2015 | 9       | 35      | 1           |
| Acer                 | Aspire 4315                                      | 2007 | 9       | 35      | 1           |
| Dell                 | Inspiron 3737                                    | 2013 | 9       | 34      | 1           |
| ASUSTek Computer     | X75A1                                            | 2012 | 9       | 34      | 1           |
| ASUSTek Computer     | 1005PXD                                          | 2011 | 9       | 34      | 1           |
| Lenovo               | B51-80 80LM                                      | 2015 | 9       | 33      | 1           |
| Acer                 | Aspire ES1-411                                   | 2014 | 9       | 33      | 1           |
| ASUSTek Computer     | 1015PEM                                          | 2010 | 9       | 33      | 1           |
| Hewlett-Packard      | Presario C700                                    | 2007 | 9       | 33      | 1           |
| Dell                 | Inspiron 6000                                    | 2005 | 9       | 33      | 1           |
| Acer                 | TravelMate B113                                  | 2012 | 9       | 32      | 1           |
| Hewlett-Packard      | Compaq nx6110                                    | 2005 | 9       | 32      | 1           |
| Dell                 | Vostro 2520                                      | 2012 | 9       | 31      | 1           |
| Lenovo               | IdeaPad 110S-11IBR 80WG                          | 2016 | 9       | 30      | 1           |
| Google               | Candy                                            | 2016 | 9       | 30      | 1           |
| ASUSTek Computer     | UX303UA                                          | 2015 | 9       | 29      | 1           |
| Acer                 | Aspire 5349                                      | 2011 | 9       | 29      | 1           |
| Packard Bell         | EasyNote MH35                                    | 2007 | 9       | 29      | 1           |
| Hewlett-Packard      | Stream Notebook                                  | 2015 | 9       | 28      | 1           |
| Hampoo               | Cherry Trail CR                                  | 2016 | 9       | 25      | 1           |
| Dell                 | XPS 17 9710                                      | 2021 | 8       | 63      | 1           |
| Lenovo               | IdeaPad Y560                                     | 2010 | 8       | 61      | 1           |
| Dell                 | Precision M6400                                  | 2009 | 8       | 57      | 1           |
| ASUSTek Computer     | G60JX                                            | 2009 | 8       | 57      | 1           |
| Razer                | Blade 15 Base Model (Early 2020) - RZ09-0328     | 2020 | 8       | 50      | 1           |
| Hewlett-Packard      | ZBook 15u G6                                     | 2019 | 8       | 50      | 1           |
| Chuwi                | GemiBook                                         | 2020 | 8       | 49      | 1           |
| Teclast              | F6 Plus                                          | 2019 | 8       | 48      | 1           |
| Lenovo               | V330-14ARR 81B1                                  | 2019 | 8       | 48      | 1           |
| System76             | Serval                                           | 2018 | 8       | 48      | 1           |
| ASUSTek Computer     | A7U                                              | 2007 | 8       | 48      | 1           |
| ASUSTek Computer     | Zephyrus G GU502DU_GA502DU                       | 2019 | 8       | 47      | 1           |
| Lenovo               | IdeaPad Y500 20193                               | 2012 | 8       | 47      | 1           |
| Apple                | MacBookAir3,2                                    | 2010 | 8       | 47      | 1           |
| ASUSTek Computer     | TUF Gaming FX505DD_FX505DD                       | 2019 | 8       | 46      | 1           |
| Dell                 | Precision M4400                                  | 2010 | 8       | 46      | 1           |
| Dell                 | Latitude XT2                                     | 2010 | 8       | 45      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512DA_A512DA                | 2019 | 8       | 44      | 1           |
| Dell                 | Inspiron 7737                                    | 2013 | 8       | 44      | 1           |
| Acer                 | Aspire E1-772G                                   | 2013 | 8       | 44      | 1           |
| Hewlett-Packard      | ZBook 15u G3                                     | 2016 | 8       | 43      | 1           |
| Notebook             | W65_67SZ                                         | 2013 | 8       | 43      | 1           |
| Lenovo               | ThinkPad SL                                      | 2008 | 8       | 43      | 1           |
| Dell                 | Latitude D531                                    | 2007 | 8       | 43      | 1           |
| Packard Bell         | EasyNote TE11BZ                                  | 2012 | 8       | 42      | 1           |
| ASUSTek Computer     | X501U                                            | 2012 | 8       | 42      | 1           |
| Toshiba              | Satellite P750                                   | 2011 | 8       | 42      | 1           |
| Hewlett-Packard      | ProBook 6465b                                    | 2011 | 8       | 42      | 1           |
| Toshiba              | Satellite A205                                   | 2008 | 8       | 42      | 1           |
| Acer                 | Aspire 9300                                      | 2006 | 8       | 42      | 1           |
| Hewlett-Packard      | Pavilion Laptop 14-dv0xxx                        | 2021 | 8       | 41      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X515JA_X515JA                | 2021 | 8       | 41      | 1           |
| Hewlett-Packard      | Pavilion dv2500                                  | 2007 | 8       | 41      | 1           |
| ASUSTek Computer     | UX331UN                                          | 2018 | 8       | 40      | 1           |
| Samsung Electronics  | 800G5M/800G5W                                    | 2017 | 8       | 40      | 1           |
| Hewlett-Packard      | Pavilion TS 11                                   | 2013 | 8       | 40      | 1           |
| ASUSTek Computer     | K46CM                                            | 2012 | 8       | 40      | 1           |
| Lenovo               | ThinkPad X121e                                   | 2011 | 8       | 40      | 1           |
| Semp Toshiba         | IS 1412                                          | 2009 | 8       | 40      | 1           |
| TUXEDO               | InfinityBook Pro 14 Gen6                         | 2021 | 8       | 39      | 1           |
| Lenovo               | IdeaPad 5 14ITL05 82FE                           | 2020 | 8       | 39      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X415JA_X415JA                | 2020 | 8       | 39      | 1           |
| Acer                 | Aspire E5-551                                    | 2014 | 8       | 39      | 1           |
| ASUSTek Computer     | TP300LA                                          | 2014 | 8       | 39      | 1           |
| MSI                  | CR61 2M/CX61 2OC/CX61 2OD                        | 2013 | 8       | 39      | 1           |
| ASUSTek Computer     | K40AB                                            | 2009 | 8       | 39      | 1           |
| Acer                 | Aspire A514-53                                   | 2020 | 8       | 38      | 1           |
| Hewlett-Packard      | Laptop 15-da2xxx                                 | 2019 | 8       | 38      | 1           |
| MSI                  | GS65 Stealth Thin 8RE                            | 2018 | 8       | 38      | 1           |
| MSI                  | GL62M 7RDX                                       | 2017 | 8       | 38      | 1           |
| Clevo                | W251EFQ/W270EFQ                                  | 2012 | 8       | 38      | 1           |
| Toshiba              | Satellite L670                                   | 2010 | 8       | 38      | 1           |
| Toshiba              | Satellite C650D                                  | 2010 | 8       | 38      | 1           |
| Fujitsu Siemens      | AMILO PRO V3515                                  | 2006 | 8       | 38      | 1           |
| Lenovo               | IdeaPad 320S-14IKB 80X4                          | 2017 | 8       | 37      | 1           |
| ASUSTek Computer     | K501UQ                                           | 2016 | 8       | 37      | 1           |
| ASUSTek Computer     | G73Sw                                            | 2011 | 8       | 37      | 1           |
| Hewlett-Packard      | Presario CQ62                                    | 2010 | 8       | 37      | 1           |
| Hewlett-Packard      | 2133                                             | 2008 | 8       | 37      | 1           |
| ASUSTek Computer     | VivoBook 14_ASUS Laptop X441MA_X441MA            | 2018 | 8       | 36      | 1           |
| Hewlett-Packard      | Presario CQ43                                    | 2011 | 8       | 36      | 1           |
| ASUSTek Computer     | 1015BXO                                          | 2011 | 8       | 36      | 1           |
| Acer                 | Aspire 5542                                      | 2009 | 8       | 36      | 1           |
| Samsung Electronics  | 670Z5E                                           | 2013 | 8       | 35      | 1           |
| MSI                  | CR70 2M/CX70 2OC/CX70 2OD                        | 2013 | 8       | 35      | 1           |
| ASUSTek Computer     | X450LCP                                          | 2013 | 8       | 35      | 1           |
| Hewlett-Packard      | Pavilion dv1000                                  | 2005 | 8       | 35      | 1           |
| Samsung Electronics  | 900X3C/900X3D/900X3E/900X4C/900X4D               | 2013 | 8       | 34      | 1           |
| Dell                 | Inspiron 3721                                    | 2013 | 8       | 34      | 1           |
| Samsung Electronics  | N250P                                            | 2010 | 8       | 34      | 1           |
| Hewlett-Packard      | Laptop 14-dq0xxx                                 | 2019 | 8       | 33      | 1           |
| Dell                 | Inspiron 7370                                    | 2018 | 8       | 33      | 1           |
| Purism               | Librem 15 v3                                     | 2017 | 8       | 33      | 1           |
| Lenovo               | IdeaPad S145-14IWL 81MU                          | 2019 | 8       | 32      | 1           |
| ASUSTek Computer     | X201EP                                           | 2012 | 8       | 32      | 1           |
| Samsung Electronics  | RC420/RC520/RC720                                | 2011 | 8       | 32      | 1           |
| ASUSTek Computer     | X101H                                            | 2011 | 8       | 32      | 1           |
| ASUSTek Computer     | 1005P                                            | 2009 | 8       | 32      | 1           |
| ASUSTek Computer     | X71SL                                            | 2008 | 8       | 31      | 1           |
| Hewlett-Packard      | Stream Laptop 14-cb0XX                           | 2018 | 8       | 30      | 1           |
| Acer                 | Aspire E3-111                                    | 2014 | 8       | 29      | 1           |
| Acer                 | Aspire E5-411                                    | 2014 | 8       | 28      | 1           |
| ASUSTek Computer     | E202SA                                           | 2015 | 8       | 25      | 1           |
| OEM                  | I41SI                                            | 2009 | 8       | 25      | 1           |
| Mediacom             | SmartBook 14 FullHD - SB14UC                     | 2016 | 8       | 24      | 1           |
| Hewlett-Packard      | EliteBook 745 G5                                 | 2019 | 7       | 56      | 1           |
| Notebook             | PB50_70RF,RD,RC                                  | 2019 | 7       | 54      | 1           |
| Alienware            | m15                                              | 2018 | 7       | 54      | 1           |
| Notebook             | P7xxTM1                                          | 2017 | 7       | 54      | 1           |
| Lenovo               | Legion R7000 2020 82B6                           | 2020 | 7       | 51      | 1           |
| Hewlett-Packard      | ZBook 14u G5                                     | 2018 | 7       | 49      | 1           |
| Fujitsu              | LIFEBOOK S710                                    | 2010 | 7       | 49      | 1           |
| Hewlett-Packard      | Compaq 8710w                                     | 2007 | 7       | 48      | 1           |
| Dell                 | Latitude 3420                                    | 2021 | 7       | 47      | 1           |
| ASUSTek Computer     | G750JW                                           | 2013 | 7       | 47      | 1           |
| Lenovo               | ThinkPad L412                                    | 2010 | 7       | 47      | 1           |
| Dell                 | Inspiron 15 5510                                 | 2021 | 7       | 46      | 1           |
| Hewlett-Packard      | Pavilion Laptop 15-cd0xx                         | 2017 | 7       | 46      | 1           |
| Lenovo               | ThinkPad S5-S540                                 | 2014 | 7       | 46      | 1           |
| ASUSTek Computer     | K73TK                                            | 2011 | 7       | 46      | 1           |
| ASUSTek Computer     | M50SA                                            | 2008 | 7       | 46      | 1           |
| MSI                  | Prestige 15 A11SCX                               | 2020 | 7       | 45      | 1           |
| Toshiba              | PORTEGE M780                                     | 2010 | 7       | 45      | 1           |
| Lenovo               | IdeaPad S145-15IIL 82DJ                          | 2020 | 7       | 44      | 1           |
| ASUSTek Computer     | ZenBook UX434DA_UM433DA                          | 2019 | 7       | 43      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FB_F509FB                | 2019 | 7       | 43      | 1           |
| MSI                  | GE70 2PE                                         | 2014 | 7       | 43      | 1           |
| ASUSTek Computer     | K75DE                                            | 2012 | 7       | 43      | 1           |
| Hewlett-Packard      | ENVY Laptop 13-ad0xx                             | 2017 | 7       | 42      | 1           |
| ASUSTek Computer     | N552VX                                           | 2016 | 7       | 42      | 1           |
| Lenovo               | IdeaPad 3 14ITL6 82H7                            | 2021 | 7       | 41      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X421IA_M433IA                | 2020 | 7       | 41      | 1           |
| Lenovo               | IdeaPad 500-15ACZ 80K4                           | 2015 | 7       | 41      | 1           |
| Fujitsu              | STYLISTIC Q702                                   | 2012 | 7       | 41      | 1           |
| ASUSTek Computer     | K95VJ                                            | 2012 | 7       | 41      | 1           |
| Lenovo               | IdeaPad S12 20021,2959                           | 2009 | 7       | 41      | 1           |
| Hewlett-Packard      | Compaq 6735b                                     | 2008 | 7       | 41      | 1           |
| MSI                  | VR610                                            | 2007 | 7       | 41      | 1           |
| Hewlett-Packard      | ProBook 640 G5                                   | 2019 | 7       | 40      | 1           |
| ASUSTek Computer     | X542UQ                                           | 2017 | 7       | 40      | 1           |
| Acer                 | Aspire ES1-524                                   | 2016 | 7       | 40      | 1           |
| ASUSTek Computer     | N752VX                                           | 2016 | 7       | 40      | 1           |
| MSI                  | GP60 2PE                                         | 2014 | 7       | 40      | 1           |
| Acer                 | Aspire VN7-791                                   | 2014 | 7       | 40      | 1           |
| Quanta               | TWC                                              | 2012 | 7       | 40      | 1           |
| Hewlett-Packard      | Pavilion Laptop 14-ce3xxx                        | 2019 | 7       | 39      | 1           |
| Chuwi                | AeroBook                                         | 2019 | 7       | 39      | 1           |
| Lenovo               | IdeaPad 330-17AST 81D7                           | 2018 | 7       | 39      | 1           |
| Hewlett-Packard      | Pavilion Laptop 15-cs1xxx                        | 2018 | 7       | 39      | 1           |
| Hewlett-Packard      | Pavilion Laptop 15-ck0xx                         | 2018 | 7       | 39      | 1           |
| Acer                 | Predator PH317-52                                | 2018 | 7       | 39      | 1           |
| Lenovo               | IdeaPad 720S-13IKB 81BV                          | 2017 | 7       | 39      | 1           |
| Hewlett-Packard      | 15 TS                                            | 2014 | 7       | 39      | 1           |
| ASUSTek Computer     | G551JM                                           | 2014 | 7       | 39      | 1           |
| Lenovo               | ThinkPad S3-S440                                 | 2013 | 7       | 39      | 1           |
| Hewlett-Packard      | ENVY TS 17                                       | 2013 | 7       | 39      | 1           |
| Toshiba              | Satellite P870                                   | 2012 | 7       | 39      | 1           |
| Gateway              | NV57H                                            | 2011 | 7       | 39      | 1           |
| Samsung Electronics  | R780/R778                                        | 2010 | 7       | 39      | 1           |
| Apple                | MacBookPro4,1                                    | 2008 | 7       | 39      | 1           |
| Hewlett-Packard      | Laptop 17-by1xxx                                 | 2018 | 7       | 38      | 1           |
| Hewlett-Packard      | 245 G6                                           | 2017 | 7       | 38      | 1           |
| Toshiba              | Satellite S55-A                                  | 2013 | 7       | 38      | 1           |
| Lenovo               | IdeaPad S206 20154                               | 2012 | 7       | 38      | 1           |
| Fujitsu              | LIFEBOOK AH532/G21                               | 2012 | 7       | 38      | 1           |
| Acer                 | Aspire V3-731                                    | 2012 | 7       | 38      | 1           |
| ASUSTek Computer     | K75VM                                            | 2012 | 7       | 38      | 1           |
| Acer                 | Aspire 3830TG                                    | 2011 | 7       | 38      | 1           |
| Google               | Rammus                                           | 2019 | 7       | 37      | 1           |
| ASUSTek Computer     | X555QA                                           | 2018 | 7       | 37      | 1           |
| ASUSTek Computer     | X510URR                                          | 2017 | 7       | 37      | 1           |
| ASUSTek Computer     | X411UA                                           | 2017 | 7       | 37      | 1           |
| Hewlett-Packard      | ProBook 440 G1                                   | 2014 | 7       | 37      | 1           |
| ASUSTek Computer     | N76VJ                                            | 2012 | 7       | 37      | 1           |
| Dell                 | Vostro V131                                      | 2011 | 7       | 37      | 1           |
| Lenovo               | G555 20045                                       | 2010 | 7       | 37      | 1           |
| Toshiba              | Satellite L505                                   | 2009 | 7       | 37      | 1           |
| Apple                | MacBookAir1,1                                    | 2008 | 7       | 37      | 1           |
| Acer                 | Aspire 9420                                      | 2006 | 7       | 37      | 1           |
| Acer                 | Aspire 9410                                      | 2006 | 7       | 37      | 1           |
| Hewlett-Packard      | 350 G1                                           | 2014 | 7       | 36      | 1           |
| Lenovo               | IdeaPad S510p 20298                              | 2013 | 7       | 36      | 1           |
| Toshiba              | Satellite M100                                   | 2006 | 7       | 36      | 1           |
| Hewlett-Packard      | Laptop 15-dy2xxx                                 | 2020 | 7       | 35      | 1           |
| Hewlett-Packard      | Pavilion Laptop 13-an0xxx                        | 2018 | 7       | 35      | 1           |
| Apple                | MacBook10,1                                      | 2018 | 7       | 35      | 1           |
| Acer                 | Aspire A315-53G                                  | 2018 | 7       | 35      | 1           |
| Samsung Electronics  | 270E5K/270E5Q/271E5K/2570EK                      | 2016 | 7       | 35      | 1           |
| Hewlett-Packard      | ProBook 430 G4                                   | 2016 | 7       | 35      | 1           |
| Pegatron             | A17                                              | 2011 | 7       | 35      | 1           |
| Lenovo               | Edge 15 80K9                                     | 2015 | 7       | 34      | 1           |
| Lenovo               | G400 20235                                       | 2013 | 7       | 34      | 1           |
| ASUSTek Computer     | Q550LF                                           | 2013 | 7       | 34      | 1           |
| Toshiba              | PORTEGE Z930                                     | 2012 | 7       | 34      | 1           |
| Hewlett-Packard      | Mini 110-3700                                    | 2011 | 7       | 34      | 1           |
| Hewlett-Packard      | G61                                              | 2009 | 7       | 34      | 1           |
| Lenovo               | IdeaPad S130-11IGM 81J1                          | 2018 | 7       | 33      | 1           |
| Hewlett-Packard      | Pavilion Laptop 14-ce0xxx                        | 2018 | 7       | 33      | 1           |
| ASUSTek Computer     | P553UA                                           | 2016 | 7       | 33      | 1           |
| ASUSTek Computer     | X75VD                                            | 2012 | 7       | 33      | 1           |
| Lenovo               | IdeaPad 300-15ISK 80RS                           | 2016 | 7       | 32      | 1           |
| ASUSTek Computer     | UX305UA                                          | 2015 | 7       | 32      | 1           |
| Lenovo               | IdeaPad Flex 14 20308                            | 2013 | 7       | 32      | 1           |
| Acer                 | Aspire S3-391                                    | 2012 | 7       | 32      | 1           |
| ASUSTek Computer     | X75A                                             | 2012 | 7       | 32      | 1           |
| Acer                 | Aspire ES1-532G                                  | 2016 | 7       | 31      | 1           |
| Lenovo               | IdeaPad S100 20109                               | 2011 | 7       | 31      | 1           |
| Dell                 | Latitude D600                                    | 2004 | 7       | 31      | 1           |
| MSI                  | PS42 Modern 8MO                                  | 2018 | 7       | 30      | 1           |
| Positivo             | CHT12CP                                          | 2017 | 7       | 26      | 1           |
| Positivo             | N1103                                            | 2016 | 7       | 24      | 1           |
| Alienware            | 17 R3                                            | 2016 | 6       | 58      | 1           |
| Alienware            | 13 R3                                            | 2016 | 6       | 57      | 1           |
| ASUSTek Computer     | ZenBook Pro Duo UX581GV_UX581GV                  | 2019 | 6       | 56      | 1           |
| Microtech            | ebookPro                                         | 2018 | 6       | 54      | 1           |
| TUXEDO               | Polaris 15 AMD Gen1                              | 2020 | 6       | 51      | 1           |
| ASUSTek Computer     | G752VY                                           | 2016 | 6       | 50      | 1           |
| ASUSTek Computer     | ROG Zephyrus G15 GA502IV_GA502IV                 | 2020 | 6       | 49      | 1           |
| Hewlett-Packard      | Pavilion Laptop 15z-cw100                        | 2019 | 6       | 49      | 1           |
| Hewlett-Packard      | ZBook 15u G5                                     | 2018 | 6       | 49      | 1           |
| Hewlett-Packard      | OMEN by HP Laptop 17-cb0xxx                      | 2019 | 6       | 48      | 1           |
| Hewlett-Packard      | ProBook 640 G4                                   | 2019 | 6       | 47      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512DK_X512DK                | 2019 | 6       | 47      | 1           |
| Hewlett-Packard      | EliteBook 1050 G1                                | 2018 | 6       | 47      | 1           |
| ASUSTek Computer     | N552VW                                           | 2016 | 6       | 47      | 1           |
| ASUSTek Computer     | F3Sr                                             | 2007 | 6       | 47      | 1           |
| SLIMBOOK             | PROX15-AMD                                       | 2020 | 6       | 46      | 1           |
| Nuvision             | NES11                                            | 2018 | 6       | 46      | 1           |
| Dell                 | Vostro 3300                                      | 2010 | 6       | 46      | 1           |
| Dell                 | Inspiron 5505                                    | 2020 | 6       | 45      | 1           |
| ASUSTek Computer     | UX410UAR                                         | 2018 | 6       | 45      | 1           |
| Jumper               | EZpad                                            | 2015 | 6       | 45      | 1           |
| Hewlett-Packard      | ENVY m7 Notebook                                 | 2015 | 6       | 45      | 1           |
| ASUSTek Computer     | K42Jv                                            | 2010 | 6       | 45      | 1           |
| Acer                 | Aspire 5935                                      | 2009 | 6       | 45      | 1           |
| ASUSTek Computer     | F3Sg                                             | 2008 | 6       | 44      | 1           |
| Hewlett-Packard      | ENVY Laptop 15-ep0xxx                            | 2020 | 6       | 43      | 1           |
| Acer                 | Aspire A517-52                                   | 2020 | 6       | 43      | 1           |
| Dell                 | Inspiron 5391                                    | 2019 | 6       | 43      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X430FN_S430FN                | 2019 | 6       | 43      | 1           |
| Acer                 | Aspire A717-72G                                  | 2018 | 6       | 43      | 1           |
| ASUSTek Computer     | X401U                                            | 2012 | 6       | 43      | 1           |
| ASUSTek Computer     | K42Jc                                            | 2010 | 6       | 43      | 1           |
| Dell                 | Inspiron 1764                                    | 2009 | 6       | 43      | 1           |
| Samsung Electronics  | 550XDA                                           | 2021 | 6       | 42      | 1           |
| Dell                 | Inspiron 16 7610                                 | 2021 | 6       | 42      | 1           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E210MA_L210MA               | 2020 | 6       | 42      | 1           |
| Acer                 | Aspire VN7-791G                                  | 2015 | 6       | 42      | 1           |
| Lenovo               | B575 Brazos                                      | 2011 | 6       | 42      | 1           |
| ASUSTek Computer     | A8SR                                             | 2007 | 6       | 42      | 1           |
| Hewlett-Packard      | EliteBook 840 G8 Notebook PC                     | 2021 | 6       | 41      | 1           |
| Razer                | Blade Stealth 13 Late 2019                       | 2019 | 6       | 41      | 1           |
| Acer                 | Swift SF313-52                                   | 2019 | 6       | 41      | 1           |
| Lenovo               | G50-45 80MQ                                      | 2014 | 6       | 41      | 1           |
| ASUSTek Computer     | X455LD                                           | 2014 | 6       | 41      | 1           |
| Fujitsu              | LIFEBOOK S761                                    | 2011 | 6       | 41      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X705FD_N705FD                | 2018 | 6       | 40      | 1           |
| Alienware            | M11x                                             | 2010 | 6       | 40      | 1           |
| ASUSTek Computer     | A8Le                                             | 2007 | 6       | 40      | 1           |
| ASUSTek Computer     | F3JC                                             | 2006 | 6       | 40      | 1           |
| Lenovo               | IdeaPad Slim 1-11AST-05 81VR                     | 2019 | 6       | 39      | 1           |
| Dell                 | Inspiron 13-7378                                 | 2018 | 6       | 39      | 1           |
| Acer                 | Aspire E5-475G                                   | 2016 | 6       | 39      | 1           |
| Lenovo               | IdeaPad Y700 Touch-15ISK 80NW                    | 2015 | 6       | 39      | 1           |
| Alienware            | M14xR2                                           | 2013 | 6       | 39      | 1           |
| Packard Bell         | EasyNote LV11HC                                  | 2012 | 6       | 39      | 1           |
| Clevo                | W251ESQ/W270ESQ                                  | 2012 | 6       | 39      | 1           |
| Lenovo               | IdeaPad S205 Brazos                              | 2011 | 6       | 39      | 1           |
| Acer                 | Aspire 4741                                      | 2010 | 6       | 39      | 1           |
| Lenovo               | ThinkPad X60                                     | 2006 | 6       | 39      | 1           |
| Lenovo               | ThinkPad R60                                     | 2006 | 6       | 39      | 1           |
| Hewlett-Packard      | Laptop 17-cn0xxx                                 | 2021 | 6       | 38      | 1           |
| MSI                  | GF63 Thin 10SCSR                                 | 2020 | 6       | 38      | 1           |
| Lenovo               | IdeaPad 3 15IML05 81WB                           | 2020 | 6       | 38      | 1           |
| Lenovo               | IdeaPad S145-14AST 81ST                          | 2019 | 6       | 38      | 1           |
| ASUSTek Computer     | X705UAR                                          | 2019 | 6       | 38      | 1           |
| MSI                  | GF63 8RD                                         | 2018 | 6       | 38      | 1           |
| ASUSTek Computer     | N551JX                                           | 2015 | 6       | 38      | 1           |
| Fujitsu              | LIFEBOOK E744                                    | 2014 | 6       | 38      | 1           |
| MSI                  | GP60 2OD                                         | 2013 | 6       | 38      | 1           |
| Toshiba              | Satellite P850                                   | 2012 | 6       | 38      | 1           |
| Lenovo               | G575 4383                                        | 2011 | 6       | 38      | 1           |
| ASUSTek Computer     | U31SD                                            | 2011 | 6       | 38      | 1           |
| eMachines            | G640                                             | 2010 | 6       | 38      | 1           |
| CCE                  | Capella & IbexPeak-M Chipset                     | 2010 | 6       | 38      | 1           |
| Acer                 | Aspire 5734Z                                     | 2010 | 6       | 38      | 1           |
| Intel                | Calistoga & ICH7M Chipset                        | 2006 | 6       | 38      | 1           |
| HUAWEI               | BOHB-WAX9                                        | 2021 | 6       | 37      | 1           |
| Hewlett-Packard      | ProBook 440 G8 Notebook PC                       | 2020 | 6       | 37      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X513EA_K513EA                | 2020 | 6       | 37      | 1           |
| DEXP                 | Notebook                                         | 2015 | 6       | 37      | 1           |
| ASUSTek Computer     | X751LN                                           | 2014 | 6       | 37      | 1           |
| Toshiba              | PORTEGE R30-A                                    | 2013 | 6       | 37      | 1           |
| MSI                  | GT70                                             | 2012 | 6       | 37      | 1           |
| Samsung Electronics  | R25P                                             | 2007 | 6       | 37      | 1           |
| Apple                | MacBookPro2,2                                    | 2007 | 6       | 37      | 1           |
| Timi                 | TM1709                                           | 2018 | 6       | 36      | 1           |
| Notebook             | N13_N140ZU                                       | 2018 | 6       | 36      | 1           |
| Dell                 | Inspiron 7566                                    | 2016 | 6       | 36      | 1           |
| Fujitsu              | LIFEBOOK AH544                                   | 2014 | 6       | 36      | 1           |
| Notebook             | W250EGQ / W270EGQ                                | 2012 | 6       | 36      | 1           |
| Acer                 | Aspire V5-571PG                                  | 2012 | 6       | 36      | 1           |
| ASUSTek Computer     | U56E                                             | 2011 | 6       | 36      | 1           |
| Packard Bell         | EasyNote LJ65                                    | 2009 | 6       | 36      | 1           |
| Samsung Electronics  | R710                                             | 2008 | 6       | 36      | 1           |
| Packard Bell         | EasyNote MH36                                    | 2008 | 6       | 36      | 1           |
| Dell                 | Latitude 3450                                    | 2014 | 6       | 35      | 1           |
| ASUSTek Computer     | X550JK                                           | 2014 | 6       | 35      | 1           |
| ASUSTek Computer     | UX303LAB                                         | 2014 | 6       | 35      | 1           |
| Lenovo               | G780                                             | 2012 | 6       | 35      | 1           |
| Alienware            | M14xR1                                           | 2011 | 6       | 35      | 1           |
| Samsung Electronics  | R20/P400                                         | 2007 | 6       | 35      | 1           |
| IBM                  | ThinkPad T42                                     | 2004 | 6       | 35      | 1           |
| ASUSTek Computer     | UX303LN                                          | 2014 | 6       | 34      | 1           |
| ASUSTek Computer     | X450CA                                           | 2013 | 6       | 34      | 1           |
| Hewlett-Packard      | Folio 13                                         | 2012 | 6       | 34      | 1           |
| AMI                  | T3 MRD                                           | 2018 | 6       | 33      | 1           |
| Dell                 | Latitude 3550                                    | 2014 | 6       | 33      | 1           |
| Acer                 | Aspire ES1-111                                   | 2014 | 6       | 33      | 1           |
| ASUSTek Computer     | X751NV                                           | 2017 | 6       | 32      | 1           |
| Toshiba              | Satellite S50-B                                  | 2014 | 6       | 32      | 1           |
| MSI                  | CR500                                            | 2009 | 6       | 32      | 1           |
| Samsung Electronics  | 530XBB                                           | 2019 | 6       | 31      | 1           |
| Hewlett-Packard      | 450                                              | 2012 | 6       | 31      | 1           |
| Acer                 | Aspire 3610                                      | 2005 | 6       | 31      | 1           |
| Dell                 | Inspiron 3531                                    | 2014 | 6       | 30      | 1           |
| VIT                  | P2400                                            | 2012 | 6       | 30      | 1           |
| Samsung Electronics  | 350U2A/350U2B/300U1A/351U2A/351U2B/301U1A        | 2012 | 6       | 30      | 1           |
| Lenovo               | S20-30 20421                                     | 2014 | 6       | 29      | 1           |
| BESSTAR Tech         | Z83-F                                            | 2018 | 6       | 23      | 1           |
| Insyde               | BayTrail                                         | 2014 | 6       | 23      | 1           |
| Acer                 | Switch One SW1-011                               | 2016 | 6       | 22      | 1           |
| ilife                | S806                                             | 2014 | 6       | 22      | 1           |
| TUXEDO               | P7xxTM1                                          | 2017 | 5       | 59      | 1           |
| ASUSTek Computer     | Zephyrus M GM501GS                               | 2018 | 5       | 57      | 1           |
| MSI                  | GS40 6QE Phantom                                 | 2016 | 5       | 52      | 1           |
| ASUSTek Computer     | ROG Zephyrus G15 GA503QM_GA503QM                 | 2021 | 5       | 51      | 1           |
| MSI                  | GE66 Raider 10SFS                                | 2020 | 5       | 49      | 1           |
| Dell                 | Inspiron 7590                                    | 2019 | 5       | 49      | 1           |
| Sony                 | VPCEB1S1E                                        | 2010 | 5       | 49      | 1           |
| Dell                 | G5 5500                                          | 2020 | 5       | 48      | 1           |
| AZW                  | GT-R                                             | 2020 | 5       | 48      | 1           |
| Notebook             | N150CU                                           | 2019 | 5       | 48      | 1           |
| ASUSTek Computer     | ASUS TUF Dash F15 FX516PR_FX516PR                | 2021 | 5       | 47      | 1           |
| ASUSTek Computer     | TUF Gaming FX705DY_FX705DY                       | 2020 | 5       | 47      | 1           |
| Dell                 | Latitude 7380                                    | 2017 | 5       | 47      | 1           |
| Toshiba              | PORTEGE R700                                     | 2010 | 5       | 47      | 1           |
| Lenovo               | IdeaPad 3 15ITL6 82H8                            | 2021 | 5       | 46      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X712DAP_M712DA               | 2020 | 5       | 46      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509DA_D509DA                | 2019 | 5       | 46      | 1           |
| ASUSTek Computer     | M50Vm                                            | 2008 | 5       | 46      | 1           |
| Hewlett-Packard      | Laptop 14s-dk0xxx                                | 2019 | 5       | 45      | 1           |
| Hewlett-Packard      | EliteBook 1040 G4                                | 2018 | 5       | 45      | 1           |
| Teclast              | F7 Plus                                          | 2009 | 5       | 45      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412FL_X412FL                | 2019 | 5       | 44      | 1           |
| ASUSTek Computer     | ROG Strix G731GW_G731GW                          | 2019 | 5       | 44      | 1           |
| ASUSTek Computer     | ROG Strix G531GV_G531GV                          | 2019 | 5       | 44      | 1           |
| Dell                 | Inspiron 7773                                    | 2017 | 5       | 44      | 1           |
| Acer                 | Aspire VN7-793G                                  | 2017 | 5       | 44      | 1           |
| Acer                 | Aspire E5-523                                    | 2016 | 5       | 44      | 1           |
| Dell                 | Inspiron 5749                                    | 2015 | 5       | 44      | 1           |
| Hewlett-Packard      | TouchSmart tx2                                   | 2009 | 5       | 44      | 1           |
| ASUSTek Computer     | F3Se                                             | 2007 | 5       | 44      | 1           |
| Hewlett-Packard      | Pavilion Laptop 14-ce2xxx                        | 2019 | 5       | 43      | 1           |
| Hewlett-Packard      | 245 G7 Notebook PC                               | 2019 | 5       | 43      | 1           |
| Sony                 | VGN-FZ140E                                       | 2007 | 5       | 43      | 1           |
| ASUSTek Computer     | F3Sc                                             | 2007 | 5       | 43      | 1           |
| Lenovo               | IdeaPad 730S-13IWL 81JB                          | 2018 | 5       | 42      | 1           |
| Notebook             | W65_67SR                                         | 2013 | 5       | 42      | 1           |
| Lenovo               | XiaoXinAir-14ARE 2020 81YN                       | 2020 | 5       | 41      | 1           |
| Hewlett-Packard      | Laptop 15s-fq0xxx                                | 2019 | 5       | 41      | 1           |
| Dell                 | Vostro 3583                                      | 2019 | 5       | 41      | 1           |
| Dell                 | Inspiron 5585                                    | 2019 | 5       | 41      | 1           |
| MSI                  | GE63 Raider RGB 8RE                              | 2018 | 5       | 41      | 1           |
| Hewlett-Packard      | EliteBook 725 G2                                 | 2014 | 5       | 41      | 1           |
| ASUSTek Computer     | G750JH                                           | 2013 | 5       | 41      | 1           |
| Hewlett-Packard      | ENVY 14                                          | 2011 | 5       | 41      | 1           |
| eMachines            | E720                                             | 2008 | 5       | 41      | 1           |
| Fujitsu Siemens      | AMILO Li3910                                     | 2008 | 5       | 41      | 1           |
| Lenovo               | Legion Y7000 2019 PG0 81T0                       | 2019 | 5       | 40      | 1           |
| Acer                 | Swift SF514-53T                                  | 2019 | 5       | 40      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X510QA_X510QA                | 2019 | 5       | 40      | 1           |
| Lenovo               | IdeaPad 700-17ISK 80RV                           | 2017 | 5       | 40      | 1           |
| Acer                 | Predator G9-791                                  | 2015 | 5       | 40      | 1           |
| MSI                  | GE70 2PL                                         | 2014 | 5       | 40      | 1           |
| ASUSTek Computer     | X550ZA                                           | 2014 | 5       | 40      | 1           |
| Toshiba              | Satellite L855D                                  | 2012 | 5       | 40      | 1           |
| Lenovo               | ThinkPad X130e                                   | 2011 | 5       | 40      | 1           |
| Packard Bell         | EasyNote TX86                                    | 2010 | 5       | 40      | 1           |
| Lenovo               | IdeaPad 330S-14AST 81F8                          | 2019 | 5       | 39      | 1           |
| Acer                 | Swift SF514-54T                                  | 2019 | 5       | 39      | 1           |
| Acer                 | Aspire A317-51G                                  | 2019 | 5       | 39      | 1           |
| Dell                 | Latitude 5285                                    | 2018 | 5       | 39      | 1           |
| Acer                 | Aspire V3-574G                                   | 2015 | 5       | 39      | 1           |
| ASUSTek Computer     | G550JK                                           | 2014 | 5       | 39      | 1           |
| Dell                 | Inspiron 5523                                    | 2012 | 5       | 39      | 1           |
| ASUSTek Computer     | S550CA                                           | 2012 | 5       | 39      | 1           |
| Toshiba              | Satellite U500                                   | 2010 | 5       | 39      | 1           |
| Hewlett-Packard      | ENVY Laptop 13-ba0xxx                            | 2020 | 5       | 38      | 1           |
| Toshiba              | Satellite P50-C                                  | 2015 | 5       | 38      | 1           |
| Hewlett-Packard      | Pavilion 10 TS                                   | 2013 | 5       | 38      | 1           |
| Hewlett-Packard      | ProBook 4310s                                    | 2009 | 5       | 38      | 1           |
| Acer                 | Swift SF515-51T                                  | 2018 | 5       | 37      | 1           |
| TUXEDO               | N13xWU                                           | 2017 | 5       | 37      | 1           |
| ASUSTek Computer     | X555UJ                                           | 2015 | 5       | 37      | 1           |
| ASUSTek Computer     | N501JW                                           | 2015 | 5       | 37      | 1           |
| Dell                 | Latitude 14 Rugged                               | 2014 | 5       | 37      | 1           |
| Dell                 | Latitude E5430 vPro                              | 2013 | 5       | 37      | 1           |
| ASUSTek Computer     | S551LB                                           | 2013 | 5       | 37      | 1           |
| ASUSTek Computer     | K95VB                                            | 2013 | 5       | 37      | 1           |
| Acer                 | Aspire M3-581TG                                  | 2012 | 5       | 37      | 1           |
| Acer                 | P5WE0                                            | 2011 | 5       | 37      | 1           |
| Dell                 | Inspiron 1012                                    | 2009 | 5       | 37      | 1           |
| Acer                 | Extensa 4220                                     | 2008 | 5       | 37      | 1           |
| ASUSTek Computer     | VivoBook E14 E402YA_E402YA                       | 2019 | 5       | 36      | 1           |
| Lenovo               | ThinkPad S3 Yoga 14                              | 2018 | 5       | 36      | 1           |
| Lenovo               | IdeaPad 320-15IKB 81G3                           | 2018 | 5       | 36      | 1           |
| MSI                  | GE62 6QD                                         | 2017 | 5       | 36      | 1           |
| Lenovo               | ThinkPad T570 W10DG                              | 2017 | 5       | 36      | 1           |
| Lenovo               | ThinkPad 13                                      | 2016 | 5       | 36      | 1           |
| Hewlett-Packard      | ZBook 17 G3                                      | 2016 | 5       | 36      | 1           |
| Dell                 | Latitude E5270                                   | 2016 | 5       | 36      | 1           |
| ASUSTek Computer     | X455LJ                                           | 2015 | 5       | 36      | 1           |
| Toshiba              | TECRA R950                                       | 2012 | 5       | 36      | 1           |
| Fujitsu              | LIFEBOOK U772                                    | 2012 | 5       | 36      | 1           |
| ASUSTek Computer     | N46VM                                            | 2012 | 5       | 36      | 1           |
| ASUSTek Computer     | U36SG                                            | 2011 | 5       | 36      | 1           |
| Aquarius             | Pro, Std, Elt Series                             | 2010 | 5       | 36      | 1           |
| Fujitsu Siemens      | ESPRIMO Mobile M9410                             | 2009 | 5       | 36      | 1           |
| Fujitsu Siemens      | AMILO Pi 1505                                    | 2007 | 5       | 36      | 1           |
| ASUSTek Computer     | F3JR                                             | 2007 | 5       | 36      | 1           |
| Acer                 | Aspire 5600                                      | 2006 | 5       | 36      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512UA                       | 2019 | 5       | 35      | 1           |
| Dell                 | Vostro 5370                                      | 2018 | 5       | 35      | 1           |
| MSI                  | GP62 7RD                                         | 2016 | 5       | 35      | 1           |
| ASUSTek Computer     | UX430UA                                          | 2016 | 5       | 35      | 1           |
| Toshiba              | Satellite S55-C                                  | 2015 | 5       | 35      | 1           |
| Toshiba              | dynabook Satellite B552/G                        | 2012 | 5       | 35      | 1           |
| Toshiba              | NB100                                            | 2008 | 5       | 35      | 1           |
| Lenovo               | ThinkPad X1C 5th W10DG                           | 2018 | 5       | 34      | 1           |
| Hewlett-Packard      | 240 G7 Notebook PC                               | 2018 | 5       | 34      | 1           |
| Acer                 | TravelMate P259-M                                | 2017 | 5       | 34      | 1           |
| ASUSTek Computer     | X540UV                                           | 2017 | 5       | 34      | 1           |
| Lenovo               | Yoga 500-14ISK 80R5                              | 2015 | 5       | 34      | 1           |
| Toshiba              | Satellite S55-B                                  | 2014 | 5       | 34      | 1           |
| ASUSTek Computer     | X450CC                                           | 2013 | 5       | 34      | 1           |
| ASUSTek Computer     | X402CA                                           | 2013 | 5       | 34      | 1           |
| Compal               | QAL51                                            | 2012 | 5       | 34      | 1           |
| Sony                 | VPCEH2J1R                                        | 2011 | 5       | 34      | 1           |
| Dell                 | XPS L412Z                                        | 2011 | 5       | 34      | 1           |
| TUXEDO               | N7x0WU                                           | 2017 | 5       | 33      | 1           |
| MSI                  | GL62M 7REX                                       | 2017 | 5       | 33      | 1           |
| Lenovo               | IdeaPad 520S-14IKB 80X2                          | 2017 | 5       | 33      | 1           |
| Hewlett-Packard      | Split 13 x2 PC                                   | 2013 | 5       | 33      | 1           |
| Lenovo               | ThinkPad Edge E130                               | 2012 | 5       | 33      | 1           |
| Lenovo               | G770 1037                                        | 2011 | 5       | 33      | 1           |
| Toshiba              | PORTEGE R500                                     | 2008 | 5       | 33      | 1           |
| Acer                 | Swift SF314-58                                   | 2019 | 5       | 32      | 1           |
| ASUSTek Computer     | ZenBook UX431FA                                  | 2019 | 5       | 32      | 1           |
| Fujitsu              | LIFEBOOK A357                                    | 2018 | 5       | 32      | 1           |
| ASUSTek Computer     | VivoBook S15 X530UN                              | 2018 | 5       | 32      | 1           |
| MSI                  | CX62 6QD                                         | 2016 | 5       | 32      | 1           |
| Dell                 | Inspiron 13-7353                                 | 2016 | 5       | 32      | 1           |
| Lenovo               | M30-70 20446                                     | 2014 | 5       | 32      | 1           |
| Toshiba              | Satellite L20                                    | 2005 | 5       | 32      | 1           |
| Acer                 | TravelMate 2410                                  | 2005 | 5       | 32      | 1           |
| Hewlett-Packard      | Pavilion zd8000                                  | 2004 | 5       | 32      | 1           |
| Sony                 | SVF15213CBW                                      | 2013 | 5       | 31      | 1           |
| ASUSTek Computer     | X541SC                                           | 2016 | 5       | 30      | 1           |
| Hewlett-Packard      | Stream Laptop 11-ah0XX                           | 2018 | 5       | 29      | 1           |
| Hewlett-Packard      | Pro Tablet 608 G1                                | 2016 | 5       | 29      | 1           |
| Lenovo               | S21e-20 80M4                                     | 2015 | 5       | 29      | 1           |
| Lenovo               | IdeaPad 330-14IGM 81D0                           | 2018 | 5       | 28      | 1           |
| Positivo             | V142N_4G                                         | 2018 | 5       | 26      | 1           |
| Google               | Relm                                             | 2017 | 5       | 26      | 1           |
| Multilaser           | PC024                                            | 2017 | 5       | 23      | 1           |
| Exo                  | CloudbookE15                                     | 2017 | 5       | 23      | 1           |
| Apple                | MacBookPro14,3                                   | 2018 | 4       | 89      | 1           |
| TUXEDO               | InfinityBook S 14 Gen6                           | 2020 | 4       | 55      | 1           |
| MSI                  | GS75 Stealth 9SF                                 | 2019 | 4       | 55      | 1           |
| Chuwi                | Hi13                                             | 2017 | 4       | 55      | 1           |
| TUXEDO               | InfinityBook S 15 Gen6                           | 2020 | 4       | 54      | 1           |
| Notebook             | PCX0DX                                           | 2020 | 4       | 54      | 1           |
| Lenovo               | Legion 5 17IMH05H 81Y8                           | 2020 | 4       | 53      | 1           |
| Dell                 | Precision 3561                                   | 2021 | 4       | 52      | 1           |
| UNOWHY               | Y13G010S4EI                                      | 2020 | 4       | 52      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X421IAY_M413IA               | 2020 | 4       | 52      | 1           |
| Hewlett-Packard      | ZBook Firefly 15 inch G8 Mobile Workstation PC   | 2020 | 4       | 51      | 1           |
| Teclast              | F7S                                              | 2020 | 4       | 50      | 1           |
| Dell                 | Latitude 7290                                    | 2018 | 4       | 50      | 1           |
| Acer                 | Swift SF314-41G                                  | 2019 | 4       | 49      | 1           |
| Dell                 | Latitude 7390 2-in-1                             | 2018 | 4       | 49      | 1           |
| MSI                  | GF65 Thin 10SDR                                  | 2020 | 4       | 48      | 1           |
| MSI                  | Alpha 15 A3DDK                                   | 2019 | 4       | 48      | 1           |
| ASUSTek Computer     | TUF Gaming FA506IV_FA506IV                       | 2020 | 4       | 47      | 1           |
| Lenovo               | Legion Y740-17IRHg 81UJ                          | 2019 | 4       | 47      | 1           |
| Avell High Perfor... | 1513                                             | 2018 | 4       | 47      | 1           |
| Hewlett-Packard      | HDX 18                                           | 2008 | 4       | 47      | 1           |
| Lenovo               | Yoga Slim 7 14ITL05 82A3                         | 2020 | 4       | 46      | 1           |
| Hewlett-Packard      | OMEN by HP Laptop 17-cb1xxx                      | 2020 | 4       | 46      | 1           |
| ASUSTek Computer     | ROG Strix G512LV_G512LV                          | 2020 | 4       | 46      | 1           |
| VINGA                | Iron S140                                        | 2018 | 4       | 46      | 1           |
| Lenovo               | Y50-70 Touch 20349                               | 2014 | 4       | 46      | 1           |
| Toshiba              | TECRA M10                                        | 2008 | 4       | 46      | 1           |
| ASUSTek Computer     | ROG Strix G712LV_G712LV                          | 2021 | 4       | 45      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X515DA_M515DA                | 2020 | 4       | 45      | 1           |
| ASUSTek Computer     | Strix GL504GM_GL504GM                            | 2018 | 4       | 45      | 1           |
| Hewlett-Packard      | InsydeH2O EFI BIOS                               | 2013 | 4       | 45      | 1           |
| Clevo                | P150HMx                                          | 2011 | 4       | 45      | 1           |
| ASUSTek Computer     | K42JY                                            | 2011 | 4       | 45      | 1           |
| Dell                 | Studio XPS 1340                                  | 2010 | 4       | 45      | 1           |
| ASUSTek Computer     | N71Jv                                            | 2010 | 4       | 45      | 1           |
| ASUSTek Computer     | ZenBook UX425QA_UM425QA                          | 2021 | 4       | 44      | 1           |
| Dell                 | Vostro 14 5401                                   | 2020 | 4       | 44      | 1           |
| Notebook             | P9XXEN_EF_ED                                     | 2018 | 4       | 44      | 1           |
| Timi                 | A35                                              | 2021 | 4       | 43      | 1           |
| PC Specialist        | Standard                                         | 2021 | 4       | 43      | 1           |
| Notebook             | NJ5x_NJ7xLU                                      | 2020 | 4       | 43      | 1           |
| Lenovo               | ThinkBook 15 G2 ARE 20VG                         | 2020 | 4       | 43      | 1           |
| Hewlett-Packard      | 250 G8 Notebook PC                               | 2020 | 4       | 43      | 1           |
| Dell                 | Latitude 3500                                    | 2019 | 4       | 43      | 1           |
| ASUSTek Computer     | M51Va                                            | 2008 | 4       | 43      | 1           |
| MSI                  | Prestige 14Evo A11M                              | 2020 | 4       | 42      | 1           |
| Hewlett-Packard      | ZBook Firefly 14 G7 Mobile Workstation           | 2020 | 4       | 42      | 1           |
| Hewlett-Packard      | ENVY Laptop 17-cg1xxx                            | 2020 | 4       | 42      | 1           |
| Apple                | MacBook8,1                                       | 2020 | 4       | 42      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X421DA_M413DA                | 2020 | 4       | 42      | 1           |
| Lenovo               | IdeaPad S340-15APITouch 81QG                     | 2019 | 4       | 42      | 1           |
| Notebook             | N8xEJEK                                          | 2018 | 4       | 42      | 1           |
| ASUSTek Computer     | ZenBook Pro 15 UX550GDX_UX580GD                  | 2018 | 4       | 42      | 1           |
| ASUSTek Computer     | GL502VS                                          | 2016 | 4       | 42      | 1           |
| Acer                 | Aspire E5-521G                                   | 2014 | 4       | 42      | 1           |
| Positivo             | A14CR6A                                          | 2013 | 4       | 42      | 1           |
| Sony                 | VPCF236FM                                        | 2011 | 4       | 42      | 1           |
| Hewlett-Packard      | ProBook 6440b                                    | 2010 | 4       | 42      | 1           |
| System76             | Pangolin Performance                             | 2009 | 4       | 42      | 1           |
| Medion               | WIM2180                                          | 2008 | 4       | 42      | 1           |
| Hewlett-Packard      | Pavilion tx2500                                  | 2008 | 4       | 42      | 1           |
| Acer                 | Aspire 9810                                      | 2006 | 4       | 42      | 1           |
| Acer                 | Aspire A514-54                                   | 2021 | 4       | 41      | 1           |
| Hewlett-Packard      | Laptop 15-ef1xxx                                 | 2020 | 4       | 41      | 1           |
| Google               | Treeya                                           | 2020 | 4       | 41      | 1           |
| ASUSTek Computer     | ZenBook UX425IA_UM425IA                          | 2020 | 4       | 41      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X409FA_X409FA                | 2019 | 4       | 41      | 1           |
| Timi                 | TM1801                                           | 2018 | 4       | 41      | 1           |
| MSI                  | GP60 2QF                                         | 2014 | 4       | 41      | 1           |
| Lenovo               | ThinkPad T430u                                   | 2013 | 4       | 41      | 1           |
| Lenovo               | IdeaPad Y580 20132                               | 2012 | 4       | 41      | 1           |
| ASUSTek Computer     | G53SX                                            | 2011 | 4       | 41      | 1           |
| Dell                 | Vostro 1310                                      | 2009 | 4       | 41      | 1           |
| Fujitsu Siemens      | LIFEBOOK E8310                                   | 2008 | 4       | 41      | 1           |
| Acer                 | Aspire 7530G                                     | 2008 | 4       | 41      | 1           |
| ASUSTek Computer     | F3E                                              | 2008 | 4       | 41      | 1           |
| Lenovo               | V14-ADA 82C6                                     | 2020 | 4       | 40      | 1           |
| HUAWEI               | KLVC-WXX9                                        | 2020 | 4       | 40      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X513EP_X513EP                | 2020 | 4       | 40      | 1           |
| MSI                  | Prestige 14 A10RB                                | 2019 | 4       | 40      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412FA_X412FA                | 2019 | 4       | 40      | 1           |
| Google               | Cave                                             | 2018 | 4       | 40      | 1           |
| Fujitsu              | LIFEBOOK U747                                    | 2017 | 4       | 40      | 1           |
| Hewlett-Packard      | ProBook 455 G4                                   | 2016 | 4       | 40      | 1           |
| Acer                 | Aspire F5-771G                                   | 2016 | 4       | 40      | 1           |
| Toshiba              | Satellite S70-B                                  | 2014 | 4       | 40      | 1           |
| Samsung Electronics  | 400B4B/400B5B/200B4B/200B5B                      | 2011 | 4       | 40      | 1           |
| Lenovo               | IdeaPad Y470 20090                               | 2011 | 4       | 40      | 1           |
| eMachines            | eME640                                           | 2010 | 4       | 40      | 1           |
| Lenovo               | ThinkPad SL400                                   | 2009 | 4       | 40      | 1           |
| Fujitsu Siemens      | AMILO Li3710                                     | 2008 | 4       | 40      | 1           |
| Samsung Electronics  | SQ45S70S                                         | 2007 | 4       | 40      | 1           |
| IBM                  | ThinkPad Z60m                                    | 2006 | 4       | 40      | 1           |
| Hewlett-Packard      | Laptop 14-fq1xxx                                 | 2021 | 4       | 39      | 1           |
| TUXEDO               | BC1510 1710                                      | 2020 | 4       | 39      | 1           |
| Acer                 | Swift SF313-53                                   | 2020 | 4       | 39      | 1           |
| Notebook             | NH50_70RA                                        | 2019 | 4       | 39      | 1           |
| Avell High Perfor... | Avell G1555 MUV / A62 MUV                        | 2019 | 4       | 39      | 1           |
| ASUSTek Computer     | TUF Gaming FX705DY_TUF705                        | 2019 | 4       | 39      | 1           |
| Lenovo               | IdeaPad 320-17AST 80XW                           | 2017 | 4       | 39      | 1           |
| Dell                 | Precision 3510                                   | 2016 | 4       | 39      | 1           |
| MSI                  | CX61 2QF                                         | 2015 | 4       | 39      | 1           |
| Dell                 | Vostro 3546                                      | 2014 | 4       | 39      | 1           |
| Lenovo               | AILZx                                            | 2013 | 4       | 39      | 1           |
| Samsung Electronics  | 600B4B/600B5B                                    | 2012 | 4       | 39      | 1           |
| ASUSTek Computer     | N53Jg                                            | 2010 | 4       | 39      | 1           |
| Toshiba              | Satellite L550                                   | 2009 | 4       | 39      | 1           |
| Lenovo               | ThinkPad X300                                    | 2009 | 4       | 39      | 1           |
| MSI                  | EX620                                            | 2008 | 4       | 39      | 1           |
| Lenovo               | ThinkPad S2                                      | 2018 | 4       | 38      | 1           |
| Dell                 | Inspiron 7570                                    | 2018 | 4       | 38      | 1           |
| Alienware            | 15                                               | 2018 | 4       | 38      | 1           |
| Lenovo               | IdeaPad 720S-14IKB 80XC                          | 2017 | 4       | 38      | 1           |
| Lenovo               | IdeaPad 720-15IKB 81C7                           | 2017 | 4       | 38      | 1           |
| ASUSTek Computer     | N551JW                                           | 2016 | 4       | 38      | 1           |
| Lenovo               | B50-80 80LT                                      | 2015 | 4       | 38      | 1           |
| Acer                 | Aspire E5-773G                                   | 2015 | 4       | 38      | 1           |
| Lenovo               | ThinkPad Edge E145                               | 2014 | 4       | 38      | 1           |
| ASUSTek Computer     | Q551LN                                           | 2014 | 4       | 38      | 1           |
| ASUSTek Computer     | N56JRH                                           | 2014 | 4       | 38      | 1           |
| Sony                 | SVF1521B1EW                                      | 2013 | 4       | 38      | 1           |
| Toshiba              | Satellite C875D                                  | 2012 | 4       | 38      | 1           |
| eMachines            | eME732                                           | 2010 | 4       | 38      | 1           |
| Acer                 | TravelMate 8571                                  | 2009 | 4       | 38      | 1           |
| ASUSTek Computer     | F9E                                              | 2008 | 4       | 38      | 1           |
| Acer                 | Extensa 4620                                     | 2007 | 4       | 38      | 1           |
| Sony                 | VGN-FE880E                                       | 2006 | 4       | 38      | 1           |
| Lenovo               | IdeaPad S145-14API 81UV                          | 2019 | 4       | 37      | 1           |
| Hewlett-Packard      | Laptop                                           | 2018 | 4       | 37      | 1           |
| HUAWEI               | MateBook D                                       | 2018 | 4       | 37      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X540MB_D540MB            | 2018 | 4       | 37      | 1           |
| ASUSTek Computer     | P552LA                                           | 2015 | 4       | 37      | 1           |
| MSI                  | GE70 2PC                                         | 2014 | 4       | 37      | 1           |
| MSI                  | S12T 3M/S12 3M                                   | 2013 | 4       | 37      | 1           |
| Acer                 | Aspire R7-571G                                   | 2013 | 4       | 37      | 1           |
| Samsung Electronics  | 700G7C                                           | 2012 | 4       | 37      | 1           |
| MSI                  | GE60 0NC\0ND                                     | 2012 | 4       | 37      | 1           |
| Fujitsu              | LIFEBOOK S762                                    | 2012 | 4       | 37      | 1           |
| Sony                 | VPCYB35AL                                        | 2011 | 4       | 37      | 1           |
| Samsung Electronics  | QX311/QX411/QX412/QX511                          | 2011 | 4       | 37      | 1           |
| Acer                 | Aspire 4830TG                                    | 2011 | 4       | 37      | 1           |
| MSI                  | U210/U210 Light                                  | 2010 | 4       | 37      | 1           |
| Dell                 | Latitude 2120                                    | 2010 | 4       | 37      | 1           |
| Toshiba              | Portable PC                                      | 2005 | 4       | 37      | 1           |
| Hewlett-Packard      | Presario R4100                                   | 2005 | 4       | 37      | 1           |
| Hewlett-Packard      | Laptop 15q-bu0xx                                 | 2017 | 4       | 36      | 1           |
| MSI                  | GE62 7RE                                         | 2016 | 4       | 36      | 1           |
| Fujitsu              | LIFEBOOK A557                                    | 2016 | 4       | 36      | 1           |
| ASUSTek Computer     | TP300LD                                          | 2014 | 4       | 36      | 1           |
| ASUSTek Computer     | G751JM                                           | 2014 | 4       | 36      | 1           |
| Lenovo               | IdeaPad Z400 Touch VIWZ1                         | 2013 | 4       | 36      | 1           |
| Quanta               | JW6H                                             | 2012 | 4       | 36      | 1           |
| MSI                  | CX700                                            | 2012 | 4       | 36      | 1           |
| Lenovo               | G580 2189                                        | 2012 | 4       | 36      | 1           |
| Fujitsu              | LIFEBOOK N532                                    | 2012 | 4       | 36      | 1           |
| Toshiba              | Satellite L655D                                  | 2011 | 4       | 36      | 1           |
| Lenovo               | V570c HuronRiver Platform                        | 2011 | 4       | 36      | 1           |
| Intel                | Crestline & ICH8M Chipset                        | 2008 | 4       | 36      | 1           |
| Hewlett-Packard      | Presario V6000                                   | 2006 | 4       | 36      | 1           |
| Acer                 | TravelMate 5620                                  | 2006 | 4       | 36      | 1           |
| Notebook             | N150ZU                                           | 2018 | 4       | 35      | 1           |
| Hewlett-Packard      | Pavilion Laptop 14-bf0xx                         | 2017 | 4       | 35      | 1           |
| ASUSTek Computer     | X411UN                                           | 2017 | 4       | 35      | 1           |
| Acer                 | Predator G9-793                                  | 2016 | 4       | 35      | 1           |
| ASUSTek Computer     | X756UXK                                          | 2016 | 4       | 35      | 1           |
| ASUSTek Computer     | X556UAK                                          | 2016 | 4       | 35      | 1           |
| Notebook             | W54_55_94_95_97AU,AUQ                            | 2015 | 4       | 35      | 1           |
| Lenovo               | ThinkPad Edge E530c                              | 2013 | 4       | 35      | 1           |
| Toshiba              | Satellite L850-B5K                               | 2012 | 4       | 35      | 1           |
| Sony                 | SVE15125CBW                                      | 2012 | 4       | 35      | 1           |
| Hewlett-Packard      | ENVY m4                                          | 2012 | 4       | 35      | 1           |
| ASUSTek Computer     | N46VZ                                            | 2012 | 4       | 35      | 1           |
| ASUSTek Computer     | EB1033                                           | 2012 | 4       | 35      | 1           |
| Hewlett-Packard      | Compaq Mini 311-1100                             | 2009 | 4       | 35      | 1           |
| Hewlett-Packard      | Laptop 14s-dq0xxx                                | 2019 | 4       | 34      | 1           |
| Lenovo               | IdeaPad 320-17IKB 81BJ                           | 2017 | 4       | 34      | 1           |
| Intel                | Skylake Platform                                 | 2015 | 4       | 34      | 1           |
| ASUSTek Computer     | X751LX                                           | 2015 | 4       | 34      | 1           |
| ASUSTek Computer     | UX32LN                                           | 2014 | 4       | 34      | 1           |
| Acer                 | Aspire E1-532G                                   | 2013 | 4       | 34      | 1           |
| Hewlett-Packard      | ENVY 6                                           | 2012 | 4       | 34      | 1           |
| Sony                 | VPCEJ2L1E                                        | 2011 | 4       | 34      | 1           |
| Acer                 | TravelMate 7750G                                 | 2011 | 4       | 34      | 1           |
| Intel                | Pine Trail - M                                   | 2010 | 4       | 34      | 1           |
| ASUSTek Computer     | T101MT                                           | 2010 | 4       | 34      | 1           |
| Lenovo               | V310-15IKB 80T3                                  | 2017 | 4       | 33      | 1           |
| Acer                 | TravelMate X349-G2-M                             | 2016 | 4       | 33      | 1           |
| Samsung Electronics  | 450R4E/450R5E/450R4V/450R5V/4450RV               | 2014 | 4       | 33      | 1           |
| Packard Bell         | EasyNote ENLG71BM                                | 2014 | 4       | 33      | 1           |
| Sony                 | SVF15213CBB                                      | 2013 | 4       | 33      | 1           |
| ASUSTek Computer     | U30Sd                                            | 2011 | 4       | 33      | 1           |
| Hewlett-Packard      | Mini 210-2000                                    | 2010 | 4       | 33      | 1           |
| Hewlett-Packard      | Compaq Mini CQ10-400                             | 2010 | 4       | 33      | 1           |
| ASUSTek Computer     | K70ID                                            | 2010 | 4       | 33      | 1           |
| ASUSTek Computer     | VivoBook 15_ASUS Laptop X507UBR                  | 2018 | 4       | 32      | 1           |
| Acer                 | Aspire V3-331                                    | 2014 | 4       | 32      | 1           |
| Toshiba              | Satellite U920T                                  | 2012 | 4       | 32      | 1           |
| Samsung Electronics  | NC208/NC108                                      | 2011 | 4       | 32      | 1           |
| Dell                 | Latitude 7350                                    | 2015 | 4       | 31      | 1           |
| ASUSTek Computer     | X55C                                             | 2012 | 4       | 31      | 1           |
| Hewlett-Packard      | Mini 210-1100                                    | 2010 | 4       | 31      | 1           |
| Hewlett-Packard      | Laptop 15q-ds0xxx                                | 2018 | 4       | 30      | 1           |
| Intel                | ChiefRiver Platform                              | 2013 | 4       | 30      | 1           |
| eMachines            | E627                                             | 2009 | 4       | 30      | 1           |
| MSI                  | MS-N031                                          | 2009 | 4       | 30      | 1           |
| Hewlett-Packard      | 530 Notebook PC(KD080AA#ACB)                     | 2007 | 4       | 30      | 1           |
| Intel                | SLIMBOOK                                         | 2017 | 4       | 29      | 1           |
| Certified Systems... | SnugBook Q series                                | 2017 | 4       | 29      | 1           |
| Google               | Parrot                                           | 2013 | 4       | 29      | 1           |
| Lenovo               | IdeaPad 110-14IBR 80T6                           | 2017 | 4       | 28      | 1           |
| ASUSTek Computer     | UX301LAA                                         | 2014 | 4       | 28      | 1           |
| Google               | Samus                                            | 2019 | 4       | 27      | 1           |
| Acer                 | Aspire SW3-016                                   | 2016 | 4       | 27      | 1           |
| Toshiba              | Satellite C40-C                                  | 2015 | 4       | 26      | 1           |
| Clevo                | W7x0S                                            | 2009 | 4       | 26      | 1           |
| Fujitsu Siemens      | ESPRIMO Mobile V5515                             | 2007 | 4       | 26      | 1           |
| Sony                 | VPCF136FM                                        | 2010 | 3       | 63      | 1           |
| Medion               | X681X                                            | 2010 | 3       | 57      | 1           |
| Hewlett-Packard      | ZBook Create G7 Notebook PC                      | 2020 | 3       | 55      | 1           |
| System76             | Bonobo WS                                        | 2017 | 3       | 53      | 1           |
| Hewlett-Packard      | EliteBook 845 G8 Notebook PC                     | 2021 | 3       | 52      | 1           |
| LincPlus             | P1                                               | 2021 | 3       | 51      | 1           |
| Alienware            | m15 R6                                           | 2021 | 3       | 51      | 1           |
| ASUSTek Computer     | Zephyrus M GM501GM                               | 2018 | 3       | 51      | 1           |
| Lenovo               | Legion S7 15IMH5 82BC                            |      | 3       | 51      | 1           |
| Schenker             | VIA 15 Pro                                       | 2020 | 3       | 50      | 1           |
| Acer                 | Aspire VN7-593G                                  | 2017 | 3       | 50      | 1           |
| Hewlett-Packard      | HDX18                                            | 2008 | 3       | 50      | 1           |
| Lenovo               | Legion S7 15ACH6 82K8                            | 2021 | 3       | 49      | 1           |
| ASUSTek Computer     | ZenBook UX425EA_BX425EA                          | 2021 | 3       | 49      | 1           |
| Dell                 | Vostro 7500                                      | 2020 | 3       | 49      | 1           |
| Dell                 | Latitude 7310                                    | 2020 | 3       | 49      | 1           |
| MSI                  | GE66 Raider 10UG                                 | 2020 | 3       | 48      | 1           |
| ASUSTek Computer     | ROG Zephyrus G15 GA502IU_GA502IU                 | 2020 | 3       | 48      | 1           |
| Teclast              | F15                                              | 2019 | 3       | 48      | 1           |
| Dell                 | Latitude 5420 Rugged                             | 2019 | 3       | 48      | 1           |
| Hewlett-Packard      | ProBook 455 G3                                   | 2016 | 3       | 48      | 1           |
| MSI                  | Bravo 15 B5DD                                    | 2021 | 3       | 47      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506IU_FA506IU              | 2021 | 3       | 47      | 1           |
| Alienware            | m15 R4                                           | 2020 | 3       | 47      | 1           |
| ASUSTek Computer     | ROG Zephyrus M15 GU502LU_GU502LU                 | 2020 | 3       | 47      | 1           |
| MSI                  | GS75 Stealth 8SF                                 | 2019 | 3       | 47      | 1           |
| Lenovo               | ThinkPad A475                                    | 2018 | 3       | 47      | 1           |
| MSI                  | Bravo 17 A4DDR                                   | 2020 | 3       | 46      | 1           |
| Hewlett-Packard      | Laptop 17-ca2xxx                                 | 2020 | 3       | 46      | 1           |
| Notebook             | PA70ES                                           | 2018 | 3       | 46      | 1           |
| AXDIA International  | MYBOOK 14                                        | 2018 | 3       | 46      | 1           |
| Acer                 | Aspire 4930                                      | 2008 | 3       | 46      | 1           |
| TUXEDO               | Pulse 14 Gen1                                    | 2020 | 3       | 45      | 1           |
| MSI                  | GS66 Stealth 10SF                                | 2020 | 3       | 45      | 1           |
| Acer                 | Aspire A315-23G                                  | 2020 | 3       | 45      | 1           |
| Acer                 | Aspire A315-55KG                                 | 2019 | 3       | 45      | 1           |
| ASUSTek Computer     | GL703VD                                          | 2017 | 3       | 45      | 1           |
| Notebook             | P17SM-A                                          | 2014 | 3       | 45      | 1           |
| Clevo                | M7x0K                                            | 2009 | 3       | 45      | 1           |
| ASUSTek Computer     | M50SV                                            | 2008 | 3       | 45      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X415EA_X415EA                | 2021 | 3       | 44      | 1           |
| Notebook             | NL5xRU                                           | 2020 | 3       | 44      | 1           |
| MECHREVO             | Code 01 Series PF5NU1G                           | 2020 | 3       | 44      | 1           |
| Dell                 | Latitude 5290                                    | 2020 | 3       | 44      | 1           |
| Lenovo               | IdeaPad Flex-14API 81SS                          | 2019 | 3       | 44      | 1           |
| Acer                 | Predator PH317-53                                | 2019 | 3       | 44      | 1           |
| Hewlett-Packard      | ZHAN 66 Pro G1                                   | 2018 | 3       | 44      | 1           |
| Gigabyte Technology  | M1M3XAP-00                                       | 2013 | 3       | 44      | 1           |
| Lenovo               | ThinkPad Edge E135                               | 2012 | 3       | 44      | 1           |
| Hewlett-Packard      | ProBook 6540b                                    | 2010 | 3       | 44      | 1           |
| Hewlett-Packard      | Pavilion tx2000                                  | 2008 | 3       | 44      | 1           |
| MSI                  | M670                                             | 2007 | 3       | 44      | 1           |
| Lenovo               | IdeaPad Gaming3 15ARH05D 82EY                    | 2020 | 3       | 43      | 1           |
| Hewlett-Packard      | Laptop 15-gw0xxx                                 | 2020 | 3       | 43      | 1           |
| AZW                  | SEi                                              | 2020 | 3       | 43      | 1           |
| ASUSTek Computer     | ZenBook UX393EA_UX393EA                          | 2020 | 3       | 43      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506II_FA506II              | 2020 | 3       | 43      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506IH_FA506IH              | 2020 | 3       | 43      | 1           |
| Lenovo               | IdeaPad S340-15IML 81NA                          | 2019 | 3       | 43      | 1           |
| Hewlett-Packard      | OMEN by Laptop 15-dc0xxx                         | 2019 | 3       | 43      | 1           |
| PC Specialist        | Recoil II                                        | 2018 | 3       | 43      | 1           |
| ASUSTek Computer     | G752VL                                           | 2016 | 3       | 43      | 1           |
| Dell                 | Precision M4300                                  | 2010 | 3       | 43      | 1           |
| Acer                 | Extensa 4230                                     | 2009 | 3       | 43      | 1           |
| Sony                 | VGN-FW11M                                        | 2008 | 3       | 43      | 1           |
| Medion               | WIM2140                                          | 2007 | 3       | 43      | 1           |
| MSI                  | MEGA BOOK M670                                   | 2007 | 3       | 43      | 1           |
| Acer                 | TravelMate 5320                                  | 2007 | 3       | 43      | 1           |
| MSI                  | GP66 Leopard 11UG                                | 2021 | 3       | 42      | 1           |
| Acer                 | Aspire A515-56G                                  | 2021 | 3       | 42      | 1           |
| System76             | Pangolin                                         | 2020 | 3       | 42      | 1           |
| Maibenben            | S431                                             | 2020 | 3       | 42      | 1           |
| Hewlett-Packard      | ProBook 650 G8 Notebook PC                       | 2020 | 3       | 42      | 1           |
| Dell                 | Inspiron 7490                                    | 2020 | 3       | 42      | 1           |
| Acer                 | Swift SF313-52G                                  | 2020 | 3       | 42      | 1           |
| Acer                 | Nitro AN715-52                                   | 2020 | 3       | 42      | 1           |
| Acer                 | Aspire A715-42G                                  | 2020 | 3       | 42      | 1           |
| Acer                 | Aspire A715-74G                                  | 2019 | 3       | 42      | 1           |
| ASUSTek Computer     | ROG Strix G731GU_G731GU                          | 2019 | 3       | 42      | 1           |
| Lenovo               | Legion Y740-17ICHg 81HH                          | 2018 | 3       | 42      | 1           |
| MSI                  | GP72 2QE                                         | 2015 | 3       | 42      | 1           |
| Lenovo               | ThinkPad X1                                      | 2011 | 3       | 42      | 1           |
| Hewlett-Packard      | ProBook 6545b                                    | 2009 | 3       | 42      | 1           |
| Packard Bell         | EasyNote ML65                                    | 2008 | 3       | 42      | 1           |
| Acer                 | TravelMate 7730G                                 | 2008 | 3       | 42      | 1           |
| Dell                 | Latitude 5310                                    | 2020 | 3       | 41      | 1           |
| ASUSTek Computer     | ROG Zephyrus M15 GU502LV_GU502LV                 | 2020 | 3       | 41      | 1           |
| MSI                  | GL75 9SDK                                        | 2019 | 3       | 41      | 1           |
| Alienware            | 17 R2                                            | 2019 | 3       | 41      | 1           |
| ASUSTek Computer     | Zephyrus M GU502GV_GU502GV                       | 2019 | 3       | 41      | 1           |
| ASUSTek Computer     | UX490UAR                                         | 2018 | 3       | 41      | 1           |
| Hewlett-Packard      | ENVY Laptop 17-ae0xx                             | 2017 | 3       | 41      | 1           |
| MSI                  | CX61 2PC                                         | 2014 | 3       | 41      | 1           |
| Acer                 | Aspire 4560                                      | 2011 | 3       | 41      | 1           |
| Sony                 | VPCEB23FM                                        | 2010 | 3       | 41      | 1           |
| Medion               | P6612                                            | 2008 | 3       | 41      | 1           |
| Lenovo               | IdeaPad Y430 20005                               | 2008 | 3       | 41      | 1           |
| Sony                 | VGN-FZ11MR                                       | 2007 | 3       | 41      | 1           |
| Sony                 | VGN-CR19VN_B                                     | 2007 | 3       | 41      | 1           |
| MSI                  | GX700                                            | 2007 | 3       | 41      | 1           |
| Hewlett-Packard      | EliteBook 835 G7 Notebook PC                     | 2020 | 3       | 40      | 1           |
| Gigabyte Technology  | AERO 15-X9                                       | 2019 | 3       | 40      | 1           |
| ASUSTek Computer     | ZenBook UX434FLC_UX434FL                         | 2019 | 3       | 40      | 1           |
| MSI                  | GS73 Stealth 8RD                                 | 2018 | 3       | 40      | 1           |
| Lenovo               | Legion Y7000P-1060 81LF                          | 2018 | 3       | 40      | 1           |
| Hewlett-Packard      | EliteBook 745 G4                                 | 2017 | 3       | 40      | 1           |
| ASUSTek Computer     | N751JX                                           | 2015 | 3       | 40      | 1           |
| MSI                  | GE60 2PC                                         | 2014 | 3       | 40      | 1           |
| Dell                 | System XPS 15Z                                   | 2012 | 3       | 40      | 1           |
| Acer                 | Aspire 8735                                      | 2009 | 3       | 40      | 1           |
| Quanta               | TW8/SW8/DW8                                      | 2008 | 3       | 40      | 1           |
| Sony                 | VGN-FZ21M                                        | 2007 | 3       | 40      | 1           |
| ASUSTek Computer     | F3Sv                                             | 2007 | 3       | 40      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X521IA_D533IA                | 2021 | 3       | 39      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X513EAN_X513EA               | 2021 | 3       | 39      | 1           |
| Hewlett-Packard      | Pro x2 612 G1 Tablet                             | 2020 | 3       | 39      | 1           |
| Acer                 | Predator PH315-53                                | 2020 | 3       | 39      | 1           |
| Lenovo               | ThinkPad E490s                                   | 2018 | 3       | 39      | 1           |
| ASUSTek Computer     | ZenBook 13 UX331FN_UX331FN                       | 2018 | 3       | 39      | 1           |
| MSI                  | GE72 2QC                                         | 2015 | 3       | 39      | 1           |
| ASUSTek Computer     | G501VW                                           | 2015 | 3       | 39      | 1           |
| Toshiba              | QOSMIO X75-A                                     | 2014 | 3       | 39      | 1           |
| Notebook             | P65_P67SE                                        | 2014 | 3       | 39      | 1           |
| Hewlett-Packard      | ENVY TS m7                                       | 2014 | 3       | 39      | 1           |
| ASUSTek Computer     | X550WA                                           | 2014 | 3       | 39      | 1           |
| ASUSTek Computer     | N56JR                                            | 2013 | 3       | 39      | 1           |
| Lenovo               | G585 2181                                        | 2012 | 3       | 39      | 1           |
| Toshiba              | Satellite P770                                   | 2011 | 3       | 39      | 1           |
| Sony                 | VPCYB3V1E                                        | 2011 | 3       | 39      | 1           |
| MSI                  | MS-16G1                                          | 2010 | 3       | 39      | 1           |
| Semp Toshiba         | IS 1413G                                         | 2009 | 3       | 39      | 1           |
| MSI                  | MEGA BOOK S430                                   | 2007 | 3       | 39      | 1           |
| Acer                 | Swift SF314-511                                  | 2021 | 3       | 38      | 1           |
| Samsung Electronics  | 950XCJ/951XCJ/950XCR                             | 2020 | 3       | 38      | 1           |
| Samsung Electronics  | 730QCJ/730QCR                                    | 2020 | 3       | 38      | 1           |
| Fujitsu              | LIFEBOOK E5510                                   | 2020 | 3       | 38      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X515MA_X515MA                | 2020 | 3       | 38      | 1           |
| ASUSTek Computer     | ASUS EXPERTBOOK P2451FA_P2451FA                  | 2020 | 3       | 38      | 1           |
| MSI                  | GF65 Thin 9SD                                    | 2019 | 3       | 38      | 1           |
| Lenovo               | IdeaPad S540-15IML 81NG                          | 2019 | 3       | 38      | 1           |
| Hewlett-Packard      | 245 G7                                           | 2019 | 3       | 38      | 1           |
| ASUSTek Computer     | ZenBook UX434FAC_UX433FAC                        | 2019 | 3       | 38      | 1           |
| Fujitsu              | LIFEBOOK E556                                    | 2018 | 3       | 38      | 1           |
| Purism               | Librem 13 v2                                     | 2017 | 3       | 38      | 1           |
| Acer                 | Predator PH317-51                                | 2017 | 3       | 38      | 1           |
| ASUSTek Computer     | B9440UA                                          | 2017 | 3       | 38      | 1           |
| MSI                  | GS60 6QE                                         | 2016 | 3       | 38      | 1           |
| DNS                  | N150SC                                           | 2015 | 3       | 38      | 1           |
| Lenovo               | B590 62743PG                                     | 2013 | 3       | 38      | 1           |
| ASUSTek Computer     | N53Jl                                            | 2011 | 3       | 38      | 1           |
| eMachines            | eME640G                                          | 2010 | 3       | 38      | 1           |
| Gateway              | M-7315U                                          | 2008 | 3       | 38      | 1           |
| Lenovo               | ThinkPad T60p                                    | 2006 | 3       | 38      | 1           |
| Lenovo               | Yoga Slim 7 Pro 14ACH5 82MS                      | 2021 | 3       | 37      | 1           |
| Acer                 | Aspire A515-55G                                  | 2020 | 3       | 37      | 1           |
| Positivo             | NB50TH                                           | 2019 | 3       | 37      | 1           |
| HUAWEI               | VLT-WX0                                          | 2019 | 3       | 37      | 1           |
| Google               | Nami                                             | 2019 | 3       | 37      | 1           |
| ASUSTek Computer     | ZenBook UX431FLC_UX431FL                         | 2019 | 3       | 37      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X510QA_F510QA                | 2019 | 3       | 37      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509JB_X509JB                | 2019 | 3       | 37      | 1           |
| Hewlett-Packard      | Laptop 14z-cm000                                 | 2018 | 3       | 37      | 1           |
| MSI                  | PE70 6QE                                         | 2015 | 3       | 37      | 1           |
| ASUSTek Computer     | N551VW                                           | 2015 | 3       | 37      | 1           |
| Sony                 | VPCSB1V9R                                        | 2011 | 3       | 37      | 1           |
| Acer                 | Aspire 8950G                                     | 2011 | 3       | 37      | 1           |
| ASUSTek Computer     | U41SV                                            | 2011 | 3       | 37      | 1           |
| Lenovo               | 3000 G410                                        | 2008 | 3       | 37      | 1           |
| Hewlett-Packard      | Pavilion ZV6100                                  | 2005 | 3       | 37      | 1           |
| Lenovo               | IdeaPad 3 17IIL05 81WF                           | 2020 | 3       | 36      | 1           |
| Lenovo               | IdeaPad 1 11ADA05 82GV                           | 2020 | 3       | 36      | 1           |
| Dell                 | Inspiron 15 5501                                 | 2020 | 3       | 36      | 1           |
| MSI                  | GF63 Thin 9RC                                    | 2019 | 3       | 36      | 1           |
| Hewlett-Packard      | Laptop 14-bp1xx                                  | 2017 | 3       | 36      | 1           |
| ASUSTek Computer     | X411UQ                                           | 2017 | 3       | 36      | 1           |
| ASUSTek Computer     | X555UA                                           | 2015 | 3       | 36      | 1           |
| Lenovo               | ThinkPad T431s                                   | 2013 | 3       | 36      | 1           |
| Acer                 | Aspire E1-771                                    | 2013 | 3       | 36      | 1           |
| ASUSTek Computer     | X450CP                                           | 2013 | 3       | 36      | 1           |
| Toshiba              | QOSMIO X775                                      | 2012 | 3       | 36      | 1           |
| Acer                 | Aspire V5-571P                                   | 2012 | 3       | 36      | 1           |
| Hewlett-Packard      | ProBook 5320m                                    | 2010 | 3       | 36      | 1           |
| ASUSTek Computer     | K50AF                                            | 2010 | 3       | 36      | 1           |
| eMachines            | E625                                             | 2009 | 3       | 36      | 1           |
| Sony                 | VPCW21S1R                                        | 2009 | 3       | 36      | 1           |
| Hewlett-Packard      | Pavilion dv3500                                  | 2009 | 3       | 36      | 1           |
| Apple                | MacBookPro2,1                                    | 2008 | 3       | 36      | 1           |
| ASUSTek Computer     | F5GL                                             | 2008 | 3       | 36      | 1           |
| LG Electronics       | P1-J150B                                         | 2006 | 3       | 36      | 1           |
| HUAWEI               | KLVD-WXX9                                        | 2021 | 3       | 35      | 1           |
| Google               | Careena                                          | 2020 | 3       | 35      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X540MAR_X543MA               | 2020 | 3       | 35      | 1           |
| Acer                 | Swift SF314-55                                   | 2018 | 3       | 35      | 1           |
| ASUSTek Computer     | VivoBook 17_ASUS Laptop X705MB_X705MB            | 2018 | 3       | 35      | 1           |
| MSI                  | GP62MVR 7RFX                                     | 2017 | 3       | 35      | 1           |
| MSI                  | GP62M 7RDX                                       | 2017 | 3       | 35      | 1           |
| Hewlett-Packard      | ProBook x360 11 G1 EE                            | 2017 | 3       | 35      | 1           |
| MSI                  | GP62 6QF                                         | 2016 | 3       | 35      | 1           |
| MSI                  | GL62 6QF                                         | 2016 | 3       | 35      | 1           |
| MSI                  | GL62 6QD                                         | 2016 | 3       | 35      | 1           |
| Hewlett-Packard      | ENVY 15 x360 PC                                  | 2015 | 3       | 35      | 1           |
| Acer                 | Aspire V3-575G                                   | 2015 | 3       | 35      | 1           |
| Toshiba              | Satellite E45-B                                  | 2014 | 3       | 35      | 1           |
| Lenovo               | Edge 15 80H1                                     | 2014 | 3       | 35      | 1           |
| Acer                 | TMP645-M                                         | 2014 | 3       | 35      | 1           |
| ASUSTek Computer     | X751LK                                           | 2014 | 3       | 35      | 1           |
| Fujitsu              | LIFEBOOK U904                                    | 2013 | 3       | 35      | 1           |
| Casper               | Casper Nirvana Notebook                          | 2013 | 3       | 35      | 1           |
| Lenovo               | IdeaPad Z480                                     | 2012 | 3       | 35      | 1           |
| Hewlett-Packard      | ENVY Sleekbook 4                                 | 2012 | 3       | 35      | 1           |
| Acer                 | Aspire 5516                                      | 2009 | 3       | 35      | 1           |
| Semp Toshiba         | K201                                             | 2008 | 3       | 35      | 1           |
| Apple                | MacBook9,1                                       | 2020 | 3       | 34      | 1           |
| Hewlett-Packard      | Laptop 14s-cf1xxx                                | 2019 | 3       | 34      | 1           |
| Google               | Lars                                             | 2018 | 3       | 34      | 1           |
| Notebook             | W65_W67RN,RC1,RCY                                | 2016 | 3       | 34      | 1           |
| ASUSTek Computer     | UX310UQ                                          | 2016 | 3       | 34      | 1           |
| Dell                 | Inspiron 3458                                    | 2014 | 3       | 34      | 1           |
| ASUSTek Computer     | X450LN                                           | 2014 | 3       | 34      | 1           |
| Toshiba              | PORTEGE Z830                                     | 2013 | 3       | 34      | 1           |
| System76             | Bonobo Extreme                                   | 2012 | 3       | 34      | 1           |
| Sony                 | VPCZ21V9E                                        | 2012 | 3       | 34      | 1           |
| MSI                  | GE70 0NC/GE70 0ND                                | 2012 | 3       | 34      | 1           |
| Toshiba              | NB305                                            | 2010 | 3       | 34      | 1           |
| DNS                  | 101                                              | 2010 | 3       | 34      | 1           |
| Packard Bell         | EasyNote DT85                                    | 2009 | 3       | 34      | 1           |
| Hewlett-Packard      | Presario V6500                                   | 2007 | 3       | 34      | 1           |
| Medion               | WIM 2070                                         | 2005 | 3       | 34      | 1           |
| Lenovo               | IdeaPad S530-13IWL 81J7                          | 2019 | 3       | 33      | 1           |
| ASUSTek Computer     | ZenBook UX431FA_UX431FA                          | 2019 | 3       | 33      | 1           |
| MSI                  | PS63 Modern 8M                                   | 2018 | 3       | 33      | 1           |
| MSI                  | GP63 Leopard 8RD                                 | 2018 | 3       | 33      | 1           |
| MSI                  | GL63 8RE                                         | 2018 | 3       | 33      | 1           |
| Lenovo               | ThinkPad L470 W10DG                              | 2018 | 3       | 33      | 1           |
| Dell                 | Latitude 5280                                    | 2018 | 3       | 33      | 1           |
| Acer                 | Swift SF514-51                                   | 2016 | 3       | 33      | 1           |
| Lenovo               | U41-70 80JV                                      | 2015 | 3       | 33      | 1           |
| Acer                 | Aspire R3-471TG                                  | 2015 | 3       | 33      | 1           |
| Toshiba              | Satellite L75-B                                  | 2014 | 3       | 33      | 1           |
| Fujitsu              | LIFEBOOK E554                                    | 2014 | 3       | 33      | 1           |
| Alienware            | 13                                               | 2014 | 3       | 33      | 1           |
| Toshiba              | Satellite C850-B1K                               | 2012 | 3       | 33      | 1           |
| Sony                 | VPCSB35FB                                        | 2011 | 3       | 33      | 1           |
| Dell                 | Latitude X1                                      | 2006 | 3       | 33      | 1           |
| Fujitsu Siemens      | AMILO PRO V2030                                  | 2005 | 3       | 33      | 1           |
| Fujitsu Siemens      | AMILO L Series                                   | 2004 | 3       | 33      | 1           |
| MSI                  | GL62M 7RD                                        | 2017 | 3       | 32      | 1           |
| ASUSTek Computer     | P2540UA                                          | 2016 | 3       | 32      | 1           |
| Toshiba              | Satellite L55-C                                  | 2015 | 3       | 32      | 1           |
| Lenovo               | Edge 2-1580 80QF                                 | 2015 | 3       | 32      | 1           |
| Toshiba              | Satellite C70-B                                  | 2014 | 3       | 32      | 1           |
| Notebook             | W54_W550SU2                                      | 2014 | 3       | 32      | 1           |
| ASUSTek Computer     | X450LD                                           | 2014 | 3       | 32      | 1           |
| ASUSTek Computer     | U31SG                                            | 2012 | 3       | 32      | 1           |
| ASUSTek Computer     | UX390UAK                                         | 2019 | 3       | 31      | 1           |
| ASUSTek Computer     | X751MD                                           | 2014 | 3       | 31      | 1           |
| Lenovo               | B490 37722QP                                     | 2013 | 3       | 31      | 1           |
| Lenovo               | V370 HuronRiver Platform                         | 2011 | 3       | 31      | 1           |
| Lenovo               | B570 1068GEG                                     | 2011 | 3       | 31      | 1           |
| Acer                 | Aspire 1650                                      | 2005 | 3       | 31      | 1           |
| IBM                  | ThinkPad T41                                     | 2004 | 3       | 31      | 1           |
| Positivo             | Smash                                            | 2019 | 3       | 30      | 1           |
| ASUSTek Computer     | X555SJ                                           | 2016 | 3       | 30      | 1           |
| Semp Toshiba         | IS 1442                                          | 2011 | 3       | 30      | 1           |
| Lenovo               | PIWG1                                            | 2011 | 3       | 30      | 1           |
| Lenovo               | B570 1068A8U                                     | 2011 | 3       | 30      | 1           |
| Purism               | Librem 13 v4                                     | 2019 | 3       | 29      | 1           |
| ASUSTek Computer     | E403SA                                           | 2015 | 3       | 29      | 1           |
| Lenovo               | S20-30 Touch 20434                               | 2014 | 3       | 29      | 1           |
| Toshiba              | Satellite NB10-A-104                             | 2013 | 3       | 29      | 1           |
| Sony                 | VPCEH35FM                                        | 2011 | 3       | 29      | 1           |
| Google               | Kefka                                            | 2020 | 3       | 28      | 1           |
| ASUSTek Computer     | TP301UA                                          | 2016 | 3       | 28      | 1           |
| Acer                 | Aspire V3-112P                                   | 2014 | 3       | 28      | 1           |
| Compal               | PBL00                                            | 2010 | 3       | 28      | 1           |
| Dell                 | Inspiron 1010                                    | 2009 | 3       | 28      | 1           |
| Hewlett-Packard      | 510 Notebook PC                                  | 2006 | 3       | 28      | 1           |
| Dell                 | XPS L322X                                        | 2013 | 3       | 27      | 1           |
| Acer                 | Aspire V5-132                                    | 2013 | 3       | 27      | 1           |
| Intel                | SandyBridge Platform                             | 2012 | 3       | 27      | 1           |
| DEXP                 | NAVIS PX100                                      | 2018 | 3       | 26      | 1           |
| Digma                | ES6021EW                                         | 2018 | 3       | 25      | 1           |
| Digma                | CITI E301 ES3008EW                               | 2017 | 3       | 25      | 1           |
| OEM                  | U50SI1                                           | 2009 | 3       | 25      | 1           |
| Acer                 | Aspire SW5-014                                   | 2015 | 3       | 24      | 1           |
| Digma                | ES6017EW                                         | 2018 | 3       | 23      | 1           |
| TrekStor             | SurfBook W2                                      | 2016 | 3       | 23      | 1           |
| Mediacom             | WinPad 11,6 FullHD- WPU11                        | 2016 | 3       | 23      | 1           |
| ASUSTek Computer     | T101HA                                           | 2016 | 3       | 23      | 1           |
| Hewlett-Packard      | Stream 11 Pro                                    | 2014 | 3       | 23      | 1           |
| Teclast              | TbooK 11                                         | 2016 | 3       | 22      | 1           |
| Insyde               | i107c                                            | 2015 | 3       | 22      | 1           |
| Minix                | NEO Z83-4A                                       | 2018 | 3       | 21      | 1           |
| DEXP                 | OEM                                              | 2015 | 3       | 21      | 1           |
| Notebook             | P570WM                                           | 2013 | 2       | 82      | 1           |
| ONE-NETBOOK TECHN... | ONE XPLAYER                                      | 2021 | 2       | 63      | 1           |
| Notebook             | X170SM                                           | 2020 | 2       | 62      | 1           |
| Alienware            | Area-51m                                         | 2019 | 2       | 61      | 1           |
| Hewlett-Packard      | EliteBook 735 G5                                 | 2018 | 2       | 60      | 1           |
| Eluktronics          | MAX-17                                           | 2020 | 2       | 59      | 1           |
| Hewlett-Packard      | EliteBook 755 G5                                 | 2019 | 2       | 59      | 1           |
| Acer                 | Aspire 8942G                                     | 2009 | 2       | 59      | 1           |
| PC Specialist        | VortexVIII 17                                    | 2018 | 2       | 58      | 1           |
| MSI                  | MS-1727                                          | 2010 | 2       | 57      | 1           |
| ASUSTek Computer     | ZenBook UX482EA_UX482EA                          | 2021 | 2       | 56      | 1           |
| TUXEDO               | TUXEDO_Book_XA1510                               | 2020 | 2       | 56      | 1           |
| Toshiba              | QOSMIO X505                                      | 2010 | 2       | 56      | 1           |
| Sony                 | VPCF13Z1E                                        | 2010 | 2       | 56      | 1           |
| Lenovo               | ThinkPad P1 Gen 4i                               | 2021 | 2       | 55      | 1           |
| ASUSTek Computer     | TUF Gaming FA506IV_FX506IV                       | 2020 | 2       | 55      | 1           |
| Hewlett-Packard      | EliteBook 835 G8 Notebook PC                     | 2021 | 2       | 54      | 1           |
| Gigabyte Technology  | AERO 15 KD                                       | 2021 | 2       | 54      | 1           |
| SLIMBOOK             | TITAN                                            | 2021 | 2       | 53      | 1           |
| Lenovo               | IdeaPad Y570 0862                                | 2011 | 2       | 53      | 1           |
| Packard Bell         | EasyNote LX                                      | 2010 | 2       | 53      | 1           |
| Lenovo               | Legion 5P 15IMH05H 82AW                          | 2020 | 2       | 52      | 1           |
| Lenovo               | ThinkPad P15 Gen 2i                              | 2021 | 2       | 51      | 1           |
| Lenovo               | IdeaPad 5 Pro 14ACN6 82L7                        | 2021 | 2       | 51      | 1           |
| ASUSTek Computer     | TUF Gaming FA706II_FA706II                       | 2020 | 2       | 51      | 1           |
| UNOWHY               | Y13G002S4EI                                      | 2019 | 2       | 51      | 1           |
| System76             | Adder WS                                         | 2019 | 2       | 51      | 1           |
| Dell                 | Inspiron 3780                                    | 2018 | 2       | 51      | 1           |
| MSI                  | GS66 Stealth 10UG                                | 2021 | 2       | 50      | 1           |
| Lenovo               | ThinkBook 15p 20V3                               | 2020 | 2       | 50      | 1           |
| Dell                 | Latitude 7520                                    | 2021 | 2       | 49      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming F15 FX506HCB_FX506HCB            | 2021 | 2       | 49      | 1           |
| Schenker             | SCHENKER_SLIM15_SSL15L19                         | 2019 | 2       | 49      | 1           |
| Alienware            | m17 R2                                           | 2019 | 2       | 49      | 1           |
| Lenovo               | IdeaPad 720S-15IKB 81AC                          | 2018 | 2       | 49      | 1           |
| PC Specialist        | PCX0DX                                           | 2020 | 2       | 48      | 1           |
| MSI                  | GS66 Stealth 10SFS                               | 2020 | 2       | 48      | 1           |
| Fusion5              | A90B_Pro                                         | 2020 | 2       | 48      | 1           |
| TUXEDO               | Book XC1711                                      | 2019 | 2       | 48      | 1           |
| Hewlett-Packard      | ProBook 455 G6                                   | 2019 | 2       | 48      | 1           |
| Avell High Perfor... | A60 MUV                                          | 2019 | 2       | 48      | 1           |
| Dell                 | Vostro 3555                                      | 2011 | 2       | 48      | 1           |
| ASUSTek Computer     | M51Kr                                            | 2008 | 2       | 48      | 1           |
| ASUSTek Computer     | V1S                                              | 2007 | 2       | 48      | 1           |
| MSI                  | GE66 Raider 11UG                                 | 2021 | 2       | 47      | 1           |
| Acer                 | Nitro AN515-56                                   | 2021 | 2       | 47      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506QE_TUF506QE             | 2021 | 2       | 47      | 1           |
| HANSUNG COMPUTER     | TFX5470H                                         | 2020 | 2       | 47      | 1           |
| ASUSTek Computer     | TUF Gaming FA506IV_TUF506IV                      | 2020 | 2       | 47      | 1           |
| FUJITSU CLIENT CO... | LIFEBOOK U759                                    | 2019 | 2       | 47      | 1           |
| Hampoo               | C3W6_AP108_4GB                                   | 2017 | 2       | 47      | 1           |
| ASUSTek Computer     | U36JC                                            | 2011 | 2       | 47      | 1           |
| Dell                 | G15 5515                                         | 2021 | 2       | 46      | 1           |
| Lenovo               | Yoga Slim 7 15IMH05 82AB                         | 2020 | 2       | 46      | 1           |
| Lenovo               | XiaoXinAir 15ARE 2021 82GL                       | 2020 | 2       | 46      | 1           |
| Lenovo               | Legion Y540-17IRH-PG0 81T3                       | 2020 | 2       | 46      | 1           |
| Hewlett-Packard      | ZBook Studio G7 Mobile Workstation               | 2020 | 2       | 46      | 1           |
| Hewlett-Packard      | Laptop 17z-ca300                                 | 2020 | 2       | 46      | 1           |
| ASUSTek Computer     | ASUS EXPERTBOOK B9400CEA_B9450CEA                | 2020 | 2       | 46      | 1           |
| Hewlett-Packard      | ProBook 445 G6                                   | 2019 | 2       | 46      | 1           |
| Irbis                | TW118                                            | 2018 | 2       | 46      | 1           |
| ASUSTek Computer     | N73JF                                            | 2010 | 2       | 46      | 1           |
| ASUSTek Computer     | N50Vn                                            | 2009 | 2       | 46      | 1           |
| ASUSTek Computer     | M70SA                                            | 2008 | 2       | 46      | 1           |
| Hewlett-Packard      | Laptop 17-cp0xxx                                 | 2021 | 2       | 45      | 1           |
| MSI                  | Prestige 14 A11SCX                               | 2020 | 2       | 45      | 1           |
| ASUSTek Computer     | ZenBook UX434IQ_UM433IQ                          | 2020 | 2       | 45      | 1           |
| ASUSTek Computer     | TUF Gaming FA506IH_FA506IH                       | 2020 | 2       | 45      | 1           |
| ASUSTek Computer     | ROG Strix G531GT_GL531GT                         | 2020 | 2       | 45      | 1           |
| Hewlett-Packard      | ProBook 445R G6                                  | 2019 | 2       | 45      | 1           |
| Lenovo               | ThinkPad L380 Yoga                               | 2018 | 2       | 45      | 1           |
| Sony                 | VPCS12C5E                                        | 2010 | 2       | 45      | 1           |
| ASUSTek Computer     | M51Sr                                            | 2007 | 2       | 45      | 1           |
| ASUSTek Computer     | A7Sv                                             | 2007 | 2       | 45      | 1           |
| Acer                 | Swift SF514-55TA                                 | 2021 | 2       | 44      | 1           |
| ASUSTek Computer     | ASUS TUF Dash F15 FX516PE_FX516PE                | 2021 | 2       | 44      | 1           |
| Lenovo               | ThinkPad S5 Yoga 15                              | 2019 | 2       | 44      | 1           |
| Entroware            | Proteus                                          | 2019 | 2       | 44      | 1           |
| Dell                 | Inspiron 5485                                    | 2019 | 2       | 44      | 1           |
| SCHNEIDER            | SCL142ALM                                        | 2018 | 2       | 44      | 1           |
| Hometech             | Alfa 400C                                        | 2018 | 2       | 44      | 1           |
| EUROCOM              | Q6                                               | 2018 | 2       | 44      | 1           |
| MSI                  | MS-7A39                                          | 2017 | 2       | 44      | 1           |
| Sony                 | VPCSA2V9R                                        | 2012 | 2       | 44      | 1           |
| Hewlett-Packard      | Pavilion HDX9200                                 | 2008 | 2       | 44      | 1           |
| Hewlett-Packard      | HDX16                                            | 2008 | 2       | 44      | 1           |
| Fujitsu Siemens      | AMILO Xa 1526                                    | 2008 | 2       | 44      | 1           |
| ASUSTek Computer     | ROG Strix G713QM_G713QM                          | 2021 | 2       | 43      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming A17 FA706QR_FA706QR              | 2021 | 2       | 43      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming A17 FA706II_FA706II              | 2021 | 2       | 43      | 1           |
| MSI                  | GF75 Thin 10UEK                                  | 2020 | 2       | 43      | 1           |
| Lenovo               | XiaoXin Air 13IWL 81J8                           | 2019 | 2       | 43      | 1           |
| Lenovo               | Legion Y9000X 2020 81TH                          | 2019 | 2       | 43      | 1           |
| Dell                 | Inspiron 5485 2n1                                | 2019 | 2       | 43      | 1           |
| Google               | Caroline                                         | 2018 | 2       | 43      | 1           |
| Dell                 | Inspiron 7447                                    | 2015 | 2       | 43      | 1           |
| Toshiba              | QOSMIO X770                                      | 2012 | 2       | 43      | 1           |
| Lenovo               | ThinkPad E525                                    | 2011 | 2       | 43      | 1           |
| Sony                 | VPCS11X9E                                        | 2010 | 2       | 43      | 1           |
| Toshiba              | Satellite X205                                   | 2008 | 2       | 43      | 1           |
| Toshiba              | Satellite Pro A300                               | 2008 | 2       | 43      | 1           |
| ASUSTek Computer     | F8Va                                             | 2008 | 2       | 43      | 1           |
| Sony                 | VGN-FZ31SR                                       | 2007 | 2       | 43      | 1           |
| ASUSTek Computer     | X55SV                                            | 2007 | 2       | 43      | 1           |
| Intel Client Systems | LAPBC510                                         | 2021 | 2       | 42      | 1           |
| Hewlett-Packard      | OMEN Laptop 15-en1xxx                            | 2021 | 2       | 42      | 1           |
| BESSTAR Tech         | X400                                             | 2021 | 2       | 42      | 1           |
| Notebook             | NH5x_7xDCx_DDx                                   | 2020 | 2       | 42      | 1           |
| Lenovo               | ThinkBook 13s G2 ARE 20WC                        | 2020 | 2       | 42      | 1           |
| Lenovo               | 20SL                                             | 2020 | 2       | 42      | 1           |
| Hewlett-Packard      | ZHAN 66 Pro A 14 G3                              | 2020 | 2       | 42      | 1           |
| ASUSTek Computer     | ZenBook UX434FL_UX434FL                          | 2020 | 2       | 42      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming A17 FA706II_FX706II              | 2020 | 2       | 42      | 1           |
| PC Specialist        | Recoil III                                       | 2019 | 2       | 42      | 1           |
| Google               | Eve                                              | 2019 | 2       | 42      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X580GD_NX580GD               | 2019 | 2       | 42      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509FA_F509FA                | 2019 | 2       | 42      | 1           |
| Hewlett-Packard      | EliteBook 755 G2                                 | 2015 | 2       | 42      | 1           |
| Sony                 | SVE1112M1RB                                      | 2013 | 2       | 42      | 1           |
| Sony                 | SVE14A27CXH                                      | 2012 | 2       | 42      | 1           |
| Medion               | P6815                                            | 2012 | 2       | 42      | 1           |
| Hewlett-Packard      | ProBook 6565b                                    | 2012 | 2       | 42      | 1           |
| Dell                 | Vostro 1440                                      | 2012 | 2       | 42      | 1           |
| Sony                 | VPCEB4E1R                                        | 2011 | 2       | 42      | 1           |
| Sony                 | VPCS135FX                                        | 2010 | 2       | 42      | 1           |
| Fujitsu              | AMILO Pi 3560                                    | 2010 | 2       | 42      | 1           |
| ASUSTek Computer     | M70Vn                                            | 2009 | 2       | 42      | 1           |
| Sony                 | VGN-Z11MN_B                                      | 2008 | 2       | 42      | 1           |
| Notebook             | NH5x_7xDPx                                       | 2021 | 2       | 41      | 1           |
| ASUSTek Computer     | ASUS TUF Gaming F15 FX506HE_FX506HE              | 2021 | 2       | 41      | 1           |
| FUJITSU CLIENT CO... | LIFEBOOK E5510                                   | 2020 | 2       | 41      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X415JA_F415JA                | 2020 | 2       | 41      | 1           |
| ASUSTek Computer     | ROG Strix G513QR_G513QR                          | 2020 | 2       | 41      | 1           |
| SLIMBOOK             | PROX14                                           | 2019 | 2       | 41      | 1           |
| Notebook             | NHxxRZQ                                          | 2019 | 2       | 41      | 1           |
| ASUSTek Computer     | ROG Strix G531GW_G531GW                          | 2019 | 2       | 41      | 1           |
| MSI                  | GE72 7RD                                         | 2017 | 2       | 41      | 1           |
| Hewlett-Packard      | Elite x2 1012 G1 Tablet                          | 2017 | 2       | 41      | 1           |
| MSI                  | GL72 6QF                                         | 2016 | 2       | 41      | 1           |
| Sony                 | VPCSB1V9E                                        | 2012 | 2       | 41      | 1           |
| LG Electronics       | A530-T.BE76P1                                    | 2011 | 2       | 41      | 1           |
| Acer                 | Aspire 5830TG                                    | 2011 | 2       | 41      | 1           |
| Sony                 | VPCEB33FM                                        | 2010 | 2       | 41      | 1           |
| Lenovo               | 3000 N200 0769ARS                                | 2007 | 2       | 41      | 1           |
| Lenovo               | IdeaPad 3 17ALC6 82KV                            | 2021 | 2       | 40      | 1           |
| Dell                 | G15 5510                                         | 2021 | 2       | 40      | 1           |
| Google               | Grunt                                            | 2020 | 2       | 40      | 1           |
| PC Specialist        | N150CU                                           | 2019 | 2       | 40      | 1           |
| MSI                  | P65 Creator 9SE                                  | 2019 | 2       | 40      | 1           |
| Dell                 | Latitude 5424 Rugged                             | 2019 | 2       | 40      | 1           |
| Lenovo               | IdeaPad 320-14AST 80XU                           | 2018 | 2       | 40      | 1           |
| Hewlett-Packard      | ZBook 14u G4                                     | 2017 | 2       | 40      | 1           |
| Notebook             | N15_N17RD1                                       | 2016 | 2       | 40      | 1           |
| Hewlett-Packard      | EliteBook 856                                    | 2016 | 2       | 40      | 1           |
| DNS                  | W65_67SC                                         | 2015 | 2       | 40      | 1           |
| ASUSTek Computer     | X750JN                                           | 2014 | 2       | 40      | 1           |
| ASUSTek Computer     | TP300LDB                                         | 2014 | 2       | 40      | 1           |
| ASUSTek Computer     | G750JZA                                          | 2014 | 2       | 40      | 1           |
| Notebook             | W350STQ/W370ST                                   | 2013 | 2       | 40      | 1           |
| Notebook             | P15SM                                            | 2013 | 2       | 40      | 1           |
| Fujitsu              | LIFEBOOK T732                                    | 2013 | 2       | 40      | 1           |
| Acer                 | Aspire V5-552                                    | 2013 | 2       | 40      | 1           |
| Sony                 | VPCEC1M1E                                        | 2010 | 2       | 40      | 1           |
| Lenovo               | IdeaPad Y550P 20035                              | 2010 | 2       | 40      | 1           |
| Dell                 | Studio 1745                                      | 2009 | 2       | 40      | 1           |
| Clevo                | M740TU(N)/M760TU(N)/W7X0TUN                      | 2009 | 2       | 40      | 1           |
| ASUSTek Computer     | G72GX                                            | 2009 | 2       | 40      | 1           |
| Packard Bell         | EasyNote SL65                                    | 2008 | 2       | 40      | 1           |
| Fujitsu Siemens      | AMILO Xa 2528                                    | 2008 | 2       | 40      | 1           |
| Toshiba              | TECRA M9                                         | 2007 | 2       | 40      | 1           |
| Toshiba              | Satellite Pro A200                               | 2007 | 2       | 40      | 1           |
| Sony                 | VGN-TZ1RXN_B                                     | 2007 | 2       | 40      | 1           |
| Lenovo               | ThinkPad T14s Gen 2i                             | 2021 | 2       | 39      | 1           |
| Hewlett-Packard      | Laptop 15s-du3xxx                                | 2021 | 2       | 39      | 1           |
| Hewlett-Packard      | ZHAN 66 Pro 14 G4 Notebook PC                    | 2020 | 2       | 39      | 1           |
| HUAWEI               | BOHL-WXX9                                        | 2020 | 2       | 39      | 1           |
| MSI                  | GS65 Stealth 9SF                                 | 2019 | 2       | 39      | 1           |
| Lenovo               | 14w 81MQ000JUS                                   | 2019 | 2       | 39      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FJ_F512FJ                | 2019 | 2       | 39      | 1           |
| ASUSTek Computer     | ROG Strix G731GT_G731GT                          | 2019 | 2       | 39      | 1           |
| MSI                  | GE73VR 7RF                                       | 2018 | 2       | 39      | 1           |
| Hewlett-Packard      | 245 G4 Notebook PC                               | 2018 | 2       | 39      | 1           |
| MSI                  | GE62 2QD                                         | 2015 | 2       | 39      | 1           |
| Positivo             | W942SW_SW1                                       | 2014 | 2       | 39      | 1           |
| Panasonic            | CF53-4                                           | 2014 | 2       | 39      | 1           |
| Dell                 | Inspiron 3135                                    | 2014 | 2       | 39      | 1           |
| Lenovo               | M490s 20215                                      | 2013 | 2       | 39      | 1           |
| Acer                 | Aspire E1-422                                    | 2013 | 2       | 39      | 1           |
| Sony                 | VPCSB25FB                                        | 2012 | 2       | 39      | 1           |
| LG Electronics       | R490-K.BE55P1                                    | 2010 | 2       | 39      | 1           |
| Lenovo               | 3000 N200 0769BAG                                | 2008 | 2       | 39      | 1           |
| Hewlett-Packard      | Compaq 2710p                                     | 2008 | 2       | 39      | 1           |
| Hewlett-Packard      | Pavilion dv2600                                  | 2007 | 2       | 39      | 1           |
| Toshiba              | QOSMIO G30                                       | 2006 | 2       | 39      | 1           |
| Notebook             | NH5x_NH7x_HHx_HJx_HKx                            | 2021 | 2       | 38      | 1           |
| Lenovo               | Yoga 14sACH 2021 82MS                            | 2021 | 2       | 38      | 1           |
| SLIMBOOK             | PROX14-10                                        | 2020 | 2       | 38      | 1           |
| Hewlett-Packard      | ZHAN 66 Pro 15 G3                                | 2020 | 2       | 38      | 1           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E410MA_L410MA               | 2020 | 2       | 38      | 1           |
| MSI                  | GS65 Stealth 9SD                                 | 2019 | 2       | 38      | 1           |
| MSI                  | GL65 9SDK                                        | 2019 | 2       | 38      | 1           |
| Hewlett-Packard      | Nami                                             | 2019 | 2       | 38      | 1           |
| Gigabyte Technology  | AERO 15-SA                                       | 2019 | 2       | 38      | 1           |
| Dell                 | Latitude 7424 Rugged Extreme                     | 2019 | 2       | 38      | 1           |
| Positivo             | J14KR11                                          | 2018 | 2       | 38      | 1           |
| Hewlett-Packard      | ZBook 17 G4                                      | 2018 | 2       | 38      | 1           |
| Hewlett-Packard      | Laptop 17z-ca000                                 | 2018 | 2       | 38      | 1           |
| Gigabyte Technology  | P15FV7                                           | 2017 | 2       | 38      | 1           |
| Acer                 | Aspire E5-722                                    | 2016 | 2       | 38      | 1           |
| MSI                  | GE72 2QD                                         | 2015 | 2       | 38      | 1           |
| Durabook             | S15H                                             | 2015 | 2       | 38      | 1           |
| Notebook             | Titanium B155 MAX                                | 2014 | 2       | 38      | 1           |
| MSI                  | GE60 2PE                                         | 2014 | 2       | 38      | 1           |
| Sony                 | SVS13A25PBS                                      | 2013 | 2       | 38      | 1           |
| Toshiba              | Satellite L740                                   | 2012 | 2       | 38      | 1           |
| Samsung Electronics  | 700Z7C                                           | 2012 | 2       | 38      | 1           |
| ASUSTek Computer     | U38N                                             | 2012 | 2       | 38      | 1           |
| Sony                 | VPCF23P1E                                        | 2011 | 2       | 38      | 1           |
| Sony                 | VPCF2390X                                        | 2011 | 2       | 38      | 1           |
| Toshiba              | Satellite Pro S500                               | 2010 | 2       | 38      | 1           |
| Acer                 | TravelMate 8372                                  | 2010 | 2       | 38      | 1           |
| Acer                 | Aspire 3935                                      | 2009 | 2       | 38      | 1           |
| PC Specialist        | Fusion IV                                        | 2020 | 2       | 37      | 1           |
| MSI                  | GL75 9SCK                                        | 2020 | 2       | 37      | 1           |
| Lenovo               | IdeaPad 3 17IML05 81WC                           | 2020 | 2       | 37      | 1           |
| LG Electronics       | 17Z90N-V.AA77G                                   | 2020 | 2       | 37      | 1           |
| Hewlett-Packard      | ZBook Firefly 15 G7 Mobile Workstation           | 2020 | 2       | 37      | 1           |
| Google               | Liara                                            | 2020 | 2       | 37      | 1           |
| ASUSTek Computer     | VivoBook_ASUS Laptop E510MA_L510MA               | 2020 | 2       | 37      | 1           |
| ASUSTek Computer     | ASUS EXPERTBOOK P2451FB_P2451FB                  | 2020 | 2       | 37      | 1           |
| Medion               | S17402 MD63000                                   | 2019 | 2       | 37      | 1           |
| Lenovo               | ThinkPad P51s W10DG                              | 2019 | 2       | 37      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412FJ_X412FJ                | 2019 | 2       | 37      | 1           |
| Fujitsu              | LIFEBOOK U758                                    | 2018 | 2       | 37      | 1           |
| TUXEDO               | P65_P67RGRERA                                    | 2016 | 2       | 37      | 1           |
| Sony                 | SVF1521D1RW                                      | 2016 | 2       | 37      | 1           |
| Lenovo               | ZHAOYANG E42-80 80T9                             | 2016 | 2       | 37      | 1           |
| Lenovo               | ThinkPad L560                                    | 2016 | 2       | 37      | 1           |
| MSI                  | GP70 2QF                                         | 2015 | 2       | 37      | 1           |
| Lenovo               | E50-80 80J2                                      | 2015 | 2       | 37      | 1           |
| Lenovo               | Flex 2 Pro-15 80FL                               | 2014 | 2       | 37      | 1           |
| Acer                 | Aspire V3-572P                                   | 2014 | 2       | 37      | 1           |
| ASUSTek Computer     | N46VB                                            | 2013 | 2       | 37      | 1           |
| Sony                 | VPCSB16FG                                        | 2012 | 2       | 37      | 1           |
| Sony                 | SVE1713M1RW                                      | 2012 | 2       | 37      | 1           |
| MSI                  | GT70 0NC/GT70 0NC                                | 2012 | 2       | 37      | 1           |
| Dell                 | Latitude E5530 vPro                              | 2012 | 2       | 37      | 1           |
| Acer                 | Calpella                                         | 2012 | 2       | 37      | 1           |
| Clevo                | W150HNM/W170HN                                   | 2011 | 2       | 37      | 1           |
| MSI                  | GX720                                            | 2010 | 2       | 37      | 1           |
| Packard Bell         | SJV50MV                                          | 2009 | 2       | 37      | 1           |
| ASUSTek Computer     | UL50Vg                                           | 2009 | 2       | 37      | 1           |
| Gateway              | P-7805u                                          | 2008 | 2       | 37      | 1           |
| Toshiba              | Satellite U305                                   | 2007 | 2       | 37      | 1           |
| RO                   | HEL81C                                           | 2007 | 2       | 37      | 1           |
| Packard Bell         | EasyNote MB65                                    | 2007 | 2       | 37      | 1           |
| Acer                 | Aspire A315-57G                                  | 2020 | 2       | 36      | 1           |
| HUAWEI               | MACHR-WX9                                        | 2019 | 2       | 36      | 1           |
| ASUSTek Computer     | Zephyrus M GU502GU_GU502GU                       | 2019 | 2       | 36      | 1           |
| MSI                  | GF62 8RE                                         | 2018 | 2       | 36      | 1           |
| ASUSTek Computer     | VivoBook S14 X430UN                              | 2018 | 2       | 36      | 1           |
| Dell                 | Latitude 7414                                    | 2017 | 2       | 36      | 1           |
| MSI                  | GP62 2QE                                         | 2016 | 2       | 36      | 1           |
| MSI                  | GE62 2QC                                         | 2015 | 2       | 36      | 1           |
| Lenovo               | E31-70 80KX                                      | 2015 | 2       | 36      | 1           |
| Hewlett-Packard      | ENVY 17 Notebook PC                              | 2015 | 2       | 36      | 1           |
| Toshiba              | Satellite PRO C70-B                              | 2014 | 2       | 36      | 1           |
| Dell                 | Latitude 12 Rugged Extreme                       | 2014 | 2       | 36      | 1           |
| Sony                 | SVF14N13CXB                                      | 2013 | 2       | 36      | 1           |
| Lenovo               | ThinkPad S430                                    | 2013 | 2       | 36      | 1           |
| IGEL Technology      | M330C                                            | 2013 | 2       | 36      | 1           |
| Hewlett-Packard      | ENVY 14 Sleekbook                                | 2013 | 2       | 36      | 1           |
| Toshiba              | Satellite P875                                   | 2012 | 2       | 36      | 1           |
| Sony                 | SVE1511V1RW                                      | 2012 | 2       | 36      | 1           |
| Motion Computing     | J3500                                            | 2012 | 2       | 36      | 1           |
| Lenovo               | V580 20147                                       | 2012 | 2       | 36      | 1           |
| LG Electronics       | N450-P.BE55P1                                    | 2012 | 2       | 36      | 1           |
| Lenovo               | B575 1450A7U                                     | 2011 | 2       | 36      | 1           |
| Acer                 | Aspire 5951G                                     | 2011 | 2       | 36      | 1           |
| eMachines            | D440                                             | 2010 | 2       | 36      | 1           |
| MSI                  | CR700                                            | 2010 | 2       | 36      | 1           |
| Fujitsu Siemens      | LIFEBOOK T1010                                   | 2009 | 2       | 36      | 1           |
| ASUSTek Computer     | F50Z                                             | 2008 | 2       | 36      | 1           |
| Toshiba              | QOSMIO G20                                       | 2007 | 2       | 36      | 1           |
| Timi                 | RedmiBook Pro 15                                 | 2021 | 2       | 35      | 1           |
| MSI                  | GF75 Thin 9SCSR                                  | 2020 | 2       | 35      | 1           |
| Hewlett-Packard      | ProBook 430 G8 Notebook PC                       | 2020 | 2       | 35      | 1           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X705MAR_X705MA               | 2020 | 2       | 35      | 1           |
| Lenovo               | Yoga S730-13IWL 81J0                             | 2018 | 2       | 35      | 1           |
| Gigabyte Technology  | P57                                              | 2018 | 2       | 35      | 1           |
| ASUSTek Computer     | K501LX                                           | 2017 | 2       | 35      | 1           |
| Lenovo               | E51-80 80QB                                      | 2016 | 2       | 35      | 1           |
| Cube                 | i16-L                                            | 2016 | 2       | 35      | 1           |
| Toshiba              | Satellite C50-A-L6K                              | 2014 | 2       | 35      | 1           |
| Hewlett-Packard      | Pavilion TS 14                                   | 2014 | 2       | 35      | 1           |
| Acer                 | Aspire E5-571P                                   | 2014 | 2       | 35      | 1           |
| Sony                 | SVF1521L6EW                                      | 2013 | 2       | 35      | 1           |
| Sony                 | SVF14211CLB                                      | 2013 | 2       | 35      | 1           |
| Acer                 | V3-771                                           | 2012 | 2       | 35      | 1           |
| Lenovo               | IdeaPad Z570 10246ZG                             | 2011 | 2       | 35      | 1           |
| Dell                 | Inspiron 1090                                    | 2011 | 2       | 35      | 1           |
| Acer                 | TravelMate 5742Z                                 | 2010 | 2       | 35      | 1           |
| Acer                 | Aspire 5534                                      | 2009 | 2       | 35      | 1           |
| Toshiba              | PORTEGE R600                                     | 2008 | 2       | 35      | 1           |
| Toshiba              | Satellite M105                                   | 2006 | 2       | 35      | 1           |
| Medion               | S15450                                           | 2020 | 2       | 34      | 1           |
| Dell                 | Inspiron 7573                                    | 2018 | 2       | 34      | 1           |
| ASUSTek Computer     | UX310UAR                                         | 2018 | 2       | 34      | 1           |
| Gigabyte Technology  | Sabre 15                                         | 2017 | 2       | 34      | 1           |
| Dell                 | Latitude 5414                                    | 2017 | 2       | 34      | 1           |
| ASUSTek Computer     | X556UB                                           | 2016 | 2       | 34      | 1           |
| Toshiba              | Satellite C70-A-K2W                              | 2014 | 2       | 34      | 1           |
| Acer                 | Aspire E5-731G                                   | 2014 | 2       | 34      | 1           |
| ASUSTek Computer     | GR8                                              | 2014 | 2       | 34      | 1           |
| Toshiba              | Satellite L50-A                                  | 2013 | 2       | 34      | 1           |
| Lenovo               | IdeaPad Z410 20292                               | 2013 | 2       | 34      | 1           |
| Lenovo               | B590 62743NG                                     | 2013 | 2       | 34      | 1           |
| Lenovo               | B590 627439G                                     | 2013 | 2       | 34      | 1           |
| Toshiba              | Satellite M840-C1T                               | 2012 | 2       | 34      | 1           |
| Sony                 | SVE1712E1RB                                      | 2012 | 2       | 34      | 1           |
| ASUSTek Computer     | B400A                                            | 2012 | 2       | 34      | 1           |
| Sony                 | VPCEJ3S1R                                        | 2011 | 2       | 34      | 1           |
| Samsung Electronics  | NS310                                            | 2011 | 2       | 34      | 1           |
| Sony                 | VPCM120AL                                        | 2010 | 2       | 34      | 1           |
| eMachines            | eMG620                                           | 2008 | 2       | 34      | 1           |
| DEPO Computers       | DEPO VIP                                         | 2008 | 2       | 34      | 1           |
| Lenovo               | ThinkPad X60 Tablet                              | 2007 | 2       | 34      | 1           |
| Fujitsu Siemens      | LIFEBOOK P1610                                   | 2007 | 2       | 34      | 1           |
| ASUSTek Computer     | R1F                                              | 2006 | 2       | 34      | 1           |
| Lenovo               | Yoga Slim 7 14IIL05 82A1                         | 2020 | 2       | 33      | 1           |
| Lenovo               | V510-14IKB 80WR                                  | 2017 | 2       | 33      | 1           |
| Toshiba              | TECRA Z40-A                                      | 2015 | 2       | 33      | 1           |
| Lenovo               | Z40-70 20366                                     | 2015 | 2       | 33      | 1           |
| Lenovo               | ThinkPad S5-S531                                 | 2014 | 2       | 33      | 1           |
| Toshiba              | Satellite C870-DQK                               | 2013 | 2       | 33      | 1           |
| Sony                 | SVF1521V6EB                                      | 2013 | 2       | 33      | 1           |
| Sony                 | SVF1521N6EW                                      | 2013 | 2       | 33      | 1           |
| Sony                 | SVF1421E2EW                                      | 2013 | 2       | 33      | 1           |
| MSI                  | CX61 0OC/CX61 0OD/CX61 0OL                       | 2013 | 2       | 33      | 1           |
| Sony                 | SVE1512G1RB                                      | 2012 | 2       | 33      | 1           |
| Sony                 | VPCSB36FG                                        | 2011 | 2       | 33      | 1           |
| Sony                 | VPCEH1E1R                                        | 2011 | 2       | 33      | 1           |
| Intel                | Calpella Platform                                | 2010 | 2       | 33      | 1           |
| Samsung Electronics  | R517/R717                                        | 2009 | 2       | 33      | 1           |
| Fujitsu Siemens      | ESPRIMO Mobile V6555                             | 2009 | 2       | 33      | 1           |
| MiTAC                | Notebook PC                                      | 2007 | 2       | 33      | 1           |
| Toshiba              | Satellite M40X                                   | 2005 | 2       | 33      | 1           |
| Lenovo               | V17-IIL 82GX                                     | 2020 | 2       | 32      | 1           |
| Lenovo               | V310-14ISK 80SX                                  | 2019 | 2       | 32      | 1           |
| ASUSTek Computer     | UX330UAK                                         | 2019 | 2       | 32      | 1           |
| Hewlett-Packard      | EliteBook Folio 1020 G1                          | 2018 | 2       | 32      | 1           |
| Toshiba              | PORTEGE X30-D                                    | 2017 | 2       | 32      | 1           |
| Panasonic            | CF-54-2                                          | 2017 | 2       | 32      | 1           |
| Gigabyte Technology  | P55V5                                            | 2016 | 2       | 32      | 1           |
| ASUSTek Computer     | P2540UV                                          | 2016 | 2       | 32      | 1           |
| Lenovo               | B51-30 80LK                                      | 2015 | 2       | 32      | 1           |
| Acer                 | Aspire R3-431T                                   | 2014 | 2       | 32      | 1           |
| Acer                 | Aspire E5-511G                                   | 2014 | 2       | 32      | 1           |
| Sony                 | SVF1521A6EW                                      | 2013 | 2       | 32      | 1           |
| Toshiba              | Satellite C675                                   | 2011 | 2       | 32      | 1           |
| Sony                 | VPCEG15FB                                        | 2011 | 2       | 32      | 1           |
| ASUSTek Computer     | N43SL                                            | 2011 | 2       | 32      | 1           |
| Fujitsu Siemens      | AMILO Li 1818                                    | 2007 | 2       | 32      | 1           |
| Clevo                | M540SS                                           |      | 2       | 32      | 1           |
| Hewlett-Packard      | Laptop 14q-cs0xxx                                | 2019 | 2       | 31      | 1           |
| Fujitsu              | LIFEBOOK E544                                    | 2017 | 2       | 31      | 1           |
| ASUSTek Computer     | UX303LB                                          | 2015 | 2       | 31      | 1           |
| Hewlett-Packard      | 240 G2                                           | 2014 | 2       | 31      | 1           |
| Toshiba              | Satellite C75-A                                  | 2013 | 2       | 31      | 1           |
| Lenovo               | ThinkPad Edge E220s                              | 2013 | 2       | 31      | 1           |
| Lenovo               | B590 62743BG                                     | 2013 | 2       | 31      | 1           |
| Lenovo               | B590 62742QG                                     | 2012 | 2       | 31      | 1           |
| Fujitsu              | LIFEBOOK AH512                                   | 2012 | 2       | 31      | 1           |
| Lenovo               | V570 1066A9U                                     | 2011 | 2       | 31      | 1           |
| Compal               | PBL20                                            | 2011 | 2       | 31      | 1           |
| Samsung Electronics  | NC20/NB20                                        | 2009 | 2       | 31      | 1           |
| Dell                 | Latitude D400                                    | 2005 | 2       | 31      | 1           |
| Dell                 | Latitude D505                                    | 2004 | 2       | 31      | 1           |
| ASUSTek Computer     | UX330UAR                                         | 2017 | 2       | 30      | 1           |
| Sony                 | VPCEH25EN                                        | 2012 | 2       | 30      | 1           |
| Compal               | QAL50                                            | 2012 | 2       | 30      | 1           |
| Hewlett-Packard      | Compaq Presario F700                             | 2007 | 2       | 30      | 1           |
| Fujitsu Siemens      | AMILO M3438 Series                               | 2005 | 2       | 30      | 1           |
| Dell                 | Vostro 14-5459                                   | 2016 | 2       | 29      | 1           |
| DEXP                 | NH4BT18                                          | 2015 | 2       | 29      | 1           |
| Acer                 | Aspire V3-111P                                   | 2015 | 2       | 29      | 1           |
| Toshiba              | Satellite NB10t-A-102                            | 2014 | 2       | 29      | 1           |
| Lenovo               | ACLU12                                           | 2014 | 2       | 29      | 1           |
| Hewlett-Packard      | x360 310 G1 PC                                   | 2014 | 2       | 29      | 1           |
| ASUSTek Computer     | T300LA                                           | 2014 | 2       | 29      | 1           |
| Samsung Electronics  | 450R4E/450R5E/450R4V/450R5V                      | 2013 | 2       | 29      | 1           |
| Medion               | E6222                                            | 2011 | 2       | 29      | 1           |
| THD                  | PX1                                              | 2010 | 2       | 29      | 1           |
| Hewlett-Packard      | Presario R3200                                   | 2004 | 2       | 29      | 1           |
| Dell                 | Inspiron 8600                                    | 2004 | 2       | 29      | 1           |
| IBM                  | ThinkPad T40                                     | 2003 | 2       | 29      | 1           |
| MSI                  | Modern 14 A10RAS                                 | 2020 | 2       | 28      | 1           |
| TaNix                | Tx85                                             | 2019 | 2       | 28      | 1           |
| Hewlett-Packard      | 246 G6 Notebook PC                               | 2017 | 2       | 28      | 1           |
| Fujitsu              | LIFEBOOK E736                                    | 2016 | 2       | 28      | 1           |
| Dell                 | Inspiron 5452                                    | 2016 | 2       | 28      | 1           |
| Acer                 | Extensa 2511                                     | 2015 | 2       | 28      | 1           |
| Motion Computing     | F5te                                             | 2014 | 2       | 28      | 1           |
| Bluechip Computer    | BUSINESSline                                     | 2013 | 2       | 28      | 1           |
| Acer                 | Aspire E1-431                                    | 2013 | 2       | 28      | 1           |
| Samsung Electronics  | 900X3B/900X4B                                    | 2012 | 2       | 28      | 1           |
| Lenovo               | V570 1066AWU                                     | 2011 | 2       | 28      | 1           |
| Samsung Electronics  | P29/28/26                                        | 2005 | 2       | 28      | 1           |
| Google               | Wolf                                             | 2020 | 2       | 27      | 1           |
| Toshiba              | Satellite L55t-B                                 | 2014 | 2       | 27      | 1           |
| OEM                  | I40SI1                                           | 2009 | 2       | 27      | 1           |
| Dell                 | Inspiron 5100                                    | 2004 | 2       | 27      | 1           |
| Hewlett-Packard      | Split 13 x2 Detachable PC                        | 2018 | 2       | 26      | 1           |
| AMI                  | Intel Education Tablet                           | 2018 | 2       | 26      | 1           |
| TrekStor             | Surfbook W2                                      | 2017 | 2       | 26      | 1           |
| TELECOMITALIA        | M7x0S                                            | 2009 | 2       | 26      | 1           |
| Dell                 | Latitude D510                                    | 2005 | 2       | 26      | 1           |
| I-Life Digital Te... | ZED AIR                                          | 2016 | 2       | 25      | 1           |
| Hewlett-Packard      | x2 210                                           | 2016 | 2       | 25      | 1           |
| Winnovo              | V146                                             | 2019 | 2       | 24      | 1           |
| SHENZHEN X&F TECH... | ST106                                            | 2019 | 2       | 24      | 1           |
| Dell                 | Venue 8 Pro 5855                                 | 2018 | 2       | 24      | 1           |
| Oysters              | T104W3G                                          | 2015 | 2       | 24      | 1           |
| OEM                  | I41SI1                                           | 2010 | 2       | 24      | 1           |
| Fujitsu Siemens      | ESPRIMO Mobile V5555                             | 2008 | 2       | 24      | 1           |
| Hungaro Flotta Kft   | Navon Stark NX14 PRO 2018                        | 2018 | 2       | 23      | 1           |
| CAPTIVA              | Board                                            | 2017 | 2       | 23      | 1           |
| Linx                 | VISION004                                        | 2015 | 2       | 23      | 1           |
| Dell                 | Latitude C840                                    | 2002 | 2       | 23      | 1           |
| AZW                  | Z83 V                                            | 2019 | 2       | 22      | 1           |
| Insignia             | NS-P10W8100                                      | 2017 | 2       | 22      | 1           |
| TrekStor             | SurfTab duo W1 10.1                              | 2016 | 2       | 22      | 1           |
| AXDIA International  | FUSION WIN 12 PRO                                | 2016 | 2       | 22      | 1           |
| Packard Bell         | EasyNote MX52                                    | 2008 | 2       | 22      | 1           |
| Prestigio            | PSB116C01BFH                                     | 2017 | 2       | 21      | 1           |
| ONDA                 | OBOOK 20 PLUS                                    | 2016 | 2       | 21      | 1           |
| IBM                  | 264070A                                          |      | 2       | 21      | 1           |
| OEM                  | V40SI2                                           | 2009 | 2       | 20      | 1           |
| Hewlett-Packard      | Notebook                                         | 2015 | 490     | 48      | 2           |
| Hewlett-Packard      | Pavilion 15                                      | 2013 | 223     | 45      | 2           |
| Acer                 | Nitro AN515-54                                   | 2019 | 166     | 50      | 2           |
| Hewlett-Packard      | 15                                               | 2013 | 161     | 46      | 2           |
| Dell                 | Latitude E6410                                   | 2010 | 158     | 53      | 2           |
| Hewlett-Packard      | Laptop 15-bw0xx                                  | 2017 | 148     | 43      | 2           |
| Dell                 | XPS 15 7590                                      | 2019 | 146     | 64      | 2           |
| Lenovo               | ThinkPad X220                                    | 2011 | 144     | 43      | 2           |
| Dell                 | Inspiron N5110                                   | 2011 | 133     | 41      | 2           |
| Dell                 | Latitude E6400                                   | 2008 | 132     | 55      | 2           |
| Dell                 | Inspiron 15-3567                                 | 2016 | 130     | 42      | 2           |
| Apple                | MacBookAir7,2                                    | 2016 | 125     | 44      | 2           |
| Hewlett-Packard      | Laptop 15-bs0xx                                  | 2017 | 117     | 38      | 2           |
| Lenovo               | G50-45 80E3                                      | 2014 | 115     | 43      | 2           |
| Apple                | MacBookPro8,1                                    | 2011 | 110     | 51      | 2           |
| Hewlett-Packard      | EliteBook 8470p                                  | 2011 | 102     | 46      | 2           |
| Hewlett-Packard      | G62                                              | 2010 | 101     | 43      | 2           |
| Acer                 | Aspire V3-571G                                   | 2012 | 99      | 41      | 2           |
| Hewlett-Packard      | Laptop 15-da0xxx                                 | 2018 | 94      | 51      | 2           |
| Lenovo               | ThinkPad X250                                    | 2015 | 92      | 47      | 2           |
| Apple                | MacBookAir7,1                                    | 2016 | 85      | 35      | 2           |
| Hewlett-Packard      | EliteBook 8440p                                  | 2010 | 83      | 51      | 2           |
| Hewlett-Packard      | Pavilion 17                                      | 2013 | 82      | 50      | 2           |
| Lenovo               | ThinkPad T530                                    | 2012 | 81      | 46      | 2           |
| Acer                 | Aspire 5750G                                     | 2011 | 79      | 44      | 2           |
| Hewlett-Packard      | 2000                                             | 2011 | 78      | 48      | 2           |
| Hewlett-Packard      | EliteBook 840 G6                                 | 2019 | 77      | 53      | 2           |
| Dell                 | Latitude E7450                                   | 2014 | 77      | 44      | 2           |
| Lenovo               | ThinkPad E14 Gen 2                               | 2020 | 75      | 58      | 2           |
| Lenovo               | G580 20150                                       | 2012 | 73      | 37      | 2           |
| Dell                 | Latitude E6440                                   | 2013 | 72      | 53      | 2           |
| Dell                 | G3 3579                                          | 2018 | 72      | 47      | 2           |
| Acer                 | Aspire A515-51G                                  | 2017 | 72      | 40      | 2           |
| Dell                 | Latitude E6530                                   | 2012 | 71      | 46      | 2           |
| Hewlett-Packard      | ProBook 650 G1                                   | 2013 | 70      | 45      | 2           |
| Hewlett-Packard      | 250 G6 Notebook PC                               | 2017 | 69      | 37      | 2           |
| Dell                 | Inspiron 15 7000 Gaming                          | 2017 | 68      | 48      | 2           |
| Lenovo               | ThinkPad T460                                    | 2016 | 67      | 46      | 2           |
| Samsung Electronics  | 300E4A/300E5A/300E7A/3430EA/3530EA               | 2011 | 66      | 35      | 2           |
| Hewlett-Packard      | Pavilion dv5                                     | 2008 | 65      | 51      | 2           |
| Lenovo               | ThinkPad T450s                                   | 2015 | 65      | 45      | 2           |
| Hewlett-Packard      | EliteBook 840 G2                                 | 2015 | 65      | 40      | 2           |
| Lenovo               | ThinkPad T520                                    | 2011 | 64      | 43      | 2           |
| Dell                 | Latitude E7240                                   | 2013 | 61      | 43      | 2           |
| Hewlett-Packard      | Presario CQ57                                    | 2011 | 59      | 43      | 2           |
| Lenovo               | ThinkPad T440                                    | 2013 | 58      | 47      | 2           |
| Lenovo               | Z50-70 20354                                     | 2014 | 58      | 41      | 2           |
| Toshiba              | Satellite A300                                   | 2008 | 56      | 43      | 2           |
| Samsung Electronics  | 355V4C/356V4C/3445VC/3545VC                      | 2013 | 55      | 46      | 2           |
| Lenovo               | Y520-15IKBN 80WK                                 | 2017 | 55      | 41      | 2           |
| Positivo             | MOBILE                                           | 2009 | 55      | 41      | 2           |
| Acer                 | Extensa 5620                                     | 2007 | 55      | 41      | 2           |
| Acer                 | Aspire E1-571G                                   | 2012 | 55      | 39      | 2           |
| Lenovo               | ThinkPad E490                                    | 2018 | 54      | 48      | 2           |
| Acer                 | Aspire V3-771                                    | 2012 | 54      | 40      | 2           |
| Acer                 | Aspire 5738                                      | 2009 | 54      | 39      | 2           |
| Hewlett-Packard      | EliteBook 2560p                                  | 2011 | 53      | 43      | 2           |
| Lenovo               | ThinkPad T510                                    | 2009 | 52      | 56      | 2           |
| Lenovo               | IdeaPad 330-15AST 81D6                           | 2018 | 52      | 50      | 2           |
| Positivo             | Mobile                                           | 2007 | 52      | 40      | 2           |
| Lenovo               | ThinkPad E480                                    | 2017 | 51      | 53      | 2           |
| Dell                 | Latitude E6500                                   | 2008 | 51      | 48      | 2           |
| Dell                 | Inspiron 5559                                    | 2015 | 51      | 43      | 2           |
| Dell                 | Precision 5530                                   | 2018 | 50      | 61      | 2           |
| Hewlett-Packard      | EliteBook Folio 9470m                            | 2012 | 50      | 45      | 2           |
| Lenovo               | IdeaPad 100-15IBD 80QQ                           | 2015 | 50      | 40      | 2           |
| Lenovo               | ThinkPad W520                                    | 2011 | 49      | 51      | 2           |
| HUAWEI               | BOHK-WAX9X                                       | 2019 | 49      | 47      | 2           |
| Lenovo               | ThinkPad T470                                    | 2017 | 49      | 47      | 2           |
| Hewlett-Packard      | Pavilion dv4                                     | 2008 | 49      | 46      | 2           |
| Dell                 | Inspiron 5558                                    | 2015 | 49      | 39      | 2           |
| Hewlett-Packard      | EliteBook 6930p                                  | 2008 | 48      | 48      | 2           |
| Apple                | MacBookPro5,5                                    | 2009 | 48      | 41      | 2           |
| Lenovo               | G510 20238                                       | 2013 | 47      | 38      | 2           |
| Samsung Electronics  | 350V5C/351V5C/3540VC/3440VC                      | 2013 | 47      | 37      | 2           |
| Hewlett-Packard      | EliteBook 840 G5                                 | 2018 | 46      | 57      | 2           |
| Lenovo               | IdeaPad L340-15IRH Gaming 81LK                   | 2019 | 46      | 44      | 2           |
| Dell                 | Latitude E5430 non-vPro                          | 2012 | 46      | 43      | 2           |
| Lenovo               | G505 20240                                       | 2013 | 46      | 40      | 2           |
| Acer                 | Aspire A315-42                                   | 2019 | 45      | 48      | 2           |
| Hewlett-Packard      | 14                                               | 2013 | 45      | 40      | 2           |
| Acer                 | Aspire E5-573G                                   | 2015 | 45      | 39      | 2           |
| Dell                 | Inspiron 3421                                    | 2012 | 45      | 34      | 2           |
| Lenovo               | B570e HuronRiver Platform                        | 2011 | 45      | 34      | 2           |
| Dell                 | XPS 17 9700                                      | 2020 | 44      | 64      | 2           |
| Dell                 | Inspiron 7559                                    | 2015 | 44      | 43      | 2           |
| Acer                 | Swift SF314-42                                   | 2020 | 43      | 53      | 2           |
| Dell                 | G5 5587                                          | 2018 | 42      | 53      | 2           |
| Dell                 | Inspiron 3543                                    | 2014 | 42      | 41      | 2           |
| Lenovo               | G710 20252                                       | 2013 | 42      | 41      | 2           |
| Dell                 | Inspiron 15-3552                                 | 2015 | 42      | 31      | 2           |
| Lenovo               | ThinkPad T580                                    | 2018 | 41      | 56      | 2           |
| Dell                 | XPS 15 9550                                      | 2015 | 41      | 55      | 2           |
| Lenovo               | V330-15IKB 81AX                                  | 2017 | 41      | 44      | 2           |
| ASUSTek Computer     | X550CL                                           | 2013 | 41      | 39      | 2           |
| Lenovo               | B590 20208                                       | 2012 | 41      | 38      | 2           |
| Dell                 | Latitude E4300                                   | 2008 | 40      | 45      | 2           |
| Apple                | MacBookPro11,1                                   | 2014 | 40      | 44      | 2           |
| Hewlett-Packard      | EliteBook 8560p                                  | 2011 | 40      | 44      | 2           |
| Dell                 | XPS L502X                                        | 2011 | 40      | 44      | 2           |
| Lenovo               | ThinkPad X260                                    | 2016 | 40      | 42      | 2           |
| Dell                 | Latitude E5440                                   | 2013 | 40      | 41      | 2           |
| Dell                 | Latitude E4310                                   | 2010 | 39      | 50      | 2           |
| Lenovo               | ThinkPad X200                                    | 2008 | 39      | 45      | 2           |
| Hewlett-Packard      | ProBook 450 G6                                   | 2018 | 39      | 44      | 2           |
| Dell                 | Latitude E7250                                   | 2015 | 39      | 42      | 2           |
| Lenovo               | V580c 20160                                      | 2012 | 39      | 41      | 2           |
| Acer                 | Aspire 7750G                                     | 2011 | 39      | 39      | 2           |
| Dell                 | Inspiron 5520                                    | 2012 | 39      | 37      | 2           |
| Lenovo               | ThinkPad E580                                    | 2018 | 38      | 52      | 2           |
| Dell                 | Latitude 7480                                    | 2017 | 38      | 50      | 2           |
| Hewlett-Packard      | EliteBook 820 G1                                 | 2014 | 38      | 43      | 2           |
| Lenovo               | IdeaPad Z570 HuronRiver Platform                 | 2011 | 38      | 36      | 2           |
| Lenovo               | ThinkPad E590                                    | 2019 | 37      | 54      | 2           |
| ASUSTek Computer     | K53SC                                            | 2011 | 37      | 41      | 2           |
| Packard Bell         | EasyNote TS11HR                                  | 2011 | 37      | 40      | 2           |
| Acer                 | Nitro AN515-44                                   | 2020 | 36      | 47      | 2           |
| Hewlett-Packard      | ENVY Notebook                                    | 2015 | 36      | 47      | 2           |
| Lenovo               | IdeaPad S145-15AST 81N3                          | 2019 | 36      | 40      | 2           |
| System76             | Oryx Pro                                         | 2016 | 35      | 52      | 2           |
| Hewlett-Packard      | Pavilion dv6000                                  | 2006 | 35      | 43      | 2           |
| Hewlett-Packard      | ProBook 640 G1                                   | 2013 | 35      | 39      | 2           |
| Hewlett-Packard      | ProBook 6550b                                    | 2010 | 34      | 48      | 2           |
| Dell                 | Latitude 5490                                    | 2018 | 34      | 46      | 2           |
| Acer                 | Aspire A315-41                                   | 2018 | 34      | 45      | 2           |
| Dell                 | Latitude E7270                                   | 2016 | 34      | 40      | 2           |
| Hewlett-Packard      | Compaq 6710b                                     | 2007 | 34      | 40      | 2           |
| Hewlett-Packard      | ProBook 450 G4                                   | 2016 | 34      | 39      | 2           |
| Lenovo               | ThinkPad X1 Carbon 4th                           | 2016 | 33      | 52      | 2           |
| Lenovo               | IdeaPad Z580                                     | 2012 | 33      | 49      | 2           |
| Hewlett-Packard      | ZBook 15                                         | 2013 | 33      | 48      | 2           |
| HUAWEI               | KLVL-WXX9                                        | 2020 | 33      | 47      | 2           |
| Lenovo               | ThinkPad Edge E540                               | 2013 | 33      | 45      | 2           |
| Hewlett-Packard      | EliteBook 8570p                                  | 2012 | 33      | 45      | 2           |
| Dell                 | Latitude D620                                    | 2006 | 33      | 45      | 2           |
| Dell                 | Latitude E5530 non-vPro                          | 2012 | 33      | 44      | 2           |
| Lenovo               | ThinkPad X220 Tablet                             | 2011 | 33      | 44      | 2           |
| Hewlett-Packard      | Pavilion dv6500                                  | 2007 | 33      | 42      | 2           |
| Lenovo               | IdeaPad 520-15IKB 81BF                           | 2017 | 33      | 38      | 2           |
| ASUSTek Computer     | K52JT                                            | 2010 | 32      | 64      | 2           |
| Dell                 | G7 7588                                          | 2018 | 32      | 55      | 2           |
| Apple                | MacBookPro6,2                                    | 2010 | 32      | 48      | 2           |
| Lenovo               | ThinkPad T470s                                   | 2017 | 32      | 47      | 2           |
| ASUSTek Computer     | X550CA                                           | 2013 | 32      | 37      | 2           |
| Hewlett-Packard      | 250 G3                                           | 2014 | 32      | 35      | 2           |
| Hewlett-Packard      | 650                                              | 2012 | 32      | 32      | 2           |
| Acer                 | Aspire A515-43                                   | 2019 | 31      | 49      | 2           |
| Lenovo               | IdeaPad 330S-15ARR 81FB                          | 2018 | 31      | 47      | 2           |
| Hewlett-Packard      | ENVY m6                                          | 2012 | 31      | 42      | 2           |
| Lenovo               | B50-30 20382                                     | 2014 | 31      | 36      | 2           |
| Lenovo               | IdeaPad S145-15IWL 81MV                          | 2019 | 31      | 35      | 2           |
| ASUSTek Computer     | K54HR                                            | 2011 | 31      | 35      | 2           |
| Samsung Electronics  | 300E5EV/300E4EV/270E5EV/270E4EV/2470EV/2470EE    | 2014 | 31      | 32      | 2           |
| Hewlett-Packard      | Pavilion Gaming Laptop 15-ec0xxx                 | 2019 | 30      | 50      | 2           |
| Hewlett-Packard      | EliteBook 8570w                                  | 2012 | 30      | 48      | 2           |
| Dell                 | Latitude E5500                                   | 2008 | 30      | 48      | 2           |
| Acer                 | Aspire E5-575                                    | 2016 | 30      | 42      | 2           |
| Samsung Electronics  | 305E4A/305E5A/305E7A                             | 2011 | 29      | 50      | 2           |
| Hewlett-Packard      | Compaq 6730b                                     | 2008 | 29      | 43      | 2           |
| Acer                 | Extensa 5630                                     | 2008 | 29      | 43      | 2           |
| Hewlett-Packard      | ProBook 450 G2                                   | 2014 | 29      | 42      | 2           |
| Lenovo               | IdeaPad 320-15IAP 80XR                           | 2017 | 29      | 40      | 2           |
| Lenovo               | ThinkPad Edge E530                               | 2012 | 29      | 40      | 2           |
| Toshiba              | Satellite A100                                   | 2006 | 29      | 38      | 2           |
| Toshiba              | Satellite C55-B                                  | 2014 | 29      | 33      | 2           |
| Apple                | MacBookPro11,3                                   | 2016 | 28      | 51      | 2           |
| Hewlett-Packard      | Pavilion Gaming Laptop 15-dk0xxx                 | 2019 | 28      | 46      | 2           |
| ASUSTek Computer     | GL553VD                                          | 2017 | 28      | 45      | 2           |
| ASUSTek Computer     | N550JV                                           | 2013 | 28      | 45      | 2           |
| Hewlett-Packard      | Laptop 14-dk0xxx                                 | 2019 | 28      | 44      | 2           |
| Hewlett-Packard      | EliteBook 820 G2                                 | 2015 | 28      | 41      | 2           |
| ASUSTek Computer     | X550LD                                           | 2013 | 28      | 41      | 2           |
| Hewlett-Packard      | Pavilion 14                                      | 2013 | 28      | 40      | 2           |
| ASUSTek Computer     | K56CB                                            | 2013 | 28      | 39      | 2           |
| Lenovo               | IdeaPad Z510 20287                               | 2013 | 28      | 38      | 2           |
| Hewlett-Packard      | Laptop 15-ra0xx                                  | 2018 | 28      | 32      | 2           |
| Acer                 | Swift SF114-32                                   | 2018 | 28      | 32      | 2           |
| Acer                 | Nitro AN515-42                                   | 2018 | 27      | 46      | 2           |
| Dell                 | Latitude D830                                    | 2007 | 27      | 45      | 2           |
| Lenovo               | ThinkPad X201 Tablet                             | 2010 | 27      | 43      | 2           |
| Dell                 | Vostro 5470                                      | 2014 | 27      | 41      | 2           |
| Lenovo               | ThinkPad 13 2nd Gen                              | 2017 | 27      | 39      | 2           |
| Hewlett-Packard      | ProBook 430 G2                                   | 2014 | 27      | 38      | 2           |
| Acer                 | Aspire 5732Z                                     | 2009 | 27      | 37      | 2           |
| Lenovo               | ThinkPad W510                                    | 2010 | 26      | 68      | 2           |
| Dell                 | Latitude 5410                                    | 2020 | 26      | 62      | 2           |
| Lenovo               | ThinkPad T470 W10DG                              | 2017 | 26      | 45      | 2           |
| Hewlett-Packard      | EliteBook 8560w                                  | 2011 | 26      | 43      | 2           |
| Lenovo               | IdeaPad 320-15IKB 80YH                           | 2017 | 26      | 36      | 2           |
| Hewlett-Packard      | EliteBook 8540p                                  | 2010 | 25      | 61      | 2           |
| Hewlett-Packard      | EliteBook 845 G7 Notebook PC                     | 2020 | 25      | 55      | 2           |
| Lenovo               | ThinkPad P51                                     | 2017 | 25      | 54      | 2           |
| Lenovo               | IdeaPad S340-14API 81NB                          | 2019 | 25      | 53      | 2           |
| Hewlett-Packard      | Compaq 6910p                                     | 2007 | 25      | 44      | 2           |
| Hewlett-Packard      | Laptop 15s-eq0xxx                                | 2020 | 25      | 43      | 2           |
| Dell                 | Inspiron 7560                                    | 2016 | 25      | 43      | 2           |
| Apple                | MacBookPro11,5                                   | 2017 | 25      | 41      | 2           |
| Hewlett-Packard      | ProBook 4740s                                    | 2012 | 25      | 41      | 2           |
| Lenovo               | IdeaPad Y700-15ISK 80NV                          | 2015 | 25      | 36      | 2           |
| Samsung Electronics  | 350V5C/350V5X/350V4C/350V4X/351V5C/351V5X/351... | 2012 | 25      | 35      | 2           |
| Dell                 | Precision 5510                                   | 2016 | 24      | 60      | 2           |
| Dell                 | G3 3779                                          | 2018 | 24      | 49      | 2           |
| HUAWEI               | HN-WX9X                                          | 2019 | 24      | 48      | 2           |
| Hewlett-Packard      | EliteBook 2740p                                  | 2010 | 24      | 46      | 2           |
| Lenovo               | ThinkPad X1 Carbon 2nd                           | 2014 | 24      | 41      | 2           |
| ASUSTek Computer     | X55U                                             | 2012 | 24      | 41      | 2           |
| Quanta               | TWH                                              | 2010 | 24      | 40      | 2           |
| Lenovo               | ThinkBook 15-IIL 20SM                            | 2019 | 24      | 39      | 2           |
| Hewlett-Packard      | Laptop 15s-fq2xxx                                | 2020 | 24      | 38      | 2           |
| Acer                 | Aspire E5-771G                                   | 2014 | 24      | 38      | 2           |
| Dell                 | Inspiron 3520                                    | 2012 | 24      | 34      | 2           |
| Dell                 | Precision 7530                                   | 2018 | 23      | 68      | 2           |
| Dell                 | Precision 7510                                   | 2016 | 23      | 50      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509DA_M509DA                | 2019 | 23      | 47      | 2           |
| ASUSTek Computer     | K52Jc                                            | 2010 | 23      | 44      | 2           |
| Dell                 | Latitude E5550                                   | 2015 | 23      | 39      | 2           |
| Digibras             | NH4CU03                                          | 2013 | 23      | 35      | 2           |
| Dell                 | Latitude 5401                                    | 2019 | 22      | 55      | 2           |
| Hewlett-Packard      | ProBook 6450b                                    | 2010 | 22      | 45      | 2           |
| Hewlett-Packard      | Laptop 14-cm0xxx                                 | 2018 | 22      | 43      | 2           |
| Acer                 | Aspire A515-52G                                  | 2018 | 22      | 42      | 2           |
| Dell                 | Vostro 5568                                      | 2016 | 22      | 40      | 2           |
| Lenovo               | B50-70 20384                                     | 2014 | 22      | 37      | 2           |
| Dell                 | Latitude E6220                                   | 2011 | 22      | 37      | 2           |
| Lenovo               | G770 20089                                       | 2011 | 22      | 36      | 2           |
| Lenovo               | Legion 5 15ARH05H 82B1                           | 2020 | 21      | 53      | 2           |
| Hewlett-Packard      | OMEN Laptop 15-en0xxx                            | 2020 | 21      | 52      | 2           |
| Hewlett-Packard      | ProBook 440 G5                                   | 2017 | 21      | 47      | 2           |
| Dell                 | Latitude 5500                                    | 2019 | 21      | 46      | 2           |
| Acer                 | Aspire V3-772                                    | 2013 | 21      | 45      | 2           |
| Dell                 | Inspiron 7537                                    | 2013 | 21      | 39      | 2           |
| Hewlett-Packard      | ProBook 640 G2                                   | 2016 | 21      | 36      | 2           |
| Lenovo               | IdeaPad 530S-14IKB 81EU                          | 2018 | 21      | 35      | 2           |
| Samsung Electronics  | NC10                                             | 2008 | 21      | 34      | 2           |
| Dell                 | Precision M4700                                  | 2012 | 20      | 48      | 2           |
| TUXEDO               | Pulse 15 Gen1                                    | 2020 | 20      | 43      | 2           |
| Hewlett-Packard      | Laptop 17-ak0xx                                  | 2017 | 20      | 43      | 2           |
| Lenovo               | IdeaPad Z500 20202                               | 2012 | 20      | 42      | 2           |
| Hewlett-Packard      | EliteBook Folio 9480m                            | 2014 | 20      | 40      | 2           |
| Hewlett-Packard      | EliteBook 2760p                                  | 2011 | 20      | 39      | 2           |
| Lenovo               | G780 20138                                       | 2012 | 20      | 35      | 2           |
| Toshiba              | Satellite C55-A                                  | 2013 | 20      | 34      | 2           |
| Hewlett-Packard      | EliteBook 8540w                                  | 2010 | 19      | 65      | 2           |
| Hewlett-Packard      | EliteBook 830 G6                                 | 2019 | 19      | 54      | 2           |
| Lenovo               | ThinkBook 15-IML 20RW                            | 2019 | 19      | 47      | 2           |
| Dell                 | Vostro 5481                                      | 2018 | 19      | 44      | 2           |
| Hewlett-Packard      | ZBook 15 G3                                      | 2016 | 19      | 44      | 2           |
| Dell                 | Inspiron 5480                                    | 2018 | 19      | 43      | 2           |
| Apple                | MacBookPro11,2                                   | 2016 | 19      | 43      | 2           |
| Dell                 | Precision M6600                                  | 2011 | 19      | 43      | 2           |
| Notebook             | W65_67SF                                         | 2014 | 19      | 40      | 2           |
| Acer                 | Aspire V5-571G                                   | 2012 | 19      | 40      | 2           |
| ASUSTek Computer     | S400CA                                           | 2012 | 19      | 37      | 2           |
| Dell                 | Inspiron 5521                                    | 2012 | 19      | 35      | 2           |
| ASUSTek Computer     | X200CA                                           | 2013 | 19      | 33      | 2           |
| Dell                 | Precision 5550                                   | 2020 | 18      | 61      | 2           |
| Dell                 | Precision 7520                                   | 2017 | 18      | 57      | 2           |
| Apple                | MacBookPro16,1                                   | 2020 | 18      | 56      | 2           |
| Hewlett-Packard      | ZBook Studio G5                                  | 2018 | 18      | 56      | 2           |
| Acer                 | Aspire A515-44                                   | 2020 | 18      | 53      | 2           |
| ASUSTek Computer     | ROG Zephyrus G14 GA401QM_GA401QM                 | 2021 | 18      | 52      | 2           |
| Lenovo               | IdeaPad S540-14API 81NH                          | 2019 | 18      | 48      | 2           |
| Hewlett-Packard      | Pavilion Laptop 15-eg0xxx                        | 2020 | 18      | 45      | 2           |
| Hewlett-Packard      | EliteBook 850 G1                                 | 2014 | 18      | 44      | 2           |
| Lenovo               | G550 2958                                        | 2009 | 18      | 42      | 2           |
| Lenovo               | ThinkPad L420                                    | 2011 | 18      | 41      | 2           |
| Acer                 | Swift SF314-54                                   | 2018 | 18      | 40      | 2           |
| Acer                 | Extensa 5230                                     | 2008 | 18      | 40      | 2           |
| ASUSTek Computer     | X556URK                                          | 2016 | 18      | 39      | 2           |
| Dell                 | Vostro 3560                                      | 2012 | 18      | 39      | 2           |
| ASUSTek Computer     | X555LN                                           | 2014 | 18      | 38      | 2           |
| ASUSTek Computer     | X555LF                                           | 2015 | 18      | 37      | 2           |
| ASUSTek Computer     | 1215N                                            | 2010 | 18      | 37      | 2           |
| Hewlett-Packard      | Pavilion Sleekbook 15 PC                         | 2012 | 18      | 36      | 2           |
| Lenovo               | ThinkPad L460                                    | 2016 | 18      | 35      | 2           |
| Dell                 | Precision 7540                                   | 2019 | 17      | 64      | 2           |
| Dell                 | Precision M4500                                  | 2010 | 17      | 61      | 2           |
| Acer                 | Swift SF314-41                                   | 2019 | 17      | 48      | 2           |
| Alienware            | 17                                               | 2013 | 17      | 48      | 2           |
| Acer                 | Aspire ES1-533                                   | 2016 | 17      | 46      | 2           |
| Acer                 | Aspire A715-71G                                  | 2017 | 17      | 45      | 2           |
| Dell                 | Vostro 1015                                      | 2009 | 17      | 45      | 2           |
| Lenovo               | IdeaPad L340-17IRH Gaming 81LL                   | 2019 | 17      | 44      | 2           |
| Acer                 | Aspire A515-56                                   | 2020 | 17      | 43      | 2           |
| ASUSTek Computer     | X550LC                                           | 2013 | 17      | 42      | 2           |
| ASUSTek Computer     | UX430UAR                                         | 2017 | 17      | 41      | 2           |
| ASUSTek Computer     | X541UJ                                           | 2016 | 17      | 41      | 2           |
| Lenovo               | ThinkPad E570                                    | 2017 | 17      | 40      | 2           |
| Hewlett-Packard      | EliteBook 850 G2                                 | 2015 | 17      | 38      | 2           |
| ASUSTek Computer     | N73SV                                            | 2011 | 17      | 37      | 2           |
| Lenovo               | ThinkPad E520                                    | 2011 | 17      | 36      | 2           |
| Lenovo               | IdeaPad 320-15IKB 81BG                           | 2017 | 17      | 34      | 2           |
| Acer                 | Aspire E5-511                                    | 2014 | 17      | 29      | 2           |
| Dell                 | Precision 3551                                   | 2020 | 16      | 58      | 2           |
| Dell                 | Latitude 5491                                    | 2018 | 16      | 57      | 2           |
| Dell                 | Latitude 7410                                    | 2020 | 16      | 55      | 2           |
| Lenovo               | ThinkPad W500                                    | 2009 | 16      | 52      | 2           |
| Hewlett-Packard      | ProBook 470 G5                                   | 2018 | 16      | 49      | 2           |
| Lenovo               | IdeaPad 5 15ITL05 82FG                           | 2020 | 16      | 48      | 2           |
| Dell                 | XPS M1330                                        | 2008 | 16      | 48      | 2           |
| Hewlett-Packard      | Compaq 8510p                                     | 2007 | 16      | 48      | 2           |
| ASUSTek Computer     | GL752VW                                          | 2015 | 16      | 44      | 2           |
| Lenovo               | ThinkPad Edge E431                               | 2013 | 16      | 44      | 2           |
| Apple                | MacBookPro5,1                                    | 2012 | 16      | 44      | 2           |
| ASUSTek Computer     | GL553VE                                          | 2017 | 16      | 41      | 2           |
| Dell                 | Inspiron 5490                                    | 2019 | 16      | 40      | 2           |
| Lenovo               | IdeaPad 520-15IKB 80YL                           | 2017 | 16      | 39      | 2           |
| Dell                 | Inspiron 7472                                    | 2018 | 16      | 38      | 2           |
| Lenovo               | Z710 20250                                       | 2013 | 16      | 38      | 2           |
| Clevo                | W150ER                                           | 2012 | 16      | 38      | 2           |
| Dell                 | Precision M6500                                  | 2010 | 15      | 72      | 2           |
| Lenovo               | Legion 5 Pro 16ACH6H 82JQ                        | 2021 | 15      | 63      | 2           |
| Lenovo               | ThinkPad P1                                      | 2018 | 15      | 62      | 2           |
| Hewlett-Packard      | EliteBook 840 G7 Notebook PC                     | 2020 | 15      | 54      | 2           |
| Hewlett-Packard      | EliteBook 855 G7 Notebook PC                     | 2020 | 15      | 50      | 2           |
| Lenovo               | ThinkPad E14 Gen 3                               | 2021 | 15      | 49      | 2           |
| Dell                 | Latitude 7300                                    | 2019 | 15      | 49      | 2           |
| Hewlett-Packard      | 255 G4                                           | 2015 | 15      | 47      | 2           |
| Hewlett-Packard      | ProBook 430 G6                                   | 2019 | 15      | 46      | 2           |
| Hewlett-Packard      | ENVY Laptop 13-aq0xxx                            | 2019 | 15      | 43      | 2           |
| Acer                 | Aspire A315-55G                                  | 2019 | 15      | 41      | 2           |
| ASUSTek Computer     | X550JX                                           | 2015 | 15      | 40      | 2           |
| Hewlett-Packard      | ZBook 17                                         | 2014 | 15      | 40      | 2           |
| Acer                 | Aspire A515-54                                   | 2019 | 15      | 36      | 2           |
| Dell                 | Inspiron 5737                                    | 2013 | 15      | 36      | 2           |
| ASUSTek Computer     | X55VD                                            | 2012 | 15      | 36      | 2           |
| Acer                 | Swift SF315-52                                   | 2018 | 15      | 34      | 2           |
| Lenovo               | ThinkPad T470p                                   | 2017 | 14      | 51      | 2           |
| ASUSTek Computer     | GL552VW                                          | 2015 | 14      | 49      | 2           |
| HUAWEI               | HLYL-WXX9                                        | 2020 | 14      | 48      | 2           |
| Hewlett-Packard      | Laptop 14-dk1xxx                                 | 2019 | 14      | 48      | 2           |
| Dell                 | Vostro 1520                                      | 2009 | 14      | 47      | 2           |
| Lenovo               | ThinkPad X1 Extreme Gen 3                        | 2020 | 14      | 45      | 2           |
| Lenovo               | V155-15API 81V5                                  | 2019 | 14      | 45      | 2           |
| Hewlett-Packard      | ProBook 455 G1                                   | 2013 | 14      | 45      | 2           |
| Lenovo               | ThinkPad L470                                    | 2017 | 14      | 44      | 2           |
| Apple                | MacBookPro5,3                                    | 2009 | 14      | 43      | 2           |
| Infomash             | RoverBook                                        | 2005 | 14      | 43      | 2           |
| Dell                 | Vostro 1510                                      | 2008 | 14      | 42      | 2           |
| Acer                 | Aspire 8930                                      | 2008 | 14      | 42      | 2           |
| Acer                 | Aspire 5930                                      | 2008 | 14      | 41      | 2           |
| Lenovo               | V330-14IKB 81B0                                  | 2017 | 14      | 40      | 2           |
| Hewlett-Packard      | Compaq nc6320                                    | 2006 | 14      | 40      | 2           |
| Lenovo               | IdeaPad 330-14AST 81D5                           | 2018 | 14      | 39      | 2           |
| Hewlett-Packard      | ENVY dv6                                         | 2012 | 14      | 38      | 2           |
| Hewlett-Packard      | EliteBook Folio 1040 G1                          | 2016 | 14      | 37      | 2           |
| Fujitsu              | LIFEBOOK E752                                    | 2012 | 14      | 36      | 2           |
| Hewlett-Packard      | ProBook 4535s                                    | 2011 | 13      | 52      | 2           |
| Lenovo               | IdeaPad L340-17API 81LY                          | 2019 | 13      | 48      | 2           |
| Hewlett-Packard      | ProBook 4545s                                    | 2012 | 13      | 46      | 2           |
| Lenovo               | Y720-15IKB 80VR                                  | 2017 | 13      | 44      | 2           |
| Notebook             | W65_67SJ                                         | 2014 | 13      | 44      | 2           |
| Hewlett-Packard      | EliteBook 2530p                                  | 2008 | 13      | 43      | 2           |
| Lenovo               | ThinkPad Edge E430                               | 2012 | 13      | 40      | 2           |
| Hewlett-Packard      | Compaq 6530b                                     | 2008 | 13      | 40      | 2           |
| Lenovo               | ThinkPad L450                                    | 2015 | 13      | 39      | 2           |
| Apple                | MacBookPro10,2                                   | 2018 | 13      | 38      | 2           |
| Acer                 | TravelMate P259-MG                               | 2017 | 13      | 37      | 2           |
| ASUSTek Computer     | N55SF                                            | 2011 | 13      | 37      | 2           |
| Samsung Electronics  | 550XCJ/550XCR                                    | 2020 | 13      | 36      | 2           |
| Acer                 | Aspire V5-573G                                   | 2013 | 13      | 36      | 2           |
| Hewlett-Packard      | EliteBook 745 G6                                 | 2019 | 12      | 66      | 2           |
| Lenovo               | Legion 5 15ACH6H 82JU                            | 2021 | 12      | 57      | 2           |
| Lenovo               | ThinkPad A485                                    | 2019 | 12      | 57      | 2           |
| Hewlett-Packard      | ProBook 455R G6                                  | 2019 | 12      | 55      | 2           |
| Dell                 | Latitude 5511                                    | 2020 | 12      | 54      | 2           |
| Hewlett-Packard      | EliteBook 8760w                                  | 2011 | 12      | 52      | 2           |
| Hewlett-Packard      | Pavilion Gaming Laptop 15-dk1xxx                 | 2020 | 12      | 48      | 2           |
| Fujitsu              | LIFEBOOK E780                                    | 2010 | 12      | 48      | 2           |
| Hewlett-Packard      | Pavilion dv9000                                  | 2006 | 12      | 47      | 2           |
| Acer                 | Aspire E5-774G                                   | 2016 | 12      | 46      | 2           |
| Samsung Electronics  | 305V4A/305V5A/3415VA                             | 2012 | 12      | 45      | 2           |
| Dell                 | XPS M1530                                        | 2008 | 12      | 45      | 2           |
| ASUSTek Computer     | K53BR                                            | 2011 | 12      | 43      | 2           |
| Lenovo               | ThinkPad Edge E440                               | 2014 | 12      | 42      | 2           |
| Acer                 | Aspire 5050                                      | 2006 | 12      | 42      | 2           |
| Lenovo               | IdeaPad 5 15IIL05 81YK                           | 2020 | 12      | 41      | 2           |
| Hewlett-Packard      | Pavilion dv2700                                  | 2007 | 12      | 41      | 2           |
| Fujitsu              | LIFEBOOK E751                                    | 2011 | 12      | 40      | 2           |
| Timi                 | TM1703                                           | 2018 | 12      | 38      | 2           |
| Dell                 | Vostro 3478                                      | 2018 | 12      | 38      | 2           |
| Hewlett-Packard      | Laptop 15-rb0xx                                  | 2017 | 12      | 38      | 2           |
| Lenovo               | ThinkPad T560                                    | 2016 | 12      | 38      | 2           |
| ASUSTek Computer     | X550LA                                           | 2013 | 12      | 38      | 2           |
| Lenovo               | Yoga S740-14IIL 81RS                             | 2019 | 12      | 37      | 2           |
| Acer                 | Swift SF315-52G                                  | 2018 | 12      | 37      | 2           |
| ASUSTek Computer     | N56VB                                            | 2013 | 12      | 36      | 2           |
| ASUSTek Computer     | X75VC                                            | 2013 | 12      | 35      | 2           |
| Samsung Electronics  | 700Z3A/700Z4A/700Z5A/700Z5B                      | 2012 | 12      | 33      | 2           |
| Toshiba              | Satellite P55W-C                                 | 2015 | 12      | 32      | 2           |
| Clevo                | M7x0S                                            | 2009 | 12      | 28      | 2           |
| ASUSTek Computer     | ROG Zephyrus G14 GA401IU_GA401IU                 | 2020 | 11      | 59      | 2           |
| MSI                  | Modern 14 B4MW                                   | 2020 | 11      | 45      | 2           |
| Hewlett-Packard      | Laptop 14-dq1xxx                                 | 2019 | 11      | 45      | 2           |
| Apple                | MacBookPro6,1                                    | 2015 | 11      | 45      | 2           |
| Dell                 | Vostro 3350                                      | 2011 | 11      | 45      | 2           |
| Lenovo               | ThinkPad L580                                    | 2018 | 11      | 44      | 2           |
| Acer                 | Swift SF315-41                                   | 2018 | 11      | 44      | 2           |
| Acer                 | Aspire A715-72G                                  | 2018 | 11      | 44      | 2           |
| ASUSTek Computer     | X550DP                                           | 2013 | 11      | 44      | 2           |
| Hewlett-Packard      | Pavilion m6                                      | 2012 | 11      | 44      | 2           |
| Lenovo               | ThinkPad X140e                                   | 2014 | 11      | 43      | 2           |
| ASUSTek Computer     | X550LB                                           | 2013 | 11      | 43      | 2           |
| Alienware            | M17xR4                                           | 2012 | 11      | 43      | 2           |
| Lenovo               | IdeaPad Y570 20091                               | 2011 | 11      | 43      | 2           |
| ASUSTek Computer     | Strix 15 GL503GE                                 | 2018 | 11      | 42      | 2           |
| Toshiba              | Satellite L775D                                  | 2011 | 11      | 42      | 2           |
| HUAWEI               | HVY-WXX9                                         | 2020 | 11      | 41      | 2           |
| Dell                 | Vostro 3590                                      | 2019 | 11      | 41      | 2           |
| Acer                 | Aspire 5739G                                     | 2009 | 11      | 40      | 2           |
| Apple                | MacBookAir3,1                                    | 2012 | 11      | 39      | 2           |
| Hewlett-Packard      | Compaq 6510b                                     | 2007 | 11      | 39      | 2           |
| Dell                 | Latitude D430                                    | 2007 | 11      | 39      | 2           |
| Lenovo               | ThinkPad L530                                    | 2012 | 11      | 38      | 2           |
| Hewlett-Packard      | ProBook 470 G0                                   | 2013 | 11      | 37      | 2           |
| Hewlett-Packard      | ProBook 4340s                                    | 2012 | 11      | 37      | 2           |
| MSI                  | Modern 14 A10M                                   | 2019 | 11      | 36      | 2           |
| Acer                 | Aspire E1-531G                                   | 2012 | 11      | 36      | 2           |
| Hewlett-Packard      | Mini 110-3100                                    | 2010 | 11      | 36      | 2           |
| Lenovo               | IdeaPad Y700-17ISK 80Q0                          | 2015 | 11      | 35      | 2           |
| Lenovo               | IdeaPad 510-15ISK 80SR                           | 2016 | 11      | 34      | 2           |
| IBM                  | ThinkPad T43                                     | 2005 | 11      | 34      | 2           |
| ASUSTek Computer     | ROG Zephyrus G15 GA503QR_GA503QR                 | 2021 | 10      | 52      | 2           |
| Hewlett-Packard      | ProBook 645 G1                                   | 2015 | 10      | 48      | 2           |
| ASUSTek Computer     | ROG Zephyrus G14 GA401II_GA401II                 | 2020 | 10      | 47      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X409DA_M409DA                | 2019 | 10      | 47      | 2           |
| Acer                 | Aspire 5530                                      | 2008 | 10      | 47      | 2           |
| Hewlett-Packard      | Pavilion Gaming Laptop 17-cd1xxx                 | 2020 | 10      | 46      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412DA_X412DA                | 2019 | 10      | 46      | 2           |
| Hewlett-Packard      | Compaq 8510w                                     | 2007 | 10      | 46      | 2           |
| ASUSTek Computer     | K55DR                                            | 2012 | 10      | 45      | 2           |
| ASUSTek Computer     | TUF Gaming FX705DT_FX705DT                       | 2019 | 10      | 44      | 2           |
| Hewlett-Packard      | OMEN by HP Laptop 15-dc1xxx                      | 2019 | 10      | 42      | 2           |
| Lenovo               | ThinkPad Edge E531                               | 2013 | 10      | 42      | 2           |
| ASUSTek Computer     | K53BY                                            | 2011 | 10      | 42      | 2           |
| Notebook             | W35xSS_370SS                                     | 2014 | 10      | 41      | 2           |
| Hewlett-Packard      | Pavilion 13 x360 PC                              | 2014 | 10      | 41      | 2           |
| Acer                 | Aspire 5253                                      | 2010 | 10      | 40      | 2           |
| Lenovo               | B5400 20278                                      | 2013 | 10      | 39      | 2           |
| Dell                 | Inspiron 5721                                    | 2013 | 10      | 37      | 2           |
| Lenovo               | ThinkPad E460                                    | 2015 | 10      | 36      | 2           |
| Hewlett-Packard      | ProBook 440 G2                                   | 2015 | 10      | 36      | 2           |
| Toshiba              | Satellite P100                                   | 2006 | 10      | 36      | 2           |
| Hewlett-Packard      | Pavilion Laptop 15-cc5xx                         | 2017 | 10      | 35      | 2           |
| Acer                 | Aspire 5750ZG                                    | 2011 | 10      | 35      | 2           |
| Acer                 | Aspire 3000                                      | 2005 | 10      | 30      | 2           |
| Dell                 | Latitude 5591                                    | 2019 | 9       | 64      | 2           |
| Lenovo               | ThinkPad T470s W10DG                             | 2017 | 9       | 52      | 2           |
| HUAWEI               | KPRC-WX0                                         | 2019 | 9       | 46      | 2           |
| Dell                 | Precision 3530                                   | 2018 | 9       | 46      | 2           |
| ASUSTek Computer     | M50Vc                                            | 2008 | 9       | 46      | 2           |
| Dell                 | Latitude 5300                                    | 2019 | 9       | 45      | 2           |
| AVITA                | NS14A8                                           | 2020 | 9       | 44      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512FA_X512FA                | 2019 | 9       | 44      | 2           |
| Google               | Coral                                            | 2018 | 9       | 44      | 2           |
| Acer                 | Swift SF314-57                                   | 2019 | 9       | 43      | 2           |
| Hewlett-Packard      | EliteBook 745 G3                                 | 2016 | 9       | 43      | 2           |
| Acer                 | Aspire VN7-792G                                  | 2015 | 9       | 43      | 2           |
| Acer                 | Swift SF314-57G                                  | 2011 | 9       | 43      | 2           |
| ASUSTek Computer     | UX430UNR                                         | 2017 | 9       | 42      | 2           |
| ASUSTek Computer     | FX503VD                                          | 2017 | 9       | 42      | 2           |
| ASUSTek Computer     | GL753VE                                          | 2017 | 9       | 41      | 2           |
| Fujitsu              | LIFEBOOK E754                                    | 2014 | 9       | 41      | 2           |
| ASUSTek Computer     | N56JN                                            | 2014 | 9       | 41      | 2           |
| Clevo                | P170EM                                           | 2012 | 9       | 41      | 2           |
| Lenovo               | ThinkPad W550s                                   | 2015 | 9       | 40      | 2           |
| Hewlett-Packard      | ProBook 470 G1                                   | 2013 | 9       | 39      | 2           |
| ASUSTek Computer     | X750JB                                           | 2013 | 9       | 39      | 2           |
| Dell                 | Vostro 14-5480                                   | 2014 | 9       | 38      | 2           |
| ASUSTek Computer     | TP500LA                                          | 2014 | 9       | 38      | 2           |
| ASUSTek Computer     | N76VB                                            | 2013 | 9       | 38      | 2           |
| Lenovo               | B580 20144                                       | 2012 | 9       | 37      | 2           |
| Dell                 | Inspiron 13-7359                                 | 2016 | 9       | 36      | 2           |
| Lenovo               | B71-80 80RJ                                      | 2015 | 9       | 36      | 2           |
| Acer                 | Swift SF314-51                                   | 2016 | 9       | 35      | 2           |
| Acer                 | Aspire E5-572G                                   | 2014 | 9       | 35      | 2           |
| Acer                 | AO531h                                           | 2008 | 9       | 35      | 2           |
| Lenovo               | Yoga 900-13ISK 80MK                              | 2015 | 9       | 34      | 2           |
| MSI                  | GE620/GE620DX/FX620DX/FX623                      | 2011 | 9       | 34      | 2           |
| ASUSTek Computer     | 900                                              | 2008 | 9       | 34      | 2           |
| Dell                 | Latitude D610                                    | 2005 | 9       | 34      | 2           |
| DNS                  | W9x0LU                                           | 2015 | 9       | 32      | 2           |
| Dell                 | Vostro 3360                                      | 2012 | 9       | 32      | 2           |
| Lenovo               | IdeaPad 300-15IBR 80M3                           | 2015 | 9       | 31      | 2           |
| ASUSTek Computer     | X550MJ                                           | 2015 | 9       | 31      | 2           |
| Dell                 | Latitude 5411                                    | 2020 | 8       | 60      | 2           |
| Hewlett-Packard      | ZBook 17 G5                                      | 2018 | 8       | 57      | 2           |
| Dell                 | Precision 3541                                   | 2020 | 8       | 56      | 2           |
| Dell                 | Precision 5750                                   | 2020 | 8       | 55      | 2           |
| ASUSTek Computer     | ROG Strix G513QY_G513QY                          | 2021 | 8       | 51      | 2           |
| MSI                  | Prestige 14 A10SC                                | 2019 | 8       | 47      | 2           |
| Dell                 | Vostro 7590                                      | 2019 | 8       | 47      | 2           |
| Dell                 | Vostro 5590                                      | 2019 | 8       | 47      | 2           |
| YJKC                 | vBOOK Plus                                       | 2017 | 8       | 46      | 2           |
| Hewlett-Packard      | HDX 16                                           | 2008 | 8       | 46      | 2           |
| Acer                 | Swift SF314-54G                                  | 2019 | 8       | 44      | 2           |
| Hewlett-Packard      | EliteBook 2730p                                  | 2009 | 8       | 44      | 2           |
| Acer                 | Aspire 6530G                                     | 2008 | 8       | 44      | 2           |
| Alienware            | 17 R5                                            | 2018 | 8       | 43      | 2           |
| Acer                 | Swift SF314-56G                                  | 2018 | 8       | 43      | 2           |
| Acer                 | Aspire 7560G                                     | 2011 | 8       | 43      | 2           |
| Apple                | MacBookPro5,2                                    | 2009 | 8       | 43      | 2           |
| Acer                 | TravelMate 5730                                  | 2008 | 8       | 43      | 2           |
| Lenovo               | IdeaPad 5 15ALC05 82LN                           | 2021 | 8       | 42      | 2           |
| Lenovo               | ThinkPad T550                                    | 2015 | 8       | 42      | 2           |
| Clevo                | P15xEMx                                          | 2012 | 8       | 42      | 2           |
| Toshiba              | Satellite L750D                                  | 2011 | 8       | 42      | 2           |
| Clevo                | W150HRM                                          | 2011 | 8       | 41      | 2           |
| Samsung Electronics  | QX310/QX410/QX510/SF310/SF410/SF510              | 2010 | 8       | 41      | 2           |
| Acer                 | Aspire 5610Z                                     | 2006 | 8       | 41      | 2           |
| Hewlett-Packard      | Laptop 14s-fq0xxx                                | 2020 | 8       | 40      | 2           |
| Acer                 | Swift SF314-55G                                  | 2018 | 8       | 40      | 2           |
| ASUSTek Computer     | UX430UQ                                          | 2016 | 8       | 40      | 2           |
| Lenovo               | ThinkPad X61 Tablet                              | 2007 | 8       | 40      | 2           |
| Hewlett-Packard      | Pavilion Notebook 15-bc5xxx                      | 2019 | 8       | 39      | 2           |
| Toshiba              | PORTEGE R830                                     | 2011 | 8       | 39      | 2           |
| ASUSTek Computer     | N75SF                                            | 2011 | 8       | 39      | 2           |
| Lenovo               | IdeaPad S540-14IWL 81ND                          | 2019 | 8       | 37      | 2           |
| Acer                 | Aspire VN7-592G                                  | 2015 | 8       | 37      | 2           |
| ASUSTek Computer     | X555LB                                           | 2015 | 8       | 37      | 2           |
| Toshiba              | Satellite A135                                   | 2006 | 8       | 36      | 2           |
| Acer                 | Swift SF514-52T                                  | 2017 | 8       | 35      | 2           |
| BANGHO               | MOV                                              | 2012 | 8       | 35      | 2           |
| Dell                 | Inspiron 7548                                    | 2015 | 8       | 34      | 2           |
| ASUSTek Computer     | X751SJ                                           | 2015 | 8       | 33      | 2           |
| ASUSTek Computer     | X540NV                                           | 2017 | 8       | 30      | 2           |
| Notebook             | NH5xAx                                           | 2020 | 7       | 60      | 2           |
| Fujitsu              | LIFEBOOK T902                                    | 2012 | 7       | 51      | 2           |
| SLIMBOOK             | PROX14-AMD                                       | 2020 | 7       | 50      | 2           |
| Dell                 | Precision 3540                                   | 2019 | 7       | 49      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509DJ_M509DJ                | 2019 | 7       | 47      | 2           |
| Hewlett-Packard      | Pavilion dv3                                     | 2010 | 7       | 46      | 2           |
| ASUSTek Computer     | TUF Gaming FA506II_FA506II                       | 2020 | 7       | 45      | 2           |
| ASUSTek Computer     | K46CB                                            | 2013 | 7       | 45      | 2           |
| Acer                 | Aspire 5737Z                                     | 2009 | 7       | 45      | 2           |
| Toshiba              | TECRA A11                                        | 2010 | 7       | 44      | 2           |
| Toshiba              | TECRA A10                                        | 2008 | 7       | 44      | 2           |
| Dell                 | Inspiron 5580                                    | 2019 | 7       | 43      | 2           |
| Fujitsu              | LIFEBOOK S760                                    | 2010 | 7       | 43      | 2           |
| Dell                 | Precision 7710                                   | 2018 | 7       | 42      | 2           |
| Dell                 | Precision M6300                                  | 2007 | 7       | 42      | 2           |
| ASUSTek Computer     | VivoBook 15 ASUS Laptop X570UD                   | 2018 | 7       | 39      | 2           |
| Notebook             | N85_N87,HJ,HJ1,HK1                               | 2016 | 7       | 39      | 2           |
| Dell                 | Inspiron 5755                                    | 2016 | 7       | 39      | 2           |
| Apple                | MacBookPro3,1                                    | 2008 | 7       | 39      | 2           |
| Fujitsu Siemens      | AMILO La1703                                     | 2006 | 7       | 39      | 2           |
| Acer                 | Aspire A717-71G                                  | 2017 | 7       | 38      | 2           |
| ASUSTek Computer     | X556UJ                                           | 2015 | 7       | 38      | 2           |
| Lenovo               | IdeaPad S540-15IWL D 81NE                        | 2019 | 7       | 37      | 2           |
| Acer                 | Swift SF314-52G                                  | 2017 | 7       | 37      | 2           |
| Acer                 | Aspire 3810T                                     | 2009 | 7       | 37      | 2           |
| Hewlett-Packard      | Pavilion Laptop 14-bf1xx                         | 2017 | 7       | 35      | 2           |
| Acer                 | TravelMate P449-G2-M                             | 2017 | 7       | 35      | 2           |
| Timi                 | RedmiBook 14                                     | 2019 | 7       | 34      | 2           |
| Lenovo               | V14-IIL 82C4                                     | 2019 | 7       | 33      | 2           |
| Hewlett-Packard      | 240 G3                                           | 2014 | 7       | 32      | 2           |
| ASUSTek Computer     | T102HA                                           | 2016 | 7       | 27      | 2           |
| Dell                 | Precision 7750                                   | 2020 | 6       | 67      | 2           |
| ASUSTek Computer     | TUF Gaming FA506IU_TUF506IU                      | 2020 | 6       | 54      | 2           |
| ASUSTek Computer     | TUF Gaming FA706IU_FA706IU                       | 2020 | 6       | 51      | 2           |
| Lenovo               | IdeaPad 720S-13ARR 81BR                          | 2017 | 6       | 51      | 2           |
| Dell                 | MXG071                                           | 2008 | 6       | 49      | 2           |
| ASUSTek Computer     | ROG Flow X13 GV301QE_GV301QE                     | 2021 | 6       | 48      | 2           |
| Acer                 | ConceptD CN315-71P                               | 2020 | 6       | 48      | 2           |
| Dell                 | Studio 1749                                      | 2010 | 6       | 47      | 2           |
| ASUSTek Computer     | M50Vn                                            | 2008 | 6       | 47      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412DAP_F412DA               | 2019 | 6       | 46      | 2           |
| ASUSTek Computer     | K43TK                                            | 2012 | 6       | 45      | 2           |
| Lenovo               | E43                                              | 2009 | 6       | 45      | 2           |
| Hewlett-Packard      | ENVY Laptop 17-ce1xxx                            | 2019 | 6       | 44      | 2           |
| Acer                 | Nitro AN715-51                                   | 2019 | 6       | 44      | 2           |
| MSI                  | PS63 Modern 8RC                                  | 2018 | 6       | 44      | 2           |
| MSI                  | GT70 2PC                                         | 2014 | 6       | 44      | 2           |
| Hewlett-Packard      | ProBook 650 G4                                   | 2018 | 6       | 43      | 2           |
| Hewlett-Packard      | EliteBook 850 G4                                 | 2017 | 6       | 43      | 2           |
| Hewlett-Packard      | ZBook 14 G2                                      | 2016 | 6       | 43      | 2           |
| Acer                 | TravelMate P633-M                                | 2012 | 6       | 43      | 2           |
| Packard Bell         | EasyNote TS11SB                                  | 2011 | 6       | 43      | 2           |
| Acer                 | Aspire 5253G                                     | 2011 | 6       | 43      | 2           |
| Hewlett-Packard      | ZBook 15v G5                                     | 2019 | 6       | 42      | 2           |
| Dell                 | Inspiron 7586                                    | 2018 | 6       | 42      | 2           |
| MSI                  | GE70 2OC\2OD\2OE                                 | 2013 | 6       | 42      | 2           |
| Dell                 | MXG061                                           | 2006 | 6       | 42      | 2           |
| Hewlett-Packard      | 255 G8 Notebook PC                               | 2020 | 6       | 41      | 2           |
| Lenovo               | IdeaPad S540-15IWL GTX 81SW                      | 2019 | 6       | 41      | 2           |
| Hewlett-Packard      | Snappy                                           | 2019 | 6       | 41      | 2           |
| Fujitsu              | LIFEBOOK E782                                    | 2012 | 6       | 41      | 2           |
| Dell                 | Vostro1710                                       | 2008 | 6       | 41      | 2           |
| Dell                 | Precision M90                                    | 2007 | 6       | 41      | 2           |
| Dell                 | Inspiron 5482                                    | 2018 | 6       | 40      | 2           |
| ASUSTek Computer     | G551JW                                           | 2015 | 6       | 40      | 2           |
| Lenovo               | IdeaPad U430 Touch 20270                         | 2013 | 6       | 40      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512JA_F512JA                | 2020 | 6       | 38      | 2           |
| Fujitsu              | LIFEBOOK E756                                    | 2016 | 6       | 38      | 2           |
| Toshiba              | Satellite C55t-C                                 | 2015 | 6       | 38      | 2           |
| Notebook             | P65_P67RGRERA                                    | 2015 | 6       | 38      | 2           |
| Lenovo               | Y70-70 Touch 80DU                                | 2014 | 6       | 38      | 2           |
| Notebook             | W35xSTQ_370ST                                    | 2013 | 6       | 38      | 2           |
| Hewlett-Packard      | ENVY 17 Leap Motion SE NB PC                     | 2013 | 6       | 38      | 2           |
| Toshiba              | PORTEGE R930                                     | 2012 | 6       | 38      | 2           |
| Dell                 | System XPS L702X                                 | 2012 | 6       | 37      | 2           |
| Clevo                | W250ENQ / W270ENQ                                | 2012 | 6       | 37      | 2           |
| ASUSTek Computer     | N55SL                                            | 2011 | 6       | 37      | 2           |
| ASUSTek Computer     | X75VCP                                           | 2013 | 6       | 36      | 2           |
| Lenovo               | IdeaPad Z370                                     | 2011 | 6       | 33      | 2           |
| Packard Bell         | DOTS E2                                          | 2010 | 6       | 33      | 2           |
| ASUSTek Computer     | ROG Strix G533QS_G533QS                          | 2021 | 5       | 59      | 2           |
| Acer                 | Aspire 8943G                                     | 2010 | 5       | 59      | 2           |
| Dell                 | Precision 7730                                   | 2019 | 5       | 57      | 2           |
| Notebook             | P65_67HSHP                                       | 2016 | 5       | 55      | 2           |
| ASUSTek Computer     | ASUS TUF Gaming A17 FA706IU_FA706IU              | 2020 | 5       | 54      | 2           |
| Intel Client Systems | LAPBC710                                         | 2021 | 5       | 53      | 2           |
| Lenovo               | ThinkPad W700                                    | 2009 | 5       | 50      | 2           |
| Acer                 | Aspire 8920                                      | 2008 | 5       | 50      | 2           |
| ASUSTek Computer     | N50Vc                                            | 2008 | 5       | 50      | 2           |
| Notebook             | P95_96_97Ex,Rx                                   | 2019 | 5       | 49      | 2           |
| Hewlett-Packard      | ZBook Studio G4                                  | 2018 | 5       | 49      | 2           |
| Hewlett-Packard      | ProBook 655 G1                                   | 2014 | 5       | 49      | 2           |
| Hewlett-Packard      | ProBook 635 Aero G7 Notebook PC                  | 2020 | 5       | 47      | 2           |
| ASUSTek Computer     | ROG Strix G512LW_G512LW                          | 2020 | 5       | 47      | 2           |
| Lenovo               | IdeaPad Gaming 3 15IMH05 82CG                    | 2020 | 5       | 46      | 2           |
| Samsung Electronics  | 760XBE                                           | 2019 | 5       | 46      | 2           |
| Dell                 | Inspiron 1521                                    | 2007 | 5       | 46      | 2           |
| ASUSTek Computer     | ROG Strix G513IH_G513IH                          | 2021 | 5       | 45      | 2           |
| Samsung Electronics  | 305E4Z/305E5Z/305E7Z                             | 2012 | 5       | 45      | 2           |
| Acer                 | TravelMate 6592                                  | 2007 | 5       | 45      | 2           |
| Toshiba              | Satellite A300D                                  | 2008 | 5       | 44      | 2           |
| Hewlett-Packard      | Compaq 8710p                                     | 2007 | 5       | 44      | 2           |
| Lenovo               | ThinkPad P15s Gen 1                              | 2020 | 5       | 43      | 2           |
| Acer                 | Predator PT315-51                                | 2019 | 5       | 43      | 2           |
| MSI                  | GE72 6QF                                         | 2015 | 5       | 43      | 2           |
| Lenovo               | G585                                             | 2012 | 5       | 43      | 2           |
| Lenovo               | ThinkPad X200T                                   | 2008 | 5       | 43      | 2           |
| Avell High Perfor... | B.ON                                             | 2021 | 5       | 42      | 2           |
| Hewlett-Packard      | ProBook 455 G2                                   | 2014 | 5       | 42      | 2           |
| Toshiba              | Satellite U400                                   | 2010 | 5       | 42      | 2           |
| Sony                 | VGN-TZ3RXN_B                                     | 2007 | 5       | 42      | 2           |
| Acer                 | Aspire 5110                                      | 2007 | 5       | 42      | 2           |
| HONOR                | HLYL-WXX9                                        | 2021 | 5       | 41      | 2           |
| Lenovo               | IdeaPad 3 15ARE05 81W4                           | 2020 | 5       | 41      | 2           |
| Toshiba              | TECRA A9                                         | 2007 | 5       | 41      | 2           |
| Acer                 | Aspire 4920                                      | 2007 | 5       | 41      | 2           |
| Hewlett-Packard      | ENVY Laptop 13-aq1xxx                            | 2019 | 5       | 40      | 2           |
| Lenovo               | ThinkPad L570                                    | 2017 | 5       | 40      | 2           |
| Lenovo               | IdeaPad MIIX 700-12ISK 80QL                      | 2015 | 5       | 40      | 2           |
| Fujitsu Siemens      | LIFEBOOK E8410                                   | 2007 | 5       | 40      | 2           |
| Hewlett-Packard      | Compaq tc4400                                    | 2006 | 5       | 40      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X521IA_M533IA                | 2020 | 5       | 39      | 2           |
| Notebook             | P95xER                                           | 2018 | 5       | 39      | 2           |
| Acer                 | Aspire E5-721                                    | 2014 | 5       | 39      | 2           |
| Lenovo               | M5400 20281                                      | 2013 | 5       | 39      | 2           |
| MSI                  | GX60 1AC                                         | 2012 | 5       | 39      | 2           |
| Acer                 | Aspire 5610                                      | 2006 | 5       | 39      | 2           |
| Google               | Kindred                                          | 2021 | 5       | 38      | 2           |
| ASUSTek Computer     | UX310UAK                                         | 2016 | 5       | 38      | 2           |
| Notebook             | N141CU                                           | 2019 | 5       | 37      | 2           |
| Hewlett-Packard      | EliteBook 820 G4                                 | 2017 | 5       | 37      | 2           |
| Lenovo               | IdeaPad L3 15IML05 81Y3                          | 2020 | 5       | 36      | 2           |
| Google               | Stout                                            | 2019 | 5       | 36      | 2           |
| MSI                  | GV62 8RE                                         | 2018 | 5       | 36      | 2           |
| ASUSTek Computer     | X556UQ                                           | 2016 | 5       | 36      | 2           |
| Hewlett-Packard      | EliteBook Folio 1040 G2                          | 2015 | 5       | 36      | 2           |
| Fujitsu              | LIFEBOOK E734                                    | 2014 | 5       | 36      | 2           |
| Notebook             | W650EH                                           | 2013 | 5       | 36      | 2           |
| Hewlett-Packard      | Compaq nc6220                                    | 2005 | 5       | 36      | 2           |
| Purism               | Librem 15 v4                                     | 2019 | 5       | 35      | 2           |
| Hewlett-Packard      | Pavilion dv4000                                  | 2005 | 5       | 35      | 2           |
| MSI                  | GL73 8RC                                         | 2018 | 5       | 34      | 2           |
| Toshiba              | TECRA Z50-A                                      | 2014 | 5       | 34      | 2           |
| Digibras             | US41II1                                          | 2013 | 5       | 34      | 2           |
| Lenovo               | Yoga 500-14IBD 80N4                              | 2015 | 5       | 33      | 2           |
| MSI                  | CX61 0NC/CX61 0ND/CX61 0NF/CX61 0NE              | 2012 | 5       | 33      | 2           |
| Hewlett-Packard      | ENVY 4                                           | 2012 | 5       | 32      | 2           |
| Hewlett-Packard      | ProBook 5330m                                    | 2011 | 5       | 31      | 2           |
| Lenovo               | IdeaPad 710S Plus-13IKB 80W3                     | 2017 | 5       | 29      | 2           |
| Dell                 | XPS L321X                                        | 2012 | 5       | 29      | 2           |
| Toshiba              | Satellite P500                                   | 2009 | 4       | 59      | 2           |
| Lenovo               | ThinkPad T14 Gen 2a                              | 2021 | 4       | 56      | 2           |
| Acer                 | Aspire 8940G                                     | 2009 | 4       | 56      | 2           |
| Hewlett-Packard      | ZBook Studio x360 G5                             | 2019 | 4       | 55      | 2           |
| Lenovo               | ThinkPad A285                                    | 2019 | 4       | 54      | 2           |
| Hewlett-Packard      | EliteBook x360 1040 G5                           | 2019 | 4       | 54      | 2           |
| Lenovo               | ThinkBook 14 G3 ACL 21A2                         | 2021 | 4       | 53      | 2           |
| Lenovo               | ThinkPad P51s                                    | 2018 | 4       | 52      | 2           |
| Acer                 | Swift SF514-54GT                                 | 2019 | 4       | 51      | 2           |
| MSI                  | GE66 Raider 10SF                                 | 2020 | 4       | 50      | 2           |
| Hewlett-Packard      | EliteBook 735 G6                                 | 2019 | 4       | 50      | 2           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506QM_FA506QM              | 2021 | 4       | 49      | 2           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506IU_TUF506IU             | 2021 | 4       | 48      | 2           |
| ASUSTek Computer     | TUF Gaming FA506IU_FA506IU                       | 2020 | 4       | 48      | 2           |
| MSI                  | GS63 7RE                                         | 2017 | 4       | 47      | 2           |
| Lenovo               | ThinkPad P72                                     | 2019 | 4       | 46      | 2           |
| Fujitsu              | STYLISTIC Q704                                   | 2014 | 4       | 46      | 2           |
| Fujitsu              | LIFEBOOK NH570                                   | 2009 | 4       | 46      | 2           |
| Fujitsu Siemens      | LIFEBOOK S6410                                   | 2007 | 4       | 46      | 2           |
| ASUSTek Computer     | ROG Strix G512LI_G512LI                          | 2020 | 4       | 45      | 2           |
| Dell                 | Vostro 1720                                      | 2009 | 4       | 45      | 2           |
| Acer                 | Nitro AN517-41                                   | 2021 | 4       | 44      | 2           |
| ASUSTek Computer     | ASUSPRO P2540FAC_P2540FA                         | 2019 | 4       | 43      | 2           |
| Acer                 | Predator PT515-51                                | 2019 | 4       | 42      | 2           |
| MSI                  | GX60 3CC                                         | 2014 | 4       | 42      | 2           |
| Notebook             | W65_W670SR                                       | 2013 | 4       | 42      | 2           |
| Alienware            | M11x R2                                          | 2010 | 4       | 42      | 2           |
| Toshiba              | Satellite Pro P300                               | 2008 | 4       | 42      | 2           |
| Fujitsu Siemens      | STYLISTIC ST6012                                 | 2008 | 4       | 42      | 2           |
| Acer                 | TravelMate 6460                                  | 2006 | 4       | 42      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X513IA_M513IA                | 2020 | 4       | 41      | 2           |
| Hewlett-Packard      | Compaq nx6325                                    | 2006 | 4       | 41      | 2           |
| HUAWEI               | NBLL-WXX9                                        | 2020 | 4       | 40      | 2           |
| HUAWEI               | KPR-WX9                                          | 2019 | 4       | 40      | 2           |
| Dell                 | Vostro 5468                                      | 2017 | 4       | 40      | 2           |
| ASUSTek Computer     | E403NA                                           | 2017 | 4       | 40      | 2           |
| MSI                  | MS-168A                                          | 2010 | 4       | 40      | 2           |
| Hewlett-Packard      | ProBook 6555b                                    | 2010 | 4       | 40      | 2           |
| Toshiba              | PORTEGE M700                                     | 2008 | 4       | 40      | 2           |
| Hewlett-Packard      | Compaq nx6125                                    | 2005 | 4       | 40      | 2           |
| TUXEDO               | InfinityBook Pro 15 v5                           | 2020 | 4       | 39      | 2           |
| Hewlett-Packard      | ZBook 15 G4                                      | 2017 | 4       | 39      | 2           |
| Hewlett-Packard      | EliteBook 725 G3                                 | 2016 | 4       | 39      | 2           |
| Toshiba              | QOSMIO X875                                      | 2012 | 4       | 39      | 2           |
| ASUSTek Computer     | X550VC                                           | 2013 | 4       | 38      | 2           |
| Dell                 | Latitude D420                                    | 2007 | 4       | 38      | 2           |
| Lenovo               | IdeaPad S540-14IML 81NF                          | 2019 | 4       | 37      | 2           |
| ASUSTek Computer     | UX550VD                                          | 2017 | 4       | 37      | 2           |
| ASUSTek Computer     | X756UQK                                          | 2016 | 4       | 37      | 2           |
| Dell                 | Latitude D810                                    | 2005 | 4       | 37      | 2           |
| Lenovo               | IdeaPad S540-15IWL 81NE                          | 2019 | 4       | 36      | 2           |
| Hewlett-Packard      | Pavilion Notebook 15-dp0xxx                      | 2019 | 4       | 36      | 2           |
| Hewlett-Packard      | ProBook 470 G4                                   | 2017 | 4       | 36      | 2           |
| Hewlett-Packard      | Spectre x2 Detachable                            | 2015 | 4       | 36      | 2           |
| Toshiba              | Satellite P75-A                                  | 2013 | 4       | 36      | 2           |
| Acer                 | Aspire V5-572PG                                  | 2013 | 4       | 36      | 2           |
| Toshiba              | Satellite P105                                   | 2006 | 4       | 36      | 2           |
| Hewlett-Packard      | Compaq nx9420                                    | 2006 | 4       | 36      | 2           |
| MSI                  | GP72 7RDX                                        | 2017 | 4       | 35      | 2           |
| Pegatron             | A17W8                                            | 2013 | 4       | 35      | 2           |
| Fujitsu              | LIFEBOOK P702                                    | 2013 | 4       | 35      | 2           |
| Fujitsu Siemens      | AMILO Pa 1510                                    | 2006 | 4       | 35      | 2           |
| Lenovo               | V570 HuronRiver Platform                         | 2011 | 4       | 34      | 2           |
| Notebook             | W65_W67RZ1                                       | 2016 | 4       | 33      | 2           |
| Toshiba              | PORTEGE Z30-B                                    | 2015 | 4       | 33      | 2           |
| Lenovo               | Yoga 500-15IBD 80N6                              | 2015 | 4       | 33      | 2           |
| Packard Bell         | EasyNote_BU45                                    | 2007 | 4       | 33      | 2           |
| ASUSTek Computer     | A6Km                                             | 2007 | 4       | 33      | 2           |
| Pegatron             | C15A                                             | 2013 | 4       | 32      | 2           |
| NEC Computers        | Packard Bell EasyNote                            | 2005 | 4       | 31      | 2           |
| Toshiba              | QOSMIO X500                                      | 2010 | 3       | 59      | 2           |
| Hewlett-Packard      | ProBook 645 G4                                   | 2018 | 3       | 58      | 2           |
| Notebook             | PCx0Dx                                           | 2020 | 3       | 54      | 2           |
| Dell                 | Latitude 5421                                    | 2021 | 3       | 53      | 2           |
| Fujitsu              | T900                                             | 2010 | 3       | 50      | 2           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506IV_FX506IV              | 2020 | 3       | 49      | 2           |
| ASUSTek Computer     | ROG Zephyrus M16 GU603HE_GU603HE                 | 2021 | 3       | 48      | 2           |
| Hewlett-Packard      | OMEN by HP Laptop 15-dh1xxx                      | 2020 | 3       | 48      | 2           |
| Lenovo               | IdeaPad 720S-14IKB 81BD                          | 2019 | 3       | 48      | 2           |
| Hewlett-Packard      | Pavilion Laptop 15-eh1xxx                        | 2021 | 3       | 47      | 2           |
| ASUSTek Computer     | ROG Zephyrus G15 GA503QS_GA503QS                 | 2021 | 3       | 47      | 2           |
| Alienware            | M17x                                             | 2010 | 3       | 46      | 2           |
| Dell                 | Vostro 1320                                      | 2009 | 3       | 45      | 2           |
| Lenovo               | ThinkPad P71                                     | 2017 | 3       | 44      | 2           |
| ASUSTek Computer     | F3Sa                                             | 2007 | 3       | 44      | 2           |
| Lenovo               | Yoga 14sARH 2021 82LB                            | 2021 | 3       | 43      | 2           |
| Timi                 | RedmiBook 13 R                                   | 2020 | 3       | 43      | 2           |
| ASUSTek Computer     | B400VC                                           | 2013 | 3       | 43      | 2           |
| Lenovo               | ThinkPad R61/R61i                                | 2007 | 3       | 43      | 2           |
| Acer                 | Aspire A515-45                                   | 2021 | 3       | 42      | 2           |
| Notebook             | NP50DE_DB                                        | 2020 | 3       | 42      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X571GT_F571GT                | 2019 | 3       | 42      | 2           |
| Lenovo               | ThinkPad E565                                    | 2015 | 3       | 42      | 2           |
| Fujitsu              | LIFEBOOK S782                                    | 2012 | 3       | 42      | 2           |
| Fujitsu Siemens      | LIFEBOOK E8420                                   | 2008 | 3       | 42      | 2           |
| Lenovo               | ThinkBook 14p Gen 2 20YN                         | 2021 | 3       | 41      | 2           |
| Hewlett-Packard      | ENVY Laptop 13-ba1xxx                            | 2021 | 3       | 41      | 2           |
| Timi                 | Mi Laptop Pro 15 2020                            | 2020 | 3       | 41      | 2           |
| Lenovo               | ThinkPad T480S                                   | 2020 | 3       | 41      | 2           |
| Hewlett-Packard      | Laptop 14-cm1xxx                                 | 2019 | 3       | 41      | 2           |
| Dell                 | Latitude 5290 2-in-1                             | 2018 | 3       | 41      | 2           |
| ASUSTek Computer     | S451LB                                           | 2013 | 3       | 41      | 2           |
| Fujitsu Siemens      | LIFEBOOK S7210                                   | 2008 | 3       | 41      | 2           |
| Lenovo               | ThinkPad P51 W10DG                               | 2019 | 3       | 40      | 2           |
| MSI                  | GE60 2PL                                         | 2014 | 3       | 40      | 2           |
| MSI                  | GE70 2OC\2OE                                     | 2013 | 3       | 40      | 2           |
| Toshiba              | TECRA R940                                       | 2012 | 3       | 40      | 2           |
| Fujitsu Siemens      | LIFEBOOK S6420                                   | 2008 | 3       | 40      | 2           |
| ASUSTek Computer     | GX501VIK                                         | 2017 | 3       | 39      | 2           |
| Hewlett-Packard      | Pavilion TS Sleekbook 15                         | 2013 | 3       | 39      | 2           |
| ASUSTek Computer     | S301LP                                           | 2013 | 3       | 39      | 2           |
| ASUSTek Computer     | N73SM                                            | 2011 | 3       | 39      | 2           |
| Toshiba              | Satellite Pro U400                               | 2009 | 3       | 39      | 2           |
| Hewlett-Packard      | Compaq nc8430                                    | 2007 | 3       | 39      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X571GT_K571GT                | 2019 | 3       | 38      | 2           |
| Lenovo               | IdeaPad 330-17ICH 81FL                           | 2018 | 3       | 38      | 2           |
| Monster              | ABRA A5 V1                                       | 2014 | 3       | 38      | 2           |
| Notebook             | W350SKQ/W370SK                                   | 2013 | 3       | 38      | 2           |
| Toshiba              | TECRA R850                                       | 2011 | 3       | 38      | 2           |
| MSI                  | VR601                                            | 2007 | 3       | 38      | 2           |
| Sony                 | VGN-FE41M                                        | 2006 | 3       | 38      | 2           |
| MSI                  | GP72 6QF                                         | 2016 | 3       | 37      | 2           |
| ASUSTek Computer     | G750JZ                                           | 2014 | 3       | 37      | 2           |
| Lenovo               | ThinkPad Edge E320                               | 2011 | 3       | 37      | 2           |
| Hewlett-Packard      | Laptop 15s-du2xxx                                | 2021 | 3       | 36      | 2           |
| Fujitsu              | LIFEBOOK U938                                    | 2018 | 3       | 36      | 2           |
| Medion               | X6816                                            | 2011 | 3       | 36      | 2           |
| Toshiba              | Satellite A110                                   | 2006 | 3       | 36      | 2           |
| Samsung Electronics  | 750XDA                                           | 2021 | 3       | 35      | 2           |
| Fujitsu              | LIFEBOOK T935                                    | 2015 | 3       | 35      | 2           |
| ASUSTek Computer     | X302LJ                                           | 2015 | 3       | 35      | 2           |
| MSI                  | GS70 2OD                                         | 2013 | 3       | 35      | 2           |
| Packard Bell         | EasyNote TS13HR                                  | 2012 | 3       | 35      | 2           |
| IBM                  | ThinkPad X41                                     | 2005 | 3       | 35      | 2           |
| Hewlett-Packard      | Compaq nc6120                                    | 2005 | 3       | 35      | 2           |
| ASUSTek Computer     | UX530UX                                          | 2017 | 3       | 34      | 2           |
| Wortmann AG          | TERRA_MOBILE_1541H                               | 2014 | 3       | 34      | 2           |
| Fujitsu              | LIFEBOOK U745                                    | 2014 | 3       | 34      | 2           |
| ASUSTek Computer     | X75VB                                            | 2013 | 3       | 34      | 2           |
| IBM                  | ThinkPad R52                                     | 2006 | 3       | 34      | 2           |
| Acer                 | Aspire 3100                                      | 2006 | 3       | 34      | 2           |
| Hewlett-Packard      | Compaq nx8220                                    | 2005 | 3       | 34      | 2           |
| Lenovo               | ThinkPad Yoga 260                                | 2015 | 3       | 33      | 2           |
| ASUSTek Computer     | P453UA                                           | 2015 | 3       | 33      | 2           |
| Dell                 | Latitude 6430U                                   | 2012 | 3       | 33      | 2           |
| Toshiba              | TECRA A50-C                                      | 2015 | 3       | 32      | 2           |
| Beelink              | BT3 PRO                                          | 2018 | 3       | 31      | 2           |
| Lenovo               | V320-17IKB 81CN                                  | 2018 | 3       | 30      | 2           |
| MSI                  | PS42 Modern 8RA                                  | 2019 | 3       | 29      | 2           |
| Acer                 | Aspire 5000                                      | 2005 | 3       | 26      | 2           |
| Phoenix/SiS          | M7x0S                                            |      | 3       | 26      | 2           |
| Lenovo               | ThinkPad W701                                    | 2012 | 2       | 64      | 2           |
| Dell                 | Latitude 9420                                    | 2021 | 2       | 63      | 2           |
| PC Specialist        | X170SM                                           | 2020 | 2       | 59      | 2           |
| TUXEDO               | Book XP1511                                      | 2020 | 2       | 55      | 2           |
| Lenovo               | ThinkPad P17 Gen 1                               | 2021 | 2       | 51      | 2           |
| Lenovo               | Legion R7000P2020H 82GR                          | 2020 | 2       | 50      | 2           |
| Dell                 | Inspiron 5583                                    | 2019 | 2       | 50      | 2           |
| Notebook             | P17SM                                            | 2013 | 2       | 48      | 2           |
| Toshiba              | Satellite M300                                   | 2008 | 2       | 48      | 2           |
| Lenovo               | Legion 5 15ACH6 82JW                             | 2021 | 2       | 47      | 2           |
| ASUSTek Computer     | ROG Zephyrus M16 GU603HR_GU603HR                 | 2021 | 2       | 47      | 2           |
| Lenovo               | Yoga Slim 7 Pro 14ITL5 82FX                      | 2020 | 2       | 47      | 2           |
| FUJITSU CLIENT CO... | LIFEBOOK U9310                                   | 2020 | 2       | 47      | 2           |
| ASUSTek Computer     | ASUS TUF Gaming A17 FA706IU_TUF706IU             | 2020 | 2       | 47      | 2           |
| Acer                 | ConceptD CN715-71                                | 2019 | 2       | 46      | 2           |
| MSI                  | GE66 Raider 11UH                                 | 2021 | 2       | 45      | 2           |
| Dell                 | G7 7590                                          | 2020 | 2       | 45      | 2           |
| ASUSTek Computer     | TUF Gaming FA506IU_FX506IU                       | 2020 | 2       | 45      | 2           |
| PC Specialist        | P9XXEN_EF_ED                                     | 2019 | 2       | 45      | 2           |
| Fujitsu              | LIFEBOOK T900                                    | 2010 | 2       | 45      | 2           |
| Sony                 | VGN-Z570AN                                       | 2008 | 2       | 45      | 2           |
| MSI                  | GP76 Leopard 10UE                                | 2021 | 2       | 44      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X515JF_X515JF                | 2021 | 2       | 44      | 2           |
| ASUSTek Computer     | ROG Flow X13 GV301QH_GV301QH                     | 2021 | 2       | 44      | 2           |
| Samsung Electronics  | 900X5T                                           | 2018 | 2       | 44      | 2           |
| Lenovo               | Legion Y730-15ICH 81HD                           | 2018 | 2       | 44      | 2           |
| PC Specialist        | N85_N87,HJ,HJ1,HK1                               | 2017 | 2       | 44      | 2           |
| Sony                 | VGN-FZ31M                                        | 2007 | 2       | 44      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X570ZD_K570ZD                | 2019 | 2       | 43      | 2           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X570DD_D570DD                | 2019 | 2       | 43      | 2           |
| Sony                 | VPCEB1E1E                                        | 2010 | 2       | 43      | 2           |
| Getac                | S400                                             | 2010 | 2       | 43      | 2           |
| ASUSTek Computer     | N53Jn                                            | 2010 | 2       | 43      | 2           |
| ASUSTek Computer     | M51Sn                                            | 2007 | 2       | 43      | 2           |
| Hewlett-Packard      | ENVY Laptop 17m-ce0xxx                           | 2019 | 2       | 42      | 2           |
| Hewlett-Packard      | ZBook Fury 17 G7 Mobile Workstation              | 2020 | 2       | 41      | 2           |
| Hewlett-Packard      | ProBook 645 G3                                   | 2017 | 2       | 41      | 2           |
| Hewlett-Packard      | ProBook 6455b                                    | 2010 | 2       | 41      | 2           |
| Fujitsu              | LIFEBOOK T5010                                   | 2009 | 2       | 41      | 2           |
| MSI                  | P75 Creator 9SE                                  | 2019 | 2       | 40      | 2           |
| Hewlett-Packard      | ZBook 15u G4                                     | 2019 | 2       | 40      | 2           |
| Notebook             | P95_HP                                           | 2017 | 2       | 40      | 2           |
| MSI                  | GE72 2QE                                         | 2015 | 2       | 40      | 2           |
| Fujitsu              | LIFEBOOK NH532                                   | 2013 | 2       | 40      | 2           |
| Clevo                | P170HMx                                          | 2011 | 2       | 40      | 2           |
| MSI                  | PS63 Modern 8SC                                  | 2019 | 2       | 39      | 2           |
| MSI                  | GL63 8SD                                         | 2019 | 2       | 39      | 2           |
| Lenovo               | IdeaPad S540-15IML D 81NG                        | 2019 | 2       | 39      | 2           |
| Chuwi                | UBook Pro                                        | 2019 | 2       | 39      | 2           |
| MSI                  | PS63 Modern 8RD                                  | 2018 | 2       | 39      | 2           |
| Gigabyte Technology  | P57V6                                            | 2018 | 2       | 39      | 2           |
| Dream Machines       | N85_N87,HJ,HJ1,HK1                               | 2018 | 2       | 39      | 2           |
| ASUSTek Computer     | K501LB                                           | 2015 | 2       | 39      | 2           |
| ASUSTek Computer     | N551JQ                                           | 2014 | 2       | 39      | 2           |
| ASUSTek Computer     | V1J                                              | 2006 | 2       | 39      | 2           |
| PC Specialist        | NH5x_7xRCx,RDx                                   | 2019 | 2       | 38      | 2           |
| Notebook             | P7xxDM2(-G)                                      | 2016 | 2       | 38      | 2           |
| Sony                 | SVS1511V9RB                                      | 2012 | 2       | 38      | 2           |
| Hewlett-Packard      | EliteBook 8460w                                  | 2011 | 2       | 38      | 2           |
| Compal               | HGL31I                                           | 2007 | 2       | 38      | 2           |
| HUAWEI               | KLVL-WXXW                                        | 2021 | 2       | 37      | 2           |
| Lenovo               | ThinkPad L13 Gen 2                               | 2020 | 2       | 37      | 2           |
| Wortmann AG          | 1220579_1470096                                  | 2017 | 2       | 37      | 2           |
| MSI                  | GE72VR 6RF                                       | 2016 | 2       | 37      | 2           |
| Fujitsu              | LIFEBOOK S904                                    | 2014 | 2       | 37      | 2           |
| ARCELIK              | GNB 15xx B1 Serisi                               | 2014 | 2       | 37      | 2           |
| Sony                 | VPCSB15GB                                        | 2012 | 2       | 37      | 2           |
| Sony                 | VPCSA3AFX                                        | 2011 | 2       | 37      | 2           |
| Fujitsu              | LIFEBOOK T731                                    | 2011 | 2       | 37      | 2           |
| Clevo                | M660SR                                           | 2007 | 2       | 37      | 2           |
| Acer                 | TravelMate 4230                                  | 2006 | 2       | 37      | 2           |
| Acer                 | Extensa 215-52                                   | 2020 | 2       | 36      | 2           |
| Gigabyte Technology  | AERO 15-XA                                       | 2019 | 2       | 36      | 2           |
| Acer                 | TravelMate P653-M                                | 2012 | 2       | 36      | 2           |
| Acer                 | Aspire 5710Z                                     | 2007 | 2       | 36      | 2           |
| Lenovo               | 20208                                            | 2014 | 2       | 35      | 2           |
| ASUSTek Computer     | VivoBook 17_ASUS Laptop X705MA_F705MA            | 2018 | 2       | 34      | 2           |
| Toshiba              | PORTEGE Z30-C                                    | 2016 | 2       | 34      | 2           |
| Lenovo               | HuronRiver Platform                              | 2011 | 2       | 34      | 2           |
| Clevo                | M540SR                                           | 2007 | 2       | 34      | 2           |
| Dell                 | Venue 11 Pro 7139                                | 2014 | 2       | 33      | 2           |
| Sony                 | SVF1521A7EB                                      | 2013 | 2       | 33      | 2           |
| ASUSTek Computer     | X750LB                                           | 2013 | 2       | 33      | 2           |
| Medion               | S15449                                           | 2021 | 2       | 32      | 2           |
| Notebook             | NBxxTA                                           | 2019 | 2       | 32      | 2           |
| Acer                 | Aspire E5-532G                                   | 2015 | 2       | 32      | 2           |
| ASUSTek Computer     | Q551LB                                           | 2015 | 2       | 32      | 2           |
| Fujitsu              | LIFEBOOK T734                                    | 2013 | 2       | 32      | 2           |
| MSI                  | MS-N081                                          | 2011 | 2       | 32      | 2           |
| Dell                 | Latitude D410                                    | 2005 | 2       | 32      | 2           |
| Lenovo               | IdeaPad 310-15IAP 80TT                           | 2017 | 2       | 31      | 2           |
| Toshiba              | Satellite M115                                   | 2008 | 2       | 31      | 2           |
| Acer                 | Aspire 9500                                      | 2005 | 2       | 31      | 2           |
| Hewlett-Packard      | Mini 5101                                        | 2009 | 2       | 30      | 2           |
| Toshiba              | Satellite L10                                    | 2008 | 2       | 30      | 2           |
| Acer                 | TravelMate P446-M                                | 2014 | 2       | 29      | 2           |
| Standard             | MT40II                                           | 2012 | 2       | 29      | 2           |
| Phoenix/SiS          | M7X0SU                                           |      | 2       | 29      | 2           |
| ASUSTek Computer     | F81Se                                            | 2009 | 2       | 28      | 2           |
| Acer                 | Aspire 3500                                      | 2005 | 2       | 22      | 2           |
| Hewlett-Packard      | Pavilion dv6                                     | 2008 | 392     | 61      | 3           |
| Hewlett-Packard      | Pavilion g6                                      | 2011 | 387     | 47      | 3           |
| Lenovo               | ThinkPad T420                                    | 2011 | 180     | 43      | 3           |
| Dell                 | Latitude E6430                                   | 2012 | 148     | 47      | 3           |
| Lenovo               | ThinkPad T440p                                   | 2013 | 124     | 44      | 3           |
| Dell                 | XPS 15 9570                                      | 2018 | 122     | 64      | 3           |
| Lenovo               | ThinkPad T410                                    | 2010 | 120     | 49      | 3           |
| Hewlett-Packard      | EliteBook 8460p                                  | 2011 | 114     | 43      | 3           |
| Lenovo               | ThinkPad X1 Carbon 7th                           | 2019 | 112     | 56      | 3           |
| Dell                 | Latitude D630                                    | 2007 | 104     | 43      | 3           |
| Dell                 | XPS 15 9560                                      | 2017 | 101     | 64      | 3           |
| Hewlett-Packard      | EliteBook 840 G3                                 | 2016 | 98      | 40      | 3           |
| Lenovo               | ThinkPad T400                                    | 2008 | 96      | 52      | 3           |
| Dell                 | XPS 15 9500                                      | 2020 | 94      | 62      | 3           |
| Hewlett-Packard      | EliteBook 840 G1                                 | 2013 | 92      | 43      | 3           |
| Lenovo               | G570 20079                                       | 2011 | 92      | 35      | 3           |
| Dell                 | Latitude E7440                                   | 2014 | 90      | 45      | 3           |
| Lenovo               | ThinkPad T61                                     | 2007 | 87      | 54      | 3           |
| Lenovo               | ThinkPad X240                                    | 2013 | 87      | 40      | 3           |
| HUAWEI               | NBLK-WAX9X                                       | 2019 | 85      | 46      | 3           |
| Lenovo               | ThinkPad T460s                                   | 2016 | 78      | 48      | 3           |
| Dell                 | XPS 13 9310                                      | 2020 | 77      | 59      | 3           |
| Lenovo               | ThinkPad T440s                                   | 2014 | 76      | 43      | 3           |
| Lenovo               | ThinkPad T480s                                   | 2018 | 72      | 58      | 3           |
| Dell                 | Latitude E6540                                   | 2013 | 71      | 51      | 3           |
| Dell                 | Latitude E6520                                   | 2011 | 69      | 44      | 3           |
| ASUSTek Computer     | X550CC                                           | 2013 | 66      | 43      | 3           |
| Hewlett-Packard      | 255 G7 Notebook PC                               | 2018 | 63      | 51      | 3           |
| Lenovo               | ThinkPad W530                                    | 2012 | 62      | 51      | 3           |
| Hewlett-Packard      | Pavilion Gaming Laptop 15-cx0xxx                 | 2018 | 60      | 48      | 3           |
| Lenovo               | IdeaPad 5 15ARE05 81YQ                           | 2020 | 60      | 45      | 3           |
| Lenovo               | ThinkPad E15 Gen 2                               | 2020 | 59      | 52      | 3           |
| Lenovo               | ThinkPad E15                                     | 2019 | 58      | 45      | 3           |
| Lenovo               | ThinkPad P50                                     | 2015 | 56      | 56      | 3           |
| Lenovo               | ThinkPad T500                                    | 2008 | 56      | 52      | 3           |
| Hewlett-Packard      | ENVY 15                                          | 2010 | 56      | 50      | 3           |
| Apple                | MacBookPro8,2                                    | 2012 | 52      | 43      | 3           |
| ASUSTek Computer     | ZenBook UX431DA_UM431DA                          | 2019 | 51      | 48      | 3           |
| Lenovo               | ThinkPad T450                                    | 2015 | 51      | 41      | 3           |
| Hewlett-Packard      | ProBook 4530s                                    | 2011 | 51      | 40      | 3           |
| ASUSTek Computer     | TUF Gaming FX505DT_FX505DT                       | 2019 | 50      | 46      | 3           |
| Lenovo               | Legion 5 15ARH05 82B5                            | 2020 | 49      | 52      | 3           |
| Lenovo               | ThinkPad T60                                     | 2006 | 48      | 40      | 3           |
| Dell                 | Latitude E5520                                   | 2011 | 45      | 44      | 3           |
| Hewlett-Packard      | Pavilion dv6700                                  | 2007 | 44      | 42      | 3           |
| Lenovo               | ThinkPad W540                                    | 2014 | 43      | 53      | 3           |
| Dell                 | Precision M4600                                  | 2011 | 43      | 51      | 3           |
| Dell                 | Latitude E5570                                   | 2016 | 43      | 50      | 3           |
| Dell                 | Vostro 3550                                      | 2011 | 43      | 43      | 3           |
| Dell                 | Latitude E5420                                   | 2011 | 42      | 38      | 3           |
| Lenovo               | ThinkPad P14s Gen 1                              | 2020 | 41      | 64      | 3           |
| Hewlett-Packard      | Pavilion Laptop 15-cw1xxx                        | 2019 | 41      | 48      | 3           |
| Hewlett-Packard      | OMEN by HP Laptop                                | 2016 | 41      | 46      | 3           |
| Dell                 | Inspiron 3537                                    | 2013 | 41      | 41      | 3           |
| Timi                 | TM1701                                           | 2017 | 39      | 48      | 3           |
| Hewlett-Packard      | Laptop 15-db1xxx                                 | 2019 | 39      | 47      | 3           |
| Hewlett-Packard      | Pavilion Laptop 15-cw0xxx                        | 2018 | 39      | 45      | 3           |
| Dell                 | Inspiron 1501                                    | 2006 | 39      | 41      | 3           |
| Lenovo               | ThinkPad T420s                                   | 2011 | 38      | 53      | 3           |
| Hewlett-Packard      | ENVY 17                                          | 2011 | 38      | 53      | 3           |
| Dell                 | Vostro 3500                                      | 2010 | 37      | 44      | 3           |
| Lenovo               | ThinkPad T540p                                   | 2013 | 36      | 44      | 3           |
| Hewlett-Packard      | Laptop 15-da1xxx                                 | 2018 | 36      | 35      | 3           |
| ASUSTek Computer     | ROG Zephyrus G14 GA401IV_GA401IV                 | 2020 | 35      | 57      | 3           |
| Lenovo               | ThinkPad E495                                    | 2019 | 34      | 53      | 3           |
| ASUSTek Computer     | X555LJ                                           | 2015 | 34      | 40      | 3           |
| Acer                 | Nitro AN515-51                                   | 2017 | 34      | 39      | 3           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512DA_X512DA                | 2019 | 33      | 51      | 3           |
| Dell                 | Inspiron 3593                                    | 2019 | 33      | 44      | 3           |
| Lenovo               | B560                                             | 2010 | 33      | 42      | 3           |
| Acer                 | Aspire 5100                                      | 2006 | 33      | 40      | 3           |
| Acer                 | Aspire 5560                                      | 2011 | 32      | 44      | 3           |
| ASUSTek Computer     | N53SV                                            | 2010 | 32      | 40      | 3           |
| Lenovo               | ThinkPad T590                                    | 2019 | 31      | 55      | 3           |
| Hewlett-Packard      | Laptop 17-ca0xxx                                 | 2018 | 31      | 49      | 3           |
| Hewlett-Packard      | ProBook 450 G1                                   | 2013 | 31      | 41      | 3           |
| ASUSTek Computer     | X555LD                                           | 2014 | 30      | 41      | 3           |
| Lenovo               | ThinkPad P1 Gen 2                                | 2019 | 29      | 64      | 3           |
| Lenovo               | ThinkPad L15 Gen 1                               | 2020 | 29      | 61      | 3           |
| Lenovo               | IdeaPad 3 15ADA05 81W1                           | 2020 | 29      | 48      | 3           |
| Hewlett-Packard      | Laptop 17-ca1xxx                                 | 2019 | 29      | 47      | 3           |
| Lenovo               | IdeaPad 310-15ISK 80SM                           | 2016 | 29      | 33      | 3           |
| Hewlett-Packard      | Laptop 15s-eq1xxx                                | 2020 | 28      | 68      | 3           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X509JA_X509JA                | 2019 | 28      | 35      | 3           |
| Lenovo               | IdeaPad L340-15API 81LW                          | 2019 | 27      | 50      | 3           |
| Hewlett-Packard      | ProBook 440 G7                                   | 2019 | 27      | 42      | 3           |
| Apple                | MacBookPro10,1                                   | 2015 | 27      | 42      | 3           |
| Lenovo               | ThinkPad X1 Carbon                               | 2012 | 27      | 34      | 3           |
| Hewlett-Packard      | Laptop 14-bs0xx                                  | 2017 | 25      | 57      | 3           |
| ASUSTek Computer     | S551LN                                           | 2014 | 25      | 41      | 3           |
| Hewlett-Packard      | Pavilion Laptop 15-cs0xxx                        | 2018 | 24      | 52      | 3           |
| Dell                 | G5 5505                                          | 2020 | 23      | 64      | 3           |
| Lenovo               | ThinkPad P1 Gen 3                                | 2020 | 23      | 56      | 3           |
| Lenovo               | ThinkPad X1 Carbon Gen 9                         | 2021 | 23      | 54      | 3           |
| Hewlett-Packard      | ProBook 445 G7                                   | 2020 | 23      | 46      | 3           |
| Acer                 | Aspire A315-23                                   | 2020 | 23      | 45      | 3           |
| Dell                 | Latitude 5501                                    | 2019 | 22      | 61      | 3           |
| Dell                 | Precision M6800                                  | 2013 | 22      | 50      | 3           |
| Lenovo               | B50-45 20388                                     | 2014 | 22      | 41      | 3           |
| Hewlett-Packard      | EliteBook 850 G6                                 | 2019 | 21      | 55      | 3           |
| Hewlett-Packard      | EliteBook 850 G5                                 | 2018 | 21      | 50      | 3           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X512DA_F512DA                | 2019 | 21      | 49      | 3           |
| Dell                 | Latitude 3400                                    | 2019 | 21      | 46      | 3           |
| Dell                 | Latitude 5520                                    | 2020 | 20      | 54      | 3           |
| Lenovo               | ThinkPad T460p                                   | 2016 | 20      | 46      | 3           |
| Lenovo               | ThinkPad T410s                                   | 2010 | 20      | 46      | 3           |
| Hewlett-Packard      | Pavilion 11 x360 PC                              | 2014 | 20      | 33      | 3           |
| Lenovo               | ThinkPad E15 Gen 3                               | 2021 | 19      | 49      | 3           |
| Lenovo               | ThinkPad W541                                    | 2015 | 19      | 48      | 3           |
| Hewlett-Packard      | ProBook 430 G5                                   | 2017 | 19      | 46      | 3           |
| Dell                 | Inspiron 3793                                    | 2019 | 18      | 45      | 3           |
| Hewlett-Packard      | ENVY dv7                                         | 2012 | 18      | 41      | 3           |
| Hewlett-Packard      | ProBook 430 G1                                   | 2013 | 18      | 37      | 3           |
| Lenovo               | ThinkPad X390                                    | 2019 | 17      | 52      | 3           |
| Apple                | MacBookPro15,1                                   | 2012 | 17      | 52      | 3           |
| ASUSTek Computer     | Strix 17 GL703GE                                 | 2018 | 17      | 50      | 3           |
| ASUSTek Computer     | N750JV                                           | 2013 | 17      | 42      | 3           |
| Dell                 | Vostro 3460                                      | 2012 | 17      | 39      | 3           |
| Lenovo               | ThinkPad L490                                    | 2019 | 16      | 55      | 3           |
| Lenovo               | ThinkPad P52s                                    | 2018 | 16      | 55      | 3           |
| Lenovo               | ThinkPad T15 Gen 1                               | 2020 | 16      | 53      | 3           |
| Lenovo               | IdeaPad C340-14API 81N6                          | 2019 | 16      | 51      | 3           |
| Hewlett-Packard      | Pavilion Gaming Laptop 17-cd0xxx                 | 2019 | 16      | 46      | 3           |
| Acer                 | Aspire 5630                                      | 2006 | 16      | 41      | 3           |
| Hewlett-Packard      | ProBook 450 G0                                   | 2013 | 16      | 36      | 3           |
| Lenovo               | ThinkPad T495s                                   | 2019 | 15      | 58      | 3           |
| Hewlett-Packard      | Pavilion Gaming Laptop 15-ec2xxx                 | 2021 | 15      | 49      | 3           |
| Acer                 | Aspire A515-52                                   | 2018 | 15      | 43      | 3           |
| Toshiba              | Satellite P300                                   | 2008 | 15      | 42      | 3           |
| Hewlett-Packard      | ProBook 650 G2                                   | 2016 | 15      | 40      | 3           |
| Lenovo               | ThinkPad X280                                    | 2018 | 14      | 46      | 3           |
| ASUSTek Computer     | X555UB                                           | 2015 | 14      | 38      | 3           |
| Acer                 | Aspire 5310                                      | 2007 | 14      | 38      | 3           |
| Hewlett-Packard      | ZBook 15 G5                                      | 2018 | 13      | 63      | 3           |
| ASUSTek Computer     | X550ZE                                           | 2014 | 13      | 49      | 3           |
| Hewlett-Packard      | Compaq nc6400                                    | 2006 | 13      | 40      | 3           |
| Lenovo               | IdeaPad 3 14ADA05 81W0                           | 2020 | 12      | 46      | 3           |
| Lenovo               | IdeaPad 510-15IKB 80SV                           | 2016 | 12      | 39      | 3           |
| Dell                 | Vostro 3450                                      | 2011 | 12      | 37      | 3           |
| Hewlett-Packard      | Elite x2 1012 G1                                 | 2016 | 11      | 59      | 3           |
| Hewlett-Packard      | ENVY TS 15                                       | 2013 | 11      | 42      | 3           |
| Timi                 | TM1604                                           | 2017 | 11      | 41      | 3           |
| Acer                 | Aspire 3690                                      | 2006 | 11      | 40      | 3           |
| Hewlett-Packard      | Pavilion dv8                                     | 2009 | 10      | 64      | 3           |
| Dell                 | Precision 5560                                   | 2021 | 10      | 59      | 3           |
| Hewlett-Packard      | EliteBook 8730w                                  | 2008 | 10      | 51      | 3           |
| Lenovo               | IdeaPad 5 Pro 16ACH6 82L5                        | 2021 | 10      | 47      | 3           |
| Acer                 | Nitro AN515-45                                   | 2021 | 10      | 47      | 3           |
| Hewlett-Packard      | Pavilion dm3                                     | 2009 | 10      | 38      | 3           |
| Lenovo               | IdeaPad 530S-15IKB 81EV                          | 2018 | 10      | 36      | 3           |
| ASUSTek Computer     | UX550VE                                          | 2017 | 9       | 50      | 3           |
| Acer                 | Aspire 6935                                      | 2008 | 9       | 45      | 3           |
| ASUSTek Computer     | N751JK                                           | 2014 | 9       | 39      | 3           |
| Hewlett-Packard      | ENVY Laptop 13-ad1xx                             | 2017 | 9       | 38      | 3           |
| Lenovo               | ThinkPad P43s                                    | 2019 | 8       | 56      | 3           |
| Hewlett-Packard      | ZBook 17 G6                                      | 2019 | 8       | 54      | 3           |
| Lenovo               | ThinkPad L590                                    | 2019 | 8       | 51      | 3           |
| Hewlett-Packard      | Laptop 15-dw2xxx                                 | 2020 | 8       | 42      | 3           |
| Notebook             | WA50SRQ                                          | 2013 | 8       | 41      | 3           |
| Lenovo               | V560                                             | 2010 | 8       | 41      | 3           |
| Lenovo               | ThinkPad T570                                    | 2017 | 8       | 40      | 3           |
| ASUSTek Computer     | TP500LN                                          | 2014 | 8       | 40      | 3           |
| Dell                 | Latitude D820                                    | 2008 | 8       | 40      | 3           |
| Acer                 | TravelMate 2490                                  | 2006 | 8       | 40      | 3           |
| Lenovo               | ThinkPad L13                                     | 2019 | 8       | 38      | 3           |
| MSI                  | PS42 8RB                                         | 2018 | 8       | 34      | 3           |
| Lenovo               | IdeaPad 130-15IKB 81H7                           | 2018 | 8       | 34      | 3           |
| Lenovo               | ThinkBook 16p Gen 2 20YM                         | 2021 | 7       | 60      | 3           |
| Lenovo               | ThinkPad P15v Gen 1                              | 2020 | 7       | 60      | 3           |
| MSI                  | Prestige 15 A11SCS                               | 2020 | 7       | 49      | 3           |
| MSI                  | GF65 Thin 10UE                                   | 2020 | 7       | 47      | 3           |
| Lenovo               | IdeaPad 3 17ADA05 81W2                           | 2020 | 7       | 45      | 3           |
| Lenovo               | Legion 5 15IMH05 82AU                            | 2020 | 7       | 44      | 3           |
| Fujitsu Siemens      | LIFEBOOK S7220                                   | 2009 | 7       | 43      | 3           |
| ASUSTek Computer     | ASUS TUF Gaming F15 FX506LH_FX506LH              | 2021 | 7       | 39      | 3           |
| Lenovo               | ThinkPad P14s Gen 2a                             | 2021 | 6       | 52      | 3           |
| Hewlett-Packard      | Pavilion tx1000                                  | 2007 | 6       | 46      | 3           |
| Lenovo               | IdeaPad 3 17ARE05 81W5                           | 2020 | 6       | 43      | 3           |
| Fujitsu              | LIFEBOOK T901                                    | 2011 | 6       | 40      | 3           |
| Timi                 | TM1707                                           | 2018 | 6       | 39      | 3           |
| Acer                 | Aspire 5680                                      | 2007 | 6       | 39      | 3           |
| Toshiba              | PORTEGE Z30-A                                    | 2014 | 6       | 35      | 3           |
| Acer                 | Aspire V5-431                                    | 2012 | 6       | 33      | 3           |
| ASUSTek Computer     | X550MD                                           | 2014 | 6       | 32      | 3           |
| Lenovo               | ThinkPad X1 Nano Gen 1                           | 2020 | 5       | 56      | 3           |
| Lenovo               | ThinkPad P15 Gen 1                               | 2020 | 5       | 56      | 3           |
| Lenovo               | ThinkPad T15g Gen 1                              | 2020 | 5       | 54      | 3           |
| Lenovo               | ThinkPad P70                                     | 2018 | 5       | 54      | 3           |
| Lenovo               | IdeaPad Gaming 3 15ACH6 82K2                     | 2021 | 5       | 51      | 3           |
| ASUSTek Computer     | ASUS TUF Dash F15 FX516PM_FX516PM                | 2021 | 5       | 49      | 3           |
| MOTILE               | M142                                             | 2019 | 5       | 49      | 3           |
| Hewlett-Packard      | Laptop 15q-dy0xxx                                | 2018 | 5       | 43      | 3           |
| Fujitsu              | LIFEBOOK T730                                    | 2010 | 5       | 43      | 3           |
| Acer                 | TravelMate 6292                                  | 2007 | 5       | 43      | 3           |
| Acer                 | Aspire 3050                                      | 2007 | 5       | 40      | 3           |
| Acer                 | Aspire 5943G                                     | 2010 | 4       | 57      | 3           |
| ASUSTek Computer     | ROG Strix G533QR_G533QR                          | 2020 | 4       | 55      | 3           |
| Hewlett-Packard      | ZBook Fury 15 G7 Mobile Workstation              | 2021 | 4       | 54      | 3           |
| Timi                 | Mi Laptop Pro 15                                 | 2019 | 4       | 50      | 3           |
| Lenovo               | ThinkPad P53s                                    | 2019 | 4       | 45      | 3           |
| Fujitsu Siemens      | CELSIUS H270                                     | 2008 | 4       | 45      | 3           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X412DA_A412DA                | 2019 | 4       | 43      | 3           |
| Lenovo               | ThinkPad 25                                      | 2017 | 4       | 41      | 3           |
| Acer                 | Aspire 5710                                      | 2007 | 4       | 36      | 3           |
| Hewlett-Packard      | 240 G7                                           | 2020 | 4       | 34      | 3           |
| Phoenix/SiS          | M720SR                                           | 2008 | 4       | 31      | 3           |
| Dell                 | Precision 7560                                   | 2021 | 3       | 59      | 3           |
| Lenovo               | ThinkPad L14 Gen 2a                              | 2021 | 3       | 52      | 3           |
| ASUSTek Computer     | ROG Strix G513QM_G513QM                          | 2021 | 3       | 51      | 3           |
| Lenovo               | ThinkPad Edge E535                               | 2012 | 3       | 48      | 3           |
| Acer                 | TravelMate 6593                                  | 2008 | 3       | 48      | 3           |
| Lenovo               | ThinkPad P15s Gen 2i                             | 2021 | 3       | 46      | 3           |
| ASUSTek Computer     | VivoBook_ASUSLaptop X712DA_M712DA                | 2019 | 3       | 46      | 3           |
| Hewlett-Packard      | Victus by Laptop 16-e0xxx                        | 2021 | 3       | 44      | 3           |
| MSI                  | PR200                                            | 2007 | 3       | 43      | 3           |
| MSI                  | Katana GF66 11UC                                 | 2021 | 3       | 42      | 3           |
| Toshiba              | TECRA R840                                       | 2011 | 3       | 42      | 3           |
| Lenovo               | IdeaPad 3 14ARE05 81W3                           | 2020 | 3       | 41      | 3           |
| Compal               | HEL80I                                           | 2007 | 3       | 40      | 3           |
| ASUSTek Computer     | BU201LA                                          | 2014 | 3       | 39      | 3           |
| Lenovo               | XiaoXinPro-13IML 2019 81XB                       | 2019 | 3       | 38      | 3           |
| LG Electronics       | 17Z990-R.AAS8U1                                  | 2019 | 3       | 38      | 3           |
| Toshiba              | TECRA M11                                        | 2010 | 3       | 38      | 3           |
| ASUSTek Computer     | P2440UQ                                          | 2017 | 3       | 37      | 3           |
| Acer                 | Extensa 5510                                     | 2007 | 3       | 36      | 3           |
| Chuwi                | HeroBox Pro                                      | 2021 | 3       | 31      | 3           |
| MSI                  | GE76 Raider 10UH                                 | 2021 | 2       | 50      | 3           |
| Acer                 | Nitro AN515-57                                   | 2021 | 2       | 48      | 3           |
| Lenovo               | ThinkBook 15 G3 ACL 21A4                         | 2021 | 2       | 47      | 3           |
| Hewlett-Packard      | Laptop 17-ca3xxx                                 | 2020 | 2       | 44      | 3           |
| Lenovo               | ThinkPad P14s Gen 2i                             | 2021 | 2       | 42      | 3           |
| Hewlett-Packard      | Compaq nc4400                                    | 2006 | 2       | 42      | 3           |
| Lenovo               | ThinkPad P50s                                    | 2016 | 2       | 41      | 3           |
| Fujitsu              | LIFEBOOK P770                                    | 2010 | 2       | 41      | 3           |
| Toshiba              | PORTEGE M800                                     | 2009 | 2       | 41      | 3           |
| Sony                 | VGN-SZ2XRP_C                                     | 2006 | 2       | 40      | 3           |
| TUXEDO               | P95_HR                                           | 2017 | 2       | 39      | 3           |
| ASUSTek Computer     | UX302LG                                          | 2013 | 2       | 38      | 3           |
| ASUSTek Computer     | BU403UA                                          | 2016 | 2       | 36      | 3           |
| Medion               | P6669 MD60147                                    | 2016 | 2       | 35      | 3           |
| Acer                 | Swift SF315-51                                   | 2017 | 2       | 34      | 3           |
| Toshiba              | TECRA A50-A                                      | 2014 | 2       | 34      | 3           |
| Toshiba              | Satellite M65                                    | 2005 | 2       | 34      | 3           |
| Hewlett-Packard      | Pavilion Notebook                                | 2015 | 188     | 44      | 4           |
| Lenovo               | ThinkPad X230                                    | 2012 | 182     | 41      | 4           |
| Dell                 | Inspiron 3542                                    | 2014 | 107     | 37      | 4           |
| Lenovo               | ThinkPad T14 Gen 1                               | 2020 | 100     | 61      | 4           |
| Lenovo               | ThinkPad E14                                     | 2019 | 92      | 49      | 4           |
| Hewlett-Packard      | ProBook 4540s                                    | 2012 | 80      | 39      | 4           |
| Acer                 | Aspire 5742G                                     | 2010 | 74      | 42      | 4           |
| Acer                 | Aspire E5-575G                                   | 2016 | 71      | 42      | 4           |
| Dell                 | Latitude E5470                                   | 2016 | 70      | 44      | 4           |
| Acer                 | Aspire E5-571G                                   | 2014 | 55      | 38      | 4           |
| Dell                 | Latitude E6320                                   | 2011 | 50      | 44      | 4           |
| Hewlett-Packard      | ProBook 450 G5                                   | 2017 | 48      | 50      | 4           |
| Dell                 | Inspiron 5566                                    | 2016 | 46      | 35      | 4           |
| Dell                 | Precision M4800                                  | 2013 | 44      | 57      | 4           |
| Hewlett-Packard      | ProBook 450 G3                                   | 2015 | 43      | 36      | 4           |
| Lenovo               | ThinkPad X1 Extreme                              | 2018 | 37      | 60      | 4           |
| Hewlett-Packard      | Pavilion Gaming Laptop 15-ec1xxx                 | 2020 | 37      | 48      | 4           |
| Lenovo               | ThinkPad X1 Carbon Gen 8                         | 2020 | 35      | 55      | 4           |
| Hewlett-Packard      | ProBook 455 G7                                   | 2020 | 30      | 57      | 4           |
| Lenovo               | ThinkPad T495                                    | 2019 | 29      | 62      | 4           |
| Dell                 | Precision 5520                                   | 2017 | 28      | 52      | 4           |
| Hewlett-Packard      | EliteBook 820 G3                                 | 2016 | 27      | 39      | 4           |
| Lenovo               | Legion Y540-15IRH 81SX                           | 2019 | 24      | 50      | 4           |
| Dell                 | XPS L702X                                        | 2011 | 24      | 42      | 4           |
| Lenovo               | IdeaPad FLEX-14API 81SS                          | 2019 | 23      | 55      | 4           |
| Acer                 | Aspire V3-571                                    | 2012 | 23      | 41      | 4           |
| Hewlett-Packard      | ZBook 15 G2                                      | 2015 | 22      | 46      | 4           |
| Acer                 | Aspire A715-75G                                  | 2020 | 22      | 42      | 4           |
| Dell                 | G3 3500                                          | 2020 | 21      | 58      | 4           |
| Lenovo               | ThinkPad E560                                    | 2015 | 21      | 35      | 4           |
| Hewlett-Packard      | 550                                              | 2008 | 20      | 35      | 4           |
| Lenovo               | ThinkPad X13 Gen 1                               | 2020 | 19      | 59      | 4           |
| Dell                 | Latitude 7280                                    | 2016 | 19      | 47      | 4           |
| Hewlett-Packard      | ProBook 430 G3                                   | 2015 | 19      | 35      | 4           |
| Dell                 | Precision 7550                                   | 2020 | 16      | 63      | 4           |
| Dell                 | Inspiron 3501                                    | 2020 | 16      | 40      | 4           |
| Hewlett-Packard      | EliteBook 830 G5                                 | 2018 | 15      | 47      | 4           |
| Hewlett-Packard      | ProBook 440 G3                                   | 2015 | 15      | 35      | 4           |
| Lenovo               | ThinkPad                                         | 2013 | 14      | 57      | 4           |
| ASUSTek Computer     | N501VW                                           | 2015 | 14      | 41      | 4           |
| Apple                | MacBookPro16,2                                   | 2020 | 12      | 56      | 4           |
| Acer                 | Swift SF114-34                                   | 2020 | 12      | 37      | 4           |
| ASUSTek Computer     | TUF Gaming FX505DU_FX505DU                       | 2019 | 11      | 48      | 4           |
| Hewlett-Packard      | ProBook 650 G5                                   | 2019 | 11      | 45      | 4           |
| ASUSTek Computer     | K55VJ                                            | 2012 | 10      | 42      | 4           |
| Lenovo               | ThinkPad L15 Gen 2                               | 2021 | 10      | 40      | 4           |
| Acer                 | Aspire VN7-571G                                  | 2014 | 10      | 38      | 4           |
| ASUSTek Computer     | F5SR                                             | 2008 | 9       | 29      | 4           |
| Lenovo               | ThinkPad T15 Gen 2i                              | 2021 | 8       | 60      | 4           |
| Dell                 | Precision 7740                                   | 2019 | 8       | 52      | 4           |
| ASUSTek Computer     | ASUS TUF Gaming A15 FA506IV_FA506IV              | 2020 | 8       | 48      | 4           |
| ASUSTek Computer     | ZenBook UX534FTC_UX534FT                         | 2019 | 8       | 43      | 4           |
| Toshiba              | Satellite M70                                    | 2005 | 8       | 38      | 4           |
| Apple                | MacBookPro15,2                                   | 2020 | 7       | 50      | 4           |
| Dell                 | Precision 3560                                   | 2021 | 6       | 52      | 4           |
| Apple                | MacBookPro14,2                                   | 2018 | 6       | 49      | 4           |
| Lenovo               | ThinkPad L14 Gen 2                               | 2020 | 6       | 48      | 4           |
| Hewlett-Packard      | EliteBook 830 G7 Notebook PC                     | 2020 | 6       | 46      | 4           |
| Lenovo               | ThinkPad T61p                                    | 2007 | 6       | 46      | 4           |
| Lenovo               | E31-80 80MX                                      | 2015 | 6       | 34      | 4           |
| Hewlett-Packard      | Laptop 15s-du1xxx                                | 2019 | 4       | 40      | 4           |
| Packard Bell         | EasyNote TV43HC                                  | 2012 | 4       | 36      | 4           |
| Lenovo               | ThinkPad L15 Gen 2a                              | 2021 | 3       | 54      | 4           |
| Dell                 | Vostro 14 5410                                   | 2021 | 3       | 43      | 4           |
| Lenovo               | XiaoXin-15ARE 2020 81YR                          | 2020 | 3       | 42      | 4           |
| Acer                 | Aspire A317-33                                   | 2021 | 3       | 38      | 4           |
| ASUSTek Computer     | G2SV                                             | 2008 | 2       | 49      | 4           |
| MSI                  | PR210                                            | 2007 | 2       | 45      | 4           |
| Apple                | MacBookPro15,4                                   | 2019 | 2       | 40      | 4           |
| Apple                | MacBookAir9,1                                    | 2020 | 2       | 38      | 4           |
| Synology             | DS1019+                                          | 2018 | 2       | 38      | 4           |
| Hewlett-Packard      | Pavilion dv7                                     | 2008 | 231     | 66      | 5           |
| Lenovo               | ThinkPad T430                                    | 2012 | 206     | 54      | 5           |
| Hewlett-Packard      | Pavilion g7                                      | 2011 | 131     | 49      | 5           |
| Lenovo               | G500 20236                                       | 2013 | 106     | 43      | 5           |
| Samsung Electronics  | 300E4C/300E5C/300E7C                             | 2012 | 90      | 35      | 5           |
| Lenovo               | ThinkPad X1 Carbon 6th                           | 2018 | 87      | 51      | 5           |
| Acer                 | Nitro AN515-52                                   | 2018 | 72      | 44      | 5           |
| Lenovo               | Legion Y530-15ICH 81FV                           | 2018 | 62      | 55      | 5           |
| Dell                 | Latitude E6510                                   | 2010 | 60      | 58      | 5           |
| Lenovo               | ThinkPad P53                                     | 2019 | 39      | 69      | 5           |
| Lenovo               | ThinkPad L14 Gen 1                               | 2020 | 39      | 60      | 5           |
| Lenovo               | IdeaPad Gaming 3 15ARH05 82EY                    | 2020 | 32      | 51      | 5           |
| MSI                  | Prestige 15 A10SC                                | 2019 | 25      | 51      | 5           |
| Hewlett-Packard      | 630                                              | 2011 | 22      | 40      | 5           |
| Lenovo               | ThinkPad X270                                    | 2017 | 21      | 43      | 5           |
| Lenovo               | ThinkBook 15 G2 ITL 20VE                         | 2020 | 18      | 49      | 5           |
| Lenovo               | ThinkPad T14 Gen 2i                              | 2021 | 16      | 50      | 5           |
| Lenovo               | ThinkPad X395                                    | 2019 | 14      | 54      | 5           |
| Apple                | MacBookPro8,3                                    | 2015 | 14      | 45      | 5           |
| Hewlett-Packard      | Pavilion Laptop 15-cs3xxx                        | 2019 | 14      | 39      | 5           |
| Dell                 | Latitude 7420                                    | 2020 | 13      | 62      | 5           |
| Fujitsu              | LIFEBOOK S752                                    | 2012 | 13      | 37      | 5           |
| Dell                 | Vostro 5581                                      | 2018 | 11      | 43      | 5           |
| Hewlett-Packard      | OMEN by HP Laptop 17-an0xx                       | 2017 | 8       | 39      | 5           |
| Acer                 | Aspire 8730                                      | 2008 | 7       | 39      | 5           |
| Razer                | Blade 15 Mid 2019-Base                           | 2019 | 5       | 54      | 5           |
| Acer                 | Aspire ES1-431                                   | 2015 | 4       | 29      | 5           |
| MSI                  | GL63 8SE                                         | 2018 | 3       | 42      | 5           |
| MSI                  | GV72 8RD                                         | 2018 | 2       | 38      | 5           |
| Dell                 | Latitude E6420                                   | 2011 | 169     | 45      | 6           |
| Hewlett-Packard      | Laptop 15-db0xxx                                 | 2018 | 105     | 46      | 6           |
| Dell                 | XPS 13 9360                                      | 2016 | 103     | 52      | 6           |
| Dell                 | Latitude E7470                                   | 2015 | 63      | 46      | 6           |
| Lenovo               | ThinkPad T14s Gen 1                              | 2020 | 54      | 59      | 6           |
| Lenovo               | Y50-70 20378                                     | 2014 | 49      | 53      | 6           |
| Lenovo               | ThinkPad X1 Extreme 2nd                          | 2019 | 40      | 61      | 6           |
| Hewlett-Packard      | Pavilion Gaming Laptop 16-a0xxx                  | 2020 | 8       | 48      | 6           |
| Lenovo               | ThinkPad P73                                     | 2019 | 7       | 59      | 6           |
| Fujitsu              | LIFEBOOK S751                                    | 2011 | 7       | 38      | 6           |
| ASUSTek Computer     | ZenBook UX333FN_UX333FN                          | 2018 | 6       | 41      | 6           |
| MSI                  | GS60 2PE                                         | 2014 | 3       | 35      | 6           |
| Hewlett-Packard      | EliteBook x360 1040 G6                           | 2019 | 2       | 42      | 6           |
| Dell                 | Latitude 5480                                    | 2017 | 62      | 44      | 7           |
| Hewlett-Packard      | EliteBook 2540p                                  | 2010 | 51      | 52      | 7           |
| Dell                 | Latitude 5400                                    | 2019 | 38      | 54      | 7           |
| Lenovo               | ThinkPad T490s                                   | 2019 | 34      | 52      | 7           |
| Lenovo               | ThinkPad L480                                    | 2018 | 25      | 49      | 7           |
| Dell                 | Latitude 5580                                    | 2017 | 24      | 47      | 7           |
| Dell                 | Latitude E5540                                   | 2014 | 23      | 42      | 7           |
| Lenovo               | G40-45 80E1                                      | 2014 | 21      | 40      | 7           |
| Dell                 | Precision M6700                                  | 2012 | 18      | 45      | 7           |
| MSI                  | Bravo 15 A4DDR                                   | 2020 | 10      | 52      | 7           |
| Acer                 | Aspire V5-561G                                   | 2013 | 9       | 35      | 7           |
| Lenovo               | ThinkPad T480                                    | 2018 | 112     | 59      | 8           |
| Dell                 | Latitude 7490                                    | 2018 | 75      | 52      | 8           |
| Dell                 | G3 3590                                          | 2019 | 67      | 53      | 8           |
| Lenovo               | ThinkPad T430s                                   | 2012 | 52      | 43      | 8           |
| Lenovo               | ThinkPad T490                                    | 2019 | 93      | 61      | 9           |
| Hewlett-Packard      | ZBook 15 G6                                      | 2019 | 22      | 66      | 9           |
| Dell                 | Latitude E5450                                   | 2014 | 37      | 43      | 10          |
| Lenovo               | ThinkPad P52                                     | 2018 | 29      | 59      | 10          |

By Vendor
---------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Samples | Devices | Non-working |
|----------------------|---------|---------|-------------|
| Lenovo               | 12881   | 36      | 1           |
| Hewlett-Packard      | 11799   | 36      | 1           |
| Dell                 | 10301   | 37      | 1           |
| ASUSTek Computer     | 8324    | 35      | 1           |
| Acer                 | 7150    | 35      | 1           |
| Toshiba              | 2300    | 35      | 1           |
| Samsung Electronics  | 2072    | 33      | 1           |
| Apple                | 1440    | 37      | 1           |
| Sony                 | 1221    | 37      | 1           |
| MSI                  | 1184    | 37      | 1           |
| Packard Bell         | 509     | 34      | 1           |
| Notebook             | 451     | 37      | 1           |
| Fujitsu              | 438     | 35      | 1           |
| HUAWEI               | 406     | 39      | 1           |
| Positivo             | 349     | 29      | 1           |
| Medion               | 315     | 35      | 1           |
| Fujitsu Siemens      | 297     | 35      | 1           |
| eMachines            | 260     | 35      | 1           |
| Google               | 255     | 28      | 1           |
| Clevo                | 205     | 35      | 1           |
| System76             | 187     | 40      | 1           |
| Timi                 | 187     | 36      | 1           |
| Alienware            | 184     | 41      | 1           |
| LG Electronics       | 174     | 35      | 1           |
| Gateway              | 131     | 34      | 1           |
| Panasonic            | 128     | 36      | 1           |
| TUXEDO               | 127     | 40      | 1           |
| Intel                | 117     | 30      | 1           |
| Pegatron             | 94      | 34      | 1           |
| Razer                | 92      | 39      | 1           |
| Gigabyte Technology  | 88      | 37      | 1           |
| Chuwi                | 76      | 42      | 1           |
| Itautec              | 65      | 35      | 1           |
| PC Specialist        | 64      | 40      | 1           |
| Quanta               | 56      | 36      | 1           |
| Semp Toshiba         | 55      | 35      | 1           |
| Digibras             | 54      | 29      | 1           |
| IBM                  | 48      | 31      | 1           |
| Compal               | 43      | 34      | 1           |
| DNS                  | 42      | 33      | 1           |
| Teclast              | 40      | 39      | 1           |
| Insyde               | 40      | 23      | 1           |
| Schenker             | 40      | 43      | 1           |
| Avell High Perfor... | 35      | 40      | 1           |
| AMI                  | 33      | 28      | 1           |
| NEC Computers        | 33      | 32      | 1           |
| TrekStor             | 32      | 37      | 1           |
| DEXP                 | 32      | 30      | 1           |
| Digma                | 30      | 25      | 1           |
| Wortmann AG          | 29      | 34      | 1           |
| Jumper               | 28      | 40      | 1           |
| SLIMBOOK             | 28      | 39      | 1           |
| Philco               | 25      | 36      | 0           |
| Prestigio            | 25      | 30      | 1           |
| Purism               | 24      | 31      | 1           |
| BANGHO               | 24      | 31      | 1           |
| Monster              | 24      | 38      | 1           |
| Compaq               | 23      | 33      | 0           |
| OEM                  | 22      | 25      | 1           |
| Framework            | 21      | 48      | 0           |
| Pine Microsystems    | 21      | 18      | 1           |
| GPU Company          | 20      | 45      | 1           |
| Exo                  | 20      | 32      | 1           |
| Multilaser           | 20      | 32      | 1           |
| Irbis                | 20      | 28      | 1           |
| Mediacom             | 20      | 29      | 1           |
| HASEE Computer       | 20      | 35      | 1           |
| Aquarius             | 19      | 31      | 1           |
| GPD                  | 18      | 39      | 0           |
| VIT                  | 17      | 28      | 1           |
| AVITA                | 17      | 38      | 1           |
| BenQ                 | 16      | 33      | 1           |
| Positivo Bahia - ... | 15      | 37      | 0           |
| Standard             | 15      | 28      | 1           |
| FUJITSU CLIENT CO... | 15      | 42      | 1           |
| ECS                  | 14      | 42      | 0           |
| Star Labs            | 14      | 37      | 0           |
| Maibenben            | 14      | 37      | 1           |
| Infomash             | 14      | 37      | 1           |
| Getac                | 14      | 35      | 1           |
| Eluktronics          | 13      | 48      | 1           |
| AZW                  | 13      | 38      | 1           |
| Hampoo               | 13      | 28      | 1           |
| Thomson              | 12      | 29      | 1           |
| Dynabook             | 12      | 34      | 1           |
| HONOR                | 12      | 35      | 1           |
| Complet              | 11      | 42      | 0           |
| Casper               | 11      | 34      | 1           |
| AXDIA International  | 11      | 31      | 1           |
| Intel Client Systems | 11      | 45      | 1           |
| Microtech            | 10      | 47      | 1           |
| MOTILE               | 10      | 42      | 1           |
| MECHREVO             | 10      | 38      | 1           |
| Entroware            | 10      | 34      | 1           |
| Advent               | 9       | 34      | 0           |
| ONE-NETBOOK TECHN... | 9       | 44      | 1           |
| EVOO                 | 9       | 40      | 1           |
| Haier                | 9       | 31      | 1           |
| BESSTAR Tech         | 9       | 28      | 1           |
| YJKC                 | 9       | 37      | 1           |
| Cube                 | 9       | 31      | 1           |
| Phoenix/SiS          | 9       | 27      | 2           |
| MouseComputer        | 8       | 32      | 1           |
| UMAX                 | 8       | 44      | 1           |
| Motion Computing     | 8       | 32      | 1           |
| EUROCOM              | 8       | 42      | 1           |
| HCL Infosystems L... | 7       | 36      | 1           |
| Hungaro Flotta Kft   | 7       | 35      | 1           |
| Linx                 | 7       | 22      | 1           |
| UNOWHY               | 7       | 48      | 1           |
| Shuttle              | 6       | 27      | 0           |
| CCE                  | 6       | 36      | 1           |
| Metabox              | 6       | 40      | 1           |
| Hometech             | 6       | 30      | 1           |
| ilife                | 6       | 21      | 1           |
| Minix                | 6       | 23      | 1           |
| Nuvision             | 6       | 45      | 1           |
| GEO                  | 5       | 46      | 0           |
| ViewSonic            | 5       | 33      | 0           |
| Coradir              | 5       | 32      | 0           |
| LNV                  | 5       | 28      | 0           |
| SECO                 | 5       | 27      | 0           |
| YiFang               | 5       | 22      | 0           |
| Ematic               | 5       | 24      | 1           |
| Insignia             | 5       | 23      | 1           |
| Fusion5              | 5       | 33      | 1           |
| Beelink              | 5       | 33      | 1           |
| Olivetti             | 5       | 32      | 1           |
| I-Life Digital Te... | 5       | 32      | 1           |
| IT Channel Pty       | 5       | 43      | 1           |
| ICL                  | 4       | 38      | 0           |
| IDEALMAX             | 4       | 37      | 0           |
| Dixonsxp             | 4       | 33      | 0           |
| 3Q                   | 4       | 33      | 0           |
| Hannspree            | 4       | 31      | 0           |
| Kiano                | 4       | 28      | 0           |
| Novastar             | 4       | 26      | 0           |
| VINGA                | 4       | 44      | 1           |
| CyberPowerPC         | 4       | 42      | 1           |
| MAXDATA              | 4       | 33      | 1           |
| Login Informatica    | 4       | 30      | 1           |
| Certified Systems... | 4       | 27      | 1           |
| LincPlus             | 4       | 49      | 1           |
| OEGStone             | 4       | 37      | 1           |
| SIEMENS              | 4       | 35      | 1           |
| TR                   | 4       | 34      | 1           |
| DEPO Computers       | 4       | 34      | 1           |
| ARCELIK              | 4       | 32      | 1           |
| HKC                  | 4       | 22      | 1           |
| XMG                  | 4       | 40      | 1           |
| Xplore               | 4       | 39      | 1           |
| Durabook             | 4       | 38      | 1           |
| Dream Machines       | 4       | 36      | 2           |
| CSL-Computer         | 3       | 46      | 0           |
| AWOW                 | 3       | 46      | 0           |
| Akstron              | 3       | 43      | 0           |
| Mustek               | 3       | 37      | 0           |
| TQ-Group             | 3       | 36      | 0           |
| realme               | 3       | 35      | 0           |
| Allview              | 3       | 35      | 0           |
| RM                   | 3       | 34      | 0           |
| iRU                  | 3       | 33      | 0           |
| Megaware             | 3       | 33      | 0           |
| Direkt-Tek           | 3       | 31      | 0           |
| MTC                  | 3       | 29      | 0           |
| AMD                  | 3       | 29      | 0           |
| Essentiel B          | 3       | 27      | 0           |
| Navigator            | 3       | 27      | 0           |
| NOBLEX               | 3       | 25      | 0           |
| Radxa                | 3       | 23      | 0           |
| LIVEFAN              | 3       | 22      | 0           |
| Foxconn              | 3       | 22      | 0           |
| Advantec             | 3       | 22      | 0           |
| Shenzhen Da&Fong ... | 3       | 19      | 0           |
| HANSUNG COMPUTER     | 3       | 42      | 1           |
| Itronix              | 3       | 35      | 1           |
| Bluechip Computer    | 3       | 28      | 1           |
| AXIOO                | 3       | 27      | 1           |
| ARIMA                | 3       | 36      | 1           |
| IGEL Technology      | 3       | 33      | 1           |
| Archos               | 3       | 21      | 1           |
| SCHNEIDER            | 3       | 36      | 1           |
| Novatech             | 3       | 33      | 1           |
| ONDA                 | 3       | 21      | 1           |
| Pixus                | 2       | 43      | 0           |
| KOGAN                | 2       | 43      | 0           |
| mPTech               | 2       | 42      | 0           |
| Qbex                 | 2       | 39      | 0           |
| Datto                | 2       | 39      | 0           |
| Daten Tecnologia     | 2       | 39      | 0           |
| LEADER               | 2       | 38      | 0           |
| speedmaster          | 2       | 36      | 0           |
| Aftershock           | 2       | 36      | 0           |
| PINNACLEMICRO        | 2       | 35      | 0           |
| CompuLab             | 2       | 35      | 0           |
| LDLC                 | 2       | 34      | 0           |
| TPVAOC               | 2       | 33      | 0           |
| Alcor                | 2       | 33      | 0           |
| BOX                  | 2       | 32      | 0           |
| Qilive               | 2       | 31      | 0           |
| IP3 Tech             | 2       | 31      | 0           |
| UNITCOM              | 2       | 31      | 0           |
| win element          | 2       | 30      | 0           |
| VIZIO                | 2       | 30      | 0           |
| ONKYO                | 2       | 30      | 0           |
| Biostar              | 2       | 30      | 0           |
| China                | 2       | 29      | 0           |
| Coconics Private ... | 2       | 28      | 0           |
| MLS                  | 2       | 26      | 0           |
| Tangent Computer     | 2       | 25      | 0           |
| Lex BayTrail         | 2       | 25      | 0           |
| Garbarino SAIC       | 2       | 25      | 0           |
| TECNO                | 2       | 24      | 0           |
| PEAQ                 | 2       | 22      | 0           |
| 4Good                | 2       | 22      | 0           |
| LAMINA               | 2       | 21      | 0           |
| Nokia                | 2       | 19      | 0           |
| MPMAN                | 2       | 19      | 0           |
| MODECOM              | 2       | 17      | 0           |
| Kruger&Matz          | 2       | 45      | 1           |
| SiComputer           | 2       | 41      | 1           |
| THUNDEROBOT          | 2       | 35      | 1           |
| 51nb                 | 2       | 30      | 1           |
| Olidata              | 2       | 30      | 1           |
| TaNix                | 2       | 25      | 1           |
| SHENZHEN X&F TECH... | 2       | 24      | 1           |
| Winnovo              | 2       | 23      | 1           |
| Info Quest Techno... | 2       | 23      | 1           |
| WS                   | 2       | 23      | 1           |
| CAPTIVA              | 2       | 23      | 1           |
| Oysters              | 2       | 22      | 1           |
| Aava Mobile Oy       | 2       | 20      | 1           |
| SANTECH              | 2       | 54      | 1           |
| Micro Electronics    | 2       | 47      | 1           |
| Terrans Force        | 2       | 46      | 1           |
| KREZ                 | 2       | 45      | 1           |
| OriginPC             | 2       | 42      | 1           |
| OBSIDIAN-PC          | 2       | 41      | 1           |
| CONNEX               | 2       | 37      | 1           |
| RO                   | 2       | 36      | 1           |
| Trenton Systems      | 2       | 36      | 1           |
| Carbon Systems       | 2       | 34      | 1           |
| MiTAC                | 2       | 33      | 1           |
| THD                  | 2       | 29      | 1           |
| TELECOMITALIA        | 2       | 25      | 1           |
| BBEN                 | 2       | 23      | 1           |
| HIGRADED             | 2       | 31      | 2           |
| Pendo Industries     | 2       | 21      | 2           |
| Synology             | 2       | 38      | 4           |

Best Linux Mini Pc
==================

This is a list of mini pcs with best Linux-compatibility in the terms of
maximal number of Linux-compatible devices on board and maximal overall
popularity of a model.

Everyone can contribute to this report by uploading probes of their computers
by the [hw-probe](https://github.com/linuxhw/hw-probe) tool:

    sudo -E hw-probe -all -upload

Contents
--------

1. [ By Model ](#by-model)
2. [ By Vendor ](#by-vendor)

By Model
--------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Model                                            | Year | Samples | Devices | Non-working |
|----------------------|--------------------------------------------------|------|---------|---------|-------------|
| Apple                | Macmini5,1                                       | 2012 | 20      | 42      | 0           |
| Intel Client Systems | NUC8i5BEK                                        | 2018 | 14      | 38      | 0           |
| ASUSTek Computer     | PN40                                             | 2018 | 11      | 29      | 0           |
| Intel                | NUC8i7HVK                                        | 2018 | 10      | 54      | 0           |
| Intel Client Systems | NUC7PJYH                                         | 2019 | 10      | 29      | 0           |
| Intel                | NUC7i5BNH                                        | 2017 | 9       | 37      | 0           |
| Intel                | NUC7PJYH                                         | 2018 | 8       | 31      | 0           |
| Intel Client Systems | NUC8i3BEK                                        | 2019 | 7       | 37      | 0           |
| Intel                | NUC7CJYH                                         | 2018 | 7       | 29      | 0           |
| Beelink              | BT3 Pro                                          | 2017 | 7       | 24      | 0           |
| Intel Client Systems | NUC10i3FNK                                       | 2020 | 6       | 43      | 0           |
| Intel                | NUC6i7KYB H90766-405                             | 2017 | 6       | 33      | 0           |
| Apple                | Macmini1,1                                       | 2006 | 6       | 33      | 0           |
| Hewlett-Packard      | EliteDesk 800 G3 DM 35W                          | 2017 | 6       | 26      | 0           |
| Prestigio            | PSB133S01ZFP                                     | 2017 | 5       | 46      | 0           |
| Intel Client Systems | NUC7i5BNH                                        | 2018 | 5       | 41      | 0           |
| Intel                | NUC7i3BNB J22859-303                             | 2017 | 5       | 40      | 0           |
| Intel                | NUC7i7BNH                                        | 2017 | 5       | 38      | 0           |
| ZOTAC                | ZBOX-CI620/CI640/CI660                           | 2018 | 5       | 37      | 0           |
| ZOTAC                | ZBOX-CI323NANO                                   | 2016 | 5       | 35      | 0           |
| Intel                | NUC5CPYB H61145-408                              | 2016 | 5       | 34      | 0           |
| Intel                | NUC7i7DNHE                                       | 2018 | 5       | 32      | 0           |
| Intel                | NUC5PPYB H76558-107                              | 2016 | 5       | 30      | 0           |
| Endless              | EC-200                                           | 2015 | 5       | 26      | 0           |
| Intel Client Systems | NUC8i7HNK                                        | 2018 | 4       | 43      | 0           |
| ZOTAC                | ZBOX-EN1070/1060,EN1070K/1060K                   | 2017 | 4       | 40      | 0           |
| Intel Client Systems | NUC11PAHi5                                       | 2020 | 4       | 37      | 0           |
| ZOTAC                | ZBOX-ID92/ZBOX-IQ01                              | 2013 | 4       | 36      | 0           |
| ZOTAC                | ZBOX-CI320NANO series                            | 2014 | 4       | 34      | 0           |
| Intel                | NUC5CPYB H61145-413                              | 2018 | 4       | 31      | 0           |
| AZW                  | U55                                              | 2018 | 4       | 31      | 0           |
| Intel                | NUC5PPYB H76558-106                              | 2016 | 4       | 30      | 0           |
| ZOTAC                | ZBOX-ID18                                        | 2013 | 4       | 29      | 0           |
| Intel                | NUC5i7RYB H73774-104                             | 2016 | 4       | 28      | 0           |
| Intel                | NUC7i5BNB J31144-303                             | 2018 | 3       | 38      | 0           |
| AZW                  | SEi                                              | 2020 | 3       | 36      | 0           |
| AZW                  | GK mini                                          | 2021 | 3       | 35      | 0           |
| Intel                | NUC7i5BNB J31144-304                             | 2017 | 3       | 35      | 0           |
| Intel                | NUC5i7RYB H73774-101                             | 2015 | 3       | 34      | 0           |
| Intel Client Systems | NUC8i7BEK                                        | 2019 | 3       | 31      | 0           |
| Intel                | NUC6i5SYB H81131-504                             | 2016 | 3       | 31      | 0           |
| Intel                | NUC7i5BNK                                        | 2017 | 3       | 30      | 0           |
| Intel                | NUC5i3RYB H41000-507                             | 2017 | 3       | 29      | 0           |
| Lenovo               | ThinkCentre M720q 10T700BGGE                     | 2019 | 3       | 28      | 0           |
| Intel                | NUC5PPYB H76558-109                              | 2018 | 3       | 28      | 0           |
| MSI                  | MS-B120                                          | 2015 | 3       | 25      | 0           |
| AWOW                 | NV41                                             | 2019 | 2       | 56      | 0           |
| ZOTAC                | ZBOX-AD04                                        | 2012 | 2       | 51      | 0           |
| Intel Client Systems | NUC11PHi7                                        | 2021 | 2       | 46      | 0           |
| Intel                | NUC8i7HNK                                        | 2019 | 2       | 46      | 0           |
| Intel Client Systems | NUC9i5QNX                                        | 2020 | 2       | 45      | 0           |
| ZOTAC                | ZBOX-EN72080V/EN72070V/EN52060V/EN51660T         | 2019 | 2       | 44      | 0           |
| ZOTAC                | ZBOX-CI329NANO                                   | 2019 | 2       | 42      | 0           |
| CompuLab             | fitlet2                                          | 2018 | 2       | 41      | 0           |
| Intel                | NUC6i7KYB H90766-402                             | 2016 | 2       | 40      | 0           |
| Apple                | Macmini5,3                                       | 2012 | 2       | 39      | 0           |
| Intel Client Systems | NUC8i5INH                                        | 2019 | 2       | 38      | 0           |
| Intel Client Systems | NUC7i7BNH                                        | 2019 | 2       | 38      | 0           |
| Hewlett-Packard      | t610 PLUS WW Thin Client                         | 2013 | 2       | 37      | 0           |
| Hewlett-Packard      | t820 Flexible Thin Client                        | 2013 | 2       | 36      | 0           |
| Intel Client Systems | NUC10i5FNK                                       | 2020 | 2       | 35      | 0           |
| Lenovo               | ThinkCentre M625q 10TF001NRU                     | 2019 | 2       | 35      | 0           |
| Intel                | NUC6i7KYB H90766-404                             | 2018 | 2       | 35      | 0           |
| AZW                  | U57                                              | 2020 | 2       | 34      | 0           |
| Intel Client Systems | NUC8i3CYS                                        | 2018 | 2       | 34      | 0           |
| Dell                 | Wyse 5070 Thin Client                            | 2018 | 2       | 34      | 0           |
| ASUSTek Computer     | PN60-R                                           | 2018 | 2       | 34      | 0           |
| Intel                | NUC7i3BNB J22859-302                             | 2017 | 2       | 34      | 0           |
| Intel                | NUC5PGYB H78167-102                              | 2017 | 2       | 33      | 0           |
| ZOTAC                | ZBOX-MA320                                       | 2014 | 2       | 33      | 0           |
| Intel                | NUC7i5BNHX                                       | 2020 | 2       | 32      | 0           |
| Intel Client Systems | NUC7i3BNH                                        | 2019 | 2       | 32      | 0           |
| ZOTAC                | ZBOX-ID84                                        | 2012 | 2       | 32      | 0           |
| Lenovo               | ThinkCentre M90n-1 11AH000UUS                    | 2019 | 2       | 31      | 0           |
| Intel Client Systems | NUC7i7DNKE                                       | 2019 | 2       | 31      | 0           |
| Intel Client Systems | NUC7i3DNHE                                       | 2020 | 2       | 30      | 0           |
| Hewlett-Packard      | ProDesk 400 G6 Desktop Mini PC                   | 2020 | 2       | 29      | 0           |
| Lenovo               | C20-00 F0BB00RMRK                                | 2016 | 2       | 29      | 0           |
| Intel                | NUC6i5SYB H81131-505                             | 2016 | 2       | 29      | 0           |
| Intel                | NUC6CAYB J23203-402                              | 2016 | 2       | 29      | 0           |
| Intel                | NUC5i5RYB H40999-502                             | 2015 | 2       | 29      | 0           |
| Intel                | NUC5i3MYBE H47781-202                            | 2014 | 2       | 29      | 0           |
| ASUSTek Computer     | MINIPC PN62S                                     | 2020 | 2       | 28      | 0           |
| Supermicro           | A1SAi                                            | 2014 | 2       | 28      | 0           |
| Supermicro           | SYS-5039MS-H12TRF                                | 2020 | 2       | 27      | 0           |
| ZOTAC                | ZBOX-BI324                                       | 2017 | 2       | 27      | 0           |
| Intel                | NUC6i3SYB H81132-503                             | 2017 | 2       | 26      | 0           |
| Intel                | NUC5i7RYB H73774-102                             | 2015 | 2       | 26      | 0           |
| Hewlett-Packard      | ProDesk 600 G3 DM                                | 2020 | 2       | 25      | 0           |
| Beelink              | SII                                              | 2018 | 2       | 25      | 0           |
| Intel                | NUC5i5RYB H40999-507                             | 2016 | 2       | 25      | 0           |
| BESSTAR Tech         | GN31                                             | 2019 | 2       | 24      | 0           |
| ASUSTek Computer     | MINIPC PN61                                      | 2019 | 2       | 24      | 0           |
| MiPi PC              | Mini PC                                          | 2015 | 2       | 22      | 0           |
| MODECOM              | FreeONE                                          | 2015 | 2       | 21      | 0           |
| HCD                  | G2 Mini PC                                       | 2015 | 2       | 20      | 0           |
| RCA                  | W101 V2                                          | 2015 | 2       | 16      | 0           |
| Intel Client Systems | NUC8i7BEH                                        | 2018 | 40      | 48      | 1           |
| Intel Client Systems | NUC8i3BEH                                        | 2018 | 33      | 40      | 1           |
| Apple                | Macmini3,1                                       | 2008 | 21      | 42      | 1           |
| Intel Client Systems | NUC8i7HVK                                        | 2018 | 20      | 54      | 1           |
| Apple                | Macmini6,2                                       | 2015 | 19      | 42      | 1           |
| ASUSTek Computer     | MINIPC PN50                                      | 2020 | 18      | 49      | 1           |
| Intel                | NUC7i3BNH                                        | 2017 | 16      | 35      | 1           |
| Intel Client Systems | NUC10i5FNH                                       | 2020 | 15      | 43      | 1           |
| Apple                | Macmini5,2                                       | 2011 | 14      | 47      | 1           |
| Apple                | Macmini4,1                                       | 2010 | 14      | 44      | 1           |
| Hewlett-Packard      | t610 WW Thin Client                              | 2012 | 11      | 38      | 1           |
| Apple                | Macmini2,1                                       | 2007 | 10      | 33      | 1           |
| Intel                | NUC5i3RYB H41000-502                             | 2015 | 9       | 36      | 1           |
| Intel Client Systems | NUC7CJYH                                         | 2018 | 8       | 29      | 1           |
| Compulab             | fitlet2                                          | 2018 | 7       | 48      | 1           |
| Hewlett-Packard      | t510 Thin Client                                 | 2012 | 7       | 36      | 1           |
| Hewlett-Packard      | t630 Thin Client                                 | 2017 | 5       | 35      | 1           |
| BESSTAR Tech         | Z83-F                                            | 2017 | 5       | 25      | 1           |
| Prestigio            | PSB116A                                          | 2016 | 5       | 23      | 1           |
| Intel Client Systems | NUC11PAHi7                                       | 2020 | 4       | 40      | 1           |
| Hewlett-Packard      | t530 Thin Client                                 | 2017 | 4       | 39      | 1           |
| Hewlett-Packard      | Z2 Mini G3 Workstation                           | 2017 | 4       | 36      | 1           |
| Intel                | NUC7i3BNK                                        | 2020 | 4       | 34      | 1           |
| Hewlett-Packard      | 260 G3 DM                                        | 2019 | 4       | 30      | 1           |
| Hewlett-Packard      | ProDesk 400 G3 DM                                | 2017 | 4       | 26      | 1           |
| Intel Client Systems | NUC9i9QNX                                        | 2019 | 3       | 49      | 1           |
| ZOTAC                | ZBOXNANO-AD10                                    | 2011 | 3       | 40      | 1           |
| ASUSTek Computer     | MINIPC PN50-E1                                   | 2021 | 3       | 38      | 1           |
| Hewlett-Packard      | Z2 Mini G4 Workstation                           | 2019 | 3       | 34      | 1           |
| Intel                | NUC6i3SYB H81132-505                             | 2018 | 3       | 26      | 1           |
| Beelink              | AP34                                             | 2017 | 2       | 50      | 1           |
| KOGAN                | KAMPE300XA                                       | 2018 | 2       | 48      | 1           |
| Chuwi                | NA14                                             | 2017 | 2       | 46      | 1           |
| Acute angle          | AA-B4                                            | 2018 | 2       | 45      | 1           |
| ZOTAC                | B410                                             | 2020 | 2       | 44      | 1           |
| ASUSTek Computer     | MINIPC PN51-E1                                   | 2021 | 2       | 38      | 1           |
| ZOTAC                | ZBOX-ECM73070C/53060C                            | 2021 | 2       | 36      | 1           |
| ASUSTek Computer     | MINIPC PN62                                      | 2019 | 2       | 36      | 1           |
| CompuLab             | Intense-PC                                       | 2013 | 2       | 36      | 1           |
| ZOTAC                | ZBOX-ID88/ID89/ID90                              | 2013 | 2       | 35      | 1           |
| Intel                | NUC7i7BNB J31145-303                             | 2019 | 2       | 34      | 1           |
| ZOTAC                | ZBOX-MI551/BOX-MI571                             | 2015 | 2       | 30      | 1           |
| Intel                | NUC5i5RYB H40999-503                             | 2015 | 2       | 30      | 1           |
| Intel                | D425KT AAE93083-401                              | 2011 | 2       | 28      | 1           |
| Intel Client Systems | NUC8CCHK                                         | 2019 | 2       | 25      | 1           |
| CSL-Computer         | Mini-PC on a Stick                               | 2015 | 2       | 24      | 1           |
| AMI                  | Aptio CRB                                        | 2014 | 69      | 38      | 2           |
| Apple                | Macmini7,1                                       | 2015 | 34      | 43      | 2           |
| Apple                | Macmini6,1                                       | 2012 | 25      | 47      | 2           |
| Intel Client Systems | NUC6CAYH                                         | 2018 | 16      | 35      | 2           |
| Intel                | NUC6i7KYB H90766-406                             | 2016 | 15      | 38      | 2           |
| System76             | Meerkat                                          | 2017 | 10      | 48      | 2           |
| Apple                | Macmini8,1                                       | 2020 | 8       | 48      | 2           |
| Intel                | NUC6CAYH                                         | 2017 | 8       | 40      | 2           |
| Intel                | NUC5CPYB H61145-404                              | 2015 | 7       | 30      | 2           |
| ZOTAC                | ZBOX-CI327NANO-GS-01                             | 2017 | 6       | 32      | 2           |
| AWOW                 | PC BOX                                           | 2019 | 5       | 30      | 2           |
| Intel                | NUC6CAYB J23203-403                              | 2017 | 4       | 37      | 2           |
| Intel Client Systems | NUC9VXQNX                                        | 2020 | 2       | 51      | 2           |
| Hewlett-Packard      | EliteDesk 805 G6 Desktop Mini PC                 | 2020 | 2       | 44      | 2           |
| Intel                | NUC6CAYS                                         | 2018 | 2       | 39      | 2           |
| ZOTAC                | ZBOXNANO-ID62                                    | 2014 | 2       | 29      | 2           |
| Hewlett-Packard      | EliteDesk 800 G6 Desktop Mini PC                 | 2020 | 2       | 27      | 2           |
| Intel Client Systems | NUC10i7FNH                                       | 2019 | 30      | 46      | 3           |
| Hewlett-Packard      | ProDesk 405 G6 Desktop Mini PC                   | 2020 | 11      | 51      | 3           |
| Intel Client Systems | NUC10i7FNK                                       | 2019 | 9       | 41      | 3           |
| Intel Client Systems | NUC8i5BEH                                        | 2018 | 32      | 46      | 4           |
| Intel Client Systems | NUC10i3FNH                                       | 2019 | 9       | 42      | 5           |
| Intel                | NUC5i5RYB H40999-504                             | 2015 | 13      | 32      | 6           |

By Vendor
---------

Devices     — avg. total devices on board,
Non-working — avg. non-working / Linux incompatible devices on board.

| Vendor               | Samples | Devices | Non-working |
|----------------------|---------|---------|-------------|
| Intel Client Systems | 303     | 35      | 1           |
| Intel                | 252     | 31      | 1           |
| Apple                | 173     | 37      | 1           |
| Lenovo               | 91      | 32      | 1           |
| ZOTAC                | 82      | 33      | 1           |
| Hewlett-Packard      | 77      | 34      | 1           |
| AMI                  | 69      | 25      | 1           |
| ASUSTek Computer     | 43      | 36      | 1           |
| AZW                  | 13      | 32      | 1           |
| BESSTAR Tech         | 12      | 29      | 1           |
| Prestigio            | 11      | 33      | 1           |
| Beelink              | 11      | 27      | 1           |
| System76             | 10      | 36      | 1           |
| Compulab             | 7       | 39      | 1           |
| AWOW                 | 7       | 34      | 1           |
| Supermicro           | 5       | 27      | 0           |
| Endless              | 5       | 26      | 0           |
| CompuLab             | 5       | 37      | 1           |
| Dell                 | 4       | 27      | 0           |
| MSI                  | 3       | 25      | 0           |
| CSL-Computer         | 3       | 26      | 1           |
| Orbsmart             | 3       | 22      | 1           |
| RCA                  | 3       | 19      | 1           |
| Chuwi                | 3       | 45      | 1           |
| KOGAN                | 3       | 42      | 1           |
| PCBOX                | 2       | 25      | 0           |
| MiPi PC              | 2       | 22      | 0           |
| MODECOM              | 2       | 21      | 0           |
| HCD                  | 2       | 20      | 0           |
| iBall                | 2       | 44      | 1           |
| Daten Tecnologia     | 2       | 26      | 1           |
| Soyo                 | 2       | 25      | 1           |
| Acute angle          | 2       | 44      | 1           |
